export const environment = {
    testlocal: true,
    Core_URL : 'http://192.168.40.152:8080',
    User_URL : "http://192.168.40.152:8082",
    Email_URL : "http://192.168.40.152:8089",
    Master_URL : "http://192.168.40.152:8083",
    Policy_URL : "http://192.168.40.152:8084",
    User_Activation : "http://192.168.40.152:8082",
    Password_Reset_URL : "http://192.168.40.152:8082",
    Test_User_URL : "http://192.168.40.152:8083",
    Role_URL : "http://192.168.40.152:8082",
    Org_Pass_URL : "http://192.168.40.152:8084",
    Ga_Url : "http://192.168.40.152:8085",
    Org_Cal_URL : "http://192.168.40.152:8081",
    Org_URL : "http://192.168.40.152:8081",
    Org_logo : "http://192.168.40.152:8081/image/",
    Auth_URL : "http://192.168.40.152:8887"
  };
  