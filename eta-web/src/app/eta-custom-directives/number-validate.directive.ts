import { Directive, ElementRef, OnInit, HostBinding, HostListener } from '@angular/core';
import { NG_VALIDATORS, Validator, AbstractControl } from '@angular/forms';

@Directive({
  selector: '[etaNumberValidate]',
  providers: [{ provide: NG_VALIDATORS, useExisting: NumberValidateDirective, multi: true }]
})
export class NumberValidateDirective implements Validator {

  constructor() { }

  public validate(control: AbstractControl): { [key: string]: any } {

    let numRegEx = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/i;
    let valid = numRegEx.test(control.value);
    return control.value < 1 || valid ? null : { 'isNumber': true };

  }

}
