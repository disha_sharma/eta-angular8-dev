import { Directive, ElementRef, OnInit, HostBinding, HostListener } from '@angular/core';
import { NG_VALIDATORS, Validator, AbstractControl } from '@angular/forms';

@Directive({
  selector: '[etaNameValidate]',
  providers: [{ provide: NG_VALIDATORS, useExisting: NameValidateDirective, multi: true }]
})
export class NameValidateDirective implements Validator{

  constructor() { }
  public validate(control: AbstractControl): { [key: string]: any } {
    let nameRegEx = /^[a-zA-Z]+[\-'\s]?[a-zA-Z ]+$/i;
    let valid = nameRegEx.test(control.value);
    return control.value < 1 || valid ? null : { 'isName': true };
  } 

}
