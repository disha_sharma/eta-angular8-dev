import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ArrayType } from '@angular/compiler/src/output/output_ast';
import { Inject } from '@angular/core';//for popup
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';//for popup
/* scope mutli select code first part starts here */
import { SelectionModel } from '@angular/cdk/collections';
import { FlatTreeControl } from '@angular/cdk/tree';
import { Injectable } from '@angular/core';
import { MatTreeFlatDataSource, MatTreeFlattener } from '@angular/material/tree';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { MasterServiceService } from '../master-service/master-service.service';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead';
import { mergeMap } from 'rxjs/operators';
import { TreeviewItem, TreeviewConfig } from '../../../lib';
import { Container } from '../../model/container';
import { DatamodelServiceService } from '../../data-model-setup/datamodel-service/datamodel-service.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';



@Component({
  selector: 'app-add-masterdata',
  templateUrl: './add-masterdata.component.html',
  styleUrls: ['./add-masterdata.component.css']
})

export class AddMasterdataComponent implements OnInit {
  deleteButtonFlag: boolean = false;
  checkAllItem: boolean = false;
  nameField = new FormControl('', [Validators.required]); // for custom input field error
  groupName: string = '';
  groupValue: string = '';
  order: string = '';
  pageSize: number;
  dataSourceGroupName: Observable<any>;
  dataSourceGroupValue: Observable<any>;
  typeaheadLoadingGroup: boolean;
  typeaheadLoadingGroupValue: boolean;
  duplicateGroupName: boolean = false;
  duplicateGroupValue: boolean = false;
  groupList: any[] = [];
  groupValueList: any[] = [];
  addStatus: boolean = false;
  showAddMessage: boolean = false;

  selectedRadio: string = 'manual';
  //FILE UPLOADS
  selectedFiles: FileList;
  currentFileUpload: File;
  uploadFileName: string = "";
  validFile: boolean = false;
  currDateTime: string = "";
  fileUploadButton : boolean = false;

  fileUploaded: boolean = false;

  fileValue: string = "";
  

  countOnEdit : number;
  disabledOnEdit : boolean = true;


  scopeData : TreeviewItem[];
  treeView : TreeviewItem[];
  treeVal : TreeviewItem;
    /**
   * 
    These are variables for the scope tree 

   */
rows: string[];
dropdownEnabled = true;
  items: TreeviewItem[];
  values: number[];
  config = TreeviewConfig.create({
      hasAllCheckBox: true,
      hasFilter: true,
      hasCollapseExpand: true,
      decoupleChildFromParent: false,
      maxHeight: 400
  });

  buttonClasses = [
      'btn-outline-primary',
      'btn-outline-secondary',
      'btn-outline-success',
      'btn-outline-danger',
      'btn-outline-warning',
      'btn-outline-info',
      'btn-outline-light',
      'btn-outline-dark'
  ];
  buttonClass = this.buttonClasses[0];

public containerDetails : Container;

  getErrorMessage() {
    return this.nameField.hasError('required') ? 'You must enter a value' :
      '';
  }
  constructor(private spinnerService: Ng4LoadingSpinnerService, public dialogRef: MatDialogRef<AddMasterdataComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, private fb: FormBuilder, private masterService: MasterServiceService, private dataModelService : DatamodelServiceService) {
      this.spinnerService.show();   
      
        this.dataSourceGroupName = Observable.create((observer: any) => {
            // Runs on every search
            observer.next(this.groupName);
          })
            .pipe(
              mergeMap((token: string) => this.getGroupNameAsObservable(token))
            );

          this.dataSourceGroupValue = Observable.create((observer: any) => {
            // Runs on every search
            observer.next(this.groupValue);
          })
            .pipe(
              mergeMap((token: string) => this.getGroupValueAsObservable(token))
            );
            
            this.dataModelService.getContainerScope().subscribe(response =>{
              this.treeVal = response;
              this.scopeData = [this.treeVal];  
              this.scopeData = this.getData();            
            });      
      }//end of the constructor 

      getData(): any[] { 
        const itCategory = new TreeviewItem(this.scopeData[0]);         
        return [itCategory]; 
      }


      ngAfterViewInit(){
        this.spinnerService.hide();
      }

    onNoClick(): void {
      this.dialogRef.close();
      this.masterService.addStatus = false;
    }

  masterForm: FormGroup;
  
  public getSelectedScope(){ 
    this.dataModelService.getSelectedScopeCount(JSON.stringify(this.scopeData)).subscribe(response =>{
    });      
  }

  ngOnInit() {
        this.pageSize = 10;
        this.currDateTime = "20190408_111214";
        this.masterForm = this.fb.group({
          'groupName': ['', Validators.required],
          'spdScope': [''],
          'fileInput': ['null'],
          'masterGroupValues': this.fb.array([
            this.inititem()
          ])
        });

        /**This will bring all the function names for typeahead */
        this.masterService.getAllGroupNameList().subscribe(data => {
          for (let s of data) {
            this.groupList.push(s);
          }
        });

        /**This will bring all the data item value for typeahead */
        this.masterService.getAllGroupValueList().subscribe(data => {
          for (let s of data) {
            this.groupValueList.push(s);
          }
        });

        

  }

  inititem() {
    return this.fb.group({
      'itemCheckedStatus': [''],
      'groupValue': ['', [Validators.required]],

    });
  }

  changeTypeaheadLoadingGroup(e: boolean): void {
    this.typeaheadLoadingGroup = e;
  }

   typeaheadOnSelectGroup(e: TypeaheadMatch): void {
    this.groupName = e.value;
    if(e && e.value){
      this.masterService.checkDuplicateMasterDataGroupName(e.value).subscribe(data =>{
        if(data.status == true){
          this.masterForm.controls['groupName'].setErrors({ 'groupName': true });
         // this.duplicateGroupName = true;   
        }else {
         // this.duplicateGroupName=false; 
        }
      });
    }
  }


  changeTypeaheadLoadingGroupValue(e: boolean): void {
    this.typeaheadLoadingGroup = e;
  }

  /*
  This Function For Check Group Value Unique When value Select.
  */

 typeaheadOnSelectGroupValue(e: TypeaheadMatch, index): void {
  const FormValues = this.masterForm.controls['masterGroupValues'].value;
  for (let j = 0; j < this.groupValueList.length; j++) {
    var groupValueCheck = this.groupValueList[j].groupValue.toLowerCase();
    if (groupValueCheck == e.value.toLowerCase()) {
      const control = <FormArray>this.masterForm.controls['masterGroupValues'];
      let secondControl = <FormArray>control.controls[index];
      secondControl.controls['groupValue'].setErrors({ 'customError': true });
      return;
    }
  }
}


  getGroupNameAsObservable(token: string): Observable<any> {
    const query = new RegExp(token, 'i');
    return of(
      this.groupList.filter((str: any) => {
        return query.test(str.groupName);
      })
    );
  }

  getGroupValueAsObservable(token: string): Observable<any> {
    const query = new RegExp(token, 'i');
    return of(
      this.groupValueList.filter((str: any) => {
        return query.test(str.groupValue);
      })
    );
  }

  // for Group name Validation 
  omitSpecialChar(event) {
    var k;
    k = event.charCode;  //         k = event.keyCode;  (Both can be used)
    return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || k == 40 || k == 41 || k == 58 || k == 92 || k == 124 || k == 95 || k == 62 || (k >= 48 && k <= 57) || (k > 43 && k < 48));
  }

  //add data object
  additem(event) {
    const control = <FormArray>this.masterForm.controls['masterGroupValues'];
    control.push(this.inititem());
    if(event){
      setTimeout(() => {
        this.scrollToTarget();
      }, 0);
    }
    }
    @ViewChild('masterListScrollableContent', { read: ElementRef, static: true }) public masterListScrollableContent: ElementRef<any>;
    @ViewChild('scrollTarget', { read: ElementRef, static: true }) public scrollTarget: ElementRef<any>;
    scrollToTarget() {
     this.masterListScrollableContent.nativeElement.scrollTo({ top: this.masterListScrollableContent.nativeElement.scrollHeight, behavior: 'smooth' });
     this.scrollTarget.nativeElement.scrollIntoView(false);
  }
 

  //delete data object
  deleteitem() {

    var i = this.masterForm.controls['masterGroupValues'].value.length;
    while (i--) {
      if (this.masterForm.controls['masterGroupValues'].value[i].itemCheckedStatus == true) {
        const control = <FormArray>this.masterForm.controls['masterGroupValues'];
        control.removeAt(i);
      }
    }
    this.selection.clear();
    let lengthAfterDelete = this.masterForm.controls['masterGroupValues'].value.length;
    if(!lengthAfterDelete){
      this.additem(undefined);
    }

  }
  getCurrentDateTime() {

    var fullDate = new Date();
    let year = fullDate.getFullYear();
    let month = fullDate.getMonth() + 1;
    let date = fullDate.getDate();
    let hours = fullDate.getHours();
    let minutes = fullDate.getMinutes();
    let seconds = fullDate.getSeconds();
    let miliSeconds = fullDate.getMilliseconds();
    let currDateTimeString = year + '' + month + '' + date + '_' + hours + '' + minutes + '' + seconds + '' + miliSeconds;
    this.currDateTime = currDateTimeString;
  } 
  userPreferenceChanged() {
    if (this.selectedRadio == 'fileUpload') {
      this.getCurrentDateTime();
      var i = this.masterForm.controls['masterGroupValues'].value.length;
      while (i--) {
        const control = <FormArray>this.masterForm.controls['masterGroupValues'];
        control.removeAt(i);
      }
      this.masterForm.patchValue({
        'fileInput': ''
      })
      this.selection.clear()
    }
    else if (this.selectedRadio == 'manual') {
      this.additem(undefined);
      this.masterForm.patchValue({
        'fileInput': 'MYnULL'
      })
    }
  }
  //for selection
  selection = new SelectionModel(true, []);

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const control = <FormArray>this.masterForm.controls['masterGroupValues'];
    const numSelected = this.selection.selected.length;
    const numRows = control.controls.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    const formControl = <FormArray>this.masterForm.controls['masterGroupValues'];
    this.isAllSelected() ?
      this.selection.clear() :
      formControl.controls.forEach(row => this.selection.select(row));
    let length = formControl.value.length;

    for (let i = 0; i < length; i++) {
      formControl.controls[i].get('itemCheckedStatus').setValue(true);
    }
  }

  /*file upload code starts here */
  fileInput: string = "";
  hide: boolean = false;
  progressBarValue = 0;

  validateAndNotSupportedControls: boolean; // for showing validation and not supported buttons on screen.


  //function for upload file
  uploadFile() {
    if (this.selectedFiles && this.selectedFiles.item(0)) {
      const file = this.selectedFiles.item(0)
      this.selectedFiles = this.selectedFiles;
      this.fileInput = file.name;
      const fileName = this.selectedFiles[0].name;
      const reader = new FileReader();
      reader.readAsDataURL(this.selectedFiles[0]);
      var ext = file.name.substr(file.name.lastIndexOf('.') + 1);
      if (ext === 'csv' || ext === 'xlsx') {
        this.validateFile();
        this.currentFileUpload = this.selectedFiles.item(0);
        this.masterService.pushFileToStorage(this.currentFileUpload, "test", "test").subscribe(response => {
          if (response.status == true) {
            this.progressBarValue = 100;
            this.fileUploaded = true;
            this.fileUploadButton = true;
          } else {
            this.fileNotSupported();
            this.fileUploadButton = false;
          }
        });
      }else {
        this.fileNotSupported();
        this.fileUploadButton = false;
      }
    }
  }

  inputChangeFunction(event) {
    this.fileUploaded = false;
    this.fileUploadButton = false;
    if (event && event.target && event.target.files) {
      this.selectedFiles = event.target.files;
    }
    this.progressBarValue = 0;
    this.masterForm.controls['fileInput'].markAsTouched();
    this.masterForm.controls['fileInput'].setValue(this.fileValue.split('\\').pop());
  }

  /*function for successfully validate File*/
  validateFile() {
    this.progressBarValue = 100;
  }
 /*function for make the file not supported*/
  fileNotSupported() {
    this.progressBarValue = 100;
    this.masterForm.controls['fileInput'].setErrors({ 'invalidFile': true });
    this.masterForm.controls['fileInput'].markAsTouched();
  }

  /*file upload code ends here */
  /* scope multi select second part starts here */
  selected = 'option2';
  selectedInfo = [];
  // selectedValue = 'Shyam';


  
  checkOnKeyUpGroupName(event) {
    this.groupName = event.target.value;
    var value = event.target.value;
    if(event.target && event.target.value){
      this.masterService.checkDuplicateMasterDataGroupName(value).subscribe(data =>{
        if(data.status == true){
          this.masterForm.controls['groupName'].setErrors({ 'groupName': true });
         // this.duplicateGroupName = true;   
        }else {
         // this.duplicateGroupName=false; 
        }
      });
    }
  }
  /*
   This Function For Check Group Value Unique When value Typed.
   */
  checkDuplicateGroupValue(index, groupValue) {
    //check value from type ahead.
    const FormValues = this.masterForm.controls['masterGroupValues'].value;
    for (let j = 0; j < this.groupValueList.length; j++) {
      var groupValueTrim = this.groupValueList[j].groupValue.trim();
      var groupValueCheck = groupValueTrim.toLowerCase();
      var currentValue = groupValue.trim();
      if (groupValueCheck == currentValue.toLowerCase()) {
        const control = <FormArray>this.masterForm.controls['masterGroupValues'];
        let secondControl = <FormArray>control.controls[index];
        secondControl.controls['groupValue'].setErrors({ 'customError': true });
        return;
      }
    }
    //check value when type duplicate values.
    let count = 0;
    if (FormValues.length > 0) {
      for (let k = 0; k < FormValues.length; k++) {
        var formValues = FormValues[k].groupValue.trim();
        var formValueCheck = formValues.toLowerCase();
        var currentCheckValues = groupValue.trim();
        if (formValueCheck == currentCheckValues.toLocaleLowerCase()) {
          count++;
        }
      }
      if (count > 1) {
        const control = <FormArray>this.masterForm.controls['masterGroupValues'];
        let secondControl = <FormArray>control.controls[index];
        secondControl.controls['groupValue'].setErrors({ 'customError': true });
      }
    }
  }


  /*
  This function is use for When Name AlphaNumeric Validation.
  */
 checkCharValidationForName(value) {
  var str = value.target.value;
      if (str) {
      const matches = str.match(/^[A-Za-z][A-Za-z0-9/.,->_()\\*|:" ]*$/);
      return matches?null: this.masterForm.controls['groupName'].setErrors({ 'patternError': true });
      }
}

checkCharValidationForGroupValue(value,index){
  var str = value.target.value;
      if (str) {
      const matches = str.match(/^[A-Za-z][A-Za-z0-9/.,->_()\\*|:" ]*$/);
      const control = <FormArray>this.masterForm.controls['masterGroupValues'];
        let secondControl = <FormArray>control.controls[index];
      return matches?null: secondControl.controls['groupValue'].setErrors({ 'patternGroupError': true });
      }
}

  /** ********Add Master Data ***************/
  addMasterData() {
    this.duplicateGroupValue = false;
    if (!this.masterForm.valid) {
      return;
    }
    if (this.selectedRadio == "fileUpload") {
      this.masterForm.value['fileName'] = this.currentFileUpload.name;
    }

    this.masterService.addMasterData(this.masterForm.value, JSON.stringify(this.scopeData), 0).subscribe(response => {
        this.dialogRef.close();
        this.showAddMessage = true;
        this.masterService.addStatus = true;
        this.masterService.groupName = response.groupName;
    }, error => {
        this.dialogRef.close();
        this.masterService.addError = true;
        this.masterService.addStatus = false;
        this.masterService.addFunctionError = "Error in adding function.";
    }
    )

  }


  ngAfterViewChecked(){
    if(this.countOnEdit != 0){
      if(this.countOnEdit != this.values.length){
        this.disabledOnEdit = true;
      }
    }
    this.countOnEdit = this.values.length;
  }

}
