import { Component, OnInit, ViewChild, ElementRef, ChangeDetectorRef } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource, MatDialog, MatDialogConfig } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient, HttpHeaderResponse } from '@angular/common/http';
import { ServiceService } from './../../eta-services/service.service'
import { merge, Observable, of as observableOf } from 'rxjs';
import { catchError, map, startWith, switchMap } from 'rxjs/operators';
import { SelectionModel } from '@angular/cdk/collections';
import { DashboardServiceService } from '../../dash-board/dashboard-service.service';
import { AddMasterdataComponent } from '../add-masterdata/add-masterdata.component';
import { EditMasterdataComponent } from '../edit-masterdata/edit-masterdata.component';
import { MasterServiceService } from '../master-service/master-service.service';
import { trigger, state, style, transition, animate } from '@angular/animations';




@Component({
  selector: 'app-master-dashboard',
  templateUrl: './master-dashboard.component.html',
  styleUrls: ['./master-dashboard.component.css'],
  animations: [
    trigger('detailExpand', [
      state('void', style({ height: '0px', minHeight: '0', visibility: 'hidden' })),
      state('*', style({ height: '*', visibility: 'visible' })),
      transition('void <=> *', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
   ]),
  ]
})
export class MasterDashboardComponent implements OnInit {
  Master_URL: any;
  public shown: string = 'MASTER';

  public menuList: any = [];
  public mainbreadcrumb: string;
  public breadcrumb: string;
  pageSize: number;
  resultsLength = 0;
  isLoadingResults = true;
  isRateLimitReached = false;
  httpResponse: number = 0;
  public defaultColumnToSort: any;
  masterDataHttpDao: MasterDataHttpDao | null;
  gridMasterDataColumns: any;
  public message: string;
  public isEditAllowed: boolean = false;
  dataSource: MatTableDataSource<MasterFormData>;

  readMoreCheckbox: boolean;
  displayedColumns: string[] = ['checkbox', 'group', 'items', 'type', 'scope', 'status'];
  masterDataSource: MatTableDataSource<MasterFormData>;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  pageSizeOptions = "";
  groupName: string;
  groupId: string;
  successAdded: boolean = false;
  successUpdated: boolean = false;
  enableFlag: boolean = false;
  masterList: any[] = [];

  constructor(private http: HttpClient, private route: ActivatedRoute, private r: Router, private shareServices: ServiceService, private dashboardService: DashboardServiceService, public dialog: MatDialog, private changeDetectorRefs: ChangeDetectorRef, private masterService: MasterServiceService) {
    this.defaultColumnToSort = "groupName";
    this.Master_URL = this.shareServices.Master_URL;
    this.getHeaderMenuList();

  }//end of the constructor

  ngOnInit() {

    this.pageSize = 5;
    // If the user changes the sort order, reset back to the first page.
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    this.masterDataHttpDao = new MasterDataHttpDao(this.http, this.dashboardService);
    // If the user changes the sort order, reset back to the first page.
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        startWith({}),
        switchMap(() => {
          this.isLoadingResults = true;
          if (this.paginator.pageSize != undefined) {
            this.pageSize = this.paginator.pageSize;
          }
          return this.masterDataHttpDao!.getRepoIssues(this.sort.active, this.sort.direction, this.paginator.pageIndex, this.pageSize);
        }),
        map(data => {
          // Flip flag to show that loading has finished.
          this.isLoadingResults = false;
          this.isRateLimitReached = false;
          this.resultsLength = data.recordsTotal;
          console.log("data length : " + data.recordsTotal);
          return data.data;
        }),
        catchError((response: HttpHeaderResponse) => {
          if (response.status === 404) {
            this.httpResponse = response.status;
          }

          this.isLoadingResults = false;
          // Catch if the GitHub API has reached its rate limit. Return empty data.
          this.isRateLimitReached = true;
          return observableOf([]);
        })
      ).subscribe(data => {
        this.httpResponse = 200;
        this.masterDataSource = new MatTableDataSource(data);
      });



  }

  public resetAll() {
    this.successAdded = false;
    this.successUpdated = false;
    this.enableFlag = false;
  }

  onPaginateChange(event) {
    this.pageSizeOptions = event.pageSizeOptions;
    console.log("page Size index : " + this.pageSizeOptions);
  }


  public clearShareObj() {
    this.shareServices.setgridChangeMessage(null);
  }


  getHeaderMenuList() {
    this.shareServices.getHeaderMenu()
      .subscribe(res => {
        this.menuList = res;
        this.menuList.forEach(element => {
          if (element.routerLink == "#2") {
            this.mainbreadcrumb = element.translation;
          }
          if (element.routerLink == "eta-web/masterdata") {
            this.breadcrumb = element.translation;
          }
        })

      })
  }


  shownRouterLink(routerlink) {
    this.r.navigate(["/" + routerlink]);
  }

  public activeLink = 'masterdata';
  background = '';

  toggleBackground() {
    this.background = this.background ? '' : 'primary';
  }

  isExpansionDetailRow = (index, row) => row.hasOwnProperty('detailRow');

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
  }
  stopPropagationFunction(event) {
    event.stopPropagation();
  }
  markActiveButton(event) {
    document.getElementsByClassName('active-button')[0].classList.remove('active-button');
    event.target.classList.add('active-button');
  }

  //for selection
  selection = new SelectionModel(true, []);

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.masterDataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.masterDataSource.data.forEach(row => this.selection.select(row));
  }
  check() {
    console.log(this.selection);
  }

  //for popup
  animal: string;
  name: string;

  openAddDialog(): void {
    this.resetAll();

    const dialogRef = this.dialog.open(AddMasterdataComponent, {
      disableClose: true,
      data: { name: this.name, animal: this.animal }
    });

    dialogRef.afterClosed().subscribe(result => {

      if (this.masterService.addStatus == true) {
        this.successAdded = true;
        this.refreshMasterDataList();
        this.masterList.splice(0, this.masterList.length);
        this.groupName = this.masterService.groupName; //this.dashboardService.groupName;
      }

      this.animal = result;
    });
  }
  // popup ends


  openEditDialog(row): void {
    
    console.log(row);
    
    this.resetAll();

    const dialogRef = this.dialog.open(EditMasterdataComponent, {
      disableClose: true,
      width: '500px',
      data: { name: this.name, animal: this.animal, editableRow: row }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.refreshMasterDataList();
      this.masterList.splice(0, this.masterList.length);
      this.groupName = this.masterService.groupName;
      if (this.masterService.updateStatus == true) {
        this.successUpdated = true;
        this.refreshMasterDataList();
        this.masterList.splice(0, this.masterList.length);
        this.groupName = this.masterService.groupName;
      }

      this.animal = result;
    });
  }
  // popup ends

  public refreshMasterDataList() {
    this.dashboardService.getMasterDataTableDetails("asc", 0, this.pageSize).subscribe(response => {
      this.resultsLength = response.recordsTotal;
      this.masterDataSource = new MatTableDataSource(response.data);
      this.paginator.pageIndex = 0;
      this.pageSize = 5;
    })
    this.changeDetectorRefs.detectChanges();
  }


  removeSuccess() {
    console.log("removeSuccess");
    this.successAdded = false;
    this.refreshMasterDataList();
    this.masterList.splice(0, this.masterList.length);
  }

  removeUpdate() {
    console.log("removeUpdate");
    this.successUpdated = false;
    this.refreshMasterDataList();
    this.masterList.splice(0, this.masterList.length);
  }

}


export interface MasterFormData {
  groupID: number;
  groupName: string;
  isActive: number;
  itemcount: number;
  scope: number;
  status: number;
  createdBy: number;
  createdDate: string;
  modifiedBy: number;
  modifiedDate: string;
  createdName: string;
  modifiedName: string;
  recordsTotal: number;
}

export class MasterDataHttpDao {
  constructor(private http: HttpClient, private dashboardService: DashboardServiceService) {
  }

  getRepoIssues(sort: string, order: string, page: number, pageSize: number): Observable<any> {
    console.log("order : " + order);
    console.log("page : " + page);
    console.log("pageSize : " + pageSize);
    if (order.length === 0)
      order = "asc";
    return this.dashboardService.getMasterDataTableDetails(order, page, pageSize);
  }

}//end of the class
