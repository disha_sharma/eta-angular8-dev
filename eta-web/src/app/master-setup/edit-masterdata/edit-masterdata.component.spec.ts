import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditMasterdataComponent } from './edit-masterdata.component';

describe('EditMasterdataComponent', () => {
  let component: EditMasterdataComponent;
  let fixture: ComponentFixture<EditMasterdataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditMasterdataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditMasterdataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
