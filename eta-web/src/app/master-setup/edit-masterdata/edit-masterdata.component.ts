import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import {FormControl} from '@angular/forms';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ArrayType } from '@angular/compiler/src/output/output_ast';
import {  Inject } from '@angular/core';//for popup
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';//for popup
/* scope mutli select code first part starts here */
import {SelectionModel} from '@angular/cdk/collections';
import {FlatTreeControl} from '@angular/cdk/tree';
import { Injectable} from '@angular/core';
import {MatTreeFlatDataSource, MatTreeFlattener} from '@angular/material/tree';
import {BehaviorSubject, Observable, of} from 'rxjs';
import { MasterServiceService } from '../master-service/master-service.service';
import { DialogData } from '../../form/core/add-task-detail/add-task-detail.component';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead';
import { mergeMap } from 'rxjs/operators';
import { TreeviewItem, TreeviewConfig } from '../../../lib';
import { DatamodelServiceService } from '../../data-model-setup/datamodel-service/datamodel-service.service';

/**
 * Node for to-do item
 */
export class TodoItemNode {
  children: TodoItemNode[];
   item: string;
 }

/** Flat to-do item node with expandable and level information */
export class TodoItemFlatNode {
  item: string;
  level: number;
  expandable: boolean;
}

/**
 * The Json object for to-do list data.
 */
const TREE_DATA = {
  Groceries: {
    'Almond Meal flour': null,
    'Organic eggs': null,
    'Protein Powder': null,
    Fruits: {
      Apple: null,
      Berries: ['Blueberry', 'Raspberry'],
      Orange: null
    }
  }
};

/**
 * Checklist database, it can build a tree structured Json object.
 * Each node in Json object represents a to-do item or a category.
 * If a node is a category, it has children items and new items can be added under the category.
 */
@Injectable()
export class ChecklistDatabase {
  dataChange = new BehaviorSubject<TodoItemNode[]>([]);

  get data(): TodoItemNode[] { return this.dataChange.value; }

  constructor() {
    this.initialize();
  }

  initialize() {
    // Build the tree nodes from Json object. The result is a list of `TodoItemNode` with nested
    //     file node as children.
    const data = this.buildFileTree(TREE_DATA, 0);

    // Notify the change.
    this.dataChange.next(data);
  }

  /**
   * Build the file structure tree. The `value` is the Json object, or a sub-tree of a Json object.
   * The return value is the list of `TodoItemNode`.
   */
  buildFileTree(obj: object, level: number): TodoItemNode[] {
    return Object.keys(obj).reduce<TodoItemNode[]>((accumulator, key) => {
      const value = obj[key];
      const node = new TodoItemNode();
      node.item = key;

      if (value != null) {
        if (typeof value === 'object') {
          node.children = this.buildFileTree(value, level + 1);
        } else {
          node.item = value;
        }
      }

      return accumulator.concat(node);
    }, []);
  }

  /** Add an item to to-do list */
  insertItem(parent: TodoItemNode, name: string) {
    if (parent.children) {
      parent.children.push({item: name} as TodoItemNode);
      this.dataChange.next(this.data);
    }
  }

  updateItem(node: TodoItemNode, name: string) {
    node.item = name;
    this.dataChange.next(this.data);
  }
}
@Component({
  selector: 'app-edit-masterdata',
  templateUrl: './edit-masterdata.component.html',
  styleUrls: ['./edit-masterdata.component.css'],
  providers: [ChecklistDatabase]
})
export class EditMasterdataComponent implements OnInit {
  deleteButtonFlag: boolean = false;
  checkAllItem: boolean = false;
  nameField = new FormControl('', [Validators.required]); // for custom input field error
  groupName : string = '';
  groupValue: string = '';
  scopeId : number;
  dataSourceGroupName : Observable<any>;
  dataSourceGroupValue : Observable<any>;
  typeaheadLoadingGroup : boolean;
  typeaheadLoadingGroupValue : boolean;
  duplicateGroupName :  boolean = false;
  duplicateGroupValue :  boolean = false;
  groupList: any[] = [];
  groupValueList: any[] = [];
  scopeData : TreeviewItem[];
  treeView : TreeviewItem[];
  treeVal : TreeviewItem;
  editRealName : string = "";
  disabledOnEdit : boolean = false;
  countOnEdit : number = 0;
  checkInit : boolean = false;

  GroupFormValues : any[] =[];




    /**
   * 
    These are variables for the scope tree 

   */
rows: string[];
dropdownEnabled = true;
  items: TreeviewItem[];
  values: number[];
  config = TreeviewConfig.create({
      hasAllCheckBox: true,
      hasFilter: true,
      hasCollapseExpand: true,
      decoupleChildFromParent: false,
      maxHeight: 400
  });

  buttonClasses = [
      'btn-outline-primary',
      'btn-outline-secondary',
      'btn-outline-success',
      'btn-outline-danger',
      'btn-outline-warning',
      'btn-outline-info',
      'btn-outline-light',
      'btn-outline-dark'
  ];
  buttonClass = this.buttonClasses[0];

  getErrorMessage() {
   return this.nameField.hasError('required') ? 'You must enter a value' :
      '';
 }
 constructor(public dialogRef: MatDialogRef<EditMasterdataComponent>,
  @Inject(MAT_DIALOG_DATA) public data: any, private fb: FormBuilder, private database: ChecklistDatabase, 
  private masterService:MasterServiceService, private dataModelService : DatamodelServiceService) {  
  console.log("Edit records details : "+this.data);
  console.log(this.data.editableRow.scopeId);
  this.scopeId = this.data.editableRow.scopeId;

  this.dataSourceGroupName = Observable.create((observer: any) => {
    // Runs on every search
    observer.next(this.groupName);
  })
  .pipe(
    mergeMap((token: string) => this.getGroupNameAsObservable(token))
  );

  this.dataSourceGroupValue= Observable.create((observer: any) => {
    // Runs on every search
    observer.next(this.groupValue);
  })
  .pipe(
    mergeMap((token: string) => this.getGroupValueAsObservable(token))
  );


  this.treeFlattener = new MatTreeFlattener(this.transformer, this.getLevel,
  this.isExpandable, this.getChildren);
  this.treeControl = new FlatTreeControl<TodoItemFlatNode>(this.getLevel, this.isExpandable);
  this.dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);

  database.dataChange.subscribe(data => {
    this.dataSource.data = data;
  });
  }
  onNoClick(): void {
    this.dialogRef.close();
    this.masterService.updateStatus = false;
  }
  masterForm: FormGroup;

  ngOnInit() {
      console.log("scopeId  : "+this.scopeId);
    
      this.dataModelService.getContainerScopeByScopeId(this.scopeId).subscribe(response =>{
        this.treeVal = response;
        this.scopeData = [this.treeVal];  
        this.scopeData = this.getData();            
      });      

      /**This will bring all the function names for typeahead */
      this.masterService.getAllGroupNameList().subscribe(data => {
        for(let s of data) {
          this.groupList.push(s);
        }
      }); 
    
     /**This will bring all the data item value for typeahead */ 
     this.masterService.getAllGroupValueList().subscribe(data => {
          for(let s of data) {
            this.groupValueList.push(s);
          }
      }); 

    this.expandNodesInitially(); //for scope tree

    this.masterForm = this.fb.group({
      'groupName': ['', Validators.required],
      'groupID': '',
      'spdScope' : [''],
      'fileInput': [''],
      'isActive': this.data.editableRow.isActive,
      'status' : this.data.editableRow.status,
      'typeId' : this.data.editableRow.typeId,
      'masterGroupValues': this.fb.array([
        this.inititem()
      ])
    });

    this.editForm();
}

  getData(): any[] { 
      const itCategory = new TreeviewItem(this.scopeData[0]);         
      return [itCategory]; 
  }


inititem() {
  return this.fb.group({
   'itemCheckedStatus': [''],
   'groupValue': ['', [Validators.required]],
    'id': ''
  });
}

ngAfterViewChecked(){
  console.log("Changed length :"+this.values.length);
  if(this.countOnEdit != 0){
    if(this.countOnEdit != this.values.length){
      this.disabledOnEdit = true;
    }
  }
  
  this.countOnEdit = this.values.length;
}

enableEdit(){
  this.disabledOnEdit = true;
  console.log(this.disabledOnEdit);
}


changeTypeaheadLoadingGroup(e: boolean): void {
  this.typeaheadLoadingGroup = e;
}

typeaheadOnSelectGroup(e: TypeaheadMatch): void {
  this.groupName = e.value;
  if(e && e.value){
    this.masterService.checkDuplicateMasterDataGroupName(e.value).subscribe(data =>{
      if(data.status == true){
        if(this.editRealName !== e.value.trim()){
          this.masterForm.controls['groupName'].setErrors({ 'duplicate': true });
         this.duplicateGroupName = true;
        }else {
          this.duplicateGroupName=false; 
        } 
      }else {
        this.duplicateGroupName=false; 
      }
    });
  }
}

changeTypeaheadLoadingGroupValue(e: boolean): void {
  this.typeaheadLoadingGroup = e;
}

typeaheadOnSelectGroupValue(e: TypeaheadMatch,index): void {
  //this.groupName = e.value;
  const FormValues = this.masterForm.controls['masterGroupValues'].value;
  for(let i=0;i<this.GroupFormValues.length;i++){
    var checkConstant = this.GroupFormValues[i].groupValue.toLowerCase();
    if(checkConstant == e.value.toLowerCase()){
      let count = 0;
      if(FormValues.length>0){
        for(let k=0;k<FormValues.length;k++){
          var formValueCheck = FormValues[k].groupValue.toLowerCase();
          if(formValueCheck == e.value.toLocaleLowerCase()){
            count++;
          }
        }
        if(count>1){
          const control = <FormArray>this.masterForm.controls['masterGroupValues'];
          let secondControl = <FormArray>control.controls[index];
          secondControl.controls['groupValue'].setErrors({ 'customError': true });
        }
      }
      return;
    }
  }
  for(let j=0;j<this.groupValueList.length;j++){
    var groupValueCheck = this.groupValueList[j].groupValue.toLowerCase();
    if(groupValueCheck == e.value.toLowerCase()){
          const control = <FormArray>this.masterForm.controls['masterGroupValues'];
          let secondControl = <FormArray>control.controls[index];
          secondControl.controls['groupValue'].setErrors({ 'customError': true });
          return;
        }
      }

     
}

/*
  This Function For Check Group Value Unique When value Typed.
  */
 checkDuplicateGroupValue(index, groupValue) {
  const FormValues = this.masterForm.controls['masterGroupValues'].value;
  for(let i=0;i<this.GroupFormValues.length;i++){
    var checkTrim = this.GroupFormValues[i].groupValue.trim();
    var checkConstant = checkTrim.toLowerCase();
    var currentCheck = groupValue.trim();
    if(checkConstant == currentCheck.toLowerCase()){
      let count = 0;
      //Check Duplicate value when same as select in edit values.
      if(FormValues.length>0){
        for(let k=0;k<FormValues.length;k++){
          var formTrim = FormValues[k].groupValue.trim();
          var formValueCheck = formTrim.toLowerCase();
          var currentCheckForm = groupValue.trim();
          if(formValueCheck == currentCheckForm.toLocaleLowerCase()){
            count++;
          }
        }
        if(count>1){
          const control = <FormArray>this.masterForm.controls['masterGroupValues'];
          let secondControl = <FormArray>control.controls[index];
          secondControl.controls['groupValue'].setErrors({ 'customError': true });
        }
      }
      return;
    }
  }
  //Check Duplicate value when new values select.
  let countNew = 0;
  if(FormValues.length>0){
    for(let k=0;k<FormValues.length;k++){
      var formNewTrim = FormValues[k].groupValue.trim();
      var formValueCheck = formNewTrim.toLowerCase();
      var newCurrentTrim = groupValue.trim();
      if(formValueCheck == newCurrentTrim.toLocaleLowerCase()){
        countNew++;
      }
    }
    if(countNew>1){
      const control = <FormArray>this.masterForm.controls['masterGroupValues'];
      let secondControl = <FormArray>control.controls[index];
      secondControl.controls['groupValue'].setErrors({ 'customError': true });
    }
  }
  //Check Duplicate value when new values select from type ahead list.
  for(let j=0;j<this.groupValueList.length;j++){
    var listTrim = this.groupValueList[j].groupValue.trim();
    var groupValueCheck = listTrim.toLowerCase();
    var listCurrent = groupValue.trim();
    if(groupValueCheck == listCurrent.toLowerCase()){
          const control = <FormArray>this.masterForm.controls['masterGroupValues'];
          let secondControl = <FormArray>control.controls[index];
          secondControl.controls['groupValue'].setErrors({ 'customError': true });
          return;
        }
      }
}

checkOnKeyUpGroupName(event){
  this.groupName = event.target.value;
  if(event && event.target.value){
    this.masterService.checkDuplicateMasterDataGroupName(event.target.value).subscribe(data =>{
      if(data.status == true){
        if(this.editRealName.trim() !== event.target.value.trim()){
          this.masterForm.controls['groupName'].setErrors({ 'duplicate': true });
         this.duplicateGroupName = true;
        }else {
          this.duplicateGroupName=false; 
        } 
      }else {
        this.duplicateGroupName=false; 
      }
    });
  }
}

checkCharValidationForGroupValue(value,index){
  var str = value.target.value;
      if (str) {
      const matches = str.match(/^[A-Za-z][A-Za-z0-9/.,->_()\\*|:" ]*$/);
      const control = <FormArray>this.masterForm.controls['masterGroupValues'];
        let secondControl = <FormArray>control.controls[index];
      return matches?null: secondControl.controls['groupValue'].setErrors({ 'patternGroupError': true });
      }
}


getGroupNameAsObservable(token: string): Observable<any> {
  const query = new RegExp(token, 'i'); 
  return of(
    this.groupList.filter((str: any) => {
      return query.test(str.groupName);
    })
  );
}

getGroupValueAsObservable(token: string): Observable<any> {
  const query = new RegExp(token, 'i'); 
  return of(
    this.groupValueList.filter((str: any) => {
      return query.test(str.groupValue);
    })
  );
}


//add data object
additem(event) {
  const control = <FormArray>this.masterForm.controls['masterGroupValues'];
  control.push(this.inititem());

  if(event){
    setTimeout(() => {
      this.scrollToTarget();
    }, 0);
  }
  }
  @ViewChild('masterListScrollableContent', { read: ElementRef, static: true }) public masterListScrollableContent: ElementRef<any>;
  @ViewChild('masterListScrollTarget', { read: ElementRef, static: true }) public masterListScrollTarget: ElementRef<any>;
  scrollToTarget() {
   this.masterListScrollableContent.nativeElement.scrollTo({ top: this.masterListScrollableContent.nativeElement.scrollHeight, behavior: 'smooth' });
   this.masterListScrollTarget.nativeElement.scrollIntoView(false);
}

//delete data object
deleteitem(){
  let arr = [];
 var i = this.masterForm.controls['masterGroupValues'].value.length;
  while (i--) {
     if (this.masterForm.controls['masterGroupValues'].value[i].itemCheckedStatus == true) {
      arr.push(this.masterForm.controls['masterGroupValues'].value[i]);
      const control = <FormArray>this.masterForm.controls['masterGroupValues'];
      control.removeAt(i);
      }
  }
  //delete from patch value array.
  for (let z = 0; z < arr.length; z++) {
    for (let k = 0; k < this.GroupFormValues.length; k++) {
        if (arr[z].groupValue == this.GroupFormValues[k].groupValue) {
          this.GroupFormValues.splice(z, 1);
      }
    }
  }
  this.selection.clear();
  let lengthAfterDelete = this.masterForm.controls['masterGroupValues'].value.length;
  if(!lengthAfterDelete){
    this.additem(undefined);
  }
  console.log('this.GroupFormValues',this.GroupFormValues);
}
deleteAllitems(){
  let length = this.masterForm.controls['masterGroupValues'].value.length;
  for(let i = 0;i < length;  i++){
    const control = <FormArray>this.masterForm.controls['masterGroupValues'];
      control.removeAt(i);
  }
}
editForm(){
  this.deleteAllitems();
  this.GroupFormValues = this.data.editableRow.masterGroupValues;
  let jsonGroupLength = this.data.editableRow.masterGroupValues.length;
  for(let i = 0; i < jsonGroupLength; i++ ){
    this.additem(undefined);
  }
this.editRealName = this.data.editableRow.groupName;
this.groupName = this.data.editableRow.groupName;
  this.masterForm.patchValue({
    'groupName': this.data.editableRow.groupName,
    'groupID': this.data.editableRow.groupID,
    'masterGroupValues': this.data.editableRow.masterGroupValues
  })
}
submit(){
  console.log(this.masterForm.value);
  
}

// for Group name Validation 
omitSpecialChar(event){   
   var k;  
   k = event.charCode;  //         k = event.keyCode;  (Both can be used)
   return((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || k == 40 || k == 41 || k == 58 || k == 92 || k == 124 || k == 95 || k == 62 || (k >= 48 && k <= 57) || (k > 43 && k <48)); 
}

  //for selection
  selection = new SelectionModel(true, []);

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const control = <FormArray>this.masterForm.controls['masterGroupValues'];
    const numSelected = this.selection.selected.length;
    const numRows = control.controls.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    const formControl = <FormArray>this.masterForm.controls['masterGroupValues'];
    this.isAllSelected() ?
        this.selection.clear() :
      formControl.controls.forEach(row => this.selection.select(row));
        let length = formControl.value.length;
        
        for(let i = 0; i < length; i++){
          formControl.controls[i].get('itemCheckedStatus').setValue(true);
         }
  }

myFunction(){
  console.log(this.masterForm);
  
}
/*file upload code starts here */
fileInput: string = "";
  hide: boolean = false;
  fileNotSupported: boolean;
  newInput: string;
  validatedStatus: boolean;
  uploadButtonEnabled: boolean = false;
  progressBarValue = 0;

  validateAndNotSupportedControls: boolean; // for showing validation and not supported buttons on screen.


  //function for upload file
  uploadFile() {
    if(this.validatedStatus){
      return true;
    }
    else{
    this.validateAndNotSupportedControls = true;
    this.progressBarValue = 50;
    }
  }
//function for successfully Validate
  validate() {
    this.validatedStatus = true;
    this.progressBarValue = 100;
    this.uploadButtonEnabled = true;
    this.validateAndNotSupportedControls = false;
  }
  // function for make the file not supported
  notSupported(){
    this.progressBarValue = 100;
    this.fileNotSupported = true;
    this.validatedStatus = false;
    this.uploadButtonEnabled = false;
    this.validateAndNotSupportedControls = false;

  }
  inputChangeFunction(){

    this.fileNotSupported = false;
    this.validatedStatus = false;
    this.validateAndNotSupportedControls = false;
    this.progressBarValue = 0;
    if(this.fileInput) {
      this.uploadButtonEnabled = true;
    }
    else{
      this.uploadButtonEnabled = false;
    }


  }

/*file upload code ends here */
/* scope multi select second part starts here */
selected = 'option2';
selectedInfo = [];
// selectedValue = 'Shyam';


/** Map from flat node to nested node. This helps us finding the nested node to be modified */
flatNodeMap = new Map<TodoItemFlatNode, TodoItemNode>();

/** Map from nested node to flattened node. This helps us to keep the same object for selection */
nestedNodeMap = new Map<TodoItemNode, TodoItemFlatNode>();

/** A selected parent node to be inserted */
selectedParent: TodoItemFlatNode | null = null;

/** The new item's name */
newItemName = '';

treeControl: FlatTreeControl<TodoItemFlatNode>;

treeFlattener: MatTreeFlattener<TodoItemNode, TodoItemFlatNode>;

dataSource: MatTreeFlatDataSource<TodoItemNode, TodoItemFlatNode>;

/** The selection for checklist */
checklistSelection = new SelectionModel<TodoItemFlatNode>(true /* multiple */);



getLevel = (node: TodoItemFlatNode) => node.level;

isExpandable = (node: TodoItemFlatNode) => node.expandable;

getChildren = (node: TodoItemNode): TodoItemNode[] => node.children;

hasChild = (_: number, _nodeData: TodoItemFlatNode) => _nodeData.expandable;

hasNoContent = (_: number, _nodeData: TodoItemFlatNode) => _nodeData.item === '';

/**
* Transformer to convert nested node to flat node. Record the nodes in maps for later use.
*/
transformer = (node: TodoItemNode, level: number) => {
const existingNode = this.nestedNodeMap.get(node);
const flatNode = existingNode && existingNode.item === node.item
    ? existingNode
    : new TodoItemFlatNode();
flatNode.item = node.item;
flatNode.level = level;
flatNode.expandable = !!node.children;
this.flatNodeMap.set(flatNode, node);
this.nestedNodeMap.set(node, flatNode);
return flatNode;
}
/* expand all node automatically on initialization. */
expandNodesInitially(){
for (let i = 0; i < this.treeControl.dataNodes.length; i++) {

    this.treeControl.expand(this.treeControl.dataNodes[i])
}
}

/** Whether all the descendants of the node are selected */
descendantsAllSelected(node: TodoItemFlatNode): boolean {
const descendants = this.treeControl.getDescendants(node);
return descendants.every(child => this.checklistSelection.isSelected(child));
}

/** Whether part of the descendants are selected */
descendantsPartiallySelected(node: TodoItemFlatNode): boolean {
const descendants = this.treeControl.getDescendants(node);
const result = descendants.some(child => this.checklistSelection.isSelected(child));
return result && !this.descendantsAllSelected(node);
}

/** Toggle the to-do item selection. Select/deselect all the descendants node */
todoItemSelectionToggle(node: TodoItemFlatNode): void {
 this.checklistSelection.toggle(node);
 const descendants = this.treeControl.getDescendants(node);
 this.checklistSelection.isSelected(node)
  ? this.checklistSelection.select(...descendants)
  : this.checklistSelection.deselect(...descendants);
}
stopPropegationFunction(event) {
event.stopPropagation();
}
/*custom function for get values.*/
checkNodes() {

this.selectedInfo = [];

const completeNodes = this.treeControl.dataNodes;

const descAllSelected = completeNodes.forEach(child => {

  if(this.checklistSelection.isSelected(child)) {

   // this.selectedInfo.push({name: child.item, status: true})
   this.selectedInfo.push(child.item);
this.masterForm.patchValue({
  spdScope: {scope: '[{"ORG":"1","FUN":"","PRO":"","TASKF":""}]'}
})
  }

});

if(this.selectedInfo.length == 1){
this.selected = 'oneChecked';
}
else if(this.selectedInfo.length > 1){
this.selected = 'multipleChecked';
}
else if(this.selectedInfo.length == 0){
this.selected = '';
this.masterForm.patchValue({
  scope: this.selectedInfo
})
}
 console.log(this.selectedInfo);

}


/** ********Add Master Data ***************/
  addMasterData() {
    this.duplicateGroupName = false;
    this.duplicateGroupValue = false;
    this.masterService.addMasterData(this.masterForm.value, JSON.stringify(this.scopeData), this.scopeId).subscribe(response => {
      this.dialogRef.close();
      this.masterService.updateStatus = true;
      this.masterService.groupName = response.groupName;
    })

}

   /*
  This function is use for When Name AlphaNumeric Validation.
  */
 checkCharValidationForName(value) {
  var str = value.target.value;
      if (str) {
      const matches = str.match(/^[A-Za-z][A-Za-z0-9/.,->_()\\*|:" ]*$/);
      return matches?null: this.masterForm.controls['groupName'].setErrors({ 'patternError': true });
      }
}

/** ********get typeahed for group name ***************/
/* getAllGroupNameList(){
 this.masterService.getAllGroupNameList().subscribe(response => {
   console.log("getAllGroupNameList typeahhed response : "+response);      
 })
 this.dialogRef.close();
 } */

 /** ********get typeahed for group value ***************/
/* getAllGroupValueList(){
  this.masterService.getAllGroupValueList().subscribe(response => {
    console.log("getAllGroupValueList typeahhed response : "+response);      
  })
  this.dialogRef.close();
  } */


  checkIfChecked(){
    console.log("TEST ----------------------"+this.checkInit);
    if(!this.checkInit)
        this.disabledOnEdit = false;

    console.log(this.disabledOnEdit);
    
  }
}
