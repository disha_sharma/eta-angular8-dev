import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment.test';
import { TreeviewItem, TreeviewConfig } from '../../../lib';
import { MasterDataMapping } from '../../model/master-data-mapping';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class MasterServiceService {
  adminRole = "admin";
  myRole;
  
  addStatus:boolean = false;
  updateStatus:boolean = false;
  successUpdated :boolean = false;
  cancelAdd : boolean = false;
  cancelEdit : boolean = false;
  addFunctionError : string = "";
  addError : boolean = false;
  groupName :  string = "";

  constructor(private cookieService: CookieService, private httpClient: HttpClient) { 
    this.myRole = cookieService.get('my_role');
    this.checkedSessionToken();
  }

  /* toGetToken */
  public checkedSessionToken(): boolean {
    let istokenExist: boolean = false;
    if (sessionStorage.getItem("AuthToken") != null) {
      istokenExist = true;
    }
    return istokenExist;
  }

  /*****************************  Add Master Data Method *************************/
  public addMasterData(masterDetails: any, scopeData : string, scopeId: number): Observable<any> {
    const masterDataMap = new MasterDataMapping(masterDetails, scopeData, scopeId);
    const body = JSON.stringify(masterDataMap);
    return this.httpClient.post<any>(environment.Master_URL  + '/addMasterData', body, httpOptions);
  }

  /*****************************  Get all Group name list for typeahed *************************/
  public getAllGroupNameList() {
    return this.httpClient.get<any>(environment.Master_URL  + '/getAllGroupNameList');
  } 

  /*****************************  Get all Group Value list for typeahed *************************/
  public getAllGroupValueList() {
    return this.httpClient.get<any>(environment.Master_URL  + '/getAllGroupValueList');
  } 

  /*****************************  Get Master Data Details *************************/
  public getMasterDataTableDetails(order: string, page : number, pageSize : number): Observable<any> {
    let gridValue = '{"draw":1,"columns":[{"data":"check","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"dataContainerName","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"dataContainerDescription","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"createdName","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"modifiedName","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"createdDate","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}}],"order":[{"column":0,"dir":"asc"}],"start":0,"length":4,"search":{"value":"","regex":false},"transactionId":7}';
    const body = JSON.stringify({
      "page": page,
      "size" : pageSize,
      "direction": order,
      "columnName" : "groupName",
      observe: 'response'
    });
    return this.httpClient.post<any>(environment.Master_URL  + '/getAllMasterDataInfo', body, httpOptions);
  }



  public pushFileToStorage(file: File, fileType : string, type : string): Observable<any> {
    let formdata: FormData = new FormData();
    formdata.append('file', file);
    formdata.append('file_type', fileType);
    formdata.append('type', type);
    return this.httpClient.post<any>(environment.Master_URL  + '/masterCsvXlsxUploadFile', formdata);
  }  

public checkDuplicateMasterDataGroupName(name): Observable<any> {
    const object = {
      "groupName":name
    }
    return this.httpClient.post<any>(environment.Master_URL  + '/checkDuplicateMasterDataGroupName', object, httpOptions);
  }

}//end of the class
 