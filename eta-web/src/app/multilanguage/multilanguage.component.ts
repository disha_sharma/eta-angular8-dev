import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Http, Headers, RequestOptions } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { ServiceService } from './../eta-services/service.service'
import { templateJitUrl } from '@angular/compiler';

@Component({
  selector: 'app-multilanguage',
  templateUrl: './multilanguage.component.html',
  styleUrls: ['./multilanguage.component.css'],
  providers: [ServiceService]
})
export class MultilanguageComponent implements OnInit {
  dropdownList = [];
  selectedItems = [];
  dropdownSettings = {};
  orgIdOnChange;
  selectedData=[];
  @Input() myorgId: number;
  constructor(private _formBuilder: FormBuilder, private http: HttpClient, private newService: ServiceService) {

   }
   languagelist :any[] ;
   _orgId: any[];
   showOrgValueForEditOrgDetails(change){
   this.orgIdOnChange = change;
}
/**********************************************Fetch All Language List******************************************************************************************************** */
  ngOnInit() {
    
    this.getLanguageByOrgId(this.myorgId)
    this.newService.getAllLanguageName()
    .subscribe(data => {
    data.forEach(element => {
      let id =element.languageID;
      let language =element.language;
      let languageObj:any ={};

      languageObj.id = id;
      languageObj.itemName =language;
      this.dropdownList.push(languageObj);
    });

   });

   this.selectedItems =this.selectedData;

   this.dropdownSettings = { 
    singleSelection: false, 
    text:"Select Languages",
    selectAllText:'Select All',
    unSelectAllText:'UnSelect All',
    enableSearchFilter: true,
    classes:"myclass custom-class"
  };  
  
  
}

/*******************************************************************Edit Language Details**********************************************************/
getLanguageByOrgId(orgId){
  
  this.newService.getLanguageByOrgId(orgId).subscribe(data => {
  data.forEach(element => {
        let id =element.languageId;
       let LanguageID =element.Language;
        let countryLanguageObj:any ={};  
        countryLanguageObj.id = id;
        countryLanguageObj.itemName =LanguageID;       
        this.selectedData = countryLanguageObj;
        this.selectedItems.push(countryLanguageObj);       
      });

  });

}
/***********************************************************Save Language Location*******************************************************************************/
onSubmitLanguage(){ 
  let language =[];
  this.selectedItems.forEach(element => {
    let obj:any ={};
    obj['languageID'] = element.id;
    language.push(obj);
  });

 let orgLanguageObj: Object = {};
  orgLanguageObj["orgId"] =this.myorgId;
  orgLanguageObj["language"]=language;  
  this.newService.postOrgLangWrapperLocation(orgLanguageObj)

    }

  }


