import { Component, OnInit, Inject } from '@angular/core';
import { EmbedVideoService } from 'ngx-embed-video';
import { MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-video-player-component',
  templateUrl: './video-player-component.component.html',
  styleUrls: ['./video-player-component.component.css']
})
export class VideoPlayerComponentComponent implements OnInit {
  videoEmbed:any;
  youtubeEmbedID:any;

  constructor(private embedService: EmbedVideoService,  @Inject(MAT_DIALOG_DATA) popupdata){

    let videoID = popupdata.videoID
    this.youtubeEmbedID = popupdata.youtubeEmbedID
    let videoEmbedID =popupdata.videoEmbedID

if(videoID === null || videoID === undefined || videoID === ''){
  if(videoEmbedID.mediaType.toLowerCase() === "youtube"){
    this.videoEmbed=this.embedService.embed_youtube(videoEmbedID.mediaID, {
      attr: { width: 600, height: 350 }
    });
  }
  if(videoEmbedID.mediaType.toLowerCase() === "vimeo"){
    this.videoEmbed=this.embedService.embed_vimeo(videoEmbedID.mediaID, {
      attr: { width: 600, height: 350 }
    });
  }
}

if(videoEmbedID === null || videoEmbedID === undefined || videoEmbedID === ''){
    this.videoEmbed=this.embedService.embed_vimeo(videoID, {
      attr: { width: 600, height: 350 }
    });
  
  }}

  ngOnInit() {
  }

}
