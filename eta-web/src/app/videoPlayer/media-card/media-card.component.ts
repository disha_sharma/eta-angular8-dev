import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { EmbedVideoService } from 'ngx-embed-video';
import { TaskServiceService } from '../../dash-board/task-service.service';
import { MatDialogConfig, MatDialog } from '@angular/material';
import { VideoPlayerComponentComponent } from '../video-player-component/video-player-component.component';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-media-card',
  templateUrl: './media-card.component.html',
  styleUrls: ['./media-card.component.css']
})
export class MediaCardComponent implements OnInit {

  mediaID = [];
  mediaData: any;
  videoEmbedArray=[];
  channelName: any;
  constructor(private embedService: EmbedVideoService, private taskServices: TaskServiceService,public dialog: MatDialog,public router: Router, public activateRoute: ActivatedRoute) {
  this.activateRoute.params.subscribe(params => {
    this.channelName = params['channelName'];
   this.getMediaData();
  });
 
//   this.router.routeReuseStrategy.shouldReuseRoute = function(){
//     return false;
// }  
  }

  ngOnInit() {
  }

getMediaData(){
  this.taskServices.getMediaDetails().subscribe(res => {

this.createJson(res);
  })

}
createJson(response){
  let onlyIds = [];
  for(let k = 0; k < response.length; k++){
if(onlyIds.indexOf(response[k].taskInstanceID) != -1){
continue;
}
else {
onlyIds.push(response[k].taskInstanceID);
}
}
let parsedData = [];
 for(let i = 0; i < onlyIds.length; i++){
   let obj = {}
   for(let k = 0; k < response.length; k++){
   if(onlyIds[i] == response[k].taskInstanceID){
     if(response[k].dataElementInsatnceFieldName){
    let fieldName = response[k].dataElementInsatnceFieldName.replace(/\s/g,'').toLowerCase();
        if(fieldName == 'videoid'){
          obj['mediaID'] = response[k]['dataElementInstanceValue'];
        }
        else if (fieldName == 'provider'){
          obj['mediaType'] = response[k]['dataElementInstanceValue'];
        }
        else if(fieldName == 'description'){
           obj['mediaDescription'] = response[k]['dataElementInstanceValue'];
        }
        else{
           obj[fieldName] = response[k]['dataElementInstanceValue'];
        }
      	    
             obj['taskInstanceID'] = response[k]['taskInstanceID'];
   }
  }
   }
parsedData.push(obj);
 }

 let filterData;
 let customisedChannelName = 'Year'+ this.channelName;
   filterData = parsedData.filter(obj => obj.channel.replace(/\s/g,'') == customisedChannelName);
 console.log('filtered Data is', filterData);
 
 this.getMediaDetails(filterData);
}

  getMediaDetails(parsedData) {
    let jsonToBeUsed = {};
   // this.taskServices.getMediaDetails().subscribe(res => {
     let myJson = [{channelName: this.channelName,
    'channelData': parsedData}]
      this.mediaData = myJson;
  
      for (let i = 0; i < Object.keys(this.mediaData).length; i++) {
        jsonToBeUsed = {};
        for (let j = 0; j < this.mediaData[i].channelData.length; j++) {
          let item = { "mediaType": '', "mediaID": '' };
          item.mediaType = this.mediaData[i].channelData[j].mediaType;
          item.mediaID = this.mediaData[i].channelData[j].mediaID;
          jsonToBeUsed[j]=item;
        }
        this.mediaID[i] = jsonToBeUsed
      }
      this.embedVideo();
 //   })
  }

  embedVideo(){
       let videoEmbedArray =[]
       this.videoEmbedArray = [];
    for (let i = 0; i < this.mediaID.length; i++) {
      videoEmbedArray =[]
      for (let j = 0; j < Object.keys(this.mediaID[i]).length; j++) {

        if(this.mediaID[i][j].mediaType.toLowerCase() === "youtube"){
          videoEmbedArray.push(this.embedService.embed_youtube(this.mediaID[i][j].mediaID, {
            attr: { width: 120, height: 80 }
          }))
        }
        else if(this.mediaID[i][j].mediaType.toLowerCase() === "vimeo"){
          videoEmbedArray.push(this.embedService.embed_vimeo(this.mediaID[i][j].mediaID, {
            attr: { width: 120, height: 80 }
          }))
        }

      }
      this.videoEmbedArray.push(videoEmbedArray)
    }
  }

}
