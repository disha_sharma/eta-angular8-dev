import { Component, OnInit, Input, ViewChild, ElementRef, SimpleChanges, OnChanges } from '@angular/core';
import { MatDialogConfig, MatDialog } from '@angular/material';
import { VideoPlayerComponentComponent } from '../video-player-component/video-player-component.component';

@Component({
  selector: 'app-media-channel',
  templateUrl: './media-channel.component.html',
  styleUrls: ['./media-channel.component.css']
})
export class MediaChannelComponent implements OnInit, OnChanges {
  @Input() data;
  @Input() videoEmbedArray;
  @Input() index;
  @Input() mediaID;
  likedVideos = [];
  constructor(public dialog: MatDialog) { }
  ngOnChanges(changes: SimpleChanges) {
    for (let propName in changes) {
   // only run when property "data" changed
   if (propName === 'data') {
        this.data = changes[propName].currentValue;  
        console.log('this.data', this.data);
        
   }
   else  if (propName === 'videoEmbedArray') {
    this.videoEmbedArray = changes[propName].currentValue;  
    console.log('videoEmbed Array', this.videoEmbedArray);
    
}
else  if (propName === 'index') {
  this.index = changes[propName].currentValue;  
}
else  if (propName === 'mediaID') {
  this.mediaID = changes[propName].currentValue;  
}
  }
}

  ngOnInit() {
   setTimeout(() => {
    this.toggleScrollButtonsForCards();
    /*for toggle like, unline icon on media card */
    for(let i = 0; this.data.channelData.length > i; i++ ){
        this.likedVideos.push({'liked': false});
    }
    /*for toggle like, unline icon on media card ends*/
   }, 2);
  }
  cardsScrollForwardButtonFlag: boolean;
  cardsScrollBackButtonFlag: boolean;
  @ViewChild('cardsContainer', { read: ElementRef, static: true }) public cardsContainer: ElementRef<any>;
  @ViewChild('cardsScrollableContent', { read: ElementRef, static: true }) public cardsScrollableContent: ElementRef<any>;

   onWindowResized(event) {
     this.toggleScrollButtonsForCards();
   }
   toggleScrollButtonsForCards(){
     if(this.cardsScrollableContent.nativeElement.clientWidth > this.cardsContainer.nativeElement.clientWidth){
  this.cardsScrollForwardButtonFlag = true;
     }
     else{
       this.cardsScrollForwardButtonFlag = false;
      this.cardsScrollBackButtonFlag = false;
     }
   }
   cardsScrollBackFunction(){
     this.cardsContainer.nativeElement.scrollTo({ left: (this.cardsContainer.nativeElement.scrollLeft -300), behavior: 'smooth' });
     this.cardsScrollForwardButtonFlag = true;
     if(this.cardsContainer.nativeElement.scrollLeft < 301){
          this.cardsScrollBackButtonFlag = false;
       }
    }

    cardsScrollForwardButton(){
      this.cardsContainer.nativeElement.scrollWidth;
    this.cardsContainer.nativeElement.scrollTo({ left: (this.cardsContainer.nativeElement.scrollLeft + 300), behavior: 'smooth' });
     this.cardsScrollBackButtonFlag = true;
     if(this.cardsContainer.nativeElement.offsetWidth + this.cardsContainer.nativeElement.scrollLeft > (this.cardsContainer.nativeElement.scrollWidth - 301)){
                 this.cardsScrollForwardButtonFlag = false;
             }
   }

   toggleLikeUnlike(index){
     this.likedVideos[index].liked = !this.likedVideos[index].liked;
   }

   getIndex(j){

    const dialogConfig = new MatDialogConfig();
    let embedID:any;
        embedID= this.mediaID[this.index][j]

    dialogConfig.data = {
     videoEmbedID : embedID
    };
    dialogConfig.panelClass = 'video-player-mat-dialog-container-panel-class'


    const dialogRef = this.dialog.open(VideoPlayerComponentComponent, dialogConfig);

     dialogRef.afterClosed()
  }
}
