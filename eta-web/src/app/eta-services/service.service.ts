import { Location } from '@angular/common';

import { HttpClient, HttpRequest, HttpHeaders } from '@angular/common/http';
import { Injectable, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import 'rxjs/add/operator/map';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';

import { AddTaskflow } from './../form/core/add-taskflow/add-taskflow';



const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  }; 


@Injectable()
export class ServiceService implements OnInit {
  Core_URL = 'http://192.168.40.152:8080';
  User_URL = "http://192.168.40.152:8082";
  Email_URL = "http://192.168.40.152:8089";
  Master_URL = "http://192.168.40.152:8083";
  Policy_URL = "http://192.168.40.152:8084";
  User_Activation = "http://192.168.40.152:8082";
  Password_Reset_URL = "http://192.168.40.152:8082";
  Test_User_URL = "http://192.168.40.152:8083";
  adminRole = "admin";
  myRole;
  Role_URL = "http://192.168.40.152:8082";
  Org_Pass_URL = "http://192.168.40.152:8084";
  Ga_Url = "http://192.168.40.152:8085";
  Org_Cal_URL = "http://192.168.40.152:8081";
  Org_URL = "http://192.168.40.152:8081";
  Org_logo = "http://192.168.40.152:8081/image/";
  Auth_URL = "http://192.168.40.152:8887";

   

  DROP_DOWN = 4;
  MULTI_SELECT = 5;

  taskflowId : number;

 /*  Core_URL = 'http://192.168.40.121:8080';
  User_URL = "http://192.168.40.121:8082";
  Email_URL = "http://192.168.40.121:8089";
  Master_URL = "http://192.168.40.121:8083";
  Policy_URL = "http://192.168.40.121:8084";
  User_Activation = "http://192.168.40.121:8082";
  Password_Reset_URL = "http://192.168.40.121:8082";
  Test_User_URL = "http://192.168.40.121:8083";
  adminRole = "admin";
  myRole;
  Role_URL = "http://192.168.40.121:8082";
  Org_Pass_URL = "http://192.168.40.121:8084";
  Ga_Url = "http://192.168.40.121:8085";
  Org_Cal_URL = "http://192.168.40.121:8081";
  Org_URL = "http://192.168.40.121:8081";
  Org_logo = "http://192.168.40.121:8081/image/";  

  Core_URL = 'http://192.168.40.115:8080';
  Auth_URL = "http://192.168.40.115:8887"; 
  User_URL = "http://192.168.40.115:8082";
  Email_URL = "http://192.168.40.115:8089";
  Master_URL = "http://192.168.40.115:8083";
  Policy_URL = "http://192.168.40.115:8084";
  User_Activation = "http://192.168.40.115:8082";
  Password_Reset_URL = "http://192.168.40.115:8082";
  Test_User_URL = "http://192.168.40.115:8083";  
  Role_URL = "http://192.168.40.115:8082";
  Org_Pass_URL = "http://192.168.40.115:8084";
  Ga_Url = "http://192.168.40.115:8085";
  Org_Cal_URL = "http://192.168.40.115:8081";
  Org_URL = "http://192.168.40.115:8081";
  Org_logo = "http://192.168.40.115:8081/image/";


  
  adminRole = "admin";
  myRole;

  /**Ravi machine config */
/*   Core_URL = 'http://192.168.40.150:8080';
  User_URL = "http://192.168.40.150:8082";
  Email_URL = "http://192.168.40.150:8089";
  Master_URL = "http://192.168.40.150:8083";
  Policy_URL = "http://192.168.40.150:8084";
  User_Activation = "http://192.168.40.150:8082";
  Password_Reset_URL = "http://192.168.40.150:8082";
  Test_User_URL = "http://192.168.40.150:8083";
  adminRole = "admin";
  myRole;
  Role_URL = "http://192.168.40.150:8082";
  Org_Pass_URL = "http://192.168.40.150:8084";
  Ga_Url = "http://192.168.40.150:8085";
  Org_Cal_URL = "http://192.168.40.150:8081";
  Org_URL = "http://192.168.40.150:8081";
  Org_logo = "http://192.168.40.150:8081/image/";  */


  public shown: any;

  
  
  //private isValidToken: boolean = false;


  constructor(private location: Location, private cookieService: CookieService, private r: Router, private httpClient: HttpClient) {

    this.myRole = cookieService.get('my_role');
    this.checkedSessionToken();
    
  }

  ngOnInit() {

  }

  /* toGetToken */
  public checkedSessionToken(): boolean {
    let istokenExist: boolean = false;
    if (sessionStorage.getItem("AuthToken") != null) {
      istokenExist = true;
    }
    return istokenExist;
  }
  /*  Header Menu hide and show */

  private loggedIn: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  get isLoggedIn() {
    return this.loggedIn.asObservable();
  }
  public refresh() {
    this.loggedIn.next(true);
  }
  public login() {
    console.log('welcome to login');
    
    this.loggedIn.next(true);
    this.r.navigate(['/eta-web']);

  }
  public loginTasK() {
    this.loggedIn.next(true);
    this.getUserActivationStatusByToken().subscribe(response => {
      if(response && response.userType){
        let userType = response.userType;
        if(userType =='ORG_Reviewer'){
          this.r.navigate(['/eta-web/mytask']);
        }
        else{
          this.r.navigate(['/eta-web']);
        }
      }
    });
    //this.r.navigate(['/mytask']);

  }
  public logout() {
    this.loggedIn.next(false);
    this.r.navigate(['/login']);
  }
  public profileView() {
    //this.loggedIn.next(true);
    this.r.navigate(['/eta-web/userprofilepage']);
  }
  // Notification
  public getAllEmailConfigMaster(): Observable<any> {
    return this.httpClient.get(this.Email_URL + '/getallmasterconfig');
  }

  public getLanguageByTempIdTempVidLangId(tempid, tempvid, labgid): Observable<any> {
    return this.httpClient.get(this.Email_URL + '/emailLanguageFindByTempIdTempVIdLangId/' + tempid + "/" + tempvid + "/" + labgid + "/");
  }

  public getSpdConfigById(): Observable<any> {
    return this.httpClient.get(this.Email_URL + '/getspdconfig');
  }
  public getTemplateByOrgId(orgId): Observable<any> {
    return this.httpClient.get(this.Email_URL + '/gettemplate/' + orgId);
  }
  public getHeaderLanguageByOrgId(orgId): Observable<any> {
    return this.httpClient.get(this.Email_URL + '/getHeaderLanguageByOrgId/' + orgId);
  }
  public getTemplateById() {
    return this.httpClient.get(this.Email_URL + '/gettemplate');
  }
  public getAllLanguage(): Observable<any> {
    return this.httpClient.get(this.Email_URL + '/alllanguage');
  }

  public postLanguage(userobj) {
    this.httpClient.post(this.Email_URL + '/addlanguage', userobj)
      .subscribe(data => {
        //console.log(data);
      });
  }
  public postTemplate(userobj): Observable<any> {
    return this.httpClient.post(this.Email_URL + '/addtemplate', userobj)

  }

  public postSpdConfig(userobj): Observable<any> {
    return this.httpClient.post(this.Email_URL + '/spdEmailConfigSave', userobj)
  }

  // Master
  public getAllLanguageName(): Observable<any> {
    return this.httpClient.get(this.Master_URL + '/getAllLanguage');
  }

  public getAllCountryByLanguageID(languageID): Observable<any> {
    return this.httpClient.get(this.Master_URL + '/getAllCountryByLanguageID/' + languageID);
  }
  public getAllSalutation(): Observable<any> {
    return this.httpClient.get(this.Master_URL + '/getAllSalutation');
  }

  public getAllCountry(): Observable<any> {
    return this.httpClient.get(this.Master_URL + '/getAllCountry');
  }

  // Role & Privilege

  public getAllRoleType(): Observable<any> {
    return this.httpClient.get(this.Role_URL + '/getAllRoleType');
  }

  public getAllRoleTypeDetails(): Observable<any> {
    return this.httpClient.get(this.User_URL + '/getAllRoleTypeDetails');
  }
  public setRoleDetails(role) {
    this.httpClient.post(this.User_URL + '/setRoleDetails', role).subscribe(data => {
      //console.log(data);
    });

  }
  public postPrivilege(privilegeObj): Observable<any> {
    return this.httpClient.post(this.Role_URL + '/setprivilege', privilegeObj)
  }

  public postRole(roleObj): Observable<any> {
    return this.httpClient.post(this.Role_URL + '/setrole', roleObj)

  }

  public getAllRoleOrgIdAndRoleTypeId(roleTypeId): Observable<any> {
    return this.httpClient.get(this.User_URL + '/getAllRoleOrgIdAndRoleTypeId/' + roleTypeId);
  }

  public setUserRoleWrapperDetails(userrole): Observable<any> {
    return this.httpClient.post(this.User_URL + '/setUserRoleWrapperDetails', userrole)
  }

  public getUserRoleByUserId(userId): Observable<any> {
    return this.httpClient.get(this.User_URL + '/getUserRoleByUserId/' + userId);
  }

  public getAllUserRole(): Observable<any> {
    return this.httpClient.get(this.User_URL + '/getAllUserRole');
  }

  public setUserRole(userrole) {
    this.httpClient.post(this.User_URL + '/setUserRole', userrole).subscribe(data => {
      //console.log(data);
    });
  }

  // Org
  public getAllOrgDays(): Observable<any> {
    return this.httpClient.get(this.Org_URL + '/getAllOrgDays');
  }

  public getLocationByOrgId(orgId): Observable<any> {
    return this.httpClient.get(this.Org_URL + '/getOrgLocByOrgId/' + orgId);
  }

  public getLanguageByOrgId(orgId): Observable<any> {
    return this.httpClient.get(this.Org_URL + '/getOrgLangByOrgId/' + orgId);
  }

  public getAllOrgDetails(): Observable<any> {
    return this.httpClient.get(this.Org_URL + '/getAllOrgDetails');
  }

  public getAllOrgDetailsMenuDropdown(): Observable<any> {
    return this.httpClient.get(this.Org_URL + '/getAllOrgDetailsForMenu');
  }

  public getOrgDetailsByOrgId(orgId): Observable<any> {
    return this.httpClient.get(this.Org_URL + '/getOrgByOrgId/' + orgId);
  }

  public postOrganization(orgObj): Observable<any> {
    return this.httpClient.post(this.Org_URL + '/setOrgDetails', orgObj);

  }

  public postOrgLocation(orgCountryObj) {
    this.httpClient.post(this.Org_URL + '/setOrgLocDetails', orgCountryObj)
      .subscribe(data => {
        ////console.log(data);
      });
  }

  /*public postOrgWrapperLocation(orgCountryObj) {
    this.httpClient.post(this.Org_URL + '/setOrgLocWrapperDetails', orgCountryObj)
      .subscribe(data => {
        //console.log(data);
      });
  }*/

  public postOrgWrapperLocation(orgCountryObj): Observable<any> {
    return this.httpClient.post(this.Org_URL + '/setOrgLocWrapperDetails', orgCountryObj);
  }


  /*public postOrgLangWrapperLocation(orgLanguageObj) {
    this.httpClient.post(this.Org_URL + '/setOrgLangWrapperDetails', orgLanguageObj)
      .subscribe(data => {
        //console.log("============44444==============");

        //console.log(data);
      });
  }*/

  public postOrgLangWrapperLocation(orgLanguageObj): Observable<any> {
    return this.httpClient.post(this.Org_URL + '/setOrgLangWrapperDetails', orgLanguageObj);
  }

  public postOrgTiming(orgfrm): Observable<any> {
    return this.httpClient.post(this.Org_URL + '/setOrgTiming', orgfrm)
  }

  // User 
  public getAllUser(): Observable<any> {
    return this.httpClient.get(this.User_URL + '/getAllUserData');
  }

  public getUserByUserId(userId): Observable<any> {
    return this.httpClient.get(this.User_URL + '/getUserByUserId/' + userId);
  }

  public getLoggedInUserByUserId(): Observable<any> {
    return this.httpClient.get(this.User_URL + '/getLoggedInUserByUserId');
  }

  public postUserDetails(userObj): Observable<any> {
    console.log(userObj);

    return this.httpClient.post(this.User_URL + '/setUserDetails', userObj)

  }


  //13 April Policy GET/POST service

  public getAllExpiryDays(): Observable<any> {
    return this.httpClient.get(this.Org_Pass_URL + '/getAllOrgMasterPolicy');
  }

  public setOrgPolicy(orgfrm): Observable<any> {
    return this.httpClient.post(this.Org_Pass_URL + '/setOrgPolicy', orgfrm)
  }

  public getAllPolicyByOrgId(orgId): Observable<any> {
    return this.httpClient.get(this.Policy_URL + '/getAllPolicy/' + orgId);
  }

  public getPolicyLanguageFindByTempIdTempVIdLangId(tempid, tempvid, langid): Observable<any> {
    return this.httpClient.get(this.Policy_URL + '/getPolicyLanguageFindByTempIdTempVIdLangId/' + tempid + "/" + tempvid + "/" + langid);

  }

  public postPolicyTranslate(userObj) {
    this.httpClient.post(this.Policy_URL + '/setPolicyLanguage', userObj)
      .subscribe(data => {
        //console.log(data);
      });

  }

  public getTermsLanguageFindByTempIdTempVIdLangId(tempid, tempvid, langid): Observable<any> {
    return this.httpClient.get(this.Policy_URL + '/getTermsLanguageFindByTempIdTempVIdLangId/' + tempid + "/" + tempvid + "/" + langid);

  }
  public postTermsTranslate(userObj) {
    this.httpClient.post(this.Policy_URL + '/setTermsLanguage', userObj)
      .subscribe(data => {
      //console.log(data);
      });

  }



  //16 April Password Reset Tool 

  public getUserDetailsPasswordReset(orgId): Observable<any> {
    return this.httpClient.get(this.Password_Reset_URL + '/userpasswordreset?userId=' + orgId);
  }

  public postNewPassword(userObj): Observable<any> {
    return this.httpClient.post(this.Password_Reset_URL + '/setNewPassword', userObj)
  }

  //Accept terms and Condition


  public getUserTerms(): Observable<any> {
    return this.httpClient.get(this.User_Activation + '/getAcceptTerms');
  }
  public setAcceptTermsFlag(): Observable<any> {
    return this.httpClient.get(this.User_Activation + '/setAcceptTerms');
  }

  // 17 April Accept Privacy Policy

  public getUserPolicy(): Observable<any> {
    return this.httpClient.get(this.User_Activation + '/getAcceptPolicy');
  }

  public setAcceptPolicyFlag(): Observable<any> {
    return this.httpClient.get(this.User_Activation + '/setAcceptPolicy');
  }


  //get User Details  by User Id
  public getUserDetailsByUserId(userId): Observable<any> {
    return this.httpClient.get(this.Policy_URL + '/getUserDetails/' + userId);
  }

  //set Policy  21 april

  public postPolicyTemplate(userObj): Observable<any> {
    return this.httpClient.post(this.Policy_URL + '/setPolicy', userObj)
  }

  public postTermsTemplate(userObj): Observable<any> {
    return this.httpClient.post(this.Policy_URL + '/setTermsAndCondition', userObj)


  }

  public getSpdConfigByOrgIdAndConfigId(configId, orgId): Observable<any> {
    return this.httpClient.get(this.Email_URL + '/spdDetailsByOrgIdAndConfigId/' + configId + '/' + orgId);
  }


  public getRoleByRoleId(roleId): Observable<any> {
    return this.httpClient.get(this.Role_URL + '/getRoleById/' + roleId);
  }

  //All GA 23 april
  public getAllGaConfiguration(): Observable<any> {
    return this.httpClient.get(this.Ga_Url + '/getAllGaConfiguration');
  }

  //Set Ga Data 23 april
  public setGaConfiguration(gaobj): Observable<any> {
    return this.httpClient.post(this.Ga_Url + '/setGaConfiguration', gaobj)

  }

  public getGaOuterColumn(): Observable<any> {
    // return this.httpClient.post(this.Ga_Url + '/getGaDashboardGrid', this.gridDataValue)
    return this.httpClient.post(this.Ga_Url + '/getGaDashboardGrid', this.gridDataValue)
  }

  public getGaInnerColumn(): Observable<any> {
    return this.httpClient.post(this.Ga_Url + '/getGaInnerGrid', this.gridDataValue)
  }

  //Org Password By Id 24 april
  /* public getOrgPasswordById(id): Observable<any> {
    return this.httpClient.get(this.Org_Pass_URL + '/getOrgPasswordPolicyById/' + id);
  } */

  public getOrgPasswordById(id): Observable<any> {
    return this.httpClient.get(this.Org_Pass_URL + '/getOrgPasswordPolicyById/' + id);
  }

  public getSpdOrgPasswordPolicyUserId(userId): Observable<any> {
    return this.httpClient.get(this.Org_Pass_URL + '/getSpdOrgPasswordPolicyUserId/' + userId);
  }


  public getAllOrgLoc(): Observable<any> {
    return this.httpClient.get(this.Org_URL + '/getAllOrgLoc');
  }

  public getOrgTimingById(id): Observable<any> {
    return this.httpClient.get(this.Org_URL + '/getOrgTimingById/' + id);
  }


  public getPrivilegeById(PId): Observable<any> {
    return this.httpClient.get(this.Role_URL + '/getPrivilegeById/' + PId);
  }


  //27 April new code
  public getEditPrivacyPolcy(tempid, tempvid): Observable<any> {
    return this.httpClient.get(this.Policy_URL + '/getPolicyByTempTempVid/' + tempid + "/" + tempvid);
  }
  public getEditTnc(tempid, tempvid): Observable<any> {
    return this.httpClient.get(this.Policy_URL + '/getTermsByTempTempVid/' + tempid + "/" + tempvid);
  }


  public editPolicy(editObj): Observable<any> {
    return this.httpClient.post(this.Policy_URL + '/editPolicy', editObj)
  }
  public EditTermsAndCondition(editObj): Observable<any> {
    return this.httpClient.post(this.Policy_URL + '/setEditTermsAndCondition', editObj)
  }


  pageRefresh() {
    location.reload();
  }


  private messageSource = new BehaviorSubject<any>("editFalse");
  currentMessage = this.messageSource.asObservable();

  setgridChangeMessage(message: string) {
    console.log(message);

    this.messageSource.next(message)
  }

  public refreshEditMessage() {
    this.messageSource.next("editFalse")
  }

  // 28 april edit email template 


  public getTemplateByTmpTmpvid(tempid, tempvid): Observable<any> {
    return this.httpClient.get(this.Email_URL + '/getTemplateByTmpTmpvid/' + tempid + "/" + tempvid);
  }


  public EditEmailTemplate(editObj): Observable<any> {
    return this.httpClient.post(this.Email_URL + '/setEditTemplate', editObj)
  }

  public MultiSelectPrivilege(editObj) {
    //alert("MultiSelectPrivilege");
    this.httpClient.post(this.Role_URL + '/setPrivilegeRole', editObj)
      .subscribe(data => {
        //console.log(data);
      });

  }
  public getPrivilegeRoleByRoleId(roleId): Observable<any> {
    return this.httpClient.get(this.Role_URL + '/getPrivilegeRoleByRoleId/' + roleId);
  }

  public imageUplaod(editObj: File, orgId, getOrgLogo: string) {
    let formdata: FormData = new FormData();
    formdata.append('file', editObj);
    //alert(getOrgLogo);
    return this.httpClient.post(this.Org_URL + '/postOrgLogo/' + orgId + "/" + getOrgLogo, formdata)


  }
  public getUserActivationStatusByToken(): Observable<any> {
    return this.httpClient.get(this.User_URL + '/getUserActivationStatusByToken');
  }

  public getlogoImage(orgId, imagename): Observable<any> {
    //console.log("youa are in ");

    //return this.httpClient.get(this.Org_URL + '/image/1/org.png' ,{responseType: "blob"});
    return this.httpClient.get(this.Org_URL + '/image/' + orgId + '/' + imagename, { responseType: "blob" });
  }
  public get(orgId, imagename): Observable<any> {
    //console.log("youa are in ");

    //return this.httpClient.get(this.Org_URL + '/image/1/org.png' ,{responseType: "blob"});
    return this.httpClient.get(this.Org_URL + '/getOrgLogoInfo/' + orgId + '/' + imagename, { responseType: "blob" });
  }

  public getOrgLogoInfoByDomain(domainName): Observable<any> {
    //alert("domainName "+domainName);
    return this.httpClient.get(this.User_URL + '/getOrgLogoInfoByDomain/' + domainName);
  }


  //image convert


  imageToShow: any;

  createImageFromBlob(image: Blob) {
    let reader = new FileReader();
    reader.addEventListener("load", () => {
      this.imageToShow = reader.result;
    }, false);

    if (image) {
      reader.readAsDataURL(image);
    }
  }

  public shownChnage(shownValue) {
    //console.log("you are in Show chnage " + shownValue);
    this.shown = shownValue;
    //console.log(this.shown);
  }
  /* go back to last page*/
  public gridRefresh() {
    this.location.back();
  }

  public getPolicyHeaderLanguageByOrgId(): Observable<any> {
    return this.httpClient.get(this.Policy_URL + '/getPolicyHeaderLanguageByOrgId');
  }

  public getRolePrivHeaderLanguageByOrgId(): Observable<any> {
    return this.httpClient.get(this.User_URL + '/getRolePrivHeaderLanguageByOrgId');
  }

  /* getUserType Method */
  public getUserType(): Observable<any> {
    return this.httpClient.get(this.User_URL + '/getUserType');
  }

  /* Get user Login Info */
  public getLoginInfo(): Observable<any> {
    return this.httpClient.get(this.User_URL + '/getLoginInfo');
  }
  /* Upload user profile image*/

  public profileImageUplaod(editObj: File, userId, getNewFileName) {
    //alert("getNewFileName :: "+getNewFileName);
    let formdata: FormData = new FormData();

    formdata.append('file', editObj);
    //console.log(formdata);
    return this.httpClient.post(this.User_URL + '/postProfileImage/' + userId + "/" + getNewFileName, formdata)


  }
  public getOrgProfileImage(orgId, imagename): Observable<any> {
    return this.httpClient.get(this.User_URL + '/image/' + orgId + '/' + imagename, { responseType: "blob" });
  }

  public getUserProfileImage(userId, imagename): Observable<any> {
    return this.httpClient.get(this.User_URL + '/userProfile/' + imagename, { responseType: "blob" });
  }

  /* Get user logout Info */
  public getLogoutInfo(): Observable<any> {
    return this.httpClient.get(this.User_URL + '/logoutInfo');
  }
  // public loggedIn: any = null;

  public getHeaderMenu(): Observable<any> {
    return this.httpClient.get(this.User_URL + '/headerMenuByOrgId')
  }

  private gridColumnData = new BehaviorSubject<any>("editFalse");
  currentMessag = this.gridColumnData.asObservable();

  setgridData(message: any) {
    //console.log("_______change Call Services_____");
    //console.log(message);

    this.gridColumnData.next(message)
  }



  gridDataValue = '{"draw":1,"columns":[{"data":"check","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"orgId","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"fullName","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"userType","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"emailAddress","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"createdName","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"createdDate","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"modifiedName","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"modifiedDate","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}}],"order":[{"column":0,"dir":"asc"}],"start":0,"length":4,"search":{"value":"","regex":false},"orgId":0}';
  /* Dymanic Grid Column Method */
  public getUserColumn(): Observable<any> {
    let gridValue = '{"draw":1,"columns":[{"data":"check","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"orgId","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"fullName","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"userType","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"emailAddress","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"createdName","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"createdDate","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"modifiedName","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"modifiedDate","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}}],"order":[{"column":0,"dir":"asc"}],"start":0,"length":4,"search":{"value":"","regex":false},"orgId":0}';
    return this.httpClient.post(this.User_URL + '/getAllUserDataGridByOrgId', gridValue)

  }
  public getUserInnerColumn(): Observable<any> {
    let gridValue = '{"draw":1,"columns":[{"data":"check","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"orgId","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"fullName","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"userType","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"emailAddress","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"createdName","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"createdDate","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"modifiedName","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"modifiedDate","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}}],"order":[{"column":0,"dir":"asc"}],"start":0,"length":4,"search":{"value":"","regex":false},"orgId":0}';
    return this.httpClient.post(this.User_URL + '/getAllUserInnerGrid', gridValue)

  }
  public getUserRoleColumn(): Observable<any> {
    let gridValue = '{"draw":1,"columns":[{"data":"check","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"orgId","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"fullName","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"userType","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"emailAddress","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"createdName","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"createdDate","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"modifiedName","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"modifiedDate","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}}],"order":[{"column":0,"dir":"asc"}],"start":0,"length":4,"search":{"value":"","regex":false},"orgId":0}';
    return this.httpClient.post(this.User_URL + '/getAllRoleGrid', gridValue)

  }
  public getUserRoleInnerColumn(): Observable<any> {
    let gridValue = '{"draw":1,"columns":[{"data":"check","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"orgId","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"fullName","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"userType","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"emailAddress","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"createdName","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"createdDate","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"modifiedName","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"modifiedDate","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}}],"order":[{"column":0,"dir":"asc"}],"start":0,"length":4,"search":{"value":"","regex":false},"orgId":0}';
    return this.httpClient.post(this.User_URL + '/getAllRoleInnerGrid', gridValue)

  }
  public getUserPrivilegeColumn(): Observable<any> {
    let gridValue = '{"draw":1,"columns":[{"data":"check","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"orgId","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"fullName","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"userType","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"emailAddress","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"createdName","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"createdDate","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"modifiedName","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"modifiedDate","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}}],"order":[{"column":0,"dir":"asc"}],"start":0,"length":4,"search":{"value":"","regex":false},"orgId":0}';
    return this.httpClient.post(this.User_URL + '/getAllPrivilegeGrid', gridValue)
  }
  public getUserPrivilegeInnerColumn(): Observable<any> {
    return this.httpClient.post(this.User_URL + '/getAllPrivilegeInnerGrid', this.gridDataValue)
  }
  public getOrgnizationOuterColumn(): Observable<any> {
    return this.httpClient.post(this.Org_URL + '/getAllOrgGridData', this.gridDataValue)
  }
  public getOrgnizationInnerColumn(): Observable<any> {
    return this.httpClient.post(this.Org_URL + '/getInnerGridByOrgId', this.gridDataValue)
  }
  public getOrgCalenderColumn(): Observable<any> {
    return this.httpClient.post(this.Org_URL + '/getWorkingCalenderDashboardGrid', this.gridDataValue)
  }
  public getOrgCalenderInnerColumn(): Observable<any> {
    return this.httpClient.post(this.Org_URL + '/getWorkingCalenderInnerGrid', this.gridDataValue)
  }
  public getOrgPassPolicyColumn(): Observable<any> {
    return this.httpClient.post(this.Org_Pass_URL + '/getAllOrgPassPolicyGridData', this.gridDataValue)
  }
  public getOrgPassPolicyInnerColumn(): Observable<any> {
    return this.httpClient.post(this.Org_Pass_URL + '/getOrgPassPolicyInnerGrid', this.gridDataValue)
  }
  public getOrgPrivacyPolicyColumn(): Observable<any> {
    return this.httpClient.post(this.Policy_URL + '/getAllPolicyGrid', this.gridDataValue)
  }
  public getOrgPrivacyPolicyInnerColumn(): Observable<any> {
    return this.httpClient.post(this.Policy_URL + '/getAllPolicyInnerGrid', this.gridDataValue)
  }
  public getOrgPrivacyTermsColumn(): Observable<any> {
    return this.httpClient.post(this.Policy_URL + '/getAllTermsGrid', this.gridDataValue)
  }
  public getOrgPrivacyTermsInnerColumn(): Observable<any> {
    return this.httpClient.post(this.Policy_URL + '/getAllTermsInnerGrid', this.gridDataValue)
  }

  public getEmailProviderMainColumn(): Observable<any> {
    return this.httpClient.post(this.Email_URL + '/spdEmailProvidersGrid', this.gridDataValue)
  }

  public getEmailProviderInnerColumn(): Observable<any> {
    return this.httpClient.post(this.Email_URL + '/spdEmailProvidersInnerGrid', this.gridDataValue)
  }

  public getEmailTemplateColumn(): Observable<any> {
    return this.httpClient.post(this.Email_URL + '/getAlltemplateGrid', this.gridDataValue)
  }
  /* public getEditEmailTemplateColumn(): Observable<any> {
    return this.httpClient.post(this.Email_URL + '/getAlltemplateGrid', this.gridDataValue)
  } */
  public getEmailTemplateInnerColumn(): Observable<any> {
    return this.httpClient.post(this.Email_URL + '/getTemplateInnerGrid', this.gridDataValue)
  }
  public getEmailSentLogColumn(): Observable<any> {
    return this.httpClient.post(this.Email_URL + '/sendEmailLogGrid', this.gridDataValue)
  }


  public getCoreFunctionColumn(): Observable<any> {
    let gridValue = '{"draw":1,"columns":[{"data":"check","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"functionName","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"functionDescription","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"createdName","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"modifiedName","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"createdDate","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}}],"order":[{"column":0,"dir":"asc"}],"start":0,"length":4,"search":{"value":"","regex":false},"transactionId":7}';
    return this.httpClient.post(this.Core_URL + '/getAllFunctionsByOrgIdGrid', gridValue)

  }

  public getCoreInnerFunctionColumn(): Observable<any> {
    let gridValue = '{"draw":1,"columns":[{"data":"check","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"functionName","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"functionDescription","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"createdName","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"createdDate","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"activeYN","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}}],"order":[{"column":0,"dir":"asc"}],"start":0,"length":4,"search":{"value":"","regex":false},"transactionId":7}';
    return this.httpClient.post(this.Core_URL + '/getAllInnerFunctionsByOrgIdGrid', gridValue)

  }

  public getCoreProcessColumn(): Observable<any> {
    let gridValue = '{"draw":1,"columns":[{"data":"check","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"processName","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"processDescription","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"createdName","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"modifiedName","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"createdDate","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"activeYN","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}}],"order":[{"column":0,"dir":"asc"}],"start":0,"length":4,"search":{"value":"","regex":false},"transactionId":7}';
    return this.httpClient.post(this.Core_URL + '/getAllProcessByOrgIdGrid', gridValue)

  }

  public getCoreInnerProcessColumn(): Observable<any> {
    let gridValue = '{"draw":1,"columns":[{"data":"check","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"processName","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"processDescription","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"createdName","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"modifiedName","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"createdDate","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}}],"order":[{"column":0,"dir":"asc"}],"start":0,"length":4,"search":{"value":"","regex":false},"transactionId":7}';
    return this.httpClient.post(this.Core_URL + '/getAllInnerProcessByOrgIdGrid', gridValue)

  }

  public getCoreTaskColumn(): Observable<any> {
    let gridValue = '{"draw":1,"columns":[{"data":"check","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"taskName","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"taskDescription","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"createdName","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"modifiedName","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"createdDate","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}}],"order":[{"column":0,"dir":"asc"}],"start":0,"length":4,"search":{"value":"","regex":false},"transactionId":7}';
    return this.httpClient.post(this.Core_URL + '/getAllTaskByOrgIdGrid', gridValue)

  }

  public getCoreInnerTaskColumn(): Observable<any> {
    let gridValue = '{"draw":1,"columns":[{"data":"check","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"taskName","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"taskDescription","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"createdName","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"modifiedName","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"createdDate","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}}],"order":[{"column":0,"dir":"asc"}],"start":0,"length":4,"search":{"value":"","regex":false},"transactionId":7}';
    return this.httpClient.post(this.Core_URL + '/getAllInnerTaskByOrgIdGrid', gridValue)

  }

  public getCoreFormColumn(): Observable<any> {
    let gridValue = '{"draw":1,"columns":[{"data":"formName","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"formDescription","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"createdName","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"createdDate","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"activeYN","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}}],"order":[{"column":0,"dir":"asc"}],"start":0,"length":4,"search":{"value":"","regex":false},"transactionId":7}';
    return this.httpClient.post(this.Core_URL + '/getAllFormsByOrgIdGrid', gridValue)

  }

  public getCoreInnerFormColumn(): Observable<any> {
    let gridValue = '{"draw":1,"columns":[{"data":"formName","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"formDescription","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"createdName","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"createdDate","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"activeYN","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}}],"order":[{"column":0,"dir":"asc"}],"start":0,"length":4,"search":{"value":"","regex":false},"transactionId":7}';
    return this.httpClient.post(this.Core_URL + '/getAllInnerFormsByOrgIdGrid', gridValue)

  }

  public getCoreDataContainerColumn(): Observable<any> {
    let gridValue = '{"draw":1,"columns":[{"data":"check","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"dataContainerName","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"dataContainerDescription","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"createdName","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"modifiedName","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"createdDate","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}}],"order":[{"column":0,"dir":"asc"}],"start":0,"length":4,"search":{"value":"","regex":false},"transactionId":7}';
    return this.httpClient.post(this.Core_URL + '/getAllDataContainerByOrgIdGrid', gridValue)

  } 
 
  public getCoreInnerDataContainerColumn(): Observable<any> {
    let gridValue = '{"draw":1,"columns":[{"data":"dataContainerName","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"dataContainerDescription","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"createdName","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"createdDate","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"activeYN","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}}],"order":[{"column":0,"dir":"asc"}],"start":0,"length":4,"search":{"value":"","regex":false},"transactionId":7}';
    return this.httpClient.post(this.Core_URL + '/getAllInnerDataContainerByOrgIdGrid', gridValue)

  }

  public getCoreDataObjectColumn(): Observable<any> {
    let gridValue = '{"draw":1,"columns":[{"data":"check","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"dataObjectName","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"dataObjectDescription","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"createdName","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"modifiedName","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"createdDate","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}}],"order":[{"column":0,"dir":"asc"}],"start":0,"length":4,"search":{"value":"","regex":false},"transactionId":7}';
    return this.httpClient.post(this.Core_URL + '/getAllDataObjectByOrgIdGrid', gridValue)

  }

  public getCoreInnerDataObjectColumn(): Observable<any> {
    let gridValue = '{"draw":1,"columns":[{"data":"dataObjectName","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"dataObjectDescription","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"createdName","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"createdDate","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"activeYN","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}}],"order":[{"column":0,"dir":"asc"}],"start":0,"length":4,"search":{"value":"","regex":false},"transactionId":7}';
    return this.httpClient.post(this.Core_URL + '/getAllInnerDataObjectByOrgIdGrid', gridValue)

  }


  public getCoreDataElementColumn(): Observable<any> {
    let gridValue = '{"draw":1,"columns":[{"data":"check","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"dataElementName","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"dataElementSourceName","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"displayDataElementBehaviorName","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"displayName","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"createdName","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"modifiedName","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"createdDate","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}}],"order":[{"column":0,"dir":"asc"}],"start":0,"length":4,"search":{"value":"","regex":false},"transactionId":7}';
    return this.httpClient.post(this.Core_URL + '/getAllDataElementByOrgIdGrid', gridValue)

  }

  public getCoreInnerDataElementColumn(): Observable<any> {
    let gridValue = '{"draw":1,"columns":[{"data":"check","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"dataElementName","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"createdName","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"modifiedName","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"createdDate","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"dataElementSource","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"dataElementType","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"displayDataElementBehavior","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}}],"order":[{"column":0,"dir":"asc"}],"start":0,"length":4,"search":{"value":"","regex":false},"transactionId":7}';
    return this.httpClient.post(this.Core_URL + '/getAllInnerDataElementByOrgIdGrid', gridValue)

  }

  public getMyTaskColumn(urlParameter): Observable<any> {
    let gridValue = '{"draw":1,"columns":[{"data":"check","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"taskInstanceID","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"processName","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"taskName","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"formName","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"createdName","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"modifideName","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"formName","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}}],"order":[{"column":0,"dir":"asc"}],"start":0,"length":4,"search":{"value":"","regex":false},"transactionId":7}';

    return this.httpClient.post(this.Core_URL + '/getMyTask/' + urlParameter, gridValue)

  }

  public getTaskMgtColumn(urlParameter): Observable<any> {
    let gridValue = '{"draw":1,"columns":[{"data":"check","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"taskInstanceID","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"processName","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"taskName","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"formName","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"createdName","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"modifideName","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"formName","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"claimButton","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}}],"order":[{"column":0,"dir":"asc"}],"start":0,"length":4,"search":{"value":"","regex":false},"transactionId":7}';
    return this.httpClient.post(this.Core_URL + '/getTaskMgt/' + urlParameter, gridValue)

  }

  public getCompletedTaskColumn(urlParameter): Observable<any> {
    let gridValue = '{"draw":1,"columns":[{"data":"check","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"taskInstanceID","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"processName","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"taskName","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"formName","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"createdName","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"modifideName","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"formName","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}}],"order":[{"column":0,"dir":"asc"}],"start":0,"length":4,"search":{"value":"","regex":false},"transactionId":7}';
    return this.httpClient.post(this.Core_URL + '/getCompletedTask/' + urlParameter, gridValue)

  }

  public getAllRoleDetails(): Observable<any> {
    return this.httpClient.get(this.User_URL + '/getAllRoleDetails');
  }

  /* Upload function profile image*/
  public functionProfileImageUplaod(editObj: File, functionId, getFunctionFileName) {
    let formdata: FormData = new FormData();
    formdata.append('file', editObj);
    return this.httpClient.post(this.Core_URL + '/postFunctionProfileImage/' + functionId + "/" + getFunctionFileName, formdata)
  }

  /* Upload Process profile image*/
  public processProfileImageUplaod(editObj: File, processId, getProcessFileName) {
    let formdata: FormData = new FormData();
    formdata.append('file', editObj);
    return this.httpClient.post(this.Core_URL + '/postProcessProfileImage/' + processId + "/" + getProcessFileName, formdata)
  }

  public getFunctionImage(functionId, imagename): Observable<any> {
    return this.httpClient.get(this.User_URL + '/imagefile/' + functionId + '/' + imagename, { responseType: "blob" });
  }

  public getProcessImage(processId, imagename): Observable<any> {
    return this.httpClient.get(this.User_URL + '/imagefileProcess/' + processId + '/' + imagename, { responseType: "blob" });
  }

  public postPrivilegeLanguage(obj) {
    this.httpClient.post(this.User_URL + '/setPrivilegeLanguage', obj)
      .subscribe(data => {
        console.log(data);
        //console.log(data);
      });
  }
  public postRoleLanguage(obj) {
    this.httpClient.post(this.User_URL + '/setRoleLanguage', obj)
      .subscribe(data => {
        //console.log(data);
      });
  }
  public getPrivilegeLanguageGrid(): Observable<any> {
    return this.httpClient.get(this.User_URL + '/getPrivilegeLanguageGrid');
  }



  getPrivilegeCheck(privilege, privilegeVersionId, languageId): Observable<any> {

    return this.httpClient.get(this.User_URL + '/getPrivilegeCheck/' + privilege + '/' + privilegeVersionId + '/' + languageId);


  }

  public clearShareObj() {
    this.setgridChangeMessage(null);
  }

  public dynamicInlineEditSave(fullObject, editValue, editColumnName) {
    let dummyObject: any = {};

    if (fullObject.hasOwnProperty('userName')) { // true
      dummyObject['tableName'] = "_user";
      dummyObject['columnName'] = editColumnName;
      dummyObject['columnValue'] = editValue;
      dummyObject['primaryKey'] = "and userId = " + fullObject.userId;
      dummyObject['orgId'] = fullObject.orgId;
    }
    if (fullObject.hasOwnProperty('roleName')) { // true
      dummyObject['tableName'] = "_rolegelanguage";
      dummyObject['columnName'] = editColumnName;
      dummyObject['columnValue'] = editValue;
      dummyObject['primaryKey'] = "and roleId = " + fullObject.roleId;
      dummyObject['orgId'] = fullObject.orgId;
      console.log(dummyObject);
    }
    if (fullObject.hasOwnProperty('privilegeName')) { // true
      dummyObject['tableName'] = "_privilegelanguage";
      dummyObject['columnName'] = editColumnName;
      dummyObject['columnValue'] = editValue;
      dummyObject['primaryKey'] = " and privilegeId = " + fullObject.privilegeID + " and privilegeVersionId =" + fullObject.privilegeVersionId + " and languageID =" + "'" + fullObject.languageId + "'";
      dummyObject['orgId'] = fullObject.orgId;
    }
    if (fullObject.hasOwnProperty('orgDomain')) { // true
      console.log("org Details called");
      dummyObject['tableName'] = "_orgdetails";
      dummyObject['columnName'] = editColumnName;
      dummyObject['columnValue'] = editValue;
      dummyObject['primaryKey'] = "";
      dummyObject['orgId'] = fullObject.orgId;
    }
    if (fullObject.hasOwnProperty('smtpUserName')) { // true
      console.log("org Details called");
      dummyObject['tableName'] = "_emailconfiguration";
      dummyObject['columnName'] = editColumnName;
      dummyObject['columnValue'] = editValue;
      dummyObject['primaryKey'] = "and configId = " + fullObject.configId;
      dummyObject['orgId'] = fullObject.orgId;
    }

    // return this.httpClient.get(this.User_URL + '/getInlineEditValue/' + tableName + '/' + editColumnName + '/' + editValue + '/' + primarykey + '/' + primarykeyValue);

    this.httpClient.post(this.User_URL + '/setDynamicInlineEditValue', dummyObject)
      .subscribe(data => {
        console.log(data);

      })

  }

  dataTableConfigurationByLanguageId(): Observable<any> {

    return this.httpClient.get(this.User_URL + '/dataTableConfigurationByLanguageId');


  }

  getRoleMainGridTest(): Observable<any> {
    return this.httpClient.get(this.User_URL + '/getRoleMainGridTest');
  }


  public getSpdFormJsonSchema(schemaName: string): Observable<any> {
    return this.httpClient.get('http://192.168.40.159:8080/getSpdFormJsonSchema/' + schemaName)
  }


  public getUserAngularGridData(): Observable<any> {
    return this.httpClient.get(this.User_URL + '/getUserAngularGrid/1/5/asc/salutation');
  }

  public getUserAngularInnerGridData(): Observable<any> {
    return this.httpClient.get(this.User_URL + '/getUserAngularInnnerGrid/1/5/asc/salutation');
  }

  public getStartYourFreeTrial(obj): Observable<any> {
    return this.httpClient.post(this.Org_URL + '/startfreetrialnow', obj)

  }

  public getTotalNumberOfUser(): Observable<any> {
    return this.httpClient.get(this.User_URL + '/getTotalNumberOfUser');
  }


  public getMasterAuthLicesingList(): Observable<any> {
    return this.httpClient.get(this.Auth_URL + '/allauthlicense');
  }
  public buyAuthLicense(obj): Observable<any> {
    return this.httpClient.post(this.Auth_URL + '/setlicense', obj)

  }
  public checkAuthLiceseInfo(orgId): Observable<any> {
    return this.httpClient.get(this.Auth_URL + '/license/' + orgId);
  }

  //All State List
  public getAllState(): Observable<any> {
    return this.httpClient.get(this.Master_URL + '/getAllState');
  }

  //All Currency List
  public getAllCurrency(): Observable<any> {
    return this.httpClient.get(this.Master_URL + '/getAllCurrency');
  }

  //All TimeZone List
  public getAllTimeZone(): Observable<any> {
    return this.httpClient.get(this.Master_URL + '/getAllTimeZone');
  }




  /**
   *Master Service For Country States
   *
   * @memberof ServiceService
   */
  public getStateByCountry(countryId): Observable<any> {
    return this.httpClient.get(this.Master_URL + '/getAllStateByCountryId/' + countryId);
  }

  public getCurrencyByCountry(countryId): Observable<any> {
    return this.httpClient.get(this.Master_URL + '/getCurrencyByCountryId/' + countryId);
  }
  public getTimeZoneByCountry(countryId): Observable<any> {
    return this.httpClient.get(this.Master_URL + '/getAllTimeZoneByCountryId/' + countryId);
  }
  public getMeasurementByIdentifier(identifierName): Observable<any> {
    return this.httpClient.get(this.Master_URL + '/getAllIdentifierNameByIdentifierName/' + identifierName);
  }
  public getAllDateFormatList(): Observable<any> {
    return this.httpClient.get(this.Master_URL + '/getAllDateFormat');
  }

  public setCountryDetails(obj): Observable<any> {
    return this.httpClient.post(this.Org_URL + '/setcountrydetails', obj)

  }

  public getOrgAngularGridData(): Observable<any> {
    return this.httpClient.get(this.Org_URL + '/getOrgAngularGrid/1/5/asc/orgName');
  }
  public getOrgAngularInnnerGridData(): Observable<any> {
    return this.httpClient.get(this.Org_URL + '/getOrgAngularInnnerGrid/1/5/asc/orgName');
  }
  public getOrgPasswordPolicyAngularGridData(): Observable<any> {
    return this.httpClient.get(this.Org_Pass_URL + '/getOrgPasswordPolicyAngularGrid/1/5/asc/passConfigId');
  }
  public getOrgPasswordPolicyAngularInnerGridData(): Observable<any> {
    return this.httpClient.get(this.Org_Pass_URL + '/getOrgPasswordPolicyAngularInnnerGrid/1/5/asc/passConfigId');
  }

  public getPrivacyPolicyAngularGridData(): Observable<any> {
    return this.httpClient.get(this.Policy_URL + '/getPrivacyPolicyAngularGrid/1/5/asc/policyTemplateId');
  }
  public getPrivacyPolicyAngularInnerGridData(): Observable<any> {
    return this.httpClient.get(this.Policy_URL + '/getPrivacyPolicyAngularInnnerGrid/1/5/asc/policyTemplateId');
  }
  public getTermsConditionAngularGridData(): Observable<any> {
    return this.httpClient.get(this.Policy_URL + '/getTermsConditionAngularGrid/1/5/asc/termsTemplateId');
  }
  public getTermsConditionAngularInnnerGridData(): Observable<any> {
    return this.httpClient.get(this.Policy_URL + '/getTermsConditionAngularInnnerGrid/1/5/asc/termsTemplateId');
  }
  public getWorkingCalenderAngularGridData(): Observable<any> {
    return this.httpClient.get(this.Org_Cal_URL + '/getWorkingCalenderAngularGrid/1/5/asc/orgId');
  }
  public getWorkingCalenderAngularInnerGridData(): Observable<any> {
    return this.httpClient.get(this.Org_Cal_URL + '/getWorkingCalenderAngularInnnerGrid/1/5/asc/orgId');
  }
  public getUserRoleAngularGridData(): Observable<any> {
    return this.httpClient.get(this.Role_URL + '/getUserRoleAngularGrid/1/5/asc/roleId');
  }
  public getUserRoleAngularInnerGridData(): Observable<any> {
    return this.httpClient.get(this.Role_URL + '/getUserRoleAngularInnnerGrid/1/5/asc/roleId');
  }
  public getUserPrivilegeAngularGridData(): Observable<any> {
    return this.httpClient.get(this.Role_URL + '/getUserPrivilegeAngularGrid/1/5/asc/privilege');
  }
  public getUserPrivilegeAngularInnerGridData(): Observable<any> {
    return this.httpClient.get(this.Role_URL + '/getUserPrivilegeAngularInnnerGrid/1/5/asc/privilege');
  }
  public getEmailServiceProvidersAngularGridData(): Observable<any> {
    return this.httpClient.get(this.Email_URL + '/getEmailProvidersAngularGrid/1/5/asc/smtpUserName');
  }
  public getEmailServiceProvidersAngularInnerGridData(): Observable<any> {
    return this.httpClient.get(this.Email_URL + '/getEmailProvidersAngularInnnerGrid/1/5/asc/smtpUserName');
  }
  public getEmailTemplateAngularGridData(): Observable<any> {
    return this.httpClient.get(this.Email_URL + '/getEmailTemplateAngularGrid/1/5/asc/emailTemplateId');
  }
  public getEmailTemplateAngularInnerGridData(): Observable<any> {
    return this.httpClient.get(this.Email_URL + '/getEmailTemplateAngularInnnerGrid/1/5/asc/emailTemplateId');
  }

  public getAllSpdMstList(): Observable<any> {
    return this.httpClient.get(this.Core_URL + '/getAllSpdMstListByOrgId')
  }

  public setCustomTable(obj): Observable<any> {
    return this.httpClient.post(this.Core_URL + '/setCustomSpdDetails', obj)
  }

  /* Upload user profile image*/

  public taskFileUpload(editObj: File, id, getNewFileName) {
    let formdata: FormData = new FormData();
    formdata.append('file', editObj);
    return this.httpClient.post(this.Core_URL + '/submitTask/' + id + "/" + getNewFileName, formdata)




  }
  /*  public taskFileUpload(editObj) {
     let formdata: FormData = new FormData();
     formdata.append('file', editObj);
     return this.httpClient.post('https://192.168.40.150/upload ', editObj)
   } */

  /***   */
  private splitterTaskId = new BehaviorSubject<any>("taskId");
  createdTask = this.splitterTaskId.asObservable();

  createdSplitterTask(message: string) {
    console.log(message);

    this.splitterTaskId.next(message)
  }

  public getTaskByTaskId(taskId): Observable<any> {
    return this.httpClient.get(this.Core_URL + '/getTaskByTaskId/' + taskId)
  }

  public checkEmailAvailability(email): Observable<any> {
    return this.httpClient.get(this.User_URL + '/checkEmailAvailability/' + email)
  }

  public getAllTemplateByOrgId(): Observable<any> {
    return this.httpClient.get(this.Email_URL + '/getAllTemplateByOrgId')
  }

  /** Get all orgType */
  public getAllOrgType(): Observable<any> {
    return this.httpClient.get(this.Org_URL + '/getAllOrgType');
  }
  
  /** Get All TaskFlow  */
  public getAllTaskFlow(): Observable<any>{
    return this.httpClient.get(this.Core_URL + '/getAllTaskFlow');
  }
  public checkUserNameAvailability(username): Observable<any> {
    return this.httpClient.get(this.User_URL + '/checkUserNameAvailability/' + username)
  }
  public checkUserNameAvailabilityFreeTrial(username): Observable<any> {
    return this.httpClient.get(this.User_URL + '/checkUserNameAvailabilityFreeTrial/' + username)
  }

  public getUserRoleUpdateData(status, roleId): Observable<any> {
    return this.httpClient.get(this.Role_URL + '/setUserRoleStatus/' + status + '/' + roleId);
  }


  /* Sprint 1 Dev 15 and 16 */

  public getAllTaskType(): Observable<any> {
    return this.httpClient.get(this.Core_URL + '/getAllTaskType');
  }

  public getAllDataContainer(): Observable<any> {
    return this.httpClient.get(this.Core_URL + '/getAllDataContainer')
 }
 
  public getAllProcessList(): Observable<any> {
    return this.httpClient.get(this.Core_URL + '/getAllProcessList')
  }

  public pushFileToStorage(file: File): Observable<any> {
    let formdata: FormData = new FormData();
    formdata.append('file', file);
    return this.httpClient.post(this.Core_URL + '/uploadFile', formdata);
    } 


  /* getAllProcessByFunctionId */
  public getAllProcessByFunctionId(functionId): Observable<any> {
    return this.httpClient.get(this.Core_URL + '/getAllProcessByFunctionId/' + functionId)
  }

  /** Get All TaskFlow via ProcessID */
  public getAllTaskFlowByProcessId(processId): Observable<any>{
    return this.httpClient.get(this.Core_URL + '/getAllTaskFlowByProcessId/' + processId);
  }

  /** Get All TaskFlowType List */
  public getAllSpdTaskFlowType(): Observable<any>{
    return this.httpClient.get(this.Core_URL + '/getAllSpdTaskFlowType');
  }

  /** Save  TaskFlow */
  public saveTaskFlow(addTaskFlow : AddTaskflow):Observable<any>{
    return this.httpClient.post(this.Core_URL + '/saveTaskFlow', addTaskFlow,  httpOptions);

  }
  /** All Process is Not working , it fetches a single record */
  public getAllProcess(): Observable<any>{
    return this.httpClient.get(this.Core_URL + '/getAllProcess');
  }
  /** Added By Kavita */
  public getAllTaskByTaskFlowId(taskFlowId) : Observable<any> {
    return this.httpClient.get(this.Core_URL + '/getAllTaskByTaskFlowId/' + taskFlowId);
  }

  public saveTask(taskObj : any): Observable<any> {
    // this.isSpinnerShow = true;
    return this.httpClient.post(this.Core_URL + '/saveTask/', taskObj);
  }

  

   /* getAllProcessByProcessId */
   public getAllProcessByProcessId(processId): Observable<any> {
    return this.httpClient.get(this.Core_URL + '/getAllProcessByProcessId/' + processId);
  }

/*get all dynamic css for Branding (Called from app.component and branding components) */
public getDefaultCss(dashBoardId) : Observable<any> {
  return this.httpClient.get(this.Org_URL + '/resetTheme/' + dashBoardId);
}

public GetDynamicCss() : Observable<any> {
  //console.log('=====>>>>>',this.httpClient.get(this.Org_URL + '/getCustomizedTheme/'));
  
  return this.httpClient.get(this.Org_URL + '/getCustomizedTheme/');
} 
public updateDynamicCss(cssObj : any): Observable <any> {
  return this.httpClient.post(this.Org_URL + '/setDashBoard/', cssObj);
}

public uploadImageForBranding(file: File, fileType: string, updateFor: string): Observable<any> {
  let formdata: FormData = new FormData();
 
  formdata.append('file', file);
    formdata.append('file_type', fileType);
    formdata.append('type', updateFor);
  return this.httpClient.post(this.Org_URL + '/uploadBrandingImg', formdata);
  } 

}
