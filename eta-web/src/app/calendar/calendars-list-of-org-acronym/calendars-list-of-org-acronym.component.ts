import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { SelectionModel } from '@angular/cdk/collections';
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { ServiceService } from '../../eta-services/service.service';


@Component({
  selector: 'app-calendars-list-of-org-acronym',
  templateUrl: './calendars-list-of-org-acronym.component.html',
  styleUrls: ['./calendars-list-of-org-acronym.component.css']
})
export class CalendarsListOfOrgAcronymComponent implements OnInit {

  displayedColumns: string[] = ['checkboxColumn', 'location', 'locale', 'timezone', 'fiscalStart', 'adminStart', 'holidays'];
   dataSource  = new MatTableDataSource<PeriodicElement>();
  rowsSelection = new SelectionModel(true, []);
 
  @ViewChild(MatPaginator,{static: true}) paginator: MatPaginator;

  @ViewChild(MatSort,{static: true}) sort: MatSort;
  showSuccess:boolean=false

  isExpansionDetailRow = (index, row) => row.hasOwnProperty('detailRow');

  // ELEMENT_DATA: PeriodicElement[];

constructor( private http: HttpClient, private router: Router,private activatedRoute: ActivatedRoute ,private service: ServiceService){}

  ngOnInit() {
    this.showSuccess = this.activatedRoute.snapshot.queryParams['showSuccess'];
    this.router.navigate(['eta-web/calendarsListOfOrgAcronym'], { queryParams : {}}) ;
    this.getOrgCalender();
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }
    /** Whether the number of selected elements matches the total number of rows. */
    isAllSelected() {
      const numSelected = this.rowsSelection.selected.length;
      const numRows = this.dataSource.data.length;
      return numSelected === numRows;
    }
  
    /** Selects all rows if they are not all selected; otherwise clear rowsSelection. */
    masterToggle() {
      this.isAllSelected() ?
          this.rowsSelection.clear() :
          this.dataSource.data.forEach(row => this.rowsSelection.select(row));
    }


    

getOrgCalender(){

  let url=this.service.Org_Cal_URL
  let calendarsList
  this.http.get(url+"/getAllOrgCalendarByOrgId").subscribe(data=>{
   calendarsList =data;
    this.dataSource = new MatTableDataSource<PeriodicElement>(calendarsList)
    console.log('calendarsList',calendarsList);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  })
}

addNewCalendar(){
   this.router.navigate(['eta-web/addNewCalendarToOrgAcronym']);
}

removeSuccess(){
  this.showSuccess=false;
}

editCalender(calendarId){
  this.router.navigate(['eta-web/editCalendarToOrgAcronym'],{queryParams:{"calendarId":calendarId}});

}

   
}

export interface PeriodicElement {
  location: string;
  locale: string;
  type: string;
  timezone: string;
  fiscalStart: string;
  adminStart: string;
  holidays: number
  
}

// const ELEMENT_DATA: PeriodicElement[] = [
  
//   {location: 'California', locale: 'System (Default)', type: 'system', timezone: 'UTC - 8.0', fiscalStart: 'January', adminStart: 'January', holidays: 5},
//   {location: 'Rajasthan', locale: 'South-east Asia', type: 'human', timezone: 'UTC - 8.0', fiscalStart: 'January', adminStart: 'January', holidays: 5},
//   {location: 'Rajasthan', locale: 'South-east Asia', type: 'human', timezone: 'UTC - 8.0', fiscalStart: 'January', adminStart: 'January', holidays: 5},
//   {location: 'Rajasthan', locale: 'South-east Asia', type: 'human', timezone: 'UTC - 8.0', fiscalStart: 'January', adminStart: 'January', holidays: 5},
//   {location: 'Rajasthan', locale: 'South-east Asia', type: 'human', timezone: 'UTC - 8.0', fiscalStart: 'January', adminStart: 'January', holidays: 5},
//   {location: 'Rajasthan', locale: 'South-east Asia', type: 'human', timezone: 'UTC - 8.0', fiscalStart: 'January', adminStart: 'January', holidays: 5},
//   {location: 'Rajasthan', locale: 'South-east Asia', type: 'human', timezone: 'UTC - 8.0', fiscalStart: 'January', adminStart: 'January', holidays: 5},
//   {location: 'Rajasthan', locale: 'South-east Asia', type: 'human', timezone: 'UTC - 8.0', fiscalStart: 'January', adminStart: 'January', holidays: 5},

 
// ];
