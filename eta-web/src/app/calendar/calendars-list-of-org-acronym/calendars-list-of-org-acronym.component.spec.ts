import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CalendarsListOfOrgAcronymComponent } from './calendars-list-of-org-acronym.component';

describe('CalendarsListOfOrgAcronymComponent', () => {
  let component: CalendarsListOfOrgAcronymComponent;
  let fixture: ComponentFixture<CalendarsListOfOrgAcronymComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CalendarsListOfOrgAcronymComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalendarsListOfOrgAcronymComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
