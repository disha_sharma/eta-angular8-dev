import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddNewCalendarToOrgAcronymComponent } from './add-new-calendar-to-org-acronym.component';

describe('AddNewCalendarToOrgAcronymComponent', () => {
  let component: AddNewCalendarToOrgAcronymComponent;
  let fixture: ComponentFixture<AddNewCalendarToOrgAcronymComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddNewCalendarToOrgAcronymComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddNewCalendarToOrgAcronymComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
