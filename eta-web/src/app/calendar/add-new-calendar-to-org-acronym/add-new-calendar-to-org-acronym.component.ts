import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroupDirective } from '@angular/forms';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ArrayType } from '@angular/compiler/src/output/output_ast';


import { SelectionModel } from '@angular/cdk/collections';
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { ServiceService } from '../../eta-services/service.service';
import { Observable } from 'rxjs/Observable';
@Component({
  selector: 'app-add-new-calendar-to-org-acronym',
  templateUrl: './add-new-calendar-to-org-acronym.component.html',
  styleUrls: ['./add-new-calendar-to-org-acronym.component.css']
})
export class AddNewCalendarToOrgAcronymComponent implements OnInit {



  form: FormGroup;
  showSuccess: boolean = false;
  id = 0;
  calendarDetailByID: any;
  deletedHolidayListId: any = [];
  deletedHourListId: any = [];
  deletedWeekListId: any = [];


  constructor(private fb: FormBuilder, private http: HttpClient, private router: Router, private activatedRoute: ActivatedRoute, private taskService: ServiceService) {

    this.id = this.activatedRoute.snapshot.queryParams['calendarId'];
    this.editForm()
  }
  ngOnInit() {


    this.form = this.newForm()

  }

  newForm() {
    return this.fb.group({
      locale: ['', Validators.required],
      location: ['', Validators.required],
      timezone: ['', Validators.required],
      fiscalStart: ['', Validators.required],
      administrativeStart: ['', Validators.required],
      weekList: this.fb.array([
        this.initworkWeek()
      ]),
      hourList: this.fb.array([
        this.initworkShift()
      ]),
      holidayList: this.fb.array([
        this.initholiday()
      ])
    });
  }
  initworkWeek() {
    return this.fb.group({
      workWeekCheckedStatus: [''],
      name: ['', Validators.required],
      workdays: ['', Validators.required],
      startDay: ['', Validators.required],
      workHours: ['', Validators.required],
      workWeekId: [0],
      calendarId: [this.id]

    });
  }
  //add data object 
  addworkWeek() {
    const control = <FormArray>this.form.controls['weekList'];
    control.push(this.initworkWeek());
    // this.form.controls['name']
  }

  //delete data object
  deleteworkWeek() {
    var i = this.form.controls['weekList'].value.length;
    while (i--) {
      if (this.form.controls['weekList'].value[i].workWeekCheckedStatus == true) {
        let deletedRowID = this.form.controls['weekList'].value[i].workWeekId
        //console.log('deletedRowID',deletedRowID);
        if (deletedRowID != 0) {
          this.deletedWeekListId.push(deletedRowID)
        }
        const control = <FormArray>this.form.controls['weekList'];
        control.removeAt(i);
      }
    }
    // if (this.deletedWeekListId.length > 0) {
    //   let url = this.taskService.Org_URL;
    //   let deletedListId: any = {};
    //   deletedListId.deletedWeekListId = this.deletedWeekListId
    //   this.http.post(url + "/deleteOrgCalendarWorkWeek", deletedListId).subscribe(res => {
    //     console.log(res);

    //   })
    //   console.log(deletedListId);

    // }
    //console.log(this.deletedWeekListId);

    this.selectionForworkWeek.clear();
    let lengthAfterDelete = this.form.controls['weekList'].value.length;
    if (!lengthAfterDelete) {
      this.addworkWeek();
    }
   // this.deletedWeekListId = [];

  }


  //data object  selection for delete
  selectionForworkWeek = new SelectionModel(true, []);

  // Whether the number of selected elements matches the total number of rows.
  isAllworkWeeksSelected() {
    let control = <FormArray>this.form.controls['weekList'];
    const numSelected = this.selectionForworkWeek.selected.length;
    const numRows = control.controls.length;
    return numSelected === numRows;
  }

  // Selects all rows if they are not all selected; otherwise clear selection. 
  masterToggleForworkWeeks() {
    let formControl = <FormArray>this.form.controls['weekList'];
    this.isAllworkWeeksSelected() ?
      this.uncheckAllworkWeeks() : this.checkAllworkWeeks();

  }
  uncheckAllworkWeeks() {
    this.selectionForworkWeek.clear();
    let formControl = <FormArray>this.form.controls['weekList'];
    let length = formControl.value.length;
    console.log('I got here', length);
    for (let i = 0; i < length; i++) {
      formControl.controls[i].patchValue({
        'workWeekCheckedStatus': false
      })
    }
  }
  checkAllworkWeeks() {
    let formControl = <FormArray>this.form.controls['weekList'];
    formControl.controls.forEach(row => this.selectionForworkWeek.select(row));

    let length = formControl.value.length;
    for (let i = 0; i < length; i++) {
      //formControl.controls[i].get('workWeekCheckedStatus').setValue(true);
      formControl.controls[i].patchValue({
        'workWeekCheckedStatus': true
      })
    }
  }


  // reactive form for workShift

  initworkShift() {
    return this.fb.group({

      workShiftCheckedStatus: [''],
      name: [''],
      startTime: ['', Validators.required],
      workHours: ['', Validators.required],
      breaks: ['', Validators.required],
      duration: ['', Validators.required],
      workHourId: [0],
      calendarId: [this.id]
    });
  }


  //add workShift
  addworkShift() {
    const control = <FormArray>this.form.controls['hourList'];
    control.push(this.initworkShift());
    // this.form.controls['name']
  }

  //delete workShift
  deleteworkShift() {
    var i = this.form.controls['hourList'].value.length;
    while (i--) {
      if (this.form.controls['hourList'].value[i].workShiftCheckedStatus == true) {
        let deletedRowID = this.form.controls['hourList'].value[i].workHourId
        //console.log('deletedRowID',deletedRowID);
        if (deletedRowID != 0) {
          this.deletedHourListId.push(deletedRowID)
        }
        const control = <FormArray>this.form.controls['hourList'];
        control.removeAt(i);
      }
    }
    // if (this.deletedHourListId.length > 0) {
    //   let url = this.taskService.Org_URL;
    //   let deletedListId: any = {};
    //   deletedListId.deletedHourListId = this.deletedHourListId
    //   this.http.post(url + "/deleteOrgCalendarWorkHour", deletedListId).subscribe(res => {
    //     console.log(res);

    //   })
    //   console.log(deletedListId);

    // }
    //console.log(this.deletedHourListId);

    this.selectionForworkShift.clear();
    let lengthAfterDelete = this.form.controls['hourList'].value.length;
    if (!lengthAfterDelete) {
      this.addworkShift();
    }
    //this.deletedHourListId = [];

  }


  //workShift  selection model
  selectionForworkShift = new SelectionModel(true, []);

  // Whether the number of selected elements matches the total number of rows.
  isAllworkShiftsSelected() {
    let control = <FormArray>this.form.controls['hourList'];
    const numSelected = this.selectionForworkShift.selected.length;
    const numRows = control.controls.length;
    return numSelected === numRows;
  }

  // Selects all rows if they are not all selected; otherwise clear selection. 
  masterToggleForworkShifts() {
    let formControl = <FormArray>this.form.controls['hourList'];
    this.isAllworkShiftsSelected() ?
      this.uncheckAllworkShifts() : this.checkAllworkShifts();

  }
  uncheckAllworkShifts() {
    this.selectionForworkShift.clear();
    let formControl = <FormArray>this.form.controls['hourList'];
    let length = formControl.value.length;
    console.log('I got here', length);
    for (let i = 0; i < length; i++) {
      formControl.controls[i].patchValue({
        'workShiftCheckedStatus': false
      })
    }
  }
  checkAllworkShifts() {
    let formControl = <FormArray>this.form.controls['hourList'];
    formControl.controls.forEach(row => this.selectionForworkShift.select(row));

    let length = formControl.value.length;
    for (let i = 0; i < length; i++) {
      //formControl.controls[i].get('workShiftCheckedStatus').setValue(true);
      formControl.controls[i].patchValue({
        'workShiftCheckedStatus': true
      })
    }
  }



  // reactive form for holiday

  initholiday() {
    return this.fb.group({
      holidayCheckedStatus: [''],
      name: [''],
      type: ['', Validators.required],
      occurence: ['', Validators.required],
      date: ['', Validators.required],
      day: ['', Validators.required],
      holidayId: [0],
      calendarId: [this.id]

    });
  }


  //add holiday
  addholiday() {
    const control = <FormArray>this.form.controls['holidayList'];
    control.push(this.initholiday());
    // this.form.controls['name']
  }

  //delete holiday
  deleteholiday() {
    var i = this.form.controls['holidayList'].value.length;
    while (i--) {
      if (this.form.controls['holidayList'].value[i].holidayCheckedStatus == true) {
        let deletedRowID = this.form.controls['holidayList'].value[i].holidayId
        //console.log('deletedRowID',deletedRowID);
        if (deletedRowID != 0) {
          this.deletedHolidayListId.push(deletedRowID)
        }
        const control = <FormArray>this.form.controls['holidayList'];
        control.removeAt(i);
      }
    }
    // if(this.deletedHolidayListId.length>0){
    //   let url = this.taskService.Org_URL;
    //   let deletedListId: any = {};
    //   deletedListId.deletedHolidayListId = this.deletedHolidayListId
    //   this.http.post(url+"/deleteOrgCalendarHoliday", deletedListId).subscribe(res=>{
    //     console.log(res);

    //   })
    //   console.log(deletedListId);

    // }
    //console.log(this.deletedHolidayListId);

    this.selectionForholiday.clear();
    let lengthAfterDelete = this.form.controls['holidayList'].value.length;
    if (!lengthAfterDelete) {
      this.addholiday();
    }
    // this.deletedHolidayListId=[];
  }

  editForm() {
    let url = this.taskService.Org_Cal_URL
    if (this.id != undefined) {
      this.http.get(url + "/getOrgCalendar/" + this.id).subscribe(data => {
        this.calendarDetailByID = data
        console.log(this.calendarDetailByID);
        this.form.patchValue({
          locale: this.calendarDetailByID.locale,
          location: this.calendarDetailByID.location,
          timezone: this.calendarDetailByID.timezone,
          fiscalStart: this.calendarDetailByID.fiscalStart,
          administrativeStart: this.calendarDetailByID.administrativeStart
        })
        this.patchWeekList(this.calendarDetailByID.weekList),
          this.patchHourList(this.calendarDetailByID.hourList),
          this.patchHolidayList(this.calendarDetailByID.holidayList)
      })

    }
    else {
      this.newForm()
    }
  }

  // get formArr() {
  //   return this.form.get('weekList') as FormArray;
  // }

  patchWeekList(weekList) {

    let weekListArray = this.form.get('weekList') as FormArray;
    const control = <FormArray>this.form.controls['weekList'];
    control.removeAt(0);
    weekList.forEach((x) => {
      x.workWeekCheckedStatus = ''
      x.workHours = String(x.workHours)
      x.workdays = String(x.workdays)
      weekListArray.push(this.fb.group(x))
    });
  }

  patchHourList(hourList) {
    let hourListArray = this.form.get('hourList') as FormArray;
    const control = <FormArray>this.form.controls['hourList'];
    control.removeAt(0);
    hourList.forEach((x) => {
      x.workShiftCheckedStatus = ''
      x.breaks = String(x.breaks)
      x.startTime = String(x.startTime)
      x.workHours = String(x.workHours)
      hourListArray.push(this.fb.group(x))
    });
  }
  patchHolidayList(holidayList) {
    let holidayListArray = this.form.get('holidayList') as FormArray;
    const control = <FormArray>this.form.controls['holidayList'];
    control.removeAt(0);
    holidayList.forEach((x) => {
      // if(x.name == null && x.occurence==null && x.type==null && x.date==null && x.day==null){
      //   this.deletedHolidayListId.push(x.holidayId)
      //   control.removeAt(i);
      // }
      // else{
      x.holidayCheckedStatus = ''
      x.date = new Date(x.date)
      holidayListArray.push(this.fb.group(x))//}
    });
  }


  //holiday  selection model
  selectionForholiday = new SelectionModel(true, []);

  // Whether the number of selected elements matches the total number of rows.
  isAllholidaysSelected() {
    let control = <FormArray>this.form.controls['holidayList'];
    const numSelected = this.selectionForholiday.selected.length;
    const numRows = control.controls.length;
    return numSelected === numRows;
  }

  // Selects all rows if they are not all selected; otherwise clear selection. 
  masterToggleForholidays() {
    let formControl = <FormArray>this.form.controls['holidayList'];
    this.isAllholidaysSelected() ?
      this.uncheckAllholidays() : this.checkAllholidays();

  }
  uncheckAllholidays() {
    this.selectionForholiday.clear();
    let formControl = <FormArray>this.form.controls['holidayList'];
    let length = formControl.value.length;
    console.log('I got here', length);
    for (let i = 0; i < length; i++) {
      formControl.controls[i].patchValue({
        'holidayCheckedStatus': false
      })
    }
  }
  checkAllholidays() {
    let formControl = <FormArray>this.form.controls['holidayList'];
    formControl.controls.forEach(row => this.selectionForholiday.select(row));

    let length = formControl.value.length;
    for (let i = 0; i < length; i++) {
      //formControl.controls[i].get('holidayCheckedStatus').setValue(true);
      formControl.controls[i].patchValue({
        'holidayCheckedStatus': true
      })
    }
  }
  //holiday reacitve form ends
  onSubmit(formValue, formDirective: FormGroupDirective, submitType) {

    let url = this.taskService.Org_URL;
    let formObj: any = {};
    let holidayList = [];
    let weekList = [];
    let hourList = [];
    formValue.holidayList.forEach(element => {
      let holidayListVariables: any = {};

      holidayListVariables.name = element.name;
      holidayListVariables.type = +element.type;
      holidayListVariables.occurence = element.occurence;
      holidayListVariables.date = +element.date;
      holidayListVariables.day = element.day;
      holidayListVariables.holidayId = element.holidayId;
      holidayListVariables.calendarId = +element.calendarId;

      holidayList.push(holidayListVariables)

    });

    formValue.weekList.forEach(element => {
      let weekListVariables: any = {};
      weekListVariables.name = element.name;
      weekListVariables.workdays = +element.workdays;
      weekListVariables.startDay = element.startDay;
      weekListVariables.workHours = +element.workHours;
      weekListVariables.workWeekId = element.workWeekId;
      weekListVariables.calendarId = +element.calendarId;

      weekList.push(weekListVariables)

    });

    formValue.hourList.forEach(element => {
      let hourListVariables: any = {};
      hourListVariables.name = element.name;
      hourListVariables.startTime = +element.startTime;
      hourListVariables.workHours = +element.workHours;
      hourListVariables.breaks = +element.breaks;
      hourListVariables.duration = element.duration;
      hourListVariables.workHourId = element.workHourId;
      hourListVariables.calendarId = +element.calendarId;


      hourList.push(hourListVariables)
    });

    formObj["locale"] = formValue.locale;
    formObj["location"] = formValue.location;
    formObj["timezone"] = formValue.timezone;
    formObj["fiscalStart"] = formValue.fiscalStart;
    formObj["administrativeStart"] = formValue.administrativeStart;
    formObj["holidayList"] = holidayList;
    formObj["weekList"] = weekList;
    formObj["hourList"] = hourList;

    if (this.id == 0 || this.id == undefined) {
      this.http.post(url + "/saveOrgCalendar", formObj).subscribe(data => {
        let details = data

        this.onSaveComplete(formDirective, submitType);
      })
    }
    else {

      let url = this.taskService.Org_URL;

      if (this.deletedHolidayListId.length > 0) {
        
        let deletedListId: any = {};
        deletedListId.deletedHolidayListId = this.deletedHolidayListId

        this.updateHolidayList(url, deletedListId);
        this.deletedHolidayListId = [];
      }
      if (this.deletedWeekListId.length > 0) {
        
        let deletedListId: any = {};
        deletedListId.deletedWeekListId = this.deletedWeekListId

        this.updateWeekList(url, deletedListId);
        this.deletedWeekListId = [];
      }
      if (this.deletedHourListId.length > 0) {
        
        let deletedListId: any = {};
        deletedListId.deletedHourListId = this.deletedHourListId

        this.updateHourList(url, deletedListId);
        this.deletedHourListId = [];
      }


      formObj["calendarId"] = this.calendarDetailByID.calendarId
      // formObj["orgId"] = this.calendarDetailByID.orgId
      console.log('formObj', formObj);

      this.updateForm(url,formObj,formDirective, submitType);
      
    }

  }

  updateHolidayList(url, deletedListId){
    
    this.http.post(url + "/deleteOrgCalendarHoliday", deletedListId).subscribe(res => {})
  }
  updateWeekList(url, deletedListId){
    this.http.post(url + "/deleteOrgCalendarWorkWeek", deletedListId).subscribe(res => {})
  }
  updateHourList(url, deletedListId){
    this.http.post(url + "/deleteOrgCalendarWorkHour", deletedListId).subscribe(res => {})
  }

  updateForm(url,formObj,formDirective, submitType){
   this.http.put(url + "/updateOrgCalendar", formObj).subscribe(data => {
     let details = data
     console.log(details);
     
   })
   this.onSaveComplete(formDirective, submitType);
  }

  onSaveComplete(formDirective, submitType) {
   // console.log("details", details.body);
    formDirective.resetForm();
    this.form.reset();
    // this.showSuccess = true;
    this.router.navigate(['eta-web/calendarsListOfOrgAcronym'], {
      queryParams: {
        "showSuccess": submitType
      }
    });
  }




}


