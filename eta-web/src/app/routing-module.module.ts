import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './form/login/login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { materialModule } from './material';
import { AcceptTermsConditionsComponent } from './form/user/user-activation/accept-terms-conditions/accept-terms-conditions.component';
import { AcceptPrivacyPolicyComponent } from './form/user/user-activation/accept-privacy-policy/accept-privacy-policy.component';



const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'acceptcondition', component: AcceptTermsConditionsComponent },
  { path: 'acceptpolicy', component: AcceptPrivacyPolicyComponent },
  { path: 'eta-web', loadChildren: './post-login-module/post-login-module.module#PostLoginModuleModule' },
  { path: '', redirectTo: '/login', pathMatch: 'full' },

];

@NgModule({
  declarations: [LoginComponent, AcceptTermsConditionsComponent, AcceptPrivacyPolicyComponent],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes),
    materialModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  exports: [RouterModule],
})
export class RoutingModuleModule { }
