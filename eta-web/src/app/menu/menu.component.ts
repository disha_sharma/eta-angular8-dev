import { Component, OnInit, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormGroup, FormControl, FormArray, NgForm } from '@angular/forms';
import { Http, Headers, RequestOptions } from '@angular/http';
import { ServiceService } from './../eta-services/service.service'

import { Router } from '@angular/router';
@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {
  Core_URL: any;
  adminRole: any;
  myRole: any;
  orgList: any;
  selectedOrgId: number;
  headerMenuList:any;

  constructor(private http: HttpClient, private shareServices: ServiceService, private route: Router) {
    this.Core_URL = this.shareServices.Core_URL;
    this.myRole = this.shareServices.myRole;
    this.adminRole = this.shareServices.adminRole;
    this.getMenu();
    this.getAllOrgList();

  }

  data: any;
  process: any[];
  ngOnInit() {
    this.getHeaderMenuList();
    

  }
  private getMenu() {
    
    this.http.get( this.shareServices.User_URL+ '/getAllFunctionAndProcess')
      .subscribe(data => {
        this.data = data;
        //console.log("*********************** your menu *************************");
        //console.log(this.data);
      });
  }
  private getAllOrgList() {
    this.shareServices.getAllOrgDetailsMenuDropdown()
      .subscribe(data => {
        this.orgList = data;
      })
  }
  orgIdSelected(event) {

    this.selectedOrgId = event;
    localStorage.setItem('EtaSelectOrgId', event);
    this.route.navigateByUrl("/refresh" , { skipLocationChange: false }).then(() =>
      this.route.navigate(["/refresh"]));
  }

  menuTypeList: any = [];
  menuList: any = [];
  private functionImage:any;
  private functionId:any;
  getHeaderMenuList() {
    this.shareServices.getHeaderMenu()
      .subscribe(res => {
       // console.log(res);
        
        this.menuList = res;
        let menu: any = {};
        
        this.menuList.forEach(element => {
          this.functionImage = element.functionImage;
          this.functionId = element.fucntionId;
          let mainMenuType: any = {};
          if (element.menuType == 1) {
            if(this.functionId!=null){
            this.getFuinctiondatabyOrgId(this.functionId,this.functionImage);
            }
            mainMenuType.translation = element.translation;
            mainMenuType.mainType = element.menuType;
            mainMenuType.mainRef = element.menuRef;
            mainMenuType.mainId = element.menuId;
            mainMenuType.routerLink = element.routerLink;            
            this.menuList.forEach(subelement => {
              let subMenuList: any = [];
              let subMenuType: any = {};
              if (subelement.menuType == 2) {              
               
                if (mainMenuType.mainId == subelement.menuRef) {
                  subMenuType.translation = subelement.translation;
                  subMenuType.mainType = subelement.menuType;
                  subMenuType.mainRef = subelement.menuRef;
                  subMenuType.mainId = subelement.menuId;                 
                  subMenuList.push(subelement);
                  mainMenuType.subMenu = subMenuList;
                }
              }
            })
            
            this.menuTypeList.push(mainMenuType);
          }
         
          menu.menu = this.menuTypeList;

        });      
        // console.log("menuTypeList :111:: ");
       // console.log(this.menuTypeList);
      }) 
  
      
  }

  getFuinctiondatabyOrgId(functionId,functionImage) {      
        this.shareServices.getFunctionImage(this.functionId, this.functionImage)
        .subscribe(res => {         
        this.createImageFromBlob(res)
        })   
  }



  imageToShow: any;

  createImageFromBlob(image: Blob) {
    let reader = new FileReader();
    reader.addEventListener("load", () => {
      this.imageToShow = reader.result;
    }, false);

    if (image) {
      reader.readAsDataURL(image);
    }
  }
  colorSelect: Array<any>;

}