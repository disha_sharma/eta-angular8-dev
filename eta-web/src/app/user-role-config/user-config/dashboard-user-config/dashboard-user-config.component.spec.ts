import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardUserConfigComponent } from './dashboard-user-config.component';

describe('DashboardUserConfigComponent', () => {
  let component: DashboardUserConfigComponent;
  let fixture: ComponentFixture<DashboardUserConfigComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardUserConfigComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardUserConfigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
