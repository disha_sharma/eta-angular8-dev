import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort, MatDialog, MatDialogConfig } from '@angular/material';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { SelectionModel } from '@angular/cdk/collections';
import { Router, ActivatedRoute } from '@angular/router';
import { merge, Observable, of as observableOf } from 'rxjs';
import { catchError, map, startWith, switchMap } from 'rxjs/operators';
import { HttpClient, HttpHeaderResponse } from '@angular/common/http';
import { UserConfigServiceService } from '../user-service/user-config-service.service';
import { ServiceService } from '../../../eta-services/service.service';
@Component({
  selector: 'app-users-in-org-acronym',
  templateUrl: './dashboard-user-config.component.html',
  styleUrls: ['./dashboard-user-config.component.css'],
  animations: [
    trigger('detailExpand', [
      state('void', style({ height: '0px', minHeight: '0', visibility: 'hidden' })),
      state('*', style({ height: '*', visibility: 'visible' })),
      transition('void <=> *', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ]
})
export class DashboardUserConfigComponent implements OnInit {


  displayedColumns: string[] = ['checkbox', 'name', 'roles', 'username', 'type', 'status'];
  dataSourceUsers = new MatTableDataSource<UserData>();
  rowsSelection = new SelectionModel(true, []);
  selection = new SelectionModel(true, []);
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  pageSize: number;
  resultsLength = 0;
  showMessageAlert: string = "";
  pageSizeOptions = "";
  isLoadingResults = true;
  isRateLimitReached = false;
  httpResponse: number = 0;
  username : string = "";
  deleteFlag: boolean = false;
  public deleteUserData: any = [];

  userRolesDao: UserRolesHttpDao | null;

  OrgListDetails : any[] =[];
  public mainbreadcrumb: string;
  public menuList: any = [];
  public breadcrumb: string;

  isExpansionDetailRow = (index, row) => row.hasOwnProperty('detailRow');

  constructor(private http: HttpClient, public dialog: MatDialog, private router: Router, private userConfigService: UserConfigServiceService, private activatedRoute: ActivatedRoute,public shareServices: ServiceService) {
    this.getHeaderMenuList();

  }

  ngOnInit() {
    this.getAllOrgList();
    this.removeMessageAlert();
    this.pageSize = 5;

    this.dataSourceUsers.paginator = this.paginator;
    this.dataSourceUsers.sort = this.sort;

    let action = this.activatedRoute.snapshot.queryParams['action'];
    this.username = this.activatedRoute.snapshot.queryParams['userName'];
    console.log('this.activatedRoute.snapshot.queryParams',this.activatedRoute.snapshot.queryParams);
    if (action == 'submit' || action == 'update') {
      this.showMessageAlert = this.activatedRoute.snapshot.queryParams['action'];
      this.getAllUsersAndRolesInfo();
    }
    // If the user changes the sort order, reset back to the first page.
   // this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    this.userRolesDao = new UserRolesHttpDao(this.http, this.userConfigService);
    // If the user changes the sort order, reset back to the first page.
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        startWith({}),
        switchMap(() => {
          this.isLoadingResults = true;
          if (this.paginator.pageSize != undefined) {
            this.pageSize = this.paginator.pageSize;
          }
          return this.userRolesDao!.getRepoIssues(this.sort.active, this.sort.direction, this.paginator.pageIndex, this.pageSize);
        }),
        map(data => {
          // Flip flag to show that loading has finished.
          this.isLoadingResults = false;
          this.isRateLimitReached = false;
          this.resultsLength = data.recordsTotal;
          console.log("data length : " + data.recordsTotal);
          return data.data;
        }),
        catchError((response: HttpHeaderResponse) => {
          if (response.status === 404) {
            this.httpResponse = response.status;
          }

          this.isLoadingResults = false;
          // Catch if the GitHub API has reached its rate limit. Return empty data.
          this.isRateLimitReached = true;
          return observableOf([]);
        })
      ).subscribe(data => {
        this.httpResponse = 200;
        this.dataSourceUsers = new MatTableDataSource(data);
      });
      this.router.navigate(['eta-web/userConfigDashboard'], { queryParams: {} });

      

  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSourceUsers.filter = filterValue;
  }
  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    this.removeMessageAlert();
    const numSelected = this.rowsSelection.selected.length;
    const numRows = this.dataSourceUsers.data.length;
    return numSelected === numRows;
  }

  onPaginateChange(event) {
    this.pageSizeOptions = event.pageSizeOptions;
    console.log("page Size index : " + this.pageSizeOptions);
  }



  /** Selects all rows if they are not all selected; otherwise clear rowsSelection. */
  masterToggle() {
    this.removeMessageAlert();
    this.isAllSelected() ?
      this.rowsSelection.clear() :
      this.dataSourceUsers.data.forEach(row => this.rowsSelection.select(row));
  }

  public getAllUsersAndRolesInfo(): void {
    if (this.pageSize == null || this.pageSize === undefined)
      this.pageSize = 5;
    this.selection.clear();
    this.userConfigService.getAllUsersRolefoDetails("asc", 0, this.pageSize).subscribe(response => {
      this.resultsLength = response.recordsTotal;
      this.dataSourceUsers = new MatTableDataSource(response.data);
      this.paginator.pageIndex = 0;
    })
  }

  openAddPage(){
    this.removeMessageAlert();
    this.router.navigate(['eta-web/add-user']);
  }

  /*This Function to use for Display Breadcrumb Menu*/
  getHeaderMenuList() {
    this.shareServices.getHeaderMenu()
      .subscribe(res => {
        this.menuList = res;
        this.menuList.forEach(element => {
          if (element.routerLink == "#1") {
            this.mainbreadcrumb = element.translation;
          }
          if (element.routerLink == "eta-web/userConfigDashboard") {
            this.breadcrumb = element.translation;
          }
        })
      })
  }

   /*This Function to use for remove Message Alert*/
   removeMessageAlert() {
    this.showMessageAlert = "";
  }

   /*This Function to use for Delete roles*/
   deleteUserDataFunction() {
    this.rowsSelection.selected.forEach(item => {
      this.deleteUserData.push(item.userId);
    });
    this.userConfigService.deleteUser(this.deleteUserData).subscribe(response => {
      if(response.responseMsg = "Deleted successfully"){
        this.showMessageAlert = 'delete';
        this.deleteFlag = true;
        this.getAllUsersAndRolesInfo();
      }
    });
    this.rowsSelection.clear();
  }

   /* This function is use for open edit Page*/
   openEditPage(UserId) {
    this.removeMessageAlert();
    this.router.navigate(['eta-web/edit-user'], { queryParams: { UserId: UserId } });
  }

  //STOP PROPEGATON FUNCTION
  stopPropegation(event) {
    event.stopPropagation();
  }

  getAllOrgList(){
    this.userConfigService.getAllOrgList().subscribe(response => {
      if(response[0] && response[0].orgName){
        this.OrgListDetails =  response[0].orgName;
      }
    })
  }
}

export interface UserData {
  name: string;
  roles: string;
  username: string;
  type: string;
  status: string;
  details: any;

}



export class UserRolesHttpDao {
  constructor(private http: HttpClient, private userConfigServiceService: UserConfigServiceService) {
  }

  getRepoIssues(sort: string, order: string, page: number, pageSize: number): Observable<any> {
    console.log("order : " + order);
    console.log("page : " + page);
    console.log("pageSize : " + pageSize);
    if (order.length === 0)
      order = "asc";
    return this.userConfigServiceService.getAllUsersRolefoDetails(order, page, pageSize);
  }

}//end of the class
