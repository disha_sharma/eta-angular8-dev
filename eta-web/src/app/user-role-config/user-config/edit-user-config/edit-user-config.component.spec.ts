import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditUserConfigComponent } from './edit-user-config.component';

describe('EditUserConfigComponent', () => {
  let component: EditUserConfigComponent;
  let fixture: ComponentFixture<EditUserConfigComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditUserConfigComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditUserConfigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
