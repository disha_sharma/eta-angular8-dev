import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormControl } from '@angular/forms';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ArrayType } from '@angular/compiler/src/output/output_ast';
import { UserConfigServiceService } from '../user-service/user-config-service.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable, of } from 'rxjs';
import { mergeMap } from 'rxjs/operators';
import { SelectionModel } from '@angular/cdk/collections';
import { throwError } from 'rxjs';
import { ServiceService } from '../../../eta-services/service.service';

@Component({
  selector: 'app-edit-user-config',
  templateUrl: './edit-user-config.component.html',
  styleUrls: ['./edit-user-config.component.css']
})
export class EditUserConfigComponent implements OnInit {
  form: FormGroup;
  roleList: any[] = [];
  typeList: any[] = [];
  selectedArray: any[] = [];
  userListDetails: any[] = [];
  dataSourceUserName: Observable<any>;
  userName: string = '';
  userList: any[] = [];
  typeaheadLoadingUser: boolean;
  duplicateUserName = false;
  userId: any;
  userRealName: string = "";
  UserDetails: any;
  roleIdList: any[] = [];
  submitButtonDisabled : boolean = true;
  OrgListDetails : any[] =[];

  public mainbreadcrumb: string;
  public menuList: any = [];
  public breadcrumb: string;


  constructor(private fb: FormBuilder, private userConfigService: UserConfigServiceService, private router: Router, private activeRouter: ActivatedRoute,private loginService: ServiceService, public shareServices: ServiceService) {
    this.getHeaderMenuList();

    this.dataSourceUserName = Observable.create((observer: any) => {
      observer.next(this.userName); // Runs on every search
    })
      .pipe(
        mergeMap((token: string) => this.getDataUser(token))
      );
    this.userId = this.activeRouter.snapshot.queryParams['UserId'];
    this.userConfigService.getUserDetailsByUserId(parseInt(this.userId)).subscribe(response => {
      this.UserDetails = response;
      this.getRolesList();
    });
  }

  ngOnInit() {
    this.form = this.fb.group({
      'firstname': ['', Validators.required],
      'lastname': ['', Validators.required],
      'email': ['', Validators.required],
      'username': ['', Validators.required],
      'type': ['', Validators.required],
      'roleConfigInfoList': this.fb.array([
        this.initdynamicItem()
      ])
    });
    this.getAllRoleList();
    this.getAllTypeList();
    this.getUserDetails();
    this.getOrgList();
  }
  onFilterChange(value: string) {
    console.log('filter:', value);
  }
  initdynamicItem() {
    return this.fb.group({
      'dynamicItemCheckedStatus': [''],
      'roleId': [[], Validators.required],
      'filteredBanks': []

    });
  }

  getRolesList() {
    this.deleteAllRoles();
    for (let i = 0; i < this.UserDetails.roleConfigInfoList.length; i++) {
      this.adddynamicItem(undefined);
    }
    this.userRealName = this.UserDetails.userName;
    this.form.patchValue({
      firstname: this.UserDetails.firstName,
      lastname: this.UserDetails.lastName,
      email: this.UserDetails.emailAddress,
      type: this.UserDetails.userType,
      username: this.UserDetails.userName,
      roleConfigInfoList: this.UserDetails.roleConfigInfoList,
    })
  }

  deleteAllRoles() {
    let length = this.form.controls['roleConfigInfoList'].value.length;
    for (let i = 0; i < length; i++) {
      const control = <FormArray>this.form.controls['roleConfigInfoList'];
      control.removeAt(i);
    }
  }


  //add data object 
  adddynamicItem(event) {
    const control = <FormArray>this.form.controls['roleConfigInfoList'];
    control.push(this.initdynamicItem());
    if(event){
      setTimeout(() => {
        this.scrollToTarget();
      }, 0);
    }
    }
    @ViewChild('usersScrollabeContent', { read: ElementRef, static: true }) public usersScrollabeContent: ElementRef<any>;
    @ViewChild('scrollTarget', { read: ElementRef, static: true }) public scrollTarget: ElementRef<any>;
    scrollToTarget() {
     this.usersScrollabeContent.nativeElement.scrollTo({ top: this.usersScrollabeContent.nativeElement.scrollHeight, behavior: 'smooth' });
      this.scrollTarget.nativeElement.scrollIntoView(false);
  }

  //delete data object
  deletedynamicItem() {
    this.enableSubmitButtonForEdit();
    var i = this.form.controls['roleConfigInfoList'].value.length;
    while (i--) {
      if (this.form.controls['roleConfigInfoList'].value[i].dynamicItemCheckedStatus == true) {
        const control = <FormArray>this.form.controls['roleConfigInfoList'];
        control.removeAt(i);
      }
    }
    this.selectionFordynamicItem.clear();
    let lengthAfterDelete = this.form.controls['roleConfigInfoList'].value.length;
    if (!lengthAfterDelete) {
      this.adddynamicItem(undefined);
    }

  }


  //data object  selection for delete
  selectionFordynamicItem = new SelectionModel(true, []);

  // Whether the number of selected elements matches the total number of rows.
  isAlldynamicItemsSelected() {
    let control = <FormArray>this.form.controls['roleConfigInfoList'];
    const numSelected = this.selectionFordynamicItem.selected.length;
    const numRows = control.controls.length;
    return numSelected === numRows;
  }

  // Selects all rows if they are not all selected; otherwise clear selection. 
  masterToggleFordynamicItems() {
    let formControl = <FormArray>this.form.controls['roleConfigInfoList'];
    this.isAlldynamicItemsSelected() ?
      this.unCheckAlldynamicItems() : this.checkAlldynamicItems();

  }
  unCheckAlldynamicItems() {
    this.selectionFordynamicItem.clear();
    let formControl = <FormArray>this.form.controls['roleConfigInfoList'];
    let length = formControl.value.length;
    for (let i = 0; i < length; i++) {
      formControl.controls[i].patchValue({
        'dynamicItemCheckedStatus': false
      })
    }
  }
  checkAlldynamicItems() {
    let formControl = <FormArray>this.form.controls['roleConfigInfoList'];
    formControl.controls.forEach(row => this.selectionFordynamicItem.select(row));

    let length = formControl.value.length;
    for (let i = 0; i < length; i++) {
      formControl.controls[i].patchValue({
        'dynamicItemCheckedStatus': true
      })
    }
  }


  getAllRoleList() {
    var userType = "";
    this.loginService.getUserActivationStatusByToken().subscribe(response => {
      if(response && response.userType){
        userType = response.userType;
        this.userConfigService.getAllRoleList(userType).subscribe(response => {
          this.roleList = response
        })
      }
    });
    
  }

  getAllTypeList() {
    this.userConfigService.getAllTypeList().subscribe(response => {
      this.typeList = response
    })
  }

  /*
 This function is use for When Name AlphaNumeric Validation.
 */
  checkCharValidationForName(value) {
    var charCode = value.keyCode;
    if (this.form.controls['firstname'].value.length == 0) {
      if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123))
        return true;
      else
        return false;
    }
  }

  checkCharValidationForlastname(value) {
    var charCode = value.keyCode;
    if (this.form.controls['lastname'].value.length == 0) {
      if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123))
        return true;
      else
        return false;
    }
  }

  changeRole(value, index) {
    if (value) {
      if (this.form.value.roleConfigInfoList.length > 0) {
        for (var i = 0; i < this.form.value.roleConfigInfoList.length; i++) {
          if (i != index) {
            if (this.form.value.roleConfigInfoList[i].roleId == value) {
              const control = <FormArray>this.form.controls['roleConfigInfoList'];
              let secondControl = <FormArray>control.controls[index];
              secondControl.controls['roleId'].setErrors({ 'myError': true });
            }
          }
        }
      }
    }
  }



  submitEditUser() {
    if (this.form.value && this.form.value.roleConfigInfoList.length > 0) {
      for (var i = 0; i < this.form.value.roleConfigInfoList.length; i++) {
        this.roleIdList.push(this.form.value.roleConfigInfoList[i].roleId);
      }
    }
    this.userConfigService.saveData(this.form.value, this.userId, this.roleIdList).subscribe(data => {
      this.router.navigate(['eta-web/userConfigDashboard'], { queryParams: { action: 'update', userName: this.userRealName}})
    }
    );
  }
  cancel() {
    this.router.navigate(['eta-web/userConfigDashboard'], { queryParams: { action: 'cancel' } })
  }

  getUserDetails() {
    this.userConfigService.userDetails().subscribe(response => {
      this.userListDetails = response;
      for (let s of this.userListDetails) {
        this.userList.push(s);
      }
    })
  }

  changeEmail(value) {
    if (this.userListDetails.length > 0) {
      for (var i = 0; i < this.userListDetails.length; i++) {
        if (this.userListDetails[i].emailAddress == value) {
          const control = <FormArray>this.form.controls['email'];
          control.setErrors({ 'myEmailError': true });
        }
      }
    }
  }

  /*
  * This function use for get Users data.
  */
  public getDataUser(token: string): Observable<any> {
    const query = new RegExp(token, 'i');
    this.userList.filter((str: any) => {
      if (query.test(str.userName)) {
      }
    })
    return of(
      this.userList.filter((str: any) => {
        return query.test(str.userName);
      })
    );
  }

  /*
    * This function use for change Type ahead Loading User.
    */
  changeTypeaheadLoadingUser(e) {
    this.typeaheadLoadingUser = e;
  }

  /*
  * This function use for type ahead On Select User.
  */
  typeaheadOnSelectUser(e) {
    this.userName = e.value.toLocaleLowerCase();
    var userPatchName = this.userRealName.toLocaleLowerCase();
    this.duplicateUserName = false;
    for (let s of this.userList) {
      JSON.parse(JSON.stringify(s), (key, value) => {
        if (key === 'userName') {
          value = value.toLocaleLowerCase();
          if (this.userName.trim().length === userPatchName.trim().length && this.userName.trim() === userPatchName.trim()) {
            this.duplicateUserName = false;
          } else if ((this.userName.trim() != userPatchName.trim()) && (value.trim() === this.userName.trim())) {
            this.form.controls['username'].setErrors({ 'invalid': true });
            this.duplicateUserName = true;
          }
        }
      });
    }
  }





  /*
    * This function use for on key up in User type ahead.
    */
  checkOnKeyUpUser(event) {
    this.duplicateUserName = false;
    if (event && event.target && event.target.value) {
      var valueUserName = event.target.value.toLowerCase();
      var userName = this.userRealName.toLocaleLowerCase();
      for (let s of this.userList) {
        JSON.parse(JSON.stringify(s), (key, value) => {
          if (key === 'userName') {
            value = value.toLocaleLowerCase();
            if (valueUserName.trim().length === userName.trim().length && this.userRealName.toLocaleLowerCase() == valueUserName && valueUserName.trim() === userName.trim()) {
              this.duplicateUserName = false;
            } else if ((valueUserName.trim() != userName.trim()) && (value == valueUserName) && (value.trim() === valueUserName.trim())) {
              this.form.controls['username'].setErrors({ 'invalid': true });
              this.duplicateUserName = true;
            }
          }
        });
      }
    }

  }

  /*
  This function is use for When Name AlphaNumeric Validation.
  */
  checkCharValidationForUserName(value) {
    var charCode = value.keyCode;
    if (this.form.controls['username'].value && this.form.controls['username'].value.length == 0) {
      if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123))
        return true;
      else
        return false;
    }
  }

  /**This Function use for Call On every Field Change Function*/
  public enableSubmitButtonForEdit() {
    this.submitButtonDisabled = false;
  }

  getOrgList(){
    this.userConfigService.getAllOrgList().subscribe(response => {
      if(response[0] && response[0].orgName){
        this.OrgListDetails =  response[0].orgName;
      }
      
    })
  }

  /*This Function to use for Display Breadcrumb Menu*/
  getHeaderMenuList() {
    this.shareServices.getHeaderMenu()
      .subscribe(res => {
        this.menuList = res;
        this.menuList.forEach(element => {
          if (element.routerLink == "#6") {
            this.mainbreadcrumb = element.translation;
          }
          if (element.routerLink == "eta-web/userConfigDashboard") {
            this.breadcrumb = element.translation;
          }
        })
      })
  }


}
