import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddUserConfigComponent } from './add-user-config.component';

describe('AddUserConfigComponent', () => {
  let component: AddUserConfigComponent;
  let fixture: ComponentFixture<AddUserConfigComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddUserConfigComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddUserConfigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
