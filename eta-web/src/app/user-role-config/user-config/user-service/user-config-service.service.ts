import { Injectable } from '@angular/core';
//import { ServiceService } from '../../eta-services/service.service';
import { CookieService } from 'ngx-cookie-service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../../environments/environment.test';



const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};


const httpOptions2 = {
  headers: new HttpHeaders({ 'responseType': 'text' })
};

@Injectable({
  providedIn: 'root'
})
export class UserConfigServiceService {

  adminRole = "admin";
  myRole;
  constructor(private cookieService: CookieService, private httpClient: HttpClient) {
    this.myRole = cookieService.get('my_user');
    this.checkedSessionToken();
   }

   /********* toGetToken *************/
   public checkedSessionToken(): boolean {
    let istokenExist: boolean = false;
    if (sessionStorage.getItem("AuthToken") != null) {
      istokenExist = true;
    }
    return istokenExist;
  }

   /**
   * This method get all users and role details
   * 
   */

  public getAllUsersRolefoDetails(order: string, page : number, pageSize : number): Observable<any> {
    const body = JSON.stringify({
      "page": page,
      "size" : pageSize,
      "direction": order,
      "columnName" : "userName",
      observe: 'response'
    });
    return this.httpClient.post<any>(environment.User_URL  + '/getAllUserRoles', body, httpOptions);
  }

  public getAllRoleList(userType){
    return this.httpClient.get<any>(environment.User_URL  + '/getAllRolesByOrgIdAndRoleTypeId/' + userType);
  }

  public getAllTypeList(){
    return this.httpClient.get<any>(environment.User_URL  + '/getUserConfigType');
  }

  public addUserDetails(value): Observable<any>{
    const body = JSON.stringify({
      "firstName":value.firstname,
      "lastName":value.lastname,
      "emailAddress":value.email,
      "userType":value.type,
      "fullName":value.firstname +" "+value.lastname,
      "userName":value.username,
      "roleConfigInfoList":value.roleConfigInfoList,
      "preferredLanguageID":"L01",
   });
    return this.httpClient.post<any>(environment.User_URL  + '/addUserDetails', body, httpOptions);
  }

  public userDetails(){
    return this.httpClient.get<any>(environment.User_URL  + '/getAllUserDetails');
  }

  public deleteUser(userIds){
    var object = {
      "userIds":userIds
    }
    return this.httpClient.post<any>(environment.User_URL + '/deleteUserByUserIds', object, httpOptions);
  }

  public getUserDetailsByUserId(userId){
    var  object = {
       "userId" : userId
    }
    return this.httpClient.post<any>(environment.User_URL  + '/getUserRolesDetailsByUserId' , object,httpOptions);
  }

  public saveData(value,userId,roleIdList){
    var  object = {
      "userId": userId,
      "roleId": roleIdList,
      "firstName" :value.firstname,
      "lastName" :value.lastname,
      "userType" :value.type
    }
    return this.httpClient.post<any>(environment.User_URL  + '/editUserRoleConfigDetails' , object,httpOptions);
  }

  public getAllLdapUserNames(){
    return this.httpClient.get<any>(environment.User_URL  + '/getAllLdapUserNames');
  }
  public getAllOrgList(){
    return this.httpClient.get<any>(environment.Org_URL  + '/getAllOrgNameList');
  }
}
