import { TestBed } from '@angular/core/testing';

import { UserConfigServiceService } from './user-config-service.service';

describe('UserConfigServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UserConfigServiceService = TestBed.get(UserConfigServiceService);
    expect(service).toBeTruthy();
  });
});
