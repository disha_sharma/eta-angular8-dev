import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardRoleConfigComponent } from './dashboard-role-config.component';

describe('DashboardRoleConfigComponent', () => {
  let component: DashboardRoleConfigComponent;
  let fixture: ComponentFixture<DashboardRoleConfigComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardRoleConfigComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardRoleConfigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
