import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort, MatDialog, MatDialogConfig } from '@angular/material';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { SelectionModel } from '@angular/cdk/collections';
import { AddRoleConfigComponent } from '../add-role-config/add-role-config.component';
import { Router, ActivatedRoute } from '@angular/router';
import { RoleConfigServiceService } from '../role-service/role-config-service.service';
import { ServiceService } from '../../../eta-services/service.service';
import { merge, Observable, of as observableOf } from 'rxjs';
import { startWith, switchMap, map, catchError } from 'rxjs/operators';
import { HttpHeaderResponse, HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-dashboard-role-config',
  templateUrl: './dashboard-role-config.component.html',
  styleUrls: ['./dashboard-role-config.component.css'],
  animations: [
    trigger('detailExpand', [
      state('void', style({ height: '0px', minHeight: '0', visibility: 'hidden' })),
      state('*', style({ height: '*', visibility: 'visible' })),
      transition('void <=> *', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class DashboardRoleConfigComponent implements OnInit {

  displayedColumns: string[] = ['checkbox', 'roleName', 'privilegeCount', 'userCount', 'typeName', 'status'];
  dataSourceRoles = new MatTableDataSource<RolesData>();
  selection = new SelectionModel(true, []);
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  pageSize: number;
  resultsLength = 0;
  showMessageAlert: string = "";
  dataResponse: any;

  isExpansionDetailRow = (index, row) => row.hasOwnProperty('detailRow');

  public menuList: any = [];
  public mainbreadcrumb: string;
  public breadcrumb: string;
  roleName: string = "";
  isLoadingResults = true;
  isRateLimitReached = false;
  httpResponse: number = 0;
  public defaultColumnToSort: any;
  pageSizeOptions = "";
  rolesDao: RolesDataHttpDao | null;
  deleteFlag: boolean = false;
  public deleteRoleData: any = [];
  OrgListDetails: any[] = [];


  constructor(public dialog: MatDialog, private router: Router, private roleConfigService: RoleConfigServiceService, private activatedRoute: ActivatedRoute,
    public shareServices: ServiceService, private http: HttpClient) {

    this.getHeaderMenuList();

  }

  ngOnInit() {
    this.removeMessageAlert();
    this.getAllOrgList();
    this.pageSize = 5;
    this.dataSourceRoles.paginator = this.paginator;
    this.dataSourceRoles.sort = this.sort;

    let action = this.activatedRoute.snapshot.queryParams['action'];
    if (action == 'cancel' || action == 'submit' || action == 'update') {
      this.getAllRolesInfo();
    }

    this.roleName = this.activatedRoute.snapshot.queryParams['roleName'];
    if (this.activatedRoute.snapshot.queryParams['action'] != 'cancel') {
      this.showMessageAlert = this.activatedRoute.snapshot.queryParams['action'];
      this.getAllRolesInfo();

    }
    this.rolesDao = new RolesDataHttpDao(this.http, this.roleConfigService);
    // If the user changes the sort order, reset back to the first page.
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        startWith({}),
        switchMap(() => {
          this.isLoadingResults = true;
          if (this.paginator.pageSize != undefined) {
            this.pageSize = this.paginator.pageSize;
          }
          return this.rolesDao.getAllRolesDetails(this.sort.active, this.sort.direction, this.paginator.pageIndex, this.pageSize);
        }),
        map(data => {
          // Flip flag to show that loading has finished.
          this.isLoadingResults = false;
          this.isRateLimitReached = false;
          this.resultsLength = data.recordsTotal;
          return data.data;
        }),
        catchError((response: HttpHeaderResponse) => {
          if (response.status === 404) {
            this.httpResponse = response.status;
          }

          this.isLoadingResults = false;
          // Catch if the GitHub API has reached its rate limit. Return empty data.
          this.isRateLimitReached = true;
          return observableOf([]);
        })
      ).subscribe(data => {
        this.httpResponse = 200;
        this.dataSourceRoles = new MatTableDataSource(data);
      });
    this.router.navigate(['eta-web/roleConfigDashboard'], { queryParams: {} });

  }
  /*This Function to use for filter on Grid*/
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSourceRoles.filter = filterValue;
  }
  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    //this.removeMessageAlert();
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSourceRoles.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    //this.removeMessageAlert();
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSourceRoles.data.forEach(row => this.selection.select(row));
  }
  /*This Function to use for filter on Grid*/
  public openAddRole() {
    this.removeMessageAlert();
    this.router.navigate(['/eta-web/add-role']);
  }
  /*This Function to use for get All Info of Roles*/
  public getAllRolesInfo(): void {
    if (this.pageSize == null || this.pageSize === undefined)
      this.pageSize = 5;
    this.selection.clear();
    this.roleConfigService.getAllRolesInfoDetails("asc", 0, this.pageSize).subscribe(response => {
      this.dataResponse = response.data;
      this.resultsLength = response.recordsTotal;
      this.dataSourceRoles = new MatTableDataSource(response.data);
      this.paginator.pageIndex = 0;

    })
  }
  /*This Function to use for Display Breadcrumb Menu*/
  getHeaderMenuList() {
    this.shareServices.getHeaderMenu()
      .subscribe(res => {
        this.menuList = res;
        this.menuList.forEach(element => {
          if (element.routerLink == "#1") {
            this.mainbreadcrumb = element.translation;
          }
          if (element.routerLink == "eta-web/roleConfigDashboard") {
            this.breadcrumb = element.translation;
          }
        })
      })
  }
  /*This Function to use for remove Message Alert*/
  removeMessageAlert() {
    this.showMessageAlert = "";
  }
  /*This Function to use for Page Change Event*/
  onPageChange(event) {
    this.pageSizeOptions = event.pageSizeOptions;
  }
  /*This Function to use for Delete roles*/
  deleteRoleDataFunction() {
    this.selection.selected.forEach(item => {
      if (item.status != 1) {
        this.deleteRoleData.push(item.roleId);
      }
    });
    this.roleConfigService.deleteRole(this.deleteRoleData).subscribe(response => {
      if (response.deleteStatus == true) {
        this.showMessageAlert = 'delete';
        this.deleteFlag = true;
        this.getAllRolesInfo();
      }
    });

  }
  /* This function is use for open edit Page*/
  openEditPage(roleId) {
    this.removeMessageAlert();
    this.router.navigate(['/eta-web/edit-role'], { queryParams: { roleId: roleId } });
  }

  //STOP PROPEGATON FUNCTION
  stopPropegation(event) {
    event.stopPropagation();
  }

  getAllOrgList() {
    this.roleConfigService.getAllOrgList().subscribe(response => {
      if (response[0] && response[0].orgName) {
        this.OrgListDetails = response[0].orgName;
      }
    })
  }

}

/*This is interface to set data in grid*/
export interface RolesData {
  roleName: string;
  privilegeName: string;
  typeName: number;
  status: string;
  privilegeCount: number;
  userCount: number;
  privilegeList: any;

}

export class RolesDataHttpDao {
  constructor(private http: HttpClient, private roleConfigServiceService: RoleConfigServiceService) {
  }
  /*This is call to get roles data according to page change event*/
  getAllRolesDetails(sort: string, order: string, page: number, pageSize: number): Observable<any> {
    if (order.length === 0)
      order = "asc";
    return this.roleConfigServiceService.getAllRolesInfoDetails(order, page, pageSize);
  }

}


