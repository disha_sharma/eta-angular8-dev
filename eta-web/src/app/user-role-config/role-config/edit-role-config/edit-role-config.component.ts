import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormControl } from '@angular/forms';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ArrayType } from '@angular/compiler/src/output/output_ast';
import { SelectionModel } from '@angular/cdk/collections';
import { Router, ActivatedRoute } from '@angular/router';
import { RoleConfigServiceService } from '../role-service/role-config-service.service';
import { Observable, of } from 'rxjs';
import { mergeMap } from 'rxjs/operators';
import { ServiceService } from '../../../eta-services/service.service';

@Component({
  selector: 'app-edit-role-config',
  templateUrl: './edit-role-config.component.html',
  styleUrls: ['./edit-role-config.component.css']
})
export class EditRoleConfigComponent implements OnInit {

  form: FormGroup;
  selectedArray: any[] = [];
  privilegeTypeList: any[] = [];
  roleName: any;
  roleId: any;
  indexOfPrivilageList: any = [];
  roleDetails: any;
  roleList : any [] = [];
  dataSourceRoleName :  Observable<any>;
  typeaheadLoadingRole: boolean;
  duplicateroleName = false;
  roleRealName : string = "";
  submitButtonDisabled : boolean = true;
  OrgListDetails : any[] =[];

  public mainbreadcrumb: string;
  public menuList: any = [];
  public breadcrumb: string;


  constructor(private fb: FormBuilder, private router: Router, private roleConfigService: RoleConfigServiceService, private activeRouter: ActivatedRoute, public shareServices : ServiceService) {
    this.getHeaderMenuList();

    this.dataSourceRoleName = Observable.create((observer: any) => {
      observer.next(this.roleName); // Runs on every search
    })
    .pipe(
      mergeMap((token: string) => this.getDataRoles(token))
    );

    this.roleId = this.activeRouter.snapshot.queryParams['roleId'];
    this.roleConfigService.getRoleInfo(parseInt(this.roleId)).subscribe(response => {
      this.roleDetails = response;
      this.getPrivliageList();
    });
  }

  ngOnInit() {
    this.form = this.fb.group({
      'roleName': ['', Validators.required],
      'privilegeList': this.fb.array([
        this.initdynamicItem()
      ])
    });
    this.getAllPrivilegeTypeList();
    this.getAllOrgList();
    /*
    * This function use for get Role Name List for type head.
    */
   this.roleConfigService.getAllRoleNameList().subscribe(data =>{
    for(let s of data) {
      this.roleList.push(s);
    }
  });
  }

  getPrivliageList() {
    this.deleteAllPrivilage();
    for (let i = 0; i < this.roleDetails.privilegeList.length; i++) {
      this.adddynamicItem(undefined);
    }
    this.roleRealName = this.roleDetails.roleName;
    this.form.patchValue({
      roleName: this.roleDetails.roleName,
      privilegeList: this.roleDetails.privilegeList,
    })
  }

  deleteAllPrivilage() {
    let length = this.form.controls['privilegeList'].value.length;
    for (let i = 0; i < length; i++) {
      const control = <FormArray>this.form.controls['privilegeList'];
      control.removeAt(i);
    }
  }

  /*
  * This function use for filter on Grid.
  */
  onFilterChange(value: string) {
    console.log('filter:', value);
  }
  /*
    * This function use for intiliaze form group.
    */
  initdynamicItem() {
    return this.fb.group({
      'dynamicItemCheckedStatus': [''],
      'privilegeID': [[], Validators.required],
    });
  }

  /*This Function use for add privilege List*/
  adddynamicItem(event) {
    const control = <FormArray>this.form.controls['privilegeList'];
    control.push(this.initdynamicItem());
    if(event){
      setTimeout(() => {
        this.scrollToTarget();
      }, 0);
    }
    }
    @ViewChild('rolesScrollabeContent', { read: ElementRef, static: true }) public rolesScrollabeContent: ElementRef<any>;
    @ViewChild('scrollTarget', { read: ElementRef, static: true }) public scrollTarget: ElementRef<any>;
    scrollToTarget() {
     this.rolesScrollabeContent.nativeElement.scrollTo({ top: this.rolesScrollabeContent.nativeElement.scrollHeight, behavior: 'smooth' });
      this.scrollTarget.nativeElement.scrollIntoView(false);
  }

  /*This Function use for delete privilege List*/
  deletedynamicItem() {
    this.enableSubmitButtonForEdit();
    var i = this.form.controls['privilegeList'].value.length;
    while (i--) {
      if (this.form.controls['privilegeList'].value[i].dynamicItemCheckedStatus == true) {
        const control = <FormArray>this.form.controls['privilegeList'];
        control.removeAt(i);
      }
    }
    this.selectionFordynamicItem.clear();
    let lengthAfterDelete = this.form.controls['privilegeList'].value.length;
    if (!lengthAfterDelete) {
      this.adddynamicItem(undefined)
    }

  }
  /*This Function use for  privilege List selection for delete*/
  selectionFordynamicItem = new SelectionModel(true, []);

  /*This Function use for  Whether the number of selected elements matches the total number of rows*/
  isAlldynamicItemsSelected() {
    let control = <FormArray>this.form.controls['privilegeList'];
    const numSelected = this.selectionFordynamicItem.selected.length;
    const numRows = control.controls.length;
    return numSelected === numRows;
  }
  /*This Function use for  Selects all rows if they are not all selected; otherwise clear selection*/
  masterToggleFordynamicItems() {
    let formControl = <FormArray>this.form.controls['privilegeList'];
    this.isAlldynamicItemsSelected() ?
      this.unCheckAlldynamicItems() : this.checkAlldynamicItems();

  }
  /*This Function use for  UnSelect  rows*/
  unCheckAlldynamicItems() {
    this.selectionFordynamicItem.clear();
    let formControl = <FormArray>this.form.controls['privilegeList'];
    let length = formControl.value.length;
    for (let i = 0; i < length; i++) {
      formControl.controls[i].patchValue({
        'dynamicItemCheckedStatus': false
      })
    }
  }
  /*This Function use for  check All dynamic Items*/
  checkAlldynamicItems() {
    let formControl = <FormArray>this.form.controls['privilegeList'];
    formControl.controls.forEach(row => this.selectionFordynamicItem.select(row));

    let length = formControl.value.length;
    for (let i = 0; i < length; i++) {
      formControl.controls[i].patchValue({
        'dynamicItemCheckedStatus': true
      })
    }
  }
  /*
    This is call when click on Cancel Button
  */
  public cancel() {
    this.router.navigate(['eta-web/roleConfigDashboard'], { queryParams: { action: 'cancel' } })
  }
  /*
  This is call when click on Submit  Button
*/
  submitEditRole() {
    /*if (!this.form.valid) {
      return;
    }*/
    this.roleName = this.form.value.roleName;
    this.roleConfigService.editRolesDetails(this.form.value, this.roleId, this.roleDetails).subscribe(data => {
      this.router.navigate(['eta-web/roleConfigDashboard'], { queryParams: { action: 'update', roleName: this.roleName } })
    });
  }
  /*
  This is use for change event for privilage dropdown.
*/
  changePrivilege(value, index) {
    if (value) {
      if (this.form.value.privilegeList.length > 0) {
        for (var i = 0; i < this.form.value.privilegeList.length; i++) {
          if (i != index) {
            if (this.form.value.privilegeList[i].privilegeID == value) {
              const control = <FormArray>this.form.controls['privilegeList'];
              let secondControl = <FormArray>control.controls[index];
              secondControl.controls['privilegeID'].setErrors({ 'myError': true });
            }
          }
        }
      }
    }
  }

  /** Display All Privilege Type List Related With Human Task */
  public getAllPrivilegeTypeList() {
    this.roleConfigService.getAllPrivilegeTypeList().subscribe(data => {
      this.privilegeTypeList = data;
    });
  }

  /*
This function is use for When Name AlphaNumeric Validation.
*/
  checkCharValidationForName(value) {
    var str = value.target.value;
    if (str) {
    const matches = str.match(/^[A-Za-z][A-Za-z0-9/.,->_()\\*|:" ]*$/);
    return matches?null: this.form.controls['roleName'].setErrors({ 'patternError': true });
    }
  }

    /*
    * This function use for get Roles data.
    */
   public getDataRoles(token: string): Observable<any> {
    const query = new RegExp(token, 'i'); 
    this.roleList.filter((str: any) => {     
      if(query.test(str.roleName)){
      }  
    })
    return of(
      this.roleList.filter((str: any) => {     
        return query.test(str.roleName);
      })
    );
  }

  /*
    * This function use for change Type ahead Loading Role.
    */
   changeTypeaheadLoadingRole(e){
    this.typeaheadLoadingRole = e;
  }

   /*
    * This function use for type ahead On Select Role.
    */
   typeaheadOnSelectRole(e){
     console.log('e',e);
    this.roleName = e.value;
   // var RoleName = this.roleName.toLowerCase();
    this.duplicateroleName = false;
    for(let s of this.roleList) {
      JSON.parse(JSON.stringify(s), (key, value) => {
        if(key === 'roleName'){
           if(this.roleName.trim().length === this.roleRealName.trim().length && this.roleName.trim() === this.roleRealName.trim()){
            this.duplicateroleName=false;  
          }else if((this.roleName.trim().length != this.roleRealName.trim().length) && (this.roleName.trim() != this.roleRealName.trim()) && (value.trim().length === this.roleName.trim().length) && (value.trim() === this.roleName.trim())){
            this.form.controls['roleName'].setErrors({'invalid': true});
            this.duplicateroleName=true;  
          }
        }
      });       
    } 
  }

 
  /*
    * This function use for on key up in role type ahead.
    */
   checkOnKeyUpRole(event){
    this.duplicateroleName = false;
    if(event && event.target && event.target.value){
      var valueRoleName = event.target.value.trim();
      var roleEdit = this.roleRealName.trim();
      for(let s of this.roleList) {
        JSON.parse(JSON.stringify(s), (key, value) => {
          if(key === 'roleName'){
            var valueCheck = value.toLowerCase();
            if(valueRoleName.length === roleEdit.length &&  valueRoleName.toLocaleLowerCase() == roleEdit.toLowerCase()){
              this.duplicateroleName=false; 
            }else if(valueRoleName.length != roleEdit.length && valueRoleName.toLowerCase() != roleEdit.toLowerCase() && value.length === valueRoleName.length && value.toLowerCase() === valueRoleName.toLowerCase()){
              this.form.controls['roleName'].setErrors({'invalid': true});
              this.duplicateroleName=true;  
            }
          }
        });       
      }
    }
  }


  /**This Function use for Call On every Field Change Function*/
  public enableSubmitButtonForEdit() {
    this.submitButtonDisabled = false;
  }

  getAllOrgList(){
    this.roleConfigService.getAllOrgList().subscribe(response => {
      if(response[0] && response[0].orgName){
        this.OrgListDetails =  response[0].orgName;
      }
    })
  }

   /*This Function to use for Display Breadcrumb Menu*/
   getHeaderMenuList() {
    this.shareServices.getHeaderMenu()
      .subscribe(res => {
        this.menuList = res;
        this.menuList.forEach(element => {
          if (element.routerLink == "#6") {
            this.mainbreadcrumb = element.translation;
          }
          if (element.routerLink == "eta-web/roleConfigDashboard") {
            this.breadcrumb = element.translation;
          }
        })
      })
  }


}


