import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditRoleConfigComponent } from './edit-role-config.component';

describe('EditRoleConfigComponent', () => {
  let component: EditRoleConfigComponent;
  let fixture: ComponentFixture<EditRoleConfigComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditRoleConfigComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditRoleConfigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
