import { TestBed } from '@angular/core/testing';

import { RoleConfigServiceService } from './role-config-service.service';

describe('RoleConfigServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RoleConfigServiceService = TestBed.get(RoleConfigServiceService);
    expect(service).toBeTruthy();
  });
});
