import { Injectable } from '@angular/core';
//import { ServiceService } from '../../eta-services/service.service';
import { CookieService } from 'ngx-cookie-service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../../environments/environment.test';


const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};


const httpOptions2 = {
  headers: new HttpHeaders({ 'responseType': 'text' })
};

@Injectable({
  providedIn: 'root'
})
export class RoleConfigServiceService {
  adminRole = "admin";
  myRole;
  constructor(private cookieService: CookieService, private httpClient: HttpClient) {
    this.myRole = cookieService.get('my_role');
    this.checkedSessionToken();
   }

   /********* toGetToken *************/
   public checkedSessionToken(): boolean {
    let istokenExist: boolean = false;
    if (sessionStorage.getItem("AuthToken") != null) {
      istokenExist = true;
    }
    return istokenExist;
  }

    /**
   * This method will add the DataContainerDetails
   * 
   */
  public addRolesDetails(roleDetails: any) : Observable<any> {
    const body = JSON.stringify(roleDetails);
    return this.httpClient.post<any>(environment.User_URL  + '/addRoleConfig', body, httpOptions);
  }
   /*
   * This method for get All role Details
   */
  public getAllRolesInfoDetails(order: string, page : number, pageSize : number): Observable<any> {
    const body = JSON.stringify({
      "page": page,
      "size" : pageSize,
      "direction": order,
      "columnName" : "roleName",
      observe: 'response'
    });
    return this.httpClient.post<any>(environment.User_URL  + '/getAllRoleDetails', body, httpOptions);
  }
  /*
   * This method for get Privliage List
   */
  public getAllPrivilegeTypeList() : Observable<any> {
      return this.httpClient.get<any>(environment.User_URL + '/getPrivilegeListByOrgIdAndPrivilegeType/');
  }
  /*
   * This method for get Information basis on Role Id.
   */
  public getRoleInfo(roleId){
    return this.httpClient.get<any>(environment.User_URL + '/getRoleConfigByRoleId/' + roleId);
  }
  /*
   * This method for get Information basis on Role Id on Edit Page.
   */
  public editRolesDetails(roleDetails: any,roleId,details) : Observable<any> {
    var object = {
      "roleName" : roleDetails.roleName,
      "roleId" :roleId,
      "privilegeList" : roleDetails.privilegeList,
      "orgId" : details.orgId,
      "modifiedBy" : details.modifiedBy,
      "status":details.status,
      "typeId":details.typeId  
    }
    return this.httpClient.post<any>(environment.User_URL  + '/addRoleConfig', object, httpOptions);
  }
  /*
   * This method for get All Role Name List for Type head.
   */
  public getAllRoleNameList(){
    return this.httpClient.get<any>(environment.User_URL + '/getAllRoleNameList');
  }
 /*
   * This method for Delete role basis on role id list.
   */
  public deleteRole(roleIdList){
    var object = {
      "roleIds":roleIdList
    }
    return this.httpClient.post<any>(environment.User_URL + '/deleteRoleDataByRoleId', object, httpOptions);
  }

  public getAllOrgList(){
    return this.httpClient.get<any>(environment.Org_URL  + '/getAllOrgNameList');
  }
  
  
}
