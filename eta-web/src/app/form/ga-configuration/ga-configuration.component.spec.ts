import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GaConfigurationComponent } from './ga-configuration.component';

describe('GaConfigurationComponent', () => {
  let component: GaConfigurationComponent;
  let fixture: ComponentFixture<GaConfigurationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GaConfigurationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GaConfigurationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
