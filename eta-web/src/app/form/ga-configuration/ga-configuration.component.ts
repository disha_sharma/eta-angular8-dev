import { Component, OnInit } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { ServiceService } from './../../eta-services/service.service'

@Component({
  selector: 'app-ga-configuration',
  templateUrl: './ga-configuration.component.html',
  styleUrls: ['./ga-configuration.component.css']
})
export class GaConfigurationComponent implements OnInit {
  /***   For Sub Breadcrumb  **/
  public breadcrumb: String;
  /*** For Main Breadcrumb **/
  public mainbreadcrumb: string;
  checkbox :any;
  /** Read Ga URL form service  */
  public GA_URL:any;
  /** Ga Data URL */
  public gaUrl:any;
   /** GA Grid Column List */
  public gaUrlCol: any;
  /** Form Group  */
  public gafrm: FormGroup;

  constructor(private _formBuilder: FormBuilder,private http: HttpClient, private route: ActivatedRoute,
    private r: Router,private newService: ServiceService) {
      this.GA_URL=newService.Ga_Url;
      this.gaUrl=this.GA_URL+'/getGaDashboardGrid';
      //this.checkbox ='configId';
    this.getAllGaConfiguration();
  }

  /* gaUrlCol: any = [
    { data: 'check' , displayName: "Check Id",  hyperlink: false ,edit: false  },
    { data: 'orgId',  displayName: "Org Id", hyperlink: 'orgId', edit: false , hover  :'modifiedName'},
    { data: 'gaConfigId', displayName: "ga ConfigId", hyperlink: 'gaConfigId', edit: false  },
    { data: 'gaKey', displayName: "ga Key", hyperlink: 'gaKey', edit: false },
    { data: 'gaDomainName', displayName: "ga Domain Name", hyperlink: 'gaDomainName', edit: false }
  ]; */

  hypercol: any = [
   { check: 'link' }
 ];

  data: object;

  private getAllGaConfiguration() {
    let gaobj: Object = {};
    this.newService.getAllGaConfiguration()
      .subscribe(data => {
        this.data = data;
        console.log(data);
      });
  }

  refresh(): void {
    window.location.reload();
  }

  reloadWithNewId() {
    this.r.navigateByUrl('gaCongig');
  }

 /*  public setGaConfiguration() {
    this.refresh();
    let myForm = <HTMLFormElement>document.getElementById("gaData");
    let gaobj: Object = {};
    gaobj["gaKey"] = (<HTMLInputElement>document.getElementById("gaKey")).value;
    gaobj["gaDomainName"] = (<HTMLInputElement>document.getElementById("gaDomainName")).value;
    gaobj["orgId"] = '007';
    gaobj["createdBy"] = '01';
    gaobj["modifiedBy"] = '01';
    gaobj["activeYN"] = '1';
    this.newService.setGaConfiguration(gaobj)
  } */

  onSubmit() {
    let gaobj: Object = {};
    gaobj["gaKey"] = this.gafrm.value.gaKey;
    gaobj["gaDomainName"] = this.gafrm.value.gaDomainName;
    gaobj["orgId"] = this.gafrm.value.orgId;
    gaobj["gaConfigId"] = this.gafrm.value.gaConfigId;
    this.gaUrlCol = null;

    this.newService.setGaConfiguration(gaobj)
      .subscribe(data => {
      });
    
    }



  ngOnInit() {
    this.getGaColoumnList();
    this.gafrm = this._formBuilder.group({
      gaKey: [],
      gaDomainName: [],
      orgId: [],
      gaConfigId:[]
    })
    
  }

  private getGaColoumnList() {
    this.newService.getGaInnerColumn()
      .subscribe(res => {
        this.gaUrlCol = res.coloumnName;
      })
  }
}
