import { NgModel } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { ServiceService } from './../../../eta-services/service.service'

@Component({
  selector: 'app-create-data-container',
  templateUrl: './create-data-container.component.html',
  styleUrls: ['./create-data-container.component.css']
})

export class CreateDataContainerComponent implements OnInit {

  dataContainerUrl: any;
  Core_URL: any;
  createdatacontainer: FormGroup;
  formobj: Object = {};
  allDataObject: any;
  containerEditObj: any;
  isEdit = false;
  dropdownList = [];
  selectedItems = [];
  dropdownSettings = {};
  dataContainers = [];
  isDataContainersLoad = false;
  gridTranslationColumns: any;
  isDataLoaded = false;
  dataContainerTransUrl: any;

  get_Data: any;
  datacontainerId: any;
  gridDataContainerColumns: any;
  public defaultColumnToSort: any;
  public recordsTotal: number;

  constructor(private _formBuilder: FormBuilder, private http: HttpClient, private route: ActivatedRoute,
    private r: Router, private shareServices: ServiceService) {
    this.defaultColumnToSort = "dataContainerName"
    this.Core_URL = this.shareServices.Core_URL;
    this.dataContainerUrl = this.Core_URL + '/getAllInnerDataContainerByOrgIdGrid';
    this.getAllDataObject();
    this.getHeaderMenuList();
  }

  ngOnInit() {
    this.getDataContainerColoumnList();
    this.getAllDC();
    this.shareServices.currentMessage.subscribe(message => this.containerEditObj = message);
    this.createdatacontainer = this._formBuilder.group({
      dataobject: [],
      createcontainername: [],
      descriptiontextarea: [],
      selectedConatinerValue: []
    })

    /** only for edit */
    if (this.containerEditObj != null && this.containerEditObj != 'editFalse') {
      console.log(this.containerEditObj);

      this.isEdit = true;
      this.createdatacontainer.patchValue({ selectedConatinerValue: this.containerEditObj.dataObjectIds });
      this.createdatacontainer.patchValue({ createcontainername: this.containerEditObj.dataContainerName });
      this.createdatacontainer.patchValue({ descriptiontextarea: this.containerEditObj.dataContainerDescription });
      this.datacontainerId = this.containerEditObj.dataContainerID;


    }
    else {
      this.isEdit = false;
    }
  }
  public getAllDC() {
    let listData;
    this.http.get(this.Core_URL + '/getAllDataObject').subscribe(data => {
      listData = data;
      listData.forEach(element => {
        let DO: any = {};
        DO.item_id = element.dataObjectID;
        DO.item_text = element.dataObjectName;
        this.dataContainers.push(DO);
      });
      if (this.isEdit == true) {
        let dataContainsers;
        this.http.get(this.Core_URL + '/getDataObjectBydataContainerId/' + this.containerEditObj.dataContainerID)
          .subscribe(data => {
            dataContainsers = data;
            this.dataContainers.forEach(el => {
              dataContainsers.forEach(element => {
                if (element.dataObjectId == el.item_id) {
                  this.selectedItems.push(el);
                }
              });
            });
            this.isDataContainersLoad = true;
          });
      } else {
        this.isDataContainersLoad = true;
      }
    });
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'item_id',
      textField: 'item_text',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      //itemsShowLimit: 3,
      allowSearchFilter: true
    };
  }

  public setAllSelectedDC() {
    this.selectedItems = [
      { item_id: 1, item_text: "data" }
    ];
  }

  public getIdsOfSelectedItem() {
    let ids = [];
    this.selectedItems.forEach(element => {
      ids.push(element.item_id);
    });
    return ids;
  }

  newForm() {
    this.clearShareObj();
    this.r.navigateByUrl('createdataform');
  }

  onSubmit() {

    if (this.isEdit) {
      this.containerEditObj.dataContainerName = this.createdatacontainer.value.createcontainername;
      this.containerEditObj.dataContainerDescription = this.createdatacontainer.value.descriptiontextarea;
      let datacontainer = this.createdatacontainer.value.datacontainer;
      this.containerEditObj.dataObjectIds = this.createdatacontainer.value.selectedConatinerValue;

      this.http.post(this.Core_URL + '/updateDataContainer/', this.containerEditObj)
        .subscribe(data => {
          this.get_Data = data;
          this.datacontainerId = this.get_Data.dataContainerID;
          if (this.datacontainerId != null) {
            alert("Updated successfully !! Please add translations")
            this.gridDataContainerColumns = null;
            this.getDataContainerColoumnList();
          } else {
            alert("data not saved")
          }
        });
    }
    else {
      this.formobj["dataContainerName"] = this.createdatacontainer.value.createcontainername
      this.formobj["dataContainerDescription"] = this.createdatacontainer.value.descriptiontextarea
      this.formobj["dataObjectIds"] = this.createdatacontainer.value.selectedConatinerValue;

      console.log("form obj");
      
      console.log(this.formobj);
      console.log(this.createdatacontainer.value.selectedConatinerValue);
      
      

      this.http.post(this.Core_URL + '/updateDataContainer/', this.formobj)
        .subscribe(data => {
          this.get_Data = data;
          this.datacontainerId = this.get_Data.dataContainerID;
          if (this.datacontainerId != null) {
            alert("Updated successfully !! Please add translations")
            this.clearShareObj();
            this.gridDataContainerColumns = null;
            this.getDataContainerColoumnList();
          } else {
            alert("data not saved")
          }
        });
    }
  }

  private getAllDataObject() {
    this.http.get(this.Core_URL + '/getAllDataObject')
      .subscribe(data => {
        this.allDataObject = data;
      });
  }

  private clearShareObj() {
    this.shareServices.setgridChangeMessage(null);
  }
  public cancel() {
    this.clearShareObj();
    this.r.navigateByUrl('datacontainerdashboard');
  }

  public translationColumns() {
    if (this.datacontainerId != null) {
      let coloumNames: any;
      let myColoumsObj: any;
      this.http.get(this.Core_URL + '/getCoreGridHeaderLanguageByOrgId')
        .subscribe(data => {
          myColoumsObj = data;
          coloumNames = myColoumsObj.coloumnName;
          if (Object.keys(coloumNames).length > 0) {
            this.gridTranslationColumns = coloumNames;
          }
          this.isDataLoaded = true;
          this.dataContainerTransUrl = this.Core_URL + '/getDataContainerLanguageInfo/' + this.datacontainerId;
        });
    } else {
      alert("please add container");
    }
  }

  public getTranslationEditObject(event: any) {
    let translationEditObj = {};
    translationEditObj["id"] = event.obj.id;
    translationEditObj["versionId"] = event.obj.versionId;
    translationEditObj["languageId"] = event.languageId;
    translationEditObj["orgId"] = event.obj.id;
    translationEditObj["columnName"] = event.obj.field;
    translationEditObj["columnValue"] = event.newEditValue;
    this.http.post(this.Core_URL + '/updateContainerLanguage', translationEditObj)
      .subscribe(data => {
      });
  }

  public closePopUp() {
    this.isDataLoaded = false;
  }

  private getDataContainerColoumnList() {
    this.shareServices.getCoreInnerDataContainerColumn()
      .subscribe(data => {
        this.gridDataContainerColumns = data.coloumnName;
        this.recordsTotal = data.recordsTotal;
      });
  }
  public mainbreadcrumb: string;
  public breadcrumb: string;
  private menuList: any = [];
  getHeaderMenuList() {
    this.shareServices.getHeaderMenu()
      .subscribe(res => {
        // console.log(res);
        this.menuList = res;
        this.menuList.forEach(element => {
          if (element.routerLink == "eta-web/dashboardDataObject") {
            this.mainbreadcrumb = element.translation;
          }
          if (element.routerLink == "eta-web/datacontainerdashboard") {
            this.breadcrumb = element.translation;
          }
        })

      })
  }

  selectAll(list) {
    console.log(list);
    console.log(this.createdatacontainer.get('selectedConatinerValue'));
    let tempList: any = [];
    list.forEach(element => {
      tempList.push(element.item_id);
    });
    console.log(tempList);

    this.createdatacontainer.get('selectedConatinerValue').patchValue(tempList)
  }
  deselectAll() {
    this.createdatacontainer.get('selectedConatinerValue').patchValue([])
  }



}
