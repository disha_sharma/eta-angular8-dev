import { Component, OnInit } from '@angular/core';
import { DialogData } from '../../../popup-template/popup-template.component';
import { Inject} from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-review-form',
  templateUrl: './review-form.component.html',
  styleUrls: ['./review-form.component.css']
})
export class ReviewFormComponent implements OnInit {

  public currentTaskJson : any;
  formName : string = "";
  userFormData: FormGroup;
  withOutValueArray : any = [];
  valueArray : any = [];
  
  constructor(public dialogRef: MatDialogRef<ReviewFormComponent>,
    @Inject(MAT_DIALOG_DATA) data,private fb: FormBuilder) { 
      this.userFormData = this.fb.group({
        // userPhoto: []
       })
      this.formName = data.formName;
      this.currentTaskJson = data.regConfig;
      for(var i=0;i<this.currentTaskJson.length;i++){
        if(this.currentTaskJson[i].value){
          this.valueArray.push(this.currentTaskJson[i]);
        }else {
          this.withOutValueArray.push(this.currentTaskJson[i]);
          console.log('this.withOutValueArray',this.withOutValueArray);
          this.withOutValueArray.forEach(field => {
            const control = this.fb.control(
              field.value,
              this.bindValidations(field.validations || [])
            );
            if (field.htmlControlTypeName != 'file' && field.controlName != 'button') {
              this.userFormData.addControl(field.labelID, control);
            }
          });
        }
      }
      console.log('this.valueArray',this.valueArray);
     
    }

  ngOnInit() {
   // this.createControl();
    this.userFormData = this.fb.group({
     // userPhoto: []
    })
  }

  createControl() {
    const group = this.fb.group({});
    this.withOutValueArray.forEach(field => {
      if (field.controlName === "button") return;
      const control = this.fb.control(
        field.value,
        this.bindValidations(field.validations || [])
      );
      group.addControl(field.labelID, control);
    });
    return group;
  }
  bindValidations(validations: any) {
    if (validations.length > 0) {
      const validList = [];
      validations.forEach(valid => {
        validList.push(valid.validator);
      });
      return Validators.compose(validList);
    }
    return null;
  }


  

}
