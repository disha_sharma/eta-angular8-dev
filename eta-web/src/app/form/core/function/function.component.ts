import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { ServiceService } from './../../../eta-services/service.service'

@Component({
  selector: 'app-function',
  templateUrl: './function.component.html',
  styleUrls: ['./function.component.css']
})

export class FunctionComponent implements OnInit {

  Core_URL: any;
  addfrm: FormGroup;
  formobj: Object = {};
  gridFunctionData: any;
  isDataLoadedFunction = false;

  gridTranslationColumns: any;
  isDataLoaded = false;
  functionUrl: any;
  functionSubmitUrl: any;
  functionTranscol: any = [];
  functionTransUrl: any;
  functionTransSubmitUrl: any;
  functionObj: any = null;
  isEdit = false;
  get_FunctionId: any;
  gridFunctionColumns: any;
  selectedFiles: FileList;
  currentFileUpload: File;
  imageName: any;
  getlogo: any;
  getFunctionFileName: any;
  key: any = ["languageID", "functionTranslation", "functionDesTranslation"];
  public defaultColumnToSort: any;
  public recordsTotal: number;


  constructor(private _formBuilder: FormBuilder, private http: HttpClient, private route: ActivatedRoute,
    private r: Router, private shareServices: ServiceService) {
    this.defaultColumnToSort = "functionName"
    this.Core_URL = this.shareServices.Core_URL;
    this.functionUrl = this.Core_URL + '/getAllInnerFunctionsByOrgIdGrid';
    this.functionSubmitUrl = this.Core_URL + '/updateFunction';
    this.getHeaderMenuList();
  }

  ngOnInit() {
    this.getFunctionColoumnList();
    ////console.log("_______________ Function  Call___________");
    this.shareServices.currentMessage.subscribe(message => this.functionObj = message);
    ////console.log(this.functionObj);
    this.addfrm = this._formBuilder.group({
      functionName: [],
      imageName: [],
      functioinDes: [],
      search: []
    })

    /** only for edit */
    if (this.functionObj != null && this.functionObj != 'editFalse') {
      this.addfrm.patchValue({ functionName: this.functionObj.functionName });
      this.addfrm.patchValue({ functioinDes: this.functionObj.functionDescription });
      this.get_FunctionId = this.functionObj.functionID;
      this.isEdit = true;
      //this.functionTransUrl =  this.Core_URL+'/getFunctionLanguageInfo/'+this.get_FunctionId;
    }
    else {
      this.isEdit = false;
    }
  }

  public onSubmit() {


    if (this.isEdit) {
    
      let formdata: FormData = new FormData();
      formdata.append('functionName', this.addfrm.value.functionName);
      formdata.append('functionDescription', this.addfrm.value.functioinDes);
      formdata.append('functionImage', this.functionObj.functionImage);
      if (this.imageName != undefined) {
        alert("null");
        formdata.append('functionImage', this.imageName);
        formdata.append('file', this.currentFileUpload);
      }
      if (this.imageName == undefined) {
        formdata.append('functionImage', this.functionObj.functionImage);
      }
      formdata.append('functionID', this.functionObj.functionID);
      formdata.append('activeYN', this.functionObj.activeYN);


      this.http.post(this.Core_URL + '/updateFunction', formdata)
        .subscribe(data => {
          let get_Data: any = data;
          this.get_FunctionId = get_Data.functionID;
          this.getFunctionFileName = get_Data.functionImage;
          if (this.get_FunctionId != null) {

            alert("Updated successfully !! Please add translations");
            this.gridFunctionColumns = null;
            this.getFunctionColoumnList();
          } else {
            alert("data not saved")
          }
        }, error=>{
          this.getFunctionColoumnList();
        });
    } else {

      let formdata: FormData = new FormData();
      console.log(this.currentFileUpload);

      formdata.append('functionName', this.addfrm.value.functionName);
      formdata.append('functionDescription', this.addfrm.value.functioinDes);
      formdata.append('functionImage', this.imageName);
      formdata.append('file', this.currentFileUpload);
      /* this.formobj["functionName"] = this.addfrm.value.functionName;
      this.formobj["functionDescription"] = this.addfrm.value.functioinDes;
      this.formobj['functionImage'] = this.imageName; */
      this.http.post(this.Core_URL + '/updateFunction', formdata)
        .subscribe(data => {
          let get_Data: any = data;
          this.get_FunctionId = get_Data.functionID;
          this.getFunctionFileName = get_Data.functionImage;
          if (this.get_FunctionId != null) {
            /*  if (this.selectedFiles != null && this.selectedFiles.length > 0) {
               this.currentFileUpload = this.selectedFiles.item(0);
               let formdata: FormData = new FormData();
               formdata.append('file', this.currentFileUpload);
               this.shareServices.functionProfileImageUplaod(this.currentFileUpload, this.get_FunctionId, this.getFunctionFileName)
                 .subscribe(data => {
                 })
             } */
            this.clearShareObj();
            alert("Updated successfully !! Please add translations")
           this.gridFunctionColumns = null;
            this.getFunctionColoumnList();
          } else {
            alert("data not saved")
          }

        }, error=>{
          this.getFunctionColoumnList();
        });
    }
  }

  public selectFile(event) {
    this.imageName = event.target.files[0].name;
    this.selectedFiles = event.target.files;
    this.currentFileUpload = this.selectedFiles.item(0);
  }

  public cancel() {
    this.clearShareObj();
    this.r.navigateByUrl('home');
  }

  public translationColumns() {
    if (this.get_FunctionId != null) {
      let coloumNames: any;
      let myColoumsObj: any;
      this.http.get(this.Core_URL + '/getCoreGridHeaderLanguageByOrgId')
        .subscribe(data => {
          console.log(data);

          this.isDataLoaded = true;
          myColoumsObj = data;
          coloumNames = myColoumsObj.coloumnName;
          if (Object.keys(coloumNames).length > 0) {
            this.gridTranslationColumns = coloumNames;
          }
          this.functionTransUrl = this.Core_URL + '/getFunctionLanguageInfo/' + this.get_FunctionId;
        });
    } else {
      alert("please add function");
    }
  }

  private clearShareObj() {
    this.shareServices.setgridChangeMessage(null);
  }


  public getTranslationEditObject(event: any) {
    let translationEditObj = {};
    translationEditObj["id"] = event.obj.id
    translationEditObj["versionId"] = event.obj.versionId
    translationEditObj["languageId"] = event.languageId
    translationEditObj["orgId"] = event.obj.id
    translationEditObj["columnName"] = event.obj.field
    translationEditObj["columnValue"] = event.newEditValue
    this.http.post(this.Core_URL + '/updateFunctionLanguage', translationEditObj)
      .subscribe(data => {
      });
  }

  public closePopUp() {
    this.isDataLoaded = false;
  }

  private getFunctionColoumnList() {
    this.shareServices.getCoreInnerFunctionColumn()
      .subscribe(data => {
        console.log(data);

        this.gridFunctionColumns = data.coloumnName;
        this.recordsTotal = data.recordsTotal;


      });
  }
  /* method to get braedcrumb*/

  public mainbreadcrumb: string;
  public breadcrumb: string;
  private menuList: any = [];
  getHeaderMenuList() {
    this.shareServices.getHeaderMenu()
      .subscribe(res => {
        //console.log(res);
        this.menuList = res;

        this.menuList.forEach(element => {
          if (element.routerLink == "eta-web/home") {
            this.mainbreadcrumb = element.translation;
          }
          if (element.routerLink == "eta-web/home") {
            this.breadcrumb = element.translation;
          }
        })

      })
  }
  /* method to get braedcrumb end*/
}
