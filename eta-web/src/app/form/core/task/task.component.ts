import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { Router } from '@angular/router';
import { PopupTemplateComponent } from '../../../popup-template/popup-template.component';
import { ServiceService } from './../../../eta-services/service.service';
@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.css']
})
export class TaskComponent implements OnInit {

  Core_URL: any;
  taskfrm: FormGroup;
  taskObj: Object = {};
  privilegeObj: Object = {};
  allFrom: any;
  allProcess: any;
  allTask: any;
  allTaskType: any;
  subTaskType: any;
  taskEditObj: any;
  isEdit = false;
  subTaskValue: any;
  gridTranslationColumns: any;
  isDataLoaded = false;
  taskTransUrl: any;
  get_Data: any;
  taskId: any;
  privilegeId: any;
  formDropDownDisabled: boolean = false;
  gridTaskColumns: any;


  taskUrl: any;
  public defaultColumnToSort: any;
  public recordsTotal: number;

  /* Testing Only */
  dataElementList: any = [];
  relationalOpraters: any = [];
  showConstantTextBox1: any;
  showConstantTextBox2: any;

  repeatingBlockConstantValue1: any;
  repeatingBlockConstantValue2: any;
  currentIndex: any;


  taskName: any;

  /**
   *For Two Way router Conditions
   *
   * @type {*}
   * @memberof TaskComponent
   */
  public twoWayTaskCondition: number;
  /** EmailTemplate Name List */
  templateNameList: any = [];


  /* Mat Chip veriable */
  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = true;
  readonly separatorKeysCodes: number[] = [ENTER, COMMA];
  splitter_task: any = [];
  splitter_task_id: any = [];

  joinner_task: any = [];
  joinner_task_id: any = [];

  taskListArray: any = [12, 13, 14];
  constructor(private _formBuilder: FormBuilder, private http: HttpClient, private shareServices: ServiceService, private r: Router, public dialog: MatDialog) {
    this.defaultColumnToSort = "taskName"
    this.Core_URL = this.shareServices.Core_URL;
    this.taskUrl = this.Core_URL + '/getAllInnerTaskByOrgIdGrid';
    this.getHeaderMenuList();
    this.getAllDataElement();
  }

  ngOnInit() {
    this.getTaskColoumnList();
    this.getAllProcess();
    this.getAllTaskType();
    this.getEmailTemplateNameInfo();
    this.shareServices.currentMessage.subscribe(message => this.taskEditObj = message);


    /* this.taskListArray.forEach(element => {
      this.shareServices.getTaskByTaskId(element)
        .subscribe(res => {
          console.log(res);

          const value = res.taskName;
          if ((value || '').trim()) {
            this.splitter_task.push({ name: value.trim(), id: res.taskID });
          }
        });

    }); */





    this.taskfrm = this._formBuilder.group({
      tasktitle: [],
      taskdes: [],
      previlege: [],
      previlegename: [],
      previlegedes: [],
      process: [],
      previoustask: [],
      type: [],
      subtype: [],
      form: [],
      lasttask: [],
      firstDataElement: [],
      relationalOpraters: [],
      secondDataElement: [],
      /* Task IF/ELSE conditions */
      taskTrue: [],
      taskFalse: [],
      templateName: [],
      OtherConstant1: [],
      OtherConstant2: [],
      businessRuleTask :[],
      bRuleSecondDataElement :[],
      bRuleRelationalOpraters : [],
      bRuleFirstDataElement: [],
      bRuleOtherConstant2:[],
      businessRuleElseTask:[],

      /* For Repeating Block */
      itemRows: this._formBuilder.array([this.initItemRows()]),
      businessRuleRow: this._formBuilder.array([this.initBusinessRuleRows()])

    });
    this.taskfrm.setControl('itemRows', this._formBuilder.array([]));

    /** only for edit */
    if (this.taskEditObj != null && this.taskEditObj != 'editFalse') {
      this.isEdit = true;
      this.subTaskValue = this.taskEditObj.subTaskTypeID;
      this.taskfrm.patchValue({ tasktitle: this.taskEditObj.taskName });
      this.taskfrm.patchValue({ taskdes: this.taskEditObj.taskDescription });
      this.taskfrm.patchValue({ previlege: this.taskEditObj.previlege });
      this.taskfrm.patchValue({ previlegename: this.taskEditObj.privilegeName });
      this.taskfrm.patchValue({ previlegedes: this.taskEditObj.privilegeDescription });
      this.taskfrm.patchValue({ process: this.taskEditObj.processID });
      this.taskfrm.patchValue({ type: this.taskEditObj.taskTypeID });
      this.taskfrm.patchValue({ subtype: this.taskEditObj.subTaskTypeID });
      this.taskfrm.patchValue({ form: this.taskEditObj.formId });
      this.taskfrm.patchValue({ lasttask: this.taskEditObj.isStartOrEntTask });
      this.getAllTaskByProcessId(this.taskEditObj.processID);
      this.taskfrm.patchValue({ previoustask: this.taskEditObj.previousTaskID });
      this.taskId = this.taskEditObj.taskID;
      this.privilegeId = this.taskEditObj.privilegeId;


    }
    else {
      this.isEdit = false;
    }
  }

  initItemRows() {
    return this._formBuilder.group({
      // list all your form controls here, which belongs to your form array
      relationalConditionRow: [],
      firstDataElementRow: [],
      relationalOpratersRow: [],
      secondDataElementRow: [],
      OtherConstant1: [],
      OtherConstant2: []
    });
  }
  initBusinessRuleRows() {
    return this._formBuilder.group({
      // list all your form controls here, which belongs to your form Business rule array
      bRulefirstDataElementRow: [],
      bRuleRelationalOpratersRow: [],
      bRuleSecondDataElementRow: [],
      businessRuleTaskRow: [],
      bRuleOtherConstantRow: [],
      OtherConstant2: [],
    });
  }

  addNewRow() {
    // control refers to your formarray
    const control = <FormArray>this.taskfrm.controls['itemRows'];
    // add new formgroup
    control.push(this.initItemRows());
  }
  deleteRow(index: number) {
    // control refers to your formarray
    const control = <FormArray>this.taskfrm.controls['itemRows'];
    // remove the chosen row
    control.removeAt(index);
  }


  public getAllProcess() {
    this.http.get(this.Core_URL + '/getAllProcess')
      .subscribe(data => {
        this.allProcess = data;
      });
  }
  public getAllForm() {
    this.http.get(this.Core_URL + '/getAllForm')
      .subscribe(data => {
        this.allFrom = data;
        let formList: any = [];
        formList = data;
        if (formList.length == 0) {
          alert("No form avilable!!")
        }
      });
  }
  public getAllTaskType() {
    this.http.get(this.Core_URL + '/getAllTaskType')
      .subscribe(data => {
        this.allTaskType = data;
        if (this.isEdit) {
          this.taskChange(this.taskEditObj.taskTypeID);
          this.taskfrm.patchValue({ subtype: this.taskEditObj.subTaskTypeID });

        }
      });
  }

  public taskChange(changevalue: any) {
    if (changevalue == 1) {
      this.formDropDownDisabled = false;
      this.getAllForm();
    } else {
      this.formDropDownDisabled = true;
      this.allFrom = null;
    }
    this.allTaskType.forEach(element => {
      if (element.taskTypeID == changevalue) {
        this.subTaskType = element.subTaskTypeDAO;
        console.log( this.subTaskType);
        
      }
    });
  }

  public cancel() {
    this.clearShareObj();
    this.r.navigateByUrl('taskdashboard');
  }


  processId: number;
  public getAllTaskByProcessId(processId: number) {

    this.processId = processId;
    this.http.get(this.Core_URL + '/getAllTaskByProcessId/' + processId)
      .subscribe(data => {
        let taskList: any = [];
        taskList = data;

        if (taskList.length == 0) {
          alert('No task available');
        }
        this.allTask = data;
        console.log(data);

      });
  }

  onSubmit = function (taskValue) {

    /**
   * To Store 2way task Array list
   *
   * @type {*}
   * @memberof TaskComponent
   */
    let task2WayArray = [];
    let businessRuleArray=[];


    if (this.isEdit) {
      this.taskEditObj.taskName = this.taskfrm.value.tasktitle;
      this.taskEditObj.taskDescription = this.taskfrm.value.taskdes;
      this.taskEditObj.previlege = this.taskfrm.value.previlege;
      this.taskEditObj.privilegeName = this.taskfrm.value.previlegename;
      this.taskEditObj.privilegeDescription = this.taskfrm.value.previlegedes;
      this.taskEditObj.processID = this.taskfrm.value.process;
      this.taskEditObj.previousTaskID = this.taskfrm.value.previoustask;
      this.taskEditObj.taskTypeID = this.taskfrm.value.type;
      this.taskEditObj.subTaskTypeID = this.taskfrm.value.subtype;
      this.taskEditObj.formId = this.taskfrm.value.form;
      this.taskEditObj.isStartOrEntTask = this.taskfrm.value.lasttask;

      console.log(this.taskEditObj);

      this.http.post(this.Core_URL + '/saveTask/', this.taskEditObj)
        .subscribe(data => {
          this.get_Data = data;
          this.taskId = this.get_Data.taskID;
          this.privilegeId = this.get_Data.privilegeId;
          if (this.taskId != null) {
            alert("Updated successfully !! Please add translations");
            this.gridTaskColumns = null;
            this.getTaskColoumnList();
          } else {
            alert("data not saved")
          }
        });
    }
    else {
      console.log(taskValue);

      let twoWayBindingObj: any = {};

      /* Reading First Two block for 2 way tasks */
      twoWayBindingObj.logicalOperators = "";
      twoWayBindingObj.expression1 = taskValue.firstDataElement;
      twoWayBindingObj.relationalOperators = taskValue.relationalOpraters;
      twoWayBindingObj.expression2 = taskValue.secondDataElement;
      twoWayBindingObj.otherConstant1 = this.taskfrm.value.OtherConstant1;
      twoWayBindingObj.otherConstant2 = this.taskfrm.value.OtherConstant2;
      task2WayArray.push(twoWayBindingObj)

      /* Reading values from repeating block */
      taskValue.itemRows.forEach(element => {
        let twoWayBindingObjInner: any = {};
        twoWayBindingObjInner.logicalOperators = element.relationalConditionRow;
        twoWayBindingObjInner.expression1 = element.firstDataElementRow;
        twoWayBindingObjInner.relationalOperators = element.relationalOpratersRow;
        twoWayBindingObjInner.expression2 = element.secondDataElementRow;

        task2WayArray.push(twoWayBindingObjInner)

      });

      

      let businessRuleObj: any={};
      businessRuleObj.busnessTask = taskValue.businessRuleTask;
      businessRuleObj.expression1 = taskValue.bRuleFirstDataElement;
      businessRuleObj.relationalOperators = taskValue.bRuleRelationalOpraters;
      businessRuleObj.expression2 = taskValue.bRuleSecondDataElement;
      businessRuleObj.otherConstant1 = this.taskfrm.value.OtherConstant1;
      businessRuleObj.otherConstant2 = this.taskfrm.value.bRuleOtherConstant2;
      businessRuleArray.push(businessRuleObj)

      taskValue.businessRuleRow.forEach(element => {

        let businessRuleInnerObj:any ={};
        businessRuleInnerObj.busnessTask = element.businessRuleTaskRow;
        businessRuleInnerObj.expression1 = element.bRulefirstDataElementRow;
        businessRuleInnerObj.relationalOperators = element.bRuleRelationalOpratersRow;
        businessRuleInnerObj.expression2 = element.bRuleSecondDataElementRow;
        businessRuleArray.push(businessRuleInnerObj)
      });



      console.log(task2WayArray);

      /* Splitter Task -- Task Id Array */
      let allSplitterTaskId: any = [];
      this.splitter_task.forEach(element => {
        let splitterTask: any = {};
        splitterTask = element.id;

        allSplitterTaskId.push(splitterTask)
      });

      let joinnerTask: any;
      this.joinner_task.forEach(element => {
        joinnerTask = element.id;
      });


      this.taskObj["taskName"] = this.taskfrm.value.tasktitle;
      this.taskObj["taskDescription"] = this.taskfrm.value.taskdes;
      this.taskObj["previlege"] = this.taskfrm.value.previlege;
      this.taskObj["privilegeName"] = this.taskfrm.value.previlegename;
      this.taskObj["privilegeDescription"] = this.taskfrm.value.previlegedes;
      this.taskObj["processID"] = this.taskfrm.value.process;
      this.taskObj["previousTaskID"] = this.taskfrm.value.previoustask;
      this.taskObj["taskTypeID"] = this.taskfrm.value.type;
      this.taskObj["subTaskTypeID"] = this.taskfrm.value.subtype;
      this.taskObj["formId"] = this.taskfrm.value.form;
      this.taskObj["isStartOrEntTask"] = this.taskfrm.value.lasttask;
      this.taskObj["twoWayRepeating"] = task2WayArray;
      this.taskObj["taskTrue"] = taskValue.taskTrue;
      this.taskObj["taskFalse"] = taskValue.taskFalse;
      this.taskObj["templateName"] = taskValue.templateName;
      this.taskObj["splitterArray"] = allSplitterTaskId;
      this.taskObj["joinerTask"] = joinnerTask;
      this.taskObj["businessRuleArray"] = businessRuleArray;
      this.taskObj["businessRuleElseTask"] = taskValue.businessRuleElseTask;
      

      console.log(this.taskObj);

      this.http.post(this.Core_URL + '/saveTask/', this.taskObj)
        .subscribe(data => {
          this.get_Data = data;
          this.taskId = this.get_Data.taskID;
          this.privilegeId = this.get_Data.privilegeId;
          if (this.taskId != null) {
            this.clearShareObj();
            alert("Updated successfully !! Please add translations");
            this.getAllTaskByProcessId(this.processId);
            this.gridTaskColumns = null;
            this.getTaskColoumnList();
          } else {
            alert("data not saved");
          }
        });
    }
  }
  private clearShareObj() {
    this.shareServices.setgridChangeMessage(null);
  }

  public translationColumns() {
    if (this.taskId != null) {
      let coloumNames: any;
      let myColoumsObj: any;
      this.http.get(this.Core_URL + '/getCoreGridHeaderLanguageByOrgId')
        .subscribe(data => {
          myColoumsObj = data;
          coloumNames = myColoumsObj.coloumnName;
          if (Object.keys(coloumNames).length > 0) {
            this.gridTranslationColumns = coloumNames;
          }
          this.isDataLoaded = true;
          this.taskTransUrl = this.Core_URL + '/getTaskLanguageInfo/' + this.taskId + '/' + this.privilegeId;
        });
    } else {
      alert("please add task");
    }
  }

  public getTranslationEditObject(event: any) {
    let translationEditObj = {};
    translationEditObj["id"] = event.obj.id;
    translationEditObj["versionId"] = event.obj.versionId;
    translationEditObj["languageId"] = event.languageId;
    translationEditObj["orgId"] = event.obj.id;
    translationEditObj["columnName"] = event.obj.field;
    translationEditObj["columnValue"] = event.newEditValue;
    if (event.obj.field === 'Privilege' || event.obj.field === 'Privilege Description') {
      this.http.post(this.Core_URL + '/updatePrivilegeLanguage', translationEditObj)
        .subscribe(data => {
        });
    } else {

      this.http.post(this.Core_URL + '/updateTaskLanguage', translationEditObj)
        .subscribe(data => {
        });
    }
  }

  public closePopUp() {
    this.isDataLoaded = false;
  }

  public getTaskColoumnList() {
    this.shareServices.getCoreInnerTaskColumn()
      .subscribe(data => {
        this.gridTaskColumns = data.coloumnName;
        this.recordsTotal = data.recordsTotal;
      });
  }

  public mainbreadcrumb: string;
  public breadcrumb: string;
  public menuList: any = [];
  getHeaderMenuList() {
    this.shareServices.getHeaderMenu()
      .subscribe(res => {
        //console.log(res);
        this.menuList = res;

        this.menuList.forEach(element => {
          if (element.routerLink == "eta-web/home") {
            this.mainbreadcrumb = element.translation;
          }
          if (element.routerLink == "eta-web/taskdashboard") {
            this.breadcrumb = element.translation;
          }
        })

      })
  }

  public getAllDataElement() {
    this.http.get(this.Core_URL + '/getAllDataElementByOrgId')
      .subscribe(data => {
        this.dataElementList = data;
      });
    this.http.get(this.Core_URL + '/getAllSpdOperators')
      .subscribe(data => {
        this.relationalOpraters = data;
      });
  }


  getEmailTemplateNameInfo() {
    this.shareServices.getAllTemplateByOrgId()
      .subscribe(res => {
        console.log(res);
        this.templateNameList = res;
        if (this.templateNameList == null || this.templateNameList.length == 0) {
          alert("No Template Found!! please add notification template");
        }

      })
  }


  openDialog(): void {

    /*  const dialogRef = this.dialog.open(PopupTemplateComponent, {
       width: '80% auto',
       data: { taskId: this.taskId, taskName: this.taskName }
     }); */

    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    dialogConfig.data = {
      id: 1,
      title: 'Angular For Beginners'
    };

    const dialogRef = this.dialog.open(PopupTemplateComponent, dialogConfig);

    dialogRef.afterClosed()
      .subscribe(result => {
        console.log('The dialog was closed' + result);
        console.log(result);
        // Add our fruit
        this.shareServices.getTaskByTaskId(result)
          .subscribe(res => {
            const value = res.taskName;
            if ((value || '').trim()) {
              this.splitter_task.push({ name: value.trim(), id: res.taskID });
              // this.splitter_task_id.push({id:res.id});
            }
          }, error => {
            alert("No Splitter Task Added !!!")
          })
      });
  }

  /*  add(event: MatChipInputEvent): void {
     const input = event.input;
     const value = event.value;
 
     // Add our fruit
     if ((value || '').trim()) {
       this.splitter_task.push({ name: value.trim() });
     }
 
     // Reset the input value
     if (input) {
       input.value = '';
     }
   } */
  remove(task: any): void {
    const index = this.splitter_task.indexOf(task);

    if (index >= 0) {
      this.splitter_task.splice(index, 1);
    }
  }



  /*  getEmailTemplateNameInfo()  {
     this.shareServices.getAllTemplateByOrgId()
       .subscribe(res  =>  {
         console.log(res);
         this.templateNameList  =  res;
         if(this.templateNameList== null || this.templateNameList.length ==0){
           alert("No Template Found!! please add notification template");
         }
 
       })
   } */

  subTaskChange(event) {
    console.log(event);
    this.subTaskValue = event;
  }


  openFutureTaskDialog(): void {

    /*  const dialogRef = this.dialog.open(PopupTemplateComponent, {
       width: '80% auto',
       data: { taskId: this.taskId, taskName: this.taskName }
     }); */

    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    dialogConfig.data = {
      id: 1,
      title: 'Angular For Beginners'
    };

    const dialogRef = this.dialog.open(PopupTemplateComponent, dialogConfig);

    dialogRef.afterClosed()
      .subscribe(result => {
        console.log('The dialog was closed' + result);
        console.log(result);
        // Add our fruit
        this.shareServices.getTaskByTaskId(result)
          .subscribe(res => {
            const value = res.taskName;
            if ((value || '').trim()) {
              this.joinner_task.push({ name: value.trim(), id: res.taskID });
              // this.splitter_task_id.push({id:res.id});
            }
          }, error => {
            alert("No Joinner Task Added !!!")
          })
      });
  }


  OtherConstantValue(constantValue) {
    //  alert(constantValue)
    this.showConstantTextBox1 = constantValue;
  }
  OtherConstantValue1(constantValue) {
    // alert(constantValue)
    this.showConstantTextBox2 = constantValue;

  }

  repeatingOtherConstantValue1(constantValue, index) {

    this.currentIndex = index;
    this.repeatingBlockConstantValue1 = constantValue

  }
  repeatingOtherConstantValue2(constantValue, index) {
    console.log(constantValue + "and  index value :" + index);


    this.currentIndex = index;
    this.repeatingBlockConstantValue2 = constantValue

  }


  openTaskTrueDialog(): void {

    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    const dialogRef = this.dialog.open(PopupTemplateComponent, dialogConfig);

    dialogRef.afterClosed()
      .subscribe(result => {
        this.shareServices.getTaskByTaskId(result)
          .subscribe(res => {
            const value = res.taskName;
            if ((value || '').trim()) {
              this.getAllTaskByProcessId(this.processId);
              this.taskfrm.patchValue({ taskTrue: res.taskID });
            }
          }, error => {
            alert("No  Task Added !!!")
          })
      });
  }


  openTaskFalseDialog(): void {

    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    const dialogRef = this.dialog.open(PopupTemplateComponent, dialogConfig);

    dialogRef.afterClosed()
      .subscribe(result => {
        this.shareServices.getTaskByTaskId(result)
          .subscribe(res => {
            const value = res.taskName;
            if ((value || '').trim()) {
              this.getAllTaskByProcessId(this.processId);
              this.taskfrm.patchValue({ taskFalse: res.taskID });

            }
          }, error => {
            alert("No  Task Added !!!")
          })
      });
  }

  addNewBusinessRule() {
    // control refers to your formarray
    const control = <FormArray>this.taskfrm.controls['businessRuleRow'];
    // add new formgroup
    control.push(this.initBusinessRuleRows());
  }
  deleteBusinessRule(index: number) {
    // control refers to your formarray
    const control = <FormArray>this.taskfrm.controls['businessRuleRow'];
    // remove the chosen row
    control.removeAt(index);
  }

}


