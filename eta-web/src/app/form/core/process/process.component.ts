
import { Component, OnInit, ViewChild, Inject, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validator, Validators } from '@angular/forms';
import { HttpClient, HttpResponse, HttpEventType } from '@angular/common/http';
import { ServiceService } from './../../../eta-services/service.service';
import { DashboardServiceService } from './../../../dash-board/dashboard-service.service';
import { ProcessDetails } from './Process';
import { FormControl } from '@angular/forms';
import { MatPaginator, MatSort, MatTableDataSource, MatDialog, MatDialogConfig } from '@angular/material';
import { Observable, of } from 'rxjs';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead';
import { mergeMap } from 'rxjs/operators';
import { MAT_DIALOG_DATA } from '@angular/material';
import { MatIconRegistry } from "@angular/material/icon";

import { DomSanitizer } from "@angular/platform-browser";
import { log } from 'util';


@Component({
  selector: 'app-process',
  templateUrl: './process.component.html',
  styleUrls: ['./process.component.css']
})


export class ProcessComponent implements OnInit {
  dOrder = "";
  progress: { percentage: number } = { percentage: 0 }
  displayOrderList = <any>[];
  showAddMessage: boolean = false;
  processName: string = '';
  acronym: string = '';
  processDescription: string = '';
  userId: string = '';
  firstName: string;
  typeaheadLoading: boolean;
  typeaheadLoadingProcess: boolean;
  typeaheadLoadingAcronym: boolean;
  typeaheadNoResults: boolean;
  dataSource: Observable<any>;
  dataSourceProcess: Observable<any>;
  dataSourceAcronym: Observable<any>;
  processList: any[] = [];
  acronymList: any[] = [];
  usersList: any[] = [];

  //validation variables
  allFunction: any;
  //parameter pass for save process
  functionId: number;
  processId: string;
  functionAcronym: string;

  //validation code
  processControl = new FormControl();
  addProcessForm = new FormGroup({
    processNameControl: this.processControl
  });

  duplicateProcessName: boolean = false;
  ownerNameSelection: boolean = false;
  fileNotLoaded: boolean = false;
  public functionID: number;
  activeYN = "";

  fileValue: string = "";
  fileUploadButton: boolean = false;
  fileUploaded: string = "";
  isLoadingResults = false;

  constructor(private http: HttpClient, private route: ActivatedRoute, private router: Router, private shareServices: ServiceService, private dashboardService: DashboardServiceService, public dialog: MatDialog, private formBuilder: FormBuilder, private changeDetectorRefs: ChangeDetectorRef, private matIconRegistry: MatIconRegistry, private domSanitizer: DomSanitizer, @Inject(MAT_DIALOG_DATA) data) {
    this.matIconRegistry.addSvgIcon(
      "file",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../assets/svg-icons/file.svg")
    );

    this.createForm();
    this.getAllFunction();
    this.functionId = data.functionId;
    this.functionAcronym = data.currentFuncitonAcronym;

    this.dataSourceProcess = Observable.create((observer: any) => {
      // Runs on every search
      observer.next(this.processName);
    })
      .pipe(
        mergeMap((token: string) => this.getStatesAsObservableProcess(token))
      );
    this.dataSourceAcronym = Observable.create((observer: any) => {
      // Runs on every search
      observer.next(this.acronym);
    })
      .pipe(
        mergeMap((token: string) => this.getStatesAsObservableAcronym(token))
      );
    this.dataSource = Observable.create((observer: any) => {
      // Runs on every search
      observer.next(this.userId);
    })
      .pipe(
        mergeMap((token: string) => this.getStatesAsObservable(token))
      );
  }
/**This Function use for Get Data of Acronym*/
  getStatesAsObservableAcronym(token: string): Observable<any> {
    const query = new RegExp(token, 'i');
    return of(
      this.acronymList.filter((str: any) => {
        return query.test(str.acronym);
      })
    );
  }
/**This Function use for Get Data of Process*/
  getStatesAsObservableProcess(token: string): Observable<any> {
    const query = new RegExp(token, 'i');
    return of(
      this.processList.filter((str: any) => {

        return query.test(str.processName);
      })
    );
  }
/**This Function use for Get Data of Owner Name*/
  getStatesAsObservable(token: string): Observable<any> {
    const query = new RegExp(token, 'i');
    return of(
      this.usersList.filter((state: any) => {
        return query.test(state.firstName);
      })
    );
  }
/**This Function use for Change TypeAhead Loading on Process Name*/
  changeTypeaheadLoadingProcess(e: boolean): void {
    this.typeaheadLoadingProcess = e;
  }
/**This Function use for Selection Change On Process Name*/
  typeaheadOnSelectProcess(e: TypeaheadMatch): void {
    this.processName = e.value;
    for (let s of this.processList) {
      JSON.parse(JSON.stringify(s), (key, value) => {
        if (key === 'processName') {
          if (value.trim().length === this.processName.trim().length && value.trim() === this.processName.trim()) {
            this.addProcessForm.controls['processNameControl'].setErrors({ 'invalid': true });
            this.duplicateProcessName = true;
          }
        }
      });
    }
  }
/**This Function use for Change TypeAhead Loading on Acronym*/
  changeTypeaheadLoadingAcronym(e: boolean): void {
    this.typeaheadLoadingAcronym = e;
  }
/**This Function use for Selection Change On Acronym*/
  typeaheadOnSelectAcronym(e: TypeaheadMatch): void {
    this.acronym = e.value;
    for (var i = 0; i < this.acronymList.length; i++) {
      if (this.acronymList[i].acronym.trim() == e.value.trim()) {
        this.addProcessForm.controls['acronymControl'].setErrors({ 'myError': true });
        return;
      } else {
        this.addProcessForm.controls['acronymControl'].setErrors({ 'myError': false });
      }
    }
  }

/**This Function use for Change TypeAhead Loading on Owner Name*/
  changeTypeaheadLoading(e: boolean): void {
    this.typeaheadLoading = e;
  }
/**This Function use for Selection Change On Owner Name*/
  typeaheadOnSelect(e: TypeaheadMatch): void {
    this.firstName = e.value;
  }

/**This Function is Use for Validation on Initialization formBuilder */
  createForm() {
    this.addProcessForm = this.formBuilder.group({
      processNameControl: ['', [Validators.required, Validators.pattern('^[a-zA-Z0-9 ]*$')]],
      acronymControl: ['', [Validators.required, Validators.pattern('^[a-zA-Z0-9\.]*$')]],
      decriptionControl: ['', [Validators.required, /*Validators.pattern(/^([^0-9]*)$/)*/]],
      ownerNameControl: ['', [Validators.required]],
      displayOrderControl: '',
      fileInput: '',
    })
  }
  ngOnInit() {
    this.dashboardService.getAllProcessDisplayOrderList().subscribe(data => {
      this.displayOrderList = data;
    });
    this.showAddMessage = false;
    /**This will bring all the orgs for type ahead */
    this.dashboardService.getAllUserList().subscribe(data => {
      for (let s of data) {
        this.usersList.push(s);
      }
    });

    /**This will bring all the process names for typeahead */
    this.dashboardService.getAllProcessList().subscribe(data => {
      for (let s of data) {
        this.processList.push(s);
      }
    });

    /**This will bring all the process acronym list for typeahead    */
    this.dashboardService.getAllProcessAcronymList().subscribe(data => {
      for (let s of data) {
        this.acronymList.push(s);
      }
    });
  }
/**This Function use for Get All Functions*/
  private getAllFunction() {
    this.dashboardService.getAllFunctionsList()
      .subscribe(data => {
        this.allFunction = data;
        this.allFunction.forEach(element => {
          if (this.functionId == element.functionID) {
            this.functionAcronym = element.acronym;
          }
        })
      });
  }
/**This Function use for Get Process data by Function Id*/
  public getAllProcessByFunctionId(functionId) {
    this.shareServices.getAllProcessByFunctionId(functionId)
      .subscribe(data => {
        this.dataSource = data;
        this.changeDetectorRefs.detectChanges();
      });
  }

/**This Function use for Save  Record */
  saveRecord() {
    this.isLoadingResults = true;
    this.duplicateProcessName = false;
    //validation codes
    if (this.progressBarValue !== 100 || !(this.fileUploaded = "Uploaded")) {
      this.fileNotLoaded = true;
      return;
    } else {
      this.fileNotLoaded = false;
    }
    if (!this.addProcessForm.valid) {
      return;
    } else {
      this.validateAllFormFields(this.addProcessForm);
    }

    if (this.dOrder == null || this.dOrder == "") {
      this.dOrder = "0";
    }

    for (let s of this.usersList) {
      JSON.parse(JSON.stringify(s), (key, value) => {
        if (key === 'firstName') {
          if (value === this.firstName) {
            JSON.parse(JSON.stringify(s), (key, value) => {
              if (key === 'userId') {
                this.userId = value;
              }
            });
          }
        }
      });
    }

    let processDetails: ProcessDetails = new ProcessDetails(this.functionId, this.processId, this.userId, this.processName, this.processDescription, this.acronym, this.dOrder, this.processImage, this.activeYN);
    if (processDetails.process_owner === undefined) {
      alert("This is not correct User please change it");
      return;
    }
    if (!this.isNumber(processDetails.process_owner)) {
      alert("This is not correct User please change it");
      return;
    }
    this.dashboardService.addNewProcessDetails(processDetails).subscribe(response => {
      if (response) {
        this.isLoadingResults = false;
        this.dialog.closeAll();
        this.showAddMessage = true;
        this.dashboardService.addStatus = true;
        this.dashboardService.processName = response.processName;
        this.getAllProcessByFunctionId(this.functionID);
      }
    }, error => {
      this.dashboardService.addStatus = false;
      this.dialog.closeAll();
      this.showAddMessage = true;
    });

  }
  /**This Function use for Validate All Fields*/
  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  /**This Function use for Check Value Is Number*/
  isNumber(value: string | number): boolean {
    return !isNaN(Number(value.toString()));
  }
  /**This Function use for Click on Cancel*/
  cancel() {
    this.dashboardService.cancelAdd = true;
  }

  /**File upload */
  processImage = "";
  selectedFiles: FileList;
  currentFileUpload: File;
  fileInput: string = "";
  hide: boolean = false;
  progressBarValue = 0;
  displayOrderValue: any;

  /*
    This function is use for When Name AlphaNumeric Validation.
    */
  checkCharValidationForName(value) {
    var str = value.target.value;
    if (str) {
    const matches = str.match(/^[A-Za-z][A-Za-z0-9/.,->_()\\*|:" ]*$/);
    return matches?null: this.addProcessForm.controls['processNameControl'].setErrors({ 'patternError': true });
    }
  }

  checkOnProcessName(event){
    this.processName = event.target.value;
    var value = event.target.value.toLowerCase();
    this.duplicateProcessName = false;
    if(event && event.target && event.target.value){
      var valueProcessName = event.target.value.toLowerCase();
      for(let s of this.processList) {
        JSON.parse(JSON.stringify(s), (key, value) => {
          if(key === 'processName'){
            var valueCheck = value.toLowerCase();
            if(valueCheck.trim().length === valueProcessName.trim().length && valueCheck.trim() === valueProcessName.trim() && valueCheck.toLowerCase() == valueProcessName){
              this.addProcessForm.controls['processNameControl'].setErrors({'invalid': true});
              this.duplicateProcessName=true;    
            }
          }
        });       
      }
    }
  }

  checkOnAcronymName(event){
     this.acronym = event.target.value;
    var value = event.target.value.trim();
    for (var i = 0; i < this.acronymList.length; i++) {
      if (this.acronymList[i].acronym.trim() == event.target.value.trim()) {
        setTimeout(() => {
        this.addProcessForm.controls['acronymControl'].setErrors({ 'myError': true });
      }, 0);
        return;
      }
    }
  }

  

  /*
  This function is use for When Acronym AlphaNumeric Validation.
  */
  checkCharValidationForAcronym(value) {
    var str = value.target.value;
      if (str) {
      const matches = str.match(/^[A-Za-z][A-Za-z0-9/.,->_()\\*|:" ]*$/);
      return matches?null: this.addProcessForm.controls['acronymControl'].setErrors({ 'patternAcronymError': true });
      }
  }

  /*
  This function is use for When File Input Change.
  */
  fileInputChanged(event) {
    this.fileUploaded = "";
    this.fileUploadButton = false;
    if (event && event.target && event.target.files) {
      this.selectedFiles = event.target.files;
    }
    this.progressBarValue = 0;
    this.addProcessForm.controls['fileInput'].markAsTouched();
    this.addProcessForm.controls['fileInput'].setValue(this.fileValue.split('\\').pop());
  }

  /*function for make the file not supported*/
  fileNotSupported() {
    this.progressBarValue = 100;
    this.addProcessForm.controls['fileInput'].setErrors({ 'invalidFile': true });
    this.addProcessForm.controls['fileInput'].markAsTouched();
  }

  /*function for successfully validate File*/
  validateFile() {
    this.progressBarValue = 100;
  }

  /*function for upload file*/
  upload() {
    if (this.selectedFiles && this.selectedFiles.item(0)) {
      const file = this.selectedFiles.item(0)
      const reader = new FileReader();
      reader.readAsDataURL(this.selectedFiles[0]);
      var ext = file.name.substr(file.name.lastIndexOf('.') + 1);
      reader.onload = event => {
        const img = new Image();
        img.src = (event.target as any).result;
        img.onload = () => {
          const elem = document.createElement('canvas');
          elem.width = img.width;
          elem.height = img.height;
          if (elem.width <= 128 && elem.height <= 128 && ext === 'png') {
            this.validateFile();
            this.currentFileUpload = this.selectedFiles.item(0);
            this.dashboardService.pushFileToStorageProcess(this.currentFileUpload, "icon", "process").subscribe(response => {
              this.processImage = response.fileName;
              this.addProcessForm.patchValue({
                fileInput: response.fileName
              });
              this.progressBarValue = 100;
              this.fileUploaded = "Uploaded";
              this.fileUploadButton = true;
            })
          } else {
            this.fileNotSupported();
            this.fileUploadButton = false;
          }
        },
          reader.onerror = error => console.log(error);
      };

    }

  }

  /*
    This function is use for When Owner Name  Validation.
    */
  checkOwnerName(event) {
    var test: boolean = false;
    for (let s of this.usersList) {
      JSON.parse(JSON.stringify(s), (key, value) => {
        if (key === 'firstName') {
          if (event.target.value != null && event.target.value != "") {
            if (value != null && value != "" && value !== event.target.value) {
              if (!test) {
                this.addProcessForm.controls['ownerNameControl'].setErrors({ 'invalid': true });
                this.ownerNameSelection = true;
                return;
              }
            } else {
              test = true;
              this.ownerNameSelection = false;
              return;
            }
          }
        }
      });
    }
  }

}


