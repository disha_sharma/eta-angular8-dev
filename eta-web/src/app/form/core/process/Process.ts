export class ProcessDetails {
    functionID: number;
    process_owner: string;
    processName: string;
    processDescription: string;
    acronym: string;
    displayOrder: string;
    processImage: string;
    processID: string;
    activeYN: string;
    
  
    constructor(functionId: number, processId: string, orgId: string, processName: string, processDescription: string, acronym: string, displayOrder: string, processImage: string, activeYN: string){
        this.functionID = functionId;
        this.process_owner = orgId;
        this.processID = processId;
        this.processName = processName;
        this.processDescription = processDescription;
        this.acronym = acronym;
        this.displayOrder = displayOrder;
        this.processImage = processImage;
        this.activeYN = activeYN;
        
    }  
  }