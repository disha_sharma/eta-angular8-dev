import { AbstractControl, ValidationErrors } from "@angular/forms"

export const taskNameValidator = function (control: AbstractControl): ValidationErrors | null {

  let value: string = control.value || '';
  let msg = "";

  if (value) {
    const matches = value.match(/^[A-Za-z][A-Za-z0-9/.,?=\[\]'><_&()*|" ]*$/);
    return matches ? null : { 'invalidName': true };
  } else {
    return null;
  }

  ///^[A-Za-z][A-Za-z0-9~`!@#$%^&*()_+-={}\|?/,.:;' ]*$
}

export const taskFlowNameValidator = function (control: AbstractControl): ValidationErrors | null {

  let value: string = control.value || '';
  let msg = "";

  if (value) {
    const matches = value.match(/^[A-Za-z][A-Za-z0-9 ]*$/);
    return matches ? null : { 'invalidName': true };
  } else {
    return null;
  }

  ///^[A-Za-z][A-Za-z0-9~`!@#$%^&*()_+-={}\|?/,.:;' ]*$
}
// export const NameValidator = function (control: AbstractControl): ValidationErrors | null {

//   let value: string = control.value || '';
//   let msg = "";

//   if (value) {
//     const matches = value.match(/^[A-Za-z][A-Za-z0-9/.,->_()*|:"\\ ]*$/);
//     return matches ? null : { 'invalidName': true };
//   } else {
//     return null;
//   }

//   ///^[A-Za-z][A-Za-z0-9~`!@#$%^&*()_+-={}\|?/,.:;' ]*$
// }
