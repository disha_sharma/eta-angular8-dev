import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef, MatTableDataSource, _getOptionScrollPosition } from '@angular/material';
import { FormGroup, FormBuilder, FormArray, Validators, ValidatorFn, AbstractControl, FormControl } from '@angular/forms';
import { HttpClient } from '@angular/common/http';



import { MatDialog, MatDialogConfig } from '@angular/material'; // for splitter task, added by Shyam joshi
import { ActivatedRoute, Router } from '@angular/router'; // for getting value from url Shyam joshi
import { ServiceService } from '../../../eta-services/service.service';
import { DashboardServiceService } from '../../../dash-board/dashboard-service.service';
import { AddTaskDetailPopupComponent } from '../../../popup/add-task-popup/add-task-detail-popup.component';
import { TaskServiceService } from '../../../dash-board/task-service.service';
import { Validation } from '../../../model/validation';
import { Observable, of } from 'rxjs';
import { mergeMap } from 'rxjs/operators';
import { TypeaheadMatch } from 'ngx-bootstrap';
import { FormServiceService } from '../../../forms/form-services/form-service.service';
import { element } from 'protractor';
import { taskNameValidator } from './taskName.validation';

export interface DialogData {
  id: "",
  action: ''
}

@Component({
  selector: 'app-add-task-detail',
  templateUrl: './add-task-detail.component.html',
  styleUrls: ['./add-task-detail.component.css']
})
export class AddTaskDetailComponent implements OnInit {


  taskTypeId: any;
  taskfrm: FormGroup;
  taskFlowName: string;
  Core_URL: any;
  category: any;
  allTaskType: any;
  subTaskType: any;
  dataContainer: any;
  allForm: any;
  taskObj: any = {};
  taskflowId: any;
  processId: any;
  responseMsg: any = {};

  public typeAheadTaskName: string = '';
  dataSourceTaskName: Observable<any>;
  public duplicateTaskFlowName: string = '';
  public checkDuplicateTaskFlowName: boolean = false;
  // public allTaskNameList: any=[];
  typeaheadLoadingTaskFlow: boolean;


  public isSpinnerShow: boolean = false;
  private taskEditObj: any;
  private buttonAction: boolean;
  private actionValue: String;
  private editTrueFlag: boolean = false;
  public allTask: any;
  public relationalOpraters: any;
  public categoryValue: any;
  splitter_task: any = []; // for splitter task, added by Shyam joshi
  joinner_task: any = [];// for splitter task, added by Shyam joshi
  joinner_task_id: any = [];// for splitter task, added by Shyam joshi
  /** EmailTemplate Name List */
  templateNameList: any = [];
  dataElementList: any = [];

  showConstantTextBox1: any;
  showConstantTextBox2: any;
  showConstantTextBox3: any;
  showConstantTextBox2ForBesinessRule: any;

  repeatingBlockConstantValue1: any;
  repeatingBlockConstantValue2: any;
  currentIndex: any;
  taskFlowList: any = [];
  id:any;

  dataSourceForTaskDetail = new MatTableDataSource();
  taskId = 0;
  data;
  taskDetails: any;
  public derivationData: Array<{ dataElementOneID: any, derivationOperatorID: any, dataElementTwoID: any, derivationDataElementID: any }> = [];
  public derivationDataWithId: Array<{ dataElementOneID: any, derivationOperatorID: any, dataElementTwoID: any, derivationDataElementID: any }> = [];
  operators: any;
  disableSubmit = false;
  showSuccessForTrueTask: boolean = false;
  showSuccessForFalseTask: boolean = false;
  newTrueTaskName: string;
  newFalseTaskName: string;
  space:boolean=false;
  disableSelect:boolean=false;




  constructor(private _formBuilder: FormBuilder, private http: HttpClient, private router: Router, public shareServices: ServiceService, private dashboardService: DashboardServiceService,
    private _activatedRouter: ActivatedRoute, @Inject(MAT_DIALOG_DATA) popupdata, public dialog: MatDialog, public taskService: TaskServiceService, private dialogRef: MatDialogRef<AddTaskDetailComponent>,
    private formService: FormServiceService) {
    this.Core_URL = this.shareServices.Core_URL;
    this.taskflowId = popupdata.taskflowId;
    this.processId = popupdata.processId;
    this.actionValue = popupdata.action;
    this.taskId = popupdata.id;
    this.data = popupdata
    this.getAllDataElement();// shyam joshi
    this.getAllTaskFlowList();
    // console.log('task flow id is');
    // console.log(this.taskflowId);
    // console.log("this.taskflowId:::: " + this.taskflowId);
    // console.log(popupdata.id);

    /** For Typeahead Search */
    this.dataSourceTaskName = Observable.create((observer: any) => {
      observer.next(this.duplicateTaskFlowName); // Runs on every search
    })
      .pipe(
        mergeMap((token: string) => this.getStatesAsObservableTaskFlow(token))
      );


  }

  ngOnInit() {

    /**This will bring all the function names for typeahead */


    this.getEmailTemplateNameInfo();
    this.taskfrm = this.newform();


    this.taskfrm.setControl('itemRows', this._formBuilder.array([])); //shyam joshi
    // this.taskfrm.setControl('darivationTaskList', this._formBuilder.array([]));

    /* Functions Start Here */
    this.getAllTaskType();
    this.getAllDataContainer();
    this.getAllForm();
    // console.log("Here i am.");

    this.getAllTaskByTaskFlowId(this.taskflowId)
    this.getAllOperators();

    this.shareServices.currentMessage.subscribe(message => this.taskEditObj = message);
    //this.patchTaskDetails();



  }


  getStatesAsObservableTaskFlow(token: string): Observable<any> {

    const query = new RegExp(token, 'i');
    return of(
      this.allTask.filter((str: any) => {
        return query.test(str.taskName);
      })
    );
  }

  changeTypeaheadLoadingTaskFlow(e: boolean): void {
    this.typeaheadLoadingTaskFlow = e;
  }



  typeaheadOnSelectTaskFlow(e: TypeaheadMatch): void {
    console.log("Type ahead  : " + e);
    for (let s of this.allTask) {
      JSON.parse(JSON.stringify(s), (key, value) => {
        if (key === 'taskName') {
          if (value.trim().length === this.duplicateTaskFlowName.trim().length && value.trim() === this.duplicateTaskFlowName.trim()) {
            this.taskfrm.controls['taskName'].setErrors({ 'invalid': true });
            this.taskfrm.controls['taskName'].markAsTouched();
            this.taskfrm.controls['taskName'].setErrors({ 'checkDuplicateTaskFlowName': true });
          }
        }
      });
    }
  }
  
  checkOnKeyUpName(event: any) {
    this.taskfrm.controls['taskName'].markAsTouched();

    // var str = '"some "quoted" string"';
    // console.log( str );
    // console.log( str.replace(/"/g, '') );
  
    for (let s of this.allTask) {
      JSON.parse(JSON.stringify(s), (key, value) => {
        if (key === 'taskName') {
          if (value.trim().length === this.duplicateTaskFlowName.trim().length && value.trim() === this.duplicateTaskFlowName.trim()) {
            this.taskfrm.controls['taskName'].setErrors({ 'invalid': true });
            this.taskfrm.controls['taskName'].markAsTouched();
            this.taskfrm.controls['taskName'].setErrors({ 'checkDuplicateTaskFlowName': true });
          }
        }
      });
    }
  }



  newform(): FormGroup {
    return this._formBuilder.group({

      previousTask: ['', [Validators.required]],
      taskName: ['', [Validators.required,taskNameValidator]],//this.noWhitespaceValidator]],
      category: ['', [Validators.required]],
      taskType: ['', [Validators.required]],
      container: [],
      taskDes: [],
      privilege: [],
      form: [],

      joinerTask: [],
      formDerivationData: [],

      //shyam joshi
      tasktitle: [],
      //taskdes: [],
      previlege: [],
      previlegename: [],
      previlegedes: [],
      process: [],
      previoustask: [],
      type: [],
      subtype: [],
      lasttask: [],
      firstDataElement: [],
      relationalOpraters: [],
      secondDataElement: [],

      /* Task IF/ELSE conditions */ // shyam joshi
      taskTrue: [],
      taskFalse: [],
      templateName: [],
      OtherConstant1: [],
      OtherConstant2: [],
      businessRuleTask: [],
      bRuleSecondDataElement: [],
      bRuleRelationalOpraters: [],
      bRuleFirstDataElement: [],
      bRuleOtherConstant2: [],
      businessRuleElseTask: [],

      /*for repeating blocks*/
      itemRows: this._formBuilder.array([this.initItemRows()]), //shyam joshi
      businessRuleRow: this._formBuilder.array([this.initBusinessRuleRows()]), // shyam joshi

      darivationTaskList: this._formBuilder.array([this.intiDerivationTaskArray()])


    });
  }

  

  // public noWhitespaceValidator(control: FormControl) {
  //   if (!/^[A-Za-z][A-Za-z0-9~`!@#$%^&*()_+-={}\|?/,.:;' ]*$/.test(control.value)) {
  //     return { validPassword: true };
  //   }
  //   return null;
  //   let task = control.value
  //   let index = task.indexOf(' ')
  //   console.log(task);
    
  //   if(index===0){
  //   const isWhitespace = index ===0;
  //   const isValid = !isWhitespace;
  //   return isValid ? null : { 'whitespace': true };
  // }
    
//}

numberOnly(event): boolean {
  const charCode = (event.which) ? event.which : event.keyCode;
  console.log('charCode',charCode);
  console.log('event.which',event.which);
  console.log('event',event);
  
  if (charCode > 31 && (charCode < 48 || charCode > 57)) {
    return false;
  }
  return true;

}

public inputValidator(event: any) {
  //console.log(event.target.value);
  const pattern = /^[a-zA-Z][a-zA-Z0-9 ]*$/;   
  //let inputChar = String.fromCharCode(event.charCode)
  if (pattern.test(event.target.value)) {
    //event.target.value = event.target.value.replace(/[^a-zA-Z0-9]/g, "");
    // invalid character, prevent input
    event.preventDefault();

  }
}

  patchTaskDetails() {
    if (this.actionValue === 'Edit') {
      // alert('hello')
      this.shareServices.getTaskByTaskId(this.taskId).subscribe(data => {
        this.taskDetails = data
         console.log(this.taskDetails);
        this.taskfrm.patchValue({
          previousTask: this.taskDetails.previousTaskID,
          taskName: this.taskDetails.taskName,
          category: this.getCategory(this.taskDetails.taskTypeID),//this.taskDetails.taskTypeID,
          taskType: this.getTaskType(this.taskDetails.subTaskTypeID),//taskDetails.subTaskType.subTaskTypeID,
          // container: this.taskDetails.container,
          taskDes: this.taskDetails.taskDescription,
          // privilege: this.taskDetails.privilege,
          form: this.taskDetails.formId,
          // tasktitle: this.taskDetails.tasktitle,
          //..................... //taskdes: this.taskDetails.taskdes,
          previlege: this.taskDetails.previlege,
          previlegename: this.taskDetails.privilegeName,
          // previlegedes: this.taskDetails.previlegedes,
          // process: this.taskDetails.process,
          //.....................previoustask: this.taskDetails.previousTaskID,
          // type: this.taskDetails.type,
          // subtype: this.taskDetails.subtype,
          lasttask: this.taskDetails.isEndTask,
          // firstDataElement: this.taskDetails.firstDataElement,
          // relationalOpraters: this.taskDetails.relationalOpraters,
          // secondDataElement: this.taskDetails.secondDataElement,
          taskTrue: this.taskDetails.taskTrue,
          taskFalse: this.taskDetails.taskFalse,
          templateName: this.taskDetails.templateName,
          // OtherConstant1: this.taskDetails.OtherConstant1,
          // OtherConstant2: this.taskDetails.OtherConstant2,
          // businessRuleTask: this.taskDetails.businessRuleTask,
          // bRuleSecondDataElement: this.taskDetails.bRuleSecondDataElement,
          // bRuleRelationalOpraters: this.taskDetails.bRuleRelationalOpraters,
          // bRuleFirstDataElement: this.taskDetails.bRuleFirstDataElement,
          // bRuleOtherConstant2: this.taskDetails.bRuleOtherConstant2,
          businessRuleElseTask: this.taskDetails.businessRuleElseTask,

        })
      })
      //this.taskId=0;
    }
    else {
      this.newform
    }
  }
  getCategory(value) {
    this.categoryChange(value, 0);
    return this.taskDetails.taskTypeID
  }
  getTaskType(value) {
    this.taskTypeFx(value);
    console.log(value)
    return this.taskDetails.subTaskType.subTaskTypeId
  }




  intiDerivationTaskArray() {
    return this._formBuilder.group({
      // list all your form controls here, which belongs to your form Business rule array
      dataElementOneID: ['', [Validators.required]],
      derivationOperatorID: ['', [Validators.required]],
      dataElementTwoID: ['', [Validators.required]],
      derivationDataElementID: ['', [Validators.required]],

    });
  }

  addDerivationData() {

    let control = <FormArray>this.taskfrm.controls['darivationTaskList'];
    let secondControl = control.controls[0];
    let dataElementOneIDControl = secondControl.get('dataElementOneID').value;
    let derivationOperatorIDControl = secondControl.get('derivationOperatorID').value;
    // console.log(derivationOperatorIDControl.id)
    let dataElementTwoIDControl = secondControl.get('dataElementTwoID').value;
    let derivationDataElementIDControl = secondControl.get('derivationDataElementID').value;


    if (dataElementOneIDControl == '' || derivationOperatorIDControl == '' || dataElementTwoIDControl == '' || derivationDataElementIDControl == 0 ||
      dataElementOneIDControl == null || derivationOperatorIDControl == null || dataElementTwoIDControl == null || derivationDataElementIDControl == null) {
      alert('All fields are mandatory')
    }
    else {
      //console.log('disha');

      this.derivationData.push({
        dataElementOneID: dataElementOneIDControl.elementName, derivationOperatorID: derivationOperatorIDControl.operator,
        dataElementTwoID: dataElementTwoIDControl.elementName, derivationDataElementID: derivationDataElementIDControl.elementName
      });

      this.derivationDataWithId.push({
        dataElementOneID: dataElementOneIDControl.elementId, derivationOperatorID: derivationOperatorIDControl.id,
        dataElementTwoID: dataElementTwoIDControl.elementId, derivationDataElementID: derivationDataElementIDControl.elementId
      });
      //console.log(this.derivationData);
      console.log(this.derivationDataWithId);
      //if you want to clear input
      control.reset();
      this.checkDerivationDataToClearValidation();
    }
    this.taskfrm.controls['formDerivationData'].setValue(this.derivationDataWithId)
  }

  checkDerivationDataToClearValidation() {
    let control = <FormArray>this.taskfrm.controls['darivationTaskList'];
    let secondControl = control.controls[0];
    let dataElementOneIDControl = secondControl.get('dataElementOneID');
    let derivationOperatorIDControl = secondControl.get('derivationOperatorID');
    let dataElementTwoIDControl = secondControl.get('dataElementTwoID');
    let derivationDataElementIDControl = secondControl.get('derivationDataElementID');

    if (this.derivationDataWithId.length > 0) {
      this.disableSubmit = false;
      dataElementOneIDControl.clearValidators();
      derivationOperatorIDControl.clearValidators();
      dataElementTwoIDControl.clearValidators();
      derivationDataElementIDControl.clearValidators();
    }
    else {
      this.disableSubmit = true;
      dataElementOneIDControl.setValidators([Validators.required]);
      derivationOperatorIDControl.setValidators([Validators.required]);
      dataElementTwoIDControl.setValidators([Validators.required]);
      derivationDataElementIDControl.setValidators([Validators.required]);
    }
    dataElementOneIDControl.updateValueAndValidity();
    derivationOperatorIDControl.updateValueAndValidity();
    dataElementTwoIDControl.updateValueAndValidity();
    derivationDataElementIDControl.updateValueAndValidity();
  }




  taskEditFx() {
    if (this.actionValue == "Edit") {
      //  alert(this.taskEditObj.subTaskTypeID)
      // console.log("i am in edit" + this.taskEditObj.subTaskTypeID);

      this.taskTypeId = this.taskEditObj.subTaskTypeID;
      this.taskfrm.patchValue({ previousTask: this.taskEditObj.previousTaskID });
      this.taskfrm.patchValue({ taskName: this.taskEditObj.taskName });
      this.taskfrm.patchValue({ taskDes: this.taskEditObj.taskDescription });
      this.taskfrm.patchValue({ privilege: this.taskEditObj.previlege });
      this.taskfrm.patchValue({ container: this.taskEditObj.taskFlowID });
      this.taskfrm.patchValue({ category: this.taskEditObj.taskTypeID });
      this.taskfrm.patchValue({ taskType: this.taskTypeId });
      this.taskfrm.patchValue({ form: this.taskEditObj.formId });
      this.taskfrm.patchValue({ darivationTaskList: this.taskEditObj.derivationList })
      this.categoryChange(this.taskEditObj.taskTypeID, this.taskTypeId)


    }
  }

  foods: any = [
    { value: 1, viewValue: 'START TASK' },
    { value: 2, viewValue: 'MIDDLE' },
    { value: 3, viewValue: 'END TASK' }
  ];


  taskTypeFx(taskType) {
    this.disableSubmit = false;
    this.taskTypeId = taskType;
    console.log("The Task Type is :====" + this.taskTypeId);
    /** conditional validations for system task category */
    const templateNameControl = this.taskfrm.get('templateName');
    const firstDataElementControl = this.taskfrm.get('firstDataElement');
    const relationalOpratersControl = this.taskfrm.get('relationalOpraters');
    const secondDataElementControl = this.taskfrm.get('secondDataElement');
    const taskFalseControl = this.taskfrm.get('taskFalse');
    const taskTrueControl = this.taskfrm.get('taskTrue');

    let control = <FormArray>this.taskfrm.controls['darivationTaskList'];
    let secondControl = control.controls[0];
    let dataElementOneIDControl = secondControl.get('dataElementOneID');
    let derivationOperatorIDControl = secondControl.get('derivationOperatorID');
    let dataElementTwoIDControl = secondControl.get('dataElementTwoID');
    let derivationDataElementIDControl = secondControl.get('derivationDataElementID');

    // const businessRuleTaskControl = this.taskfrm.get('businessRuleTask');
    // const bRuleSecondDataElementControl = this.taskfrm.get('bRuleSecondDataElement');
    // const bRuleRelationalOpratersControl = this.taskfrm.get('bRuleRelationalOpraters');
    // const bRuleFirstDataElementControl = this.taskfrm.get('bRuleFirstDataElement');

    let controlForBusinessRule = <FormArray>this.taskfrm.controls['businessRuleRow'];
    let secondcontrolForBusinessRule = controlForBusinessRule.controls[0];
    const businessRuleTaskControl = secondcontrolForBusinessRule.get('bRulefirstDataElementRow');
    const bRuleSecondDataElementControl = secondcontrolForBusinessRule.get('bRuleRelationalOpratersRow');
    const bRuleRelationalOpratersControl = secondcontrolForBusinessRule.get('bRuleSecondDataElementRow');
    const bRuleFirstDataElementControl = secondcontrolForBusinessRule.get('businessRuleTaskRow');
    const businessRuleElseTaskControl = this.taskfrm.get('businessRuleElseTask');

    let itemRowsControl = <FormArray>this.taskfrm.controls['itemRows'];

    let joinerTaskControl = this.taskfrm.controls['joinerTask'];

    if (controlForBusinessRule.length > 1) {
      let i = 0;
      for (i = controlForBusinessRule.length; i >= 0; i--) {
        // i=controlForBusinessRule.length+1;
        for (let j = 1; j < controlForBusinessRule.length; j++) {
          controlForBusinessRule.removeAt(j);
        }
      }

    }

    if (itemRowsControl.length >= 0) {
      let i = 0;
      for (i = itemRowsControl.length; i >= 0; i--) {
        // i=itemRowsControl.length+1;
        for (let j = 0; j < itemRowsControl.length; j++) {
          itemRowsControl.removeAt(j);
        }
      }

    }

    templateNameControl.clearValidators();
    firstDataElementControl.clearValidators();
    relationalOpratersControl.clearValidators();
    secondDataElementControl.clearValidators();
    taskTrueControl.clearValidators();
    taskFalseControl.clearValidators();
    dataElementOneIDControl.clearValidators();
    derivationOperatorIDControl.clearValidators();
    dataElementTwoIDControl.clearValidators();
    derivationDataElementIDControl.clearValidators();
    businessRuleTaskControl.clearValidators();
    bRuleSecondDataElementControl.clearValidators();
    bRuleRelationalOpratersControl.clearValidators();
    bRuleFirstDataElementControl.clearValidators();
    businessRuleElseTaskControl.clearValidators();
    joinerTaskControl.clearValidators();

    if (taskType === 2) {
      templateNameControl.setValidators([Validators.required]);
    }
    if (taskType === 5) {
      firstDataElementControl.setValidators([Validators.required]);
      relationalOpratersControl.setValidators([Validators.required]);
      secondDataElementControl.setValidators([Validators.required]);
      taskTrueControl.setValidators([Validators.required]);
      taskFalseControl.setValidators([Validators.required]);
    }
    if (taskType === 10) {
      this.checkDerivationDataToClearValidation();

    }
    if (taskType === 15) {
      businessRuleTaskControl.setValidators([Validators.required]);
      bRuleSecondDataElementControl.setValidators([Validators.required]);
      bRuleRelationalOpratersControl.setValidators([Validators.required]);
      bRuleFirstDataElementControl.setValidators([Validators.required]);
      businessRuleElseTaskControl.setValidators([Validators.required]);

    }
    if (taskType === 6) {
      if(this.splitter_task.length <= 0 && this.joinner_task.length <= 0){
        this.disableSubmit=true
      }
      else{
        this.disableSubmit=false
      }
      joinerTaskControl.setValidators([Validators.required])
    }
    templateNameControl.updateValueAndValidity();
    firstDataElementControl.updateValueAndValidity();
    relationalOpratersControl.updateValueAndValidity();
    secondDataElementControl.updateValueAndValidity();
    taskTrueControl.updateValueAndValidity();
    taskFalseControl.updateValueAndValidity();

    dataElementOneIDControl.updateValueAndValidity();
    derivationOperatorIDControl.updateValueAndValidity();
    dataElementTwoIDControl.updateValueAndValidity();
    derivationDataElementIDControl.updateValueAndValidity();
    businessRuleTaskControl.updateValueAndValidity();
    bRuleSecondDataElementControl.updateValueAndValidity();
    bRuleRelationalOpratersControl.updateValueAndValidity();
    bRuleFirstDataElementControl.updateValueAndValidity();
    businessRuleElseTaskControl.updateValueAndValidity();
    joinerTaskControl.updateValueAndValidity();
    /********************************************************* */

  }

  public getAllTaskType() {
    this.shareServices.getAllTaskType()
      .subscribe(data => {
        this.category = data;
        // console.log("i am cat");
        this.patchTaskDetails();
      });
  }

  public categoryChange(changevalue: any, value: any) {

    // console.log(changevalue);
    
    //console.log('task name index',this.taskfrm.controls['taskName'].value.index(0));
    // var index = this.taskfrm.controls['taskName'].value.indexOf( " " ); 
    // console.log("indexOf found String :" + index );
    // if(index==0){
    //   this.taskfrm.controls['taskName'].setErrors({'space': true})
    // }

    this.categoryValue = changevalue;

    this.category.forEach(element => {
      if (element.taskTypeID == changevalue) {
        /*For applying validations conditionally in human task category  */
        const templateNameControl = this.taskfrm.get('templateName');
        const firstDataElementControl = this.taskfrm.get('firstDataElement');
        const relationalOpratersControl = this.taskfrm.get('relationalOpraters');
        const secondDataElementControl = this.taskfrm.get('secondDataElement');
        const taskFalseControl = this.taskfrm.get('taskFalse');
        const taskTrueControl = this.taskfrm.get('taskTrue');
        let control = <FormArray>this.taskfrm.controls['darivationTaskList'];
        let controlForBusinessRule = <FormArray>this.taskfrm.controls['businessRuleRow'];
        let itemRowsControl = <FormArray>this.taskfrm.controls['itemRows'];
        let taskTypeControl = this.taskfrm.get('taskType')

        if (controlForBusinessRule.length > 1) {
          let i = 0;
          for (i = controlForBusinessRule.length; i >= 0; i--) {
            // i=controlForBusinessRule.length+1;
            for (let j = 1; j < controlForBusinessRule.length; j++) {
              controlForBusinessRule.removeAt(j);
            }
          }

        }

        if (itemRowsControl.length >= 0) {
          let i = 0;
          for (i = itemRowsControl.length; i >= 0; i--) {
            // i=itemRowsControl.length+1;
            for (let j = 0; j < itemRowsControl.length; j++) {
              itemRowsControl.removeAt(j);
            }
          }

        }

        const privilageControl = this.taskfrm.get('privilege');
        const formControl = this.taskfrm.get('form');
        if (changevalue === 1) {

          templateNameControl.reset()
          firstDataElementControl.reset()
          relationalOpratersControl.reset()
          secondDataElementControl.reset()
          taskFalseControl.reset()
          taskTrueControl.reset()
          taskTypeControl.reset()
          control.reset();
          controlForBusinessRule.reset();
          this.derivationData.length = 0
          this.derivationDataWithId.length = 0
          this.disableSubmit=false;

          privilageControl.setValidators([Validators.required]);
          formControl.setValidators([Validators.required]);
        } else {

          privilageControl.reset()
          formControl.reset()
          taskTypeControl.reset()

          privilageControl.clearValidators();
          formControl.clearValidators();
        }
        privilageControl.updateValueAndValidity();
        formControl.updateValueAndValidity();
        /****************************************/
        this.subTaskType = element.subTaskTypeDAO;
        // console.log(this.subTaskType);
       this.taskTypeFx(value);
      }
    });
  }

  onSubmit = function (taskValue) {
    // console.log("add task on edit");

    let task2WayArray = [];
    let businessRuleArray = [];
    // let derivationList=[]
     console.log('form data',taskValue);
    console.log(this.derivationData);


    let twoWayBindingObj: any = {};

    /* Reading First Two block for 2 way tasks */
    twoWayBindingObj.logicalOperators = "";
    twoWayBindingObj.expression1 = taskValue.firstDataElement;
    twoWayBindingObj.relationalOperators = taskValue.relationalOpraters;
    twoWayBindingObj.expression2 = taskValue.secondDataElement;
    twoWayBindingObj.otherConstant1 = this.taskfrm.value.OtherConstant1;
    twoWayBindingObj.otherConstant2 = this.taskfrm.value.OtherConstant2;
    task2WayArray.push(twoWayBindingObj)

    /* Reading values from repeating block */
    taskValue.itemRows.forEach(element => {
      let twoWayBindingObjInner: any = {};
      twoWayBindingObjInner.logicalOperators = element.relationalConditionRow;
      twoWayBindingObjInner.expression1 = element.firstDataElementRow;
      twoWayBindingObjInner.relationalOperators = element.relationalOpratersRow;
      twoWayBindingObjInner.expression2 = element.secondDataElementRow;

      task2WayArray.push(twoWayBindingObjInner)

    });

    let businessRuleObj: any = {};
    businessRuleObj.busnessTask = taskValue.businessRuleTask;
    businessRuleObj.expression1 = taskValue.bRuleFirstDataElement;
    businessRuleObj.relationalOperators = taskValue.bRuleRelationalOpraters;
    businessRuleObj.expression2 = taskValue.bRuleSecondDataElement;
    businessRuleObj.otherConstant1 = this.taskfrm.value.OtherConstant1;
    businessRuleObj.otherConstant2 = this.taskfrm.value.bRuleOtherConstant2;
    businessRuleArray.push(businessRuleObj)

    taskValue.businessRuleRow.forEach(element => {

      let businessRuleInnerObj: any = {};
      businessRuleInnerObj.busnessTask = element.businessRuleTaskRow;
      businessRuleInnerObj.expression1 = element.bRulefirstDataElementRow;
      businessRuleInnerObj.relationalOperators = element.bRuleRelationalOpratersRow;
      businessRuleInnerObj.expression2 = element.bRuleSecondDataElementRow;
      businessRuleArray.push(businessRuleInnerObj)
    });



    /* Splitter Task -- Task Id Array */
    let allSplitterTaskId: any = [];
    this.splitter_task.forEach(element => {
      let splitterTask: any = {};
      splitterTask = element.id;

      allSplitterTaskId.push(splitterTask)
    });

    let joinnerTask: any;
    this.joinner_task.forEach(element => {
      joinnerTask = element.id;
    });



    //this.taskObj["taskID"] = this.taskEditObj.taskID;
    this.taskObj["taskName"] = this.taskfrm.value.taskName;
    this.taskObj["taskDescription"] = this.taskfrm.value.taskDes;
    this.taskObj["previlege"] = this.taskfrm.value.privilege;
    this.taskObj["privilegeName"] = this.taskfrm.value.privilege;
    this.taskObj["privilegeDescription"] = this.taskfrm.value.privilege;

    this.taskObj["taskFlowID"] = this.taskflowId;
    this.taskObj["twoWayRepeating"] = task2WayArray;
    this.taskObj["taskTrue"] = taskValue.taskTrue;
    this.taskObj["taskFalse"] = taskValue.taskFalse;
    this.taskObj["previousTaskID"] = this.taskfrm.value.previousTask;
    this.taskObj["taskTypeID"] = this.taskfrm.value.category;
    this.taskObj["subTaskTypeID"] = this.taskfrm.value.taskType;
    this.taskObj["formId"] = this.taskfrm.value.form;
    this.taskObj["isStartOrEntTask"] = 0;
    this.taskObj["derivationList"] = taskValue.formDerivationData;
    this.taskObj["templateName"] = taskValue.templateName;
    this.taskObj["splitterArray"] = allSplitterTaskId;
    this.taskObj["joinerTask"] = joinnerTask;
    this.taskObj["businessRuleArray"] = businessRuleArray;
    this.taskObj["businessRuleElseTask"] = taskValue.businessRuleElseTask;

    /* edit Value */
    this.taskObj["privilegeId"] = this.taskEditObj.privilegeId
    // console.log("add-task compo ......." + this.taskflowId);

    //this.taskService.saveTask(this.taskFlowId);
    ///this.shareServices.setTaskFlowId(this.taskflowId);
    // console.log('task obj', this.taskObj);
    this.isSpinnerShow = true;
    //this.shareServices.saveTask(this.taskObj).subscribe();
    console.log('form obj',this.taskObj);
    
    this.http.post(this.Core_URL + '/saveTask/', this.taskObj)
      .subscribe(data => {
        // console.log(data);
        console.log('inside api func. ',this.taskObj);
        if (data != null) {
          //alert("i am in success");
          this.responseMsg.displayMsg = "Successfully added Leasing and placed it at the end of the display order. Drag it up to reorder.";
          this.responseMsg.cssClass = "Success";
          this.isSpinnerShow = false;
          this.router.navigate(['edittaskflow', this.processId, this.taskflowId]);
        } else {
          //  alert("i am in Error");
          this.responseMsg.displayMsg = "Error added Leasing and placed it at the end of the display order. Drag it up to reorder.";
          this.responseMsg.cssClass = "Error";
          this.isSpinnerShow = false;
        }
      }, error => {
        this.responseMsg.displayMsg = "Error added Leasing and placed it at the end of the display order. Drag it up to reorder.";
        this.responseMsg.cssClass = "Error";
        this.isSpinnerShow = false;
      });
    //this.dialog.closeAll();
    console.log(this.taskObj);

    this.dialogRef.close({ status: true, data: taskValue.taskName })
  }

  getAllDataContainer() {
    this.shareServices.getAllDataContainer()
      .subscribe(res => {
        this.dataContainer = res;
        // console.log(res);

      })
  }

  public getAllForm() {
    this.http.get(this.Core_URL + '/getAllForm')
      .subscribe(data => {
        this.allForm = data;
        console.log('form', this.allForm)
      });
  }


  public getAllTaskByTaskFlowId(taskFlowId: number) {


    this.http.get(this.Core_URL + '/getAllTaskByTaskFlowId/' + taskFlowId)
      .subscribe(data => {
        // console.log(data);

        let taskList: any = [];
        taskList = data;
       
        if (taskList.length == 0) {
          alert('No task available');
        }
        if (taskList.length <= 2) {
          this.disableSelect=true;
          this.taskfrm.patchValue({
            previousTask : taskList[0].taskID
          })
        }
        this.allTask = data;
        console.log('task list', this.allTask);

      });
  }

  addNewDerivation() {
    // control refers to your formarray
    const control = <FormArray>this.taskfrm.controls['darivationTaskList'];
    // add new formgroup
    control.push(this.intiDerivationTaskArray());
  }
  deleteRowForDerivationTask(index: number) {
    // control refers to your formarray
    // const control = <FormArray>this.taskfrm.controls['darivationTaskList'];
    // remove the chosen row
    // control.removeAt(index);
    this.derivationData.splice(index, 1);
    this.derivationDataWithId.splice(index, 1);
    this.checkDerivationDataToClearValidation();
  }


  /* Get All Operator  */

  getAllOperators() {
    this.http.get(this.Core_URL + '/getAllSpdOperators')
      .subscribe(data => {
        this.relationalOpraters = data;
        // console.log(data);

      });
  }
  //code for 2w route task starts, added by shyam joshi
  initItemRows() {
    return this._formBuilder.group({
      // list all your form controls here, which belongs to your form array
      relationalConditionRow: ['', [Validators.required]],
      firstDataElementRow: ['', [Validators.required]],
      relationalOpratersRow: ['', [Validators.required]],
      secondDataElementRow: ['', [Validators.required]],
      OtherConstant1: [],
      // OtherConstant2: ['',this.conditionalValidator(
      //   (() => this.repeatingBlockConstantValue2===-2 || this.repeatingBlockConstantValue2 !==-2),
      //   Validators.required
      // )]
      OtherConstant2: [],

    });
  }
  initBusinessRuleRows() {
    return this._formBuilder.group({
      // list all your form controls here, which belongs to your form Business rule array
      bRulefirstDataElementRow: ['', [Validators.required]],
      bRuleRelationalOpratersRow: ['', [Validators.required]],
      bRuleSecondDataElementRow: ['', [Validators.required]],
      businessRuleTaskRow: ['', [Validators.required]],
      bRuleOtherConstantRow: [],
      OtherConstant2: [],
    });
  }
  addNewRow() {
    // control refers to your formarray
    const control = <FormArray>this.taskfrm.controls['itemRows'];
    // add new formgroup
    control.push(this.initItemRows());
  }
  deleteRow(index: number) {
    // control refers to your formarray
    const control = <FormArray>this.taskfrm.controls['itemRows'];
    // remove the chosen row
    control.removeAt(index);
  }
  //code for 2w route task ends



  //code for splitter task, added by Shyam joshi, starts here
  openDialog(action): void {

    /*  const dialogRef = this.dialog.open(PopupTemplateComponent, {
       width: '80% auto',
       data: { taskId: this.taskId, taskName: this.taskName }
     }); */
          

    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    dialogConfig.data = {
      action: action,
      processId: this.processId,
      taskflowId: this.taskflowId,
      taskName: this.taskfrm.controls['taskName'].value.trim(),
      title: 'Angular For Beginners'
    };

    const dialogRef = this.dialog.open(AddTaskDetailPopupComponent, dialogConfig);

    dialogRef.afterClosed()
      .subscribe(result => {
        this.getAllTaskByTaskFlowId(this.taskflowId);
        console.log('The splitter dialog value is-----', result.id, result.data);

        this.taskService.getAllTaskByTaskFlowId(result.id)
          .subscribe(data => {
            // console.log(data);

            let taskList: any = [];
            taskList = data;

            console.log('loop data', taskList);

            for (let i = 0; i < Object.keys(taskList).length; i++) {


              if (taskList[i].taskName === result.data) {

                this.shareServices.getTaskByTaskId(taskList[i].taskID)
                  .subscribe(res => {
                    const value = res.taskName;
                    if ((value || '').trim()) {
                      this.splitter_task.push({ name: value.trim(), id: res.taskID });
                      //console.log('splitter',this.splitter_task);
                      // this.splitter_task_id.push({id:res.id});
                    }
                    this.checkSplitterToClearValidation();
                  }
                    // , error => {
                    //   alert("No Splitter Task Added !!!")
                    // }
                  )
              }
              
            }

          });
          
      });
  }

  checkSplitterToClearValidation(){
    
    // if(this.splitter_task.length <= 0 && this.joinner_task.length <= 0){
    //   this.disableSubmit=true
    // }
    if(this.splitter_task.length <= 0 || this.joinner_task.length <= 0){
      this.disableSubmit=true
    }
    else{
      this.disableSubmit=false
    }
  }



  openFutureTaskDialog(action): void {

    /*  const dialogRef = this.dialog.open(PopupTemplateComponent, {
       width: '80% auto',
       data: { taskId: this.taskId, taskName: this.taskName }
     }); */

    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    dialogConfig.data = {
      action: action,
      processId: this.processId,
      taskflowId: this.taskflowId,
      taskName: this.taskfrm.controls['taskName'].value.trim(),
      title: 'Angular For Beginners'
    };

    const dialogRef = this.dialog.open(AddTaskDetailPopupComponent, dialogConfig);

    dialogRef.afterClosed()
      .subscribe(result => {
        this.getAllTaskByTaskFlowId(this.taskflowId);
        console.log('splitter result', result);

        this.taskService.getAllTaskByTaskFlowId(result.id)
          .subscribe(data => {

            let taskList: any = [];
            taskList = data;

            console.log('loop data', taskList);

            for (let i = 0; i < Object.keys(taskList).length; i++) {
              if (taskList[i].taskName === result.data) {
                this.id=taskList[i].taskID;
                console.log('task Id no =', this.id);
                
                this.shareServices.getTaskByTaskId(taskList[i].taskID)


                  .subscribe(res => {
                    const value = res.taskName;
                    if ((value || '').trim()) {
                      this.joinner_task.push({ name: value.trim(), id: res.taskID });
                      //console.log(this.joinner_task.length);
                     // console.log('splitter', this.joinner_task);
                      // this.splitter_task_id.push({id:res.id});
                    }
                    this.checkSplitterToClearValidation();
                  },
                    //  error => {
                    //   // console.log('error -----0->');
                    //   // console.log('error -----0-> ' + error);
                    //   alert("No Splitter Task Added !!!")
                    // }
                  )
              }
            }

          });
      });
  }

  
  deleteJoinerTask(index, taskId){
    console.log(taskId);
    this.taskService.deleteTask(taskId).subscribe(data=>{
      console.log('deleted')
    })
    this.allTask.splice(index,1)
    this.joinner_task.splice(index, 1);
    this.checkSplitterToClearValidation();
}

deleteSplitterTask(index, taskId){
  console.log(taskId);
  this.taskService.deleteTask(taskId).subscribe(data=>{
    console.log('deleted')
  })
  this.allTask.splice(index,1)
  this.splitter_task.splice(index, 1);
  this.checkSplitterToClearValidation();
}

  OtherConstantValue(constantValue) {
    //  alert(constantValue)
    this.showConstantTextBox1 = constantValue;
    const OtherConstant1Control = this.taskfrm.get('OtherConstant1');
    if (constantValue == -1) {
      OtherConstant1Control.setValidators([Validators.required]);
    }
    else {
      OtherConstant1Control.clearValidators()
    }
    OtherConstant1Control.updateValueAndValidity();
  }

  OtherConstantValue1(constantValue) {
    // alert(constantValue)
    this.showConstantTextBox2 = constantValue;
    const otherConstant2control = this.taskfrm.get('OtherConstant2');
    if (constantValue == -2) {
      otherConstant2control.setValidators([Validators.required]);
    }
    else {
      otherConstant2control.clearValidators();
    }
    otherConstant2control.updateValueAndValidity();
  }

  bRuleSecondDataConstant(constantValue, index) {
    // alert(constantValue)
    this.showConstantTextBox2ForBesinessRule = constantValue;
    let control = <FormArray>this.taskfrm.controls['businessRuleRow'];
    let secondControl = control.controls[index];
    let value = secondControl.get('bRuleOtherConstantRow');

    if (constantValue == -2) {
      value.setValidators([Validators.required])
    }
    else {
      value.clearValidators();
    }
    value.updateValueAndValidity();
  }

  OtherConstantValue2(constantValue) {
    // alert(constantValue)
    this.showConstantTextBox3 = constantValue;
    const bRuleOtherConstant2Control = this.taskfrm.get('bRuleOtherConstant2');
    if (constantValue === -2) {
      bRuleOtherConstant2Control.setValidators([Validators.required]);
    }
    else {
      bRuleOtherConstant2Control.clearValidators();
    }
    bRuleOtherConstant2Control.updateValueAndValidity();
  }

  repeatingOtherConstantValue1(constantValue, index) {

    this.currentIndex = index;
    this.repeatingBlockConstantValue1 = constantValue


  }
  repeatingOtherConstantValue2(constantValue, index) {
    // console.log(constantValue + "and  index value :" + index);

    this.currentIndex = index;
    this.repeatingBlockConstantValue2 = constantValue;

    let control = <FormArray>this.taskfrm.controls['itemRows'];
    let secondControl = control.controls[index];
    let value = secondControl.get('OtherConstant2');

    if (constantValue === -2) {
      value.setValidators([Validators.required]);

    }
    else {
      value.clearValidators();
    }
    value.updateValueAndValidity();

  }


  // conditionalValidator(condition: (() => any), validator: ValidatorFn): ValidatorFn {
  // console.log('came')
  //   return (control: AbstractControl): {[key: string]: any} => {
  //     if (! condition()) {
  //   console.log(control)
  //       return null;
  //     }
  //     return validator(control);
  //   }
  // }



  openTaskTrueDialog(): void {

    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      processId: this.processId,
      taskflowId: this.taskflowId,
      taskName: this.taskfrm.controls['taskName'].value.trim(),
      title: 'Angular For Beginners'
    };

    const dialogRef = this.dialog.open(AddTaskDetailPopupComponent, dialogConfig);

    dialogRef.afterClosed()
      //   setTimeout(()=> {
      // }, 2000)
      .subscribe(
        result => {
          this.getAllTaskByTaskFlowId(this.taskflowId);
          this.shareServices.getTaskByTaskId(result)
            .subscribe(res => {
              const value = res.taskName;
              if ((value || '').trim()) {
                this.shareServices.getTaskByTaskId(result);
                this.taskfrm.patchValue({ taskTrue: res.taskID });

              }
            }
              // , error => {
              //   alert("No  Task Added !!!!!")}
            )

          if (result != undefined) {
            if (result.status == true) {
              this.showSuccessForTrueTask = true;
              this.newTrueTaskName = result.data;
              setTimeout(() => {
                this.showSuccessForTrueTask = false;
              }, 3000);

            }
          }

        });
  }


  openTaskFalseDialog(): void {

    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    dialogConfig.data = {
      processId: this.processId,
      taskflowId: this.taskflowId,
      taskName: this.taskfrm.controls['taskName'].value.trim(),
      title: 'Angular For Beginners'
    };
    const dialogRef = this.dialog.open(AddTaskDetailPopupComponent, dialogConfig);

    dialogRef.afterClosed()
      .subscribe(result => {
        this.getAllTaskByTaskFlowId(this.taskflowId);
        this.shareServices.getTaskByTaskId(result)
          .subscribe(res => {
            const value = res.taskName;
            if ((value || '').trim()) {
              this.shareServices.getTaskByTaskId(result);
              this.taskfrm.patchValue({ taskFalse: res.taskID });

            }
          },
            //  error => {
            //   alert("No  Task Added !!!")
            // }
          )
        if (result != undefined) {
          if (result.status == true) {
            this.showSuccessForFalseTask = true;
            this.newFalseTaskName = result.data;
            setTimeout(() => {
              this.showSuccessForFalseTask = false;
            }, 3000);

          }
        }
      });
  }

  //code for splitter task, added by Shyam joshi, ends here

  //done by shyam joshi
  addNewBusinessRule() {
    // control refers to your formarray
    const control = <FormArray>this.taskfrm.controls['businessRuleRow'];
    // add new formgroup
    control.push(this.initBusinessRuleRows());
  }
  deleteBusinessRule(index: number) {
    // control refers to your formarray
    const control = <FormArray>this.taskfrm.controls['businessRuleRow'];
    // remove the chosen row
    control.removeAt(index);
  }

  /*for notification, done by Shyam, starts */
  getEmailTemplateNameInfo() {
    this.shareServices.getAllTemplateByOrgId()
      .subscribe(res => {
        // console.log(res);
        this.templateNameList = res;
        if (this.templateNameList == null || this.templateNameList.length == 0) {
          alert("No Template Found!! please add notification template");
        }

      })
  }
  /*for notification, done by Shyam, ends */

  //done by Shyam joshi
  public getAllDataElement() {

    this.http.get(this.Core_URL + '/getDataElementNames')
      .subscribe(data => {
        this.dataElementList = data;
        // console.log("dsafdsafdasfdafdasfdas");
        console.log('list', this.dataElementList);
      }
        , function (error) {
          // console.log('error is')
          // console.log(error);
        }
      );
    // this.formService.getDataElementNames().subscribe(data=>{
    //   this.dataContainer=data;
    // console.log(this.dataElementList);
    // }
    //   , function (error) {
    // console.log('error is')
    // console.log(error);
    //   }
    // );


    this.http.get(this.Core_URL + '/getAllSpdOperators')
      .subscribe(data => {
        this.relationalOpraters = data;
        // console.log(this.relationalOpraters);
      });
  }

  /** This Method is used for Display Task FLow Name in Dialog Box */
  getAllTaskFlowList() {
    this.taskService.getTaskFlowNameList().subscribe(data => {
      this.taskFlowList = data;
      this.taskFlowList.forEach(element => {
        if (this.taskflowId == element.taskFlowID) {
          this.taskFlowName = element.taskFlowName;
        }
      });
    });
  }

  update() {

    //  let obj = this.taskfrm.value;
    //  this.shareServices.updateTask(this.taskId,obj).subscribe(data =>{
    // console.log(data);
    //  })
    //   this.taskService.getAllTasks().subscribe(data =>{ let details =data;
    // console.log('hello',details);
    // })

    // let p= Object.assign({},this.taskfrm.value)
    //   this.taskService.updateTask(p,this.taskId).subscribe( x =>{});

  }

  omitSpecialChar(event) {

    var k;
   
    k = event.charCode; // k = event.keyCode; (Both can be used)
   // console.log(k);
   
    return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 44 || k == 124 || k == 34 || k == 95 || k == 91 || k == 93 || k == 32 ||
     (k >= 46 && k <= 57) || (k >= 38 && k <= 42) || (k >= 60 && k <= 63));
   
    }


}
