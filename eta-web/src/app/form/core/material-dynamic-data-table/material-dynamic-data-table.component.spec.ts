import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MaterialDynamicDataTableComponent } from './material-dynamic-data-table.component';

describe('MaterialDynamicDataTableComponent', () => {
  let component: MaterialDynamicDataTableComponent;
  let fixture: ComponentFixture<MaterialDynamicDataTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MaterialDynamicDataTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MaterialDynamicDataTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
