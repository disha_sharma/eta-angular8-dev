import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { ServiceService } from './../../../eta-services/service.service'
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialogConfig, MatDialog } from '@angular/material';
import { FormFactoryComponent } from '../../../dynamic-form-builder/form-factory/form-factory.component';
import { ReviewFormComponent } from '../review-form/review-form.component';
import { SampleFormComponent } from '../sample-form/sample-form.component';
import { ViewChild, ElementRef } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { SelectionModel } from '@angular/cdk/collections';


class DataTablesResponse {
  data: any[];
  draw: number;
  recordsFiltered: number;
  recordsTotal: number;
  coloumnName: {};
}
@Component({
  selector: 'app-material-dynamic-data-table',
  templateUrl: './material-dynamic-data-table.component.html',
  styleUrls: ['./material-dynamic-data-table.component.css']
})
export class MaterialDynamicDataTableComponent implements OnInit {

  
  /**
   * grid data URL
   */
  @Input() myurl: string;

  /**
   * static coloumns
   */
  @Input() mycoloums = [];

  /**
   * Inline edit Url 
   */
  @Input() mySubmitUrl: string;

  /**
   * grid data
   */
  @Input() gefields: any[];

  @Output() translationEditObj = new EventEmitter();

  dtOptions: DataTables.Settings = {};
  showPopUp = false;
  isCreateCheck = false;
  Core_URL: any;
  isDataLoad = true;
  private URL: any;
  public data: any = {};
  public currentTaskJson: any = [];
  formName: string = "";
  taskInstanceID: any;
  taskFlowID: any;
  taskId: any;
  formId: any;
  processId : any;
  functionId  :any;

  constructor(private http: HttpClient, private shareServices: ServiceService, private r: Router, public dialog: MatDialog, private route: ActivatedRoute) {
    this.Core_URL = this.shareServices.Core_URL;
  }

  ngOnInit(): void {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    for(let i = 0; i < this.mycoloums.length; i++){
      this.displayedColumns.push(this.mycoloums[i].displayName)
    }
    let firstCol = this.mycoloums[0].data;
    if (firstCol === "check") {
      this.isCreateCheck = true;
    }

    const that = this;

    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 4,
      serverSide: true,
      processing: true,
      columns: this.mycoloums,
      ajax: (dataTablesParameters: any, callback) => {
        dataTablesParameters.transactionId = 7;
        that.http
          .post<DataTablesResponse>(
            this.myurl, dataTablesParameters, {}
          ).subscribe(resp => {
            that.gefields = resp.data;
            that.dataSource = new MatTableDataSource(resp.data); 
            callback({
              recordsTotal: resp.recordsTotal,
              recordsFiltered: resp.recordsFiltered,
              data: []
            });
          });
      },
    };
  }

  /**
   * make grid cell editable  
   * @param e click event
   */
  private makeCellEdit(e) {
    e.target.parentNode.firstElementChild.setAttribute("class", "textBox");
    e.target.parentNode.firstElementChild.setAttribute("contentEditable", "true");
    //console.log("Parents parent sibling:", e.target.parentNode.firstElementChild.class = 'textBox');
  }

  /**
   * save data on focus out
   * @param event focus out
   * @param obj full row object
   * @param key edit cell value
   */
  private saveDetails(event, obj, key) {
    event.target.setAttribute("class", "");
    event.target.setAttribute("contentEditable", "false");
    let as: HTMLElement = event.target;

    this.editTranslationObj(obj, as.innerText, key);

  }

  private showUser(userEvent, gefield, userobj) {
    this.showPopUp = true;
    let colkey: any;
  }

  private closePopUp() {
    this.showPopUp = false;
  }

  private editCheckBox(event, object) {
    this.shareServices.setgridChangeMessage(object);
  }

  private editTranslationObj(editObj, editValue, key) {
    this.translationEditObj.emit({ "obj": editObj, "newEditValue": editValue, "languageId": key.data });
  }

  /**
   * claim task work only for Core
   * @param event
   * @param object 
   */
  private buttonClick(event, object, k) {
    if (k.data == 'reviewButton') {
      this.reviewTask(event, object, k);
    }
    else if (k.data == 'claimButton') {
      this.claimTask(event, object, k);
    }
  }

  private claimTask(event, object, k) {
    this.isDataLoad = false;
    this.http.get(this.Core_URL + '/claimTask/' + object.taskInstanceID)
      .subscribe(data => {
        this.isDataLoad = true;
        alert("Task Claimed !! please check your my task inbox");
      });
  }

  private reviewData: any;
  private reviewTask(event, object, k) {
    console.log('review', object);
    this.myFunction(object.taskFlowID, object.taskInstanceID, object.reveiewTaskID, object.reviewFormID, object.reviewFormName,object.processId,object.functionId);
    /*this.isDataLoad = false;
    this.http.get(this.Core_URL + '/getReviewTask/' + object.processId + '/' + object.taskInstanceID + '/' + object.formId)
      .subscribe(data => {
        this.reviewData = data;
        console.log(this.reviewData);
        this.shareServices.setgridChangeMessage(this.reviewData);
        this.r.navigateByUrl('process/' + object.processId + '/' + this.reviewData.nextTask + '/' + this.reviewData.nextfomId);
      });*/
  }

  myFunction(taskFlowID, taskInstanceID, taskId, formId, formName,processId,functionId) {
    this.formName = formName;
    this.taskFlowID = taskFlowID;
    this.taskInstanceID = taskInstanceID;
    this.taskId = taskId;
    this.formId = formId;
    this.processId = processId;
    this.functionId = functionId;
    this.getCurrentTaskFormJson();
  }

  private getCurrentTaskFormJson() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.disableClose = true;
    dialogConfig.data = {
      formName: this.formName,
      taskInstanceId: this.taskInstanceID,
      taskFlowId: this.taskFlowID,
      taskId: this.taskId,
      formId: this.formId,
      processId : this.processId,
      functionId : this.functionId
    };
    const dialogRef = this.dialog.open(SampleFormComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  print(){
    console.log('myColumn', this.mycoloums)
    console.log('getFields', this.gefields);
    ;
    
  }

  displayedColumns: string[] = [];
  dataSource = new MatTableDataSource();
  rowsSelection = new SelectionModel(true, []);
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  isExpansionDetailRow = (index, row) => row.hasOwnProperty('detailRow');


  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }
    /** Whether the number of selected elements matches the total number of rows. */
    isAllSelected() {
      const numSelected = this.rowsSelection.selected.length;
      const numRows = this.dataSource.data.length;
      return numSelected === numRows;
    }
  
    /** Selects all rows if they are not all selected; otherwise clear rowsSelection. */
    masterToggle() {
      this.isAllSelected() ?
          this.rowsSelection.clear() :
          this.dataSource.data.forEach(row => this.rowsSelection.select(row));
    }


}


