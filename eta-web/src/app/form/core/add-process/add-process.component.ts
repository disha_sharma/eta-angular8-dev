import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpClient, HttpResponse, HttpEventType, HttpHeaders } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { ServiceService } from './../../../eta-services/service.service';
import { Observable } from 'rxjs/Observable';
import { MAT_DIALOG_DATA } from '@angular/material';


export interface ProcessData {
  id: "",
  PID:""

}
@Component({
  selector: 'app-add-process',
  templateUrl: './add-process.component.html',
  styleUrls: ['./add-process.component.css']
})


export class AddProcessComponent implements OnInit {

  activeYN: any;
  Core_URL: any;
  addfrm: FormGroup;
  gridProcessCol: any;
  public defaultColumnToSort: any;
  processObj: any;
  isEdit = false;
  processId: any;
  allFunction: any;
  gridProcessData: any;
  get_Data: any;
  isDataLoaded = false;
  processName: any;
  processDescription: any;
  acronym: any;
  displayOrder: any;
  ownerName: any;
  processImage: any;
  functionID: any;
  processList: any;


  value = 'Add New Process';

  getlogo: any;
  getProcessFileName: any;
  currentFileUpload: File;
  selectedFiles: FileList;
  progress: { percentage: number } = { percentage: 0 };
  imageName: any;


  constructor(private _formBuilder: FormBuilder, private http: HttpClient, private route: ActivatedRoute, private r: Router, private shareServices: ServiceService, @Inject(MAT_DIALOG_DATA) public popupData: ProcessData) {
    this.defaultColumnToSort = "processName"
    this.Core_URL = this.shareServices.Core_URL;
    this.getAllFunction();
    this.getAllProcess();
  }

  ngOnInit() {
    this.shareServices.currentMessage.subscribe(message => this.processObj = message);
    this.addfrm = this._formBuilder.group({
      processName: [],
      imageName: [],
      function: [],
      ownerName: [],
      displayOrder: [],
      acronym: [],
      processDescription: []
    })


    this.getAllProcessByProcessId(this.popupData.PID);
 
  }


  private getAllProcessByProcessId(proceessId:any) {
    this.shareServices.getAllProcessByProcessId(proceessId)
      .subscribe(res => {
        console.log(res);
        this.activeYN = res.activeYN;
        this.addfrm.patchValue({ processName: res.processName });
        this.addfrm.patchValue({ processDescription: res.processDescription });
        this.addfrm.patchValue({ acronym: res.acronym });
        this.addfrm.patchValue({ ownerName: res.ownerName });
        this.addfrm.patchValue({ displayOrder: res.displayOrder });
        //this.addfrm.patchValue({ processID: res.processID});

      })

  }
  private getAllFunction() {
    this.http.get(this.Core_URL + '/getAllFunction')
      .subscribe(data => {
        this.allFunction = data;
      });
  }

  public getAllProcess() {
    this.shareServices.getAllProcessList()
      .subscribe(data => {
        this.processList = data;
        
      });
  }


  public selectFile(event) {
    const file = event.target.files.item(0)

    if (file.type.match('image.*')) {
      this.selectedFiles = event.target.files;
    } else {
      alert('invalid format!');
    }
  }

  upload() {
    this.progress.percentage = 0;

    this.currentFileUpload = this.selectedFiles.item(0)
    this.processImage = this.currentFileUpload.name;
    console.log(this.processImage);

    this.shareServices.pushFileToStorage(this.currentFileUpload).subscribe(event => {
      if (event.type === HttpEventType.UploadProgress) {
        this.progress.percentage = Math.round(100 * event.loaded / event.total);
      }
      else if (event instanceof HttpResponse) {
        console.log('File is completely uploaded!');
      }

    })

    this.selectedFiles = undefined
  }


  public onSubmit() {
    let processObject: any = {};
    processObject['processName'] = this.addfrm.value.processName;
    processObject['processDescription'] = this.addfrm.value.processDescription;
    processObject['functionID'] = this.popupData.id;
    processObject['displayOrder'] = this.addfrm.value.displayOrder;
    processObject['ownerName'] = this.addfrm.value.ownerName;
    processObject['acronym'] = this.addfrm.value.acronym;
    processObject['processImage'] = this.processImage;
    processObject['processID'] = this.popupData.PID;
    processObject['activeYN'] = this.activeYN;
    console.log(processObject);

    this.http.post(this.Core_URL + '/saveOrUpdateProcess', processObject)
      .subscribe(data => {
      
      });
      

  }

  private clearShareObj() {
    this.shareServices.setgridChangeMessage(null);
  }


  public cancel() {
    this.clearShareObj();
    this.r.navigateByUrl('processdash');
  }

}







