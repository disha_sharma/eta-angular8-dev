import { Injectable } from '@angular/core'

@Injectable()
export class Addprocess {
processID: number;
functionID: number;
processName: string;
processDescription: string;
acronym: string;
displayOrder:string;
ownerName:string;
processImage:string;
//process_owner: string;

constructor(processID: number,functionID:number,processName: string, processDescription: string,acronym: string,displayOrder:string,ownerName:string,processImage:string){
    this.processID=processID;
    this.functionID=functionID;
    this.processName=processName;
    this.processDescription=processDescription;
    this.acronym=acronym;
    this.displayOrder=displayOrder;
    this.ownerName=ownerName;
    this.processImage=processImage;

}
}

export class ErrorDetails {
    result: boolean;
    message: string;
    details: string;
  
    constructor(result: boolean, message: string, details: string){
        this.result = result;
        this.message = message;
        this.details = details;
    } 
}