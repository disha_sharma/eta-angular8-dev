import { NgModel } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { ServiceService } from './../../../eta-services/service.service'

@Component({
  selector: 'app-create-dynamic-form',
  templateUrl: './create-dynamic-form.component.html',
  styleUrls: ['./create-dynamic-form.component.css']
})
export class CreateDynamicFormComponent implements OnInit {

  public Core_URL: any;
  public createdynamicfrm: FormGroup;
  public formobj: Object = {};
  public ElementType: any;
  public ElementSource: any
  public  ElementBehaviour: any;
  public DisplayElement: any;
  public DataObject: any;
  public createUsrId = 1;
  public dataElementEditObj: any;
  public isEdit = false;
  public  dataElementUrl;
  public dropdownList = [];
  public selectedItems = [];
  dropdownSettings = {};
  public dataContainers = [];
  public isDataContainersLoad = false;
  public gridTranslationColumns: any;
  public isDataLoaded = false;
  public  dataElementTransUrl: any;
  public get_Data: any;
  public  dataelementId: any;
  public gridDataElementColumns: any;
  public spdMstList: any;
  public defaultColumnToSort: any;
  public recordsTotal: number;

  constructor(private _formBuilder: FormBuilder, private http: HttpClient, private route: ActivatedRoute,
    private r: Router, private shareServices: ServiceService) {
    this.defaultColumnToSort = "createdDate";
    this.Core_URL = this.shareServices.Core_URL;
    this.dataElementUrl = this.Core_URL + '/getAllInnerDataElementByOrgIdGrid';
    this.getDataElementType();
    this.getDataElementSource();
    this.getDisplayElementBehaviour();
    this.getDisplayElement();
    this.getDataObject();
    this.getHeaderMenuList();
  }

  ngOnInit() {
    this.getDataElementColoumnList();
    this.createdynamicfrm = this._formBuilder.group({
      selectedObject:[],
      elementname: [],
      elementDescription: [],
      elementtype: [],
      mandatory: [],
      elementsource: [],
      elementbehaviour: [],
      displayelement: [],
      dataobject: [],
      displayDataElementOrder: [],
      spdMstTable: []
     
    })

    this.getAllDC();

    this.shareServices.currentMessage.subscribe(message => this.dataElementEditObj = message);

    /** only for edit */
    if (this.dataElementEditObj != null && this.dataElementEditObj != 'editFalse') {
     
      console.log(this.dataElementEditObj);
      console.log(this.dataElementEditObj.displayElementID);
      this.createdynamicfrm.patchValue({ elementname: this.dataElementEditObj.dataElementName });
      this.createdynamicfrm.patchValue({ elementDescription: this.dataElementEditObj.dataElementDescription });
      this.createdynamicfrm.patchValue({ elementtype: this.dataElementEditObj.dataElementDataTypeID });
      this.createdynamicfrm.patchValue({ mandatory: this.dataElementEditObj.displayDataElementIsMandatory });

      this.createdynamicfrm.patchValue({ elementsource: this.dataElementEditObj.dataElementSourceID });
      this.createdynamicfrm.patchValue({ elementbehaviour: this.dataElementEditObj.displayDataElementBehaviorID });
     // this.createdynamicfrm.patchValue({ displayelement: this.dataElementEditObj.displayElementID });
      this.createdynamicfrm.patchValue({ dataobject: this.dataElementEditObj.dataObjectHasMultipleDataElement });
      this.createdynamicfrm.patchValue({ displayDataElementOrder: this.dataElementEditObj.displayDataElementOrder });
      this.createdynamicfrm.get('displayelement').patchValue(this.dataElementEditObj.displayElementID);
     this.createdynamicfrm.patchValue({selectedObject:this.dataElementEditObj.dataObjectsIds})

      this.isEdit = true;
      this.dataelementId = this.dataElementEditObj.dataElementID;

    }
    else {
      this.isEdit = false;
    }
  }
  public getAllDC() {
    let listData;
    this.http.get(this.Core_URL + '/getAllDataObject').subscribe(data => {
      listData = data;
      listData.forEach(element => {
        let DO: any = {};
        DO.item_id = element.dataObjectID;
        DO.item_text = element.dataObjectName;
        this.dataContainers.push(DO);
      });
      if (this.isEdit == true) {
        let dataContainsers;
        this.http.get(this.Core_URL + '/getDataObjectByDataElementId/' + this.dataElementEditObj.dataElementID)
          .subscribe(data => {
            dataContainsers = data;
            this.dataContainers.forEach(el => {
              dataContainsers.forEach(element => {
                if (element.dataObjectID == el.item_id) {
                  this.selectedItems.push(el);
                }
              });
            });
            this.isDataContainersLoad = true;
          });
      } else {
        this.isDataContainersLoad = true;
      }
    });
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'item_id',
      textField: 'item_text',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      //itemsShowLimit: 3,
      allowSearchFilter: true
    };
  }

  public setAllSelectedDC() {
    this.selectedItems = [
      { item_id: 1, item_text: "data" }
    ];
  }

  public getIdsOfSelectedItem() {
    let ids = [];
    this.selectedItems.forEach(element => {
      ids.push(element.item_id);
    });
    return ids;
  }
  private getDataElementType() {
    this.http.get(this.Core_URL + '/getDataElementType')
      .subscribe(data => {
        this.ElementType = data;
      });
  }

  private getDataElementSource() {
    this.http.get(this.Core_URL + '/getDataElementSource')
      .subscribe(data => {
        this.ElementSource = data;
      });
  }

  private getDisplayElementBehaviour() {
    this.http.get(this.Core_URL + '/getDisplayElementBehaviour')
      .subscribe(data => {
        this.ElementBehaviour = data;
      });
  }

  private getDisplayElement() {
    this.http.get(this.Core_URL + '/getDisplayElement')
      .subscribe(data => {
        this.DisplayElement = data;
      });
  }
  private getDataObject() {
    this.http.get(this.Core_URL + '/getAllDataObject')
      .subscribe(data => {
        this.DataObject = data;
      });
  }

  public newDataObj() {
    this.r.navigateByUrl('createdataobject');
  }

  private clearShareObj() {
    this.shareServices.setgridChangeMessage(null);
  }

  public cancel() {
    this.clearShareObj();
    this.r.navigateByUrl('dataelementdashboard');
  }

  onSubmit() {
    if (this.isEdit) {
      this.dataElementEditObj.dataElementName = this.createdynamicfrm.value.elementname
      this.dataElementEditObj.dataElementDescription = this.createdynamicfrm.value.elementDescription
      this.dataElementEditObj.dataElementDataTypeID = this.createdynamicfrm.value.elementtype
      this.dataElementEditObj.displayDataElementIsMandatory = this.createdynamicfrm.value.mandatory

      this.dataElementEditObj.dataElementSourceID = this.createdynamicfrm.value.elementsource
      this.dataElementEditObj.displayDataElementBehaviorID = this.createdynamicfrm.value.elementbehaviour
      this.dataElementEditObj.displayElementID = this.createdynamicfrm.value.displayelement
      this.dataElementEditObj.dataObjectsIds = this.createdynamicfrm.value.selectedObject;
      this.dataElementEditObj.displayDataElementOrder = this.createdynamicfrm.value.displayDataElementOrder

      this.http.post(this.Core_URL + '/updateDataElement', this.dataElementEditObj)
        .subscribe(data => {
          this.get_Data = data;
          this.dataelementId = this.get_Data.dataElementID;
          if (this.dataelementId != null) {
            alert("Updated successfully !! Please add translations")
            this.gridDataElementColumns = null;
            this.getDataElementColoumnList();
          } else {
            alert("data not saved")
          }
        });
    } else {
      this.formobj["dataElementName"] = this.createdynamicfrm.value.elementname
      this.formobj["dataElementDescription"] = this.createdynamicfrm.value.elementDescription
      this.formobj["dataElementDataTypeID"] = this.createdynamicfrm.value.elementtype
      this.formobj["displayDataElementIsMandatory"] = this.createdynamicfrm.value.mandatory

      this.formobj["dataElementSourceID"] = this.createdynamicfrm.value.elementsource
      this.formobj["displayDataElementBehaviorID"] = this.createdynamicfrm.value.elementbehaviour
      this.formobj["displayElementID"] = this.createdynamicfrm.value.displayelement
      this.formobj["dataObjectsIds"] =  this.createdynamicfrm.value.selectedObject;
      this.formobj["displayDataElementOrder"] = this.createdynamicfrm.value.displayDataElementOrder
      this.formobj["spdMstTableName"] = this.createdynamicfrm.value.spdMstTable

      this.http.post(this.Core_URL + '/updateDataElement/', this.formobj)
        .subscribe(data => {
          this.get_Data = data;
          this.dataelementId = this.get_Data.dataElementID;
          if (this.dataelementId != null) {
            alert("Updated successfully !! Please add translations")
            this.gridDataElementColumns = null;
            this.getDataElementColoumnList();
            this.clearShareObj();
          } else {
            alert("data not saved")
          }
        });
    }
  }
  public translationColumns() {
    if (this.dataelementId != null) {
      let coloumNames: any;
      let myColoumsObj: any;
      this.http.get(this.Core_URL + '/getCoreGridHeaderLanguageByOrgId')
        .subscribe(data => {
          myColoumsObj = data;
          coloumNames = myColoumsObj.coloumnName;
          if (Object.keys(coloumNames).length > 0) {
            this.gridTranslationColumns = coloumNames;
          }
          this.isDataLoaded = true;
          this.dataElementTransUrl = this.Core_URL + '/getDataElementLanguageInfo/' + this.dataelementId;
        });
    } else {
      alert("please add DataElement");
    }
  }

  public getTranslationEditObject(event: any) {
    let translationEditObj = {};
    translationEditObj["id"] = event.obj.id;
    translationEditObj["versionId"] = event.obj.versionId;
    translationEditObj["languageId"] = event.languageId;
    translationEditObj["orgId"] = event.obj.id;
    translationEditObj["columnName"] = event.obj.field;
    translationEditObj["columnValue"] = event.newEditValue;
    this.http.post(this.Core_URL + '/updateElementLanguage', translationEditObj)
      .subscribe(data => {
      });
  }

  public closePopUp() {
    this.isDataLoaded = false;
  }

  private getDataElementColoumnList() {
    this.shareServices.getCoreInnerDataElementColumn()
      .subscribe(data => {
        this.gridDataElementColumns = data.coloumnName;
        this.recordsTotal = data.recordsTotal;
      });
  }

  public mainbreadcrumb: string;
  public breadcrumb: string;
  private menuList: any = [];
  public getHeaderMenuList() {
    this.shareServices.getHeaderMenu()
      .subscribe(res => {
        // console.log(res);
        this.menuList = res;
        this.menuList.forEach(element => {
          if (element.routerLink == "eta-web/dashboardDataObject") {
            this.mainbreadcrumb = element.translation;
          }
          if (element.routerLink == "eta-web/dataelementdashboard") {
            this.breadcrumb = element.translation;
          }
        })

      })
  }

  selectAll(list) {
    console.log(list);
    console.log(this.createdynamicfrm.get('selectedObject'));
      let tempList: any = [];
      list.forEach(element => {
        tempList.push(element.item_id);
      });
    
    this.createdynamicfrm.get('selectedObject').patchValue(tempList)
  }
  deselectAll() {
    this.createdynamicfrm.get('selectedObject').patchValue([])
  }

  public displayElementDropDownChange(changevalue: any) {
    console.log(changevalue);
    console.log(this.createdynamicfrm.get('displayelement'));
    
    
    if (changevalue == this.shareServices.DROP_DOWN || changevalue == this.shareServices.MULTI_SELECT) {
      console.log(changevalue);
      this.shareServices.getAllSpdMstList()
        .subscribe(res => {
          this.spdMstList = res;
          console.log(res);
        })
    }
  }
  public addCustomTable(){
    this.clearShareObj();
    this.r.navigateByUrl('cuntomtable');
  }
}

