import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddTaskflowComponent } from './add-taskflow.component';

describe('TaskflowComponent', () => {
  let component: AddTaskflowComponent;
  let fixture: ComponentFixture<AddTaskflowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddTaskflowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddTaskflowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
