import { Injectable } from '@angular/core'

@Injectable()
export class AddTaskflow {
    taskFlowID: number;
    processID: string;
    taskFlowName: string;
    reusable: boolean;
    constructor(processId: string, taskFlowId:number, taskFlowName: string, reusable: boolean){
        this.processID = processId;
        this.taskFlowID =  taskFlowId;
        this.taskFlowName = taskFlowName;
        this.reusable = reusable;
    } 
    
}
