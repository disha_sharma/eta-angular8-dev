import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ServiceService } from './../../../eta-services/service.service';
import { AddTaskflow } from './add-taskflow';
import { ActivatedRoute, Router } from '@angular/router';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { AddTaskComponent } from '../../../popup/add-task/add-task.component';
import { HttpClient, HttpRequest, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { FormGroup, FormBuilder, Validator, Validators, FormControl } from '@angular/forms';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead';
import { mergeMap } from 'rxjs/operators';
import { TaskServiceService } from './../../../dash-board/task-service.service';
//import { forEach } from '@angular/router/src/utils/collection';
import { taskFlowNameValidator } from '../add-task-detail/taskName.validation';


@Component({
  selector: 'app-add-taskflow',
  templateUrl: './add-taskflow.component.html',
  styleUrls: ['./add-taskflow.component.css']
})
export class AddTaskflowComponent implements OnInit {

  public taskFlowName : string = '';
  public processList: any=[];
  public acronym: string;
  public taskList: any=[];
  public taskFlowId: number;
  public reusable: boolean = false;
  public taskEditObj:any={};
  public Id: boolean;
  public processId: string;
  public taskFlowList: any=[];
  
  showAddMessage : boolean = false;
  
  /** Type ahead Validation */
  typeaheadLoading: boolean;
  typeaheadLoadingTaskFlow: boolean;
  dataSource: Observable<any>;
  dataSourceTaskFlow: Observable<any>;

  /** For BreadCrumb Menu Display */
  public mainbreadcrumb: string;
  public breadcrumb: string;
  public menuList: any = [];
  
  currentTaskFlowId: string;  
  public duplicateTaskFlowName: boolean = false;
  isLoadingResults : boolean = false;
  //taskFlowControl = new FormControl(); 
      taskFlowForm = new FormGroup({
          taskFlowNameControl: new FormControl()
      });
      
  
  constructor(private sharedService: ServiceService, private router: Router, public dialog: MatDialog, public activeRouter : ActivatedRoute, 
              public httpClient: HttpClient, public taskService: TaskServiceService, private formBuilder: FormBuilder){ 
    
    this.getHeaderMenuList();
    this.getAllProcessNamesList();
    this.taskFlowFormValidation(); 
       
    /** For Typeahead Search */
    this.dataSourceTaskFlow = Observable.create((observer: any) => {
      observer.next(this.taskFlowName); // Runs on every search
    })
    .pipe(
      mergeMap((token: string) => this.getStatesAsObservableTaskFlow(token))
    );

    
  
  }

  ngOnInit(){
    this.processId = this.taskService.showActiveCss;
    this.activeRouter.queryParams.subscribe(params =>{
      //this.processId = params["processId"];    
      console.log("Process Id ad ngOnInit : "+this.processId);        
    });

    /** This will Fetch all the Task Flow Names for Typeahead */
    this.taskService.getTaskFlowNameList().subscribe(data =>{
      for(let s of data) {
        this.taskFlowList.push(s);
      }
      console.log(this.taskFlowList);
      
    });

  }
 

  public saveTaskFlow(){
    
    let addTaskFlow : AddTaskflow= new AddTaskflow(this.processId, this.taskFlowId, this.taskFlowName, this.reusable);
    console.log("ProcessId::: " + addTaskFlow.processID);
    console.log("taskFlowName::: " + addTaskFlow.taskFlowName);
    console.log("Reusable::: " + addTaskFlow.reusable);
    
    this.sharedService.saveTaskFlow(addTaskFlow).subscribe(data=>{
        console.log(data); 
      
        this.taskFlowId = data.taskFlowID;
        console.log(this.taskFlowId); 
        this.taskService.taskFlowName = data.taskFlowName;
        console.log("Task Flow Name : "+ this.taskService.taskFlowName );
        
        //this.taskService.getTaskFlowNameByName = data.taskFlowName;  
        //console.log(this.taskService.getTaskFlowNameByName);       
    },error =>{
      console.log("Error in adding Task Flow.");
      console.log(error);
    }); 
  }
 
  public cancel(){
    this.router.navigate(['eta-web/taskflowdashboard'],{ queryParams: { action:'cancel',  processId:  this.processId}})
  }

  getAllProcessNamesList(){
    this.taskService.getAllProcessNamesList().subscribe(data => {
      this.processList = data;
      this.processList.forEach(element => { 
        if (this.processId == element.processID) {  
          this.acronym = element.acronym;               
        }
      });         
    })
  }


  checkOnKeyUpName(){

    this.taskFlowForm.controls['taskFlowNameControl'].markAsTouched();
    for(let s of this.taskFlowList) {
      JSON.parse(JSON.stringify(s), (key, value) => {
        if(key === 'taskFlowName'){
          if(value.trim().length === this.taskFlowName.trim().length && value.trim() === this.taskFlowName.trim()){
            this.taskFlowForm.controls['taskFlowNameControl'].setErrors({'invalid': true});
            this.taskFlowForm.controls['taskFlowNameControl'].setErrors({ 'duplicateTaskFlowName': true });
            // this.duplicateTaskFlowName=true;    
          }
        }
      });       
    } 
  }
   /** For Save New Tak Flow */
  onSave(event){ 
    this.isLoadingResults = true;
    //this.duplicateTaskFlowName = false;
     // this.omit_special_char(event);
      if(!this.taskFlowForm.valid){
        return;
      }else{
        console.log("Task Flow Form inputs are valid :"+true);      
        this.validateAllTaskFLowFormFields(this.taskFlowForm);
      }

      if(this.reusable == false){    
        this.Id = false;
      }
      else{
        this.Id = true;
      }

      //this.saveTaskFlow();

      let addTaskFlow : AddTaskflow= new AddTaskflow(this.processId, this.taskFlowId, this.taskFlowName, this.reusable);
    this.sharedService.saveTaskFlow(addTaskFlow).subscribe(data=>{
      if(data){
        this.isLoadingResults = false;
        this.taskFlowId = data.taskFlowID;
        this.taskService.taskFlowName = data.taskFlowName;
        this.router.navigate(['eta-web/taskflowdashboard'],{ queryParams: { action:'submit', processId:  this.processId, taskFlowName : this.taskFlowName }})
        //this.taskService.getTaskFlowNameByName = data.taskFlowName;  
        //console.log(this.taskService.getTaskFlowNameByName);       
      }
    }); 
    


  } //End Save Method

  openDialog(action): void {
    this.router.navigate(["/taskdetail", action,this.processId, this.taskFlowId]);
  } 

  /******************************** Display Breadcrumb Menu ************************************** */
  getHeaderMenuList() {
    this.sharedService.getHeaderMenu()
      .subscribe(res => {
        this.menuList = res;

        this.menuList.forEach(element => {
          if (element.routerLink == "#2") {
            this.mainbreadcrumb = element.translation;
          }
          if (element.routerLink == "eta-web/taskflowdashboard") {
            this.breadcrumb = element.translation;
          }
        })

      })
  }
  public activeLink = 'taskflowdashboard';
  background = '';

  toggleBackground() {
    this.background = this.background ? '' : 'primary';
  }
  shownRouterLink(routerlink) {
    this.router.navigate(["/" + routerlink]);
  }

  /*************************** Omit Special Characters as Input ***************************/  
  public omit_special_char(event)
  {   
    var k;  
    k = event.charCode;  //k = event.keyCode;  (Both can be used)
    return((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57)); 
  }

  /**************************** TaskFlow List For Typeahead ********************************************************** */
  getStatesAsObservableTaskFlow(token: string): Observable<any> {
    const query = new RegExp(token, 'i'); 
    return of(
      this.taskFlowList.filter((str: any) => {
        return query.test(str.taskFlowName);
      })
    );
  }

  changeTypeaheadLoadingTaskFlow(e: boolean): void {
    this.typeaheadLoadingTaskFlow = e;
  }
 
  typeaheadOnSelectTaskFlow(e: TypeaheadMatch): void {
  //  this.taskFlowName = e.value;
    for(let s of this.taskFlowList){
      JSON.parse(JSON.stringify(s), (key, value) => {
        if(key === 'taskFlowName'){
          if(value.trim().length === this.taskFlowName.trim().length && value.trim() === this.taskFlowName.trim()){
            this.taskFlowForm.controls['taskFlowNameControl'].setErrors({'invalid': true});
            this.taskFlowForm.controls['taskFlowNameControl'].markAsTouched();
            this.taskFlowForm.controls['taskFlowNameControl'].setErrors({ 'duplicateTaskFlowName': true });
            // this.duplicateTaskFlowName=true;    
          }
        }
      });       
    }
  }
   
  taskFlowFormValidation() {
    this.taskFlowForm = this.formBuilder.group({
      taskFlowNameControl: ['', [Validators.required, taskFlowNameValidator]],
    });
  }

  validateAllTaskFLowFormFields(formGroup: FormGroup){         
      Object.keys(formGroup.controls).forEach(field =>{  
          const control = formGroup.get(field);             
          if (control instanceof FormControl){             
            control.markAsTouched({ onlySelf: true });
          }else if (control instanceof FormGroup){        
            this.validateAllTaskFLowFormFields(control);            
          }
      });
  }
 /*cards slider code starts */
 cards = [

  {heading: 8, description: 'Manager Review in GFP containing 3 Tasks. And other thing'},
  {heading: 124, description: 'Escalation Flow in SAP containing 7 Tasks. And other thing'},
  {heading: 36, description: 'Reminder Notification Flow in System containing 2 Tasks.'},
  {heading: 24, description: 'Scheduler Flow in System containing 4 Tasks. And many things'},
  {heading: 8, description: 'Validate Address in AFP containing 12 Tasks. And many things'},
  {heading: 9, description: 'Validate Address in AFP containing 12 Tasks. And many things'},
  {heading: 8, description: 'Manager Review in GFP containing 3 Tasks. And other thing'},
  {heading: 124, description: 'Escalation Flow in SAP containing 7 Tasks. And other thing'},
  {heading: 36, description: 'Reminder Notification Flow in System containing 2 Tasks.'},
  {heading: 24, description: 'Scheduler Flow in System containing 4 Tasks. And many things'},
  {heading: 8, description: 'Validate Address in AFP containing 12 Tasks. And many things'},
  {heading: 9, description: 'Validate Address in AFP containing 12 Tasks. And many things'},
  
]
  cardsScrollForwardButtonFlag: boolean;
  cardsScrollBackButtonFlag: boolean;
  @ViewChild('cardsContainer', { read: ElementRef, static: true }) public cardsContainer: ElementRef<any>;
  @ViewChild('cardsScrollableContent', { read: ElementRef, static: true }) public cardsScrollableContent: ElementRef<any>;
  onWindowResized(event) {
    this.toggleScrollButtonsForCards();
  }
  toggleScrollButtonsForCards(){
    if(this.cardsScrollableContent.nativeElement.clientWidth > this.cardsContainer.nativeElement.clientWidth){
  this.cardsScrollForwardButtonFlag = true;
    }
    else{
      this.cardsScrollForwardButtonFlag = false;
      this.cardsScrollBackButtonFlag = false;
    }
  }
  cardsScrollBackFunction(){
    this.cardsContainer.nativeElement.scrollTo({ left: (this.cardsContainer.nativeElement.scrollLeft -300), behavior: 'smooth' });
    this.cardsScrollForwardButtonFlag = true;
    if(this.cardsContainer.nativeElement.scrollLeft < 301){
         this.cardsScrollBackButtonFlag = false;
       }
    }
    
    cardsScrollForwardButton(){
      this.cardsContainer.nativeElement.scrollWidth;
    this.cardsContainer.nativeElement.scrollTo({ left: (this.cardsContainer.nativeElement.scrollLeft + 300), behavior: 'smooth' });
    this.cardsScrollBackButtonFlag = true;
    if(this.cardsContainer.nativeElement.offsetWidth + this.cardsContainer.nativeElement.scrollLeft > (this.cardsContainer.nativeElement.scrollWidth - 301)){
                this.cardsScrollForwardButtonFlag = false;
             }
    }
    ngAfterViewInit(){
      setTimeout(()=> {
        this.toggleScrollButtonsForCards();
    }, 0);
    }
    /*cards slider code ends */
}
