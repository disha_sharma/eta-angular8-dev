import { Component, OnInit, ViewChild, ElementRef, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { TaskServiceService } from './../../../dash-board/task-service.service';
import { ServiceService } from './../../../eta-services/service.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog, MatDialogConfig, MatTableDataSource } from '@angular/material';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead';
import { mergeMap } from 'rxjs/operators';
import { AddTaskflow } from './../add-taskflow/add-taskflow';
import { AddTaskComponent } from '../../../popup/add-task/add-task.component';
import { FormGroup, FormBuilder, Validator, Validators, FormControl } from '@angular/forms';
import { AddTaskDetailPopupComponent } from '../../../popup/add-task-popup/add-task-detail-popup.component';
import { AddTaskDetailComponent } from '../add-task-detail/add-task-detail.component';
import { SelectionModel } from '@angular/cdk/collections';
import { DashboardServiceService } from '../../../dash-board/dashboard-service.service';
//import { LoginComponent } from '../../login/login.component';

@Component({
  selector: 'app-edit-taskflow',
  templateUrl: './edit-taskflow.component.html',
  styleUrls: ['./edit-taskflow.component.css']
})
export class EditTaskflowComponent implements OnInit {


  public taskFlowName: string = '';
  taskFlowRealName: string = '';
  /** For BreadCrumb Menu Display **/
  public mainbreadcrumb: string;
  public breadcrumb: string;
  public menuList: any = [];

  /** Type ahead Validation */
  typeaheadLoading: boolean;
  typeaheadLoadingTaskFlow: boolean;
  dataSource: Observable<any>;
  dataSourceTaskFlow: Observable<any>;
  taskID: number;
  index: number;

  public currentTaskFlowId: number;
  public process_Id: string;
  public taskFlowList: any = [];
  public taskList: any = [];

  public reusable: boolean;
  public taskFlowId: number;

  /** To Display Process Acronym */
  public processList: any = [];
  //public processId: number;
  public acronym: string;

  public taskEditObj: any = {};
  public Id: boolean;
  orgId: number;

  successUpdated: boolean = false;
  showSuccess: boolean = false;
  displayOrder: number;
  taskName: string;
  newTaskName: string;

  public isSpinnerShow: boolean = false;
  isLoadingResults = false;
  public responseMsg: any = {};
  public duplicateTaskFlowName: boolean = false;
  updatedName: string = "";
  submitButtonDisabled: boolean = true;
  taskFlowControl = new FormControl();
  taskFlowForm = new FormGroup({
    taskFlowNameControl: this.taskFlowControl
  });

  constructor(private sharedService: ServiceService, private router: Router, public dialog: MatDialog, public activeRouter: ActivatedRoute,
    public httpClient: HttpClient, public taskService: TaskServiceService, private formBuilder: FormBuilder,
    private dashboardService: DashboardServiceService) {

    this.process_Id = this.activeRouter.snapshot.params['processId'];
    this.getHeaderMenuList();
    this.getAllProcessNamesList();

    /** For Typeahead Search */
    this.dataSourceTaskFlow = Observable.create((observer: any) => {
      observer.next(this.taskFlowName); // Runs on every search
    })
      .pipe(
        mergeMap((token: string) => this.getStatesAsObservableTaskFlow(token))
      );

    this.taskFlowFormValidation();

  }

  ngOnInit() {
    // this.getData();

    // console.log(this.que);

    this.process_Id = this.taskService.showActiveCss;
    this.currentTaskFlowId = this.activeRouter.snapshot.params['taskFlowId'];

    this.getAllTaskByTaskFlowId(this.currentTaskFlowId);

    /** For Displaying Process Acronym Name in Edit Page */
    this.processList.forEach(element => {
      if (this.process_Id == element.processID) {
        this.acronym = element.acronym;
      }
    }); /** End For Displaying Process Acronym Name in Edit Page */


    /** This will Fetch all the Task Flow Names for Typeahead */
    this.taskService.getTaskFlowNameList().subscribe(data => {
      for (let s of data) {
        console.log(s);

        this.taskFlowList.push(s);
      }
    });

    /************************ Get TaskFlowDetailsByTaskFlowId  Method Call ****************************/
    this.taskService.getTaskFlowDetailsByTaskFlowId(this.currentTaskFlowId).subscribe(data => {
      this.taskFlowList = [data];
      this.process_Id = data.processID;
      this.currentTaskFlowId = data.taskFlowID;
      this.taskFlowName = data.taskFlowName;
      this.taskFlowRealName = data.taskFlowName;
      this.reusable = data.reusable;
    });

    /************************ Get TaskFlowDetailsByTaskFlowId  Method Call ****************************/
    this.taskService.getAllTaskByTaskFlowId(this.currentTaskFlowId).subscribe(data => {
      this.taskList = data;
    });

  } //End OnIt
  selection = new SelectionModel(true, []);

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.taskList.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.taskList.forEach(row => this.selection.select(row));
  }
  /******************************** Display Breadcrumb Menu ************************************** */
  getHeaderMenuList() {
    this.sharedService.getHeaderMenu().subscribe(res => {
      this.menuList = res;
      this.menuList.forEach(element => {
        if (element.routerLink == "#2") {
          this.mainbreadcrumb = element.translation;
        }
        if (element.routerLink == "eta-web/taskflowdashboard") {
          this.breadcrumb = element.translation;
        }
      })
    });
  }

  editTaskDetail(id: number): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      taskflowId: this.currentTaskFlowId,
      action: 'Edit',
      id: id
    };
    const dialogRef = this.dialog.open(AddTaskDetailComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      this.submitButtonDisabled = false;
      if (!result) {
        return;
      }
    });
  }


  /**************************** Get All Task By TaskFlowId ********************************************************** */
  getAllTaskByTaskFlowId(currentTaskFlowId) {
    this.sharedService.getAllTaskByTaskFlowId(currentTaskFlowId).subscribe(data => {


      this.taskList = data;
      // console.log(this.taskList);
      this.taskList.forEach(element => {
        this.currentTaskFlowId = element.taskFlowID;
      });
    });
  }

  /** Cancel and Back to the TaskFlow Dashboard */
  public cancel() {
    if (this.taskID != undefined && this.index != undefined) {
      console.log(this.taskID, this.index);

      this.deleteTask(this.index, this.taskID);
    }
    this.router.navigate(['/eta-web/taskflowdashboard'], { queryParams: { action: 'cancel', processId: this.process_Id } })
    console.log(this.process_Id);

  }

  checkOnKeyUpName(event) {
    this.taskFlowName = event.target.value;
    this.taskFlowName = event.target.value;
    var taskFlowname = event.target.value.trim();
    var taskFlowEdit = this.taskFlowRealName.trim();
    for (let s of this.taskFlowList) {
      JSON.parse(JSON.stringify(s), (key, value) => {
        if (key === 'taskFlowName') {
          var value = value.trim();
          if (taskFlowname.length === taskFlowEdit.length && taskFlowname.toLowerCase() === taskFlowEdit.toLowerCase()) {
            this.duplicateTaskFlowName = false;
          } else if (taskFlowname.length != taskFlowEdit.length && taskFlowname.toLowerCase() != taskFlowEdit.toLowerCase() && value.length === taskFlowname.length && value.toLowerCase() === taskFlowname.toLowerCase()) {
            this.taskFlowForm.controls['taskFlowNameControl'].setErrors({ 'invalid': true });
            this.taskFlowForm.controls['taskFlowNameControl'].markAsTouched();
            this.duplicateTaskFlowName = true;
          }
        }
      });
    }
  }

  checkCharValidationForName(value) {
    var str = value.target.value;
      if (str) {
      const matches = str.match(/^[A-Za-z][A-Za-z0-9/.,->_()\\*|:" ]*$/);
      return matches?null: this.taskFlowForm.controls['taskFlowNameControl'].setErrors({ 'patternError': true });
      }
  }

  /** onEdit function will update data after Edit */
  onEditSubmitButton(event) {
    //this.omit_special_char(event);
    //this.duplicateTaskFlowName = false;

    /*for(let s of this.taskFlowList){
      JSON.parse(JSON.stringify(s), (key, value) => {
        if(key === 'taskFlowName'){
          if(value.trim().length === this.taskFlowName.trim().length && value.trim() === this.taskFlowName.trim()){
            this.taskFlowForm.controls['taskFlowNameControl'].setErrors({'invalid': true});
            this.taskFlowForm.controls['taskFlowNameControl'].markAsTouched();
            this.duplicateTaskFlowName=true;    
          }
        }
      });       
    }*/

    this.isLoadingResults = true;
    if (this.duplicateTaskFlowName) {
      this.submitButtonDisabled = true;
      return false;
    }
    if (!this.taskFlowName) {
      alert(" Task Flow Name field is blank.");
      return;
    } else {
      this.validateAllTaskFLowFormFields(this.taskFlowForm);
    }
    if (this.reusable == false) {
      this.Id = false;
    }
    else {
      this.Id = true;
    }
    let addTaskFlow: AddTaskflow = new AddTaskflow(this.process_Id, this.currentTaskFlowId, this.taskFlowName, this.reusable);
    this.taskService.updateTaskFlowDetails(addTaskFlow).subscribe(data => {
      this.taskFlowId = data.taskFlowID;
      this.updatedName = data.taskFlowName;
      this.isLoadingResults = false;

      this.router.navigate(['/eta-web/taskflowdashboard'], { queryParams: { action: 'update', processId: this.process_Id, updatedName: this.updatedName, taskName: this.newTaskName } });
    });
    // this.getAllTaskByTaskFlowId(this.currentTaskFlowId);
    // this.showSuccess=true;
  } //Update Task Flow Method End  

  /************************************** Get all Process Details *************************/
  public getAllProcessNamesList() {
    this.taskService.getAllProcessNamesList().subscribe(data => {
      this.processList = data;
      this.processList.forEach(element => {
        if (this.process_Id == element.processID) {
          this.acronym = element.acronym;
          console.log(this.acronym);
        }
      });
    })
  }


  openAddTaskDialog(action): void {
    this.resetAll();
    console.log(this.currentTaskFlowId);

    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    dialogConfig.data = {
      taskflowId: this.currentTaskFlowId,
      action: action,

    };
    const dialogRef = this.dialog.open(AddTaskDetailComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      console.log('result', result)
      this.sharedService.getAllTaskByTaskFlowId(this.currentTaskFlowId).subscribe(data => {
        this.taskList = data;
        for (let i = 0; i < this.taskList.length; i++) {

          if (result.data == this.taskList[i].taskName) {
            this.index = i;
            this.taskID = this.taskList[i].taskID
            console.log(this.index, this.taskID);

          }
        }
      });
      if (result != undefined) {
        if (result.status == true) {
          this.showSuccess = true;
          this.submitButtonDisabled = false;
          this.newTaskName = result.data;
          //   setTimeout(()=> {
          //     this.showSuccess=false;
          // }, 3000);

        }
      }
    }
    );
  }

  removeSuccess() {
    this.showSuccess = false;
  }
  public resetAll() {
    this.showSuccess = false;
  }

  /*openAddTaskDialog(action): void {
    this.router.navigate(["/taskdetail", action,this.process_Id, this.currentTaskFlowId]);
    } */

  /*************************** Omit Special Characters as Input ***************************/
  public omit_special_char(event) {
    var k;
    k = event.charCode;  //k = event.keyCode;  (Both can be used)
    return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57));
  }


  /**************************** TaskFlow List For Typeahead ********************************************************** */
  getStatesAsObservableTaskFlow(token: string): Observable<any> {
    const query = new RegExp(token, 'i');
    return of(
      this.taskFlowList.filter((str: any) => {
        return query.test(str.taskFlowName);
      })
    );
  }

  changeTypeaheadLoadingTaskFlow(e: boolean): void {
    this.typeaheadLoadingTaskFlow = e;
  }

  typeaheadOnSelectTaskFlow(e: TypeaheadMatch): void {
    this.taskFlowName = e.value;
    for (let s of this.taskFlowList) {
      JSON.parse(JSON.stringify(s), (key, value) => {
        if (key === 'taskFlowName') {
          if (this.taskFlowName.trim().length === this.taskFlowRealName.trim().length && this.taskFlowName.trim() === this.taskFlowRealName.trim()) {
            this.duplicateTaskFlowName = false;
          } else if (this.taskFlowName.trim().length != this.taskFlowRealName.trim().length && this.taskFlowName.trim() != this.taskFlowRealName.trim() && value.trim().length === this.taskFlowName.trim().length && value.trim() === this.taskFlowName.trim()) {
            this.taskFlowForm.controls['taskFlowNameControl'].setErrors({ 'invalid': true });
            this.taskFlowForm.controls['taskFlowNameControl'].markAsTouched();
            this.duplicateTaskFlowName = true;
          }
        }
      });
    }
  }




  taskFlowFormValidation() {
    this.taskFlowForm = this.formBuilder.group({
      taskFlowNameControl: ['', [Validators.required, Validators.pattern('^[a-zA-Z0-9 ]*$')]],
    });
  }

  validateAllTaskFLowFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllTaskFLowFormFields(control);
      }
    });
  }
  enableSubmitButton() {
    this.submitButtonDisabled = false;
  }


  /*cards slider code starts */
  cards = [

    { heading: 8, description: 'Manager Review in GFP containing 3 Tasks. And other thing' },
    { heading: 124, description: 'Escalation Flow in SAP containing 7 Tasks. And other thing' },
    { heading: 36, description: 'Reminder Notification Flow in System containing 2 Tasks.' },
    { heading: 24, description: 'Schedular Flow in System containing 4 Tasks. And many things' },
    { heading: 8, description: 'Validate Address in AFP containing 12 Tasks. And many things' },
    { heading: 9, description: 'Validate Address in AFP containing 12 Tasks. And many things' },
    { heading: 8, description: 'Manager Review in GFP containing 3 Tasks. And other thing' },
    { heading: 124, description: 'Escalation Flow in SAP containing 7 Tasks. And other thing' },
    { heading: 36, description: 'Reminder Notification Flow in System containing 2 Tasks.' },
    { heading: 24, description: 'Schedular Flow in System containing 4 Tasks. And many things' },
    { heading: 8, description: 'Validate Address in AFP containing 12 Tasks. And many things' },
    { heading: 9, description: 'Validate Address in AFP containing 12 Tasks. And many things' },

  ]
  cardsScrollForwardButtonFlag: boolean;
  cardsScrollBackButtonFlag: boolean;
  @ViewChild('cardsContainer', { read: ElementRef, static: true }) public cardsContainer: ElementRef<any>;
  @ViewChild('cardsScrollableContent', { read: ElementRef, static: true }) public cardsScrollableContent: ElementRef<any>;
  onWindowResized(event) {
    this.toggleScrollButtonsForCards();
  }
  toggleScrollButtonsForCards() {
    if (this.cardsScrollableContent.nativeElement.clientWidth > this.cardsContainer.nativeElement.clientWidth) {
      this.cardsScrollForwardButtonFlag = true;
    }
    else {
      this.cardsScrollForwardButtonFlag = false;
      this.cardsScrollBackButtonFlag = false;
    }
  }
  cardsScrollBackFunction() {
    this.cardsContainer.nativeElement.scrollTo({ left: (this.cardsContainer.nativeElement.scrollLeft - 300), behavior: 'smooth' });
    this.cardsScrollForwardButtonFlag = true;
    if (this.cardsContainer.nativeElement.scrollLeft < 301) {
      this.cardsScrollBackButtonFlag = false;
    }
  }

  cardsScrollForwardButton() {
    this.cardsContainer.nativeElement.scrollWidth;
    this.cardsContainer.nativeElement.scrollTo({ left: (this.cardsContainer.nativeElement.scrollLeft + 300), behavior: 'smooth' });
    this.cardsScrollBackButtonFlag = true;
    if (this.cardsContainer.nativeElement.offsetWidth + this.cardsContainer.nativeElement.scrollLeft > (this.cardsContainer.nativeElement.scrollWidth - 301)) {
      this.cardsScrollForwardButtonFlag = false;
    }
  }
  ngAfterViewInit() {
    setTimeout(() => {
      this.toggleScrollButtonsForCards();
    }, 0);
  }
  /*cards slider code ends */
  deleteTask(index, taskId) {
    this.taskService.deleteTask(taskId).subscribe(data => {
      console.log('deleted')
    })
    this.taskList.splice(index, 1)
  }


}

