import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditTaskflowComponent } from './edit-taskflow.component';

describe('EditTaskflowComponent', () => {
  let component: EditTaskflowComponent;
  let fixture: ComponentFixture<EditTaskflowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditTaskflowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditTaskflowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
