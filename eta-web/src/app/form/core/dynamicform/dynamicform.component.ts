import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { FormGroup, FormControl, FormArray, NgForm, FormBuilder, Validators } from '@angular/forms';
import { Http, Headers, RequestOptions } from '@angular/http';
import { CookieService } from 'ngx-cookie-service';
import { ServiceService } from './../../../eta-services/service.service'
import { NgModel } from '@angular/forms';
//import { text } from '@angular/core/src/render3/instructions';
import { Router, ActivatedRoute } from '@angular/router';
import { DashboardServiceService } from '../../../dash-board/dashboard-service.service';

@Component({
  selector: 'app-dynamicform',
  templateUrl: './dynamicform.component.html',
  styleUrls: ['./dynamicform.component.css']
})

export class DynamicformComponentPublish implements OnInit {

  private Core_URL: any;
  public userData: object;
  public data: any = {};
  private functionId: any;
  private processId: any;
  private taskId: any;
  private FormId: any;

  private editFormObj: any;
  private URL: any;
  public currentTaskJson: any = [];
  public allTaskJson: any;
  public CountryList: any;

  public listObj: any = {};

  /** Privlege Multiselect Property  */
  public dropdownList: any = [];
  /** Privlege Multiselect Property  */
  public selectedItems: any = [];
  userFormData: FormGroup;

  public selectedFiles: FileList;
  public imageName: any;
  public currentFileUpload: File;

  /* Testing dynamic form */
  form: FormGroup;

  fileUploaded: string = "";
  fileUploadButton: boolean = false;
  progressBarValue = 0;
  fileValue: string = "";

  private taskFlowId: any;

  constructor(private route: ActivatedRoute, private http: HttpClient, private cookieService: CookieService, private shareServices: ServiceService,
    private fb: FormBuilder, private router: Router, private dashboardService: DashboardServiceService) {
    this.Core_URL = this.shareServices.Core_URL;
    this.getAllPrivilegeList();

  }
  //city = 'myCities'
  // cities = ['one', 'two', 'three']
  ngOnInit() {
    //this.form = this.createControl();
    this.userFormData = this.fb.group({
      checkbox: this.fb.array([])
    })


    this.shareServices.currentMessage.subscribe(message => this.editFormObj = message);
    this.route.params.subscribe(params =>
      this.createDynamicHtml(params.taskFlowId, params.taskId, params.formId));

  }
  onChange(event) {
    const interests = <FormArray>this.userFormData.get('checkbox') as FormArray;

    if (event.checked) {
      interests.push(new FormControl(event.source.value))
    } else {
      const i = interests.controls.findIndex(x => x.value === event.source.value);
      interests.removeAt(i);
    }
  }

  createControl() {
    const group = this.fb.group({});
    this.currentTaskJson.forEach(field => {
      if (field.controlName === "button") return;
      const control = this.fb.control(
        field.value,
        this.bindValidations(field.validations || [])
      );
      group.addControl(field.labelID, control);
    });
    return group;
  }
  bindValidations(validations: any) {
    if (validations.length > 0) {
      const validList = [];
      validations.forEach(valid => {
        validList.push(valid.validator);
      });
      return Validators.compose(validList);
    }
    return null;
  }



  private createDynamicHtml(taskFlowId, taskId, formId) {
    this.taskFlowId = taskFlowId;
    this.FormId = formId;
    /**open last filed form */
    /*if (this.editFormObj != null && this.editFormObj.priviousFormJsonList !== null && this.editFormObj != 'editFalse') {
      this.allTaskJson = [];
      this.editFormObj.priviousFormJsonList.forEach(element => {
        var json = JSON.parse(element);
        this.allTaskJson.push(json);
      });
    }*/

    /**open current form in edit mode */
    let taskInstanceId = null;
    if (/* this.editFormObj != null && this.editFormObj.formId != null && this.editFormObj.taskInstanceID != null && this.editFormObj != 'editFalse' */taskInstanceId) {
      // let formId = this.editFormObj.formId;
      this.URL = 'getDataElementsEdit/' + taskFlowId + '/' + taskInstanceId + '/' + taskId + '/' + formId;
      this.getReviewTaskJson();
      // this.getCurrentTaskFormJson();
    }
    else {
      this.URL = 'getDataElements/' + this.taskFlowId + '/' + taskId + '/' + formId;
      this.getCurrentTaskFormJson();
    }
  }

  private getCurrentTaskFormJson() {
    this.http.get(this.Core_URL + '/' + this.URL)
      .subscribe(data => {
        this.data = data;
        this.functionId = this.data.functionId;
        this.taskId = this.data.taskId;
        this.FormId = this.FormId;
        var json = JSON.parse(this.data.formJson);
        let arrayDisplayOrder = [];
        var largest;

        for (var i = 0; i < json.length; i++) {
          json[i].displayOrder = i + 1;
        }
        for (var i = 0; i < json.length; i++) {
          arrayDisplayOrder.push(json[i].displayOrder);
        }
        largest = Math.max.apply(null, arrayDisplayOrder);
        for (var i = 0; i < json.length; i++) {
          if (json[i].labelName == "Submit") {
            json[i].displayOrder = ++largest;
          } else if (json[i].labelName == "Cancel") {
            json[i].displayOrder = largest + 1;
          }
        }
        function GetSortOrder(prop) {
          return function (a, b) {
            if (a[prop] > b[prop]) {
              return 1;
            } else if (a[prop] < b[prop]) {
              return -1;
            }
            return 0;
          }
        };
        json.sort(GetSortOrder("displayOrder"));
        this.currentTaskJson = json;
        console.log('this.currentTaskJson', this.currentTaskJson);
        this.currentTaskJson.forEach(field => {
          const control = this.fb.control(
            field.value,
            this.bindValidations(field.validations || [])
          );
          if (/*field.htmlControlTypeName != 'file'&&*/ field.controlName != 'button') {
            this.userFormData.addControl(field.labelID, control);
          }
        });
      });
  }

  getReviewTaskJson() {
    this.http.get(this.Core_URL + '/' + this.URL)
      .subscribe(data => {
        this.data = data;
        this.functionId = this.data.functionId;
        this.taskId = this.data.taskId;
        this.FormId = this.FormId;
        var json = JSON.parse(this.data.formJson);
        let arrayDisplayOrder = [];
        var largest;
        for (var i = 0; i < json.length; i++) {
          arrayDisplayOrder.push(json[i].displayOrder);
        }
        largest = Math.max.apply(null, arrayDisplayOrder);
        for (var i = 0; i < json.length; i++) {
          if (json[i].type == "submit") {
            json[i].displayOrder = ++largest;
          } else if (json[i].type == "cancel") {
            json[i].displayOrder = largest + 1;
          }
        }
        function GetSortOrder(prop) {
          return function (a, b) {
            if (a[prop] > b[prop]) {
              return 1;
            } else if (a[prop] < b[prop]) {
              return -1;
            }
            return 0;
          }
        };
        json.sort(GetSortOrder("displayOrder"));
        this.currentTaskJson = json;
        this.currentTaskJson.forEach(field => {
          const control = this.fb.control(
            field.value,
            this.bindValidations(field.validations || [])
          );
          if (field.htmlControlTypeName != 'file' && field.controlName != 'button') {
            this.userFormData.addControl(field.labelID, control);
          }
        });
      });
  }


  fileInputChanged(event, labelID) {
    this.fileUploaded = "";
    this.fileUploadButton = false;
    if (event && event.target && event.target.files) {
      this.selectedFiles = event.target.files;
    }
    this.progressBarValue = 0;
    this.userFormData.controls[labelID].markAsTouched();
    this.userFormData.controls[labelID].setValue(this.fileValue.split('\\').pop());
  }




  cancel() {
    this.router.navigate(['/eta-web']);
  }
  public setDetails(userDetails, form) {
    let userobjWrapper: Object = {};
    var userobjArr = [];
    /*if(userDetails){
      userobjArr.push(userDetails);
    }*/
    /*  let myForm = <HTMLFormElement>document.getElementById("userData");
     var itemsLength = Object.keys(userDetails).length;
  */
    for (var key in userDetails) {
      var checkIsArray = Array.isArray(userDetails[key])
      let userobj: Object = {};
      if (key != 'userPhoto') {
        userobj["instanceDataElement"] = key;
        userobj["dataElementInstanceValue"] = userDetails[key];
        userobj["dataElementInstanceStatusId"] = 1;
        if (userobj != null)
          userobjArr.push(userobj);
      }
      let temp: string = "";
      if (userDetails[key] != null && typeof (userDetails[key]) == "object") {
        if (userDetails[key].length > 0) {
          for (var s = 0; s < userDetails[key].length; s++) {
            if (temp == "") {
              temp = temp + userDetails[key][s];
            }
            else {
              temp = temp + "," + userDetails[key][s];
            }

          }
          userobj["dataElementInstanceValue"] = temp;
        }
      }
    }
    for (var j = 0; j < this.currentTaskJson.length; j++) {
      for (var i = 0; i < userobjArr.length; i++) {
        if (userobjArr[i].instanceDataElement == this.currentTaskJson[j].labelID) {
          userobjArr[i]['dataElementInsatnceFieldName'] = this.currentTaskJson[j].fieldName;
        }
      }
    }
    userobjWrapper["functionId"] = this.functionId;
    userobjWrapper["processId"] = this.processId;
    userobjWrapper["taskId"] = this.taskId;
    userobjWrapper["formId"] = parseInt(this.FormId);
    userobjWrapper["taskFlowId"] = parseInt(this.taskFlowId);
    userobjWrapper["formDetails"] = userobjArr;

    if (userobjWrapper) {
      let checkBox: any[] = [];
      let valueCheck: any[] = [];
      if (userobjWrapper['formDetails'].length > 0) {
        for (var j = 0; j < this.currentTaskJson.length; j++) {
          for (var i = 0; i < userobjWrapper['formDetails'].length; i++) {
            if (userobjWrapper['formDetails'][i].instanceDataElement == this.currentTaskJson[j].controlName) {
              checkBox.push(this.currentTaskJson[j]);
              valueCheck.push(userobjWrapper['formDetails'][i].dataElementInstanceValue);
            }
          }
        }
      }

      if (userobjWrapper['formDetails'].length > 0) {
        for (var i = 0; i < userobjWrapper['formDetails'].length; i++) {
          for (var j = 0; j < checkBox.length; j++) {
            if (userobjWrapper['formDetails'][i].dataElementInsatnceFieldName == checkBox[j].fieldName) {
              userobjWrapper['formDetails'][i].dataElementInstanceValue = valueCheck.join();
            }
          }

        }
      }

      for (var x = 0; x < userobjWrapper['formDetails'].length; x++) {
        if (userobjWrapper['formDetails'][x].instanceDataElement == "checkbox") {
          userobjWrapper['formDetails'].splice(x, 1);
        }
      }



    }
    console.log('userobjWrapper', userobjWrapper);

    this.http.post(this.Core_URL + '/submitTask', userobjWrapper, { responseType: 'text' })
      .subscribe(data => {
        this.data = data;
        if (data == 'ok') {
          alert('Update Successfully');
          this.router.navigate(['/eta-web']);
          /*this.shareServices.getUserActivationStatusByToken()
          .subscribe(data => {
            console.log('data',data);
            if(data.userType == "ORG_Client"){
              alert('Update Successfully');
              this.router.navigate(['/welcome']);
            }else if(data.userType == "ORG_Reviewer"){
              alert('Update Successfully');
              this.router.navigate(['/mytask']);
            }
          });*/

        }
      }, error => {
        console.log(error);

      });
  }

  private clearShareObj() {
    this.shareServices.setgridChangeMessage(null);
  }

  /*private getDropDownValue(ele: any) {
    if (ele.controlName === 'select' || 'multiselect') {
      if (this.listObj.hasOwnProperty(ele.labelID) == false) {
        this.http.get(this.Core_URL + '/getDropDownValue/' + ele.labelID)
          .subscribe(data => {
            console.log(data);
            this.CountryList = data;
            this.listObj[ele.labelID] = data;
          });
      }
    }
  }*/

  getAllPrivilegeList() {
    this.http.get(this.shareServices.Role_URL + '/getAllPrivilegeByOrgIdAndPrivilegeType/1')
      .subscribe(data => {
        console.log(data);
        this.dropdownList = data;
      });
  }


  upload($event,labelID) {
    if (this.selectedFiles && this.selectedFiles.item(0)) {
      const file = this.selectedFiles.item(0)
      const reader = new FileReader();
      reader.readAsDataURL(this.selectedFiles[0]);
      var ext = file.name.substr(file.name.lastIndexOf('.') + 1);
      if (ext === 'png' || ext === 'jpeg') {
        reader.onload = event => {
          const img = new Image();
          img.src = (event.target as any).result;
          img.onload = () => {
            const elem = document.createElement('canvas');
            elem.width = img.width;
            elem.height = img.height;
            if (elem.width <= 128 && elem.height <= 128 && ext === 'png' ||  ext === 'jpeg') {
              this.validateFile();
              this.currentFileUpload = this.selectedFiles.item(0);
              this.dashboardService.pushFileToStorage(this.currentFileUpload, "icon", "publish").subscribe(response => {
                this.fileValue = response.fileName;
                this.userFormData.controls[labelID].markAsTouched();
                this.userFormData.controls[labelID].setValue(this.fileValue);
                this.progressBarValue = 100;
                this.fileUploaded = "Uploaded";
                this.fileUploadButton = true;
              })
            } else {
              this.fileNotSupported(labelID);
              this.fileUploadButton = false;
            }
          },
            reader.onerror = error => console.log(error);
        };
      } else if(ext == 'xlsx' || ext == 'docx' || ext == 'pdf' || ext == 'doc'){
        if (ext == 'xlsx' || ext == 'docx' || ext == 'pdf' || ext == 'doc') {
        this.validateFile();
        this.currentFileUpload = this.selectedFiles.item(0);
        this.dashboardService.uploadFileImageAndDoc(this.currentFileUpload, "test", "publish").subscribe(response => {
         // this.fileValue = response.fileName;
          this.userFormData.controls[labelID].markAsTouched();
          this.userFormData.controls[labelID].setValue(this.fileValue.split('\\').pop());
          this.progressBarValue = 100;
          this.fileUploaded = "Uploaded";
          this.fileUploadButton = true;
        })
      }else {
              this.fileNotSupported(labelID);
              this.fileUploadButton = false;
      }
    }


    }

  }

  validateFile() {
    this.progressBarValue = 100;
  }
  /*function for make the file not supported*/
  fileNotSupported(labelID) {
    this.progressBarValue = 100;
    this.userFormData.controls[labelID].setErrors({ 'invalidFile': true });
    this.userFormData.controls[labelID].markAsTouched();
  }
  /*  selectAll(select: NgModel, values, array) {
     select.update.emit(values);
   }
 
   deselectAll(select: NgModel) {
     select.update.emit([]);
   }
 */
  /*selectAll(list, formControlNameValue: String) {
    this.userFormData.get(formControlNameValue + "").patchValue(list)
  }
  deselectAll(formControlNameValue: String) {
    this.userFormData.get(formControlNameValue + "").patchValue([])
  }*/

  /**
   *To Read file formLocal Storage 
   *
   * @param {*} event
   * @memberof UserdetailsComponent
   */
  /*public selectFile(event) {

    this.userFormData.get('userPhoto').patchValue(event.target.files[0].name)

    this.imageName = event.target.files[0].name;
    this.selectedFiles = event.target.files;
    let path = this.processId + "@" + this.taskId
    this.currentFileUpload = this.selectedFiles.item(0);
    let formdata: FormData = new FormData();
    formdata.append('firstName', "Harkesh");
    formdata.append('lastName', "kumar");
    formdata.append('file', this.currentFileUpload);
  }*/


  /*private taskFileUpload(path) {
    let formdata: FormData = new FormData();
    formdata.append('file', this.currentFileUpload);
    this.shareServices.taskFileUpload(this.currentFileUpload, path, this.imageName)
      .subscribe(data => {

      }, error => {
        console.log(error);

      });
  }*/

}
