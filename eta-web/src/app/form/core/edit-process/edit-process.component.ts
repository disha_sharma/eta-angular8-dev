import { Component, OnInit, Inject, ChangeDetectorRef } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource, MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient, HttpResponse, HttpEventType } from '@angular/common/http';
import { ServiceService } from './../../../eta-services/service.service';
import { DashboardServiceService } from './../../../dash-board/dashboard-service.service';
import { ProcessDetails } from './../process/Process';
import { Observable, of } from 'rxjs';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead';
import { mergeMap } from 'rxjs/operators';
import { FormGroup, FormBuilder, Validator, Validators, FormControl } from '@angular/forms';

@Component({
  selector: 'app-edit-process',
  templateUrl: './edit-process.component.html',
  styleUrls: ['./edit-process.component.css']
})
export class EditProcessComponent implements OnInit {
  processControl = new FormControl();

  editProcessForm = new FormGroup({
    processNameControl: this.processControl
  });

  Core_URL: any;
  //FILE UPLOADS
  processImage = "";
  selectedFiles: FileList;
  currentFileUpload: File;
  fileInput: string = "";
  hide: boolean = false;
  validatedStatus: boolean = true;
  progressBarValue = 0;
  displayOrderValue: any;
  progress: { percentage: number } = { percentage: 0 }
  public defaultColumnToSort: any;
  //two way data binding variables
  organizationId = "";
  processId = "";
  processDescription = "";
  displayOrderList = <any>[];
  activeYN = "";
  /**type ahead var */
  processName: string = "";
  acronym: string = "";
  userId: string;
  firstName: string = "";
  typeaheadLoading: boolean;
  typeaheadLoadingProcess: boolean;
  typeaheadLoadingAcronym: boolean;
  typeaheadNoResults: boolean;
  dataSource: Observable<any>;
  dataSourceProcess: Observable<any>;
  dataSourceAcronym: Observable<any>;
  processList: any[] = [];
  acronymList: any[] = [];
  usersList: any[] = [];
  //Parameter for edit process
  functionId: number;
  //Parameter For Showing FunctionAcronym while updating process
  functionAcronym: string;

  allFunction: any;
  showAddMessage: boolean = false;
  duplicateProcessName: boolean = false;
  processNameReal: string = "";
  acronymReal: string = "";
  displayOrder: string = "";
  dOrder;
  fileNotLoaded: boolean = false;
  newValue: string = "";
  submitButtonDisabled: boolean = true;
  ownerNameSelection: boolean = false;
  fileUploaded: string = "";
  fileUploadButton: boolean = false;
  fileValue: string = "";
  existingFileValue: string = "";

  isLoadingResults = false;


  constructor(private http: HttpClient, private route: ActivatedRoute, private router: Router, private shareServices: ServiceService, private dashboardService: DashboardServiceService, private changeDetectorRefs: ChangeDetectorRef, private formBuilder: FormBuilder, public dialog: MatDialog, @Inject(MAT_DIALOG_DATA) data) {
    this.createForm();
    this.getAllFunction();
    this.defaultColumnToSort = "formName";
    this.Core_URL = this.shareServices.Core_URL;
    this.organizationId = data.organizationId;
    this.processId = data.processId;
    this.functionId = data.functionId;

    //Showing Function Acronym While Changing the process
    this.functionAcronym = data.currentFuncitonAcronym;
    this.dataSourceProcess = Observable.create((observer: any) => {
      // Runs on every search
      observer.next(this.processName);
    })
      .pipe(
        mergeMap((token: string) => this.getStatesAsObservableProcess(token))
      );
    this.dataSourceAcronym = Observable.create((observer: any) => {
      // Runs on every search
      observer.next(this.acronym);
    })
      .pipe(
        mergeMap((token: string) => this.getStatesAsObservableAcronym(token))
      );
    this.dataSource = Observable.create((observer: any) => {
      // Runs on every search
      observer.next(this.firstName);
    })
      .pipe(
        mergeMap((token: string) => this.getStatesAsObservable(token))
      );

    this.dashboardService.getProcessDetailsByProcessId(this.processId).subscribe(data => {
      this.organizationId = data.organizationId;
      this.processId = data.processID;
      this.processName = data.processName;
      this.processDescription = data.processDescription;
      this.acronym = data.acronym;
      this.firstName = data.processOwnerName;
      this.displayOrder = data.displayOrder;
      this.fileInput = data.processImage;
      this.activeYN = data.activeYN;
      this.displayOrder = data.displayOrder;
      this.acronymReal = data.acronym;
      this.processNameReal = data.processName;
      this.fileValue = data.processImage;
      this.existingFileValue = data.processImage;
      this.fileUploaded = "Uploaded";
      this.editProcessForm.patchValue({
        fileInput: data.processImage
      });
      this.editProcessForm.patchValue({
        dOrderControl: data.displayOrder
      });

    });

  }

  createForm() {
    this.editProcessForm = this.formBuilder.group({
      processNameControl: ['', [Validators.required, Validators.pattern('^[a-zA-Z0-9 ]*$')]],
      acronymControl: ['', [Validators.required, Validators.pattern('^[a-zA-Z0-9\.]*$')]],
      decriptionControl: ['', [Validators.required /*, Validators.pattern(/^([^0-9]*)$/)*/]],
      ownerNameControl: ['', [Validators.required]],
      dOrderControl: '',
      //fileControl: '',
      fileInput: ''
    });
  }

  ngOnInit() {
    /**This will bring all the orgs for type ahead */
    this.dashboardService.updateStatus = false;
    this.dashboardService.getAllUserList().subscribe(data => {
      for (let s of data) {
        this.usersList.push(s);
      }
    });

    /**This will bring all the process names for typeahead */
    this.dashboardService.getAllProcessList().subscribe(data => {
      for (let s of data) {
        this.processList.push(s);
      }
    });

    /**This will bring all the process acronym list for typeahead    */
    this.dashboardService.getAllProcessAcronymList().subscribe(data => {
      for (let s of data) {
        this.acronymList.push(s);
      }
    });



    this.dashboardService.getAllProcessDisplayOrderList().subscribe(data => {
      this.displayOrderList = data;
    });

  }


  compareObjects(o1: any, o2: any): boolean {
    this.newValue = JSON.stringify(o1).replace(/['"]+/g, '');
    return this.newValue === this.displayOrder;
  }

  private getAllFunction() {
    this.dashboardService.getAllFunctionsList()
      .subscribe(data => {
        this.allFunction = data;
        this.allFunction.forEach(element => {
          if (this.functionId == element.functionID) {
            this.functionAcronym = element.acronym;
          }
        })
      });
  }

  /**This Function use for Update Record */
  updateRecord() {
    this.isLoadingResults = true;
    if (this.dOrder == null || this.dOrder == "") {
      this.dOrder = "0";
    }
    for (let s of this.usersList) {
      JSON.parse(JSON.stringify(s), (key, value) => {
        if (key === 'firstName') {
          if (value === this.firstName) {
            JSON.parse(JSON.stringify(s), (key, value) => {
              if (key === 'userId') {
                this.userId = value;
              }
            });
          }
        }
      });
    }
    let processDetails: ProcessDetails = new ProcessDetails(this.functionId, this.processId, this.userId, this.processName, this.processDescription, this.acronym, this.dOrder, this.fileValue, this.activeYN);
    if (processDetails.process_owner === undefined) {
      alert("This is not correct User please change it");
      return;
    }
    if (!this.isNumber(processDetails.process_owner)) {
      alert("This is not correct User please change it");
      return;
    }




    this.dashboardService.updateProcessDetails(processDetails).subscribe(response => {
      if (response) {
        this.isLoadingResults = false;
        this.dashboardService.updateStatus = true;
        this.dashboardService.processName = response.processName;
        this.dialog.closeAll();
      }
    }, error => {
      this.dashboardService.updateStatus = false;
      this.dialog.closeAll();
    });

  }
  /**This Function use for Check Value Is Number*/
  isNumber(checkVal: string | number): boolean {
    return !isNaN(Number(checkVal.toString()));
  }
  /**This Function use for Get Data of Acronym*/
  getStatesAsObservableAcronym(token: string): Observable<any> {
    const query = new RegExp(token, 'i');
    return of(
      this.acronymList.filter((str: any) => {
        return query.test(str.acronym);
      })
    );
  }

  /**This Function use for Get Data of Process*/
  getStatesAsObservableProcess(token: string): Observable<any> {
    const query = new RegExp(token, 'i');
    return of(
      this.processList.filter((str: any) => {

        return query.test(str.processName);
      })
    );
  }
  /**This Function use for Get Data of Owner Name*/
  getStatesAsObservable(token: string): Observable<any> {
    const query = new RegExp(token, 'i');
    return of(
      this.usersList.filter((state: any) => {
        return query.test(state.processOwnerName);
      })
    );
  }
  /**This Function use for Change TypeAhead Loading on Process Name*/
  changeTypeaheadLoadingProcess(e: boolean): void {
    this.typeaheadLoadingProcess = e;
  }
  /**This Function use for Selection Change On Process Name*/
  typeaheadOnSelectProcess(e: TypeaheadMatch): void {
    this.processName = e.value;
    for (let s of this.processList) {
      JSON.parse(JSON.stringify(s), (key, value) => {
        if (key === 'processName') {
          if (this.processName.trim().length === this.processNameReal.trim().length && this.processName.trim() === this.processNameReal.trim()) {
            this.duplicateProcessName = false;
          } else if (this.processName.trim() != this.processNameReal.trim() && value.trim().length === this.processName.trim().length && value.trim() === this.processName.trim()) {
            this.editProcessForm.controls['processNameControl'].setErrors({ 'invalid': true });
            this.duplicateProcessName = true;
          }
        }
      });
    }
  }
  /**This Function use for Change TypeAhead Loading on Acronym*/
  changeTypeaheadLoadingAcronym(e: boolean): void {
    this.typeaheadLoadingAcronym = e;
  }
  /**This Function use for Selection Change On Acronym*/
  typeaheadOnSelectAcronym(e: TypeaheadMatch): void {
    this.acronym = e.value;
    for (var i = 0; i < this.acronymList.length; i++) {
      if (this.acronymList[i].acronym.trim() != this.acronymReal.trim() && this.acronymList[i].acronym.trim() == e.value.trim()) {
        this.editProcessForm.controls['acronymControl'].setErrors({ 'myError': true });
        return;
      } else if (this.acronymList[i].acronym.trim() == this.acronymReal.trim()) {
        console.log('else');
      }


    }
  }

  /**This Function use for Change TypeAhead Loading on Owner Name*/
  changeTypeaheadLoading(e: boolean): void {
    this.typeaheadLoading = e;
  }
  /**This Function use for Selection Change On Owner Name*/
  typeaheadOnSelect(e: TypeaheadMatch): void {
    this.firstName = e.value;
  }
  /**This Function use for Click on Cancel*/
  cancel() {
    this.dashboardService.cancelAdd = true;
  }
  /**This Function call on every Field Change*/
  public enableSubmitButtonForEdit() {
    if (this.existingFileValue != this.fileValue) {
      this.submitButtonDisabled = true;
      return;
    }
    this.submitButtonDisabled = false;
  }

  /*
 This function is use for When Name AlphaNumeric Validation.
 */
  checkCharValidationForName(value) {
    var str = value.target.value;
    if (str) {
    const matches = str.match(/^[A-Za-z][A-Za-z0-9/.,->_()\\*|:" ]*$/);
    return matches?null: this.editProcessForm.controls['processNameControl'].setErrors({ 'patternError': true });
    }
  }

  checkCharValidationForAcronym(value){
    var str = value.target.value; 
      if (str) {
      const matches = str.match(/^[A-Za-z][A-Za-z0-9 ]*$/);
      return matches?null: this.editProcessForm.controls['acronymControl'].setErrors({ 'patternAcronymError': true });
      }
  }

  checkOnProcessName(event){
    this.processName = event.target.value;
    var processname = event.target.value.trim();
    var processEdit = this.processNameReal.trim();
    for (let s of this.processList) {
      JSON.parse(JSON.stringify(s), (key, value) => {
       // var value = value.trim();
        if (key === 'processName') {
          if (processname.length === processEdit.length && processname.toLowerCase() === processEdit.toLowerCase()) {
            this.duplicateProcessName = false;
          } else if (processname.length != processEdit.length && processname.toLowerCase() != processEdit.toLowerCase() && value.length === processname.length && value.toLowerCase() === processname.toLowerCase()) {
            this.editProcessForm.controls['processNameControl'].setErrors({ 'invalid': true });
            this.duplicateProcessName = true;
          }
        }
      });
    }
  }

  checkDuplicateAcronymName(event){
    this.acronym = event.target.value;
    var acronymname = event.target.value.trim();
    var acronymEdit = this.acronymReal.trim();
    for (let s of this.acronymList) {
      JSON.parse(JSON.stringify(s), (key, value) => {
        if (key === 'acronym') {
          if (acronymname != acronymEdit && value.length === acronymname.length && value === acronymname) {
            setTimeout(() => {
              this.editProcessForm.controls['acronymControl'].setErrors({ 'myError': true });
              }, 0);
              return;
          }
        }
      });
    }
  }

  /*
   This function is use for When Owner Name  Validation.
   */
  checkOwnerName(event) {
    var test: boolean = false;
    for (let s of this.usersList) {
      JSON.parse(JSON.stringify(s), (key, value) => {
        if (key === 'firstName') {
          if (event.target.value != null && event.target.value != "") {
            if (value != null && value != "" && value !== event.target.value) {
              if (!test) {
                this.editProcessForm.controls['ownerNameControl'].setErrors({ 'invalid': true });
                this.ownerNameSelection = true;
                return;
              }
            } else {
              test = true;
              //this.editProcessForm.controls['ownerNameControl'].setErrors({ 'invalid': false });
              this.ownerNameSelection = false;
              return;
            }
          }
        }
      });
    }
  }

  /*function for upload file*/
  upload() {
    if (this.selectedFiles && this.selectedFiles.item(0)) {
      const file = this.selectedFiles.item(0)
      const reader = new FileReader();
      reader.readAsDataURL(this.selectedFiles[0]);
      var ext = file.name.substr(file.name.lastIndexOf('.') + 1);
      reader.onload = event => {
        const img = new Image();
        img.src = (event.target as any).result;
        img.onload = () => {
          const elem = document.createElement('canvas');
          elem.width = img.width;
          elem.height = img.height;
          if (elem.width <= 128 && elem.height <= 128 && ext === 'png') {
            this.validateFile();
            this.currentFileUpload = this.selectedFiles.item(0);
            this.dashboardService.pushFileToStorageProcess(this.currentFileUpload, "icon", "process").subscribe(response => {
              this.enableSubmitButtonForEdit();
              this.fileValue = response.fileName;
              this.editProcessForm.patchValue({
                fileInput: response.fileName
              });
              this.progressBarValue = 100;
              this.fileUploaded = "Uploaded";
              this.fileUploadButton = true;
              this.submitButtonDisabled = false;
            })
          } else {
            this.fileUploadButton = false;
            this.fileNotSupported();
          }
        },
          reader.onerror = error => console.log(error);
      };

    }

  }
  /*function for successfully validate File*/
  validateFile() {
    this.progressBarValue = 100;
  }
  /*function for make the file not supported*/
  fileNotSupported() {
    this.progressBarValue = 100;
    this.editProcessForm.controls['fileInput'].setErrors({ 'invalidFile': true });
    this.editProcessForm.controls['fileInput'].markAsTouched();
  }
  /*
  This function is use for When File Input Change.
  */
  fileInputChanged(event) {
    this.fileUploadButton = false;
    if (event && event.target && event.target.files) {
      this.selectedFiles = event.target.files;
    }
    this.progressBarValue = 0;
    this.editProcessForm.controls['fileInput'].markAsTouched();
    this.editProcessForm.controls['fileInput'].setValue(this.fileValue.split('\\').pop());
    if (this.existingFileValue != this.fileValue) {
      this.submitButtonDisabled = true;
    }
  }
}
