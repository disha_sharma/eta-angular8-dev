import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { ServiceService } from './../../../eta-services/service.service'
@Component({
  selector: 'app-create-data-object',
  templateUrl: './create-data-object.component.html',
  styleUrls: ['./create-data-object.component.css']
})
export class CreateDataObjectComponent implements OnInit {

  Core_URL: any;
  createdataobject: FormGroup;
  dataObjectUrl: any;
  dataObjectId: any;
  dataObj: any;
  DataContainer: any;
  formobj: Object = {};
  isEdit = false;

  gridTranslationColumns: any;
  isDataLoaded = false;
  dataObjectTransUrl: any;

  get_Data: any;
  dataobjectId: any;

  gridDataObjectColumns: any;

  public defaultColumnToSort: any;
  public recordsTotal: number;
  constructor(private _formBuilder: FormBuilder, private http: HttpClient, private route: ActivatedRoute,
    private r: Router, private shareServices: ServiceService) {
    this.defaultColumnToSort = "dataObjectName";
    this.Core_URL = this.shareServices.Core_URL;
    this.dataObjectUrl = this.Core_URL + '/getAllInnerDataObjectByOrgIdGrid';
    this.getDataContainer();
    this.getHeaderMenuList();
  }

  ngOnInit() {
    this.getDataObjectColoumnList();
    this.shareServices.currentMessage.subscribe(message => this.dataObj = message);
    this.createdataobject = this._formBuilder.group({
      datacontainer: [],
      createobjectname: [],
      description: [],
      hasmultiple: []
    })


    /** only for edit */
    if (this.dataObj != null && this.dataObj != 'editFalse') {
      this.createdataobject.patchValue({ createobjectname: this.dataObj.dataObjectName });
      this.createdataobject.patchValue({ description: this.dataObj.dataObjectDescription });
      this.createdataobject.patchValue({ hasmultiple: this.dataObj.dataObjectHasMultipleDataElement });

      this.isEdit = true;
      this.dataobjectId = this.dataObj.dataObjectID;

    }
    else {
      this.isEdit = false;
    }
  }

  private getDataContainer() {
    this.http.get(this.Core_URL + '/getAllDataContainer')
      .subscribe(data => {
        this.DataContainer = data;
      });
  }

  public newDataContainer() {
    this.clearShareObj();
    this.r.navigateByUrl("createdatacontainer");
  }

  public onSubmit() {
    if (this.isEdit) {
      this.dataObj.dataObjectName = this.createdataobject.value.createobjectname
      this.dataObj.dataObjectDescription = this.createdataobject.value.description
      this.dataObj.dataObjectHasMultipleDataElement = this.createdataobject.value.hasmultiple
      this.http.post(this.Core_URL + '/updateDataObject', this.dataObj)
        .subscribe(data => {
          this.get_Data = data;
          this.dataobjectId = this.get_Data.dataObjectID;
          if (this.dataobjectId != null) {
            alert("Updated successfully !! Please add translations");
            this.gridDataObjectColumns = null;
            this.getDataObjectColoumnList();
          } else {
            alert("data not saved");
          }
        });
    } else {
      // console.log(this.createdataobject.value);
      this.formobj["dataObjectName"] = this.createdataobject.value.createobjectname;
      this.formobj["dataObjectDescription"] = this.createdataobject.value.description;
      this.formobj["dataObjectHasMultipleDataElement"] = this.createdataobject.value.hasmultiple;
      this.http.post(this.Core_URL + '/updateDataObject/', this.formobj)
        .subscribe(data => {
          this.get_Data = data;
          this.dataobjectId = this.get_Data.dataObjectID;
          if (this.dataobjectId != null) {
            alert("Updated successfully !! Please add translations");
            this.clearShareObj();
            this.gridDataObjectColumns = null;
            this.getDataObjectColoumnList();
          } else {
            alert("data not saved");
          }
        });
    }
  }

  private clearShareObj() {
    this.shareServices.setgridChangeMessage(null);
  }
  public cancel() {
    this.clearShareObj();
    this.r.navigateByUrl('dashboardDataObject');
  }

  public translationColumns() {
    if (this.dataobjectId != null) {
      let coloumNames: any;
      let myColoumsObj: any;
      this.http.get(this.Core_URL + '/getCoreGridHeaderLanguageByOrgId')
        .subscribe(data => {
          myColoumsObj = data;
          coloumNames = myColoumsObj.coloumnName;
          if (Object.keys(coloumNames).length > 0) {
            this.gridTranslationColumns = coloumNames;
          }
          this.isDataLoaded = true;
          this.dataObjectTransUrl = this.Core_URL + '/getDataObjectLanguageInfo/' + this.dataobjectId;
        });
    } else {
      alert("please add DataObject");
    }
  }

  public getTranslationEditObject(event: any) {
    let translationEditObj = {};
    translationEditObj["id"] = event.obj.id;
    translationEditObj["versionId"] = event.obj.versionId;
    translationEditObj["languageId"] = event.languageId;
    translationEditObj["orgId"] = event.obj.id;
    translationEditObj["columnName"] = event.obj.field;
    translationEditObj["columnValue"] = event.newEditValue;
    this.http.post(this.Core_URL + '/updateObjectLanguage', translationEditObj)
      .subscribe(data => {
      });
  }

  public closePopUp() {
    this.isDataLoaded = false;
  }

  private getDataObjectColoumnList() {
    this.shareServices.getCoreInnerDataObjectColumn()
      .subscribe(data => {
        this.gridDataObjectColumns = data.coloumnName;
        this.recordsTotal = data.recordsTotal;
      });

  }

  public mainbreadcrumb: string;
  public breadcrumb: string;
  private menuList: any = [];
  getHeaderMenuList() {
    this.shareServices.getHeaderMenu()
      .subscribe(res => {
        this.menuList = res;

        this.menuList.forEach(element => {
          if (element.routerLink == "eta-web/dashboardDataObject") {
            this.mainbreadcrumb = element.translation;
          }
          if (element.routerLink == "eta-web/dashboardDataObject") {
            this.breadcrumb = element.translation;
          }
        })

      })
  }

}
