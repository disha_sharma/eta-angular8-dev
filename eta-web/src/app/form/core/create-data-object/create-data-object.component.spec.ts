import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateDataObjectComponent } from './create-data-object.component';

describe('CreateDataObjectComponent', () => {
  let component: CreateDataObjectComponent;
  let fixture: ComponentFixture<CreateDataObjectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateDataObjectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateDataObjectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
