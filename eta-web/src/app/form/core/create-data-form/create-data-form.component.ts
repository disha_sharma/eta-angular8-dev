import { NgModel } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { ServiceService } from './../../../eta-services/service.service'

@Component({
  selector: 'app-create-data-form',
  templateUrl: './create-data-form.component.html',
  styleUrls: ['./create-data-form.component.css']
})
export class CreateDataFormComponent implements OnInit {

  Core_URL: any;
  createform: FormGroup;
  formUrl: any;
  gridFormColumnsTrans: any;
  gridFormDataTrans: any;
  isDataLoadedFormTrans: any;
  formId: any;
  formObj: any;
  isEdit = false;
  allDataContainer: any;
  dropdownList = [];
  selectedItems = [];
  dropdownSettings = {};
  dataContainers = [];
  gridTranslationColumns: any;
  isDataLoaded = false;
  formTransUrl : any;
  isDataContainersLoad = false;
  get_Data:any;
  gridFormColumns:any;

  public defaultColumnToSort:any;
  public recordsTotal:number;
  constructor(private _formBuilder: FormBuilder, private http: HttpClient, private route: ActivatedRoute,
    private r: Router, private shareServices: ServiceService) {
      this.defaultColumnToSort = "formName";
    this.Core_URL = this.shareServices.Core_URL;
    this.formUrl = this.Core_URL + '/getAllInnerFormsByOrgIdGrid';
    this.getAllDataContainet();
    this. getHeaderMenuList();
  }

  ngOnInit() {
    this.getDataFormColoumnList();
    
    this.getAllDC();
    this.shareServices.currentMessage.subscribe(message => this.formObj = message);  
    this.createform = this._formBuilder.group({
      createformname: [],
      createformdescription: [],
      datacontainer: [],
      selectedFormValue:[]
    })

    /** only for edit */
    console.log(this.formObj);
    
    if (this.formObj != null && this.formObj != 'editFalse') {
      this.createform.patchValue({ selectedFormValue: this.formObj.dataContainerIds });
      this.createform.patchValue({ createformname: this.formObj.formName });
      this.createform.patchValue({ createformdescription: this.formObj.formDescription });
      this.isEdit = true;
      this.formId =this.formObj.formID;
      
    }
    else {
      this.isEdit = false;
    }

  }

  public getAllDC() {
    let listData;
    this.http.get(this.Core_URL + '/getAllDataContainer').subscribe(data => {
      listData = data;
      listData.forEach(element => {
        let countryObj: any = {};
        countryObj.item_id = element.dataContainerID;
        countryObj.item_text = element.dataContainerName;
        this.dataContainers.push(countryObj);
        console.log(countryObj.item_text);
      });
     
      if (this.isEdit == true) {
        let dataContainsers;
        this.http.get(this.Core_URL + '/getDataContainerForm/' + this.formObj.formID)
          .subscribe(data => {
           // console.log(data);
            dataContainsers = data;
            this.dataContainers.forEach(el => {
              dataContainsers.forEach(element => {
                if (element.dataContainerId == el.item_id) {
                  this.selectedItems.push(el);
                }
              });
            });
            this.isDataContainersLoad = true;
            //console.log(this.selectedItems);
          });
      } else {
        this.isDataContainersLoad = true;
      }
    });
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'item_id',
      textField: 'item_text',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      //itemsShowLimit: 3,
      allowSearchFilter: true
    };
  }

  public setAllSelectedDC() {
    this.selectedItems = [
      { item_id: 1, item_text: "data" }
    ];
  }
  
  public createNewForm() {
    this.clearShareObj();
    this.r.navigateByUrl("task");
  }

  private getIdsOfSelectedItem() {
    let ids = [];
    this.selectedItems.forEach(element => {
      ids.push(element.item_id);
    });
    return ids;
  }

  public onSubmit() {
    if (this.isEdit) {
      this.formObj.formName = this.createform.value.createformname;
      this.formObj.formDescription = this.createform.value.createformdescription;
      this.formObj.dataContainerIds = this.createform.value.selectedFormValue;
      this.http.post(this.Core_URL + '/updateForm', this.formObj)
        .subscribe(data => {
          this.get_Data = data;
          this.formId=this.get_Data.formID;
          if(this.formId!=null){
            alert("Updated successfully !! Please add translations")
            this.gridFormColumns = null;
          this.getDataFormColoumnList();
        }else{
          alert("data not saved")
        }
        });
    }
    else {
      let formobj: Object = {};
     // console.log(this.createform.value);
      formobj["formName"] = this.createform.value.createformname
      formobj["formDescription"] = this.createform.value.createformdescription
      formobj["dataContainerIds"] = this.createform.value.selectedFormValue;
      this.http.post(this.Core_URL + '/updateForm', formobj)
        .subscribe(data => {
          this.get_Data = data;
          this.formId=this.get_Data.formID;
          if(this.formId!=null){
            alert("Updated successfully !! Please add translations")
          this.clearShareObj();
          this.gridFormColumns = null;
          this.getDataFormColoumnList();
        }else{
          alert("data not saved")
        }
        });
    }
  }
  private getAllDataContainet() {
    this.http.get(this.Core_URL + '/getAllDataContainer')
      .subscribe(data => {
        this.allDataContainer = data;
      });
  }
  public translationColumns() {
    if(this.formId!=null){
    let coloumNames: any;
    let myColoumsObj: any;
    this.http.get(this.Core_URL + '/getCoreGridHeaderLanguageByOrgId')
      .subscribe(data => {
        myColoumsObj = data;
        coloumNames = myColoumsObj.coloumnName;
        if (Object.keys(coloumNames).length > 0) {
          this.gridTranslationColumns = coloumNames;
        }
        this.isDataLoaded = true;
        this.formTransUrl =  this.Core_URL + '/getFormLanguageInfo/'+this.formId;
      });
    }else{
      alert("please add form");
    }
  }

  private clearShareObj() {
    this.shareServices.setgridChangeMessage(null);
  }
  public cancel() {
    this.clearShareObj();
    this.r.navigateByUrl('formdashboard');
  }

  public getTranslationEditObject(event:any) {
    let translationEditObj ={};
    translationEditObj["id"]=event.obj.id;
    translationEditObj["versionId"]=event.obj.versionId;
    translationEditObj["languageId"]=event.languageId ;   
    translationEditObj["orgId"]=event.obj.id;
    translationEditObj["columnName"]=event.obj.field;
    translationEditObj["columnValue"]=event.newEditValue;
    this.http.post(this.Core_URL + '/updateFormLanguage',translationEditObj)
      .subscribe(data => {
      });
  } 
  
  public closePopUp(){
    this.isDataLoaded = false;
  }

  private getDataFormColoumnList(){
    this.shareServices.getCoreInnerFormColumn()
    .subscribe(data => {
      this.gridFormColumns =data.coloumnName;
      this.recordsTotal = data.recordsTotal;

    });
  }

  public mainbreadcrumb:string;
  public breadcrumb:string;
  private menuList: any = [];
  getHeaderMenuList() {
    this.shareServices.getHeaderMenu()
      .subscribe(res => {
       // console.log(res);
        this.menuList = res;
        this.menuList.forEach(element => {
          if(element.routerLink=="eta-web/dashboardDataObject"){
            this.mainbreadcrumb = element.translation;
          }
          if(element.routerLink=="eta-web/formdashboard"){
            this.breadcrumb = element.translation;
          }
        })
       
      })
  }	

  selectAll(list) {
    console.log(list);
    console.log(this.createform.get('selectedFormValue'));
      let tempList: any = [];
      list.forEach(element => {
        tempList.push(element.item_id);
      });
    console.log(tempList);
    
    this.createform.get('selectedFormValue').patchValue(tempList)
  }
  deselectAll() {
    this.createform.get('selectedFormValue').patchValue([])
  }



}
