import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DialogData } from '../../../popup/add-task/add-task.component';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CookieService } from 'ngx-cookie-service';
import { ServiceService } from '../../../eta-services/service.service';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { DashboardServiceService } from '../../../dash-board/dashboard-service.service';
@Component({
  selector: 'app-sample-form',
  templateUrl: './sample-form.component.html',
  styleUrls: ['./sample-form.component.css']
})
export class SampleFormComponent implements OnInit {
  private Core_URL: any;
  public userData: object;
  public dataAPI: any = {};
  private functionId: any;
  private processId: any;
  private FormId: any;

  private editFormObj: any;
  private URL: any;
  public currentTaskJson: any = [];
  public allTaskJson: any;
  public CountryList: any;

  public listObj: any = {};

  /** Privlege Multiselect Property  */
  public dropdownList: any = [];
  /** Privlege Multiselect Property  */
  public selectedItems: any = [];
  userFormData: FormGroup;

  public selectedFiles: FileList;
  public imageName: any;
  public currentFileUpload: File;

  /* Testing dynamic form */
  form: FormGroup;

  private taskFlowId: any;
  withOutValueArray: any = [];
  valueArray: any = [];
  formName: string = "";
  taskInstanceID: any;
  taskFlowID: any;
  taskId: any;
  formId: any;

  fileUploaded: string = "";
  fileUploadButton: boolean = false;
  progressBarValue = 0;
  fileValue: string = "";

  constructor(private route: ActivatedRoute, private http: HttpClient, private cookieService: CookieService, private shareServices: ServiceService,
    private fb: FormBuilder, private router: Router, public dialogRef: MatDialogRef<SampleFormComponent>,
    @Inject(MAT_DIALOG_DATA) data,private dashboardService: DashboardServiceService) {
    this.Core_URL = this.shareServices.Core_URL;
    this.getAllPrivilegeList();
    this.processId = data.processId;
    this.functionId = data.functionId;
    //this.taskId = data.taskId;
    this.createDynamicHtml(data.taskInstanceId, data.taskFlowId, data.taskId, data.formId, data.formName);
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
    //this.form = this.createControl();
    this.userFormData = this.fb.group({
     // userPhoto: []
    })
  }

  createControl() {
    const group = this.fb.group({});
    this.currentTaskJson.forEach(field => {
      if (field.controlName === "button") return;
      const control = this.fb.control(
        field.value,
        this.bindValidations(field.validations || [])
      );
      group.addControl(field.labelID, control);
    });
    return group;
  }
  bindValidations(validations: any) {
    if (validations.length > 0) {
      const validList = [];
      validations.forEach(valid => {
        validList.push(valid.validator);
      });
      return Validators.compose(validList);
    }
    return null;
  }



  private createDynamicHtml(taskInstanceId, taskFlowId, taskId, formId, formName) {
    this.formName = formName;
    this.taskInstanceID =  taskInstanceId;
    this.taskFlowId= taskFlowId;
    this.taskId= taskId;
    this.formId= formId;
    this.URL = 'getDataElementsEdit/' + taskFlowId + '/' + taskInstanceId + '/' + taskId + '/' + formId;
    this.getReviewTaskJson();
  }

  getReviewTaskJson() {
    this.http.get(this.Core_URL + '/' + this.URL)
      .subscribe(data => {
        this.dataAPI = data;
        var json = JSON.parse(this.dataAPI.formJson);
        var jsonForShowValue = this.dataAPI.formPublishJson;
        let arrayDisplayOrder = [];
        var largest;
        for (var i = 0; i < json.length; i++) {
          arrayDisplayOrder.push(json[i].displayOrder);
        }
        largest = Math.max.apply(null, arrayDisplayOrder);
        for (var i = 0; i < json.length; i++) {
          if (json[i].type == "submit") {
            json[i].displayOrder = ++largest;
          } else if (json[i].type == "cancel") {
            json[i].displayOrder = largest + 1;
          }
        }
        function GetSortOrder(prop) {
          return function (a, b) {
            if (a[prop] > b[prop]) {
              return 1;
            } else if (a[prop] < b[prop]) {
              return -1;
            }
            return 0;
          }
        };
        json.sort(GetSortOrder("displayOrder"));
        this.currentTaskJson = json;
        this.valueArray = JSON.parse(jsonForShowValue);
        for (var i = 0; i < this.currentTaskJson.length; i++) {
            this.withOutValueArray.push(this.currentTaskJson[i]);
            this.withOutValueArray.forEach(field => {
              const control = this.fb.control(
                field.value,
                this.bindValidations(field.validations || [])
              );
              if (field.htmlControlTypeName != 'file' && field.controlName != 'button') {
                this.userFormData.addControl(field.labelID, control);
              }
            });
        }
        this.withOutValueArray.forEach(field => {
          const control = this.fb.control(
            field.value,
            this.bindValidations(field.validations || [])
          );
          if (field.htmlControlTypeName != 'file' && field.controlName != 'button') {
            this.userFormData.addControl(field.labelID, control);
          }
        });
      });
  }


  fileInputChanged(event, labelID) {
    this.fileUploaded = "";
    this.fileUploadButton = false;
    if (event && event.target && event.target.files) {
      this.selectedFiles = event.target.files;
    }
    this.progressBarValue = 0;
    this.userFormData.controls[labelID].markAsTouched();
    this.userFormData.controls[labelID].setValue(this.fileValue.split('\\').pop());
  }

  upload($event,labelID) {
    if (this.selectedFiles && this.selectedFiles.item(0)) {
      const file = this.selectedFiles.item(0)
      const reader = new FileReader();
      reader.readAsDataURL(this.selectedFiles[0]);
      var ext = file.name.substr(file.name.lastIndexOf('.') + 1);
      if (ext === 'png') {
        reader.onload = event => {
          const img = new Image();
          img.src = (event.target as any).result;
          img.onload = () => {
            const elem = document.createElement('canvas');
            elem.width = img.width;
            elem.height = img.height;
            if (elem.width <= 128 && elem.height <= 128 && ext === 'png') {
              this.validateFile();
              this.currentFileUpload = this.selectedFiles.item(0);
              this.dashboardService.pushFileToStorage(this.currentFileUpload, "icon", "publish").subscribe(response => {
                this.fileValue = response.fileName;
                this.userFormData.controls[labelID].markAsTouched();
                this.userFormData.controls[labelID].setValue(this.fileValue);
                this.progressBarValue = 100;
                this.fileUploaded = "Uploaded";
                this.fileUploadButton = true;
              })
            } else {
              this.fileNotSupported(labelID);
              this.fileUploadButton = false;
            }
          },
            reader.onerror = error => console.log(error);
        };
      } else {
        this.fileNotSupported(labelID);
        this.fileUploadButton = false;
      }


    }

  }

  validateFile() {
    this.progressBarValue = 100;
  }
  /*function for make the file not supported*/
  fileNotSupported(labelID) {
    this.progressBarValue = 100;
    this.userFormData.controls[labelID].setErrors({ 'invalidFile': true });
    this.userFormData.controls[labelID].markAsTouched();
  }

  clickOnDownLoad(){
    
  }

  public setDetails(userDetails,userFormData) {
    let userobjWrapper: Object = {};
    var userobjArr = [];
    for (var key in userDetails) {
      var checkIsArray = Array.isArray(userDetails[key])
      if (checkIsArray && key != 'userPhoto') {
        let multiselectArray: any[] = userDetails[key];
        var buildStringValue: string = "";
        for (var mskay in multiselectArray) {
          if (buildStringValue == "") {
            buildStringValue = multiselectArray[mskay].id;
          } else {
            buildStringValue = buildStringValue + "," + multiselectArray[mskay].id;
          }

        }
        let userobj: Object = {};
        userobj["instanceDataElement"] = key;
        userobj["dataElementInstanceValue"] = buildStringValue;
        userobj["dataElementInstanceStatusId"] = 1;
        userobjArr[key] = userobj;

      } else {
        if (key != 'userPhoto') {
          let userobj: Object = {};
          userobj["instanceDataElement"] = key;
          userobj["dataElementInstanceValue"] = userDetails[key];
          userobj["dataElementInstanceStatusId"] = 1;
          if(userobj!=null)
          userobjArr.push(userobj);
        }
      }
    }

    /* Submit data with file  */
    // let formdata: FormData = new FormData();
   //  formdata.append('functionId', this.functionId);
   //  formdata.append('processId', this.processId);
   //  formdata.append('taskId', this.taskId);
    // formdata.append('file', this.currentFileUpload); 
    // formdata.append('formDetails', userobjArr);


    //  userDetails.name = "";
    userobjWrapper["functionId"] = this.functionId;
    userobjWrapper["processId"] = this.processId;
    userobjWrapper["taskId"] = this.taskId;
    userobjWrapper["formId"] = parseInt(this.formId);
    userobjWrapper["taskFlowId"] = parseInt(this.taskFlowId);
    userobjWrapper["taskInstanceID"] = this.taskInstanceID;
    userobjWrapper["formDetails"] = userobjArr;

    console.log('userobjWrapper',userobjWrapper);
  
    


    /*if (this.editFormObj != null && this.editFormObj.formId != null && this.editFormObj.taskInstanceID) {
      let taskInstanceId = this.editFormObj.taskInstanceID;
      userobjWrapper["taskInstanceID"] = this.editFormObj.taskInstanceID;
    }
    else if (this.editFormObj != null && this.editFormObj.reviewTaskInstanceID != null) {
      userobjWrapper["taskInstanceID"] = this.editFormObj.reviewTaskInstanceID;
    }*/

    /*  let headers = new HttpHeaders();
     headers = headers.set('Content-Type', 'multipart/form-data; boundary=----WebKitFormBoundaryQJQI18FQH6BdzAAr'); */

    this.http.post(this.Core_URL + '/submitTask', userobjWrapper, { responseType: 'text' })
      .subscribe(data => {
       // this.data = data;
       if(data == 'ok'){
        alert('Update Successfully');
        this.router.navigate(['/mytask']);
      }
        this.dialogRef.close();
      }, error => {
        console.log(error);

      });
  }

  private clearShareObj() {
    this.shareServices.setgridChangeMessage(null);
  }



  getAllPrivilegeList() {
    this.http.get(this.shareServices.Role_URL + '/getAllPrivilegeByOrgIdAndPrivilegeType/1')
      .subscribe(data => {
        console.log(data);
        this.dropdownList = data;
      });
  }

}


