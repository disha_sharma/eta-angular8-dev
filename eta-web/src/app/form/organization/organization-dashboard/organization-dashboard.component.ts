import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServiceService } from './../../../eta-services/service.service';


@Component({
  selector: 'app-organization-dashboard',
  templateUrl: './organization-dashboard.component.html',
  styleUrls: ['./organization-dashboard.component.css'],
  providers: [ServiceService]
})
export class OrganizationDashboardComponent implements OnInit {

  /***   For Sub Breadcrumb  **/
  public breadcrumb: String;
  /*** For Main Breadcrumb **/
  public mainbreadcrumb: string;
  /** Read Org URL form service  */
  public ORGANIZATION_URL: any;
  /** Org Data URL */
  public organizationUrl: any;
  /** Get Organization Grid column  */
  public organizationUrlCol: any;
  /** Store Selected From Grid */
  public organizationObj: any;

  public orgDataUrl: any;

  public defaultColumnToSort: any;
  public isEditAllowed: boolean = false;

  constructor(private r: Router, private newService: ServiceService) {
    this.getHeaderMenuList();
    this.defaultColumnToSort = "orgName"
    this.ORGANIZATION_URL = this.newService.Org_URL;
    this.organizationUrl = this.ORGANIZATION_URL + '/getAllOrgGridData';
    this.orgDataUrl = this.ORGANIZATION_URL + '/getOrgAngularGrid';

  }

  ngOnInit() {

    // this.newService.currentMessage.subscribe(message => this.organizationObj = message);
    this.newService.refreshEditMessage();
    this.newService.currentMessage.subscribe(message => {
      this.organizationObj = message
      if (message != 'editFalse') {
        this.isEditAllowed = true;
      }
    });
    this.newService.getOrgnizationOuterColumn()
      .subscribe(res => {
        this.organizationUrlCol = res.coloumnName;
      })

    this.newService.getOrgAngularGridData()
      .subscribe(data => {
        // console.log(data);

      })

  }

  addOrg() {
    this.r.navigate(["/addorg"]);
  }

  editOrg() {
    this.r.navigate(["/editorg/" + this.organizationObj.orgId]);
  }

  menuList: any = [];
  public addButton: boolean;
  public addButtonText: any;
  public editButton: any;
  public editButtonText: any;

  getHeaderMenuList() {
    this.newService.getHeaderMenu()
      .subscribe(res => {
        this.menuList = res;
        this.menuList.forEach(element => {
          if (element.routerLink == "eta-web/organizationdashboard") {
            this.mainbreadcrumb = element.translation;
          }
          if (element.routerLink == "eta-web/organizationdashboard") {
            this.breadcrumb = element.translation;
          }
          if (element.menuType == 5 && element.routerLink == "eta-web/addorg") {
            this.addButton = true;
            this.addButtonText = element.translation;
          }
          if (element.menuType == 5 && element.routerLink == "eta-web/editorg") {
            this.editButton = true;
            this.editButtonText = element.translation;
          }
        })

      })
  }

  shownRouterLink(routerlink) {
    this.r.navigate(["/" + routerlink]);
  }

  activeLink = "organizationdashboard";
  background = '';

  toggleBackground() {
    this.background = this.background ? '' : 'primary';
  }

}
