import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrgTryForFreeComponent } from './org-try-for-free.component';

describe('OrgTryForFreeComponent', () => {
  let component: OrgTryForFreeComponent;
  let fixture: ComponentFixture<OrgTryForFreeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrgTryForFreeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrgTryForFreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
