import { Location } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ServiceService } from './../../../eta-services/service.service';

@Component({
  selector: 'app-org-try-for-free',
  templateUrl: './org-try-for-free.component.html',
  styleUrls: ['./org-try-for-free.component.css']
})
export class OrgTryForFreeComponent implements OnInit {
  /** Form Group  */
  public orgfrm: FormGroup;

  /**
   *
   * List of master country
   * @type {*}
   * @memberof OrgTryForFreeComponent
   */
  public countryList: any = [];
  public isSpinnerShow: boolean = false;
  public addOrEditOrg:boolean=false;

  constructor(private _formBuilder: FormBuilder, private http: HttpClient, private newService: ServiceService, private r: Router, private route: ActivatedRoute, private location: Location) { }

  ngOnInit() {
    this.orgfrm = this._formBuilder.group({
      orgName: ['', [Validators.required]],
      orgType: ['', [Validators.required]],
      phoneNumber: [],
      contactPerson: [],
      emailaddress: [],
      faxNumber: [],
      website: [],
      orgDomain: ['', [Validators.required , Validators.pattern('')]],
      remarks: [],
      image: [],
      countryId: ['', [Validators.required]],
      agreeAgreement: [],
      firstName:[],
      lastName:[],
      
      userName:[]
    })

    /** ***********************************************************For getting All Country Location*********************************** **/
    this.newService.getAllCountry()
      .subscribe(data=>{
        
        this.countryList = data;

      });


  }

  onSubmit() {
    let orgObj: Object = {};
    orgObj["orgId"] = this.orgfrm.value.orgId;
    orgObj["orgName"] = this.orgfrm.value.orgName;
    orgObj["orgType"] = this.orgfrm.value.orgType;
    orgObj["phoneNumber"] = this.orgfrm.value.phoneNumber;
    orgObj["contactPerson"] = this.orgfrm.value.contactPerson;
    orgObj["emailaddress"] = this.orgfrm.value.emailaddress;
    orgObj["faxNumber"] = this.orgfrm.value.faxNumber;
    orgObj["orgDomain"] = this.orgfrm.value.orgDomain;
    orgObj["website"] = this.orgfrm.value.website;
    orgObj["remarks"] = this.orgfrm.value.remarks;
    // orgObj["orgLogo"] = this.imageName;
    orgObj["countryID"] = this.orgfrm.value.countryId;
    orgObj["agreeAgreement"] = this.orgfrm.value.agreeAgreement;
    orgObj["firstName"] = this.orgfrm.value.firstName;
    orgObj["lastName"] = this.orgfrm.value.lastName;
    orgObj["userName"] = this.orgfrm.value.userName;

    //this.organizationUrlCol = null;
    console.log(orgObj);

    if (this.orgfrm.valid) {
      this.isSpinnerShow = true;
      this.newService.getStartYourFreeTrial(orgObj)
        .subscribe(data => {
          console.log(data);
          if (data.orgId != null) {
            this.isSpinnerShow = false;
            alert("Data Is submit")
            this.r.navigate(['/login']);
          } else {
            alert("Orgnization Data Not Added");
          }
        },error=>{
          this.isSpinnerShow = false;
          alert("Data Is Not submit")
        });

    }


  }

  cancel(){
    this.location.back();
  }

  usernameTaken: string;
  showSpinner: boolean = false;
  checkUserNameAvailability(username) {
    this.showSpinner = true;
    let usernameValue: string;


    this.newService.checkUserNameAvailabilityFreeTrial(username.target.value)
      .subscribe(res => {
        console.log(res);
        if (res==true) {
          // alert("user name is already Taken by someone else please try again !!!")
          this.showSpinner = false;
          usernameValue = "That username is taken. try another.";
          this.usernameTaken = usernameValue;
        }
        if (res == false) {
          this.showSpinner = false;
          usernameValue = "This username is available"
          this.usernameTaken = usernameValue;
        }

      }, error => {
        this.showSpinner = false;
        console.log("I am in error ");
        //this.usernameTaken =usernameValue;
      })



  }

}
