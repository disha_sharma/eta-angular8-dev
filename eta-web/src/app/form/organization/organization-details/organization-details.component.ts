import { Location } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Component, EventEmitter, OnInit, Output, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ServiceService } from './../../../eta-services/service.service';


@Component({
  selector: 'app-organization-details',
  templateUrl: './organization-details.component.html',
  styleUrls: ['./organization-details.component.css'],
  providers: [ServiceService]
})
export class OrganizationDetailsComponent implements OnInit {
  /***   For Sub Breadcrumb  **/
  public breadcrumb: String;
  /*** For Main Breadcrumb **/
  public mainbreadcrumb: string;
  /** Read Org URL form service  */
  public ORGANIZATION_URL: any;
  /** Org Data URL */
  public organizationUrl: any;
  /** Org Grid Column List */
  public organizationUrlCol: any;
  public checkorgid: any;
  public orgIdOnChange;
  /** Form Group  */
  public orgfrm: FormGroup;
  public countryList: any = [];
  /** For Multiselect countryDropdowm List */
  public countryDropdownList = [];
  /** For Multiselect Dropdowm  Selected FList */
  public selectedItems = [];
  /** MultiSelect Setting */
  public dropdownSettings = {};
  /** File uploading  */
  public selectedFiles: FileList
  /** file uploading  */
  public currentFileUpload: File
  /** File Uploading image Name */
  public imageName: any;
  /** OrgId to send With Image  */
  public sendOrgId: any;

  public orgInnerDataUrl: any;
  public defaultColumnToSort: any;
  /** For Multiselect LanguageDropdowm List */
  public languageDropdownList = [];
  /** Form Group */
  public orgCountryForm: FormGroup;
  /** Form Group */
  public orgLanguageForm: FormGroup;
  public selectedCountryItems = [];
  public selectedLanguageItems = [];
  public languageList: any = [];
  public dropdownList = [];
  public allCountryList = [];
  public isSpinnerShow: boolean = false;
  public addOrEditOrg: boolean = false;
  public allOrgTypeList = [];

  @Input() myorgId: number;
  @Output() sendOrgToMultiSelect = new EventEmitter();
  @Output() checkBoxOutput = new EventEmitter();

  constructor(private _formBuilder: FormBuilder, private http: HttpClient, private newService: ServiceService, private r: Router, private route: ActivatedRoute, private location: Location) {
    this.getHeaderMenuList();//get headermenu  
    this.getAllCountryList();//get country list
    this.getAllOrgTypeList();//get organization type
    
    this.defaultColumnToSort = "orgName";
    this.ORGANIZATION_URL = newService.Org_URL;
    this.organizationUrl = this.ORGANIZATION_URL + '/getInnerGridByOrgId';
    this.orgInnerDataUrl = this.ORGANIZATION_URL + '/getOrgAngularInnnerGrid';
    this.route.params.subscribe(params => {
      this.checkorgid = +params['orgId'];
    
    });
  }


  hypercol: any = [
    { check: 'link' }
  ];

  countrylist: any[];
  languagelist: any[];
  orglist: any[];
  _orgId: any = 1;

  showOrgValueForEditOrgDetails(change) {
    this.orgIdOnChange = change;
  }


  ngOnInit() {
    this.getOrgDetailsColoumnList();

    this.getOrgdatabyOrgId(this.checkorgid)
    this.getLocationByOrgId(this.checkorgid);
    this.getLanguageByOrgId(this.checkorgid);

    this.orgfrm = this._formBuilder.group({
      orgName: ['', [Validators.required]],
      orgType: ['', [Validators.required]],
      phoneNumber: ['', [Validators.required, Validators.maxLength(12), Validators.minLength(10), Validators.pattern('[0-9]+')]],
      contactPerson: [],
      emailaddress: ['', [Validators.required, Validators.email]],
      faxNumber: [],
      website: [],
      orgDomain: [],
      remarks: [],
      image: [],
      countryId: ['', [Validators.required]],
      firstName: [],
      lastName: [],
      fileNameDisplay: [],
      userName:[]
    })
    this.orgCountryForm = this._formBuilder.group({
      selectedCountryItems: [],
    })
    this.orgLanguageForm = this._formBuilder.group({
      selectedLanguageItems: []
    })

    this.newService.getOrgAngularInnnerGridData()
      .subscribe(data => {
        // console.log(data);

      })

    /** ***********************************************************For getting All Country Location*********************************** **/
    this.newService.getAllCountry()
      .subscribe(data => {
        this.countryDropdownList = data;
        data.forEach(element => {
          let id = element.countryId;
          let countryName = element.countryName;
          let countryObj: any = {};
          countryObj.id = id;
          countryObj.itemName = countryName;
          this.countryList.push(countryObj);
        });
      });

    /*************************************************************For getting All Language**************************************************/
    this.newService.getAllLanguageName()
      .subscribe(data => {
        this.languageDropdownList = data;
        data.forEach(element => {
          let id = element.languageID;
          let language = element.language;
          let languageObj: any = {};
          languageObj.id = id;
          languageObj.itemName = language;
          this.languageList.push(languageObj);
        });

      });

    this.dropdownSettings = {
      singleSelection: false,
      text: "Select Countries",
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      enableSearchFilter: true,
      classes: "myclass custom-class"
    };

  }

  getOrgdatabyOrgId = function (obj) {
    this.newService.getOrgDetailsByOrgId(obj)
      .subscribe(data => {

        let orgNameId :number = +data.orgType;
        this.addOrEditOrg = true;
        this.orgfrm = this._formBuilder.group({
          orgId: [data.orgId],
          orgName:  new FormControl({value: data.orgName, disabled: true}), 
          orgType: [orgNameId],
          phoneNumber: [data.phoneNumber],
          contactPerson: [data.contactPerson],
          emailaddress: [data.emailaddress],
          faxNumber: [data.faxNumber],
          orgDomain: [data.orgDomain],
          website: [data.website],
          remarks: [data.remarks],
          countryId: [data.countryID],
          fileNameDisplay: [data.orgLogo],
        })
        this.sendOrgId = data.orgId;
        this.getlogo = data.orgLogo;
        this.newService.getlogoImage(this.sendOrgId, this.getlogo)
          .subscribe(res => {
            this.createImageFromBlob(res)
          })
      });
  }

  onSubmit() {
    let orgObj: Object = {};
    orgObj["orgId"] = this.orgfrm.value.orgId;
    orgObj["orgName"] = this.orgfrm.value.orgName;
    orgObj["orgType"] = this.orgfrm.value.orgType;
    orgObj["phoneNumber"] = this.orgfrm.value.phoneNumber;
    orgObj["contactPerson"] = this.orgfrm.value.contactPerson;
    orgObj["emailaddress"] = this.orgfrm.value.emailaddress;
    orgObj["faxNumber"] = this.orgfrm.value.faxNumber;
    orgObj["orgDomain"] = this.orgfrm.value.orgDomain;
    orgObj["website"] = this.orgfrm.value.website;
    orgObj["remarks"] = this.orgfrm.value.remarks;
    orgObj["orgLogo"] = this.imageName;
    orgObj["countryID"] = this.orgfrm.value.countryId;
    orgObj["firstName"] = this.orgfrm.value.firstName;
    orgObj["lastName"] = this.orgfrm.value.lastName;
    orgObj["userName"] = this.orgfrm.value.userName;


    if (this.orgfrm.valid) {
      this.isSpinnerShow = true;
      this.newService.postOrganization(orgObj)
        .subscribe(data => {
          this.isSpinnerShow = false;
          this.sendOrgId = data.orgId;
          this.checkorgid = data.orgId;
          if (this.sendOrgId != null) {
            alert("Orgnization Data Added");
            this.organizationUrlCol  =  null;
            this.getOrgDetailsColoumnList();
          } else {
            alert("Orgnization Data Not Added");
          }

          let getOrgLogo: string = data.orgLogo;
          if (this.sendOrgId != 0) {
            if (this.selectedFiles != null && this.selectedFiles.length > 0) {
              this.currentFileUpload = this.selectedFiles.item(0);
              let formdata: FormData = new FormData();
              formdata.append('file', this.currentFileUpload);
              this.newService.imageUplaod(this.currentFileUpload, this.sendOrgId, getOrgLogo)
                .subscribe(data => {

                })
            }
          }
        }, error => {
          this.isSpinnerShow = false;
          alert("org Created with some error")
          this.getOrgDetailsColoumnList();
        });
    } else {
      alert("Please All Mandatory fileds")
    }


  }

  public getAllOrgDetails() {
    this.newService.getAllOrgDetails()
      .subscribe(data => {
        this.orglist = data;
      });
  }

  selectFile(event) {
    this.imageName = event.target.files[0].name;
    this.selectedFiles = event.target.files;
    this.orgfrm.get('fileNameDisplay').patchValue(event.target.files[0].name)

  }


  imageToShow: any = null;

  createImageFromBlob(image: Blob) {
    let reader = new FileReader();
    reader.addEventListener("load", () => {
      this.imageToShow = reader.result;
    }, false);

    if (image) {
      reader.readAsDataURL(image);
    }
  }

  private getOrgDetailsColoumnList() {
    this.newService.getOrgnizationInnerColumn()
      .subscribe(res => {

        this.organizationUrlCol = res.coloumnName;

      })
  }

  cancel() {
    localStorage.setItem('backlocation', "organizationdashboard");
    this.location.back();
  }

  /** method to get braedcrumb*/

  public mainRouterLink: any;
  public breadcrumbRouterLink: any;
  public menuList: any = [];
  getHeaderMenuList() {
    this.newService.getHeaderMenu()
      .subscribe(res => {
        this.menuList = res;
        this.menuList.forEach(element => {
          if (element.routerLink == "eta-web/organizationdashboard") {
            this.mainbreadcrumb = element.translation;
          }
          if (element.routerLink == "eta-web/organizationdashboard") {
            this.breadcrumb = element.translation;
          }
        })

      })
  }
  /* method to get braedcrumb end*/

  public shownRouterLink(routerlink) {
    this.r.navigate(["/" + routerlink]);
  }

  selectAllCountry(list) {
    let tempCountryList: any = [];
    list.forEach(element => {
      tempCountryList.push(element.id);
    });
    this.orgCountryForm.get('selectedCountryItems').patchValue(tempCountryList);
  }
  deselectAllCountry() {
    this.orgCountryForm.get('selectedCountryItems').patchValue([]);
  }

  selectAllLanguage(list) {
    let tempLanguageList: any = [];
    list.forEach(element => {
      console.log("element" + element);

      tempLanguageList.push(element.id);
    });
    this.orgLanguageForm.get('selectedLanguageItems').patchValue(tempLanguageList);
  }
  deselectAllLanguage() {
    this.orgLanguageForm.get('selectedLanguageItems').patchValue([]);
  }

  /**************************************************************Edit Country Location Details*************************************************/
  getLocationByOrgId = function (obj) {
    this.newService.getLocationByOrgId(obj)
      .subscribe(data => {
        let countryList: any = [];
        this.countryDropdownList = data;
        data.forEach(element => {
          let id = element.location;
          let countryName = element.translation;
          let countryObj: any = {};
          countryObj.id = id;
          countryObj.itemName = countryName;
          countryList.push(countryObj);
        });
        this.selectAllCountry(countryList);
      });
  }

  /***********************************************************Save Country Location*******************************************************************************/

  onSubmitCountry(objvalue) {
    let location = [];
    objvalue.selectedCountryItems.forEach(element => {
      let obj: any = {};
      obj['location'] = element;
      location.push(obj);
    });
    let orgCountryObj: Object = {};
    orgCountryObj["orgId"] = this.checkorgid;
    orgCountryObj["location"] = location;
    this.newService.postOrgWrapperLocation(orgCountryObj);
  }

  /*******************************************************************Edit Language Details**********************************************************/
  getLanguageByOrgId = function (obj) {
    this.newService.getLanguageByOrgId(obj)
      .subscribe(data => {
        let languageList: any = [];
        this.languageDropdownList = data;
        data.forEach(element => {
          let id = element.languageId;
          let LanguageID = element.Language;
          let countryLanguageObj: any = {};
          countryLanguageObj.id = id;
          countryLanguageObj.itemName = LanguageID;
          languageList.push(countryLanguageObj);
        });
        this.selectAllLanguage(languageList);

      });
  }

  /***********************************************************Save Language Details*******************************************************************************/
  onSubmitLanguage(objvalue) {
    let language = [];
    objvalue.selectedLanguageItems.forEach(element => {
      let obj: any = {};
      obj['languageID'] = element;
      language.push(obj);
    });
    let orgLanguageObj: Object = {};
    orgLanguageObj["orgId"] = this.checkorgid;
    orgLanguageObj["language"] = language;
    this.newService.postOrgLangWrapperLocation(orgLanguageObj);

  }

  /******************** Get All Country List For Org *********************/
  public getAllCountryList() {
    this.newService.getAllCountry()
      .subscribe(data => {
        this.allCountryList = data;
      });
  }
  usernameTaken: string;
  showSpinner: boolean = false;
  checkUserNameAvailability(username) {
    this.showSpinner = true;
    let usernameValue: string;


    this.newService.checkUserNameAvailability(username.target.value)
      .subscribe(res => {
        console.log(res);
        if (res==true) {
          // alert("user name is already Taken by someone else please try again !!!")
          this.showSpinner = false;
          usernameValue = "That username is taken. try another.";
          this.usernameTaken = usernameValue;
        }
        if (res == false) {
          this.showSpinner = false;
          usernameValue = "This username is available"
          this.usernameTaken = usernameValue;
        }

      }, error => {
        this.showSpinner = false;
        console.log("I am in error ");
        //this.usernameTaken =usernameValue;
      })



  }

  /******************** Get All Organization Type List *********************/
  public getAllOrgTypeList(){
    this.newService.getAllOrgType()
      .subscribe(data => {
        this.allOrgTypeList = data;
      });
  }

}