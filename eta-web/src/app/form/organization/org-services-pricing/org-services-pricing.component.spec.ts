import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrgServicesPricingComponent } from './org-services-pricing.component';

describe('OrgServicesPricingComponent', () => {
  let component: OrgServicesPricingComponent;
  let fixture: ComponentFixture<OrgServicesPricingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrgServicesPricingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrgServicesPricingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
