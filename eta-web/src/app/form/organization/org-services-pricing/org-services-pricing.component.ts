import { Component, OnInit } from '@angular/core';
import { ServiceService } from '../../../eta-services/service.service';
import { ActivatedRoute, Router } from '@angular/router';
import 'rxjs/add/operator/filter';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-org-services-pricing',
  templateUrl: './org-services-pricing.component.html',
  styleUrls: ['./org-services-pricing.component.css']
})
export class OrgServicesPricingComponent implements OnInit {

  public licenseId: number = 28;

  public licenseDetials: any = [];
  public fatched_orgId: any;
  isLoggedIn$: Observable<boolean> ;
  constructor(private newService: ServiceService, private r: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    this.isLoggedIn$ = this.newService.isLoggedIn;
    this.newService.getMasterAuthLicesingList()
      .subscribe(data => {
        console.log(data);
        this.licenseDetials = data;
      })

    this.route.queryParams
      .filter(params => params.o)
      .subscribe(params => {
        console.log(params); // {o: "orgId"}

        this.fatched_orgId = params.o;
        console.log(this.fatched_orgId); // orgID fatched 
      });
  }



  buyPackage(event) {
    let authObj: Object = {};
    authObj["licenseId"] = event;
    authObj["orgId"] = this.fatched_orgId;
    this.newService.buyAuthLicense(authObj)
      .subscribe(data => {
        console.log(data);
        alert("data submit successfully");
        if (data.orgId != null) {
          this.newService.login();
        }

      })






  }

}
