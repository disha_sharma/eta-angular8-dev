import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { ServiceService } from './../../../eta-services/service.service'

@Component({
  selector: 'app-org-customtable',
  templateUrl: './org-customtable.component.html',
  styleUrls: ['./org-customtable.component.css']
})
export class OrgCustomtableComponent implements OnInit {

  customTablefrm: FormGroup;

  constructor(private _formBuilder: FormBuilder, private http: HttpClient, private route: ActivatedRoute,
    private r: Router, private shareServices: ServiceService) { }

  ngOnInit() {
    this.customTablefrm = this._formBuilder.group({
      tableName: [],
      tableOption: [],
    })
  }
  public onSubmit() {

    console.log(this.customTablefrm.value);

    this.shareServices.setCustomTable(this.customTablefrm.value).subscribe(data => {
      console.log(data);
      alert("Data Submited");
    }
    ,error=>{
      alert("Data Not Submited");
    });
  }

  public cancel() {
    this.r.navigateByUrl('createform');
  }
}
