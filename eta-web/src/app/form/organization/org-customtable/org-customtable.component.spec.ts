import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrgCustomtableComponent } from './org-customtable.component';

describe('OrgCustomtableComponent', () => {
  let component: OrgCustomtableComponent;
  let fixture: ComponentFixture<OrgCustomtableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrgCustomtableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrgCustomtableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
