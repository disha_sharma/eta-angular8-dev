import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrganizationCalendarComponent } from './organization-calendar.component';

describe('OrganizationDetailsComponent', () => {
  let component: OrganizationCalendarComponent;
  let fixture: ComponentFixture<OrganizationCalendarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrganizationCalendarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrganizationCalendarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
