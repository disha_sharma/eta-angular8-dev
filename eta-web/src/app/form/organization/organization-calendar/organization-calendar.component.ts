import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ServiceService } from './../../../eta-services/service.service';

@Component({
  selector: 'app-organization-calendar',
  templateUrl: './organization-calendar.component.html',
  styleUrls: ['./organization-calendar.component.css'],
  providers: [ServiceService]
})
export class OrganizationCalendarComponent implements OnInit {

  /***   For Sub Breadcrumb  **/
  public breadcrumb: String;
  /*** For Main Breadcrumb **/
  public mainbreadcrumb: string;
  /** Get ORG Cal URL Form SharedService */
  public ORG_CAL_URL: any;
  /** Form builder */
  public orgfrm: FormGroup;
  /** Days List on Change */
  public daysIdOnChange;
  /** OrgId on Org List on Change */
  public orgIdOnChange;
  /** Location Value on change */
  public locationIdOnChange;
  /** Org Cal Data Url */
  public organizationCalUrl: any;
  /**Get Cal Id form Selected Checked Value  */
  public calCheckedId: any;
  /** Org List  */
  public orglist: any = [];
  /** calender Id */
  public calenderId: any;
  /** ORG calender grid column List  */
  public organizationCalUrlCol: any;

  /** working hour array list */
  public workingHours: any = [];
  public workingMinutes:any =[];
  public workingType :any=[];

  /** Selected Org Id */
  public ETA_SELECTED_ORGID: any;

  /** Org Name */
  public orgName: any;

  public workingCalenderInnerDataUrl: any;
  public defaultColumnToSort:any;

  constructor(private _formBuilder: FormBuilder, private newService: ServiceService, private route: ActivatedRoute, private location: Location) {
    this.getHeaderMenuList();
    this.defaultColumnToSort ="orgId";
    this.getAllOrgDays();
    this.getAllOrgLoc();
    this.ORG_CAL_URL = newService.Org_Cal_URL;
    this.organizationCalUrl = this.ORG_CAL_URL + '/getWorkingCalenderInnerGrid';
    this.workingCalenderInnerDataUrl=this.ORG_CAL_URL + '/getWorkingCalenderAngularInnnerGrid';
    this.route.params.subscribe(params => {
      this.calCheckedId = +params['id'];
      this.getOrgCalenderById(this.calCheckedId)
    });
    this.ETA_SELECTED_ORGID = localStorage.getItem("EtaSelectOrgId");
  }


  dayslist: any = {};
  selectOrganizationDays(change) {
    this.daysIdOnChange = change;
  }

  locationlist: any = {};
  selectOrganizationLocation(change) {
    this.locationIdOnChange = change;
  }

  private getAllOrgDays() {
    this.newService.getAllOrgDays()
      .subscribe(data => {
        this.dayslist = data;
      });
  }

  private getAllOrgLoc() {
    this.newService.getAllCountry()
      .subscribe(data => {
        this.locationlist = data;
      });
  }

  ngOnInit() {
    this.getWorkingCalenderColoumnList();
    this.orgfrm = this._formBuilder.group({
      starttime: new FormControl('', []),
      endtime: new FormControl('', []),
      startHH: new FormControl('', []),
      startMM: new FormControl('', []),
      startAMPM: new FormControl('', []),
      endHH: new FormControl('', []),
      endMM: new FormControl('', []),
      endAMPM: new FormControl('', []),
      dayid: new FormControl('', []),
      totalworkinghours: new FormControl('', []),
      location: new FormControl('', []),
      orgId: new FormControl('', []),
      orgname: [this.ETA_SELECTED_ORGID],

    })

    this.newService.getWorkingCalenderAngularInnerGridData()
    .subscribe(data => {
      // console.log(data);

    })

    this.newService.getAllOrgDetails()
      .subscribe(data => {
        this.orglist = data;
      });



    this.workingMinutes = [
      { id: "00", value: "00" },
      { id: "05", value: "05" },
      { id: "10", value: "10" },
      { id: "15", value: "15" },
      { id: "20", value: "20" },
      { id: "25", value: "25" },
      { id: "30", value: "30" },
      { id: "35", value: "35" },
      { id: "40", value: "40" },
      { id: "45", value: "45" },
      { id: "50", value: "50" },
      { id: "55", value: "55" },
    ]
    this.workingHours = [
      { id: "00", value: "01" },
      { id: "01", value: "01" },
      { id: "02", value: "02" },
      { id: "03", value: "03" },
      { id: "04", value: "04" },
      { id: "05", value: "05" },
      { id: "06", value: "06" },
      { id: "07", value: "07" },
      { id: "08", value: "08" },
      { id: "09", value: "09" },
      { id: "10", value: "10" },
      { id: "11", value: "11" },
      { id: "12", value: "12" },
    ]
    this.workingType=[
      { id: "am", value: "AM" },
      { id: "pm", value: "PM" },
    ]



  }

  onSubmit = function (calKey) {
    //alert(calKey.location);
    let orgfrm: Object = {};
    orgfrm["id"] = this.calenderId;
    orgfrm["orgId"] = this.orgfrm.value.orgId;
    orgfrm["starttime"] = this.orgfrm.value.starttime;
    orgfrm["endtime"] = this.orgfrm.value.endtime;
    orgfrm["startHH"] = this.orgfrm.value.startHH;
    orgfrm["startMM"] = this.orgfrm.value.startMM;
    orgfrm["startAMPM"] = this.orgfrm.value.startAMPM;
    orgfrm["endHH"] = this.orgfrm.value.endHH;
    orgfrm["endMM"] = this.orgfrm.value.endMM;
    orgfrm["endAMPM"] = this.orgfrm.value.endAMPM;
    orgfrm["totalworkinghours"] = this.orgfrm.value.totalworkinghours;
    orgfrm["dayid"] = calKey.dayid;
    orgfrm["location"] = calKey.location;
    //this.organizationCalUrlCol = null;
    if (this.orgfrm.valid) {
    this.newService.postOrgTiming(orgfrm).subscribe(data =>{
      let retrunData = data;
      if(retrunData.id!=null && retrunData.id != 0 ){
        alert("Data added !");
        this.organizationCalUrlCol = null;
        this.getWorkingCalenderColoumnList();
      }
      else{
        alert("Data not added !");
      }
    });
  }else{
    alert("Enter Data")
  }
}

  getOrgCalenderById(calid) {
    this.newService.getOrgTimingById(calid)
      .subscribe(data => {
        this.calenderId = data.id;
        this.orgfrm = this._formBuilder.group({
          Id: [data.id],
          dayid: [data.dayid],
          orgId: [data.orgId],
          location: [data.location],
          starttime: [data.starttime],
          startHH: [data.startHH],
          startMM: [data.startMM],
          startAMPM: [data.startAMPM],
          endtime: [data.endtime],
          endHH: [data.endHH],
          endMM: [data.endMM],
          endAMPM: [data.endAMPM],
          totalworkinghours: [data.totalworkinghours],
        })
      });

  }
  menuList: any = [];
  getHeaderMenuList() {
    this.newService.getHeaderMenu()
      .subscribe(res => {
        this.menuList = res;
        this.menuList.forEach(element => {
          if (element.routerLink == "eta-web/organizationdashboard") {
            this.mainbreadcrumb = element.translation;
          }
          if (element.routerLink == "eta-web/calendardashboard") {
            this.breadcrumb = element.translation;
          }
        })

      })
  }

  private getWorkingCalenderColoumnList() {
    this.newService.getOrgCalenderInnerColumn()
      .subscribe(res => {
        this.organizationCalUrlCol = res.coloumnName;
      })
  }

  cancel() {
    localStorage.setItem('backlocation', "showorgdetails");
    this.location.back();
  }

  public getOrgId(orgList) {
    orgList.forEach(element => {

      if (this.ETA_SELECTED_ORGID == null || this.ETA_SELECTED_ORGID == 0) {
        this.ETA_SELECTED_ORGID = 1;
      }
      if (this.ETA_SELECTED_ORGID == element.Id) {

        this.orgName = element.orgName;
      }

    });
  }

}
