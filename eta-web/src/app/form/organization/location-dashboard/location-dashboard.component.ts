import { Component, OnInit } from '@angular/core';
import { ServiceService } from '../../../eta-services/service.service';
import { Router } from '@angular/router';



@Component({
  selector: 'app-location-dashboard',
  templateUrl: './location-dashboard.component.html',
  styleUrls: ['./location-dashboard.component.css']
})
export class LocationDashboardComponent implements OnInit {

  /**
   *
   *For Location url variable to show grid information.
   * @type {*}
   * @memberof LocationDashboardComponent
   */
  public location_url: any;

  /***   For Sub Breadcrumb  **/
  public breadcrumb: String;
  /*** For Main Breadcrumb **/
  public mainbreadcrumb: string;

  public defaultColumnToSort: any;
  public isEditAllowed: boolean = false;



  constructor(private newService: ServiceService, private r: Router) {
    this.location_url = this.newService.Org_URL + '/getLocationMainGrid';
    this.defaultColumnToSort = "countryId"
    this.getHeaderMenuList();
  }

  ngOnInit() {
    /*  this.newService.refreshEditMessage();
    this.newService.currentMessage.subscribe(message => {
          this.userObj = message
          if(message != 'editFalse'){
            this.isEditAllowed =true;
          } 
        });  */
  }

  menuList: any = [];
  public addButton: boolean;
  public addButtonText: any;
  public editButton: any;
  public editButtonText: any;

  getHeaderMenuList() {
    this.newService.getHeaderMenu()
      .subscribe(res => {
        this.menuList = res;
        this.menuList.forEach(element => {
          if (element.routerLink == "eta-web/organizationdashboard") {
            this.mainbreadcrumb = element.translation;
          }
          if (element.routerLink == "eta-web/locationdashboard") {
            this.breadcrumb = element.translation;
          }
          if (element.menuType == 5 && element.routerLink == "eta-web/addgaconfig") {
            this.addButton = true;
            this.addButtonText = element.translation;
          }
          if (element.menuType == 5 && element.routerLink == "eta-web/editgaconfig") {
            this.editButton = true;
            this.editButtonText = element.translation;
          }

        })

      })
  }

  /**
   *Add location form to enter information about location details
   *
   * @memberof LocationDashboardComponent
   */
  addLoc() {
    this.r.navigate(["/countrydetails"]);
  }

  shownRouterLink(routerlink) {
    this.r.navigate(["/" + routerlink]);
  }
  activeLink = "locationdashboard";
  background = '';

  toggleBackground() {
    this.background = this.background ? '' : 'primary';
  }


}
