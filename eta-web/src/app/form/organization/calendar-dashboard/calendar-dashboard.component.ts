import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServiceService } from './../../../eta-services/service.service';


@Component({
  selector: 'app-calendar-dashboard',
  templateUrl: './calendar-dashboard.component.html',
  styleUrls: ['./calendar-dashboard.component.css']
})
export class CalendarDashboardComponent implements OnInit {


  /***   For Sub Breadcrumb  **/
  public breadcrumb: String;
  /*** For Main Breadcrumb **/
  public mainbreadcrumb: string;
  /** For Store Org Calendar URL */
  public ORG_CAL_URL: any;
  /** Org Calendar Data url */
  public organizationCalUrl: any;
  /** Org Calendar Column List */
  public organizationCalUrlCol: any;
  /** Get Selected Value From Grid */
  public workingCalendarObj: any;

  public workingCalenderUrl: any;

  public defaultColumnToSort: any;
  public isEditAllowed: boolean = false;

  constructor(private r: Router, private newService: ServiceService) {
    this.getHeaderMenuList();
    this.defaultColumnToSort = "orgId"
    this.ORG_CAL_URL = this.newService.Org_Cal_URL;
    this.organizationCalUrl = this.ORG_CAL_URL + '/getWorkingCalenderDashboardGrid';
    this.workingCalenderUrl = this.ORG_CAL_URL + '/getWorkingCalenderAngularGrid';
  }

  ngOnInit() {
    //this.newService.currentMessage.subscribe(message => this.workingCalendarObj = message);
    this.newService.refreshEditMessage();
    this.newService.currentMessage.subscribe(message => {
      this.workingCalendarObj = message
      if (message != 'editFalse') {
        this.isEditAllowed = true;
      }
    });
    this.newService.getOrgCalenderColumn()
      .subscribe(res => {
        this.organizationCalUrlCol = res.coloumnName;
      })

    this.newService.getWorkingCalenderAngularGridData()
      .subscribe(data => {
        // console.log(data);

      })
  }

  addCalendar() {
    this.r.navigate(["/addcalendar"]);
  }
  editCalendar() {
    this.r.navigate(["/editcalendar/" + this.workingCalendarObj.id]);
  }

  menuList: any = [];
  public addButton: boolean;
  public addButtonText: any;
  public editButton: any;
  public editButtonText: any;

  getHeaderMenuList() {
    this.newService.getHeaderMenu()
      .subscribe(res => {
        this.menuList = res;
        this.menuList.forEach(element => {
          if (element.routerLink == "eta-web/organizationdashboard") {
            this.mainbreadcrumb = element.translation;
          }
          if (element.routerLink == "eta-web/calendardashboard") {
            this.breadcrumb = element.translation;
          }
          if (element.menuType == 5 && element.routerLink == "addcalendar") {
            this.addButton = true;
            this.addButtonText = element.translation;
          }
          if (element.menuType == 5 && element.routerLink == "editcalendar") {
            this.editButton = true;
            this.editButtonText = element.translation;
          }
        })

      })
  }

  shownRouterLink(routerlink) {
    this.r.navigate(["/" + routerlink]);
  }

  activeLink = "calendardashboard";
  background = '';

  toggleBackground() {
    this.background = this.background ? '' : 'primary';
  }
}
