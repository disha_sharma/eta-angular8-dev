import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PolicyDtTransComponent } from './policy-dt-trans.component';

describe('PolicyDtTransComponent', () => {
  let component: PolicyDtTransComponent;
  let fixture: ComponentFixture<PolicyDtTransComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PolicyDtTransComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PolicyDtTransComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
