import { HttpClient } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ServiceService } from './../../../../../eta-services/service.service';


class DataTablesResponse {
  data: any[];
  draw: number;
  recordsFiltered: number;
  recordsTotal: number;
  coloumnName: {};
}

@Component({
  selector: 'app-policy-dt-trans',
  templateUrl: './policy-dt-trans.component.html',
  styleUrls: ['./policy-dt-trans.component.css'],
  providers: [ServiceService],
})
export class PolicyDtTransComponent implements OnInit {
  @Input() childMessage: string;
  @Input() myurl: string;
  @Input() mycoloums = [];
  @Input() orgId :number;

  
  min: number;
  max: number;
 
  
  form;
  dataEdit: any = {};
  allTemplate: any = [];
  editTemp: any = {};
  emailLang: any = {};
  tdheader: any = {};
  emailLangTrans = [];
  editEmailTemplateshow: any = {};
  languageData:any;

  tempid = "";
  tempvid = "";
  langid = "";
  editEmailTemplate = "";
  editorgId = "";
  editMailEvent = "";
  editLangId = "";
  editSubjectValue = "";
  editMessageValue = "";


  dtOptions: DataTables.Settings = {};
  gefields: any[];
  public arrayOfKeys = [];
  public arrayOfValues = [];
  ETA_SELECTED_ORGID:any='0';

  constructor(private http: HttpClient, private newService: ServiceService) { }

  ngOnInit() {
    if(localStorage.getItem("EtaSelectOrgId")!=null){
      this.ETA_SELECTED_ORGID = localStorage.getItem("EtaSelectOrgId");
    }
    
    const that = this;

    this.form = new FormGroup({
      edittemplateId: new FormControl(""),
      editlanguageId: new FormControl(""),
      editSubject: new FormControl(""),
      editMessage: new FormControl(""),
    });


    this.newService.getPolicyHeaderLanguageByOrgId()
      .subscribe(data => {
        this.tdheader = data;
        this.tdheader.forEach(element => {
          this.languageData=element.spdlanguage.language;
        });
        
      });

      $.fn['dataTable'].ext.search.push((settings, data, dataIndex) => {
        const id = parseFloat(data[0]) || 0; // use data for the id column
        if ((isNaN(this.min) && isNaN(this.max)) ||
          (isNaN(this.min) && id <= this.max) ||
          (this.min <= id && isNaN(this.max)) ||
          (this.min <= id && id <= this.max)) {
          return true;
        }
        return false;
      });


    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 4,
      serverSide: true,
      processing: true,
      columns: this.mycoloums,
      ajax: (dataTablesParameters: any, callback) => {
        dataTablesParameters.orgId=this.ETA_SELECTED_ORGID;
        that.http
          .post<DataTablesResponse>(
            this.myurl, dataTablesParameters, {}
          ).subscribe(resp => {
            that.gefields = resp.data;
            // this.arrayOfKeys =Object.keys(resp.data[0]);
            this.arrayOfKeys = Object.keys(resp.coloumnName);
            this.arrayOfValues = Object.values(resp.coloumnName); 
            callback({
              recordsTotal: resp.recordsTotal,
              recordsFiltered: resp.recordsFiltered,
              data: []
            });
          });
      },

    };


  }
  htmlToPlaintext = function (Message) {
    return Message ? String(Message).replace(/<[^>]+>/gm, '') : '';
  }
  data: object;
  onSubmit = function (tempobj) {
    let userobj: Object = {};
    let tempdataobj: Object = {};
    tempdataobj = this.editEmailTemplate;

    this.form = new FormGroup({
      edittemplateId: new FormControl(""),
      editlanguageId: new FormControl(""),
      editSubject: new FormControl(""),
      editMessage: new FormControl(""),

    });

    userobj["policyTemplateId"] = this.tempid;
    userobj["policyTemplateVersionId"] = this.tempvid;
    userobj["languageId"] = this.langid;
    userobj["policyName"] = tempobj.editSubject;
    userobj["policyText"] = tempobj.editMessage;
    this.newService.postPolicyTranslate(userobj);   
    this.refresh();
  }
  refresh(): void {   
    window.location.reload();
  }

  editMessage = function (temp, lid) {    
    this.editEmailTemplate = temp;
    this.tempid = temp.policyTemplateId;
    this.tempvid = temp.policyTemplateVersionId;
    this.langid = lid.languageId;   
     this.newService.getPolicyLanguageFindByTempIdTempVIdLangId(this.tempid, this.tempvid, this.langid)
      .subscribe(data => {
        this.editSubjectValue = data.policyName;
        this.editMessageValue = data.policyText;       
        this.form = new FormGroup({
          editSubject: new FormControl(data.policyName),
          editMessage: new FormControl(data.policyText),
        });        
      });
    this.editLangId = lid.spdlanguage.language;
    this.editEmailTemplate = temp.policyNameEvent;
  }
}
