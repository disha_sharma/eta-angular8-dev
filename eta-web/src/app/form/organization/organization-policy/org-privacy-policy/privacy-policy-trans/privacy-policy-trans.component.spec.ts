import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrivacyPolicyTransComponent } from './privacy-policy-trans.component';

describe('PrivacyPolicyTransComponent', () => {
  let component: PrivacyPolicyTransComponent;
  let fixture: ComponentFixture<PrivacyPolicyTransComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrivacyPolicyTransComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrivacyPolicyTransComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
