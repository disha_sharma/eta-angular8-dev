import { Component, OnInit } from '@angular/core';
import { ServiceService } from './../../../../../eta-services/service.service';

@Component({
  selector: 'app-privacy-policy-trans',
  templateUrl: './privacy-policy-trans.component.html',
  styleUrls: ['./privacy-policy-trans.component.css']
})
export class PrivacyPolicyTransComponent implements OnInit {

  /***   For Sub Breadcrumb  **/
  public breadcrumb: String;
  /*** For Main Breadcrumb **/
  public mainbreadcrumb: string;
  /** Read Org privacy Policy data URl  */
  public policyUrl: any;
  /** Org Language List */
  public tdheader: any = {};
  /** Selected OrgID */
  public ETA_SELECTED_ORGID: any;

  constructor(private newService: ServiceService) {
    this.getHeaderMenuList();
  }

  public functioncol: any = [];
  private column: any;


  ngOnInit() {

    this.newService.getPolicyHeaderLanguageByOrgId()
      .subscribe(data => {
        this.tdheader = data;
        this.functioncol.push({ data: 'Template' });
        data.forEach(element => {
          element.languageId;
          this.column = { data: element.languageId }
          this.functioncol.push(this.column);
        });
        this.policyUrl = this.newService.Policy_URL + "/getPolicyTranslateGrid";
      });
  }
  menuList: any = [];
  getHeaderMenuList() {
    this.newService.getHeaderMenu()
      .subscribe(res => {
        this.menuList = res;
        this.menuList.forEach(element => {
          if (element.routerLink == "eta-web/organizationdashboard") {
            this.mainbreadcrumb = element.translation;
          }
          if (element.routerLink == "eta-web/orgpolicydashboard") {
            this.breadcrumb = element.translation;
          }
        })

      })
  }
}
