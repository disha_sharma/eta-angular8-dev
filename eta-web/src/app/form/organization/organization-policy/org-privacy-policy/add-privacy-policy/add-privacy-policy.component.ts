import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormControl, FormGroup } from '@angular/forms';
import { ServiceService } from './../../../../../eta-services/service.service';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-add-privacy-policy',
  templateUrl: './add-privacy-policy.component.html',
  styleUrls: ['./add-privacy-policy.component.css']
})
export class AddPrivacyPolicyComponent implements OnInit {

  /***   For Sub Breadcrumb  **/
  public breadcrumb: String;
  /*** For Main Breadcrumb **/
  public mainbreadcrumb: string;
  /** Read Org privacy Policy URL from service TS  */
  public POLICY_URL: any;
  /**  Org privacy Policy Data URL */
  public allPolicyurl: any;
  /**  Org privacy Policy Grid Column List  */
  public policyManagementCol: any;
  /** Form Group */
  public privacyForm: FormGroup;
  /** get Slected org Id */
  public ETA_SELECTED_ORGID: any;
  /** Org List */
  public orgList: any;
  /** Org name */
  public orgName: any;
  /** Org ID */
  public get_OrgId: any;
  /** Form Group */
  public orgfrm: FormGroup;
  /** Get Selected CheckedBox Value From Grid */
  public getcheckedTempid: any;
  /** Get Selected CheckedBox Value From Grid */
  public getcheckedTempVid: any;
  /** get OrgName */
  public get_orgName: any;
  /** Edit Case Policy Template ID  */
  public editPolicyId: any = "";
  /** Edit Case Policy Template Version ID  */
  public editPolicyVId: any = "";

  public privacyPolicyInnerDataUrl: any;
  public defaultColumnToSort: any;


  editorConfig = {
    editable: true,
    spellcheck: true,
    height: '10rem',
    minHeight: '5rem',
    placeholder: 'Type something... (^_^)',
    translate: 'no',
    enableToolbar: true,
    showToolbar: true,
    imageEndPoint: "/asset",

  };

  toolbarConfig = {

  }


  constructor(private newService: ServiceService, private _formBuilder: FormBuilder, private location: Location, private route: ActivatedRoute) {
    this.getHeaderMenuList();
    this.defaultColumnToSort = "policyTemplateId";
    this.POLICY_URL = newService.Policy_URL;
    this.allPolicyurl = this.POLICY_URL + '/getAllPolicyGrid';
    this.privacyPolicyInnerDataUrl = this.POLICY_URL + '/getPrivacyPolicyAngularInnnerGrid';
    //this.ETA_SELECTED_ORGID = localStorage.getItem("EtaSelectOrgId");
    this.getAllOrgList()
    this.route.params.subscribe(params => {
      this.getcheckedTempid = +params['tempid'];
      this.getcheckedTempVid = +params['tempvid']
      this.editPrivacyPolicy(this.getcheckedTempid, this.getcheckedTempVid)
    })
  }




  ngOnInit() {
    this.getPrivacyPolicyColoumnList();

    this.privacyForm = this._formBuilder.group({
      orgname: [this.ETA_SELECTED_ORGID],
      policyname: new FormControl('', [Validators.required]),
      //policynameevent:[],
      Message: []

    })

    this.newService.getPrivacyPolicyAngularInnerGridData()
      .subscribe(data => {
        // console.log(data);

      })

  }


  onSubmit = function (policyObj) {
    let userObj: any = {};

    userObj['policyTemplateId'] = this.editPolicyId;
    userObj['policyTemplateVersionId'] = this.editPolicyVId;
    //  userObj['policyNameEvent'] = policyObj.policynameevent;
    userObj['policyName'] = policyObj.policyname;
    userObj['policyText'] = policyObj.Message;
    userObj['orgId'] = this.get_OrgId;
    //this.policyManagementCol = null;

    if (this.privacyForm.valid) {
      this.newService.postPolicyTemplate(userObj).subscribe(data => {
        if (data.policyTemplateId != null) {
          alert("Data added !");
          this.policyManagementCol = null;
          this.getPrivacyPolicyColoumnList();
        } else {
          alert("Data not added !");
        }
      })
    }else{
      alert("Enter Data")
    }

  }



  private getAllOrgList() {
    this.newService.getAllOrgDetails()
      .subscribe(data => {
        this.orgList = data;
        this.getOrgId(this.orgList);
      })
  }

  public getOrgId(orgList) {
    orgList.forEach(element => {
      this.newService.getLoginInfo()
      .subscribe(data =>{
        this.ETA_SELECTED_ORGID = data.orgId;
        console.log(this.ETA_SELECTED_ORGID)
        if (this.ETA_SELECTED_ORGID == element.orgId) {
          this.orgName = element.orgName;
        }
      })
    });
  }
 

  menuList: any = [];
  getHeaderMenuList() {
    this.newService.getHeaderMenu()
      .subscribe(res => {
        this.menuList = res;
        this.menuList.forEach(element => {
          if (element.routerLink == "eta-web/organizationdashboard") {
            this.mainbreadcrumb = element.translation;
          }
          if (element.routerLink == "eta-web/orgpolicydashboard") {
            this.breadcrumb = element.translation;
          }
        })

      })
  }

  private getPrivacyPolicyColoumnList() {
    this.newService.getOrgPrivacyPolicyInnerColumn()
      .subscribe(res => {

        this.policyManagementCol = res.coloumnName;
      })
  }

  cancel() {
    localStorage.setItem('backlocation', "orgpolicydashboard");
    this.location.back();
  }

  editPrivacyPolicy(tmp, tempid) {
    this.newService.getEditPrivacyPolcy(tmp, tempid)
      .subscribe(data => {
        console.log(data);

        this.get_OrgId = data.orgId;
        this.get_orgName = data.orgName;
        this.editPolicyId = data.policyTemplateId;
        this.editPolicyVId = data.policyTemplateVersionId,
          this.privacyForm = this._formBuilder.group({
            orgname: [data.orgName],
            Message: [data.policyText],
            policyname: [data.policyName],
            //policynameevent: [data.policyNameEvent]

          })

      })
  }
}
