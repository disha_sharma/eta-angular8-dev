import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ServiceService } from './../../../../../eta-services/service.service';


@Component({
  selector: 'app-privacy-policy',
  templateUrl: './privacy-policy.component.html',
  styleUrls: ['./privacy-policy.component.css'],
  providers: [ServiceService]
})
export class PrivacyPolicyComponent implements OnInit {

  /***   For Sub Breadcrumb  **/
  public breadcrumb: String;
  /*** For Main Breadcrumb **/
  public mainbreadcrumb: string;
  /** Read Org privacy Policy URL from service TS  */
  public policy_url: any;
  /** Org privacy Policy Data URL  */
  public allPolicyurl: any;
  /** Org privacy Policy Grid Column List  */
  public policyManagementCol: any;
  /** Form Group */
  public privacyForm: FormGroup;
  /** Get Selected CheckedBox Value From Grid */
  public getcheckedTempid: any;
  /** Get Selected CheckedBox Value From Grid */
  public getcheckedTempVid: any
  /** Edit Case Policy Template ID  */
  public editPolicyId: any = "";
  /** Edit Case Policy Template Version ID  */
  public editPolicyVId: any = "";
  /** Org ID */
  public get_OrgId: any;
  /** DraftYN */
  public get_DraftYN: any;
  /** ActiveYN */
  public get_ActiveYN: any;
  /**Org List  */
  public newData: any;
  /** get OrgName */
  public get_orgName:any;

  
  constructor(private newService: ServiceService, private _formBuilder: FormBuilder, private route: ActivatedRoute, private location: Location) {
    this.getHeaderMenuList();
    this.policy_url = newService.Policy_URL;
    this.allPolicyurl = this.policy_url + '/getAllPolicyGrid';
    this.route.params.subscribe(params => {
      this.getcheckedTempid = +params['tempid'];
      this.getcheckedTempVid = +params['tempvid']
      this.editPrivacyPolicy(this.getcheckedTempid, this.getcheckedTempVid)
    })
    this.getAllOrgList();

  }

  ngOnInit() {
    this.getEditPrivacyPolicyColoumnList();
    this.privacyForm = this._formBuilder.group({
      orgname: new FormControl('', []),
      privacypolicy: new FormControl('', []),
      Message: new FormControl('', []),
      policyname: new FormControl('', []),
      draftYN: new FormControl('', []),
      activeYN: new FormControl('', []),
      orgId: new FormControl('', [])
    })

  }


  onSubmit = function (newPolicyObj) {
    let policyObj: any = {};
    policyObj['policyTemplateId'] = this.editPolicyId;
    policyObj['policyTemplateVersionId'] = this.editPolicyVId;
    policyObj['orgId'] = this.get_OrgId;
    policyObj['draftYN'] = this.get_DraftYN;
    policyObj['activeYN'] = this.get_ActiveYN;
    policyObj['policyNameEvent'] = newPolicyObj.privacypolicy;
    policyObj['policyName'] = newPolicyObj.privacypolicy;
    this.policyManagementCol = null;

    this.newService.editPolicy(policyObj).subscribe(data => {
      if (data.policyTemplateId > 0) {
        alert("Data edited !");
        this.getEditPrivacyPolicyColoumnList();
      }
    })



  }

  editPrivacyPolicy(tmp, tempid) {
    this.newService.getEditPrivacyPolcy(tmp, tempid)
      .subscribe(data => {
        this.get_OrgId = data.orgId
        this.get_orgName=data.orgName;
        this.editPolicyId = data.policyTemplateId;
        this.editPolicyVId = data.policyTemplateVersionId,
          this.get_ActiveYN = data.activeYN,
          this.get_DraftYN = data.draftYN,
          this.privacyForm = this._formBuilder.group({
            orgname: [data.orgName],
            privacypolicy: [data.policyNameEvent],
            draftYN: [data.draftYN],
            activeYN: [data.activeYN]

          })

      })
  }
  private getAllOrgList() {
    this.newService.getOrgDetailsByOrgId(this.get_OrgId)
      .subscribe(data => {
        this.newData = data;
      })
  }
  menuList: any = [];
  getHeaderMenuList() {
    this.newService.getHeaderMenu()
      .subscribe(res => {

        this.menuList = res;
        this.menuList.forEach(element => {
          if (element.routerLink == "eta-web/organizationdashboard") {
            this.mainbreadcrumb = element.translation;
          }
          if (element.routerLink == "eta-web/orgpolicydashboard") {
            this.breadcrumb = element.translation;
          }
        })

      })
  }

  private getEditPrivacyPolicyColoumnList() {
    this.newService.getOrgPrivacyPolicyInnerColumn()
      .subscribe(res => {
        this.policyManagementCol = res.coloumnName;
      })
  }

  cancel() {
    localStorage.setItem('backlocation', "orgpolicydashboard");
    this.location.back();
  }

}
