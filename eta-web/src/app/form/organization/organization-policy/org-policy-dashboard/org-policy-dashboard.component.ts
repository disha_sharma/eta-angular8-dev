import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServiceService } from './../../../../eta-services/service.service';
@Component({
  selector: 'app-org-policy-dashboard',
  templateUrl: './org-policy-dashboard.component.html',
  styleUrls: ['./org-policy-dashboard.component.css']
})
export class OrgPolicyDashboardComponent implements OnInit {

  /***   For Sub Breadcrumb  **/
  public breadcrumb: String;
  /*** For Main Breadcrumb **/
  public mainbreadcrumb: string;
  /** Read Org privacy Policy URL from service TS  */
  public POLICY_URL: any;
  /**  Org privacy Policy Data URL */
  public privacyPolicyManagementUrl: any;
  /**  Org privacy Policy  Grid Column */
  public policyManagementCol: any;
  /** Select  Org privacy Policy  Value form Grid */
  public privacyPolicyObj: any;

  public privacyPolicyDataUrl: any;

  public defaultColumnToSort: any;
  public isEditAllowed: boolean = false;

  constructor(private r: Router, private newService: ServiceService) {
    this.getHeaderMenuList();
    this.defaultColumnToSort = "policyTemplateId"
    this.POLICY_URL = this.newService.Policy_URL;
    this.privacyPolicyManagementUrl = this.POLICY_URL + "/getAllPolicyGrid"
    this.privacyPolicyDataUrl = this.POLICY_URL + "/getPrivacyPolicyAngularGrid"
  }

  ngOnInit() {
   // this.newService.currentMessage.subscribe(message => this.privacyPolicyObj = message);
    this.newService.refreshEditMessage();
    this.newService.currentMessage.subscribe(message => {
      this.privacyPolicyObj = message
      if (message != 'editFalse') {
        this.isEditAllowed = true;
      }
    });
    this.newService.getOrgPrivacyPolicyColumn()
      .subscribe(res => {
        this.policyManagementCol = res.coloumnName;
      })

    this.newService.getPrivacyPolicyAngularGridData()
      .subscribe(data => {
        // console.log(data);

      })
  }


  addPrivacyPolicy() {
    this.r.navigate(["/addprivacypolicy"]);
  }
  editPrivacyPolicy() {
    this.r.navigate(["/editprivacypolicy/" + this.privacyPolicyObj.policyTemplateId + "/" + this.privacyPolicyObj.policyTemplateVersionId]);
  }

  addPolicyTran() {
    this.r.navigate(["/privacypolicytranslation"]);
  }

  menuList: any = [];
  public addButton: boolean;
  public addButtonText: any;
  public editButton: any;
  public editButtonText: any;
  public addTranslationButton: boolean;
  public addTranslationButtonText: any;

  getHeaderMenuList() {
    this.newService.getHeaderMenu()
      .subscribe(res => {
        this.menuList = res;

        this.menuList.forEach(element => {
          if (element.routerLink == "eta-web/organizationdashboard") {
            this.mainbreadcrumb = element.translation;
          }
          if (element.routerLink == "eta-web/orgpolicydashboard") {
            this.breadcrumb = element.translation;
          }
          if (element.menuType == 5 && element.routerLink == "addprivacypolicy") {
            this.addButton = true;
            this.addButtonText = element.translation;
          }
          if (element.menuType == 5 && element.routerLink == "editprivacypolicy") {
            this.editButton = true;
            this.editButtonText = element.translation;
          }
          if (element.menuType == 5 && element.routerLink == "privacypolicytranslation") {
            this.addTranslationButton = true;
            this.addTranslationButtonText = element.translation;
          }
        })

      })
  }

  shownRouterLink(routerlink) {
    this.r.navigate(["/" + routerlink]);
  }
  activeLink = "orgpolicydashboard";
  background = '';

  toggleBackground() {
    this.background = this.background ? '' : 'primary';
  }

}
