import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrgPolicyDashboardComponent } from './org-policy-dashboard.component';

describe('OrgPolicyDashboardComponent', () => {
  let component: OrgPolicyDashboardComponent;
  let fixture: ComponentFixture<OrgPolicyDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrgPolicyDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrgPolicyDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
