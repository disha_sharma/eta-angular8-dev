import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrgPasswordDashboardComponent } from './org-password-dashboard.component';

describe('OrgPasswordDashboardComponent', () => {
  let component: OrgPasswordDashboardComponent;
  let fixture: ComponentFixture<OrgPasswordDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrgPasswordDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrgPasswordDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
