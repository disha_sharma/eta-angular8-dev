import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServiceService } from './../../../../eta-services/service.service';

@Component({
  selector: 'app-org-password-dashboard',
  templateUrl: './org-password-dashboard.component.html',
  styleUrls: ['./org-password-dashboard.component.css']
})
export class OrgPasswordDashboardComponent implements OnInit {

  /***   For Sub Breadcrumb  **/
  public breadcrumb: String;
  /*** For Main Breadcrumb **/
  public mainbreadcrumb: string;
  /** Read Org Password Policy URL Form  service TS  */
  public ORG_PASS_URL: any;
  /** Org Password Policy Data Url  */
  public organizationPassUrl: any;
  /** Org Password Policy Grid Column */
  public organizationPassUrlCol: any;
  /**  set Select  Org Password Policy Value */
  public passwordPolicyObj: any;

  public orgPasswordPolicyDataUrl: any;

  public defaultColumnToSort: any;
  public isEditAllowed: boolean = false;

  constructor(private r: Router, private newService: ServiceService) {
    this.getHeaderMenuList();
    this.defaultColumnToSort = "passConfigId"
    this.ORG_PASS_URL = this.newService.Org_Pass_URL;
    this.organizationPassUrl = this.ORG_PASS_URL + '/getAllOrgPassPolicyGridData';
    this.orgPasswordPolicyDataUrl = this.ORG_PASS_URL + '/getOrgPasswordPolicyAngularGrid';
  }


  ngOnInit() {
    //this.newService.currentMessage.subscribe(message => this.passwordPolicyObj = message);
    this.newService.refreshEditMessage();
    this.newService.currentMessage.subscribe(message => {
      this.passwordPolicyObj = message
      if (message != 'editFalse') {
        this.isEditAllowed = true;
      }
    });
    this.newService.getOrgPassPolicyColumn()
      .subscribe(res => {
        this.organizationPassUrlCol = res.coloumnName;
      })

    this.newService.getOrgPasswordPolicyAngularGridData()
      .subscribe(data => {
        // console.log(data);

      })
  }


  addPasswordPolicy() {
    this.r.navigate(["/addpasswordpolicy"]);
  }
  editPasswordPolicy() {
    this.r.navigate(["/editpasswordpolicy/" + this.passwordPolicyObj.id]);
  }


  menuList: any = [];
  public addButton: boolean;
  public addButtonText: any;
  public editButton: any;
  public editButtonText: any;

  getHeaderMenuList() {
    this.newService.getHeaderMenu()
      .subscribe(res => {
        this.menuList = res;

        this.menuList.forEach(element => {
          if (element.routerLink == "eta-web/organizationdashboard") {
            this.mainbreadcrumb = element.translation;
          }
          if (element.routerLink == "eta-web/orgpassworddashboard") {
            this.breadcrumb = element.translation;
          }
          if (element.menuType == 5 && element.routerLink == "eta-web/addpasswordpolicy") {
            this.addButton = true;
            this.addButtonText = element.translation;
          }
          if (element.menuType == 5 && element.routerLink == "eta-web/editpasswordpolicy") {
            this.editButton = true;
            this.editButtonText = element.translation;
          }
        })

      })
  }

  shownRouterLink(routerlink) {
    this.r.navigate(["/" + routerlink]);
  }

  activeLink = "orgpassworddashboard";
  background = '';

  toggleBackground() {
    this.background = this.background ? '' : 'primary';
  }

}
