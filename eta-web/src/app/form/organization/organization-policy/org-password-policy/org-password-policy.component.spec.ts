import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrgPasswordPolicyComponent } from './org-password-policy.component';

describe('OrgPasswordPolicyComponent', () => {
  let component: OrgPasswordPolicyComponent;
  let fixture: ComponentFixture<OrgPasswordPolicyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrgPasswordPolicyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrgPasswordPolicyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
