import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ServiceService } from './../../../../eta-services/service.service';




@Component({
  selector: 'app-org-password-policy',
  templateUrl: './org-password-policy.component.html',
  styleUrls: ['./org-password-policy.component.css'],
  providers: [ServiceService]
})
export class OrgPasswordPolicyComponent implements OnInit {

  /***   For Sub Breadcrumb  **/
  public breadcrumb: String;
  /*** For Main Breadcrumb **/
  public mainbreadcrumb: string;
  /** Read Org Password Policy URL Form  service TS  */
  public ORG_PASS_URL: any;
  /** Org Password Policy Data Url  */
  public organizationPassUrl: any;
  /** Org Password Policy Grid column List  */
  public organizationPassUrlCol: any;
  /**  FormGroup */
  public orgfrm: FormGroup;
  /** Days id on change */
  public daysIdOnChange;
  /**Days List  */
  public dayslist: any = {};
  /** Org List Array */
  public orglist: any = [];
  /** Get Select Org Password Policy Id */
  public passCheckedId: any;
  /** Selected Org Id */
  public ETA_SELECTED_ORGID: any;
  /** Org List */
  public orgList: any;
  /** Org Is */
  public get_OrgId: any;
  /** Org Name */
  public orgName: any;
  /**User List  */
  public userList: any = [];
  /** UserTypeId on change */
  public userIdOnChange;
  /*** Policy id  */
  public passwordId: any;
  public passCheckedUserType: any;

  favoriteSeason: string;

  public orgPasswordPolicyInnerDataUrl: any;
  public defaultColumnToSort: any;


  constructor(private _formBuilder: FormBuilder, private newService: ServiceService, private route: ActivatedRoute, private location: Location) {
    this.getHeaderMenuList();
    this.defaultColumnToSort = "passConfigId";
    this.getUserType();
    this.ORG_PASS_URL = newService.Org_Pass_URL;
    this.organizationPassUrl = this.ORG_PASS_URL + '/getAllOrgPassPolicyGridData';
    this.orgPasswordPolicyInnerDataUrl = this.ORG_PASS_URL + '/getOrgPasswordPolicyAngularInnnerGrid';
    this.route.params.subscribe(params => {
      this.passCheckedUserType = +params['id'];
      this.getOrgPassById(this.passCheckedUserType)
    });
    this.getAllExpiryDays();
    //this.ETA_SELECTED_ORGID = localStorage.getItem("EtaSelectOrgId");
    this.getAllOrgList();
    this.getLoginInfo();
  }


  selectPassExpiryDays(change) {
    this.daysIdOnChange = change;
  }

  passUserTypelist: any = {};
  selectPassUserType(change) {
    this.userIdOnChange = change;
  }


  ngOnInit() {
    this.getPasswordPolicyColoumnList();

    this.orgfrm = this._formBuilder.group({
      policyid: new FormControl('', []),
      id: new FormControl('', []),
      orgname: [this.ETA_SELECTED_ORGID],
      minLength: new FormControl('', [Validators.required, Validators.pattern('[0-9]')]),
      number: new FormControl('', [Validators.required, Validators.pattern('[0-9]')]),
      uppercaseChar: new FormControl('', [Validators.required, Validators.pattern('[0-9]')]),
      specialChar: new FormControl('', [Validators.required, Validators.pattern('[0-9]')]),
      userId: new FormControl('', [Validators.required])
    })
    this.newService.getAllOrgDetails()
      .subscribe(data => this.orglist = data);

    this.newService.getOrgPasswordPolicyAngularInnerGridData()
      .subscribe(data => {
        // console.log(data);

      })
  }

  private getAllExpiryDays() {
    this.newService.getAllExpiryDays()
      .subscribe(data => {
        this.dayslist = data;
      });
  }

  public getUserType() {
    this.newService.getUserType()
      .subscribe(data => {
        this.userList = data;
      });
  }


  onSubmit = function (passKey) {
    let orgfrm: Object = {};

    orgfrm["id"] = this.passwordId;
    orgfrm["orgId"] = this.get_OrgId;
    orgfrm["passConfigId"] = passKey.policyid;
    orgfrm["description"] = "Password policy discription";
    orgfrm["minLength"] = passKey.minLength;
    orgfrm["number"] = passKey.number;
    orgfrm["uppercaseChar"] = passKey.uppercaseChar;
    orgfrm["specialChar"] = passKey.specialChar;
    orgfrm["userType"] = passKey.userId;
    //alert(passKey.userId);
    //this.organizationPassUrlCol = null;

    //this.getPasswordPolicyColoumnList();
    if (this.orgfrm.valid) {
      this.newService.setOrgPolicy(orgfrm)
        .subscribe(data => { 
          let retrunData = data;
          if (retrunData.passConfigId != null) {
            if(data.id==0){
              alert("Duplicate Data Not Allowed For Same user Type");
            }
            if(data.id !=0){
              alert("Policy data added !");
            }
           
            this.organizationPassUrlCol = null;
            this.getPasswordPolicyColoumnList();
          } else {
            alert("Policy data not added !");
          }

        })
    } else {
      alert("Enter Data")
    }

  }

  getOrgPassById(passKey) {
    this.newService.getOrgPasswordById(passKey)
      .subscribe(data => {
        console.log(data);
        console.log(data.orgId);
        

        //alert(data.passConfigId);
        this.passwordId = data.id
        this.get_OrgId = data.orgId;
        this.orgfrm = this._formBuilder.group({
          id: [data.id],
          orgname: [data.orgId],
          policyid: [data.passConfigId],
          minLength: [data.minLength],
          number: [data.number],
          uppercaseChar: [data.uppercaseChar],
          specialChar: [data.specialChar],
          userId: [data.userType]
        })

      });
  }
  private getAllOrgList() {
    this.newService.getAllOrgDetails()
      .subscribe(data => {
        this.orgList = data;
        this.getOrgId(this.orgList);
      })
  }

  public getOrgId(orgList) {
    orgList.forEach(element => {
    this.newService.getLoginInfo()    
      .subscribe(data => {
        this.ETA_SELECTED_ORGID = data.orgId;
        if (element.orgId == this.ETA_SELECTED_ORGID) {
        this.orgName = element.orgName;        
        }
      })  
    });
  }
 

  menuList: any = [];
  getHeaderMenuList() {
    this.newService.getHeaderMenu()
      .subscribe(res => {
        this.menuList = res;
        this.menuList.forEach(element => {
          if (element.routerLink == "eta-web/organizationdashboard") {
            this.mainbreadcrumb = element.translation;
          }
          if (element.routerLink == "eta-web/orgpassworddashboard") {
            this.breadcrumb = element.translation;
          }
        })

      })
  }

  private getPasswordPolicyColoumnList() {
    this.newService.getOrgPassPolicyInnerColumn()
      .subscribe(res => {
        this.organizationPassUrlCol = res.coloumnName;
      })
  }

  cancel() {
    this.location.back();
  }

  public getLoginInfo(){
    this.newService.getLoginInfo()
    .subscribe(data => {
      this.ETA_SELECTED_ORGID = data.orgId;
    })
  }

}