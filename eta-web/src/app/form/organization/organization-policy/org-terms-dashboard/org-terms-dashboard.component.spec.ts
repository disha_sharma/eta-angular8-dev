import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrgTermsDashboardComponent } from './org-terms-dashboard.component';

describe('OrgTermsDashboardComponent', () => {
  let component: OrgTermsDashboardComponent;
  let fixture: ComponentFixture<OrgTermsDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrgTermsDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrgTermsDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
