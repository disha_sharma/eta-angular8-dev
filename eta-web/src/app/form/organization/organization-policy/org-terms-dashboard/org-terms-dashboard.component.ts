import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServiceService } from './../../../../eta-services/service.service';


@Component({
  selector: 'app-org-terms-dashboard',
  templateUrl: './org-terms-dashboard.component.html',
  styleUrls: ['./org-terms-dashboard.component.css']
})
export class OrgTermsDashboardComponent implements OnInit {

  /***   For Sub Breadcrumb  **/
  public breadcrumb: String;
  /*** For Main Breadcrumb **/
  public mainbreadcrumb: string;
  /** Read Org TNC URL from service TS  */
  public TNC_URL: any;
  /** Org TNC Data URL */
  public TermsManagementUrl: any;
  /** Org TNC Grid Column List  */
  public termsManagementCol: any;
  /** Selected Checkbox value From Grid CheckedBox  */
  public tncObj: any;

  public termsConditionDataUrl: any;

  public defaultColumnToSort: any;

  public isEditAllowed: boolean = false;
  constructor(private r: Router, private newService: ServiceService) {
    this.getHeaderMenuList();
    this.defaultColumnToSort = "termsTemplateId"
    this.TNC_URL = this.newService.Policy_URL;
    this.TermsManagementUrl = this.TNC_URL + '/getAllTermsGrid';
    this.termsConditionDataUrl = this.TNC_URL + '/getTermsConditionAngularGrid';
  }

  ngOnInit() {
    //this.newService.currentMessage.subscribe(message => this.tncObj = message);
    this.newService.refreshEditMessage();
    this.newService.currentMessage.subscribe(message => {
      this.tncObj = message
      if (message != 'editFalse') {
        this.isEditAllowed = true;
      }
    });
    this.newService.getOrgPrivacyTermsColumn()
      .subscribe(res => {
        this.termsManagementCol = res.coloumnName;
      })

    this.newService.getTermsConditionAngularGridData()
      .subscribe(data => {
        // console.log(data);

      })
  }


  addTerms() {
    this.r.navigate(["/addtermscondition"]);
  }
  editTerms() {
    this.r.navigate(["/edittermscondition/" + this.tncObj.termsTemplateId + "/" + this.tncObj.termsTemplateVersionId]);
  }

  addTermsTrans() {
    this.r.navigate(["/tnctranslation"]);
  }

  menuList: any = [];
  public addButton: boolean;
  public addButtonText: any;
  public editButton: any;
  public editButtonText: any;
  public addTranslationButton: boolean;
  public addTranslationButtonText: any;

  getHeaderMenuList() {
    this.newService.getHeaderMenu()
      .subscribe(res => {
        this.menuList = res;
        this.menuList.forEach(element => {
          if (element.routerLink == "eta-web/organizationdashboard") {
            this.mainbreadcrumb = element.translation;
          }
          if (element.routerLink == "eta-web/orgtermsdashboard") {
            this.breadcrumb = element.translation;
          }
          if (element.menuType == 5 && element.routerLink == "eta-web/addtermscondition") {
            this.addButton = true;
            this.addButtonText = element.translation;
          }
          if (element.menuType == 5 && element.routerLink == "eta-web/edittermscondition") {
            this.editButton = true;
            this.editButtonText = element.translation;
          }
          if (element.menuType == 5 && element.routerLink == "eta-web/tnctranslation") {
            this.addTranslationButton = true;
            this.addTranslationButtonText = element.translation;
          }
        })

      })
  }

  shownRouterLink(routerlink) {
    this.r.navigate(["/" + routerlink]);
  }

  activeLink = "orgtermsdashboard";
  background = '';

  toggleBackground() {
    this.background = this.background ? '' : 'primary';
  }

}
