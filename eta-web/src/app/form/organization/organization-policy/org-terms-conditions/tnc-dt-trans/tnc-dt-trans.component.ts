import { HttpClient } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ServiceService } from './../../../../../eta-services/service.service';


class DataTablesResponse {
  data: any[];
  draw: number;
  recordsFiltered: number;
  recordsTotal: number;
  coloumnName: {};
}

@Component({
  selector: 'app-tnc-dt-trans',
  templateUrl: './tnc-dt-trans.component.html',
  styleUrls: ['./tnc-dt-trans.component.css'],
  providers: [ServiceService]
})
export class TermsConditionDatatableTranslateComponent implements OnInit {
  @Input() childMessage: string;
  @Input() myurl: string;
  @Input() mycoloums = [];
  @Input() orgId :number;

 
  min: number;
  max: number;
 
  
  form;
  dataEdit: any = {};
  allTemplate: any = [];
  editTemp: any = {};
  emailLang: any = {};
  tdheader: any = {};
  emailLangTrans = [];
  editEmailTemplateshow: any = {};



  tempid = "";
  tempvid = "";
  langid = "";


  editEmailTemplate = "";
  editorgId = "";
  editMailEvent = "";
  editLangId = "";
  editSubjectValue = "";
  editMessageValue = "";
  languageData:any;

  dtOptions: DataTables.Settings = {};
  gefields: any[];
  public arrayOfKeys = [];
  public arrayOfValues = [];
  ETA_SELECTED_ORGID:any=0;

  constructor(private http: HttpClient, private newService: ServiceService) { }

  ngOnInit(): void {
    if(localStorage.getItem("EtaSelectOrgId")!=null){
     this.ETA_SELECTED_ORGID = localStorage.getItem("EtaSelectOrgId");
    }
   
    const that = this;

    this.form = new FormGroup({
      edittemplateId: new FormControl(""),
      editlanguageId: new FormControl(""),
      editSubject: new FormControl(""),
      editMessage: new FormControl(""),
    });


    this.newService.getPolicyHeaderLanguageByOrgId()
      .subscribe(data => {
        this.tdheader = data;
        this.tdheader.forEach(element => {
          this.languageData=element.spdlanguage.language;         
        });
      });

      $.fn['dataTable'].ext.search.push((settings, data, dataIndex) => {
        const id = parseFloat(data[0]) || 0; // use data for the id column
        if ((isNaN(this.min) && isNaN(this.max)) ||
          (isNaN(this.min) && id <= this.max) ||
          (this.min <= id && isNaN(this.max)) ||
          (this.min <= id && id <= this.max)) {
          return true;
        }
        return false;
      });


    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 4,
      serverSide: true,
      processing: true,
      columns: this.mycoloums,   
      ajax: (dataTablesParameters: any, callback) => {
        dataTablesParameters.orgId=this.ETA_SELECTED_ORGID;
        that.http
          .post<DataTablesResponse>(

            this.myurl, dataTablesParameters, {}
          ).subscribe(resp => {
            that.gefields = resp.data;         
            this.arrayOfKeys = Object.keys(resp.coloumnName);  
            this.arrayOfValues = Object.values(resp.coloumnName);  
            callback({
              recordsTotal: resp.recordsTotal,
              recordsFiltered: resp.recordsFiltered,
              data: []
            });
          });
      },

    };


  }
  htmlToPlaintext = function (Message) {
    return Message ? String(Message).replace(/<[^>]+>/gm, '') : '';
  }
  data: object;

  onSubmit = function (tempobj) {
    let userobj: Object = {};
    let tempdataobj: Object = {};
    tempdataobj = this.editEmailTemplate;

    this.form = new FormGroup({
      edittemplateId: new FormControl(""),
      editlanguageId: new FormControl(""),
      editSubject: new FormControl(""),
      editMessage: new FormControl(""),

    });

    userobj["termsTemplateId"] = this.tempid;
    userobj["termsTemplateVersionId"] = this.tempvid;
    userobj["languageId"] = this.langid;
    userobj["termsName"] =  this.editSubjectValue;
    userobj["termsText"] = tempobj.editMessage;
    this.newService.postTermsTranslate(userobj);
    this.refresh();
  }
  refresh(): void {   
    window.location.reload();
  }

  editMessage = function (temp, lid) {   
    
    this.editEmailTemplate = temp;
    this.tempid = temp.termsTemplateId;
    this.tempvid = temp.termsTemplateVersionId;
    this.langid = lid.languageId;   
     this.newService.getTermsLanguageFindByTempIdTempVIdLangId(this.tempid, this.tempvid, this.langid)
      .subscribe(data => {
        this.editSubjectValue = data.termsName;
        this.editMessageValue = data.termsText;
        this.form = new FormGroup({
          editSubject: new FormControl(data.termsName),
          editMessage: new FormControl(data.termsText),
        });
        
      });
    this.editLangId = lid.spdlanguage.language;
    this.editEmailTemplate = temp.termsNameEvent;

  }

}
