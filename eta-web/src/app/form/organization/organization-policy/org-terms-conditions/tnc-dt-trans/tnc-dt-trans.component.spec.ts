import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TermsConditionDatatableTranslateComponent } from './tnc-dt-trans.component';

describe('TermsConditionDatatableTranslateComponent', () => {
  let component: TermsConditionDatatableTranslateComponent;
  let fixture: ComponentFixture<TermsConditionDatatableTranslateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TermsConditionDatatableTranslateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TermsConditionDatatableTranslateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
