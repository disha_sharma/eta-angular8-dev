import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormControl, FormGroup } from '@angular/forms';
import { ServiceService } from './../../../../../eta-services/service.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-add-tnc',
  templateUrl: './add-tnc.component.html',
  styleUrls: ['./add-tnc.component.css']
})
export class AddTncComponent implements OnInit {

  /***   For Sub Breadcrumb  **/
  public breadcrumb: String;
  /*** For Main Breadcrumb **/
  public mainbreadcrumb: string;
  /** Read Org TNC URL from service TS  */
  public TNC_url: any;
  /** Org TNC Data URL */
  public allTermsUrl: any;
  /** Org TNC Grid Column List */
  public termsManagementCol: any;
  /** Form Group */
  public termsForm: FormGroup;
  /** Get Selected Org Id */
  public ETA_SELECTED_ORGID: any;
  /** Org List */
  public orgList: any;
  /** Org Name */
  public orgName: any;
  /** Get Selected CheckedBox Value From Grid */
  public getcheckedTempid: any;
  /** Get Selected CheckedBox Value From Grid */
  public getcheckedTempVid: any;
  /** Org ID */
  public get_OrgId: any;
  /** Org name */
  public get_orgName: any;
  /** Edit Case Policy Template ID  */
  public editTempid: any = "";
  /** Edit Case Policy Template Version ID  */
  public editTempVid: any = "";

  public termsConditionInnerDataUrl: any;
  public defaultColumnToSort: any;

  public editorConfig = {
    editable: true,
    spellcheck: true,
    height: '10rem',
    minHeight: '5rem',
    placeholder: 'Type something... (^_^)',
    translate: 'no',
    enableToolbar: true,
    showToolbar: true,
    imageEndPoint: "/asset",


  };


  constructor(private newService: ServiceService, private _formBuilder: FormBuilder, private location: Location, private route: ActivatedRoute) {
    this.getHeaderMenuList();
    this.defaultColumnToSort = "termsTemplateId";
    this.TNC_url = this.newService.Policy_URL;
    this.allTermsUrl = this.TNC_url + '/getAllTermsGrid';
    this.termsConditionInnerDataUrl = this.TNC_url + '/getTermsConditionAngularInnnerGrid';
    //this.ETA_SELECTED_ORGID = localStorage.getItem("EtaSelectOrgId");
    this.getAllOrgList();
    this.route.params.subscribe(params => {
      this.getcheckedTempid = +params['tempid'];
      this.getcheckedTempVid = +params['tempvid']
      this.editTnc(this.getcheckedTempid, this.getcheckedTempVid);

    })
  }

  ngOnInit() {
    this.getTermsConditionColoumnList();
    this.termsForm = this._formBuilder.group({
      orgname: [],
      termsevent: [],
      termsname: new FormControl('', [Validators.required]),
      termstext: []
    })

    this.newService.getTermsConditionAngularInnnerGridData()
      .subscribe(data => {
        // console.log(data);

      })

  }


  onSubmit = function (termsObj) {
    /* this.termsForm = this._formBuilder.group({
      orgname: [],
      termsevent: [],
      termstext: [],
      termsname: [],
    }) */

    let userObj: any = {};
    userObj['termsTemplateId'] = this.editTempid;
    userObj['termsTemplateVersionId'] = this.editTempVid;
    //userObj['activeYN'] = 1;
    userObj['termsNameEvent'] = termsObj.termsname;
    userObj['termsName'] = termsObj.termsname;
    userObj['termsText'] = termsObj.termstext;
    //this.termsManagementCol = null;
    /* this.getTermsConditionColoumnList();
    this.newService.postTermsTemplate(userObj); */
    if (this.termsForm.valid) {
      this.newService.postTermsTemplate(userObj)
        .subscribe(data => {
          console.log(data);

          if (data.termsTemplateId != null) {
            alert("Data added !");
            this.termsManagementCol = null;
            this.getTermsConditionColoumnList();
          } else {
            alert("Data not added !");
          }

        }, error => {
          this.getTermsConditionColoumnList();
        })
    } else {
      alert("Enter Data")
    }
  }

  private getAllOrgList() {
    this.newService.getAllOrgDetails()
      .subscribe(data => {
        this.orgList = data;
        this.getOrgId(this.orgList);
      })
  }

  public getOrgId(orgList) {
    orgList.forEach(element => {
      this.newService.getLoginInfo()
      .subscribe(data =>{
        this.ETA_SELECTED_ORGID = data.orgId;
        console.log(this.ETA_SELECTED_ORGID)
        if (this.ETA_SELECTED_ORGID == element.orgId) {
          this.orgName = element.orgName;
        }
      })
    });
  }
 
  menuList: any = [];
  getHeaderMenuList() {
    this.newService.getHeaderMenu()
      .subscribe(res => {
        this.menuList = res;
        this.menuList.forEach(element => {
          if (element.routerLink == "eta-web/organizationdashboard") {
            this.mainbreadcrumb = element.translation;
          }
          if (element.routerLink == "eta-web/orgtermsdashboard") {
            this.breadcrumb = element.translation;
          }
        })

      })
  }

  private getTermsConditionColoumnList() {
    this.newService.getOrgPrivacyTermsInnerColumn()
      .subscribe(res => {
        this.termsManagementCol = res.coloumnName;
      })
  }

  cancel() {
    localStorage.setItem('backlocation', "orgtermsdashboard");
    this.location.back();
  }

  editTnc(tmp, tmpvid) {
    this.newService.getEditTnc(tmp, tmpvid)
      .subscribe(data => {
        this.get_OrgId = data.orgId;
        this.get_orgName = data.orgName;
        this.editTempid = data.termsTemplateId;
        this.editTempVid = data.termsTemplateVersionId;
        this.termsForm = this._formBuilder.group({
          orgname: [data.orgName],
          termsevent: [data.termsNameEvent],
          termsname: [data.termsName],
          termstext: [data.termsText]

        })
      })
  }
}
