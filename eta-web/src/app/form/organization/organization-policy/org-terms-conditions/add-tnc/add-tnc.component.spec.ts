import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddTncComponent } from './add-tnc.component';

describe('AddTncComponent', () => {
  let component: AddTncComponent;
  let fixture: ComponentFixture<AddTncComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddTncComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddTncComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
