import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ServiceService } from './../../../../../eta-services/service.service';



@Component({
  selector: 'app-terms-condition',
  templateUrl: './terms-condition.component.html',
  styleUrls: ['./terms-condition.component.css'],
  providers: [ServiceService]
})
export class TermsConditionComponent implements OnInit {

  /***   For Sub Breadcrumb  **/
  public breadcrumb: String;
  /*** For Main Breadcrumb **/
  public mainbreadcrumb: string;
  /** Read Org TNC URL from service TS  */
  public TNC_url: any;
  /**  Org TNC DATA URL */
  public allTermsUrl: any;
  /** Org TNC Grid Column List */
  public termsManagementCol: any;
  /** Form Group */
  public termsForm: FormGroup;
  /** Selected Checked Box Template Id From Grid  */
  public getcheckedTempid: any;
  /** Selected Checked Box Template Version Id From Grid  */
  public getcheckedTempVid: any
  /** Edit Case Template Id */
  public editTempid: any = "";
  /** Edit Case Template Version Id */
  public editTempVid: any = "";
  /** Org List array */
  public orglist: any = [];
  /** org List */
  public orgList: any;
  /** Org Id */
  public get_OrgId: any;
  /** DraftYN */
  public get_DraftYN: any;
  /** ActiveYN */
  public get_ActiveYN: any;
  /** Org List */
  public newData: any;
  /* get orgName  */
  public get_OrgName:any;

  constructor(private newService: ServiceService, private _formBuilder: FormBuilder, private route: ActivatedRoute, private location: Location) {
    this.getHeaderMenuList();
    this.TNC_url = this.newService.Policy_URL;
    // this.termsUrl = this.TNC_url + 'getTermsGrid';
    this.allTermsUrl = this.TNC_url + '/getAllTermsGrid';
    this.route.params.subscribe(params => {
      this.getcheckedTempid = +params['tempid'];
      this.getcheckedTempVid = +params['tempvid']
      this.editTnc(this.getcheckedTempid, this.getcheckedTempVid);

    })
    this.getAllOrgList()
  }

  ngOnInit() {
    this.getEditTermsConditionColoumnList();
    this.termsForm = this._formBuilder.group({
      orgname: [],
      termsevent: [],
      termstext: [],
      activeYN: [],
      draftYN: []
    })
    this.newService.getAllOrgDetails()
      .subscribe(data => this.orglist = data);
  }


  onSubmit = function (termsObj) {

    let userObj: any = {};
    userObj['termsTemplateId'] = this.editTempid
    userObj['termsTemplateVersionId'] = this.editTempVid;
    userObj['orgId'] = this.get_OrgId;
    userObj['draftYN'] = this.get_DraftYN;
    userObj['activeYN'] = this.get_ActiveYN;
    userObj['termsNameEvent'] = termsObj.termsevent;
    userObj['termsName'] = termsObj.termsevent;
    this.termsManagementCol = null;
    this.newService.EditTermsAndCondition(userObj).subscribe(data => {

      if (data.termsTemplateId!=null) {
        alert("Data added !");
        this.getEditTermsConditionColoumnList();
      } else {
        alert("Data not added !");
      }
    })
  }

  editTnc(tmp, tmpvid) {
    this.newService.getEditTnc(tmp, tmpvid)
      .subscribe(data => {
        this.get_OrgName=data.orgName;
        this.get_OrgId = data.orgId;
        this.get_ActiveYN = data.activeYN;
        this.get_DraftYN = data.draftYN;
        this.editTempid = data.termsTemplateId;
        this.editTempVid = data.termsTemplateVersionId;
        this.termsForm = this._formBuilder.group({
          orgname: [data.orgName],
          termsevent: [data.termsNameEvent],
          activeYN: [data.activeYN],
          draftYN: [data.draftYN]
        })
      })
  }
  private getAllOrgList() {
    this.newService.getOrgDetailsByOrgId(this.get_OrgId)
      .subscribe(data => {
        this.newData = data;
      })
  }

  menuList: any = [];
  getHeaderMenuList() {
    this.newService.getHeaderMenu()
      .subscribe(res => {
        console.log(res);
        this.menuList = res;

        this.menuList.forEach(element => {
          if (element.routerLink == "eta-web/organizationdashboard") {
            this.mainbreadcrumb = element.translation;
          }
          if (element.routerLink == "eta-web/orgtermsdashboard") {
            this.breadcrumb = element.translation;
          }
        })

      })
  }

  private getEditTermsConditionColoumnList() {
    this.newService.getOrgPrivacyTermsInnerColumn()
      .subscribe(res => {
        this.termsManagementCol = res.coloumnName;
      })
  }

  cancel() {
    localStorage.setItem('backlocation', "orgtermsdashboard");
    this.location.back();
  }

}
