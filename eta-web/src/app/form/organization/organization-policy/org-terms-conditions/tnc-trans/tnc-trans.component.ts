import { Component, OnInit } from '@angular/core';
import { ServiceService } from './../../../../../eta-services/service.service';

@Component({
  selector: 'app-tnc-trans',
  templateUrl: './tnc-trans.component.html',
  styleUrls: ['./tnc-trans.component.css']
})
export class TncTransComponent implements OnInit {

  /***   For Sub Breadcrumb  **/
  public breadcrumb: String;
  /*** For Main Breadcrumb **/
  public mainbreadcrumb: string;
  /* Read Org TNC URL from service TS  */
  public TNC_URL: any;
  /* Org TNC Data URL  */
  public termsUrl: any;
  /* Org Language List */
  public tdheader: any = {};
  /* Org TNC Grid Column List */
  public TNCColumnList: any = [];
  /* get Selected OrgID */
  public ETA_SELECTED_ORGID: any;

  constructor(private newService: ServiceService) {
    this.getHeaderMenuList();
    this.TNC_URL = this.newService.Policy_URL;
    this.termsUrl = this.TNC_URL + '/getTermsTranslateGrid';
    this.ETA_SELECTED_ORGID = localStorage.getItem("EtaSelectOrgId");

  }




  ngOnInit() {

    this.newService.getPolicyHeaderLanguageByOrgId()
      .subscribe(data => {
        this.tdheader = data;
        this.TNCColumnList.push({ data: 'Template' });
        data.forEach(element => {
          element.languageId;
          let column = { data: element.languageId }
          this.TNCColumnList.push(column)
        });
      });

  }
  menuList: any = [];
  getHeaderMenuList() {
    this.newService.getHeaderMenu()
      .subscribe(res => {
        this.menuList = res;
        this.menuList.forEach(element => {
          if (element.routerLink == "eta-web/organizationdashboard") {
            this.mainbreadcrumb = element.translation;
          }
          if (element.routerLink == "eta-web/orgtermsdashboard") {
            this.breadcrumb = element.translation;
          }
        })

      })
  }

}
