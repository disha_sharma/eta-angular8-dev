import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TncTransComponent } from './tnc-trans.component';

describe('TncTransComponent', () => {
  let component: TncTransComponent;
  let fixture: ComponentFixture<TncTransComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TncTransComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TncTransComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
