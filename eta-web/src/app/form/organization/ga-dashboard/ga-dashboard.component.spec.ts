import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GaDashboardComponent } from './ga-dashboard.component';

describe('GaDashboardComponent', () => {
  let component: GaDashboardComponent;
  let fixture: ComponentFixture<GaDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GaDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GaDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
