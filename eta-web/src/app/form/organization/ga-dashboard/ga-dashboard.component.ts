import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Http, Headers, RequestOptions } from '@angular/http';
import { DualListComponent } from 'angular-dual-listbox';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { ServiceService } from './../../../eta-services/service.service'


@Component({
  selector: 'app-ga-dashboard',
  templateUrl: './ga-dashboard.component.html',
  styleUrls: ['./ga-dashboard.component.css']
})
export class GaDashboardComponent implements OnInit {

  /***   For Sub Breadcrumb  **/
  public breadcrumb: String;
  /*** For Main Breadcrumb **/
  public mainbreadcrumb: string;
  /** Get GA URL form Shereservice */
  public GA_URL: any;
  /** GA Data Url */
  public gaDataUrl: any;
  /** GA Column List */
  public gaUrlCol: any;
  /** Get Selected Value From Grid */
  public gaObj: any;

  public isEditAllowed: boolean = false;




  constructor(private r: Router, private newService: ServiceService) {
    this.getHeaderMenuList();
    // this.orgcheckbox1 = 'orgId';
    this.GA_URL = this.newService.Ga_Url;
    this.gaDataUrl = this.GA_URL + '/getGaDashboardGrid';
  }




  /* gaUrlCol: any = [
    { data: 'check', displayName: "Id", hyperlink: false, edit: false },
    { data: 'orgId', displayName: "Org Id", hyperlink: 'orgId', edit: false, hover: 'modifiedName' },
    { data: 'gaConfigId', displayName: "ga ConfigId", hyperlink: 'gaConfigId', edit: false },
    { data: 'gaKey', displayName: "ga Key", hyperlink: 'gaKey', edit: false },
    { data: 'gaDomainName', displayName: "ga Domain Name", hyperlink: 'gaDomainName', edit: false }

  ]; */

  /*  getcheckBoxId(event) {
     let x = event.split("+");
     this.editid = x[0];
     this.editOrgId = x[1];
 
   } */
  ngOnInit() {
    //this.newService.currentMessage.subscribe(message => this.gaObj = message);
    this.newService.refreshEditMessage();
    this.newService.currentMessage.subscribe(message => {
      this.gaObj = message
      if (message != 'editFalse') {
        this.isEditAllowed = true;
      }
    });
    this.newService.getGaOuterColumn()
      .subscribe(res => {
        console.log(res);

        this.gaUrlCol = res.coloumnName;
      })
  }


  addGA() {
    this.r.navigate(["/addgaconfig"]);
  }
  editGA() {
    this.r.navigate(["/editgaconfig/" + this.gaObj.id]);
  }

  menuList: any = [];
  public addButton: boolean;
  public addButtonText: any;
  public editButton: any;
  public editButtonText: any;

  getHeaderMenuList() {
    this.newService.getHeaderMenu()
      .subscribe(res => {
        this.menuList = res;
        this.menuList.forEach(element => {
          if (element.routerLink == "eta-web/organizationdashboard") {
            this.mainbreadcrumb = element.translation;
          }
          if (element.routerLink == "eta-web/gadashboard") {
            this.breadcrumb = element.translation;
          }
          if (element.menuType == 5 && element.routerLink == "addgaconfig") {
            this.addButton = true;
            this.addButtonText = element.translation;
          }
          if (element.menuType == 5 && element.routerLink == "editgaconfig") {
            this.editButton = true;
            this.editButtonText = element.translation;
          }

        })

      })
  }

  shownRouterLink(routerlink) {
    this.r.navigate(["/" + routerlink]);
  }

  activeLink = "gadashboard";
  background = '';

  toggleBackground() {
    this.background = this.background ? '' : 'primary';
  }


}
