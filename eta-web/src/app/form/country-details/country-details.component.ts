import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ServiceService } from './../../eta-services/service.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-country-details',
  templateUrl: './country-details.component.html',
  styleUrls: ['./country-details.component.css'],
  providers: [ServiceService]
})
export class CountryDetailsComponent implements OnInit {

  /** Form Group  */
  public countryForm: FormGroup;
  /** to get language List  */
  public languageList: any = [];
  /** to get country List  */
  public countryList: any = [];
  /** to get state List  */
  public stateList: any = [];
  /** to get currency List  */
  public currencyList: any = [];
  /** to get currency List  */
  public timezoneList: any = [];
  /** to get Date time Format List  */
  public DateTimeFormatList: any = [];
  /** to get Distance Measurement List List  */
  public DistanceMeasurementList: any = [];
  /** Country Object  */
  public countryObj: any = {};
  public defaultColumnToSort: any;
  public location_url: any;
  demo: string = "Harkesh ahshahs";
  constructor(private formBuilder: FormBuilder, private newService: ServiceService,private location: Location) {
    this.getAllLanguageName();
    this.getAllCountry();
    //  this.getAllState();
    this.getAllCurrency();
    this.getAllTimeZone();
    this.getMeasurementByIdentifier();
    this.getAllDateFormatList();
    this.location_url = this.newService.Org_URL + '/getLocationMainGrid';
    this.defaultColumnToSort = "countryId"
  }

  ngOnInit() {
    this.countryForm = this.formBuilder.group({
      country: [],
      state: [],
      language: [],
      currency: [],
      symbol: [],
      currencySymbolFormat: [],
      measurement: [],
      timezone: [],
      domain: [],
      dateTimeFormat: []

    })

  }

  /**
   *To Get All Language List
   *
   * @memberof CountryDetailsComponent
   */
  public getAllLanguageName() {
    this.newService.getAllLanguageName()
      .subscribe(data => {
        console.log(data);
        this.languageList = data;
      });
  }

  /**
   *To get All Country List
   *
   * @memberof CountryDetailsComponent
   */
  public getAllCountry() {
    this.newService.getAllCountry()
      .subscribe(data => {
        console.log(data);

        this.countryList = data;

      });
  }

  /**
   *To Get All State List
   *
   * @memberof CountryDetailsComponent
   */
  public getAllState() {
    this.newService.getAllState()
      .subscribe(data => {
        console.log(data);
        this.stateList = data;
      });
  }

  /**
   *To Get All Currency List
   *
   * @memberof CountryDetailsComponent
   */
  public getAllCurrency() {
    this.newService.getAllCurrency()
      .subscribe(data => {
        console.log(data);

        this.currencyList = data;
      });
  }

  /**
   *To Get All TimeZone List
   *
   * @memberof CountryDetailsComponent
   */
  public getAllTimeZone() {
    this.newService.getAllTimeZone()
      .subscribe(data => {
        console.log(data);
        this.timezoneList = data;
      });
  }


  public getMeasurementByIdentifier() {
    this.newService.getMeasurementByIdentifier("Distance")
      .subscribe(res => {
        console.log(res);
        this.DistanceMeasurementList = res;
      })
  }

  public getAllDateFormatList() {
    this.newService.getAllDateFormatList()
      .subscribe(res => {
        console.log(res);
        this.DateTimeFormatList = res;
      })
  }
  /**
   * Submit Button
   *
   * @memberof CountryDetailsComponent
   */
  public onSubmit = function (calKey) {


    let countryDetialObj: any = {};

    countryDetialObj['countryId'] = this.countryForm.value.country;
    countryDetialObj['stateId'] = this.countryForm.value.state;
    countryDetialObj['languageId'] = this.countryForm.value.language;
    countryDetialObj['currencyName'] = this.countryForm.value.currency;
    countryDetialObj['currencyCode'] = this.countryForm.value.symbol;
    countryDetialObj['currencyFormat'] = this.countryForm.value.currencySymbolFormat;
    countryDetialObj['distanceMeasurement'] = this.countryForm.value.measurement;
    countryDetialObj['timeZone'] = this.countryForm.value.timezone;
    countryDetialObj['domain'] = this.countryForm.value.domain;
    countryDetialObj['dataTimeFormat'] = this.countryForm.value.dataTimeFormat;
    this.newService.setCountryDetails(countryDetialObj)
      .subscribe(res => {
        console.log(res);
        if (res != null && res.countryId != null) {
          alert("Data ")
        }
      })

  }

  currentCurrency: any;
  countrySelected(countryId) {

    this.newService.getStateByCountry(countryId)
      .subscribe(res => {
        console.log(res);
        this.stateList = res;
      })
    this.newService.getCurrencyByCountry(countryId)
      .subscribe(res => {
        console.log(res);
        this.currentCurrency = res.currencyID;
        this.countryForm = this.formBuilder.group({
          currency: [this.currentCurrency],
          symbol: [this.currentCurrency],
          currencySymbolFormat: [this.currentCurrency],
          country: [countryId],
          state: [],
          language: [],
          measurement: [],
          timezone: [],
          domain: [],
          date: [],
          dateTimeFormat: []
        })
      })
    
    // this.countryForm.value.state = this.stateList[0].stateName;


  }

  cancel() {
   
    this.location.back();
  }

}
