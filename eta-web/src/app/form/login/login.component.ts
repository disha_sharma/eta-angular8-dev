import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { ServiceService } from '../../eta-services/service.service';
//import { ServiceService } from './../../eta-services/service.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  orgfrm: FormGroup;
  formobj: Object = {};
  userData: any = {};
  userValidationObject: Object = {};

  isPrivacyPolicy: any;
  isTermsCondition: any;
  isValidated: any;
  userType: any;
  orgId: any;

  logfrm: FormGroup;
  User_url = this.newService.User_URL + '/auth';
  hide = true;
  isLoadingResults: boolean = false;

  constructor(private _formBuilder: FormBuilder, private http: HttpClient, private route: ActivatedRoute,
    private r: Router, private cookieService: CookieService, private newService: ServiceService) {

  }

  ngOnInit() {


    this.logfrm = this._formBuilder.group({
      name: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required]),
    })
  }

  workSpace() {
    this.newService.getUserActivationStatusByToken()
      .subscribe(data => {
        console.log('data',data);
        
        this.isValidated = data.isValidated;
        this.isTermsCondition = data.isTermsConditionRequired;
        this.isPrivacyPolicy = data.isPrivacyPolicyRequired;
        this.userType = data.userType;
        this.orgId = data.orgId;


        if (this.isValidated == '0') {
          console.log('i got in if');
          this.newService.getUserTerms()
            .subscribe(res => {
              if (res != null) {

                if (this.isTermsCondition == 0) {
                  this.r.navigateByUrl('acceptcondition');
                }

                if (this.isTermsCondition == 1 && this.isPrivacyPolicy == 0) {
                  this.r.navigateByUrl('acceptpolicy');
                }
              }

            },
              error => {
               // alert("I am error " + data.orgId)
                this._landingPageAfterliceseCheck(data.orgId)
              })



        } else {
          console.log('i got in else');
          console.log('i got in else data.orgId',data.orgId);
          this._landingPageAfterliceseCheck(data.orgId)
        }
      });



  }

  private _landingPageAfterliceseCheck(orgId) {
    this.newService.checkAuthLiceseInfo(orgId)
      .subscribe(res => {
       
        if (res == null && this.userType == "ORG_Onwer") {
          this.r.navigate(['/services-pricing'], { queryParams: { o: orgId } });
        } else {
          if (this.userType == "ETA_Admin" || this.userType == "ORG_Admin") {
            this.newService.login();
            this.isLoadingResults = true;
          } else {
            this.newService.loginTasK();
          }
        }
      })

  }

  userWorkSpace(priviliegeId) {
    this.r.navigateByUrl('/process/' + priviliegeId);
  }



  onSubmit = function (logindetails) {
    this.isLoadingResults = true;
    this.formobj["username"] = logindetails.name;

    this.formobj["password"] = logindetails.password;

    this.http.post(this.User_url, this.formobj)
      .subscribe(result => {
        this.authtoken = result;
        sessionStorage.setItem('AuthToken', JSON.stringify(this.authtoken.token));
        sessionStorage.setItem('refreshToken', JSON.stringify(this.authtoken.refreshToken));
        this.workSpace();
        this.error = '';
        console.log('result',result);
        
      },
        error => {
          this.isLoadingResults = false;
          this.error = error;
          this.autherror = this.error.error;
        }
      );


  }
  public startFreeTrial() {
    this.r.navigate(["/freetrial"]);
  }
}