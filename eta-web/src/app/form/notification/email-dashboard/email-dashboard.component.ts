import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServiceService } from '../../../eta-services/service.service';


@Component({
  selector: 'app-email-dashboard',
  templateUrl: './email-dashboard.component.html',
  styleUrls: ['./email-dashboard.component.css']
})
export class EmailDashboardComponent implements OnInit {

  /** For To store Email URL form Shareservice */
  Email_URL: any;

  public shown: any;
  /**
   *   For Sub Breadcrumb 
   **/
  public breadcrumb: String;
  /**
   *  For Main Breadcrumb  
    **/
  public mainbreadcrumb: string;

  /** Email data url*/
  public emailDataUrl: any;

  /** For Email Coulumn List */
  public emailConfigCol: any;
  /** For Store Selected value In Grid */
  public emailNotificationObj: any;

  /** For assign CheckedBox Value as composite key  */
  public checkbox: any;
  public checkbox2: any;

  /** For Store List of Master Smtp Config */
  masterConfig: any;

  public defaultColumnToSort: any;

  public emailUrl: any;

  public isEditAllowed: boolean = false;

  constructor(private newService: ServiceService, private router: Router, ) {
    this.getHeaderMenuList();
    this.defaultColumnToSort = "smtpUserName";
    this.Email_URL = newService.Email_URL;
    this.emailDataUrl = this.Email_URL + "/spdEmailProvidersGrid";
    this.emailUrl = this.Email_URL + "/getEmailProvidersAngularGrid";
    //this.getEmailConfiguration();

  }

  private getEmailConfiguration() {
    //let userobjWrapper: Object = {};
    this.newService.getAllEmailConfigMaster()
      .subscribe(data => {
        this.masterConfig = data;
      });
  }
  ngOnInit() {
    // this.newService.currentMessage.subscribe(message => this.emailNotificationObj = message);
    this.newService.refreshEditMessage();
    this.newService.currentMessage.subscribe(message => {
      this.emailNotificationObj = message
      if (message != 'editFalse') {
        this.isEditAllowed = true;
      }
    });
    this.newService.getEmailProviderMainColumn()
      .subscribe(res => {
        this.emailConfigCol = res.coloumnName;
      })

    this.newService.getEmailServiceProvidersAngularGridData()
      .subscribe(data => {
        // console.log(data);

      })
  }
  addMailConfig() {
    this.router.navigate(["/addemailconfiguration"]);
  }
  editMailConfig() {
    this.router.navigate(["/editemailconfiguration/" + this.emailNotificationObj.configId + "/" + this.emailNotificationObj.orgId]);
  }

  menuList: any = [];
  public addButton: boolean;
  public addButtonText: any;
  public editButton: any;
  public editButtonText: any;

  getHeaderMenuList() {
    this.newService.getHeaderMenu()
      .subscribe(res => {
        this.menuList = res;

        this.menuList.forEach(element => {
          if (element.routerLink == "eta-web/emaildashboard") {
            this.mainbreadcrumb = element.translation;
          }
          if (element.routerLink == "eta-web/emaildashboard") {
            this.breadcrumb = element.translation;
          }
          if (element.menuType == 5 && element.routerLink == "addemailconfiguration") {
            this.addButton = true;
            this.addButtonText = element.translation;
          }
          if (element.menuType == 5 && element.routerLink == "editemailconfiguration") {
            this.editButton = true;
            this.editButtonText = element.translation;
          }
        })

      })
  }


  shownRouterLink(routerlink) {
    this.router.navigate(["/" + routerlink]);
  }

  /* public getEmailProviderMainGrid() {
    this.newService.getEmailProviderMainColumn()
      .subscribe(res => {
        this.emailConfigCol = res.coloumnName;
      })
  } */

  activeLink = "emaildashboard";
  background = '';

  toggleBackground() {
    this.background = this.background ? '' : 'primary';
  }
}