import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServiceService } from '../../../eta-services/service.service';


@Component({
  selector: 'app-email-log-dashboard',
  templateUrl: './email-log-dashboard.component.html',
  styleUrls: ['./email-log-dashboard.component.css']
})
export class EmailLogDashboardComponent implements OnInit {

  /***   For Sub Breadcrumb  **/
  public breadcrumb: String;
  /*** For Main Breadcrumb **/
  public mainbreadcrumb: string;
  /** For Store Notification URL */
  public Email_URL: any;
  /** For Email Log data Url */
  public emailLogUrl: any;
  /**  For Email log Column list*/
  public emailLogCol: any;
  /** For Menu List  */
  public menuList: any = [];

  constructor( private newService: ServiceService, private router: Router,) {
    this.getHeaderMenuList();
    this.Email_URL = newService.Email_URL;
    this.emailLogUrl = this.Email_URL + '/sendEmailLogGrid'
  }

  ngOnInit() {
    this.newService.getEmailSentLogColumn()
      .subscribe(res => {
        this.emailLogCol = res.coloumnName;
      })
  }

  public getHeaderMenuList() {
    this.newService.getHeaderMenu()
      .subscribe(res => {
        this.menuList = res;
        this.menuList.forEach(element => {
          if (element.routerLink == "eta-web/emaildashboard") {
            this.mainbreadcrumb = element.translation;
          }
          if (element.routerLink == "eta-web/emaillogdashboard") {
            this.breadcrumb = element.translation;
          }
        })
      })
  }


  public shownRouterLink(routerlink) {
    this.router.navigate(["/" + routerlink]);
  }
  activeLink = "emaillogdashboard";
  background = '';

  toggleBackground() {
    this.background = this.background ? '' : 'primary';
  }

}
