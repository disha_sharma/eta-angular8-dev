import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmailLogDashboardComponent } from './email-log-dashboard.component';

describe('EmailLogDashboardComponent', () => {
  let component: EmailLogDashboardComponent;
  let fixture: ComponentFixture<EmailLogDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmailLogDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmailLogDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
