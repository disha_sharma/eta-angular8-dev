import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmailTempletConfigurationComponent } from './email-templet-configuration.component';

describe('EmailTempletConfigurationComponent', () => {
  let component: EmailTempletConfigurationComponent;
  let fixture: ComponentFixture<EmailTempletConfigurationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmailTempletConfigurationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmailTempletConfigurationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
