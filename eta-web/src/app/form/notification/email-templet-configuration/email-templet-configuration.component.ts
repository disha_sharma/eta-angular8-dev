import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ServiceService } from './../../../eta-services/service.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-email-templet-configuration',
  templateUrl: './email-templet-configuration.component.html',
  styleUrls: ['./email-templet-configuration.component.css'],
  providers: [ServiceService]
})
export class EmailTempletConfigurationComponent implements OnInit {

  /***   For Sub Breadcrumb  **/
  public breadcrumb: String;
  /*** For Main Breadcrumb **/
  public mainbreadcrumb: string;
  /** For Store Notification URL */
  public Email_URL: any;
  /** For Email Template  data Url */
  public templateurl: any;
  /** Form builder */
  public templateform: FormGroup;
  /** set Checked Box Value  */
  public checkbox1: any;
  /** set Checked Box Value  */
  public checkbox2: any;
  /** Org list */
  public orgList: any;
  /** Selected OrgId */
  public ETA_SELECTED_ORGID: any;
  /** Email Template Column List */
  public templateConfigCol: any;
  /** Org Name  */
  public orgName: any;
  public defaultColumnToSort:any;
  public emailTemplateInnerDataUrl: any; 
  /** Get checked Template id Value form Grid  */
  public getcheckedTempid: any;
  /** Get checked Template Version id Value form Grid  */
  public getcheckedTempVid: any 
  /** Org ID */
  public get_OrgId: any;
   /** get OrgName */
   public get_orgName: any;
   /** Edit Case Policy Template ID  */
   public editTempId: any = "";
   /** Edit Case Policy Template Version ID  */
   public editTempVId: any = "";

  public editorConfig = {
    editable: true,
    spellcheck: true,
    height: '10rem',
    minHeight: '5rem',
    placeholder: 'Type something... (^_^)',
    translate: 'no',
    enableToolbar: true,
    showToolbar: true,
    imageEndPoint: "/asset",
  };
   /**    Template Data url  */
   public templateDataUrl: any;

  constructor(private newService: ServiceService, private _formBuilder: FormBuilder, private router: Router, private location: Location, private route: ActivatedRoute) {
    this.getHeaderMenuList();
    this.defaultColumnToSort ="emailTemplateId";
    this.newService.getAllOrgDetails().subscribe(data => {
      this.orgList = data;
      this.getOrgId(this.orgList);
    })
    this.checkbox1 = 'emailTemplateId'
    this.checkbox2 = 'emailTemplateVersionId'
    this.Email_URL = this.newService.Email_URL;
    this.templateDataUrl = this.Email_URL + '/getTemplateInnerGrid';
    this.emailTemplateInnerDataUrl=this.Email_URL + '/getEmailTemplateAngularInnnerGrid';
    this.ETA_SELECTED_ORGID = localStorage.getItem("EtaSelectOrgId");
    //this.getAllOrgList();
    this.defaultColumnToSort ="emailTemplateId";
    this.emailTemplateInnerDataUrl=this.Email_URL + '/getEmailTemplateAngularInnnerGrid';
    this.route.params.subscribe(params => {
      this.getcheckedTempid = +params['tempid'];
      this.getcheckedTempVid = +params['tempvid']
      this.editTemplateConfig(this.getcheckedTempid, this.getcheckedTempVid)
    })
  }



  ngOnInit() {
    this.getEmailTemplateColoumnList();
    this.templateform = this._formBuilder.group({
      orgname: [this.ETA_SELECTED_ORGID],
      templatename: new FormControl('', [Validators.required]),
      Message:[],
      Subject: [],
    })
    this.newService.getEmailTemplateAngularInnerGridData()
    .subscribe(data => {
      // console.log(data);

    })
    this.newService.getEmailTemplateAngularInnerGridData()
    .subscribe(data => {
      // console.log(data);

    })
  }

  onSubmit = function (temp) {
    let userobj: Object = {};
    let langobj: Object = {};
    userobj["emailTemplateId"] = this.editTempId;
    userobj["emailTemplateVersionId"] = this.editTempVId;
    userobj["formId"] = '1002';
    userobj["mailSendEvent"] = temp.templatename;
    userobj["translationMailSubject"] = temp.Subject;
    userobj["translationMailMessage"] = temp.Message;
    userobj["orgId"] = this.get_OrgId;
    //this.templateConfigCol = null;
    if (this.templateform.valid) {
      this.newService.postTemplate(userobj)
        .subscribe(data => {
          if (data.emailTemplateId != null) {
            alert("Data added !" + data.emailTemplateId);
            this.templateConfigCol = null;
            this.getEmailTemplateColoumnList();
          } else {
            alert("Data not added");
          }
        }, error => {
          alert("Data not added");
          this.getEmailTemplateColoumnList();
        });
    } else {
      alert("Enter Data")
    }
  }

  editTemplateConfig(tempid, tempvid) {
    this.newService.getTemplateByTmpTmpvid(tempid, tempvid)
    .subscribe(data => {
      console.log(data);
      
      this.editTempId = data.emailTemplateId,
      this.editTempVId = data.emailTemplateVersionId,
      this.get_OrgId = data.orgId,
      this.get_orgName = data.orgName
      this.templateform = this._formBuilder.group({
        orgname: [data.orgName],
        templatename: [data.mailSendEvent],
        Message: [data.translationMailMessage],
        Subject: [data.translationMailSubject],
      })
    });
    
  }

  addMailTemplateTrans() {
    this.router.navigate(["/emailtempletlanguage"]);

  }


  cancel() {
    localStorage.setItem('backlocation', "MAILTEMP");
    this.location.back();
  }

  public getOrgId(orgList) {
    orgList.forEach(element => {
      this.newService.getLoginInfo()
      .subscribe(data =>{
        this.ETA_SELECTED_ORGID = data.orgId;
        console.log(this.ETA_SELECTED_ORGID)
        if (this.ETA_SELECTED_ORGID == element.orgId) {
          this.orgName = element.orgName;
        }
      })
    });
  }
 

  menuList: any = [];
  getHeaderMenuList() {
    this.newService.getHeaderMenu()
      .subscribe(res => {
        this.menuList = res;
        this.menuList.forEach(element => {
          if (element.routerLink == "eta-web/emaildashboard") {
            this.mainbreadcrumb = element.translation;
          }
          if (element.routerLink == "eta-web/emailtemplatedashboard") {
            this.breadcrumb = element.translation;
          }
        })

      })
  }

  private getEmailTemplateColoumnList() {
    this.newService.getEmailTemplateInnerColumn()
      .subscribe(res => {
        this.templateConfigCol = res.coloumnName;
      })
  }
}

