import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServiceService } from '../../../eta-services/service.service';


@Component({
  selector: 'app-email-template-dashboard',
  templateUrl: './email-template-dashboard.component.html',
  styleUrls: ['./email-template-dashboard.component.css']
})
export class EmailTemplateDashboardComponent implements OnInit {

  /***   For Sub Breadcrumb  **/
  public breadcrumb: String;
  /*** For Main Breadcrumb **/
  public mainbreadcrumb: string;
  /** For Store Notification URL */
  public Email_URL: any;
  /** For Email Template  data Url */
  public templateConfigUrl: any;
  /** For Email Template Column List */
  public templateConfigCol: any;
  /** Email Template Obj To store Checked Value */
  public templateObj: any;
  /** Set checkBox Value  */
  public templatecheckbox1: any;
  /** Set checkBox Value  */
  public templatecheckbox2: any;

  public defaultColumnToSort: any;

  public emailTemplateConfigUrl: any;

  public isEditAllowed: boolean = false;

  constructor(private newService: ServiceService, private router: Router, ) {

    this.getHeaderMenuList();
    this.defaultColumnToSort = "emailTemplateId"
    this.Email_URL = newService.Email_URL;
    this.templateConfigUrl = this.Email_URL + '/getAlltemplateGrid';
    this.emailTemplateConfigUrl = this.Email_URL + '/getEmailTemplateAngularGrid';
  }

  ngOnInit() {
    // this.newService.currentMessage.subscribe(message => this.templateObj = message);
    this.newService.refreshEditMessage();
    this.newService.currentMessage.subscribe(message => {
      this.templateObj = message
      if (message != 'editFalse') {
        this.isEditAllowed = true;
      }
    });
    this.newService.getEmailTemplateColumn()
      .subscribe(res => {
        this.templateConfigCol = res.coloumnName;
      })
    this.newService.getEmailTemplateAngularGridData()
      .subscribe(data => {
        // console.log(data);

      })
  }

  addMailTemplate() {
    this.router.navigate(["/addemailtemplate"]);
  }
  editMailTemplate() {
    this.router.navigate(["/addemailtemplate/" + this.templateObj.emailTemplateId + "/" + this.templateObj.emailTemplateVersionId]);
  }
  addMailTemplateTrans() {
    this.router.navigate(["/emailtempletlanguage"]);
  }

  menuList: any = [];
  public addButton: boolean;
  public addButtonText: any;
  public editButton: any;
  public editButtonText: any;
  public emailTranslationButton: any;
  public emailTranslationButtonText: any;

  getHeaderMenuList() {
    this.newService.getHeaderMenu()
      .subscribe(res => {
        this.menuList = res;
        this.menuList.forEach(element => {
          if (element.routerLink == "eta-web/emaildashboard") {
            this.mainbreadcrumb = element.translation;
          }
          if (element.routerLink == "eta-web/emailtemplatedashboard") {
            this.breadcrumb = element.translation;
          }
          if (element.menuType == 5 && element.routerLink == "eta-web/addemailtemplate") {
            this.addButton = true;
            this.addButtonText = element.translation;
          }
          if (element.menuType == 5 && element.routerLink == "eta-web/editemailtemplate") {
            this.editButton = true;
            this.editButtonText = element.translation;
          }
          if (element.menuType == 5 && element.routerLink == "eta-web/emailtempletlanguage") {
            this.emailTranslationButton = true;
            this.emailTranslationButtonText = element.translation;
          }
        })

      })
  }

  shownRouterLink(routerlink) {
    this.router.navigate(["/" + routerlink]);
  }

  activeLink = "emailtemplatedashboard";
  background = '';

  toggleBackground() {
    this.background = this.background ? '' : 'primary';
  }

}