import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmailTemplateDashboardComponent } from './email-template-dashboard.component';

describe('EmailTemplateDashboardComponent', () => {
  let component: EmailTemplateDashboardComponent;
  let fixture: ComponentFixture<EmailTemplateDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmailTemplateDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmailTemplateDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
