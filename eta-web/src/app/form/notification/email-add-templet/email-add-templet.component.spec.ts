import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmailAddTempletComponent } from './email-add-templet.component';

describe('EmailAddTempletComponent', () => {
  let component: EmailAddTempletComponent;
  let fixture: ComponentFixture<EmailAddTempletComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmailAddTempletComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmailAddTempletComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
