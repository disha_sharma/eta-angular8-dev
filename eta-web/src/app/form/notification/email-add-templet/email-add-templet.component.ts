import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ServiceService } from './../../../eta-services/service.service'
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-email-add-templet',
  templateUrl: './email-add-templet.component.html',
  styleUrls: ['./email-add-templet.component.css'],
  providers: [ServiceService]
})
export class EmailAddTempletComponent implements OnInit {


  /**    Template Data url  */
  public templateDataUrl: any;
  /**   Template Data Column list   */
  public templateConfigCol: any;
  /***   For Sub Breadcrumb     **/
  public breadcrumb: String;
  /*** For Main Breadcrumb      * */
  public mainbreadcrumb: string;
  /** Form builder */
  public templateform: FormGroup;
  /** For Notification Url form Service TS */
  public Email_URL: any;
  /** Org list */
  public orgList: any;
  /** Get checked Template id Value form Grid  */
  public getcheckedTempid: any;
  /** Get checked Template Version id Value form Grid  */
  public getcheckedTempVid: any
  /** Get Selected OrgId  */
  public ETA_SELECTED_ORGID: any;
  /** Edit Case OrgId */
  public get_OrgId: any;
  /** Edit Case DraftYN */
  public get_DraftYN: any;
  /** Edit Case ActiveYn */
  public get_ActiveYN: any;
  /** Edit Case org name */
  public orgName: any;
  /** edit Template id */
  public editTempId: any = "";
  /** Edit Template Version id */
  public editTempVId: any = "";

  public emailTemplateInnerDataUrl: any;
  public defaultColumnToSort:any;

  constructor(private newService: ServiceService, private _formBuilder: FormBuilder, private route: ActivatedRoute, private location: Location) {
    this.getHeaderMenuList();
    this.defaultColumnToSort ="emailTemplateId";
    this.newService.getAllOrgDetails().subscribe(data => {
      this.orgList = data;
    });
    this.Email_URL = newService.Email_URL;

    this.templateDataUrl = this.Email_URL + '/getTemplateInnerGrid';
    this.emailTemplateInnerDataUrl=this.Email_URL + '/getEmailTemplateAngularInnnerGrid';
    //Chnages-24-May
    this.ETA_SELECTED_ORGID = localStorage.getItem("EtaSelectOrgId");
    this.route.params.subscribe(params => {
      this.getcheckedTempid = +params['tempid'];
      this.getcheckedTempVid = +params['tempvid']

      this.getEditEmailTemplate(this.getcheckedTempid, this.getcheckedTempVid)
    })
    
  }


  ngOnInit() {
    this.getEditEmailTemplateColoumnList();
    this.templateform = this._formBuilder.group({
      emailtemplatename: new FormControl('', [Validators.required]),
      draftYN: [],
      activeYN: [],
      orgId: []
    })

    this.newService.getEmailTemplateAngularInnerGridData()
    .subscribe(data => {
      // console.log(data);

    })
  }

  onSubmit = function (temp) {
    let userobj: Object = {};
    console.log(this.editTempId);
    userobj["emailTemplateId"] = this.editTempId;
    userobj["emailTemplateVersionId"] = this.editTempVId;
    userobj["formId"] = '1002';
    userobj["orgId"] = this.get_OrgId;
    userobj["draftYN"] = this.get_DraftYN;
    userobj["activeYN"] = this.get_ActiveYN;
    userobj["mailSendEvent"] = temp.emailtemplatename;
    this.templateConfigCol = null;
    this.newService.EditEmailTemplate(userobj).subscribe(data => {
      if (data.emailTemplateId != null) {
        alert("Data added !");
        this.getEditEmailTemplateColoumnList()
      } else {
        alert("Data not added");
      }

    });
  }


  getEditEmailTemplate(tmp, tmpvid) {
    this.newService.getTemplateByTmpTmpvid(tmp, tmpvid)
      .subscribe(data => {

        this.get_OrgId = data.orgId;
        if (this.orgList != null) {
          this.orgList.forEach(element => {
            if (this.ETA_SELECTED_ORGID == 0) {
              this.ETA_SELECTED_ORGID = 1;
            }
            if (this.get_OrgId == element.orgId) {
              this.orgName = element.orgName;
            }


          });
        }
        //Chnages-24-May
        this.get_ActiveYN = data.activeYN;
        this.get_DraftYN = data.draftYN;
        this.editTempId = data.emailTemplateId;
        this.editTempVId = data.emailTemplateVersionId;
        this.templateform = this._formBuilder.group({
          //Chnages-24-May
          orgname: [data.orgName],
          emailtemplatename: [data.mailSendEvent],
          draftYN: [data.draftYN],
          activeYN: [data.activeYN]
        })

      })
  }
 

  cancel() {
    localStorage.setItem('backlocation', "MAILTEMP");
    this.location.back();
  }


  menuList: any = [];
  getHeaderMenuList() {
    this.newService.getHeaderMenu()
      .subscribe(res => {
        this.menuList = res;
        if (this.menuList != null && this.menuList != "") {
          this.menuList.forEach(element => {
            if (element.routerLink == "eta-web/emaildashboard") {
              this.mainbreadcrumb = element.translation;
            }
            if (element.routerLink == "eta-web/emailtemplatedashboard") {
              this.breadcrumb = element.translation;
            }
          })
        }
      })
  }

  private getEditEmailTemplateColoumnList() {

    this.newService.getEmailTemplateInnerColumn()
      .subscribe(res => {
        this.templateConfigCol = res.coloumnName;
       
      })
  }

}
