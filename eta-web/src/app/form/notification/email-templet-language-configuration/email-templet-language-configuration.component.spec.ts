import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmailTempletLanguageConfigurationComponent } from './email-templet-language-configuration.component';

describe('EmailTempletLanguageConfigurationComponent', () => {
  let component: EmailTempletLanguageConfigurationComponent;
  let fixture: ComponentFixture<EmailTempletLanguageConfigurationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmailTempletLanguageConfigurationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmailTempletLanguageConfigurationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
