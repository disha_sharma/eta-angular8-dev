import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ServiceService } from './../../../eta-services/service.service';

@Component({
  selector: 'app-email-templet-language-configuration',
  templateUrl: './email-templet-language-configuration.component.html',
  styleUrls: ['./email-templet-language-configuration.component.css'],
  providers: [ServiceService],

})
export class EmailTempletLanguageConfigurationComponent implements OnInit {

  /***   For Sub Breadcrumb  **/
  public breadcrumb: String;
  /*** For Main Breadcrumb **/
  public mainbreadcrumb: string;
  /** For Store Notification URL */
  public Email_URL: any;
  /** For Email Template Translation  data Url */
  public emailTemplateurl = "";
  /** Store Org Language List */
  public tdheader: any = {};
  /** Get selected orgId */
  public ETA_SELECTED_ORGID: any;


  constructor(private newService: ServiceService, private _formBuilder: FormBuilder) {
    this.getHeaderMenuList();
    this.Email_URL = this.newService.Email_URL;
    this.emailTemplateurl = this.Email_URL + '/datatableshow'

  }



  functioncol: any = [];
  array: any = [];

  ngOnInit() {
    this.ETA_SELECTED_ORGID = localStorage.getItem("EtaSelectOrgId");
    this.newService.getHeaderLanguageByOrgId(1)
      .subscribe(data => {
        this.tdheader = data;
        this.functioncol.push({ data: 'Template' });
        data.forEach(element => {
          element.languageId;
          let column = { data: element.languageId }
          this.functioncol.push(column)
        });
      });


  }

  menuList: any = [];
  getHeaderMenuList() {
    this.newService.getHeaderMenu()
      .subscribe(res => {
        this.menuList = res;
        this.menuList.forEach(element => {
          if (element.routerLink == "eta-web/emaildashboard") {
            this.mainbreadcrumb = element.translation;
          }
          if (element.routerLink == "eta-web/emailtemplatedashboard") {
            this.breadcrumb = element.translation;
          }
        })

      })
  }



}
