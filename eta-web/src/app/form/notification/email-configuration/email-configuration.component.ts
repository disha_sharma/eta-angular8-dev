import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ServiceService } from './../../../eta-services/service.service';

@Component({
  selector: 'app-email-configuration',
  templateUrl: './email-configuration.component.html',
  styleUrls: ['./email-configuration.component.css'],
  providers: [ServiceService]
})

export class EmailConfigurationComponent implements OnInit {
  /* For Store Email Notification Url Form shareservice */
  public Email_URL: any;
  /**
   *   For Sub Breadcrumb 
   **/
  public breadcrumb: String;

  /** 
     * For Main Breadcrumb  
     * */
  public mainbreadcrumb: string;
  /** 
     * For Email Data
     * */
  public emaiDataUrl: any;
  /** For Email Column List */
  emailConfigCol: any;
  /** For assign CheckedBox Value as composite key  */
  public checkbox1: any;
  public checkbox2: any;

  /** Store checked Value in Grid */
  public getcheckConfigId: number;
  public getCheckedOrgId: number;
  /** Form Builder */
  emailConfigForm: FormGroup;
  /** Master Smtp Config */
  public masterConfig: any;
  /** provider Name */
  public providername = "";
  /** Smtp name */
  public smtp = "";
  /** port No */
  public port = "";
  /** Store Edit config id  */
  public checkBoxCID = "";
  /** Store Edit orgId */
  public checkBoxOID = "";

  public emailServiceInnerDataUrl: any;
  public defaultColumnToSort: any;
  public disableEditButton:boolean =false;

  constructor(private newService: ServiceService, private route: ActivatedRoute, private _formBuilder: FormBuilder, private location: Location) {
    this.getHeaderMenuList();
    this.defaultColumnToSort = "smtpUserName";
    this.Email_URL = newService.Email_URL;
    this.checkbox1 = 'conf';
    this.checkbox2 = 'orgId';
    this.route.params.subscribe(params => {
      this.getcheckConfigId = +params['configid'];
      this.getCheckedOrgId = +params['orgId']
      this.emaiDataUrl = this.Email_URL + "/spdEmailProvidersInnerGrid";
      this.emailServiceInnerDataUrl = this.Email_URL + "/getEmailProvidersAngularInnnerGrid";
      this.editEmailConfig(this.getcheckConfigId, this.getCheckedOrgId);
    });
    this.getEmailProviderInnerGrid();
    this.getEmailConfiguration();
  }


  hypercol: any = [
    { check: 'link' }
  ];

  ETA_SELECTED_ORGID = localStorage.getItem("EtaSelectOrgId");


  ngOnInit() {

    this.emailConfigForm = this._formBuilder.group({
      configId: new FormControl('', [Validators.required]),
      username: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required]),
      // ProviderName: new FormControl('', [Validators.required]),
      smtpDetails: new FormControl('', []),
      org: new FormControl(this.ETA_SELECTED_ORGID)
    });

    this.newService.getEmailServiceProvidersAngularInnerGridData()
      .subscribe(data => {
        // console.log(data);

      })


    if (this.masterConfig != undefined && this.masterConfig != null) {
      this.providername = this.masterConfig.configProviderName;
      this.smtp = this.masterConfig.smtpAddress;
      this.port = this.masterConfig.smtpPort;
      console.log(this.emailConfigForm.value)
    }
  }
  onSubmit = function (spdconfig) {

    let userobj: Object = {};

    userobj["configId"] = spdconfig.configId;
    userobj["orgId"] = this.checkBoxOID;
    userobj["smtpUserName"] = spdconfig.username;
    userobj["smtpPassword"] = spdconfig.password;
    userobj["smtpDiscription"] = spdconfig.smtpDetails;
    userobj["status"] = '0';
    //this.emailConfigCol = null;
    if (this.emailConfigForm.valid) {
      this.newService.postSpdConfig(userobj)
        .subscribe(data => {
          let retrunData = data;
          let configId = retrunData.configId;
          if (configId != null) {
            alert("Data added !");
            this.emailConfigCol = null;
            this.getEmailProviderInnerGrid();
          } else {
            alert("Data not added !");
          }

        }, error => {
          alert("there is Some server Error !!!")
          this.getEmailProviderInnerGrid();
        });
    } else {
      alert("please enter data");
      this.getEmailProviderInnerGrid();
    }

  }

  private getEmailConfiguration() {

    let userobjWrapper: Object = {};
    this.newService.getAllEmailConfigMaster()
      .subscribe(data => {
        this.masterConfig = data;
      });
  }



  public editEmailConfig(confId, orgId) {
    if (confId > 0) {

      this.newService.getLoginInfo()
        .subscribe(res => {
          if (res.orgId == orgId) {
            this.newService.getSpdConfigByOrgIdAndConfigId(confId, orgId).subscribe(data => {
              this.checkBoxCID = data.configId;
              this.checkBoxOID = data.orgId
              this.disableEditButton= false;
              this.emailConfigForm = this._formBuilder.group({
                configId: [data.configId],
                username: [data.smtpUserName],
                password: [data.smtpPassword],
                smtpDetails: [data.smtpDiscription],
                org: [data.orgId]
              })
            })
          } else {
            this.newService.getSpdConfigByOrgIdAndConfigId(confId, orgId).subscribe(data => {
              this.checkBoxCID = data.configId;
              this.checkBoxOID = data.orgId
              this.emailConfigForm.disable();
              this.disableEditButton= true;
              this.emailConfigForm = this._formBuilder.group({
                configId: [data.configId],
                username: [data.smtpUserName],
                password: [data.smtpPassword],
                smtpDetails: [data.smtpDiscription],
                org: [data.orgId]
              })
            })
          }
        })



    }

  }

  cancel() {
    this.location.back();
  }

  private menuList: any = [];
  getHeaderMenuList() {
    this.newService.getHeaderMenu()
      .subscribe(res => {
        this.menuList = res;
        this.menuList.forEach(element => {
          if (element.routerLink == "eta-web/emaildashboard") {
            this.mainbreadcrumb = element.translation;
          }
          if (element.routerLink == "eta-web/emaildashboard") {
            this.breadcrumb = element.translation;
          }
        })

      })
  }

  private getEmailProviderInnerGrid() {
    this.newService.getEmailProviderInnerColumn()
      .subscribe(res => {
        this.emailConfigCol = res.coloumnName;
      })

  }

}
