import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ServiceService } from './../../../eta-services/service.service';

@Component({
  selector: 'app-email-log',
  templateUrl: './email-log.component.html',
  styleUrls: ['./email-log.component.css'],
  providers: [ServiceService]
})
export class EmailLogComponent implements OnInit {

/**  Extra Component **/

  /** For Store Notification Url For SharedService */
  Email_URL: any;
  /** for Email Log Data url */
  emailUrl: any;


  constructor(private http: HttpClient, private newService: ServiceService) {

    this.Email_URL = newService.Email_URL;
    this.emailUrl = this.Email_URL + "/sendEmailLogGrid"
  }
  emailLogCol: any = [
    { data: 'Check', displayName: "Check Id", hyperlink: false, edit: false, plantext: false },
    { data: 'templateName', displayName: "templateName", hyperlink: false, edit: 'templateName', plantext: false },
    { data: 'senderEmailAddress', displayName: "senderEmailAddress", hyperlink: false, edit: false, plantext: 'senderEmailAddress' },
    { data: 'receiverEmailAddress', displayName: "receiverEmailAddress", hyperlink: false, edit: false, plantext: 'receiverEmailAddress' },
    { data: 'emailSubject', displayName: "emailSubject", hyperlink: false, edit: false, plantext: 'emailSubject' },
    { data: 'emailMessage', displayName: "emailMessage", hyperlink: false, edit: false, plantext: 'emailMessage' },
    { data: 'createdBy', displayName: "createdBy", hyperlink: 'createdBy', edit: false, plantext: false },
    { data: 'sendDate', displayName: "sendDate", hyperlink: false, edit: false, plantext: 'sendDate' },
    { data: 'deliveryStatus', displayName: "deliveryStatus", hyperlink: false, edit: false, plantext: 'deliveryStatus' },

  ];



  ngOnInit() {
  }

}
