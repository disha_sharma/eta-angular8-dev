import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-popup',
  templateUrl: './popup.component.html',
  styleUrls: ['./popup.component.css']
})
export class PopupComponent implements OnInit {

  popupfrm: FormGroup;
  constructor(private _formBuilder: FormBuilder) { }
  formobj: Object = {};

  ngOnInit() {
    this.popupfrm = this._formBuilder.group({
      name: [],
      Message: []
    })
  }

  onSubmit() {
   // console.log(this.popupfrm.value);
  }
  cancel() {
    //console.log(this.popupfrm.value);
  }
}

