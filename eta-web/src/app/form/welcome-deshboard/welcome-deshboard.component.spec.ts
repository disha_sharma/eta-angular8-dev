import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WelcomeDeshboardComponent } from './welcome-deshboard.component';

describe('WelcomeDeshboardComponent', () => {
  let component: WelcomeDeshboardComponent;
  let fixture: ComponentFixture<WelcomeDeshboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WelcomeDeshboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WelcomeDeshboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
