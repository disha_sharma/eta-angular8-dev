import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ServiceService } from './../../../eta-services/service.service';

@Component({
  selector: 'app-user-assign-role',
  templateUrl: './user-assign-role.component.html',
  styleUrls: ['./user-assign-role.component.css'],
  providers: [ServiceService]
})

export class UserAssignRoleComponent implements OnInit {

  /***   For Sub Breadcrumb  **/
  public breadcrumb: String;
  /*** For Main Breadcrumb **/
  public mainbreadcrumb: string;

  /** Form Group */
  public assignRoleForm: FormGroup;
  /** Role List */
  public roleNameList: any = {};
  /** Select role List */
  public selectedRoleNameList: any = [];
  /*** user Id */
  public getUserId: any;
  /** Role Type */
  public userRoleType: any;
  /** Role Name */
  public userName: any;
  public isSpinnerShow: boolean = false;

  checked = false;

  /**
   *Creates an instance of UserAssignRoleComponent.
   * @param {FormBuilder} _formBuilder
   * @param {ServiceService} newService
   * @param {ActivatedRoute} route
   * @param {Router} r
   * @param {Location} location
   * @memberof UserAssignRoleComponent
   */
  constructor(private _formBuilder: FormBuilder, private newService: ServiceService, private route: ActivatedRoute, private r: Router, private location: Location) {
    this.getHeaderMenuList();
    this.route.params.subscribe(params => {
      this.getUserId = +params['userId'];
      this.getAllUser(this.getUserId);


    });
  }

  /**
   *Onit Method
   *
   * @memberof UserAssignRoleComponent
   */
  ngOnInit() {
    this.assignRoleForm = this._formBuilder.group({
      userid: new FormControl('', [Validators.required]),
      firstName: new FormControl('', [Validators.required]),
    });
  }


  /**
   *To Get User details by UserId
   *
   * @param {*} userId
   * @memberof UserAssignRoleComponent
   */
  public getAllUser(userId) {
    this.newService.getUserByUserId(userId).subscribe(data => {
      this.userName = data.firstName;
      this.userRoleType = data.userType;
      this.assignRoleForm = this._formBuilder.group({
        firstName: [data.firstName]
      });
      this.getAllRoleDetails(this.userRoleType);
    });
  }

  /**
   *To Get All Role List By RoleType 
   *
   * @param {*} userRoleType
   * @memberof UserAssignRoleComponent
   */
  public getAllRoleDetails(userRoleType) {
    this.newService.getAllRoleOrgIdAndRoleTypeId(userRoleType)
      .subscribe(data => {
        this.roleNameList = data;
        this.getRolesByUserId(this.getUserId);
      });

  }

  /**
   *To get Select Role Id
   *
   * @param {*} event
   * @memberof UserAssignRoleComponent
   */
  selectRoleID(event) {
    console.log(event);
    console.log(event.checked);
    console.log(event.source);
    console.log(event.source.value);

    let checkBoxevent = event.checked;
    let selectedValue = event.source.value;
    if (checkBoxevent) {
      this.selectedRoleNameList.push(selectedValue);
    }
    else {
      const index: number = this.selectedRoleNameList.indexOf(selectedValue);
      if (index !== -1) {
        this.selectedRoleNameList.splice(index, 1);
      }
    }

  }

  /**
   *To submit Assign role 
   *
   * @memberof UserAssignRoleComponent
   */
  onSubmitMethod() {
    let roleObj: any = {};
    roleObj['userId'] = this.getUserId;
    roleObj['roleId'] = this.selectedRoleNameList;
    if (this.assignRoleForm.valid) {
      this.isSpinnerShow = true;
      this.newService.setUserRoleWrapperDetails(roleObj)
        .subscribe(data => {
          this.isSpinnerShow = false;
          if (data.id == null) {
            alert("Role Not Assign");
          }
          else {
            alert("Your Data  has been updated successfully!!! ");

          }
        },error=>{
          alert("Role Not Assign");
          this.isSpinnerShow = false;
        });
      
    } else {
      alert("please  select assign role ")
    }

  }

  /**
   *Get role  by User ID
   *
   * @param {*} getUserId
   * @memberof UserAssignRoleComponent
   */
  getRolesByUserId(getUserId) {
    this.newService.getUserRoleByUserId(getUserId).subscribe(data => {
      data.forEach(element => {
        console.log(element);
        console.log("element");


        console.log($(document.getElementById(element.roleId)));

        $(document.getElementById(element.roleId)).prop('checked', true);
        this.selectedRoleNameList.push(element.roleId + "");
        console.log(this.selectedRoleNameList);

      });
    });
  }


  menuList: any = [];
  /**
   *Header menu List to display menu
   *
   * @memberof UserAssignRoleComponent
   */
  getHeaderMenuList() {
    this.newService.getHeaderMenu()
      .subscribe(res => {
        this.menuList = res;
        this.menuList.forEach(element => {
          if (element.routerLink == "eta-web/userdashboard") {
            this.breadcrumb = element.translation;
          }
          if (element.routerLink == "eta-web/userdashboard") {
            this.mainbreadcrumb = element.translation;
          }

        })

      })
  }
  /**
   *To Go back Last location or Page
   *
   * @memberof UserAssignRoleComponent
   */
  cancel() {
    localStorage.setItem('backlocation', "userdashboard");
    this.location.back();
  }
}