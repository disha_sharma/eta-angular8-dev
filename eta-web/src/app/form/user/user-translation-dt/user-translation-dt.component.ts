import { HttpClient } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { ServiceService } from '../../../eta-services/service.service';


class DataTablesResponse {
  data: any[];
  draw: number;
  recordsFiltered: number;
  recordsTotal: number;
  coloumnName: {};
}


@Component({
  selector: 'app-user-translation-dt',
  templateUrl: './user-translation-dt.component.html',
  styleUrls: ['./user-translation-dt.component.css']
})
export class UserTranslationDtComponent implements OnInit {

  @Input() childMessage: string;
  @Input() myurl: string;
  @Input() mycoloums = [];
  @Input() orgId: number;
  @Input() checkpid: number;
  @Input() gridName: string;


  min: number;
  max: number;
  tdheader: any = {};
  languageData: any;


  dtOptions: DataTables.Settings = {};
  gefields: any[];
  gefieldsName: any[];
  public arrayOfKeys = [];
  public arrayOfValues = [];
  ETA_SELECTED_ORGID: any = '0';
  myroleId: any;
  roleId: number;
  privilegeId: number;



  constructor(private http: HttpClient, private newService: ServiceService, private route: ActivatedRoute) {



  }

  ngOnInit() {
    if (localStorage.getItem("EtaSelectOrgId") != null) {
      this.ETA_SELECTED_ORGID = localStorage.getItem("EtaSelectOrgId");
    }
    
    this.privilegeId = this.checkpid;
    this.roleId = this.checkpid;
    this.myroleId = this.checkpid;;
    console.log(this.checkpid);
    console.log(this.mycoloums);
    console.log(this.myurl);


    const that = this;
    this.newService.getRolePrivHeaderLanguageByOrgId()
      .subscribe(data => {
        this.tdheader = data;
        this.tdheader.forEach(element => {
          this.languageData = element.spdlanguage.language;
        });

      });

    $.fn['dataTable'].ext.search.push((settings, data, dataIndex) => {
      const id = parseFloat(data[0]) || 0; // use data for the id column
      if ((isNaN(this.min) && isNaN(this.max)) ||
        (isNaN(this.min) && id <= this.max) ||
        (this.min <= id && isNaN(this.max)) ||
        (this.min <= id && id <= this.max)) {
        return true;
      }
      return false;
    });


    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 4,
      serverSide: true,
      processing: true,
      columns: this.mycoloums,
      ajax: (dataTablesParameters: any, callback) => {
        dataTablesParameters.orgId = this.ETA_SELECTED_ORGID;
        dataTablesParameters.roleId = this.roleId;
        dataTablesParameters.privilegeId = this.privilegeId;
        that.http
          .post<DataTablesResponse>(this.myurl, dataTablesParameters, {})
          .subscribe(resp => {
            that.gefields = resp.data;
            that.gefieldsName = resp.data;
            console.log(this.tdheader);
            console.log(this.gefields);

            this.arrayOfKeys = Object.keys(resp.coloumnName);
            this.arrayOfValues = Object.values(resp.coloumnName);
            callback({
              recordsTotal: resp.recordsTotal,
              recordsFiltered: resp.recordsFiltered,
              data: []
            });
          });
      },
    };
  }
  makeCellEdit(e) {
    console.log(e.target);
    console.log(e.target.nextElementSibling);

    e.target.nextElementSibling.setAttribute("class", "textBox");
    e.target.nextElementSibling.setAttribute("contentEditable", "true");
    console.log(e.target.nextElementSibling);
  }

  saveDetails(event, obj, key, isUpdate: boolean) {
    let as: HTMLElement = event.target;
    event.target.setAttribute("class", "");
    event.target.setAttribute("contentEditable", "false");

    let newTranslationObj = {};

    if (this.gridName === 'Role') {
      newTranslationObj["roleId"] = obj.id;
      newTranslationObj["roleVersionId"] = obj.versionId;
      newTranslationObj["languageID"] = key;
      newTranslationObj["roleTranslation"] = as.innerText;;
      newTranslationObj["roleDesTranslation"] = as.innerText;;
      console.log(newTranslationObj);
      this.http.post(this.newService.User_URL + '/setRoleLanguage', newTranslationObj)
        .subscribe(data => {
        });
    }
    else {
      newTranslationObj["privilegeId"] = obj.id;
      newTranslationObj["privilegeVersionId"] = obj.versionId;
      
      newTranslationObj["languageID"] = key;
      newTranslationObj["privilegeTranslation"] = as.innerText;
      newTranslationObj["privilegeDesTranslation"] = as.innerText;
      this.http.post(this.newService.User_URL + '/setPrivilegeLanguage', newTranslationObj)
        .subscribe(data => {
        });
    }
  }
}
