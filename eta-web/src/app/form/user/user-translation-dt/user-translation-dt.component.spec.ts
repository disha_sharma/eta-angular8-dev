import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserTranslationDtComponent } from './user-translation-dt.component';

describe('UsertranslationDtComponent', () => {
  let component: UserTranslationDtComponent;
  let fixture: ComponentFixture<UserTranslationDtComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserTranslationDtComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserTranslationDtComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
