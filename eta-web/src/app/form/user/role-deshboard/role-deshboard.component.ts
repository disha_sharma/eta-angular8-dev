import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { ServiceService } from './../../../eta-services/service.service';

@Component({
  selector: 'app-role-deshboard',
  templateUrl: './role-deshboard.component.html',
  styleUrls: ['./role-deshboard.component.css']
})
export class RoleDeshboardComponent implements OnInit {

  /***   For Sub Breadcrumb  **/
  public breadcrumb: String;
  /*** For Main Breadcrumb **/
  public mainbreadcrumb: string;
  /** Read User Role URL from service TS  */
  public Role_URL: any;
  /**User Role Data URL */
  public roleurl: any;
  /** User Role Grid Column List */
  public roleCol: any;
  /** Get Select checkBox  Value From Grid  */
  public roleObj: any;
  //public userDataUrl: any;

  public userRoleUrl: any;

  public defaultColumnToSort:any;

  public isEditAllowed: boolean = false;

  /**
   *Creates an instance of RoleDeshboardComponent.
   * @param {Router} r
   * @param {CookieService} cookieService
   * @param {ServiceService} newService
   * @memberof RoleDeshboardComponent
   */
  constructor(private r: Router, private cookieService: CookieService, private newService: ServiceService) {
    this.Role_URL = this.newService.Role_URL;
    this.roleurl = this.Role_URL + '/getAllRoleGrid';
    //this.userDataUrl = this.newService.User_URL + '/getUserAngularGrid'
    this.getHeaderMenuList();
    this.defaultColumnToSort ="roleId"
    this.userRoleUrl=this.Role_URL + '/getUserRoleAngularGrid';

  }


  columnDefs: any[];
  ngOnInit() {
    
    this.newService.refreshEditMessage();
    this.newService.currentMessage
      .subscribe(message => {
        this.roleObj = message
        if(message != 'editFalse'){
          this.isEditAllowed =true;
        }
        
      });
    this.newService.getUserRoleColumn()
      .subscribe(res => {
        this.roleCol = res.coloumnName;
      })

      this.newService.getUserRoleAngularGridData()
      .subscribe(data => {
        // console.log(data);

      })

  }

  /**
   *
   *For to Add Role Page when you click on add  Add Button
   * @memberof RoleDeshboardComponent
   */
  addRole() {
    this.r.navigate(["/addrole/"]);
  }
  /**
   *For to Edit Role Page when you click on add  Edit  
   *
   * @param {*} roleId
   * @memberof RoleDeshboardComponent
   */
  editRole(roleId) {
    this.r.navigate(["/editrole/" + this.roleObj.roleId]);
  }


  menuList: any = [];
  public addButton: boolean;
  public addButtonText: any;
  public editButton: any;
  public editButtonText: any;

  /**
   *Header menu List to display menu
   *
   * @memberof RoleDeshboardComponent
   */
  getHeaderMenuList() {
    this.newService.getHeaderMenu()
      .subscribe(res => {
        this.menuList = res;
        this.menuList.forEach(element => {
          if (element.routerLink == "eta-web/userdashboard") {
            this.mainbreadcrumb = element.translation;
          }
          if (element.routerLink == "eta-web/roledashboard") {
            this.breadcrumb = element.translation;
          }
          if (element.menuType == 5 && element.routerLink == "eta-web/addrole") {
            this.addButton = true;
            this.addButtonText = element.translation;
          }
          if (element.menuType == 5 && element.routerLink == "eta-web/editrole") {
            this.editButton = true;
            this.editButtonText = element.translation;
          }
        })

      })
  }

  /**
   *To set Router link Menu List
   *
   * @param {*} routerlink
   * @memberof RoleDeshboardComponent
   */
  shownRouterLink(routerlink) {
    this.r.navigate(["/" + routerlink]);
  }

  public activeLink = 'roledashboard';
  background = '';

  toggleBackground() {
    this.background = this.background ? '' : 'primary';
  }
}
