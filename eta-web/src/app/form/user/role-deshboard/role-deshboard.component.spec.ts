import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RoleDeshboardComponent } from './role-deshboard.component';

describe('RoleDeshboardComponent', () => {
  let component: RoleDeshboardComponent;
  let fixture: ComponentFixture<RoleDeshboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoleDeshboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoleDeshboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
