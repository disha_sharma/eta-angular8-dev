import { Location } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ServiceService } from './../../../eta-services/service.service';

@Component({
  selector: 'app-user-privilege',
  templateUrl: './user-privilege.component.html',
  styleUrls: ['./user-privilege.component.css'],
  providers: [ServiceService]
})
export class UserPrivilegeComponent implements OnInit {

  /***   For Sub Breadcrumb  **/
  public breadcrumb: String;
  /*** For Main Breadcrumb **/
  public mainbreadcrumb: string;
  /** Read User Privilege URL from service TS  */
  public PRIVILEGE_URL: any;
  /**  User Privilege Data Url */
  public privilegeurl: any;
  /*** Privilege Inner Grid Column Store Variable */
  public privilegeCol: any;

  /** Get Selected checked Box privilge Id */
  private privilegeId: any;

  /** Edit Case Privilege Id */
  public editId: any;
  /** Org Id */
  public get_OrgId: any;
  /** Privilege Type */
  private get_PrivilegeType: any;
  /** Store Privilege Id for Translation */
  private storePrivilegeId: any;
  /** Grid Load check for Privilege Translation Grid */
  public isDataLoaded: any;

  private column: any;

  /** Org Language List */
  public tdheader: any = {};
  /** Privilege Translation  Grid Data */
  public PrivilegeTranslationUrl: any;
  /** Privilege Translation Grid Column List */
  private privTransColumn: any = [];
  /** same string is use in user role dt for validating the services */
  public Privilege = "Privilege";

  public userPrivilegeInnerDataUrl: any;

  public defaultColumnToSort: any;
  public isSpinnerShow: boolean = false;

  /**
   *Creates an instance of UserPrivilegeComponent.
   * @param {FormBuilder} _formBuilder
   * @param {ServiceService} newService
   * @param {ActivatedRoute} route
   * @param {Location} location
   * @param {HttpClient} http
   * @param {Router} r
   * @memberof UserPrivilegeComponent
   */
  constructor(private _formBuilder: FormBuilder, private newService: ServiceService, private route: ActivatedRoute, private location: Location, private http: HttpClient,
    private r: Router) {
    this.getHeaderMenuList();
    this.defaultColumnToSort = "privilege"
    this.PRIVILEGE_URL = this.newService.Role_URL;
    this.privilegeurl = this.PRIVILEGE_URL + '/getAllPrivilegeInnerGrid';
    this.PrivilegeTranslationUrl = this.newService.User_URL + "/getPrivilegeTranslationGrid";
    this.userPrivilegeInnerDataUrl = this.PRIVILEGE_URL + '/getUserPrivilegeAngularInnnerGrid';

    this.route.params.subscribe(params => {
      this.privilegeId = +params['priviID'];
      this.editprivilege(this.privilegeId);
    });
  }

  /**
   *Form Group Variable
   *
   * @type {FormGroup}
   * @memberof UserPrivilegeComponent
   */
  public privilegeForm: FormGroup;
  public formobj: Object = {};



  ngOnInit() {
    this.getUserPrivilegeColumnList();
    this.privilegeForm = this._formBuilder.group({
      privilegeName: [],
      privilegeDiscription: [],
    })

    this.newService.getUserPrivilegeAngularInnerGridData()
      .subscribe(data => {
        // console.log(data);

      })

  }

  /**
   *To save User Privilege 
   *
   * @memberof UserPrivilegeComponent
   */
  public onSubmit = function (privilegeObj) {

    let userObj: any = {};

    userObj['privilegeID'] = this.editId;
    userObj['privilege'] = privilegeObj.privilegeName;
    userObj['privilegeDescription'] = privilegeObj.privilegeDiscription;
    userObj['orgId'] = this.get_OrgId;
    userObj['privilegeType'] = this.get_PrivilegeType;
    this.privilegeCol = null;
    if (this.privilegeForm.valid) {
      this.isSpinnerShow = true;
      this.newService.postPrivilege(userObj)
        .subscribe(data => {
          this.isSpinnerShow = false;
          if (data.privilegeID == null) {
            alert("Data Not Submited")
            this.getUserPrivilegeColumnList();

          }
          else {
            alert("Privilege has been submited successfully !!!!");
            this.storePrivilegeId = data.privilegeID;
            this.getUserPrivilegeColumnList();

          }

        }, error => {
          alert("There is some issue, Try Again !!!")
          this.isSpinnerShow = false;
        })

    } else {
      alert("please enter Data");
      this.getUserPrivilegeColumnList();

    }



  }

  /**
   *To get Privilege Details On basic of Privilege Id to Display in edit Case
   *
   * @private
   * @param {*} privilegeId
   * @memberof UserPrivilegeComponent
   */
  private editprivilege(privilegeId) {
    this.newService.getPrivilegeById(privilegeId)
      .subscribe(data => {
        this.get_PrivilegeType = data.privilegeType;
        if (this.get_PrivilegeType == 1) {
          this.get_OrgId = data.orgId;
          this.editId = data.privilegeID;
          this.storePrivilegeId = data.privilegeID;
          this.privilegeForm.disable();
          this.privilegeForm = this._formBuilder.group({
            privilegeName: [data.privilege],
            privilegeDiscription: [data.privilegeDescription],
          })

        } else {
          this.get_OrgId = data.orgId;
          this.editId = data.privilegeID;
          this.storePrivilegeId = data.privilegeID;
          this.privilegeForm = this._formBuilder.group({
            privilegeName: [data.privilege],
            privilegeDiscription: [data.privilegeDescription],
          })
        }

      })

  }


  private menuList: any = [];
  /**
   *Header menu List to display menu
   *
   * @private
   * @memberof UserPrivilegeComponent
   */
  private getHeaderMenuList() {
    this.newService.getHeaderMenu()
      .subscribe(res => {
        this.menuList = res;
        this.menuList.forEach(element => {
          if (element.routerLink == "eta-web/userdashboard") {
            this.mainbreadcrumb = element.translation;
          }
          if (element.routerLink == "eta-web/privilegedashboard") {
            this.breadcrumb = element.translation;
          }
        })
      })
  }

  /**
   *Privilege Grid Column list 
   *
   * @private
   * @memberof UserPrivilegeComponent
   */
  private getUserPrivilegeColumnList() {
    this.newService.getUserPrivilegeInnerColumn()
      .subscribe(res => {
        this.privilegeCol = res.coloumnName;
      })
  }
  /**
   * to go back to last location or page
   *
   * @memberof UserPrivilegeComponent
   */
  public cancel() {
    localStorage.setItem('backlocation', "privilegedashboard");
    this.location.back();
  }

  /**
   *To get privilege Translation Grid Column List
   *
   * @memberof UserPrivilegeComponent
   */
  public translationColumns() {
    if (this.storePrivilegeId != null) {
      let coloumNames: any;
      let myColoumsObj: any;
      this.newService.getRolePrivHeaderLanguageByOrgId()
        .subscribe(data => {
          this.isDataLoaded = true;
          this.tdheader = data;
          this.privTransColumn.push({ data: 'privilege Name' });
          data.forEach(element => {
            element.languageId;
            this.column = { data: element.languageId }
            this.privTransColumn.push(this.column);
          });
        })
    } else {
      alert("Add a privilege first")
    }
  }


}
