import { Component, OnInit, } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { AbstractControl } from "@angular/forms";
import { Location } from '@angular/common';

import { ServiceService } from './../../../../eta-services/service.service'
import { element } from 'protractor';

@Component({
  selector: 'app-user-password-reset',
  templateUrl: './user-password-reset.component.html',
  styleUrls: ['./user-password-reset.component.css'],
  providers: [ServiceService]
})


export class UserPasswordResetComponent implements OnInit {

  private userDetailsObj: any = {};
  private passwordResetForm: FormGroup;
  private minLengthLength: number = 0;
  private numberLength: number = 0;
  private specialCharLength: number = 0;
  private uppercaseCharLength: number = 0;
  private userTypeId: any;
  private passUserID = "";
  private passEmail = "";
  private passOrgId = "";
  private isPasswordValid: boolean;
  private get_OrgId: any;
  private passwordErrorMsz: any;


  private passCheckedUserType: any;

  /* Password Check anf value  */
  public lengthCheck: boolean = false;
  public uppercaseCheck: boolean = false;
  public numberCheck: boolean = false;
  public specialCharCheck: boolean = false;
  public lengthCheckMsg: any;
  public uppercaseCheckMsg: any;
  public numberCheckMsg: any;
  public specialCharCheckMsg: any;



  constructor(private _formBuilder: FormBuilder, private route: ActivatedRoute, private http: Http, private newService: ServiceService, private location: Location) {
    this.route.params.subscribe(params => {
      this.passCheckedUserType = +params['userId'];
      this.getSpdOrgPasswordPolicyUserId(this.passCheckedUserType)
      // alert(this.passCheckedUserType);
    });
  }

  ngOnInit() {
    this.isPasswordValid = false;
    this.passwordResetForm = this._formBuilder.group({
      newPassword: ['', [Validators.required, Validators.pattern("^(?=.*?[A-Z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$")]],
      confirmPassword: [null, this.passValidator]
    })
  }

  private getSpdOrgPasswordPolicyUserId(Id) {
    this.newService.getSpdOrgPasswordPolicyUserId(Id)
      .subscribe(data => {
        console.log(data);

        this.minLengthLength = data.minLength;
        this.numberLength = data.number;
        this.specialCharLength = data.specialChar;
        this.uppercaseCharLength = data.uppercaseChar;
        this.userTypeId = data.userType;

        this.lengthCheckMsg = "Min length Should be " + this.minLengthLength;
        this.uppercaseCheckMsg = "No's Upper Case in " + this.uppercaseCharLength;
        this.numberCheckMsg = "Min Number should be " + this.numberLength;
        this.specialCharCheckMsg = this.specialCharLength + " Special character  ";
      });
  }

  public passValidator = function (control: AbstractControl) {
    if (control && (control.value !== null || control.value !== undefined)) {
      const cnfpassValue = control.value;
      const passControl = control.root.get('newPassword');
      if (passControl) {
        const passValue = passControl.value;
        if (passValue !== cnfpassValue) {
          return {
            isError: true
          };
        }
      }
    }
    return null;
  }

  public onSubmit = function (newpass) {
    let passObj: any = {};
    console.log(newpass.newPassword);
    let passwordString: string = newpass.newPassword;
    let PassCharList: any = [];
    var i: number;
    console.log(passwordString.length);
    for (i = 0; i <= passwordString.length; i++) {
      PassCharList.push(passwordString.charAt(i))
    }

    let TSpecial: number = 0;
    let TNumber: number = 0;
    let TUpper: number = 0;
    let Tlength: number = passwordString.length;

    PassCharList.forEach(element => {

      let hasNumber = (/[0-9]/.test(element));
      let hasUpper = (/[A-Z]/.test(element));
      let hasLower = (/[a-z]/.test(element));
      let hasSpecial = (/[#?!@$%^&*-]/.test(element));
      let hasLength = (/[8]/.test(element));

      if (hasSpecial == true) {
        TSpecial = TSpecial + 1;
      }
      if (hasNumber == true) {
        TNumber = TNumber + 1;
      }
      if (hasUpper == true) {
        TUpper = TUpper + 1;
      }
      // return null;
    });

    console.log("passwordString.length>>" + passwordString.length + " -- TSpecial>>" + TSpecial + " -- TNumber>>" + TNumber + " --TUpper>>" + TUpper);
    console.log("minLengthLength>>" + this.minLengthLength + "--numberLength>>" + this.numberLength + "--specialCharLength>>" + this.specialCharLength + "--uppercaseCharLength" + this.uppercaseCharLength);

    /* public lengthCheck:boolean=false;
  public uppercaseCheck :boolean=false;
  public numberCheck:boolean=false;
  public specialCharCheck:boolean=false;
  public lengthCheckMsg :any;
  public uppercaseCheckMsg:any;
  public numberCheckMsg:any;
  public specialCharCheckMsg:any; */


    if ((this.minLengthLength > passwordString.length) || (this.numberLength > TNumber) || (this.specialCharLength > TSpecial) || (this.uppercaseCharLength > TUpper)) {
      this.isPasswordValid = true;
      if (this.numberLength <= TNumber) {
        this.numberCheck = true;
      }
      if (this.specialCharLength <= TSpecial) {
        this.specialCharCheck = true;
      }
      if (this.uppercaseCharLength <= TUpper) {
        this.uppercaseCheck = true;
      }
      if (this.minLengthLength <= passwordString.length) {
        this.lengthCheck = true;
      }



      this.passwordErrorMsz = "Password is not valid Minimum Length" + this.minLengthLength + "Char, " +
        this.numberLength + " number " + this.specialCharLength + " special character and  " +
        this.uppercaseCharLength + " capital letter required ";

    }

    //passObj['userId'] = this.getUserId;
    //passObj['Id']=this.passCheckedUserType;
    passObj['userId'] = this.passCheckedUserType;
    passObj['password'] = newpass.newPassword;


    if (this.passwordResetForm.valid == true) {
      alert("Valid Details");
      this.newService.postNewPassword(passObj)
        .subscribe(data => {
          if (data.userId != null && data.userId != 0) {
            alert("password updated !");
            this.passwordResetForm.reset();
            this.router.navigate(["../../"]);
          } else {
            alert("password not updated !");

          }
        })
    } else {
      alert("enter Proper Details");
      this.isPasswordValid = true;
      this.passwordResetForm.controls['newPassword'].touched = false;
      console.log(this.passwordResetForm.controls['newPassword'].touched);
    }
  }

  fatchedPassword(value) {
    console.log(value);
    let passObj: any = {};
    console.log(value);
    let passwordString: string = value;
    let PassCharList: any = [];
    var i: number;
    console.log(passwordString.length);
    for (i = 0; i <= passwordString.length; i++) {
      PassCharList.push(passwordString.charAt(i))
    }

    let TSpecial: number = 0;
    let TNumber: number = 0;
    let TUpper: number = 0;
    let Tlength: number = passwordString.length;

    PassCharList.forEach(element => {

      let hasNumber = (/[0-9]/.test(element));
      let hasUpper = (/[A-Z]/.test(element));
      let hasLower = (/[a-z]/.test(element));
      let hasSpecial = (/[#?!@$%^&*-]/.test(element));
      let hasLength = (/[8]/.test(element));

      if (hasSpecial == true) {
        TSpecial = TSpecial + 1;
      }
      if (hasNumber == true) {
        TNumber = TNumber + 1;
      }
      if (hasUpper == true) {
        TUpper = TUpper + 1;
      }
      // return null;
    });

    console.log("passwordString.length>>" + passwordString.length + " -- TSpecial>>" + TSpecial + " -- TNumber>>" + TNumber + " --TUpper>>" + TUpper);
    console.log("minLengthLength>>" + this.minLengthLength + "--numberLength>>" + this.numberLength + "--specialCharLength>>" + this.specialCharLength + "--uppercaseCharLength" + this.uppercaseCharLength);

    if ((this.minLengthLength > passwordString.length) || (this.numberLength > TNumber) || (this.specialCharLength > TSpecial) || (this.uppercaseCharLength > TUpper)) {
      this.isPasswordValid = true;
    }
    if (this.numberLength <= TNumber) {
      this.numberCheck = true;
    } else {
      this.numberCheck = false;
    }
    if (this.specialCharLength <= TSpecial) {
      this.specialCharCheck = true;
    } else {
      this.specialCharCheck = false;
    }
    if (this.uppercaseCharLength <= TUpper) {
      this.uppercaseCheck = true;
    } else {
      this.uppercaseCheck = false;
    }
    if (this.minLengthLength <= passwordString.length) {
      this.lengthCheck = true;
    } else {
      this.lengthCheck = false;
    }

  }

  cancel() {
    this.location.back();
  }
  
}


