import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ServiceService } from '../../../../eta-services/service.service';


@Component({
  selector: 'app-accept-privacy-policy',
  templateUrl: './accept-privacy-policy.component.html',
  styleUrls: ['./accept-privacy-policy.component.css'],

})
export class AcceptPrivacyPolicyComponent implements OnInit {
  acceptform: FormGroup;
  acceptTerms: any = {};
  accepted: any;
  public isAccepted: boolean = true;
  public userType: any;
  constructor(private _formBuilder: FormBuilder, private shareService: ServiceService) {

    this.getPolicyText();
  }
  ngOnInit() {

    this.acceptform = this._formBuilder.group({
      AcceptText: this.acceptTerms.termsText,
      AcceptCheck: ['', [Validators.required]]

    })



  }
  private getPolicyText() {
    this.shareService.getUserPolicy()
      .subscribe(data => {
        this.acceptTerms = data;
        this.acceptform = this._formBuilder.group({
          AcceptText: data.policyText,
          AcceptCheck: [],

        })

      });
  }

  public onSubmit = function (acceptObj) {
    if (this.acceptform.valid == true) {
      this.isAccepted = false;
    }
    if (acceptObj.AcceptCheck == true) {
      /* Updating Flag for accept policy  */
      this.shareService.setAcceptPolicyFlag()
        .subscribe(data => {
          console.log(data);
          this.userType = data.userType;
          if (data.isPrivacyPolicyRequired == 1) {


            /* Check for Auth license  and Service Packages for Org */
            this.shareService.checkAuthLiceseInfo(data.orgId)
              .subscribe(res => {
                if (res == null && this.userType == "ORG_Onwer") {
                  this.r.navigate(['/services-pricing'], { queryParams: { o: data.orgId } });
                } else {
                  if (this.userType == "ETA_Admin" || this.userType == "ORG_Admin" || this.userType == "ORG_Onwer") {

                    this.shareService.login();

                  } else {
                    this.shareService.loginTasK();
                  }
                }
              })
          }
        })
    }
  }
}
