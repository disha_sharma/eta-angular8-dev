import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ServiceService } from './../../../../eta-services/service.service';

@Component({
  selector: 'app-accept-terms-conditions',
  templateUrl: './accept-terms-conditions.component.html',
  styleUrls: ['./accept-terms-conditions.component.css'],
  providers: [ServiceService]
})
export class AcceptTermsConditionsComponent implements OnInit {

  acceptform: FormGroup;
  acceptTerms: any = {};
  accepted: any;
  showData: boolean = false;
  constructor(private newService: ServiceService, private _formBuilder: FormBuilder, private r: Router, ) {
    this.getUserTerms();
  }
  ngOnInit() {
    this.acceptform = this._formBuilder.group({
      AcceptText: this.acceptTerms.policyText,
      AcceptCheck: ['', [Validators.required]]

    })

  }

  private getUserTerms() {
    this.newService.getUserTerms()
      .subscribe(data => {
        console.log(data);

        if (data == null) {
          // alert("There is no Active Terms and Conditions please contact to admin")
        }
        if (data != null) {
          this.showData = true;
          this.acceptTerms = data;
          this.acceptform = this._formBuilder.group({
            AcceptText: data.termsText,
            AcceptCheck: []
          })
        }

        /* this.showData=true;
          this.acceptTerms = data;
          this.acceptform = this._formBuilder.group({
            AcceptText: data.termsText,
            AcceptCheck: []
          }) */

      },
        error => {
          alert("There is no Active Terms and Conditions please contact to admin");
          this.newService.getLogoutInfo()
            .subscribe(res => {
              this.newService.logout();
            })
          localStorage.clear();
          sessionStorage.clear();
        });
  }

  public onSubmitTerms(acceptObj) {
    if (acceptObj.AcceptCheck == true) {

      this.newService.setAcceptTermsFlag()
        .subscribe(data => {
          this.accepted = data;
          if (this.accepted == 1) {
            this.r.navigate(["/acceptpolicy"]);
          }
        });
    }

  }
  logout() {
    console.log("close click");
    
    this.newService.getLogoutInfo()
      .subscribe(res => {
        this.newService.logout();
      })
    localStorage.clear();
    sessionStorage.clear();
}

}
