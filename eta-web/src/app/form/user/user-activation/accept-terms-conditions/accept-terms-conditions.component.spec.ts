import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AcceptTermsConditionsComponent } from './accept-terms-conditions.component';

describe('AcceptTermsConditionsComponent', () => {
  let component: AcceptTermsConditionsComponent;
  let fixture: ComponentFixture<AcceptTermsConditionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AcceptTermsConditionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AcceptTermsConditionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
