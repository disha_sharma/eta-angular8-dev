import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ServiceService } from './../../../eta-services/service.service';
@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css'],
  providers: [ServiceService]
})

export class UserProfileComponent implements OnInit {

  /***   For Sub Breadcrumb  **/
  public breadcrumb: String;
  /*** For Main Breadcrumb **/
  public mainbreadcrumb: string;

  constructor(private _formBuilder: FormBuilder, private newService: ServiceService) {
    this.getHeaderMenuList();
  }

  ngOnInit() {
   /*  this.newService.getUserByUserId(this.userId)
      .subscribe(data => {
        this.userDetails = data;
        this.firstName = data.firstName;
      }); */


  }

  /**
   *Menu List for Menu
   *
   * @private
   * @type {*}
   * @memberof UserdetailsComponent
   */
  private menuList: any = [];
  /**
   *To Get Header Menu List from Back-end 
   *
   * @private
   * @memberof UserdetailsComponent
   */
  private getHeaderMenuList() {
    this.newService.getHeaderMenu()
      .subscribe(res => {
        this.menuList = res;
        this.menuList.forEach(element => {
          if (element.routerLink == "eta-web/userdashboard") {
            this.breadcrumb = element.translation;
          }
          if (element.routerLink == "eta-web/userdashboard") {
            this.mainbreadcrumb = element.translation;
          }

        })

      })
  }
}
