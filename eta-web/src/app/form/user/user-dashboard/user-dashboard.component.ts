import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { ServiceService } from './../../../eta-services/service.service';

@Component({
  selector: 'app-user-dashboard',
  templateUrl: './user-dashboard.component.html',
  styleUrls: ['./user-dashboard.component.css']
})
export class UserDashboardComponent implements OnInit {

  /**   For Sub Breadcrumb  **/
  public breadcrumb: String;
  /** For Main Breadcrumb **/
  public mainbreadcrumb: string;
  /** User Data URL */
  public userGridURL: any;
  /** User Grid Column List */
  public userGridFunctionCol: any;
  /*** Get Select checkBox  Value From Grid  */
  public userObj: any;

  public userDataUrl: any;

  public defaultColumnToSort: any;

  isEditAllowed: boolean = false;
  /**
   *Creates an instance of UserDashboardComponent.
   * @param {Router} r
   * @param {CookieService} cookieService
   * @param {ServiceService} newService
   * @memberof UserDashboardComponent
   */
  constructor(private r: Router, private cookieService: CookieService, private newService: ServiceService) {
    this.userGridURL = this.newService.User_URL + '/getAllUserDataGridByOrgId';
    this.userDataUrl = this.newService.User_URL + '/getUserAngularGrid'
    this.getHeaderMenuList();
    this.defaultColumnToSort = "salutation"
    console.log('menu');
    /* this.newService.getRoleMainGridTest()
      .subscribe(data => {
        console.log("============Data==========================>>>>>>>>>>>");
        console.log(data);

      }) */

  }

  ngOnInit() {
    this.newService.refreshEditMessage();
    this.newService.currentMessage
      .subscribe(message => {
        this.userObj = message
        if(message != 'editFalse'){
          this.isEditAllowed =true;
        }
        
      });
    this.newService.getUserColumn()
      .subscribe(data => {
        this.userGridFunctionCol = data.coloumnName;
      })
    this.newService.getUserAngularGridData()
      .subscribe(data => {
        // console.log(data);

      })
      
    /* this.newService.getTotalNumberOfUser()
    .subscribe(data=>{
      console.log(data);
      alert(data.errorMessage);
      //status
    }) */
  }

  /**
   *For to Add User Page when you click on add  Add
   *
   * @memberof UserDashboardComponent
   */
  addUser() {
    this.newService.getTotalNumberOfUser()
      .subscribe(data => {
        if (data.status == true) {
          this.r.navigate(["/adduser"]);
        } else {
          alert(data.errorMessage);
        }

      })

  }

  /**
   *For to Edit User Page when you click on add  Add
   *
   * @memberof UserDashboardComponent
   */
  editUser() {
    this.r.navigate(["/edituser/" + this.userObj.userId]);
  }
  /**
   *For to User Assign Role Page when you click on add  assign role
   *
   * @memberof UserDashboardComponent
   */
  AddAssignRole() {
    this.r.navigate(["/editassignrole/" + this.userObj.userId]);
  }

  menuList: any = [];
  public addButton: boolean;
  public addButtonText: any;
  public editButton: any;
  public editButtonText: any;
  public assignRoleButton: boolean;
  public assignRoleButtonText: any;

  /**
   *Header menu List to display menu
   *
   * @memberof UserDashboardComponent
   */
  getHeaderMenuList() {
    console.log('menu');
    this.newService.getHeaderMenu()
      .subscribe(res => {
        this.menuList = res;
        this.menuList.forEach(element => {
          if (element.routerLink == "eta-web/userdashboard") {
            this.breadcrumb = element.translation;
          }
          if (element.routerLink == "eta-web/userdashboard") {
            this.mainbreadcrumb = element.translation;
          }
          if (element.menuType == 5 && element.routerLink == "eta-web/adduser") {
            this.addButton = true;
            this.addButtonText = element.translation;
          }

          if (element.menuType == 5 && element.routerLink == "eta-web/edituser") {
            this.editButton = true;
            this.editButtonText = element.translation;
          }

          if (element.menuType == 5 && element.routerLink == "eta-web/editassignrole") {
            this.assignRoleButton = true;
            this.assignRoleButtonText = element.translation;
          }
        })

      })
  }
  /**
   *To set Router link Menu List
   *
   * @param {*} routerlink
   * @memberof UserDashboardComponent
   */
  shownRouterLink(routerlink) {
    console.log(routerlink);

    this.r.navigate(["/" + routerlink]);
  }



  activeLink = "userdashboard";
  background = '';

  toggleBackground() {
    this.background = this.background ? '' : 'primary';
  }
  /*   links = ['First', 'Second', 'Third'];
    activeLink = 'roledashboard';
    background = ''; */
}
