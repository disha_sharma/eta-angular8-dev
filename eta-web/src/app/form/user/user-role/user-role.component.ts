import { NgModel } from '@angular/forms';
import { Location } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ServiceService } from './../../../eta-services/service.service';
@Component({
  selector: 'app-user-role',
  templateUrl: './user-role.component.html',
  styleUrls: ['./user-role.component.css'],
  providers: [ServiceService]
})
export class UserRoleComponent implements OnInit {

  /** *   For Sub Breadcrumb  **/
  public breadcrumb: String;
  /*** For Main Breadcrumb **/
  public mainbreadcrumb: string;
  /** Read Role URL Form Service TS */
  private Role_URL: any;
  /** Role Grid Data Url */
  public roleurl: any;
  /** Role Grid Column List */
  public roleCol: any;
  /** Get Selected Role Id */
  public roleId: any;
  /** Edit Case Role id */
  public editroleId: any;
  /** Get org Id */
  public get_OrgId: any;
  /** Privlege Multiselect Property  */
  private text = "Select Privilege";
  /** Privlege Multiselect Property  */
  public id = 'privilegeID';
  /** Privlege Multiselect Property  */
  public name = 'privilege';
  /** Privlege Multiselect Property  */
  public dropdownList: any = [];
  /** Privlege Multiselect Property  */
  public selectedItems: any = [];
  /** Privlege Multiselect Property  */
  public dropdownSettings = {};

  /** Role Type Value */
  public roleTypeSelectValue: any;
  /** Privilege DropDown Check */
  public privilegeDropShow: any;
  /** Edit case RoleType  */
  public editroleType: any;
  /** Store Role to send Role Translation */
  public storeRoleId: any;
  /** Role Translation Grid Check */
  public isDataLoaded: any;
  public column: any;
  /** Org Language List */
  public tdheader: any = {};
  /**  Role Translation Grid Data Url */
  public roleTranslationUrl: any;
  /** Role Translation Grid Column List */
  public roleTranslationCol: any = [];

  /** same string is use in user role dt for validating the services */
  public Role = "Role"
  /** Get Privilege By OrgId and privilege Type Url  */
  private PrivUrl: any

  public isSpinnerShow: boolean = false;
  public userRoleInnerDataUrl: any;
  public defaultColumnToSort: any;

  constructor(private _formBuilder: FormBuilder, private newService: ServiceService, private route: ActivatedRoute, private http: HttpClient, private location: Location) {

    this.getHeaderMenuList();
    this.defaultColumnToSort = "roleId";
    this.Role_URL = this.newService.Role_URL;
    this.roleurl = this.Role_URL + '/getAllRoleInnerGrid';
    this.roleTranslationUrl = this.newService.User_URL + "/getRoleTranslationGrid";
    this.userRoleInnerDataUrl = this.Role_URL + '/getUserRoleAngularInnnerGrid';
    this.route.params.subscribe(params => {
      this.roleId = +params['roleId'];
      this.editRoles(this.roleId);
    });

    this.getAllRoleTypeList();
  }

  roleForm: FormGroup;
  formobj: Object = {};
  roleTypeList: any = {};

  ngOnInit() {
    this.getUserRoleColumnList();
    //this.getSpdFormJsonSchema();
    this.roleForm = this._formBuilder.group({
      roleName: [],
      roleDiscription: [],
      roleType: [],
      privilegeMultiselect: []

    })

    this.newService.getUserRoleAngularInnerGridData()
      .subscribe(data => {
        // console.log(data);

      })
  }


  getAllRoleTypeList() {
    this.newService.getAllRoleType()
      .subscribe(data => {
        this.roleTypeList = data;
      })

  }

  onSubmit = function (roleObj) {

    let privi = [];
    if (this.roleForm.valid ) {
      roleObj.privilegeMultiselect.forEach(element => {
        let obj: any = {};
        obj['prPrivilegeId'] = element;
        privi.push(obj);
      });

    }

    let userObj: any = {};
    let roldIdreturn = null;
    userObj['roleId'] = this.editroleId;
    userObj['roleName'] = roleObj.roleName;
    userObj['roleDescription'] = roleObj.roleDiscription;
    userObj['roleTypeId'] = this.roleTypeSelectValue;
    userObj['orgId'] = this.get_OrgId;
    userObj['privilegeMultiSelect'] = privi;
    this.roleCol = null;

    console.log(userObj);
    if (this.roleForm.valid) {
      this.isSpinnerShow = true;
      this.newService.postRole(userObj)
        .subscribe(data => {
          this.isSpinnerShow = false;
          let newdata = data; 
          this.storeRoleId = data.roleId;
          this.selectedItems = null;
          if (newdata.roleId == null) {
            alert("data not saved")
          } else {
            alert("Your role has been created successfully !! ")
            this.getUserRoleColumnList();
          }
        },error=>{
          this.isSpinnerShow = false;
        })
        
    } else {
      alert("Please Enter All Fields")
      this.getUserRoleColumnList();
    }




  }
  editRoles(roleId) {
    this.newService.getRoleByRoleId(roleId)
      .subscribe(data => {

        console.log(data.multiSelectWapperList);

        this.privilegeDropShow = "show";
        this.get_OrgId = data.orgId;
        this.editroleId = data.roleId;
        this.editroleType = data.roleTypeId;
        this.storeRoleId = data.roleId;
        this.roleTypeSelectValue = data.roleTypeId;
        this.selectedItems = data.multiSelectWapperList
        this.getAllPrivilegeList(this.editroleType);
        let tempList: any = [];
        data.multiSelectWapperList.forEach(element => {
          tempList.push(element.id);
        });
        this.roleForm = this._formBuilder.group({
          roleName: [data.roleName],
          roleDiscription: [data.roleDescription],
          roleType: [data.roleTypeId],
          privilegeMultiselect: [tempList]
        });

        //  this.selectAll(null , this.selectedItems,this.selectedItems);

      });

  }

  private menuList: any = [];
  getHeaderMenuList() {
    this.newService.getHeaderMenu()
      .subscribe(res => {
        this.menuList = res;
        this.menuList.forEach(element => {
          if (element.routerLink == "eta-web/userdashboard") {
            this.mainbreadcrumb = element.translation;
          }
          if (element.routerLink == "eta-web/roledashboard") {
            this.breadcrumb = element.translation;
          }
        })

      })
  }

  roleTypeSelect(event) {
    console.log(event);

    this.privilegeDropShow = null;
    this.roleTypeSelectValue = event;
    this.getAllPrivilegeList(this.roleTypeSelectValue)
  };

  getAllPrivilegeList(roleType) {
    this.PrivUrl = this.Role_URL + '/getAllPrivilegeByOrgIdAndPrivilegeType/' + roleType;
    this.privilegeDropShow = "show";
    this.http.get(this.PrivUrl)
      .subscribe(data => {
        console.log(data);

        this.dropdownList = data;
      });
    this.dropdownSettings = {
      singleSelection: false,
      text: this.text,
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      enableSearchFilter: true,
      onItemSelect: true,
      onItemDeSelect: true,
      classes: "myclass custom-class"
    }

  }

  private getUserRoleColumnList() {
    this.newService.getUserRoleInnerColumn()
      .subscribe(res => {
        this.roleCol = res.coloumnName;
      })
  }
  cancel() {
    localStorage.setItem('backlocation', "roledashboard");
    this.location.back();
  }

  translationColumns() {
    if (this.storeRoleId != null) {
      let coloumNames: any;
    //  let myColoumsObj: any;
     let myColoumsObj: any = [];
      this.newService.getRolePrivHeaderLanguageByOrgId()
        .subscribe(data => {
          this.isDataLoaded = true;
          this.tdheader = data;
          this.roleTranslationCol.push({ data: 'role Name' });
          data.forEach(element => {
            element.languageId;
            this.column = { data: element.languageId }
            console.log(this.column);
            myColoumsObj.push(this.column)
           // this.roleTranslationCol.push(this.column);
          });
        })
        this.roleTranslationCol = myColoumsObj; 
        console.log(this.roleTranslationCol);
        
    }
    else {
      alert("Add A Role First ")
    }
  }

  public currentTaskJson: any;
  public data: any = {};

  public getSpdFormJsonSchema() {
    let schemaName = "addRole";
    this.newService.getSpdFormJsonSchema(schemaName)
      .subscribe(res => {
        var json = JSON.parse(res.formschema);
        this.currentTaskJson = json;
      })

  }

  equals(objOne, objTwo) {
    if (typeof objOne !== 'undefined' && typeof objTwo !== 'undefined') {
      return objOne.id === objTwo.id;
    }
  }

  selectAll(list) {
    console.log(list);
    let tempList: any = [];
    list.forEach(element => {
      tempList.push(element.id);
    });


    console.log(this.roleForm.get('privilegeMultiselect'));


    this.roleForm.get('privilegeMultiselect').patchValue(tempList)
  }
  deselectAll() {
    this.roleForm.get('privilegeMultiselect').patchValue([])
  }

}
