import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ServiceService } from './../../../eta-services/service.service'
import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { $, element } from 'protractor';
import { interval } from 'rxjs';


@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.css'],
  providers: [ServiceService]
})

export class UserdetailsComponent implements OnInit {

  /***   For Sub Breadcrumb  **/
  public breadcrumb: String;
  /*** For Main Breadcrumb **/
  public mainbreadcrumb: string;
  /** User Data URL */
  public allUserGridURL = this.newService.User_URL + '/getAllUserInnerGrid';
  /** USER Grid Column List */
  public allUserGridFunctionCol: any;

  public userObj: any = {};
  public userdetailForm: FormGroup;
  public userList: any = [];
  public userIdList: any = [];
  /** to get salutation List  */
  public salutationList: any = [];
  /** to get language List  */
  public languageList: any = [];
  /** to get country List  */
  public countryList: any = [];
  /** user Id Veriable */
  public userId: any;
  /** Language Id */
  public languageId: any;
  /** user On Change in  User List  */
  public userIdOnChange: any;
  public languageIDOnChange: any;
  public languageID: any;
  public checkuserId: any;
  public get_CreatedBy: any;
  public get_IsPasswordResetRequired: any;
  public get_IsTermsConditionRequired: any;
  public get_IsPrivacyPolicyRequired: any;
  public get_IsValidated: any;
  public get_UserName: any;
  public get_OrgId: any;
  public selectedFiles: FileList;
  public currentFileUpload: File;
  public imageName: any;
  public getlogo: any;
  public getCurrentUserId: any;
  public getNewFileName: any;

  public userInnerDataUrl: any;
  public defaultColumnToSort: any;

  public getUserData: any = {};

  public isSpinnerShow: boolean = false;

  public selectedUserInfo: any = {};
  public disableEditButton:boolean =false;

  /**
   *Creates an instance of UserdetailsComponent.
   * @param {FormBuilder} _formBuilder
   * @param {ServiceService} newService
   * @param {ActivatedRoute} route
   * @param {Router} r
   * @param {Location} location
   * @memberof UserdetailsComponent
   */
  constructor(private _formBuilder: FormBuilder, private newService: ServiceService, private route: ActivatedRoute, private r: Router, private location: Location) {
    this.userInnerDataUrl = this.newService.User_URL + '/getUserAngularInnnerGrid'
    this.defaultColumnToSort = "salutation";
    this.getHeaderMenuList();
    this.getAllLanguageName();
    this.getAllSalutation();
    this.getUserType();
    this.getAllCountry();

  }

  ngOnInit() {
    this.getUserDetailsColumnList();
    // this.getSpdFormJsonSchema();
    this.userdetailForm = this._formBuilder.group({
      userid: [],
      salutation: new FormControl('', []),
      firstname: new FormControl('', [Validators.required]),
      lastname: ['', [Validators.required]],
      emailaddress: new FormControl('', [Validators.required, Validators.email]),
      officephone: new FormControl('', [Validators.required]),
      mobilephone: [],
      fax: [],
      usertype: new FormControl('', [Validators.required]),
      prefferedlanguageid: new FormControl('', [Validators.required]),
      countryid: new FormControl('', [Validators.required]),
      directphone: [],
      otherphone: [],
      twitter: [],
      jobtitle: [],
      department: [],
      skype: [],
      remarks: [],
      website: [],
      createdBy: [],
      userPhoto: [],
      userName: ['', [Validators.required]]
    });

    this.newService.getUserAngularInnerGridData()
      .subscribe(data => {
        // console.log(data);

      })

    this.newService.getLoginInfo()
      .subscribe(message => {
        this.selectedUserInfo = message;
        this.route.params.subscribe(params => {
          this.checkuserId = +params['userId'];
          this.selectUserID(this.checkuserId)
        });


      });

  }

  /**
   *To Get All Language List
   *
   * @memberof UserdetailsComponent
   */
  public getAllLanguageName() {
    this.newService.getAllLanguageName()
      .subscribe(data => {
        this.languageList = data;
      });
  }

  /**
   *To get All Country List
   *
   * @memberof UserdetailsComponent
   */
  public getAllCountry() {
    this.newService.getAllCountry()
      .subscribe(data => {
        this.countryList = data;
      });
  }

  /**
   *To get All Salution List
   *
   * @memberof UserdetailsComponent
   */
  public getAllSalutation() {
    this.newService.getAllSalutation()
      .subscribe(data => {
        this.salutationList = data;
      });
  }

  /**
   *To get All User Type List
   *
   * @memberof UserdetailsComponent
   */
  public getUserType() {
    this.newService.getUserType()
      .subscribe(data => {
        this.userList = data;
      });
  }

  private array: any[];
  private selectUserID(change) {
    this.userIdOnChange = change;
    /* Login Info Start Here */
    /* this.newService.getUserByUserId()
    .subscribe(res =>{ */
    /* Making Condition  For Current Org   equal to Selected Org */
    /*  console.log("This Selected User Info is "+this.selectedUserInfo.orgId);
     console.log("current Org you in Login "+ res.orgId); */

    //  alert( this.selectedUserInfo.orgId == res.orgId)

    let readonly = 'true';
    this.newService.getUserByUserId(this.checkuserId)
      .subscribe(data => {
        if (this.selectedUserInfo.orgId != data.orgId) {
          this.get_CreatedBy = data.createdBy
          this.userdetailForm.disable();
          this.disableEditButton=true;
          this.userdetailForm = this._formBuilder.group({
            userid: [data.userId],
            salutation: [data.salutation],
            firstname: [data.firstName],
            lastname: [data.lastName],
            emailaddress: [data.emailAddress],
            officephone: [data.officePhone],
            mobilephone: [data.mobilePhone],
            fax: [data.fax],
            usertype: [data.userType],
            prefferedlanguageid: [data.preferredLanguageID],
            countryid: [data.countryID],
            directphone: [data.directPhone],
            otherphone: [data.otherPhone],
            twitter: [data.twitter],
            jobtitle: [data.jobTitle],
            department: [data.department],
            skype: [data.skype],
            remarks: [data.remarks],
            website: [data.website],
            userPhoto: [],
            userName: new FormControl({ value: data.userName, disabled: true }),
          });
        }
        else {
          this.disableEditButton= false;
          this.getUserData = data;
          /*set values for update start*/
          this.get_CreatedBy = data.createdBy;
          this.get_UserName = data.userName;
          this.get_IsPasswordResetRequired = data.isPasswordResetRequired;
          this.get_IsTermsConditionRequired = data.isTermsConditionRequired;
          this.get_IsPrivacyPolicyRequired = data.isPrivacyPolicyRequired;
          this.get_IsValidated = data.isValidated;
          this.get_OrgId = data.orgId;
          /*set values for update end*/
          this.userdetailForm = this._formBuilder.group({
            userid: [data.userId],
            salutation: [data.salutation],
            firstname: [data.firstName],
            lastname: [data.lastName],
            emailaddress: [data.emailAddress],
            officephone: [data.officePhone],
            mobilephone: [data.mobilePhone],
            fax: [data.fax],
            usertype: [data.userType],
            prefferedlanguageid: [data.preferredLanguageID],
            countryid: [data.countryID],
            directphone: [data.directPhone],
            otherphone: [data.otherPhone],
            twitter: [data.twitter],
            jobtitle: [data.jobTitle],
            department: [data.department],
            skype: [data.skype],
            remarks: [data.remarks],
            website: [data.website],
            userPhoto: [data.profileImage],
            userName: new FormControl({ value: data.userName, disabled: true })
          });
        }
      });
    // })/* Login Info END Here */
  }

  /**
   *To save User Form Information
   *
   * @memberof UserdetailsComponent
   */
  public onSubmit = function (userFormData) {


    let formdata: FormData = new FormData();
    console.log(userFormData);

    formdata.append('salutation', this.userdetailForm.value.salutation);
    formdata.append('firstName', this.userdetailForm.value.firstname);
    formdata.append('lastName', this.userdetailForm.value.lastname);
    formdata.append('emailAddress', this.userdetailForm.value.emailaddress);
    formdata.append('officePhone', this.userdetailForm.value.officephone);
    formdata.append('mobilePhone', this.userdetailForm.value.mobilephone);
    formdata.append('fax', this.userdetailForm.value.fax);
    formdata.append('userType', this.userdetailForm.value.usertype);
    formdata.append('preferredLanguageID', this.userdetailForm.value.prefferedlanguageid);
    formdata.append('countryID', this.userdetailForm.value.countryid);
    formdata.append('directPhone', this.userdetailForm.value.directphone);
    formdata.append('otherPhone', this.userdetailForm.value.otherphone);
    formdata.append('twitter', this.userdetailForm.value.twitter);
    formdata.append('jobTitle', this.userdetailForm.value.jobtitle);
    formdata.append('department', this.userdetailForm.value.department);
    formdata.append('skype', this.userdetailForm.value.skype);
    formdata.append('remarks', this.userdetailForm.value.remarks);
    formdata.append('website', this.userdetailForm.value.website);

    if (this.userdetailForm.value.userName != undefined) {
      formdata.append('userName', this.userdetailForm.value.userName);
    }


    if (this.getUserData.userId != null && this.getUserData.userId != 0) {

      formdata.append('userId', this.getUserData.userId);
      formdata.append('orgId', this.get_OrgId);
      formdata.append('createdBy', this.get_CreatedBy);
      formdata.append('isPasswordResetRequired', this.get_IsPasswordResetRequired);
      formdata.append('isTermsConditionRequired', this.get_IsTermsConditionRequired);
      formdata.append('isPrivacyPolicyRequired', this.get_IsPrivacyPolicyRequired);
      formdata.append('isValidated', this.get_IsValidated);
      formdata.append('userName', this.get_UserName);
      formdata.append('createdBy', this.getUserData.createdBy);
      // formdata.append('createdDate', this.getUserData.createdDate);

    }



    if (this.imageName == undefined && this.imageName == null) {
      formdata.append('profileImage', this.getUserData.profileImage);
    }

    if (this.imageName != undefined && this.imageName != null) {
      formdata.append('profileImage', this.imageName);
      formdata.append('file', this.currentFileUpload);

    }
    console.log(formdata);

    if (this.userdetailForm.valid) {
      this.allUserGridFunctionCol = null;
      this.isSpinnerShow = true;
      this.newService.postUserDetails(formdata)
        .subscribe(data => {
          console.log(data);
          this.isSpinnerShow = false;

          this.getCurrentUserId = data.userId;
          this.getNewFileName = data.profileImage;
          if (this.getCurrentUserId != 0) {
            if (this.getCurrentUserId != null) {
              alert("User Added Successfully");
              this.getUserDetailsColumnList();
            } else {
              alert("User Not Added Successfully");
            }

          }
        }, error => {
          alert("user Created but No email Sent  !!!")
          this.getUserDetailsColumnList();
          this.isSpinnerShow = false;
        })

    } else {
      alert("Please Enter Values !!!");
      this.getUserDetailsColumnList();

    }

  }



  /**
   *To Read file formLocal Storage 
   *
   * @param {*} event
   * @memberof UserdetailsComponent
   */
  public selectFile(event) {
    this.userdetailForm.get('userPhoto').patchValue(event.target.files[0].name)
    this.imageName = event.target.files[0].name;
    this.selectedFiles = event.target.files;
    this.currentFileUpload = this.selectedFiles.item(0);
    console.log("Image name : "+this.imageName);
    console.log("Selected files : "+this.selectedFiles);
  }


  /**
   *Variable to store Image 
   *
   * @type {*}
   * @memberof UserdetailsComponent
   */
  public imageToShow: any = null;

  /**
   *To Create Image from blob
   *
   * @param {Blob} image
   * @memberof UserdetailsComponent
   */
  public createImageFromBlob(image: Blob) {
    let reader = new FileReader();
    reader.addEventListener("load", () => {
      this.imageToShow = reader.result;
    }, false);

    if (image) {
      reader.readAsDataURL(image);
    }
  }

  /**
   *Menu List for Menu
   *
   * @private
   * @type {*}
   * @memberof UserdetailsComponent
   */
  private menuList: any = [];
  /**
   *To Get Header Menu List from Back-end 
   *
   * @private
   * @memberof UserdetailsComponent
   */
  private getHeaderMenuList() {
    this.newService.getHeaderMenu()
      .subscribe(res => {
        this.menuList = res;
        this.menuList.forEach(element => {
          if (element.routerLink == "eta-web/userdashboard") {
            this.breadcrumb = element.translation;
          }
          if (element.routerLink == "eta-web/userdashboard") {
            this.mainbreadcrumb = element.translation;
          }

        })

      })
  }

  /**
   *To Get Grid Column List
   *
   * @private
   * @memberof UserdetailsComponent
   */
  private getUserDetailsColumnList() {
    this.newService.getUserInnerColumn()
      .subscribe(res => {
        this.allUserGridFunctionCol = res.coloumnName;
      })
  }
  /**
   *To Go back to last Page location
   *
   * @memberof UserdetailsComponent
   */
  cancel() {
    localStorage.setItem('backlocation', "userdashboard");
    this.location.back();
  }
  public currentTaskJson: any;
  public data: any = {};

  public getSpdFormJsonSchema() {
    let schemaName = "addUser";
    this.newService.getSpdFormJsonSchema(schemaName)
      .subscribe(res => {
        var json = JSON.parse(res.formschema);
        this.currentTaskJson = json;
      })

  }
  checkEmailAvailability(email) {
    console.log(email.target.value);

    this.newService.checkEmailAvailability(email.target.value)
      .subscribe(res => {
        console.log(res);

      })
  }
  usernameTaken: string;
  showSpinner: boolean = false;
  checkUserNameAvailability(username) {
    this.showSpinner = true;
    let usernameValue: string;


    this.newService.checkUserNameAvailability(username.target.value)
      .subscribe(res => {
        console.log(res);
        if (res == true) {
          // alert("user name is already Taken by someone else please try again !!!")
          this.showSpinner = false;
          usernameValue = "That username is taken. try another.";
          this.usernameTaken = usernameValue;
        }
        if (res == false) {
          this.showSpinner = false;
          usernameValue = "This username is available"
          this.usernameTaken = usernameValue;
        }

      }, error => {
        this.showSpinner = false;
        console.log("I am in error ");
        //this.usernameTaken =usernameValue;
      })
  }
} 
