import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { ServiceService } from './../../../eta-services/service.service';

@Component({
  selector: 'app-privilege-deshboard',
  templateUrl: './privilege-deshboard.component.html',
  styleUrls: ['./privilege-deshboard.component.css']
})
export class PrivilegeDeshboardComponent implements OnInit {

  /***   For Sub Breadcrumb  **/
  public breadcrumb: String;
  /*** For Main Breadcrumb **/
  public mainbreadcrumb: string;
  /** Read User Privilege URL from service TS  */
  public PRIVILEGE_URL: any;
  /**  User Privilege Data Url */
  public privilegeurl: any;
  /**  User Privilege Grid Column List */
  public privilegeCol: any;
  /** Get Select Checkbox Value From Grid  */
  public privilegeObj: any;

  public userPrivilegeUrl: any;

  public defaultColumnToSort: any;
  public isEditAllowed: boolean = false;

  /**
   *Creates an instance of PrivilegeDeshboardComponent.
   * @param {Router} r
   * @param {CookieService} cookieService
   * @param {ServiceService} newService
   * @memberof PrivilegeDeshboardComponent
   */
  constructor(private r: Router, private cookieService: CookieService, private newService: ServiceService) {
    this.PRIVILEGE_URL = this.newService.Role_URL;
    this.privilegeurl = this.PRIVILEGE_URL + '/getAllPrivilegeGrid';
    this.userPrivilegeUrl = this.PRIVILEGE_URL + '/getUserPrivilegeAngularGrid';
    this.getHeaderMenuList();
    this.defaultColumnToSort = "privilege"

  }



  ngOnInit() {
    //this.newService.currentMessage.subscribe(message => this.privilegeObj = message);
    this.newService.refreshEditMessage();
    this.newService.currentMessage.subscribe(message => {
      this.privilegeObj = message
      if (message != 'editFalse') {
        this.isEditAllowed = true;
      }
    });

    this.newService.getUserPrivilegeColumn()
      .subscribe(res => {
        console.log(res);

        this.privilegeCol = res.coloumnName;
      })

    this.newService.getUserPrivilegeAngularGridData()
      .subscribe(data => {
        // console.log(data);

      })
  }

  /**
   *For to add Privilege Page when you click on add  Button
   *
   * @memberof PrivilegeDeshboardComponent
   */
  addPrivilage() {
    this.r.navigate(["/addprivilege"]);
  }
  /**
   *For to Edit Privilege Page when you click on add  Edit
   *
   * @memberof PrivilegeDeshboardComponent
   */
  editPrivilage() {
    this.r.navigate(["/editprivilege/" + this.privilegeObj.privilegeID]);
  }

  menuList: any = [];
  public addButton: boolean;
  public addButtonText: any;
  public editButton: any;
  public editButtonText: any;

  /**
   *Header menu List to display menu
   *
   * @memberof PrivilegeDeshboardComponent
   */
  getHeaderMenuList() {
    this.newService.getHeaderMenu()
      .subscribe(res => {
        this.menuList = res;
        this.menuList.forEach(element => {
          if (element.routerLink == "eta-web/userdashboard") {
            this.mainbreadcrumb = element.translation;
          }
          if (element.routerLink == "eta-web/privilegedashboard") {
            this.breadcrumb = element.translation;
          }
          if (element.menuType == 5 && element.routerLink == "eta-web/addprivilege") {
            this.addButton = true;
            this.addButtonText = element.translation;
          }
          if (element.menuType == 5 && element.routerLink == "eta-web/editprivilege") {
            this.editButton = true;
            this.editButtonText = element.translation;
          }

        })
      })
  }

  /**
   *To set Router link Menu List
   *
   * @param {*} routerlink
   * @memberof PrivilegeDeshboardComponent
   */
  shownRouterLink(routerlink) {
    this.r.navigate(["/" + routerlink]);
  }

  public activeLink = 'privilegedashboard';
  background = '';

  toggleBackground() {
    this.background = this.background ? '' : 'primary';
  }
}
