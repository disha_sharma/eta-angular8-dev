import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrivilegeDeshboardComponent } from './privilege-deshboard.component';

describe('PrivilegeDeshboardComponent', () => {
  let component: PrivilegeDeshboardComponent;
  let fixture: ComponentFixture<PrivilegeDeshboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrivilegeDeshboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrivilegeDeshboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
