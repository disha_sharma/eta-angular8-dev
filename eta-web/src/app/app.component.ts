import { Component, OnInit} from '@angular/core';
import { trigger, state, style, animate, transition } from '@angular/animations';
   
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  animations: [
    trigger('childAnimation', [
      // ...
      state('open', style({
        width: '256px',
        position: 'relative',
        display: 'block',
        float: 'left',

      })),
      state('closed', style({
        width: '71px',
        position: 'relative',
        display: 'block',
        float: 'left',
      })),
      transition('closed => open', [
        animate('0.5s')
      ]),

    ]),
    trigger('navBarAnimation', [
   
      state('big', style({
        width: '100%',
        display: 'flex',
      })),
      state('small', style({
        width: '256px',
        display: 'flex',
      })),
      transition('* => *', [
        animate('1s')
      ]),

    ]),
  ],
})
export class AppComponent implements OnInit {

ngOnInit(){}
   

 
}