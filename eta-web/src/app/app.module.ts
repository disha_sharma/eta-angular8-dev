import { HttpClientModule, HTTP_INTERCEPTORS  } from '@angular/common/http';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { FormsModule, ReactiveFormsModule, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpModule,Http } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { materialModule } from './material';
import { DataTablesModule } from 'angular-datatables';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { CookieService } from 'ngx-cookie-service';
import { AppComponent } from './app.component';
 import { ServiceService } from './eta-services/service.service';
 import { AuthorizeInterceptor } from './interceptor/authorizeinterceptor';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule, MatButtonModule, MatSidenavModule, MatIconModule, MatListModule,MatPaginatorModule, MatInputModule, MatDialogModule, MatSlideToggleModule,   } from '@angular/material';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg';
import "froala-editor/js/froala_editor.pkgd.min.js";
import { NgxEditorModule } from 'ngx-editor'; 
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
 import {TaskServiceService } from '../app/dash-board/task-service.service';
import {Injector} from '@angular/core';
export let InjectorInstance: Injector;
 import { UserConfigServiceService } from './user-role-config/user-config/user-service/user-config-service.service';
import { Ng4LoadingSpinnerModule } from 'ng4-loading-spinner';
import { EmbedVideo } from 'ngx-embed-video';
// import { MaterialDynamicDataTableComponent } from './form/core/material-dynamic-data-table/material-dynamic-data-table.component';
import { RoutingModuleModule } from './routing-module.module';

@NgModule({
  declarations: [
    AppComponent,
    // MaterialDynamicDataTableComponent
  ],
  imports: [
    RoutingModuleModule,
    BrowserModule,
    HttpClientModule,
    HttpModule,
    FormsModule,
    DataTablesModule,
    ReactiveFormsModule,
    MatAutocompleteModule,
    MatInputModule,
    materialModule,
    AngularMultiSelectModule,
    NgMultiSelectDropDownModule.forRoot(),
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    FroalaEditorModule.forRoot(), FroalaViewModule.forRoot(),
    NgxEditorModule,
    BrowserAnimationsModule,
    TooltipModule.forRoot(),
    Ng4LoadingSpinnerModule.forRoot(),
    EmbedVideo.forRoot(),

  ],
  schemas: [NO_ERRORS_SCHEMA],
  providers: [

    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthorizeInterceptor,
      multi: true
    },
    ServiceService, CookieService, TaskServiceService, UserConfigServiceService],
  bootstrap: [AppComponent],
}
)

export class AppModule {

  constructor(private injector: Injector) 
  {
    InjectorInstance = this.injector;
  }
 }
