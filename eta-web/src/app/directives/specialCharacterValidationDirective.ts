import {Directive, ElementRef, HostListener, Input} from '@angular/core';

@Directive({
    selector: '[specialCharacter]'
})

export class SpecialCharacterValidationDirective {

  private regex = {
    formSpecialChar : new RegExp(/^[a-zA-Z0-9/. .,-/(/):/\\|_>]*$/g),
    decimal : new RegExp(/^[a-zA-Z0-9\. .,-_>|/\:()]*$/g)
};

  @Input() alphaNumericWithSpecialForm: boolean;

  constructor(private el: ElementRef) { }


  @HostListener('keypress', ['$event']) onKeyPress(event) {
    return new RegExp(this.regex.formSpecialChar).test(event.key);
  }

  @HostListener('paste', ['$event']) blockPaste(event: KeyboardEvent) {
    this.validateFields(event);
  }

  validateFields(event) {
    setTimeout(() => {

      this.el.nativeElement.value = this.el.nativeElement.value.replace(/[^A-Za-z @]/g, '').replace(/\s/g, '');
      event.preventDefault();

    }, 100)
  }
}