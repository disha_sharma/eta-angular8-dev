import { NgModule } from '@angular/core';
import { MatButtonModule, MatCheckboxModule, MatMenuModule, MatButtonToggleModule, MatDialogModule, MatNativeDateModule} from '@angular/material';
import { MatInputModule } from '@angular/material/input';
//import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatSelectModule } from '@angular/material/select';
import { MatCardModule } from '@angular/material/card';
import { MatDividerModule } from '@angular/material/divider';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatTableModule } from '@angular/material/table';
import { MatRadioModule } from '@angular/material/radio';
import { MatPaginatorModule, MatSortModule } from '@angular/material';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatBadgeModule } from '@angular/material/badge';
import { MatTabsModule } from '@angular/material/tabs';
import { MatChipsModule } from '@angular/material/chips';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatProgressBarModule } from '@angular/material/progress-bar';
//import { BrowserModule }  from '@angular/platform-browser';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatTreeModule} from '@angular/material/tree';
import {CdkTableModule} from '@angular/cdk/table';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { AngularSvgIconModule } from 'angular-svg-icon';
import {MatSnackBarModule} from '@angular/material/snack-bar';


//import { MatFileUploadModule } from 'angular-material-fileupload';


@NgModule({


    imports: [
        MatButtonModule,
        MatCheckboxModule,
        MatInputModule,
        // BrowserAnimationsModule,
        MatSelectModule,
        MatButtonModule,
        MatCardModule,
        MatDividerModule,
        MatToolbarModule,
        MatFormFieldModule,
        MatIconModule,
        MatTableModule,
        MatRadioModule,
        MatPaginatorModule,
        MatSortModule,
        MatProgressSpinnerModule,
        MatDialogModule,
        MatExpansionModule,
        MatMenuModule,
        MatBadgeModule,
        MatTabsModule,
        MatButtonToggleModule,
        MatChipsModule,
        MatTooltipModule,
        MatGridListModule,
        MatProgressBarModule,
        MatDialogModule,
        MatButtonModule,
        // BrowserModule,
        MatSlideToggleModule,
        MatTreeModule,
        MatProgressBarModule,
        CdkTableModule,
        MatDatepickerModule,
        MatNativeDateModule,
        AngularSvgIconModule,
        MatSnackBarModule
    ],
    exports: [
        MatButtonModule,
        MatCheckboxModule,
        MatInputModule,
        // BrowserAnimationsModule,
        MatSelectModule,
        MatButtonModule,
        MatCardModule,
        MatDividerModule,
        MatToolbarModule,
        MatFormFieldModule,
        MatIconModule,
        MatTableModule,
        MatRadioModule,
        MatPaginatorModule,
        MatSortModule,
        MatProgressSpinnerModule,
        MatDialogModule,
        MatExpansionModule,
        MatMenuModule,
        MatBadgeModule,
        MatTabsModule,
        MatButtonToggleModule,
        MatChipsModule,
        MatTooltipModule,
        MatGridListModule,
        MatProgressBarModule,
        MatSlideToggleModule,
        MatTreeModule,
        CdkTableModule,
        // BrowserModule, 
        MatDatepickerModule,
        MatNativeDateModule,
        AngularSvgIconModule,
        MatSnackBarModule
    ],


})
export class materialModule { }