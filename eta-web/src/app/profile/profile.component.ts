import { Component, OnInit, ViewChild } from '@angular/core';
import {FormControl} from '@angular/forms';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ArrayType } from '@angular/compiler/src/output/output_ast';
import { ProfileServiceService } from './profile-service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  form: FormGroup;
  profileData : any;
  userId  : any;
  imageValue: string = "";
  selectedFiles: FileList;
  currentFileUpload: File;
  profileName : any;
  constructor(private fb: FormBuilder,private service : ProfileServiceService, private router : Router) { }

  ngOnInit() {
    this.form = this.fb.group({
     // 'name': ['', Validators.required],
      'userName' : ['', Validators.required],
      'firstName': ['', Validators.required],
      'lastName': ['', Validators.required],
     // 'currentPassword': ['', Validators.required],
     // 'newPassword': ['', Validators.required],
     // 'repeatNewPassword': ['', Validators.required],
      'profileImage' : ''
    })
    this.service.getProfileData().subscribe(response => {
      this.profileData = response;
      this.userId = this.profileData.userId;
      this.imageValue = this.profileData.profileImage;
      this.form.patchValue({
        firstName: this.profileData.firstName,
        lastName: this.profileData.lastName,
        userName: this.profileData.userName,
        profileImage : this.profileData.profileImage
     })
    });

   
  }

  /**This is call on Cancel Button*/
  cancel(){
    this.router.navigate(['eta-web/welcome']);
  }

  saveRecord(value){
    this.service.UpdateData(value,this.userId,this.imageValue,this.profileData.userName).subscribe(response => {
      if(response){
        this.router.navigate(['eta-web/welcome']);
      }
    });
  }

  ImageClick(data,defaultFlag){
    if(defaultFlag){
      this.profileName = data.srcElement.name;
      this.imageValue = this.profileName;
    }else{
      if (data && data.target && data.target.files) {
        this.selectedFiles = data.target.files;
        if (this.selectedFiles && this.selectedFiles.item(0)) {
          const file = this.selectedFiles.item(0)
          const reader = new FileReader();
          reader.readAsDataURL(this.selectedFiles[0]);
          var ext = file.name.substr(file.name.lastIndexOf('.') + 1);
          if (ext.toLowerCase() ==='png'||ext.toLowerCase() ==='jpeg') {
            reader.onload = event => {
              const img = new Image();
              img.src = (event.target as any).result;
              img.onload = () => {
                const elem = document.createElement('canvas');
                elem.width = img.width;
                elem.height = img.height;
                if (elem.width <= 128 && elem.height <= 128 && ext.toLowerCase() ==='png'||ext.toLowerCase() ==='jpeg') {
                  let image;
                  image = this.selectedFiles.item(0);
                  this.service.uploadImage(image, 'userImage', 'profilePic').subscribe(response => {
                    this.imageValue = response.fileName;
                  })
                } else {
                  this.fileNotSupported();
                }
              },
                reader.onerror = error => console.log(error);
            };
          } else {
            this.fileNotSupported();
          }
    
    
        }
      }
    }
    
  }

 
  /*function for make the file not supported*/
  fileNotSupported() {
    this.form.controls['profileImage'].setErrors({ 'invalidFile': true });
    this.form.controls['profileImage'].markAsTouched();
  }

 

}
