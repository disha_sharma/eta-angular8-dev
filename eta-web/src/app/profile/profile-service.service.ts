import { Injectable, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders, HttpRequest, HttpEvent, HttpParams} from '@angular/common/http';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import 'rxjs/add/operator/map';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { ServiceService } from './../eta-services/service.service';
import { Body } from '@angular/http/src/body';
import { ProcessDetails } from './../form/core/process/Process';
//import { formDirectiveProvider } from '@angular/forms/src/directives/reactive_directives/form_group_directive';

@Injectable({
  providedIn: 'root'
})
export class ProfileServiceService {
  adminRole = "admin";
  myRole;
  constructor(private cookieService: CookieService, private r: Router, private httpClient: HttpClient, private service : ServiceService) { 
    this.myRole = cookieService.get('my_role');
    this.checkedSessionToken();
  }

   /* toGetToken */
   public checkedSessionToken(): boolean {
    let istokenExist: boolean = false;
    if (sessionStorage.getItem("AuthToken") != null) {
      istokenExist = true;
    }
    return istokenExist;
  }

  public getProfileData(){
    return this.httpClient.get<any>(this.service.User_URL  + '/getUserById');
  }

  /**This is call for Submit  Data */
  public UpdateData(data,userId,image,userName){
  //  data.userId = userId;
 /* var object = {
    "userId": userId,
    "firstName": data.firstname,
    "lastName": data.lastname,
    "userName": userName,
    "profileImage": image
  }*/
  data.userId = userId;
  data.userName = userName;
  data.profileImage = image;
  console.log('data',data);
//return this.httpClient.post(this.service.User_URL  + '/uploadUserImage', data);
     return this.httpClient.put<any>(this.service.User_URL  + '/updateUserDetails',data);
   }

   uploadImage(file: File, fileType: string, updateFor: string): Observable<any>{
    let formdata:FormData= new FormData();
    formdata.append('file', file);
    formdata.append('file_type', fileType);
    formdata.append('type', updateFor);
     return this.httpClient.post(this.service.User_URL  + '/uploadUserImage', formdata);
   }
}
