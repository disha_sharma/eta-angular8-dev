
import { fakeAsync, ComponentFixture, TestBed } from '@angular/core/testing';

import { MatDataTableComponent } from './mat-data-table.component';

describe('MatDataTableComponent', () => {
  let component: MatDataTableComponent;
  let fixture: ComponentFixture<MatDataTableComponent>;

  beforeEach(fakeAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ MatDataTableComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MatDataTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should compile', () => {
    expect(component).toBeTruthy();
  });
});
