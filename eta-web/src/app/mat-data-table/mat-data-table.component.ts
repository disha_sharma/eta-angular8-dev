import { HttpClient } from '@angular/common/http';
import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { MatPaginator, MatSort, MatDialog } from '@angular/material';
import { merge, Observable, of as observableOf } from 'rxjs';
import { catchError, map, startWith, switchMap } from 'rxjs/operators';
import { ServiceService } from '../eta-services/service.service';
import { PopupTemplateComponent } from '../popup-template/popup-template.component';

@Component({
  selector: 'mat-data-table',
  templateUrl: './mat-data-table.component.html',
  styleUrls: ['./mat-data-table.component.css']
})
export class MatDataTableComponent implements OnInit {
  displayedColumns: any = [];
  restApiData: GridDataSourceHttpDao | null;
  data: any[] = [];
  pageSize: number;
  resultsLength = 0;
  isLoadingResults = true;
  isRateLimitReached = false;
  gridDataDisplay: any = [];
  statusFlagList: any = []
  statusColor: any;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  @Input() myDataUrl: any;
  @Input() defaultSortColumn: any;

  constructor(private http: HttpClient, private newService: ServiceService, public dialog: MatDialog) { }

  ngOnInit() {


    this.pageSize = 4;
    this.restApiData = new GridDataSourceHttpDao(this.http);
    // If the user changes the sort order, reset back to the first page.
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        startWith({}),
        switchMap(() => {
          if (this.paginator.pageSize != undefined) {
            //alert("You are undefined");
            this.pageSize = this.paginator.pageSize;
          }

          this.isLoadingResults = true;
          return this.restApiData!.getDataConnection(
            this.sort.active, this.sort.direction, this.paginator.pageIndex, this.pageSize, this.myDataUrl, this.defaultSortColumn);
        }),
        map(data => {
          // Flip flag to show that loading has finished.
          this.isLoadingResults = false;
          this.isRateLimitReached = false;
          this.resultsLength = data.recordsTotal;
          console.log(data);
          this.gridDataDisplay = data.coloumnName;

          let columndata = [];
          data.coloumnName.forEach(element => {
            columndata.push(element.data);
            this.displayedColumns = columndata;

          });
          this.data = data.data;
          return data.data;
        }),
        catchError(() => {
          this.isLoadingResults = false;
          // Catch if the GitHub API has reached its rate limit. Return empty data.
          this.isRateLimitReached = true;
          return observableOf([]);
        })

      ).subscribe(res => { });


    this.statusFlagList = [{ id: 0, status: 'Active' }, { id: 1, status: 'Deactive' }]
  }

  /**
   *To Get Select value By User To Show in Edit Mode.
   *
   * @param {*} row
   * @memberof MatDataTableComponent
   */
  public checkBoxSelected(row) {
    // alert("you clicked row no "+ row.indexOf)
    console.log(row);
    this.newService.setgridChangeMessage(row);
  }

  openDialog(): void {
    /*  const dialogRef = this.dialog.open(PopupTemplateComponent, {
       width: '80% auto',
       data: "Hello i am testing  Popup" 
     });
 
     dialogRef.afterClosed().subscribe(result => {
       console.log('The dialog was closed'+ result);
       
     }); */
  }

  updateStatus(event,rowValue) {
   /*  console.log(event.value);
    console.log(rowValue.roleId) */
    if (event.value == 0) {
      if (window.confirm('Are sure you want to activate ?')) {

        this.newService.getUserRoleUpdateData(event.value,rowValue.roleId)
        .subscribe(data =>{
          console.log(data);
          this.submitStatusValue(event.value, "activate")
        })
        
      }
    }
    else{
      if (window.confirm('Are sure you want to deactivate ?')) {
        this.newService.getUserRoleUpdateData(event.value,rowValue.roleId)
        .subscribe(data =>{
          console.log(data);
          this.submitStatusValue(event.value, "deactivate")
        })
      }
    }
  }



  private submitStatusValue(value, msg) {

    alert("Sucessfull " + msg)

  }

}




/**
 *An Grid Data that  the data source used to retrieve data for the table.
 *
 * @export
 * @class GridDataSourceHttpDao
 */
export class GridDataSourceHttpDao {
  constructor(private http: HttpClient) { }

  /**
   *Rest Api URL Calling 
   *
   * @param {string} sort
   * @param {string} order
   * @param {number} page
   * @param {number} pageSize
   * @returns {Observable<any>}
   * @memberof GridDataSourceHttpDao
   */
  public getDataConnection(sort: string, order: string, page: number, pageSize: number, url: string, defaultSortColumn: string): Observable<any> {
    if (sort == undefined) {
      order = 'asc';
      sort = defaultSortColumn;
    }
    // alert("Api Calling Again")
    // const href = 'http://localhost:8082/getUserAngularGrid';
    const href = url;
    const requestUrl =
      `${href}/${page}/${pageSize}/${order}/${sort}`

    console.log(requestUrl);



    return this.http.get<any>(requestUrl);
  }


}