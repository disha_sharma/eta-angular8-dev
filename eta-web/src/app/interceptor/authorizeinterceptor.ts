
import {throwError as observableThrowError } from 'rxjs';

//import {map, catchError} from 'rxjs/operators';
//import {Observable} from 'rxjs/Rx';

import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/operators/catchError';


import { HttpEvent } from '@angular/common/http';
import { HttpErrorResponse } from '@angular/common/http';
import { HttpResponse } from '@angular/common/http';
import { HttpClient } from '@angular/common/http';
import { HttpInterceptor } from '@angular/common/http';
import { HttpRequest } from '@angular/common/http';
import { HttpHandler } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';




@Injectable()
export class AuthorizeInterceptor implements HttpInterceptor
{
  

    cachedRequests: Array<HttpRequest<any>> = [];

    refreshedToken:any;
    refToken:any;
    authToken: string;
    expired:boolean;
    selectedOrgId:any;
    constructor( private http: HttpClient, private route: ActivatedRoute,private rourter: Router) 
    {}
  
    private applyCredentials = (req: HttpRequest<any>, token: string,refreshToken:string) => 
    {       
        //alert("this.localStorage.getItem ::: "+localStorage.getItem("EtaSelectOrgId"));
        if(localStorage.getItem("EtaSelectOrgId") ==undefined){
            this.ETA_SELECTED_ORGID='0';
        }
        if(localStorage.getItem("EtaSelectOrgId")!=null && localStorage.getItem("EtaSelectOrgId")!=undefined){
           
                this.ETA_SELECTED_ORGID = localStorage.getItem("EtaSelectOrgId");     
           
            //alert("this.ETA_SELECTED_ORGID ::: "+this.ETA_SELECTED_ORGID);
          }
        let reqHeaders=req.headers.append('Authorization','Bearer_'+token);
        reqHeaders=reqHeaders.append('Refresh_Token','Bearer_'+refreshToken);
        reqHeaders=reqHeaders.append('selectedOrgId','SelectedOrgId_'+this.ETA_SELECTED_ORGID);
        return req.clone({ headers: reqHeaders});
    }
    
   //Gateway for all the requests
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> 
    {
        this.authToken= sessionStorage.getItem("AuthToken");
        this.refToken=sessionStorage.getItem("refreshToken");
        this.selectedOrgId=sessionStorage.getItem("selectedOrgId");
        
        if(this.authToken!=null)
        {
           // alert(this.authToken);
           
            sessionStorage.setItem('selectedOrgId',this.ETA_SELECTED_ORGID);
            this.authToken = this.authToken.replace(/['"]+/g, '');
            this.refToken = this.refToken.replace(/['"]+/g, '');
            const authReq = this.applyCredentials(req, this.authToken,this.refToken);
            return next.handle(authReq).map((event: HttpEvent<any>) => {
                if (event instanceof HttpResponse && ~~(event.status / 100) > 3) {
                  //console.info('HttpResponse::event =', event, ';');
                } else //console.info('event =', event, ';');
                return event;
              })
              .catch((err: any, caught) => {
                if (err instanceof HttpErrorResponse) {
                    let res: HttpResponse<any>;
                if (err.status === 401)
                {
                   this.collectFailedRequest(authReq);
                   sessionStorage.removeItem('AuthToken');
                  
                }


                  else if (err.status === 403) {
                   // console.info('err.error =', err.error, ';');
                  }
                  return Observable.throw(err);
                }
              });
                //intercept the respons error and displace it to the console
                //console.log(error.message);
                // if (err.status === 401)
                // {
                //    this.collectFailedRequest(authReq);
                //     return this.refreshToken()
                //       .map((res) =>
                //        {
                //             sessionStorage.removeItem('AuthToken');
                //             sessionStorage.setItem('AuthToken', (res as any).token);
                //             sessionStorage.setItem('selectedOrgId',this.ETA_SELECTED_ORGID);
                //             return  this.retryFailedRequests()//next.handle(this.applyCredentials(req, (res as any).token,this.refToken));
                //        });
                // }
                // else if (err.status === 500)
                // {
                //     return Observable.name;
                // }
                // else if(err.status === 400){
                //     return Observable.name;
                // }
                
                // //return the error to the method that called it
                // return Observable.throw(err);
        

              
        }
        else
        {
          // console.log(req);
            return next.handle(req);
        }

    }
    public collectFailedRequest ( request ): void {
        this.cachedRequests.push( request );
    }
    public retryFailedRequests () 
    {
        // retry the requests. this method can
        // be called after the token is refreshed
        this.authToken= sessionStorage.getItem("AuthToken");
        this.refToken=sessionStorage.getItem("refreshToken");
        this.cachedRequests.forEach( request => {
        //   console.log(request);
         //this.applyCredentials(request, this.authToken,this.refToken);
         let reqHeaders=request.headers.append('Authorization','Bearer_'+this.authToken);
         reqHeaders=reqHeaders.append('Refresh_Token','Bearer_'+this.refToken);
         return request.clone({ headers: reqHeaders});
           
            //??What to do here
        } );
    }
  
   isTokenExpired(token)
   {
       return this.expired=true;
   }
   
//    public refreshToken()
//    {
//        return this.http.get("http://localhost:8082/refresh");
//    }

   ETA_SELECTED_ORGID : string;
   public getSelectedOrg(){
   if(localStorage.getItem("EtaSelectOrgId")!=null){
       this.ETA_SELECTED_ORGID = localStorage.getItem("EtaSelectOrgId");
      // alert("this.ETA_SELECTED_ORGID ::: "+this.ETA_SELECTED_ORGID);
     }
    }

}