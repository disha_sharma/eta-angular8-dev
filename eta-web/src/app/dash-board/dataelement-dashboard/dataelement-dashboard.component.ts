import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { ServiceService } from './../../eta-services/service.service'

@Component({
  selector: 'app-dataelement-dashboard',
  templateUrl: './dataelement-dashboard.component.html',
  styleUrls: ['./dataelement-dashboard.component.css']
})
export class DataelementDashboardComponent implements OnInit {

  Core_URL: any;
  public shown: string = 'FORMS';

  formUrl: any;
  dataContainerUrl;
  dataObjectUrl;
  dataElementUrl;
  gridFormColumns: any;
  gridDataContainerColumns: any;
  gridDataObjectColumns: any;
  gridDataElementColumns: any;
  public defaultColumnToSort: any;
  public message:string;
  public isEditAllowed: boolean = false;
  constructor(private http: HttpClient, private route: ActivatedRoute,
    private r: Router, private shareServices: ServiceService) {
    this.defaultColumnToSort = "createdDate";
    this.Core_URL = this.shareServices.Core_URL;
    this.dataContainerUrl = this.Core_URL + '/getAllDataContainerByOrgIdGrid';
    this.dataObjectUrl = this.Core_URL + '/getAllDataObjectByOrgIdGrid';
    this.formUrl = this.Core_URL + '/getAllFormsByOrgIdGrid';
    this.dataElementUrl = this.Core_URL + '/getAllDataElementByOrgIdGrid';
    this.getHeaderMenuList();
  }

  ngOnInit() {
    this.shareServices.getCoreDataElementColumn()
      .subscribe(data => {
        this.gridDataElementColumns = data.coloumnName;

      });
    this.shareServices.refreshEditMessage();
    this.shareServices.currentMessage.subscribe(message => {
      this.message = message
      if (message != 'editFalse') {
        this.isEditAllowed = true;
      }
    });

  }

  public addDataElement() {
    this.clearShareObj();
    this.r.navigateByUrl('createform');
  }
  public editDataElement() {
    this.r.navigateByUrl('createform');
  }
  private clearShareObj() {
    this.shareServices.setgridChangeMessage(null);
  }

  public mainbreadcrumb: string;
  public breadcrumb: string;
  public menuList: any = [];
  getHeaderMenuList() {
    this.shareServices.getHeaderMenu()
      .subscribe(res => {
        //console.log(res);
        this.menuList = res;
        this.menuList.forEach(element => {
          if (element.routerLink == "eta-web/dashboardDataObject") {
            this.mainbreadcrumb = element.translation;
          }
          if (element.routerLink == "eta-web/dataelementdashboard") {
            this.breadcrumb = element.translation;
          }
        })

      })
  }

  shownRouterLink(routerlink) {
    this.r.navigate(["/" + routerlink]);
  }

  public activeLink = 'dataelementdashboard';
  background = '';

  toggleBackground() {
    this.background = this.background ? '' : 'primary';
  }
}
