import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DataelementDashboardComponent } from './dataelement-dashboard.component';

describe('DataelementDashboardComponent', () => {
  let component: DataelementDashboardComponent;
  let fixture: ComponentFixture<DataelementDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DataelementDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DataelementDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
