import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validator, Validators } from '@angular/forms';
import { HttpClient, HttpResponse, HttpEventType } from '@angular/common/http';
import { ServiceService } from './../../eta-services/service.service';
import { DashboardServiceService } from '././../dashboard-service.service';
import { FunctionDetails } from './function';
import { FormControl } from '@angular/forms';
import { MatPaginator, MatSort, MatTableDataSource, MatDialog, MatDialogConfig } from '@angular/material';
import { Observable, of } from 'rxjs';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead';
import { mergeMap } from 'rxjs/operators';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
@Component({
  selector: 'app-add-function-dialog',
  templateUrl: './add-function-dialog.component.html',
  styleUrls: ['./add-function-dialog.component.css']
})

export class AddFunctionDialogComponent implements OnInit {
  functionControl = new FormControl();
  functionForm = new FormGroup({
    functionNameControl: this.functionControl
  });


  functionDescription = "";
  dOrder = "";
  functionImage = "";
  //FILE UPLOADS
  selectedFiles: FileList;
  currentFileUpload: File;
  fileUploaded: string = "";
  displayOrderList = <any>[];
  showAddMessage: boolean = false;
  functionName: string = '';
  acronym: string = '';
  userId: string;
  firstName: string;
  typeaheadLoading: boolean;
  typeaheadLoadingFunction: boolean;
  typeaheadLoadingAcronym: boolean;
  typeaheadNoResults: boolean;
  dataSource: Observable<any>;
  dataSourceFunction: Observable<any>;
  dataSourceAcronym: Observable<any>;
  functionList: any[] = [];
  acronymList: any[] = [];
  usersList: any[] = [];

  ownerNameModel: string;


  functionNameRequired: boolean = false;
  acronymRequired: boolean = false;
  descriptionRequired: boolean = false;
  ownerNameRequired: boolean = false;
  duplicateFunctionName: boolean = false;
  fileNotLoaded: boolean = false;
  ownerNameSelection: boolean = false;
  fileValue: string = "";
  progressBarValue = 0;
  fileUploadButton: boolean = false;
  submitButtonSave: boolean = false;
  isLoadingResults: boolean = false;
  constructor(private spinnerService: Ng4LoadingSpinnerService, private http: HttpClient, private route: ActivatedRoute, private router: Router, private shareServices: ServiceService, private dashboardService: DashboardServiceService, public dialog: MatDialog, private formBuilder: FormBuilder, private changeDetectorRefs: ChangeDetectorRef) {
    this.functionFormValidation();
    this.dataSourceFunction = Observable.create((observer: any) => {
      // Runs on every search
      observer.next(this.functionName);
    })
      .pipe(
        mergeMap((token: string) => this.getStatesAsObservableFunction(token))
      );
    this.dataSourceAcronym = Observable.create((observer: any) => {
      // Runs on every search
      observer.next(this.acronym);
    })
      .pipe(
        mergeMap((token: string) => this.getStatesAsObservableAcronym(token))
      );
    this.dataSource = Observable.create((observer: any) => {
      // Runs on every search
      observer.next(this.userId);
    })
      .pipe(
        mergeMap((token: string) => this.getStatesAsObservable(token))
      );
  }
  /**This Function is Use for Validation on Initialization formBuilder */
  functionFormValidation() {
    this.functionForm = this.formBuilder.group({
      functionNameControl: ['', [Validators.required]],
      acronymControl: ['', [Validators.required]],
      decriptionControl: ['', [Validators.required]],
      ownerNameControl: ['', [Validators.required]],
      displayOrderControl: '',
      fileInput: '',
    });
  }

  /**This Function Call When Component Life cycle Start and Initialization*/
  ngOnInit() {
    this.dashboardService.getAllFunctionsDisplayOrderList().subscribe(data => {
      this.displayOrderList = data;
    });
    this.showAddMessage = false;
    /**This will bring all the orgs for type ahead */
    this.dashboardService.getAllUserList().subscribe(data => {
      for (let s of data) {
        this.usersList.push(s);
      }
    });

    /**This will bring all the function names for typeahead */
    this.dashboardService.getAllFunctionsList().subscribe(data => {
      for (let s of data) {
        this.functionList.push(s);
      }
    });

    /**This will bring all the function acronym list for typeahead    */
    this.dashboardService.getAllFunctionsAcronymList().subscribe(data => {
      for (let s of data) {
        this.acronymList.push(s);
      }
    });
  }
  /**This Function use for Get Data of Acronym*/
  getStatesAsObservableAcronym(token: string): Observable<any> {
    const query = new RegExp(token, 'i');
    return of(
      this.acronymList.filter((str: any) => {
        return query.test(str.acronym);
      })
    );
  }
  /**This Function use for Get Data of Function*/
  getStatesAsObservableFunction(token: string): Observable<any> {
    const query = new RegExp(token, 'i');
    return of(
      this.functionList.filter((str: any) => {
        return query.test(str.functionName);
      })
    );
  }
  /**This Function use for Get Data of Owner Name*/
  getStatesAsObservable(token: string): Observable<any> {
    const query = new RegExp(token, 'i');
    return of(
      this.usersList.filter((state: any) => {
        return query.test(state.firstName);
      })
    );
  }
  /**This Function use for Change TypeAhead Loading on Function Name*/
  changeTypeaheadLoadingFunction(e: boolean): void {
    this.typeaheadLoadingFunction = e;
  }
  /**This Function use for Selection Change On Function Name*/
  typeaheadOnSelectFunction(e: TypeaheadMatch): void {
    this.functionName = e.value;
    for (var i = 0; i < this.functionList.length; i++) {
      if (this.functionList[i].functionName.trim() == e.value.trim()) {
        this.functionForm.controls['functionNameControl'].setErrors({ 'nameError': true });
        this.duplicateFunctionName = true;
        return;
      }
    }
  }

  checkDuplicateFuntionName(event){
    this.functionName = event.target.value;
    var value = event.target.value.trim();
    for (var i = 0; i < this.functionList.length; i++) {
      var checkFunction = this.functionList[i].functionName.trim();
      if (checkFunction.toLowerCase() == value.toLowerCase()) {
        this.functionForm.controls['functionNameControl'].setErrors({ 'nameError': true });
        this.duplicateFunctionName = true;
        return;
      }
    }
  }
  /**This Function use for Change TypeAhead Loading on Acronym*/
  changeTypeaheadLoadingAcronym(e: boolean): void {
    this.typeaheadLoadingAcronym = e;
  }
  /**This Function use for Selection Change On Acronym*/
  typeaheadOnSelectAcronym(e: TypeaheadMatch): void {
    this.acronym = e.value;
    for (var i = 0; i < this.acronymList.length; i++) {
      if (this.acronymList[i].acronym.trim() == e.value.trim()) {
        this.functionForm.controls['acronymControl'].setErrors({ 'myError': true });
        return;
      }
    }
  }

  checkDuplicateAcronymName(event){
    this.acronym = event.target.value;
    var value = event.target.value.trim();
    for (var i = 0; i < this.acronymList.length; i++) {
      if (this.acronymList[i].acronym.trim() == event.target.value.trim()) {
        setTimeout(() => {
          this.functionForm.controls['acronymControl'].setErrors({ 'myError': true });
          }, 0);
        return;
      }
    }
  }
  /**This Function use for Change TypeAhead Loading on Owner Name*/
  changeTypeaheadLoading(e: boolean): void {
    this.typeaheadLoading = e;
  }
  /**This Function use for Selection Change On Owner Name*/
  typeaheadOnSelect(e: TypeaheadMatch): void {
    this.firstName = e.value;
  }
  /*
    This function is use for When Owner Name  Validation.
    */
  checkOwnerName(event) {
    var test: boolean = false;
    for (let s of this.usersList) {
      JSON.parse(JSON.stringify(s), (key, value) => {
        if (key === 'firstName') {
          if (event.target.value != null && event.target.value != "") {
            if (value != null && value != "" && value !== event.target.value) {
              if (!test) {
                this.functionForm.controls['ownerNameControl'].setErrors({ 'invalid': true });
                this.ownerNameSelection = true;
                return;
              }
            } else {
              test = true;
              this.ownerNameSelection = false;
              return;
            }
          }
        }
      });
    }
  }
  /**This Function use for Save  Record */
  saveRecord() {
    this.isLoadingResults = true;
    if (this.progressBarValue !== 100 || !(this.fileUploaded = "Uploaded")) {
      this.fileNotLoaded = true;
      return;
    } else {
      this.fileNotLoaded = false;
    }

    if (!this.functionForm.valid) {
      //this.functionForm.reset();
      return;
    } else {
      this.validateAllFormFields(this.functionForm);
    }
    if (this.dOrder == null || this.dOrder == "") {
      this.dOrder = "0";

    }
    for (let s of this.usersList) {
      JSON.parse(JSON.stringify(s), (key, value) => {
        if (key === 'firstName') {
          if (value === this.firstName) {
            JSON.parse(JSON.stringify(s), (key, value) => {
              if (key === 'userId') {
                this.userId = value;
              }
            });
          }
        }
      });
    }

    let functionDetails: FunctionDetails = new FunctionDetails(null, this.userId, this.functionName, this.functionDescription, this.acronym, this.dOrder, this.functionImage, null);
    if (!this.isNumber(functionDetails.function_owner)) {
      alert("This is not correct User please change it");
      return;
    }
    this.submitButtonSave = true;
    this.dashboardService.addNewFunctionDetails(functionDetails).subscribe(response => {
      this.dialog.closeAll();
      this.showAddMessage = true;
      this.dashboardService.addStatus = true;
      this.dashboardService.functionName = response.functionName;
      this.isLoadingResults = true;
    }, error => {
      this.dashboardService.addError = true;
      this.dashboardService.addStatus = false;
      this.dashboardService.addFunctionError = "Error in adding function.";
      this.dialog.closeAll();
      this.showAddMessage = true;
      this.isLoadingResults = true;
    });

  }
  /**This Function use for Validate All Fields*/
  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }
  /**This Function use for Check Value Is Number*/
  isNumber(value: string | number): boolean {
    return !isNaN(Number(value.toString()));
  }

  /**This Function use for Click on Cancel*/
  cancel() {
    this.dashboardService.cancelAdd = false;
  }

  /**File upload */
  hide: boolean = false;

  /*function for upload file*/
  upload() {
    if (this.selectedFiles && this.selectedFiles.item(0)) {
      const file = this.selectedFiles.item(0)
      const reader = new FileReader();
      reader.readAsDataURL(this.selectedFiles[0]);
      var ext = file.name.substr(file.name.lastIndexOf('.') + 1);
      if (ext === 'png') {
        reader.onload = event => {
          const img = new Image();
          img.src = (event.target as any).result;
          img.onload = () => {
            const elem = document.createElement('canvas');
            elem.width = img.width;
            elem.height = img.height;
            if (elem.width <= 128 && elem.height <= 128 && ext === 'png') {
              this.validateFile();
              this.currentFileUpload = this.selectedFiles.item(0);
              this.dashboardService.pushFileToStorage(this.currentFileUpload, "icon", "function").subscribe(response => {
                this.functionImage = response.fileName;
                this.functionForm.patchValue({
                  fileInput: response.fileName
                });
                //this.fileValue = response.fileName;
                this.progressBarValue = 100;
                this.fileUploaded = "Uploaded";
                this.fileUploadButton = true;
              })
            } else {
              this.fileNotSupported();
              this.fileUploadButton = false;
            }
          },
            reader.onerror = error => console.log(error);
        };
      } else {
        this.fileNotSupported();
        this.fileUploadButton = false;
      }


    }

  }
  /*function for successfully validate File*/
  validateFile() {
    this.progressBarValue = 100;
  }
  /*function for make the file not supported*/
  fileNotSupported() {
    this.progressBarValue = 100;
    this.functionForm.controls['fileInput'].setErrors({ 'invalidFile': true });
    this.functionForm.controls['fileInput'].markAsTouched();
  }
  /*
  This function is use for When File Input Change.
  */
  fileInputChanged(event) {
    this.fileUploaded = "";
    this.fileUploadButton = false;
    if (event && event.target && event.target.files) {
      this.selectedFiles = event.target.files;
    }
    this.progressBarValue = 0;
    this.functionForm.controls['fileInput'].markAsTouched();
    this.functionForm.controls['fileInput'].setValue(this.fileValue.split('\\').pop());
  }
  /*
  This function is use for When Name AlphaNumeric Validation.
  */
  checkCharValidationForName(value) {
    //console.log('working');
    
    var str = value.target.value;
    if (str) {
      const matches = str.match(/^[A-Za-z][A-Za-z0-9/.,->_()\\*|:" ]*$/);
      return matches ? null : this.functionForm.controls['functionNameControl'].setErrors({ 'patternError': true });
    }
  }

  // for Group name Validation 
  omitSpecialChar(event) {
    var k;
    k = event.charCode;  //         k = event.keyCode;  (Both can be used)
    return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || k == 40 || k == 41 || k == 58 || k == 92 || k == 124 || k == 95 || k == 62 || (k >= 48 && k <= 57) || (k > 43 && k < 48));
  }
  /*
  This function is use for When Acronym AlphaNumeric Validation.
  */
  checkCharValidationForAcronym(value) {
    var str = value.target.value;
    if (str) {
      const matches = str.match(/^[A-Za-z][A-Za-z0-9 ]*$/);
      return matches ? null : this.functionForm.controls['acronymControl'].setErrors({ 'patternAcronymError': true });
    }
  }

}






