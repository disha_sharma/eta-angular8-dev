export class FunctionDetails {
    function_owner: string;
    functionName: string;
    functionDescription: string;
    acronym: string;
    displayOrder: string;
    functionImage: string;
    functionID: string;
    active: string;
  
    constructor(functionId: string, orgId: string, functionName: string, functionDescription: string, acronym: string, displayOrder: string, functionImage: string, active: string){
        this.function_owner = orgId;
        this.functionID = functionId;
        this.functionName = functionName;
        this.functionDescription = functionDescription;
        this.acronym = acronym;
        this.displayOrder = displayOrder;
        this.functionImage = functionImage;
        this.active = active;
    }  
  }


  export class FunctionHide {
    functionName: string;
    functionID: string;
    hide: boolean;
  
    constructor(functionId: string, functionName: string, hide: boolean){
        this.functionID = functionId;
        this.functionName = functionName;
        this.hide = hide;
    }      
  }

  export class ErrorDetails {
    result: boolean;
    message: string;
    details: string;
  
    constructor(result: boolean, message: string, details: string){
        this.result = result;
        this.message = message;
        this.details = details;
    } 
}