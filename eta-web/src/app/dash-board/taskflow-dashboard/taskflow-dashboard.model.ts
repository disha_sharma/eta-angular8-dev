import { DataSource } from '@angular/cdk/collections';
import { ServiceService } from './../../eta-services/service.service';
import { Observable } from 'rxjs';
import { HttpClient, HttpResponse, HttpHeaderResponse } from '@angular/common/http';
import { TaskServiceService } from '../task-service.service';

export interface TaskFlowDashboardData{
    taskFlowName: string;
    taskFlowTypeName: string;
    taskCount: number;
    status: string;
    since: Date;
    total_count: number;
    taskFlowID: number;
    orgId: number;
    
    processID : number;
    processName : string;
    recordsTotal : number;

}

/* export interface TaskFlowDashboardApi{
    items: TaskFlowDashboardData[];
    total_count: number;
} */

/* export class TaskFlowHttpDao {
    constructor(private shareServices: ServiceService, private http: HttpClient, taskServices: TaskServiceService){}
    /* connect():Observable<TaskFlowDashboard[]>{
      return this.shareServices.getAllTaskFlow();
    }
    disconnect(){} 
    getTaskFlowPaginationDetails(sort: string, order: string, page: number, pageSize: number): Observable<any> {
      console.log("order : "+order);
      console.log("page : "+page);   
      console.log("pageSize : "+pageSize);   
     return this.taskServices.getTaskFlowDataTableDetails(order, page, pageSize);
    }
}
 */
export interface Process{
  processID : number;
  processName : string;
}

export interface TaskFlowCollection {
  data: TaskFlowDashboardData[];
}

