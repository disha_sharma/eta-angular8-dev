import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaskflowDashboardComponent } from './taskflow-dashboard.component';

describe('TaskflowDashboardComponent', () => {
  let component: TaskflowDashboardComponent;
  let fixture: ComponentFixture<TaskflowDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaskflowDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaskflowDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
