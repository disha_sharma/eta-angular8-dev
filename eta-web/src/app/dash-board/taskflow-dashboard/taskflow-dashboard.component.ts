import { Component, OnInit, ViewChild, ElementRef, ChangeDetectorRef, AfterViewInit  } from '@angular/core';
import { HttpClient,HttpResponse, HttpHeaderResponse } from '@angular/common/http';
import { ServiceService } from './../../eta-services/service.service';
import { MatPaginator, MatSort,  MatTableDataSource, MatDialog, MatDialogConfig, Sort } from '@angular/material';
import { merge, Observable, of as observableOf, empty } from 'rxjs';
import { catchError, map, startWith, switchMap } from 'rxjs/operators';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { TaskFlowDashboardData } from './taskflow-dashboard.model';
import { DataSource, SelectionModel } from '@angular/cdk/collections';
import { TaskServiceService } from '../task-service.service';
import { animate, state, style, transition, trigger } from '@angular/animations';
import {PageEvent} from '@angular/material';


@Component({
  selector: 'app-taskflow-dashboard',
  templateUrl: './taskflow-dashboard.component.html',
  styleUrls: ['./taskflow-dashboard.component.css'],
  animations: [
    trigger('detailExpand', [
      state('void', style({ height: '0px', minHeight: '0', visibility: 'hidden' })),
      state('*', style({ height: '*', visibility: 'visible' })),
      transition('void <=> *', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class TaskflowDashboardComponent  implements OnInit, AfterViewInit {
  displayedColumns: string[] = ['taskFlowID', 'taskFlowName', 'taskFlowTypeName', 'taskCount', 'status', 'since'];
 
  public processList: any=[];
  public processId: string = "";
  public allButtonFlag: boolean = true;
  public taskFlowList: any = [];
  
  public mainbreadcrumb: string;
  public breadcrumb: string;
  public menuList: any = [];
  taskName: string;
  
  currentTaskFlowId: string; //done by Shyam joshi

  public defaultColumnToSort: any;

  taskFlowDataSource: MatTableDataSource<TaskFlowDashboardData>;
  pageSize : number;
  resultsLength = 0;
  isLoadingResults = true;
  isRateLimitReached = false;
  taskFlowHttpDao: TaskFlowHttpDao | null;
  data: Object[] = [];
  totalRecords: number;
  httpResponse : number = 0;

  mediaID=[];

  showMessageAlert: string = "";
  taskFlowName: string = "";
  processIdCss: string = "";
  taskFlowIdList: any[] = [];
  currentProcessIdCss: number;
  checkProcessChecked: boolean = false;


  pageEvent: PageEvent;
  pageSizeOptions = "";

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;


    constructor(private shareServices: ServiceService, private router: Router, private activatedRoute:ActivatedRoute, private http: HttpClient,
       private taskServices: TaskServiceService, public dialog: MatDialog, public changeDetectorRefs: ChangeDetectorRef) {
      this.getHeaderMenuList();
   }
   

  ngOnInit(){  

    // this.getMediaDetails();

    this.pageSize = 10;
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
    this.taskFlowHttpDao = new TaskFlowHttpDao(this.http, this.taskServices);
    let processIdFromUrl = this.activatedRoute.snapshot.queryParams['processId'];
    this.processId =  processIdFromUrl;
    if(this.activatedRoute.snapshot.queryParams['action'] != 'cancel'){
        this.showMessageAlert = this.activatedRoute.snapshot.queryParams['action'];
        console.log(this.processId , processIdFromUrl);
        
    }
    if(!this.activatedRoute.snapshot.queryParams['action']){
      this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
      merge(this.sort.sortChange, this.paginator.page)
          .pipe(
            startWith({}), switchMap(() => {
              if (this.paginator.pageSize != undefined) {
                this.pageSize = this.paginator.pageSize;
              }
            this.isLoadingResults = true;
            return this.taskFlowHttpDao!.getTaskFlowPaginationDetails(this.sort.active, this.sort.direction, this.paginator.pageIndex, this.pageSize, this.processId);
          }),
          map(data => {
            this.isLoadingResults = false;
            this.isRateLimitReached = false;
            this.resultsLength = data.recordsTotal;
            return data.data;
          }),
          catchError(( response : HttpHeaderResponse) => {
            if(response.status === 404){
              this.httpResponse = response.status;
            }
            this.isLoadingResults = false;
            this.isRateLimitReached = true;
            return observableOf([]);
          })
        ).subscribe(data => {
          this.httpResponse = 200;
          this.taskFlowDataSource = new MatTableDataSource(data);
          this.changeDetectorRefs.detectChanges();
        });  
    } else if(this.activatedRoute.snapshot.queryParams['action'] === 'submit'){
      this.checkProcessChecked = true;
      this.allButtonFlag = false;
      this.taskFlowName = this.activatedRoute.snapshot.queryParams['taskFlowName'];
      this.processId = processIdFromUrl;
          this.taskServices.getTaskFlowDataTableDetailsByProcessId(this.sort.active, this.sort.direction, this.paginator.pageIndex, this.pageSize, this.processId).subscribe(response => {
            this.resultsLength = response.recordsTotal;
            this.taskFlowDataSource = new MatTableDataSource(response.data);
      });
      this.router.navigate(['eta-web/taskflowdashboard'],{ queryParams: { }});
    } else if(this.activatedRoute.snapshot.queryParams['action'] === 'update'){
      this.checkProcessChecked = true;
      this.allButtonFlag = false;
      this.taskFlowName = this.activatedRoute.snapshot.queryParams['updatedName'];
      this.taskName = this.activatedRoute.snapshot.queryParams['taskName'];
      this.processId = processIdFromUrl;
          this.taskServices.getTaskFlowDataTableDetailsByProcessId(this.sort.active, this.sort.direction, this.paginator.pageIndex, this.pageSize, this.processId).subscribe(response => {
            this.resultsLength = response.recordsTotal;
            this.taskFlowDataSource = new MatTableDataSource(response.data);
        });

      this.router.navigate(['eta-web/taskflowdashboard'],{ queryParams: { }});  //this will clean url after processing.
    } else if(this.activatedRoute.snapshot.queryParams['action'] === 'cancel'){   
          this.checkProcessChecked = true;
          this.allButtonFlag = false;
          this.processId = processIdFromUrl;
             this.taskServices.getTaskFlowDataTableDetailsByProcessId(this.sort.active, this.sort.direction, this.paginator.pageIndex, this.pageSize, this.processId).subscribe(response => {
              this.resultsLength = response.recordsTotal;
              this.taskFlowDataSource = new MatTableDataSource(response.data);
          });
          this.router.navigate(['eta-web/taskflowdashboard'],{ queryParams: { }});  //this will clean url after processing.        
    }

    this.taskServices.getAllProcessNamesList().subscribe(data => {
      this.processList = data;
    
      setTimeout(()=> {
        this.toggleScrollButtonsForTabsSlider();
    }, 2);
     
  });
    
    
  }//end of the method ngOnInit
  
  ngAfterViewInit(){
    setTimeout(()=> {
      this.toggleScrollButtonsForCards();
  }, 0);
  }

  // sortData(sort: Sort) {
    
  //   this.getDefaultColumnToSort(sort);
  // }
  // getDefaultColumnToSort(sort: Sort){
  //   this.defaultColumnToSort = sort.active;
  //   console.log(sort);
  // }
 

  /* Display All Task Flow List via ProcessId */
  public reloadTaskFlowListByProcessId(processId : string){
    this.taskServices.getTaskFlowDataTableDetailsByProcessId(this.sort.active, this.sort.direction, this.paginator.pageIndex, this.pageSize, processId).subscribe(response => {
      this.resultsLength = response.recordsTotal;
      this.taskFlowDataSource = new MatTableDataSource(response.data);
    });
  }


  selection = new SelectionModel(true, []);

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.taskFlowDataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
      this.isAllSelected() ?
      this.selection.clear() :
      this.taskFlowDataSource.data.forEach(row => this.selection.select(row));
  }
  /******************************** Display Breadcrumb Menu ************************************** */
  getHeaderMenuList() {
      this.shareServices.getHeaderMenu()
        .subscribe(res => {
          this.menuList = res;
          console.log('getHeaderMenu',res);
          

          this.menuList.forEach(element => {
            if (element.routerLink == "#2") {
              this.mainbreadcrumb = element.translation;
            }
            if (element.routerLink == "eta-web/taskflowdashboard") {
              this.breadcrumb = element.translation;
            }
          })
        })
  }

  public activeLink = 'taskflowdashboard';
  background = '';

  toggleBackground() {
      this.background = this.background ? '' : 'primary';
  }
  shownRouterLink(routerlink) {
      this.router.navigate(["/" + routerlink]);
  }

  /** Click On All Button + icon will hide, but click on any of process acronym button + icon will display */
  markActiveButton(flagButton, event){
      this.currentTaskFlowId = '';
      document.getElementsByClassName('active-button')[0].classList.remove('active-button');
      event.target.classList.add('active-button');
      if(flagButton == 'all'){
          this.allButtonFlag = true;
      }else{
          this.allButtonFlag = false;
      }
  }
  
  /** Click on process acronym it will fetch processId, Based on processId it will display All TaskFlow*/
  clickProcessName(processId){
    this.removeMessageAlert(); //remove alert message
      this.pageSize = 10;
      this.selection.clear();
      this.checkProcessChecked = true;
      this.taskServices.showActiveCss = processId;
      this.processIdCss=processId;
      this.processId = processId;
      this.paginator.pageIndex = 0;
      this.taskServices.getTaskFlowDataTableDetailsByProcessId(this.sort.active, this.sort.direction, this.paginator.pageIndex, this.pageSize, processId).subscribe(response => {
        this.resultsLength = response.recordsTotal;
        this.taskFlowDataSource = new MatTableDataSource(response.data);
        this.paginator.pageIndex = 0;
      })   
  }

  /** Display All Process Acronym Name in Button Form */
  public getAllProcessList(){
      this.shareServices.getAllProcessList()
      .subscribe(data => {
        this.processList = data;
       
        setTimeout(()=> {
          this.toggleScrollButtonsForTabsSlider();
      }, 2);
       
      });
  }

   /** Click on Add icon To add new Task Flow, this will take you in AddTaskFlow page */
   public addTaskFlow(){
    this.removeMessageAlert(); // remove alert message
      let navigationExtras: NavigationExtras = {
        queryParams: {
            "processId": this.processId,
        }
      };
      this.router.navigate(["eta-web/addtaskflow"], navigationExtras);
  }

  /** Click All Button and Display All Task Flow List */
  public clickAllButton(){
    this.removeMessageAlert(); // remove alert message
    this.processId = undefined;
      this.pageSize = 10;
      this.selection.clear();
      this.checkProcessChecked = false;
      this.paginator.pageIndex = 0;
      this.taskServices.getTaskFlowDataTableDetails(this.sort.active, this.sort.direction, this.paginator.pageIndex, this.pageSize).subscribe(response => {
        this.resultsLength = response.recordsTotal;
        this.taskFlowDataSource = new MatTableDataSource(response.data);
        this.paginator.pageIndex = 0;
      })   
  } 

  public updateCurrentTaskFlowId(taskFlowID) {    
      this.currentTaskFlowId = taskFlowID;
      if(this.taskFlowIdList.length === 0){
        console.log("This is the first id:"+taskFlowID);
        this.taskFlowIdList.push(taskFlowID);      
      }else{  
      console.log("checking if function ID exist on functionList : "+taskFlowID);
          if(this.checkTaskFlowIDExist(this.taskFlowIdList, taskFlowID)){
            console.log("function ID exist on functionList : "+taskFlowID);
            const index = this.taskFlowIdList.findIndex(f => f === taskFlowID); 
            console.log("index of remove item is : "+index);
            this.taskFlowIdList.splice(index, 1); 
          }else{
            console.log("function ID not exist : "+taskFlowID);
              this.taskFlowIdList.push(taskFlowID);
          }
      }

      console.log("length of the task id list : "+this.taskFlowIdList.length);
      console.log("current taskFlowId::: "+this.currentTaskFlowId);  
  }


   public removeMessageAlert(){
        this.showMessageAlert="";
   }

   public checkTaskFlowIDExist(array : any[], testString: string) : boolean {
        console.log("taskFlowId "+testString);
        return array.some(r => r === testString);
    }

    onPaginateChange(event) {
      console.log('event',event);
    //  this.pageSizeOptions = event.pageSizeOptions;
      console.log("page Size index",this.pageSizeOptions);   
 }


/*cards slider code starts */
cards = [
  {heading: '8', description: 'Tasks Flows'},
  {heading: '124', description: 'Tasks'},
  {heading: '36', description: 'Data Containers'},
  {heading: '24', description: 'Forms'},
  {heading: '8', description: 'Privileges'},
  {heading: '9', description: 'Data Containers'},
  {heading: '8', description: 'Tasks Flows'},
  {heading: '124', description: 'Tasks'},
  {heading: '36', description: 'Data Containers'},
  {heading: '24', description: 'Forms'},
  {heading: '8', description: 'Privileges'},
  {heading: '9', description: 'Data Containers'}
]
cardsScrollForwardButtonFlag: boolean;
cardsScrollBackButtonFlag: boolean;
@ViewChild('cardsContainer', { read: ElementRef, static: true }) public cardsContainer: ElementRef<any>;
@ViewChild('cardsScrollableContent', { read: ElementRef, static: true }) public cardsScrollableContent: ElementRef<any>;
onWindowResized(event) {
  this.toggleScrollButtonsForCards();
  this.toggleScrollButtonsForTabsSlider();
}
toggleScrollButtonsForCards(){
  if(this.cardsScrollableContent.nativeElement.clientWidth > this.cardsContainer.nativeElement.clientWidth){
this.cardsScrollForwardButtonFlag = true;
  }
  else{
    this.cardsScrollForwardButtonFlag = false;
    this.cardsScrollBackButtonFlag = false;
  }
}
cardsScrollBackFunction(){
  this.cardsContainer.nativeElement.scrollTo({ left: (this.cardsContainer.nativeElement.scrollLeft -300), behavior: 'smooth' });
  this.cardsScrollForwardButtonFlag = true;
  if(this.cardsContainer.nativeElement.scrollLeft < 301){
       this.cardsScrollBackButtonFlag = false;
     }
  }
  
  cardsScrollForwardButton(){
    this.cardsContainer.nativeElement.scrollWidth;
  this.cardsContainer.nativeElement.scrollTo({ left: (this.cardsContainer.nativeElement.scrollLeft + 300), behavior: 'smooth' });
  this.cardsScrollBackButtonFlag = true;
  if(this.cardsContainer.nativeElement.offsetWidth + this.cardsContainer.nativeElement.scrollLeft > (this.cardsContainer.nativeElement.scrollWidth - 301)){
              this.cardsScrollForwardButtonFlag = false;
           }
  }
  /*cards slider code ends */

//code for tabs scrollable
tabsScrollForwardButtonFlag: boolean;
tabsScrollbackButtonFlag: boolean;
@ViewChild('tabsContainer', { read: ElementRef, static: true }) public tabsContainer: ElementRef<any>;
@ViewChild('tabsScrollableContent', { read: ElementRef, static: true }) public tabsScrollableContent: ElementRef<any>;
toggleScrollButtonsForTabsSlider(){
  if(this.tabsScrollableContent.nativeElement.clientWidth > this.tabsContainer.nativeElement.clientWidth){
this.tabsScrollForwardButtonFlag = true;
  }
  else{
    this.tabsScrollForwardButtonFlag = false;
    this.tabsScrollbackButtonFlag = false;
  }
}
back(){
  this.tabsContainer.nativeElement.scrollTo({ left: (this.tabsContainer.nativeElement.scrollLeft -300), behavior: 'smooth' });
  this.tabsScrollForwardButtonFlag = true;
  if(this.tabsContainer.nativeElement.scrollLeft < 301){
       this.tabsScrollbackButtonFlag = false;
     }
  }
  
  forward(){
    console.log( this.tabsContainer.nativeElement.scrollWidth);
    this.tabsContainer.nativeElement.scrollWidth;
  this.tabsContainer.nativeElement.scrollTo({ left: (this.tabsContainer.nativeElement.scrollLeft + 300), behavior: 'smooth' });
  this.tabsScrollbackButtonFlag = true;
  if(this.tabsContainer.nativeElement.offsetWidth + this.tabsContainer.nativeElement.scrollLeft > (this.tabsContainer.nativeElement.scrollWidth - 301)){
              this.tabsScrollForwardButtonFlag = false;
           }
  }
}//end  of the class 

/** An example database that the data source uses to retrieve data for the table. */
export class TaskFlowHttpDao {
      constructor(private http: HttpClient, private taskServices : TaskServiceService) {}

      /** Grid Pagination For All Task Flow List */
      getTaskFlowPaginationDetails(sort: string, order: string, page: number, pageSize: number, processId : string): Observable<any> {
        console.log("order : "+order);
        console.log("page : "+page);   
        console.log("pageSize : "+pageSize);   
        console.log("Process ID : "+processId);

        if(processId == undefined)
            return this.taskServices.getTaskFlowDataTableDetails(sort, order, page, pageSize);          
        else
            return this.taskServices.getTaskFlowDataTableDetailsByProcessId(sort, order, page, pageSize, processId);
      }

      /** Grid Pagination For All Task Flow List via ProcessID */
      getTaskFlowPaginationDetailsByProcessId(sort: string, order: string, page: number, pageSize: number, processId: string): Observable<any> {
        console.log("order : "+order);
        console.log("page : "+page);   
        console.log("pageSize : "+pageSize);   
        console.log("processId :"+ processId);
        
        return this.taskServices.getTaskFlowDataTableDetailsByProcessId(sort, order, page, pageSize, processId);
      } 

      

     
}
  