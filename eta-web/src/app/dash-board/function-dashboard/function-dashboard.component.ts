import { Component, OnInit,ViewChild,ElementRef, ChangeDetectorRef, AfterViewInit  } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient, HttpResponse, HttpHeaderResponse } from '@angular/common/http';
import { ServiceService } from './../../eta-services/service.service'
import {animate, state, style, transition, trigger} from '@angular/animations';
import {MatPaginator, MatSort, MatTableDataSource, MatDialog, MatDialogConfig} from '@angular/material';
import { AddFunctionDialogComponent } from './../../dash-board/add-function-dialog/add-function-dialog.component';
import { EditFunctionComponent } from '../edit-function/edit-function.component';

import { DashboardServiceService } from '../dashboard-service.service';
import {merge, Observable, of as observableOf} from 'rxjs';
import {catchError, map, startWith, switchMap} from 'rxjs/operators';
import { FunctionDetails, ErrorDetails } from '../add-function-dialog/function';
import { log } from 'util';
import { LabelInfo } from '../../model/label';
import { SelectionModel } from '@angular/cdk/collections';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';


@Component({
  selector: 'app-function-dashboard',
  templateUrl: './function-dashboard.component.html',
  styleUrls: ['./function-dashboard.component.css'],
  animations: [
    trigger('detailExpand', [
      state('void', style({ height: '0px', minHeight: '0', visibility: 'hidden' })),
      state('*', style({ height: '*', visibility: 'visible' })),
      transition('void <=> *', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class FunctionDashboardComponent implements OnInit, AfterViewInit {
  //Core_URL: any;
  //public shown: string = 'Function';
  public mainbreadcrumb: string;
  public breadcrumb: string;
  gridFunctionColumns: any;
  gridProcessCol: any;
  gridTaskColumns: any;
  functionUrl;
  gridFunctionData: any;
  isDataLoadedFunction = false;
  gridProcessData: any;
  isDataLoadedProcess = false;
  processUrl;
  enableFlag: boolean = false;
  public isEditAllowed: boolean = false;
  functionId: string;
  failAdded : boolean = false;
  successAdded : boolean = false;
  successUpdated : boolean = false;
  successHidden : boolean = false;
  successUnHidden : boolean = false;
  successDelete : boolean = false;
  deleteFailValidation : boolean = false;
  functionName :string;
  errorInFunctionAdding : string = "";
  taskUrl;

  panelOpenState = false;
  formUrl: any;
  dataContainerUrl;
  gridFormColumns: any;
  gridDataContainerColumns: any;
  gridDataObjectColumns: any;
  gridDataElementColumns: any; 
  public message:string;

  displayedColumns = ['functionID','dragHandle', 'functionName','createdName','active','createdDate'];
  dataSource: MatTableDataSource<FunctionData>;
  pageSize : number;
  pageSizeOptions = "";
  resultsLength = 0;
  displayOrder: number;
  isLoadingResults = true;
  isRateLimitReached = false;

  labelsList : LabelInfo[] =[];
  value : any;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  functionHttpDao: FunctionHttpDao | null;
  data: Object[] = [];
  totalRecords: number;
  httpResponse : number = 0;
  isExpansionDetailRow = (i: number, row: Object) => row.hasOwnProperty('detailRow');
  expandedElement: any;
  isChecked :boolean = false;
  functionList: any[] = [];
  errorDetails : ErrorDetails[];
  result;
  fullImagePath = "";

  public activeLink = 'dashboard';

  constructor(private spinnerService: Ng4LoadingSpinnerService, private router: Router,private http: HttpClient, private route: ActivatedRoute, private r: Router, private shareServices: ServiceService, private dashboardService: DashboardServiceService, public dialog: MatDialog, private changeDetectorRefs: ChangeDetectorRef) {
    this.getHeaderMenuList();
    console.log("came after route.");
  }

  //STOP PROPEGATON FUNCTION
  stopPropegationFunction(event){
    event.stopPropagation();    
  }
  selection = new SelectionModel(true, []);

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
   // this.removeAllAlerts();
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataSource.data.forEach(row => this.selection.select(row));
  }

  ngOnInit() {
    this.spinnerService.show();
    this.pageSize = 5;
    console.log("came after route.");
    // If the user changes the sort order, reset back to the first page.
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    this.shareServices.refreshEditMessage();

    this.shareServices.currentMessage.subscribe(message => {
      this.message = message
      if (message != 'editFalse') {
        this.isEditAllowed = true;
      }
    });

    this.functionHttpDao = new FunctionHttpDao(this.http, this.dashboardService);

    // If the user changes the sort order, reset back to the first page.
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
    console.log("THIS IS RELOADING THE LIST.");
    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        startWith({}),
        switchMap(() => {
            if (this.paginator.pageSize != undefined) {
               this.pageSize = this.paginator.pageSize;
            } 
          this.isLoadingResults = true;
          return this.functionHttpDao!.getAllFunctionsInfo(this.sort.active, this.sort.direction, this.paginator.pageIndex, this.pageSize);
        }),
        map(data => {
          // Flip flag to show that loading has finished.
          this.isLoadingResults = false;
          this.isRateLimitReached = false;
          this.resultsLength = data.recordsTotal;
          console.log("data length : "+data.recordsTotal);
          return data.data;
        }),
        catchError(( response : HttpHeaderResponse) => {
          if(response.status === 404){
            this.httpResponse = response.status;
          }
          this.resetAll();
          this.isLoadingResults = false;
          // Catch if the GitHub API has reached its rate limit. Return empty data.
          this.isRateLimitReached = true;
          return observableOf([]);
        })
      ).subscribe(data => {
        this.httpResponse = 200;

       this.dataSource = new MatTableDataSource(data);
       this.resetAll();
       this.spinnerService.hide();
      });  
  }
ngAfterViewInit(){
  setTimeout(()=> {
    this.toggleScrollButtonsForCards();
}, 0);
}

   
    onPaginateChange(event) {
      this.pageSizeOptions = event.pageSizeOptions;
      console.log("page Size index : "+this.pageSizeOptions);    
    }


    public resetAll(){
        this.successAdded = false;
        this.successUpdated = false;
        this.enableFlag = false;
    }
      
    public refreshFunctionsList(taskPerformed : string){     

      this.spinnerService.show();
      if(taskPerformed == "start")
          this.paginator.pageIndex = 0;
       this.dashboardService.getFunctionDataTableDetails("displayOrder", "asc", 0, this.pageSize).subscribe(response=> {
          console.log("Response on refresh : "+response);
          console.log("Total records : "+response.recordsTotal);
          this.resultsLength = response.recordsTotal;
          this.dataSource = new MatTableDataSource(response.data);
          this.spinnerService.hide();
        })
        this.changeDetectorRefs.detectChanges();
    } 


  /**
   * 
   * @param jsonStr 
   * @param keyToFind 
   */
  public getParsedValue(jsonStr : string, keyToFind : string) : any{
    console.log(jsonStr);
    console.log("keyToFind : "+keyToFind);
    JSON.parse(jsonStr, (key, value) => {
      console.log("key : "+key + "  value : "+value);
      if (key.trim() === keyToFind.trim()) {
        return value;
      }
      return value;
    });
  }//end of the method

  public clearShareObj() {
    this.shareServices.setgridChangeMessage(null);
  }

  removeSuccess(){
    console.log("removeSuccess");    
    this.successAdded = false;
    this.refreshFunctionsList("start");       
    this.functionList.splice(0,this.functionList.length);
  }

  removeUpdate(){
    console.log("removeUpdate");
    this.successUpdated = false;
    this.refreshFunctionsList("samepage");       
    this.functionList.splice(0,this.functionList.length);
  }


removeAllAlerts(){
  this.removeSuccess();
  this.removeAddError();
  this.removeUpdate();
  this.removeHiddenMsg();
  this.removeUnHiddenMsg();
  this.removeDeleteMsg();
  this.removeFunctionDeleteValidationMsg();
}

  menuList: any = [];
  getHeaderMenuList() {
    this.shareServices.getHeaderMenu()
      .subscribe(res => {
        this.menuList = res;
        this.menuList.forEach(element => {
          if (element.routerLink == "#2") {
            this.mainbreadcrumb = element.translation;
          }
          if (element.routerLink == "eta-web/functionDashBoard") {
            this.breadcrumb = element.translation;
          }
        })

      })
  }

  shownRouterLink(routerlink) {
    console.log('routerlink');
    this.r.navigate(["/" + routerlink]);
  }

  //public activeLink = 'home';
  background = '';

  toggleBackground() {
    this.background = this.background ? '' : 'primary';
  }


 setFunctionId(functionId: string, functionName: string){
  console.log("set function value function id : "+functionId);
  console.log("set function value function name : "+functionName);
  console.log("enable flag status : "+this.enableFlag);    

   this.functionId = functionId;
   if(this.functionList.length === 0){
     console.log("This is the first id:"+functionId);
      this.functionList.push(functionId);
   }else{
    console.log("checking if function ID exist on functionList : "+functionId);
        if(this.checkFunctionIDExist(this.functionList, functionId)){
          console.log("function ID exist on functionList : "+functionId);
          const index = this.functionList.findIndex(f => f === functionId); 
          console.log("index of remove item is : "+index);
          this.functionList.splice(index, 1); 
        }else{
          console.log("function ID not exist : "+functionId);
            this.functionList.push(functionId);
        }
   }
   
    console.log("---------------------------------------------------");

    this.functionList.forEach(function (value) {
        console.log("function ID NEW : "+value);
    });

  /*  this.dashboardService.getFunctionDetailsByFunctionId(this.functionId).subscribe(data => {
    this.displayOrder = data.displayOrder;
     console.log("getting display order from edit : "+this.displayOrder);              
  }); */
 if(this.enableFlag === true && this.functionList.length === 0){
    this.enableFlag = false;
    this.functionList.splice(0,this.functionList.length);
  } else if( this.enableFlag === false || this.functionList.length != 0){
    this.enableFlag = true;
  }

  console.log("functionId List : "+this.functionList);
  
 }

 openAddDialog(): void {    
  this.resetAll();
  const dialogRef = this.dialog.open(AddFunctionDialogComponent, {
    disableClose: true
  });

  dialogRef.afterClosed().subscribe(result => {
    this.removeAllAlerts();
    this.selection.clear();
    console.log("dialog closed in edit.");
    //this.resetAll();    
    this.refreshFunctionsList("start");
    this.functionList.splice(0,this.functionList.length);
    console.log("Success add status 1:"+this.successAdded);
    console.log("cancel Add status on close : "+this.dashboardService.cancelAdd);
    if(this.successAdded === true){
      this.successAdded = false;
      console.log("Success add status 2:"+this.successAdded);
    }/* else{
      console.log("cancel Add status on close : "+this.dashboardService.cancelAdd);
      
      if(this.dashboardService.cancelAdd != true){
         this.successAdded = true;
      }
        
      console.log("Success add status 3:"+this.successAdded);
    } */

    if(!this.dashboardService.addError){
      console.log("Cancel add"+this.dashboardService.cancelAdd);
      if(this.dashboardService.cancelAdd === true){
        if(this.dashboardService.addStatus)
          this.successAdded = true;
      }
    }

    if(this.dashboardService.cancelAdd)
    this.dashboardService.addError = false;

    this.dashboardService.addStatus = false;
    console.log("addStatus : "+this.dashboardService.addStatus);

    if(this.dashboardService.addStatus){
        this.successAdded = true;
    }
    //set this false after setting successAdded true
    console.log("Success add status final:"+this.successAdded);
    this.dashboardService.cancelAdd =false;
    this.functionName = this.dashboardService.functionName;
    this.dashboardService.functionName = "";
    this.failAdded = this.dashboardService.addError;
    this.errorInFunctionAdding = this.dashboardService.addFunctionError;
  });
}


 openEditDialog(functionId : string): void{  
  this.resetAll();
  console.log("Opening dialog box for function id : "+functionId);

  /* this.dashboardService.getFunctionDetailsByFunctionId(functionId).subscribe(data => {
     console.log("functionId"+functionId);       
     this.displayOrder = data.displayOrder.trim();  
     console.log("getting display order from edit : "+this.displayOrder);              
  });
 */

  const dialogConfig = new MatDialogConfig();
  dialogConfig.autoFocus = true;
  dialogConfig.disableClose = true;
  dialogConfig.data = {
      functionId: functionId,
      displayOrder: this.displayOrder
  };
    
  const dialogRef = this.dialog.open(EditFunctionComponent, dialogConfig);

  dialogRef.afterClosed().subscribe(result => {
    console.log("dialog closed in edit.");
    this.resetAll();     
    
    console.log("Success update status:"+this.successUpdated);

    if(this.successUpdated === true){
      this.successUpdated = false;
      console.log("2Success update status:"+this.successUpdated);
    }else{     
      if(this.dashboardService.updateStatus === true){
        this.successUpdated = true;
      console.log("3Success update status:"+this.successUpdated);
      }else{
        this.successUpdated = false;
        console.log("4Success update status:"+this.successUpdated);
      }        
    }
    this.functionName = this.dashboardService.functionName;
    this.refreshFunctionsList("start");
    this.functionList.splice(0,this.functionList.length);
    this.dashboardService.functionName = "";
  });
} 

deleteFunction(){
  this.removeAllAlerts();
  for(let i = 0; i < this.selection.selected.length; i++){
    this.functionList.push(this.selection.selected[i].functionID);
  }
  this.dashboardService.delete(this.functionList).subscribe(data => {
    this.errorDetails = data.result;
    this.selection.clear();
    if(data.message === '7001')
      this.deleteFailValidation = true;
    else
      this.successDelete = true;
    this.refreshFunctionsList("start");       
    this.functionList.splice(0,this.functionList.length);
    
  });
}



removeAddError(){
  console.log("remove hidden message.");
  this.failAdded = false;
  this.dashboardService.addFunctionError = "";
  this.errorInFunctionAdding = "";
}

removeFunctionDeleteValidationMsg(){
  console.log("remove removeFunctionDeleteValidationMsg message.");
  this.deleteFailValidation = false;   
  this.resetAll(); 
  this.functionList.splice(0,this.functionList.length);
  this.refreshFunctionsList("start");       
  this.functionList.splice(0,this.functionList.length);
}

removeHiddenMsg(){
    console.log("remove hidden message.");
    this.successHidden = false;   
    this.resetAll(); 
    this.functionList.splice(0,this.functionList.length);
    this.refreshFunctionsList("samepage");       
    this.functionList.splice(0,this.functionList.length);
}

removeUnHiddenMsg(){
  console.log("remove unhidden message.");
  this.successUnHidden = false; 
  this.resetAll();  
  this.functionList.splice(0,this.functionList.length);  
  this.refreshFunctionsList("samepage");       
  this.functionList.splice(0,this.functionList.length);
}

removeDeleteMsg(){
  console.log("remove delete message.");
  this.successDelete = false; 
  this.resetAll();  
  this.functionList.splice(0,this.functionList.length);  
  this.refreshFunctionsList("start");       
  this.functionList.splice(0,this.functionList.length);
}

openHide(){
        this.removeAllAlerts();
        for(let i = 0; i < this.selection.selected.length; i++){
          this.functionList.push(this.selection.selected[i].functionID);
        }
        this.dashboardService.hide(this.functionList).subscribe(data => {
          this.successHidden = true;         
          this.selection.clear();      
          this.functionList.splice(0,this.functionList.length);
        });
        this.refreshFunctionsList("samepage"); 
         
  }

openUnHide(){
      this.removeAllAlerts();
      console.log("List of functions for Delete : ",this.selection.selected);
      for(let i = 0; i < this.selection.selected.length; i++){
        this.functionList.push(this.selection.selected[i].functionID);
      }
      
      console.log("List of IDs to hide : "+this.functionList);
      this.dashboardService.unhide(this.functionList).subscribe(data => {
        this.successUnHidden = true;        
        this.selection.clear();
        this.functionList.splice(0,this.functionList.length);                  
      });
      this.refreshFunctionsList("samepage"); 
        
}



removeitem(array : FunctionDetails[], testString: string) : FunctionDetails [] {
  this.removeAllAlerts();
  console.log("Remove function ID : "+testString);  
  const index = array.findIndex(f => f.functionID === testString); 
  console.log("index of remove item is : "+index);
  array.splice(index, 1); 
  console.log("array size after remove : "+array.length);
  return array;
}


checkStringExist(array : FunctionDetails[], testString: string) : boolean {
  console.log("FunctionID "+testString);
  return array.some(r => r.functionID === testString);
}

checkFunctionIDExist(array : any[], testString: string) : boolean {
  console.log("FunctionID "+testString);
  return array.some(r => r === testString);
}
/*cards slider code starts */
cards = [
  {heading: '3', description: 'Functions'},
  {heading: '12', description: 'Processes'},
  {heading: '15.8k', description: 'Transactions'},
  {heading: '$4M', description: 'Value of Txs.'},
  {heading: 'SAP', description: 'Most Used'},
  {heading: '2', description: 'Processes'},
  {heading: '3', description: 'Transaction2'},
  {heading: '3', description: 'Functions'},
  {heading: '12', description: 'Processes'},
  {heading: '15.8k', description: 'Transactions'},
  {heading: '$4M', description: 'Value of Txs.'},
  {heading: 'SAP', description: 'Most Used'},
  {heading: '2', description: 'Processes'},
  {heading: '3', description: 'Transaction2'},
]

cardsScrollForwardButtonFlag: boolean;
cardsScrollBackButtonFlag: boolean;
@ViewChild('cardsContainer', { read: ElementRef, static: true }) public cardsContainer: ElementRef<any>;
@ViewChild('cardsScrollableContent', { read: ElementRef, static: true }) public cardsScrollableContent: ElementRef<any>;
onWindowResized(event) {
  this.toggleScrollButtonsForCards();
}
toggleScrollButtonsForCards(){
  if(this.cardsScrollableContent.nativeElement.clientWidth > this.cardsContainer.nativeElement.clientWidth){
this.cardsScrollForwardButtonFlag = true;
  }
  else{
    this.cardsScrollForwardButtonFlag = false;
    this.cardsScrollBackButtonFlag = false;
  }
}
cardsScrollBackFunction(){
  this.cardsContainer.nativeElement.scrollTo({ left: (this.cardsContainer.nativeElement.scrollLeft -300), behavior: 'smooth' });
  this.cardsScrollForwardButtonFlag = true;
  if(this.cardsContainer.nativeElement.scrollLeft < 301){
       this.cardsScrollBackButtonFlag = false;
     }
  }
  
  cardsScrollForwardButton(){
    this.cardsContainer.nativeElement.scrollWidth;
  this.cardsContainer.nativeElement.scrollTo({ left: (this.cardsContainer.nativeElement.scrollLeft + 300), behavior: 'smooth' });
  this.cardsScrollBackButtonFlag = true;
  if(this.cardsContainer.nativeElement.offsetWidth + this.cardsContainer.nativeElement.scrollLeft > (this.cardsContainer.nativeElement.scrollWidth - 301)){
              this.cardsScrollForwardButtonFlag = false;
           }
  }

  /*cards slider code ends */
}//end of the class 



export interface FunctionData {
  organizationId : string;
  functionID: string;
  functionImage: string;
  functionIdDis: string;
  functionName: string;
  functionDescription : string;
  createdName : string;
  activeYN : string;
  createdDate : string;
  modifiedBy : string;
  modifiedName : string;
  total_count: number;

  processID : number;
  processName : string;
  recordsTotal : number;

}


export interface FunctionCollection {
  data: FunctionData[];
}

  export class FunctionHttpDao {
       constructor(private http: HttpClient, private dashboardService : DashboardServiceService) {
  }

  getAllFunctionsInfo(sort: string, order: string, page: number, pageSize: number): Observable<any> {
    console.log("Sort : "+sort);    
    console.log("order : "+order);
    console.log("page : "+page);   
    console.log("pageSize : "+pageSize);  
    if(order.length === 0)
      order = "asc"; 
   return this.dashboardService.getFunctionDataTableDetails(sort, order, page, pageSize);
  }
}