import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FunctionDashboardComponent } from './function-dashboard.component';

describe('FunctionDashboardComponent', () => {
  let component: FunctionDashboardComponent;
  let fixture: ComponentFixture<FunctionDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FunctionDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FunctionDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
