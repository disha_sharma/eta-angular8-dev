import { Injectable, OnInit } from '@angular/core';
import { Location, JsonPipe } from '@angular/common';
import { HttpClient, HttpHeaders, HttpRequest, HttpEvent, HttpParams} from '@angular/common/http';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import 'rxjs/add/operator/map';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { ServiceService } from './../eta-services/service.service';
import { AddTaskflow } from './../form/core/add-taskflow/add-taskflow';
import { tap, catchError } from 'rxjs/operators';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};



@Injectable({
  providedIn: 'root'
})
export class TaskServiceService {
  adminRole = "admin";
  myRole;
  public isSpinnerShow: boolean = false;
  showActiveCss = "";
  taskFlowName : string ="";
 
  constructor(private location: Location, private cookieService: CookieService, private r: Router, private http: HttpClient, private service : ServiceService) {
    this.myRole = cookieService.get('my_role');
    this.checkedSessionToken();
   }
     /* toGetToken */
  public checkedSessionToken(): boolean {
    let istokenExist: boolean = false;
    if (sessionStorage.getItem("AuthToken") != null) {
      istokenExist = true;
    }
    return istokenExist;
  }

  public saveTask(taskObj : any): Observable<any> {
    return this.http.post(this.service.Core_URL + '/saveTask/', taskObj);
  }

  /**This is Pagination Call for TaskFlow details */
  public getTaskFlowDataTableDetails(sort: string, order: string, page : number, pageSize : number): Observable<any> {
    console.log("sort : "+sort);
    console.log('hello');
    
    console.log("order : "+order);
    console.log("page : "+page);
    console.log("size : "+pageSize);
    let gridValue = '{"draw":1,"columns":[{"data":"check","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"dataContainerName","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"dataContainerDescription","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"createdName","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"modifiedName","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"createdDate","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}}],"order":[{"column":0,"dir":"asc"}],"start":0,"length":4,"search":{"value":"","regex":false},"transactionId":7}';
    const body = JSON.stringify({
      "page": page,
      "size" : pageSize,
      "direction": order,
      "columnName" : this.getSort2(sort),
      observe: 'response'
    });
    return this.http.post<any>(this.service.Core_URL  + '/getAllTaskFlowInfo', body, httpOptions);
  }  

  /**This is Pagination Call for Task Flow details by ProcessId */
  public getTaskFlowDataTableDetailsByProcessId(sort: string, order: string, page : number, pageSize : number, processId: string): Observable<any> {
    console.log("sort : "+sort);    
    console.log("order : "+order);
    console.log("page : "+page);
    console.log("size : "+pageSize);
    console.log("processId :"+ processId);
    let gridValue = '{"draw":1,"columns":[{"data":"check","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"dataContainerName","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"dataContainerDescription","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"createdName","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"modifiedName","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"createdDate","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}}],"order":[{"column":0,"dir":"asc"}],"start":0,"length":4,"search":{"value":"","regex":false},"transactionId":7}';
    const body = JSON.stringify({
      "sort":sort,
      "page": page,
      "size" : pageSize,
      "direction": order,
      "columnName" : this.getSort(sort),
      "processId" : processId,
      observe: 'response'
    });
    return this.http.post<any>(this.service.Core_URL  + '/getAllTaskFlowInfoByProcessId/'+ processId, body, httpOptions);
  }
  getSort(sort){
    console.log('inside get');
    console.log('inside get sort ',sort);
    if(sort == 'created' || sort == 'taskFlowTypeName' || sort == undefined)
    return "taskFlowName"
    else
    return sort
  }
  getSort2(sort){
    console.log('inside get');
    console.log('inside get sort ',sort);
    if(sort == undefined || sort == 'taskFlowTypeName' || sort == 'created')
    return "taskFlowName"
    else
    return sort
  }

  /**************************************** for Update Task Flow details *******************************************************/
  public updateTaskFlowDetails(editTaskFlowDetails: AddTaskflow): Observable<any> {
    return this.http.post(this.service.Core_URL + '/editTaskFlowInfo', editTaskFlowDetails, httpOptions);
  }

  /**************************************** Get All Task Flow Name List For Type Ahead *******************************************/
  public getTaskFlowNameList(){
    return this.http.get<any>(this.service.Core_URL + '/getTaskFlowNameList');
  }

  /************************************************** Refresh Task Flow List *******************************************************/
 /*  public refreshTaskFlowList(){
    this.getTaskFlowDataTableDetails("asc", 0, 5);
    console.log("Refresh the Task Flow list");
  } */

  /**************************************** Get Task Flow Detail By TaskFlowId *********************************************/
  public getTaskFlowDetailsByTaskFlowId(taskFlowId: number): Observable<any> {
    return this.http.get(this.service.Core_URL  + '/getTaskFlowDetailsByTaskFlowId/' + taskFlowId);
  }

  /** Not Used */
  public getTaskFlowByTaskFlowId(taskFlowId: number): Observable<any> {
    return this.http.get(this.service.Core_URL  + '/getTaskFlowByTaskFlowId/' + taskFlowId);
  }

  /*********************************** Retrieve Task Flow List based on TaskFlowId *****************************************/
  public getAllTaskByTaskFlowId(taskFlowId: number) : Observable<any> {
    return this.http.get(this.service.Core_URL + '/getAllTaskByTaskFlowId/' + taskFlowId);
  }

  /********************* Fetching TaskFLow Name To MAtch Duplicate Task Flow Name **************************/
  public getTaskFlowNameByName(taskFlowName: string, orgId : string){
    return this.http.get<any>(this.service.Core_URL  + '/getAllTaskFlowNamesByName/' + taskFlowName + "/" + orgId);
  }

  
  public getAllProcessNamesList(): Observable<any> {
    return this.http.get<any>(this.service.Core_URL  + '/getAllProcessNamesList');
 }
 
 public getAllTasks(): Observable<any> {
  return this.http.get<any>(this.service.Core_URL  + '/getAllTasks');
}

updateTask( entity: any, id: number ): Observable<any>{
  if(id != 0 || id != undefined){
  const url= 'http://192.168.40.157:8080' + '/saveTask/' +id;
  return this.http.put(url,entity)
  .pipe(
    tap(data => console.log('updated' + JSON.stringify(data)) ),
  );

}}


  // public deleteJoinerTaskByTaskId(taskId): Observable<any> {
    
  //   return this.http.post(this.service.Core_URL + '/deleteTask/'+taskId,httpOptions);
    
  // }
  public deleteTask(taskId): Observable<any> {
    return this.http.post(this.service.Core_URL + '/deleteTask/'+taskId,'');
  }
 
  getMediaDetails(){
    return this.http.get(this.service.Core_URL + '/getAllVideoIdByProcessId/' + 1);
  }

}
