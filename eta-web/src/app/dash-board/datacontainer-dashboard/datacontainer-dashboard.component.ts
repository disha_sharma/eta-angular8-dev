import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { ServiceService } from './../../eta-services/service.service'

@Component({
  selector: 'app-datacontainer-dashboard',
  templateUrl: './datacontainer-dashboard.component.html',
  styleUrls: ['./datacontainer-dashboard.component.css']
})
export class DatacontainerDashboardComponent implements OnInit {

  Core_URL: any;
  private shown: string = 'FORMS';

  formUrl: any;
  dataContainerUrl;
  dataObjectUrl;
  dataElementUrl;
  gridFormColumns: any;
  gridDataContainerColumns: any;
  gridDataObjectColumns: any;
  gridDataElementColumns: any;
  public isEditAllowed: boolean = false;
  public message: string;

  public defaultColumnToSort: any;
  constructor(private http: HttpClient, private route: ActivatedRoute,
    private r: Router, private shareServices: ServiceService) {
    this.defaultColumnToSort = "dataContainerName";
    this.Core_URL = this.shareServices.Core_URL;
    this.dataContainerUrl = this.Core_URL + '/getAllDataContainerByOrgIdGrid';
    this.dataObjectUrl = this.Core_URL + '/getAllDataObjectByOrgIdGrid';
    this.formUrl = this.Core_URL + '/getAllFormsByOrgIdGrid';
    this.dataElementUrl = this.Core_URL + '/getAllDataElementByOrgIdGrid';
    this.getHeaderMenuList();
  }

  ngOnInit() {

    this.shareServices.getCoreDataContainerColumn()
      .subscribe(data => {
        this.gridDataContainerColumns = data.coloumnName;

      });
    this.shareServices.refreshEditMessage();
    this.shareServices.currentMessage.subscribe(message => {
      this.message = message
      if (message != 'editFalse') {
        this.isEditAllowed = true;
      }
    });
  }
  public addDataContainer() {
    this.clearShareObj();
    this.r.navigateByUrl('createdatacontainer');
  }
  public editDataContainer() {
    this.r.navigateByUrl('createdatacontainer');
  }
  public clearShareObj() {
    this.shareServices.setgridChangeMessage(null);
  }

  public mainbreadcrumb: string;
  public breadcrumb: string;
  public menuList: any = [];
  getHeaderMenuList() {
    this.shareServices.getHeaderMenu()
      .subscribe(res => {
        this.menuList = res;
        this.menuList.forEach(element => {
          if (element.routerLink == "eta-web/dashboardDataObject") {
            this.mainbreadcrumb = element.translation;
          }
          if (element.routerLink == "eta-web/datacontainerdashboard") {
            this.breadcrumb = element.translation;
          }
        })

      })
  }

  shownRouterLink(routerlink) {
    this.r.navigate(["/" + routerlink]);
  }

  public activeLink = 'datacontainerdashboard';
  background = '';

  toggleBackground() {
    this.background = this.background ? '' : 'primary';
  }
}

