import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DatacontainerDashboardComponent } from './datacontainer-dashboard.component';

describe('DatacontainerDashboardComponent', () => {
  let component: DatacontainerDashboardComponent;
  let fixture: ComponentFixture<DatacontainerDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DatacontainerDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DatacontainerDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
