import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { ServiceService } from './../../eta-services/service.service'
import { MatDialog, MatDialogConfig } from '@angular/material';
import { PopupTemplateComponent } from '../../popup-template/popup-template.component';
import { AddTaskComponent } from '../../popup/add-task/add-task.component';
import {animate, state, style, transition, trigger} from '@angular/animations';

@Component({
  selector: 'app-task-dashboard',
  templateUrl: './task-dashboard.component.html',
  styleUrls: ['./task-dashboard.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0', display: 'none'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ]
})
export class TaskDashboardComponent implements OnInit {

  Core_URL: any;
  public shown: string = 'Function';
  gridFunctionColumns: any;
  gridProcessCol: any;
  gridTaskColumns: any;
  functionUrl;
  gridFunctionData: any;
  isDataLoadedFunction = false;
  gridProcessData: any;
  isDataLoadedProcess = false;
  processUrl;
  taskUrl;
  public isEditAllowed: boolean = false;


  public defaultColumnToSort: any;

  message: string;
  public showResponseMsg: any  = {};
  public taskEditObj:any={};

  @ViewChild('widgetsContent', { read: ElementRef, static: true }) 
  public widgetsContent: ElementRef<any>;

  cards = [

    {heading: 3, discription: 'Functions'},
    {heading: 12, discription: 'Processes'},
    {heading: '15.8K', discription: 'Transactions'},
    {heading: '$4M', discription: 'Value of Txs.'},
    {heading: 3, discription: 'Functions'},
    {heading: 12, discription: 'Processes'},
    {heading: '15.8K', discription: 'Transactions'},
    {heading: '$4M', discription: 'Value of Txs.'},
    {heading: 3, discription: 'Functions'},
    {heading: 12, discription: 'Processes'},
    {heading: '15.8K', discription: 'Transactions'},
    {heading: '$4M', discription: 'Value of Txs.'},
    
  ]
  
  constructor(private http: HttpClient, private route: ActivatedRoute,
    private r: Router, private shareServices: ServiceService, public dialog: MatDialog) {
    this.defaultColumnToSort = "taskName"
    this.getHeaderMenuList();
    this.Core_URL = this.shareServices.Core_URL;
    this.functionUrl = this.Core_URL + '/getAllFunctionsByOrgIdGrid';
    this.processUrl = this.Core_URL + '/getAllProcessByOrgIdGrid';
    this.taskUrl = this.Core_URL + '/getAllTaskByOrgIdGrid';

  }

  ngOnInit() {

    this.shareServices.getCoreTaskColumn()
      .subscribe(data => {
        this.gridTaskColumns = data.coloumnName;
      });
    // this.shareServices.currentMessage.subscribe(message => this.message = message);
    this.shareServices.refreshEditMessage();
    this.shareServices.currentMessage.subscribe(message => {
      this.message = message;
      this.taskEditObj = message;
      
      if (message != 'editFalse') {
        this.isEditAllowed = true;
      }
    });
  }

  public addTask() {
    this.clearShareObj();
    this.r.navigateByUrl('task');
  }
  public editTask() {
    this.r.navigateByUrl('task');
  }
  public clearShareObj() {
    this.shareServices.setgridChangeMessage(null);
  }

  public mainbreadcrumb: string;
  public breadcrumb: string;
  public menuList: any = [];
  getHeaderMenuList() {
    this.shareServices.getHeaderMenu()
      .subscribe(res => {
        this.menuList = res;

        this.menuList.forEach(element => {
          if (element.routerLink == "home") {
            this.mainbreadcrumb = element.translation;
          }
          if (element.routerLink == "eta-web/taskdashboard") {
            this.breadcrumb = element.translation;
          }
        })

      })
  }

  shownRouterLink(routerlink) {
    this.r.navigate(["/" + routerlink]);
  }

  public activeLink = 'taskdashboard';
  background = '';

  toggleBackground() {
    this.background = this.background ? '' : 'primary';
  }


  animal: string;
  name: string;

  openDialog(action): void {

    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    dialogConfig.data = {
      id: this.taskEditObj.taskID,
      action: action,
    };

    const dialogRef = this.dialog.open(AddTaskComponent, dialogConfig);

    dialogRef.afterClosed()
      .subscribe(result => {
        console.log('The dialog was closed' + result);
        console.log(result);
        // Add our fruit
        this.showResponseMsg.messages = result.displayMsg;
        this.showResponseMsg.type = result.cssClass;

      });
  }

  alerts: any[] = [];

  removeAlert() {
    this.showResponseMsg = null;

  }

  cssClass(css: any) {
    if (!css) {
      return;
    }
    // return css class based on alert type
    switch (css) {
      case "Success":
        return 'alert alert-success';
      case "Error":
        return 'alert alert-danger';
    }
  }

  prev(){

    this.widgetsContent.nativeElement.scrollTo({ left: (this.widgetsContent.nativeElement.scrollLeft -100), behavior: 'smooth' });
    
    }
    
    next(){
    
    this.widgetsContent.nativeElement.scrollTo({ left: (this.widgetsContent.nativeElement.scrollLeft + 100), behavior: 'smooth' });
    
    }

}


