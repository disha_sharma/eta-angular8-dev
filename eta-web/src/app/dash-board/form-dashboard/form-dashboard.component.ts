import { Component, OnInit, ViewChild, ElementRef, ChangeDetectorRef } from '@angular/core';
import {MatPaginator, MatSort, MatTableDataSource, MatDialog, MatDialogConfig} from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient,  HttpResponse, HttpHeaderResponse } from '@angular/common/http';
import { ServiceService } from './../../eta-services/service.service'
import { AddFormComponent } from '../../forms/add-form/add-form.component';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { FormServiceService } from '../../forms/form-services/form-service.service';
import {merge, Observable, of as observableOf} from 'rxjs';
import {catchError, map, startWith, switchMap} from 'rxjs/operators';
import { UserFormData } from '../../model/UserFormData';
import { SelectionModel } from '@angular/cdk/collections';
import { LabelInfo } from '../../model/label';



@Component({
  selector: 'app-form-dashboard',
  templateUrl: './form-dashboard.component.html',
  styleUrls: ['./form-dashboard.component.css']
})
export class FormDashboardComponent implements OnInit {

  Core_URL: any;
  public shown: string = 'FORMS';
 
  formUrl: any;
  dataContainerUrl;
  dataObjectUrl;
  dataElementUrl;
  gridFormColumns: any;
  gridDataContainerColumns: any;
  gridDataObjectColumns: any;
  gridDataElementColumns: any;
  public isEditAllowed: boolean = false;
  public allEnableDisable : boolean = true;
  public message:string;
  processList: any[] = [];
  formIdsList : any[] = [];
  labelsList : LabelInfo[] =[];
  value : any;
  processName : string;
  processId : string;
  addFormStatus : boolean = false;
  updateFormStatus : boolean = false;
  formName : string;
  formId : number;  
  displayedColumns: string[] = ['formID', 'formName', 'formType', 'formFieldCount', 'active', 'modifiedDate'];
  formDataSource: MatTableDataSource<UserFormData>;
  pageSize : number;
  //for selection
  selection = new SelectionModel(true, []);
  pageSizeOptions = "";
  resultsLength = 0;
  displayOrder: number;
  isLoadingResults = true;
  isRateLimitReached = false;
  httpResponse : number = 0;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  formHttpDao: FormHttpDao | null;

  public defaultColumnToSort: any;
  public mainbreadcrumb: string;
  public breadcrumb: string;
  public menuList: any = [];

 
  constructor(private http: HttpClient, private route: ActivatedRoute, private activatedRoute:ActivatedRoute, private r: Router, private shareServices: ServiceService, public dialog: MatDialog, private formService: FormServiceService) {
        this.defaultColumnToSort = "formName";
        this.Core_URL = this.shareServices.Core_URL;
        this.dataContainerUrl = this.Core_URL + '/getAllInnerFunctionsByOrgIdAndGrid';
        this.getHeaderMenuList();
        //this.formService.getProcessAcronymWithId();
        if(this.formService.processId != null)
          this.processId = this.formService.processId;
        
          if(this.processId == null || this.processId.length < 1){
          //this.processId = '0';
          console.log("First Initialization or process id is not available.");
          
          this.allEnableDisable = true;
        }else if(this.formService.processId != null){
            //document.getElementsByClassName('all-button')[0].classList.remove('active-button');
            this.allEnableDisable = false;
            this.processId = this.formService.processId;
            console.log("Came back to form dashboard : "+this.formService.processId);
            console.log("Local Process ID : "+this.processId);
            this.processId = this.formService.processId;
            this.processName = this.formService.processName;
            console.log("After returning from the other component Process Id : "+this.processId);
            console.log("After returning from the other component Process Name : "+this.processName);     
            this.formService.processId = this.processId;
            this.formService.processName = this.processName; 
        } 
      
  }//end of the constructor

  ngOnInit() {
    this.formName = this.activatedRoute.snapshot.queryParams['formName'];
    console.log("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
    
    console.log("Form Name : "+this.formName);

    if (typeof this.formName === "undefined") {
         console.log("Undefined");         
    } else {
         this.addFormStatus = this.formService.addStatus;
         this.updateFormStatus = this.formService.updateStatus;
         this.formService.addStatus = false;
         this.formService.updateStatus = false;
         console.log("Add Form Status : " + this.addFormStatus);
         console.log("Update Form Status : " + this.updateFormStatus);                  
         console.log("Form Name : "+this.formName);         
    }

    console.log("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
        this.pageSize=10;
        console.log("came after route.");
        // If the user changes the sort order, reset back to the first page.
        this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
        this.formHttpDao = new FormHttpDao(this.http, this.formService);
        // If the user changes the sort order, reset back to the first page.
        this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
        console.log("Getting list of forms in paginated list.");
        if(this.formService.processId == null || this.formService.processId.length < 1){
              merge(this.sort.sortChange, this.paginator.page)
              .pipe(
                startWith({}),
                switchMap(() => {
                  this.isLoadingResults = true;
                  if (this.paginator.pageSize != undefined) {
                    this.pageSize = this.paginator.pageSize;
                  } 
                  return this.formHttpDao!.getFormsDetails(this.sort.active, this.sort.direction, this.paginator.pageIndex, this.pageSize, parseInt(this.processId));
                }),
                map(data => {
                  // Flip flag to show that loading has finished.
                  this.isLoadingResults = false;
                  this.isRateLimitReached = false;
                  this.resultsLength = data.recordsTotal;
                  console.log("data length : "+data.recordsTotal);
                  return data.data;
                }),
                catchError(( response : HttpHeaderResponse) => {
                console.log("Response code :"+response.status);
                  if(response.status === 404){
                    this.httpResponse = response.status;
                  }

                  this.isLoadingResults = false;
                  // Catch if the GitHub API has reached its rate limit. Return empty data.
                  this.isRateLimitReached = true;
                  return observableOf([]);
                })
              ).subscribe(data => {
                this.httpResponse = 200;
              this.formDataSource = new MatTableDataSource(data);
              });  

        }else{
              merge(this.sort.sortChange, this.paginator.page)
              .pipe(
                startWith({}),
                switchMap(() => {
                  this.isLoadingResults = true;
                  if (this.paginator.pageSize != undefined) {
                    this.pageSize = this.paginator.pageSize;
                  } 
                  return this.formHttpDao!.getFormsDetails(this.sort.active, this.sort.direction, this.paginator.pageIndex, this.pageSize, parseInt(this.processId));
                }),
                map(data => {
                  // Flip flag to show that loading has finished.
                  this.isLoadingResults = false;
                  this.isRateLimitReached = false;
                  this.resultsLength = data.recordsTotal;
                  console.log("data length : "+data.recordsTotal);
                  return data.data;
                }),
                catchError(( response : HttpHeaderResponse) => {
                console.log("Response code :"+response.status);
                  if(response.status === 404){
                    this.httpResponse = response.status;
                  }

                  this.isLoadingResults = false;
                  // Catch if the GitHub API has reached its rate limit. Return empty data.
                  this.isRateLimitReached = true;
                  return observableOf([]);
                })
              ).subscribe(data => {
                this.httpResponse = 200;
              this.formDataSource = new MatTableDataSource(data);
              });  
        }
       

   this.getProcessAcronymWithId();
   
  }//end of the method


    public removeSuccess(){
      this.addFormStatus = false;
      this.formService.addStatus = false;
    }

    public removeUpdateSuccess(){
      this.updateFormStatus = false;
      this.formService.updateStatus = false;
    }

    public getValueByKey(key : string) : Observable<any> {
        console.log("Find value from list . Value of : "+ key);
        this.value = '';
        this.labelsList.forEach( obj => {
            console.log("obj name : "+obj.labelName);
            console.log("obj value : "+obj.labelValue);
            
            if(obj.labelName === key ){
                this.value  = obj.labelValue;
                console.log("Value is : "+ this.value);
            }
        })
        return this.value;
      }

    onPaginateChange(event) {
        this.pageSizeOptions = event.pageSizeOptions;
        console.log("page Size index : "+this.pageSizeOptions);    
    }


  
    public getFormsAllInfo() : void {
        this.processName = ''; // make to blank to keep the process name updated
        this.processId = '';
        this.formService.processId = '';
        if(this.pageSize == null || this.pageSize === undefined)
          this.pageSize = 10;
        this.formService.getFormDataTableDetails("asc", 0, this.pageSize).subscribe(response=> {
          console.log("Response on refresh : "+response);
          console.log("Total records : "+response.recordsTotal);
          this.resultsLength = response.recordsTotal;
          this.formDataSource = new MatTableDataSource(response.data);
        })
    }

    public getFormsByProcessID(processID, processName) : void {
        console.log("Process Id : " + processID);
        console.log("Process Name : " + processName);  
        this.processName = '';  
        this.processId = processID;
        this.processName = processName;
        this.formService.getFormDataTableDetailsByProcessId("asc", 0, this.pageSize, processID).subscribe(response=> {
          console.log("Response on refresh : "+response);
          console.log("Total records : "+response.recordsTotal);
          this.resultsLength = response.recordsTotal;
          this.formDataSource = new MatTableDataSource(response.data);
        })
    }

    /**
     * This will get all the process list
     */
    public getProcessAcronymWithId(){
      console.log("getProcessAcronymWithId");
      
        this.formService.getProcessAcronymWithId().subscribe( response =>{
          this.processList = response;
          setTimeout(()=> {
            this.toggleScrollButtonsForTabsSlider();
        }, 0);
          
        })
    }

      public openAddForm() {
        console.log("openAddForm");
        
        this.formService.processName = this.processName;
        this.formService.processId = this.processId;
        console.log("process Name : "+this.processId);
        console.log("proccess Id : "+this.processName);
        
        
        console.log("Process Name : "+this.formService.processName);
        console.log("Process ID : "+this.formService.processId);


        this.r.navigateByUrl('eta-web/addForm');
      }//end of the method

      public openEditForm(formId : number) {
            console.log("open Edit form Id : " + formId);
            this.formService.getFormDetailsByFormId(formId).subscribe(response => {
              console.log("This is the form response by id : ");      
              console.log("Process Acronym : "+response.processAcronym);
              console.log("Form Name"+response.formName);
              console.log("Reusable : "+response.reusable);
              console.log("Process Id :"+response.processId);      
            
              this.processId = response.processId;
              this.processName = response.processAcronym;
              this.formService.formName = response.formName;
              this.formService.formId = formId; 
              this.formService.processId = this.processId;
              this.formService.processName = this.processName;   
              this.r.navigateByUrl('eta-web/editForm');
          });
        
      }//end of the method

      public clearShareObj() {
        this.shareServices.setgridChangeMessage(null);
      }


      public getHeaderMenuList() {
        this.shareServices.getHeaderMenu()
          .subscribe(res => {
            this.menuList = res;
            this.menuList.forEach(element => {
              if (element.routerLink == "#2") {
                this.mainbreadcrumb = element.translation;
              }
              if (element.routerLink == "eta-web/formdashboard") {
                this.breadcrumb = element.translation;
              }
            })

          })
      }


      shownRouterLink(routerlink) {
        this.r.navigate(["/" + routerlink]);
      }

      public activeLink = 'formdashboard';
      background = '';

      toggleBackground() {
        this.background = this.background ? '' : 'primary';
      }


    

      /** Whether the number of selected elements matches the total number of rows. */
      public isAllSelected() {
        const numSelected = this.selection.selected.length;
        const numRows = this.formDataSource.data.length;
        return numSelected === numRows;
      }
    
      /** Selects all rows if they are not all selected; otherwise clear selection. */
      public masterToggle() {
        this.isAllSelected() ?
            this.selection.clear() :
            this.formDataSource.data.forEach(row => this.selection.select(row));
      }

      private deleteForm(): void {
            console.log(this.selection);
            for(let i=0; i<this.selection.selected.length; i++){
              console.log("Selected Form IDs : "+this.selection.selected[i].formID);
              this.formIdsList.push(this.selection.selected[i].formID);      
            }
            console.log("Forms Ids list : "+this.formIdsList);
            
            this.formService.deleteFormById(this.formIdsList).subscribe(response =>{
              console.log("Form are deleted .");    
              if(this.processName == null || this.processName.length == 0){
                  this.getFormsAllInfo();  
              }else{
                  this.getFormsByProcessID(this.processId, this.processName);
              }
            });
            this.selection.clear();
      }

      isExpansionDetailRow = (index, row) => row.hasOwnProperty('detailRow');

      private applyFilter(filterValue: string) {
        filterValue = filterValue.trim(); // Remove whitespace
        filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
        //this.dataSource.filter = filterValue;
      }

      private stopPropagationFunction(event){
        event.stopPropagation();
        console.log('stop propagatin');
      }

      private markActiveButtonForAll(event){
        this.formService.processId = "";
        this.formService.formId = 0;
        this.formService.formName = "";
        this.selection.clear();
        this.formService.all = "0";
        document.getElementsByClassName('active-button')[0].classList.remove('active-button');
        event.target.classList.add('active-button');
      }

      private markActiveButton(event){
        this.selection.clear();
        this.formService.all = "1";
        document.getElementsByClassName('active-button')[0].classList.remove('active-button');
        event.target.classList.add('active-button');
      }

      //code for tabs scrollable
tabsScrollForwardButtonFlag: boolean;
tabsScrollbackButtonFlag: boolean;
@ViewChild('tabsContainer', { read: ElementRef, static: true }) public tabsContainer: ElementRef<any>;
@ViewChild('tabsScrollableContent', { read: ElementRef, static: true }) public tabsScrollableContent: ElementRef<any>;
onWindowResized(event){
  this.toggleScrollButtonsForTabsSlider();
}
toggleScrollButtonsForTabsSlider(){
  if(this.tabsScrollableContent.nativeElement.clientWidth > this.tabsContainer.nativeElement.clientWidth){
this.tabsScrollForwardButtonFlag = true;
  }
  else{
    this.tabsScrollForwardButtonFlag = false;
    this.tabsScrollbackButtonFlag = false;
  }
}
tabsScrollBackFunction(){
  this.tabsContainer.nativeElement.scrollTo({ left: (this.tabsContainer.nativeElement.scrollLeft -300), behavior: 'smooth' });
  this.tabsScrollForwardButtonFlag = true;
  if(this.tabsContainer.nativeElement.scrollLeft < 301){
       this.tabsScrollbackButtonFlag = false;
     }
  }
  
  tabsScrollForwardFunction(){
    console.log( this.tabsContainer.nativeElement.scrollWidth);
    this.tabsContainer.nativeElement.scrollWidth;
  this.tabsContainer.nativeElement.scrollTo({ left: (this.tabsContainer.nativeElement.scrollLeft + 300), behavior: 'smooth' });
  this.tabsScrollbackButtonFlag = true;
  if(this.tabsContainer.nativeElement.offsetWidth + this.tabsContainer.nativeElement.scrollLeft > (this.tabsContainer.nativeElement.scrollWidth - 301)){
              this.tabsScrollForwardButtonFlag = false;
           }
  }
    }//end of the class

  export class FormHttpDao {
        constructor(private http: HttpClient, private formService : FormServiceService) {
         }

      public getFormsDetails(sort: string, order: string, page: number, pageSize: number, processID : number): Observable<any> {
        console.log("processID : "+processID);        
        console.log("order : "+order);
        console.log("page : "+page);   
        console.log("pageSize : "+pageSize);  

        if(order == 'asc' && order.length == 0){
          order = "desc";
        }else{
          if(order.length === 0)
            order = "asc"; 
        }
       
        if(processID > 0)
          return this.formService.getFormDataTableDetailsByProcessId(order, page, pageSize, processID);
        else
          return this.formService.getFormDataTableDetails(order, page, pageSize);
      }//end of the method
  }//end of the class