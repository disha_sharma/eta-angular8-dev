import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DataObjectDashBoardComponent } from './dataobject-dash-board.component';

describe('DataObjectDashBoardComponent', () => {
  let component: DataObjectDashBoardComponent;
  let fixture: ComponentFixture<DataObjectDashBoardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DataObjectDashBoardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DataObjectDashBoardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
