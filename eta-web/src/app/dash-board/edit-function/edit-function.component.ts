import { Component, OnInit, ViewChild, Input, Inject } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource, MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient, HttpResponse, HttpEventType } from '@angular/common/http';
import { ServiceService } from './../../eta-services/service.service';
import { DashboardServiceService } from '../dashboard-service.service';
import { FunctionDetails } from './../add-function-dialog/function';
import { Observable, of } from 'rxjs';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead';
import { mergeMap } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { filter, map, catchError } from 'rxjs/operators';
import { FormGroup, FormBuilder, Validator, Validators, FormControl } from '@angular/forms';

@Component({
  selector: 'app-edit-function',
  templateUrl: './edit-function.component.html',
  styleUrls: ['./edit-function.component.css']
})
export class EditFunctionComponent implements OnInit {
  functionControl = new FormControl();
  functionForm = new FormGroup({
    functionNameControl: this.functionControl
  });
  public shown: string = 'FORMS';
  duplicateFunctionName: boolean = false;
  newValue: string = "";
  /**File upload */
  fileUploadButton: boolean = false;
  fileUploaded: string = "";
  fileValue: string = "";
  functionImage = "";
  selectedFiles: FileList;
  currentFileUpload: File;
  fileInput: string = "";
  hide: boolean = false;
  progressBarValue = 0;
  displayOrderValue: any;
  progress: { percentage: number } = { percentage: 0 }
  wrongImage: boolean = false;
  public defaultColumnToSort: any;
  //two way data binding variables
  organizationId = "";
  functionId = "";
  functionDescription = "";
  displayOrderList = <any>[];
  /**type ahead var */
  functionName: string = "";
  functionNameReal: string = "";
  acronymReal: string = "";
  acronym: string = "";
  userId: string = "";
  firstName: string = "";
  displayOrder: string = "";
  dOrder;
  active: string = "";
  //displayOrder='7';
  typeaheadLoading: boolean;
  typeaheadLoadingFunction: boolean;
  typeaheadLoadingAcronym: boolean;
  typeaheadNoResults: boolean;
  dataSource: Observable<any>;
  dataSourceFunction: Observable<any>;
  dataSourceAcronym: Observable<any>;
  functionList: any[] = [];
  acronymList: any[] = [];
  usersList: any[] = [];
  submitButtonDisabled: boolean = true;
  ownerNameSelection: boolean = false;
  existingFileValue: string = "";

  constructor(private http: HttpClient, private route: ActivatedRoute, private router: Router, private shareServices: ServiceService, private dashboardService: DashboardServiceService, public dialog: MatDialog, @Inject(MAT_DIALOG_DATA) data, private formBuilder: FormBuilder) {
    this.createForm();
    this.defaultColumnToSort = "formName";
    this.organizationId = data.organizationId;
    this.functionId = data.functionId;
    this.dataSourceFunction = Observable.create((observer: any) => {
      // Runs on every search
      observer.next(this.functionName);
    })
      .pipe(
        mergeMap((token: string) => this.getStatesAsObservableFunction(token))
      );


    this.dataSourceAcronym = Observable.create((observer: any) => {
      // Runs on every search
      observer.next(this.acronym);
    })
      .pipe(
        mergeMap((token: string) => this.getStatesAsObservableAcronym(token))
      );


    this.dataSource = Observable.create((observer: any) => {
      // Runs on every search
      observer.next(this.firstName);
    })
      .pipe(
        mergeMap((token: string) => this.getStatesAsObservable(token))
      );

    this.dashboardService.getFunctionDetailsByFunctionId(this.functionId).subscribe(data => {
      this.organizationId = data.organizationId;
      this.functionId = data.functionID;
      this.functionName = data.functionName;
      this.functionNameReal = data.functionName;
      this.functionDescription = data.functionDescription;
      this.acronym = data.acronym;
      this.displayOrder = data.displayOrder;
      this.firstName = data.functionOwnerName;
      this.active = data.active;
      this.acronymReal = data.acronym;
      this.functionImage = data.functionImage;
      this.existingFileValue = data.functionImage;
      this.fileValue = data.functionImage;
      this.functionForm.patchValue({
        fileInput: data.functionImage
      });
      this.fileUploaded = "Uploaded";
      this.functionForm.patchValue({
        dOrderControl: data.displayOrder
      });

    });
  }//end of the constructor   

 

  createForm() {
    this.functionForm = this.formBuilder.group({
      functionNameControl: ['', [Validators.required, Validators.pattern('^[a-zA-Z0-9\. ]*$')]],
      acronymControl: ['', [Validators.required, Validators.pattern('^[a-zA-Z0-9\.]*$')]],
      decriptionControl: ['', [Validators.required]],
      ownerNameControl: ['', [Validators.required]],
      dOrderControl: '',
      fileInput: ''
    });
  }

  ngOnInit() {
    /**This will bring all the orgs for type ahead */
    this.dashboardService.updateStatus = false;
    this.dashboardService.getAllUserList().subscribe(data => {
      for (let s of data) {
        this.usersList.push(s);
      }
    });

    /**This will bring all the function names for typeahead */
    this.dashboardService.getAllFunctionsList().subscribe(data => {
      for (let s of data) {
        this.functionList.push(s);
      }
    });

    /**This will bring all the function acronym list for typeahead    */
    this.dashboardService.getAllFunctionsAcronymList().subscribe(data => {
      for (let s of data) {
        this.acronymList.push(s);
      }
    });

    this.dashboardService.getAllFunctionsDisplayOrderList().subscribe(data => {
      this.displayOrderList = data;
    });

  } 


  compareObjects(o1: any, o2: any): boolean {
    this.newValue = JSON.stringify(o1).replace(/['"]+/g, '');
    return this.newValue === this.displayOrder;
  }

/**This Function use for Update Record */
  updateRecord() {
    if (this.dOrder == null || this.dOrder == "") {
      this.dOrder = "0";
    }
    for (let s of this.usersList) {
      JSON.parse(JSON.stringify(s), (key, value) => {
        if (key === 'firstName') {
          if (value === this.firstName) {
            JSON.parse(JSON.stringify(s), (key, value) => {
              if (key === 'userId') {
                this.userId = value;
              }
            });
          }
        }
      });
    }

    let functionDetails: FunctionDetails = new FunctionDetails(this.functionId, this.userId, this.functionName, this.functionDescription, this.acronym, this.dOrder, this.functionImage, this.active);
    console.log("File Input : "+this.functionImage);
    
    if (functionDetails.function_owner === undefined) {
      alert("This is not correct User please change it");
      return;
    }

    if (!this.isNumber(functionDetails.function_owner)) {
      alert("This is not correct User please change it");
      return;
    }

    this.dashboardService.updateFunctionDetails(functionDetails).subscribe(response => {
      this.dashboardService.updateStatus = true;
      this.dashboardService.functionName = response.functionName;
      this.dialog.closeAll();
    }, error => {
      this.dashboardService.updateStatus = false;
      this.dialog.closeAll();
    });

  } 
 /**This Function use for Check Value Is Number*/
  isNumber(checkVal: string | number): boolean {
    return !isNaN(Number(checkVal.toString()));
  }
   /**This Function use for Get Data of Acronym*/
  getStatesAsObservableAcronym(token: string): Observable<any> {
    const query = new RegExp(token, 'i');
    return of(
      this.acronymList.filter((str: any) => {
        return query.test(str.acronym);
      })
    );
  }

  /**This Function use for Get Data of Function*/
  getStatesAsObservableFunction(token: string): Observable<any> {
    const query = new RegExp(token, 'i');
    return of(
      this.functionList.filter((str: any) => {

        return query.test(str.functionName);
      })
    );
  }
  /**This Function use for Get Data of Owner Name*/
  getStatesAsObservable(token: string): Observable<any> {
    const query = new RegExp(token, 'i');
    return of(
      this.usersList.filter((state: any) => {
        return query.test(state.functionOwnerName);
      })
    );
  }
/**This Function use for Change TypeAhead Loading on Function Name*/
  changeTypeaheadLoadingFunction(e: boolean): void {
    this.typeaheadLoadingFunction = e;
  }
/**This Function use for Selection Change On Function Name*/
  typeaheadOnSelectFunction(e: TypeaheadMatch): void {
    this.functionName = e.value;
    for (let s of this.functionList) {
      JSON.parse(JSON.stringify(s), (key, value) => {
        if (key === 'functionName') {
          if (this.functionName.trim().length === this.functionNameReal.trim().length && this.functionName.trim() === this.functionNameReal.trim()) {
            this.duplicateFunctionName = false;
          } else if (this.functionName.trim().length != this.functionNameReal.trim().length && this.functionName.trim() != this.functionNameReal.trim() && value.trim().length === this.functionName.trim().length && value.trim() === this.functionName.trim()) {
            this.functionForm.controls['functionNameControl'].setErrors({ 'invalid': true });
            this.duplicateFunctionName = true;
          }
        }
      });
    }
  }

  checkDuplicateFuntionName(event){
    this.functionName = event.target.value;
    var funtionname = event.target.value.trim();
    var funtionEdit = this.functionNameReal.trim();
    for (let s of this.functionList) {
      JSON.parse(JSON.stringify(s), (key, value) => {
       // var value = value.trim();
        if (key === 'functionName') {
          if (funtionname.length === funtionEdit.length && funtionname.toLowerCase() === funtionEdit.toLowerCase()) {
            this.duplicateFunctionName = false;
          } else if (funtionname.length != funtionEdit.length && funtionname.toLowerCase() != funtionEdit.toLowerCase() && value.length === funtionname.length && value.toLowerCase() === funtionname.toLowerCase()) {
            this.functionForm.controls['functionNameControl'].setErrors({ 'invalid': true });
            this.duplicateFunctionName = true;
          }
        }
      });
    }
  }
/**This Function use for Change TypeAhead Loading on Acronym*/
  changeTypeaheadLoadingAcronym(e: boolean): void {
    this.typeaheadLoadingAcronym = e;
  }
/**This Function use for Selection Change On Acronym*/
  typeaheadOnSelectAcronym(e: TypeaheadMatch): void {
    this.acronym = e.value;
    for(var i=0;i<this.acronymList.length;i++){
      if(this.acronymList[i].acronym.trim() != this.acronymReal.trim() && this.acronymList[i].acronym.trim() == e.value.trim()){
        this.functionForm.controls['acronymControl'].setErrors({ 'myError': true });
        return;
      }
    }
  }

  checkDuplicateAcronymName(event){
    this.acronym = event.target.value;
    var acronymname = event.target.value.trim();
    var acronymEdit = this.acronymReal.trim();
    for (let s of this.acronymList) {
      JSON.parse(JSON.stringify(s), (key, value) => {
        if (key === 'acronym') {
          if (acronymname != acronymEdit && value.length === acronymname.length && value === acronymname) {
            setTimeout(() => {
              this.functionForm.controls['acronymControl'].setErrors({ 'myError': true });
              }, 0);
              return;
          }
        }
      });
    }
  }

/**This Function use for Change TypeAhead Loading on Owner Name*/
  changeTypeaheadLoading(e: boolean): void {
    this.typeaheadLoading = e;
  }
/**This Function use for Selection Change On Owner Name*/
  typeaheadOnSelect(e: TypeaheadMatch): void {
    this.firstName = e.value;
  }

/**This Function use for Click on Cancel*/
  cancel() {
    this.dashboardService.cancelEdit = true;
  }
/**This Function use for Call On every Field Change Function*/
  public enableSubmitButtonForEdit() {
    console.log("enabling........");
    if(this.existingFileValue != this.fileValue){
      this.submitButtonDisabled = true;	
      return;
    }
    
    this.submitButtonDisabled = false;
  }

  /*function for upload file*/
  upload() {
    if (this.selectedFiles && this.selectedFiles.item(0)) {
      const file = this.selectedFiles.item(0)
      const reader = new FileReader();
      reader.readAsDataURL(this.selectedFiles[0]);
      var ext = file.name.substr(file.name.lastIndexOf('.') + 1);
      if (ext === 'png') {
      reader.onload = event => {
        const img = new Image();
        img.src = (event.target as any).result;
        img.onload = () => {
          const elem = document.createElement('canvas');
          elem.width = img.width;
          elem.height = img.height;
          if (elem.width <= 128 && elem.height <= 128 && ext === 'png') {
            this.validateFile();
            this.currentFileUpload = this.selectedFiles.item(0);
            this.dashboardService.pushFileToStorage(this.currentFileUpload, "icon", "function").subscribe(response => {
             this.enableSubmitButtonForEdit();
              this.functionImage = response.fileName;
              this.functionForm.patchValue({
                fileInput: response.fileName
              });
              this.progressBarValue = 100;
              this.fileUploaded = "Uploaded";
              this.fileUploadButton = true;
              this.submitButtonDisabled = false;
            })
          } else {
            this.fileUploadButton = false;
            this.fileNotSupported();
          }
        },
          reader.onerror = error => console.log(error);
      };
    } else {
      this.fileNotSupported();
      this.fileUploadButton = false;
    }

    }

  }
  /*function for successfully validate File*/ 
  validateFile() {
    this.progressBarValue = 100;
  }
  /*function for make the file not supported*/
  fileNotSupported() {
    this.progressBarValue = 100;
    this.functionForm.controls['fileInput'].setErrors({ 'invalidFile': true });
    this.functionForm.controls['fileInput'].markAsTouched();
  }
  /*
  This function is use for When File Input Change.
  */
  fileInputChanged(event) {
    this.fileUploadButton = false;
    if (event && event.target && event.target.files) {
      this.selectedFiles = event.target.files;
    }
    this.progressBarValue = 0;
    this.functionForm.controls['fileInput'].markAsTouched();
    this.functionForm.controls['fileInput'].setValue(this.fileValue.split('\\').pop());
    if(this.existingFileValue != this.fileValue){
      this.submitButtonDisabled = true;	
    }
  }
  /*
  This function is use for When Name AlphaNumeric Validation.
  */
  checkCharValidationForName(value) {
    var str = value.target.value;
      if (str) {
      const matches = str.match(/^[A-Za-z][A-Za-z0-9/.,->_()\\*|:" ]*$/);
      return matches?null: this.functionForm.controls['functionNameControl'].setErrors({ 'patternError': true });
      }
  }

   // for Group name Validation 
   omitSpecialChar(event) {
    var k;
    k = event.charCode;  //         k = event.keyCode;  (Both can be used)
    return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || k == 40 || k == 41 || k == 58 || k == 92 || k == 124 || k == 95 || k == 62 || (k >= 48 && k <= 57) || (k > 43 && k < 48));
  }
  /*
  This function is use for When Acronym AlphaNumeric Validation.
  */
  checkCharValidationForAcronym(value){
    var str = value.target.value; 
      if (str) {
      const matches = str.match(/^[A-Za-z][A-Za-z0-9 ]*$/);
      return matches?null: this.functionForm.controls['acronymControl'].setErrors({ 'patternAcronymError': true });
      }
  }

 
  /*
  This function is use for When Owner Name  Validation.
  */
  checkOwnerName(event){
    var test : boolean = false;
    for (let s of this.usersList) {
      JSON.parse(JSON.stringify(s), (key, value) => {
        if (key === 'firstName') {
          if(event.target.value !=null && event.target.value != ""){
            if(value != null && value != "" && value !== event.target.value){             
              if(!test){
                this.functionForm.controls['ownerNameControl'].setErrors({ 'invalid': true });
                this.ownerNameSelection = true;  
                return;
              }
            }else {
                test = true;    
                //this.functionForm.controls['ownerNameControl'].setErrors({ 'invalid': false });
                this.ownerNameSelection = false;
                return;            
            }
          }        
        }          
      });
    }
  }

}
