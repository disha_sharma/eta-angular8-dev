import { Injectable, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { HttpClient, HttpHeaders, HttpRequest, HttpEvent, HttpParams} from '@angular/common/http';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import 'rxjs/add/operator/map';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { FunctionDetails } from './add-function-dialog/function';
import { ServiceService } from './../eta-services/service.service';
import { Body } from '@angular/http/src/body';
import { ProcessDetails } from './../form/core/process/Process';
//import { formDirectiveProvider } from '@angular/forms/src/directives/reactive_directives/form_group_directive';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};


@Injectable({
  providedIn: 'root'
})
export class DashboardServiceService implements OnInit {
  adminRole = "admin";
  myRole;

  //status
  updateStatus:boolean = false;
  addStatus:boolean = false;
  cancelAdd : boolean = false;
  cancelEdit : boolean = false;
  functionName:string;
  processName:string;
  addFunctionError : string = "";
  addError : boolean = false;
  addProcessError : string = "";
  
  ngOnInit() {

  }
  
  constructor(private location: Location, private cookieService: CookieService, private r: Router, private httpClient: HttpClient, private service : ServiceService) {

    this.myRole = cookieService.get('my_role');
    this.checkedSessionToken();
  }
  
  /* toGetToken */
  public checkedSessionToken(): boolean {
    let istokenExist: boolean = false;
    if (sessionStorage.getItem("AuthToken") != null) {
      istokenExist = true;
    }
    return istokenExist;
  }


  /**This is call for adding Function details */
  public addNewFunctionDetails(functionDetails: FunctionDetails): Observable<any> {
    this.cancelAdd = true;
    return this.httpClient.post(this.service.Core_URL + '/addNewFunction', functionDetails, httpOptions);
  }

  /**This is call for adding Function details */
  public updateFunctionDetails(functionDetails: FunctionDetails): Observable<any> {
    return this.httpClient.post(this.service.Core_URL + '/editFunction', functionDetails, httpOptions);
  }

  
   /**This is call for hide of Function, processes and tasks */
   public delete(hideList: any[] ): Observable<any> {
    return this.httpClient.get(this.service.Core_URL  + '/delete/' + hideList);
  }


  /**This is call for hide of Function, processes and tasks */
  public deleteProcess(deleteList: any[] ): Observable<any> {
    return this.httpClient.get(this.service.Core_URL  + '/deleteProcess/' + deleteList);
  }

   /**This is call for hide of Function, processes and tasks */
   public hideProcess(hideList: any[] ): Observable<any> {
    return this.httpClient.get(this.service.Core_URL  + '/hideProcess/' + hideList);
  }

   /**This is call for hide of Function, processes and tasks */
   public unhideProcess(unhideList: any[] ): Observable<any> {
    return this.httpClient.get(this.service.Core_URL  + '/unHideProcess/' + unhideList);
  }

  /**This is call for hide of Function, processes and tasks */
  public hide(hideList: any[] ): Observable<any> {
    return this.httpClient.get(this.service.Core_URL  + '/hide/' + hideList);
  }

  /**This is call for unhide of Function, processes and tasks */
  public unhide(hideList: any[] ): Observable<any> {
    return this.httpClient.get(this.service.Core_URL  + '/unhide/' + hideList);
  }

  //FILE UPLOAD

  public pushFileToStorage(file: File, fileType : string, type : string): Observable<any> {
    console.log("Uploading file.");
    
    let formdata: FormData = new FormData();
 
    formdata.append('file', file);
    formdata.append('file_type', fileType);
    formdata.append('type', type);
    console.log("File Name : "+file.name);
    return this.httpClient.post(this.service.Core_URL  + '/uploadFile', formdata);
    //return this.httpClient.post(this.Core_URL + '/uploadFile', formdata, httpOptionsMultipart);
    //return this.httpClient.request(req);
  }


  public uploadFileImageAndDoc(file: File, fileType : string, type : string): Observable<any> {
    console.log("Uploading file.");
    
    let formdata: FormData = new FormData();
 
    formdata.append('file', file);
   // formdata.append('file_type', fileType);
   // formdata.append('type', type);
   /// console.log("File Name : "+file.name);
    return this.httpClient.post("http://192.168.40.152:8080"  + '/uploadFileImageAndDoc', formdata);
  }

  //FILE UPLOAD
  public pushFileToStorageProcess(file: File, fileType : string, type : string): Observable<any> {
      console.log("Uploading file."); 
      let formdata: FormData = new FormData(); 
      formdata.append('file', file);
      formdata.append('file_type', fileType);
      formdata.append('type', type);
      console.log("File Name : "+file.name);
      return this.httpClient.post(this.service.Core_URL + '/uploadFile', formdata);
  } 
   
  

 /*
  getFiles(): Observable<string[]> {
    return this.httpClient.get('/getallfiles');
  }

*/


  /**This is call for new Function all details */
  
  public getFunctionDataTableDetails(sort : string, order: string, page : number, pageSize : number): Observable<any> {
    console.log("order : " + order);
    console.log("Sort : " + sort);
    console.log("page : "+page);
    console.log("size : "+pageSize);

    if(!sort || sort.length < 1)
      sort = "displayOrder";

    let gridValue = '{"draw":1,"columns":[{"data":"check","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"dataContainerName","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"dataContainerDescription","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"createdName","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"modifiedName","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"createdDate","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}}],"order":[{"column":0,"dir":"asc"}],"start":0,"length":4,"search":{"value":"","regex":false},"transactionId":7}';
    const body = JSON.stringify({
      "page": page,
      "size" : pageSize,
      "direction": order,
      "columnName" : sort,
      observe: 'response'
    });
    return this.httpClient.post<any>(this.service.Core_URL  + '/getAllFunctionsInfo', body, httpOptions);
  }

  public getFunctionDetailsByFunctionId(functionId: string): Observable<any> {
    return this.httpClient.get(this.service.Core_URL  + '/getFunctionDetailsById/' + functionId);
  }

  public getFunctionByName(functionName: string, orgId : string){
    return this.httpClient.get<any>(this.service.Core_URL  + '/getAllFunctionsByName/' + 'test' + "/" + 'test');
  }

  public getAllFunctionsDisplayOrderList(){
    return this.httpClient.get<any>(this.service.Core_URL  + '/getAllDisplayOrderList');
  }

  public getAllUserList(){    
    return this.httpClient.get<any>(this.service.User_URL  + '/getUsersList');
    //return this.httpClient.get<any>(this.service.Org_URL  + '/getAllOrgList');
  }

  public getAllFunctionsList(){
    return this.httpClient.get<any>(this.service.Core_URL  + '/getAllFunctionsList');
  }

  public getAllFunctionsAcronymList(){
    return this.httpClient.get<any>(this.service.Core_URL  + '/getAllFunctionsAcronymList');
  }

  public refreshFunctionList(){
    this.getFunctionDataTableDetails("displayOrder","asc", 0, 10);
    console.log("Refresh the function list");
  }


   /**This is call for new Function all details */
  /*************************All Process Info By Abhishek****************************************** */
   public getProcessDataTableDetails(sort: string, order: string, page : number, pageSize : number): Observable<any> {
    console.log("order : "+order);
    console.log("page : "+page);
    console.log("size : "+pageSize);
    let sortName = this.getSortNameWithoutID(sort);
    console.log("sort : "+sort);
    const body = JSON.stringify({
      "page": page,
      "size" : pageSize,
      "direction": order,
      "columnName" : sortName,
      observe: 'response'
    });
    return this.httpClient.post<any>(this.service.Core_URL  + '/getAllProcessInfo', body, httpOptions);
  }
  getSortNameWithoutID(sort){
    if(sort == undefined || sort == 'createdName' || sort == 'activeYN')
    return "processName"
    else
    return sort

  }

  /******************AllProcessList By Process ID */
  public getProcessDetailsByProcessId(processId: string): Observable<any> {
    return this.httpClient.get(this.service.Core_URL  + '/getProcessDetailsById/' + processId);
  }

  /*************ProcessDisplayOrderList*****************************/
  public getAllProcessDisplayOrderList(){
    return this.httpClient.get<any>(this.service.Core_URL  + '/getDisplayOrderList');
  }

  /*************************GetAll ProcessList***********************************/
  public getAllProcessList(){
    return this.httpClient.get<any>(this.service.Core_URL  + '/getAllProcessList');
  }

  /***********************GetAll Process Acronym List******************************/
  public getAllProcessAcronymList(){
    return this.httpClient.get<any>(this.service.Core_URL  + '/getAllProcessAcronymList');
  }

  /*************Refresh ProcessList***************************** */
  public refreshProcessList(){
    // this.getProcessDataTableDetails("asc", 0, 10);
    // console.log("Refresh the process list");
  }

   /**This is call for adding Process details */
   public addNewProcessDetails(processDetails: ProcessDetails): Observable<any> {
     console.log("dashboard service");
     
    return this.httpClient.post(this.service.Core_URL + '/addNewProcess', processDetails, httpOptions);
  }

  /**This is call for updating Process details */
  public updateProcessDetails(processDetails: ProcessDetails): Observable<any> {
    return this.httpClient.post(this.service.Core_URL + '/editProcess', processDetails, httpOptions);
  }

  /******************AllProcessList By Function ID */
 /*  public getProcessDetailsByFunctionId(functionId: string): Observable<any> {
    return this.httpClient.get(this.service.Core_URL  + '/getProcessDetailsByFunctionId/' + functionId);
  } */

  /*****************getAllProcessByName********************************* */
  public getProcessByName(processName: string, orgId : string){
    return this.httpClient.get<any>(this.service.Core_URL  + '/getAllProcessByName/' + 'test' + "/" + 'test');
  }
  
  /*************************All MasterData Info By Abhishek****************************************** */
  public getMasterDataTableDetails(order: string, page : number, pageSize : number): Observable<any> {
    console.log("order : "+order);
    console.log("page : "+page);
    console.log("size : "+pageSize);

    let gridValue = '{"draw":1,"columns":[{"data":"check","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"dataContainerName","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"dataContainerDescription","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"createdName","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"modifiedName","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"createdDate","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}}],"order":[{"column":0,"dir":"asc"}],"start":0,"length":4,"search":{"value":"","regex":false},"transactionId":7}';
    
    const body = JSON.stringify({
      "page": page,
      "size" : pageSize,
      "direction": order,
      "columnName" : "groupName",
      observe: 'response'
    });
    return this.httpClient.post<any>(this.service.Master_URL  + '/getAllMasterDataInfo', body, httpOptions);
  }

  //sidharth
  /**This is Pagination Call for process details by function id */
  public getAllProcessByFunctionId(sort: string, order: string, page : number, pageSize : number, functionId: string): Observable<any> {
    console.log("sort : "+sort);    
    console.log("order : "+order);
    console.log("page : "+page);
    console.log("size : "+pageSize);
    console.log("functionId :"+ functionId);
   // let gridValue = '{"draw":1,"columns":[{"data":"check","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"dataContainerName","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"dataContainerDescription","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"createdName","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"modifiedName","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":"createdDate","name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}}],"order":[{"column":0,"dir":"asc"}],"start":0,"length":4,"search":{"value":"","regex":false},"transactionId":7}';
    let sortName=this.getSortNameWithID(sort);
   const body = JSON.stringify({
      "sort":sort,
      "page": page,
      "size" : pageSize,
      "direction": order,
      "columnName" : sortName,
      "functionId" : functionId,
      observe: 'response'
    });
    return this.httpClient.post<any>(this.service.Core_URL  + '/getAllProcessByFunctionId/'+ functionId, body, httpOptions);
  }
  getSortNameWithID(sort){
if( sort == 'createdName' || sort == 'activeYN')
return 'processName'
else
return sort
  }

}//end of the class