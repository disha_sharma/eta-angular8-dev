import { Component, OnInit } from '@angular/core';
import { ServiceService } from './../../../eta-services/service.service'
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-completed-task',
  templateUrl: './completed-task.component.html',
  styleUrls: ['./completed-task.component.css']
})
export class CompletedTaskComponent implements OnInit {

  private Core_URL: any;
  private editFormObj: any;
  public isEdit = false;
  public MyTaskUrl: any;
  public MyTaskColumns: any;
  private urlParameter: any;

  constructor(private shareServices: ServiceService, private r: Router, private route: ActivatedRoute, ) {
    this.Core_URL = this.shareServices.Core_URL;
    this.MyTaskUrl = this.Core_URL + '/getCompletedTask';
    this.route.params.subscribe(params => { this.urlParameter = params.functionId; });
    this.Core_URL = this.shareServices.Core_URL;
    this.MyTaskUrl = this.Core_URL + '/getCompletedTask/' + this.urlParameter;
  }

  ngOnInit() {

    this.clearShareObj();
    this.getHeaderMenuList();
    this.getCompletedTaskColumn();

    this.shareServices.currentMessage.subscribe(message => this.editFormObj = message);

    /** only for edit */
    if (this.editFormObj != null && this.editFormObj != 'editFalse') {
      this.isEdit = true;
    }
    else {
      this.isEdit = false;
    }
  }

  public addFunction() {
    this.clearShareObj();
    this.r.navigateByUrl('addfunction');
  }

  public editFunction() {
    if (this.editFormObj == null || this.editFormObj.processId == null) {
      alert("Please choose for edit !!")
    } else {
      this.r.navigateByUrl('process/' + this.editFormObj.processId);
    }
  }

  public clearShareObj() {
    this.shareServices.setgridChangeMessage(null);
  }

  private getCompletedTaskColumn() {
    this.shareServices.getCompletedTaskColumn(this.urlParameter)
      .subscribe(data => {
        this.MyTaskColumns = data.coloumnName;
      });
  }

  /* method to get braedcrumb*/

  public mainbreadcrumb: string;
  public mainRouterLink: any;
  public breadcrumbRouterLink: any;
  public breadcrumb: string;
  public menuList: any = [];
  private getHeaderMenuList() {
    this.shareServices.getHeaderMenu()
      .subscribe(res => {
        this.menuList = res;
        this.menuList.forEach(element => {

          if (element.menuType == 4) {
            let newrouterlink = "/completedtask/" + this.urlParameter;
            if (element.routerLink == "/mytask/" + this.urlParameter) {
              this.mainRouterLink = "/mytask/" + this.urlParameter;
              this.mainbreadcrumb = element.translation;
            }

            if (element.routerLink == newrouterlink) {
              this.breadcrumbRouterLink = element.routerLink;
              this.breadcrumb = element.translation;
            }
          }
        })
      })
  }

  /**
   *To set Router link Menu List
   *
   * @param {*} routerlink
   * @memberof PrivilegeDeshboardComponent
   */
  shownRouterLink(routerlink) {
    this.r.navigate(["/" + routerlink]);
  }

  public activeLink ='/completedtask/';
  background = '';

  toggleBackground() {
    this.background = this.background ? '' : 'primary';
  }
}
