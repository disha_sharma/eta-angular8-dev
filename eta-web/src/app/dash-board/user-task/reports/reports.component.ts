import { Component, OnInit } from '@angular/core';
import { ServiceService } from './../../../eta-services/service.service'
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.css']
})

export class ReportsComponent implements OnInit {

  Core_URL: any;
  editFormObj: any;
  isEdit = false;
  MyTaskUrl: any;
  urlParameter: any;
  MyTaskColumns: any = [
    { data: 'check', displayName: 'check', hyperlink: false, edit: false },
    { data: 'taskInstanceID', displayName: 'Task Instance ID', hyperlink: false, edit: false },
    { data: 'processName', displayName: 'Proces sName', hyperlink: false, edit: false },
    { data: 'taskName', displayName: 'Task Name', hyperlink: false, edit: false },
    { data: 'formName', displayName: 'Form Name', hyperlink: false, edit: false },
    { data: 'createdName', displayName: 'Created By', hyperlink: false, edit: false },
    { data: 'modifideName', displayName: 'Created By', hyperlink: false, edit: false },
    { data: 'formName', displayName: 'Form Name', hyperlink: false, edit: false },

  ];

  constructor(private shareServices: ServiceService, private r: Router, private route: ActivatedRoute) {
    this.route.params.subscribe(params => { this.urlParameter = params.functionId; });
    this.Core_URL = this.shareServices.Core_URL;
    this.MyTaskUrl = this.Core_URL + '/getMyTask/' + this.urlParameter;
  }

  ngOnInit() {
    this.clearShareObj();
    this.getHeaderMenuList();
    this.shareServices.currentMessage.subscribe(message => this.editFormObj = message);
    /** only for edit */
    if (this.editFormObj != null && this.editFormObj != 'editFalse') {
      this.isEdit = true;
    }
    else {
      this.isEdit = false;
    }
  }

  public addFunction() {
    this.clearShareObj();
    this.r.navigateByUrl('addfunction');
  }

  public editFunction() {
    if (this.editFormObj == null || this.editFormObj.processId == null) {
      alert("Please choose for edit !!")
    } else {
      this.r.navigateByUrl('process/' + this.editFormObj.processId);
    }
  }

  private clearShareObj() {
    this.shareServices.setgridChangeMessage(null);
  }

  /* method to get braedcrumb*/

  public mainbreadcrumb: string;
  public mainRouterLink: any;
  public breadcrumbRouterLink: any;
  public breadcrumb: string;
  public menuList: any = [];
  private getHeaderMenuList() {
    this.shareServices.getHeaderMenu()
      .subscribe(res => {
        this.menuList = res;
        this.menuList.forEach(element => {
          if (element.menuType == 4) {
            let newrouterlink = "/reports/" + this.urlParameter;

            if (element.routerLink == newrouterlink) {
              this.mainRouterLink = element.routerLink;
              this.mainbreadcrumb = element.translation;
            }

            if (element.routerLink == newrouterlink) {
              this.breadcrumbRouterLink = element.routerLink;
              this.breadcrumb = element.translation;
            }
          }
        })

      })
  }

 /**
   *To set Router link Menu List
   *
   * @param {*} routerlink
   * @memberof PrivilegeDeshboardComponent
   */
  shownRouterLink(routerlink) {
    this.r.navigate(["/" + routerlink]);
  }

  public activeLink = 'reports';
  background = '';

  toggleBackground() {
    this.background = this.background ? '' : 'primary';
  }

}
