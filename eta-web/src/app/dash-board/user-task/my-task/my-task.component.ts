import { Component, OnInit } from '@angular/core';
import { ServiceService } from './../../../eta-services/service.service'
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-my-task',
  templateUrl: './my-task.component.html',
  styleUrls: ['./my-task.component.css']
})
export class MyTaskComponent implements OnInit {
  
  private Core_URL: any;
  private editFormObj: any;
  public isEdit = false;
  public MyTaskUrl: any;
  private urlParameter: any;
  public MyTaskColumns: any;
  functionId : any;

  constructor(private shareServices: ServiceService, private r: Router, private activatedRoute: ActivatedRoute) {
   // this.route.params.subscribe( params => {this.urlParameter = params.functionId;});
    this.Core_URL = this.shareServices.Core_URL;
  }
  
  ngOnInit() {
    this.clearShareObj();
    this.getHeaderMenuList();
    this.shareServices.currentMessage.subscribe(message => this.editFormObj = message);

    /** only for edit */
    if (this.editFormObj != null && this.editFormObj != 'editFalse') {
      this.isEdit = true;
    }
    else {
      this.isEdit = false;
    }
  }

  public addFunction() {
    this.clearShareObj();
    this.r.navigateByUrl('addfunction');
  }

  public editFunction() {
    if (this.editFormObj == null || this.editFormObj.processId == null) {
      alert("Please choose for edit !!")
    } else {
      this.r.navigateByUrl('process/' + this.editFormObj.processId);
    }
  }

  public clearShareObj() {
    this.shareServices.setgridChangeMessage(null);
  }

  private getMyTaskColumn() {
    if (this.functionId != null) {
      this.shareServices.getMyTaskColumn(this.functionId)
        .subscribe(data => {
          console.log('data',data);
          this.MyTaskColumns = data.coloumnName;
          //console.log(this.MyTaskColumns);
        });
    }
  }
  /* method to get braedcrumb*/

  public mainbreadcrumb: string;
  public mainRouterLink: any;
  public breadcrumbRouterLink: any;
  public breadcrumb: string;
  public menuList: any = [];
  private getHeaderMenuList() {
    this.shareServices.getHeaderMenu()
      .subscribe(res => {
        console.log(res);
        this.functionId = res[0].fucntionId;
        this.menuList = res;
        this.shareServices.getUserActivationStatusByToken().subscribe(response => {
          if(response && response.userType){
            let userType = response.userType;
            if(userType =='ORG_Employee'){
             // this.r.navigate(['/mytask'],{queryParams : { functionId : this.functionId}})
            }else if(userType =='ORG_Reviewer'){
              this.MyTaskUrl = this.Core_URL + '/getMyTask/' + this.functionId;
              this.getMyTaskColumn();
             // this.r.navigate(['/mytask']);
            }else {
             // this.r.navigate(['/mytask']);
            }
          }
        });
        console.log('this.functionId',this.functionId);
        this.menuList.forEach(element => {
          //alert(element.menuType);
          if (element.menuType == 4) {
            let newrouterlink = "/eta-web/mytask/" + this.urlParameter;

            if (element.routerLink == newrouterlink) {
              this.mainRouterLink = element.routerLink;
              this.mainbreadcrumb = element.translation;

            }
            if (element.routerLink == newrouterlink) {
              this.breadcrumbRouterLink = element.routerLink;
              this.breadcrumb = element.translation;
            }
          }
        })

      })
  }
  
  /**
   *To set Router link Menu List
   *
   * @param {*} routerlink
   * @memberof PrivilegeDeshboardComponent
   */
  shownRouterLink(routerlink) {
    this.r.navigate(["/" + routerlink]);
  }

  public activeLink = 'mytask';
  background = '';

  toggleBackground() {
    this.background = this.background ? '' : 'primary';
  }

}
