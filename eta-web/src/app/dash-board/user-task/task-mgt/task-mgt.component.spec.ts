import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaskMgtComponent } from './task-mgt.component';

describe('TaskMgtComponent', () => {
  let component: TaskMgtComponent;
  let fixture: ComponentFixture<TaskMgtComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaskMgtComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaskMgtComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
