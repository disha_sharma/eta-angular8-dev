import { Component, OnInit } from '@angular/core';
import { ServiceService } from './../../eta-services/service.service'

@Component({
  selector: 'app-main-dashboard',
  templateUrl: './main-dashboard.component.html',
  styleUrls: ['./main-dashboard.component.css']
  
})
export class MainDashboardComponent implements OnInit {

  myRole:string;
  adminRole;
  constructor(private newService: ServiceService) {
    
   }

  ngOnInit() {
    this.adminRole = this.newService.adminRole;
    this.myRole = this.newService.myRole;
    //console.log("my role"+this.myRole); 
  }

}
