
import { Component, OnInit,ViewChild, ElementRef, ChangeDetectorRef, Inject   } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient,HttpResponse, HttpHeaderResponse } from '@angular/common/http';
import { ServiceService } from './../../eta-services/service.service';
import { MatDialog, MatDialogConfig,MatPaginator, MatSort,MatTableDataSource,MAT_DIALOG_DATA } from '@angular/material';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {merge, Observable, of as observableOf} from 'rxjs';
import {catchError, map, startWith, switchMap} from 'rxjs/operators';
import { DashboardServiceService } from '../dashboard-service.service';
import { ProcessComponent } from '../../form/core/process/process.component';
import { EditProcessComponent } from '../../form/core/edit-process/edit-process.component';
import { ProcessDetails } from '../../form/core/process/Process';
import { FormGroup, FormControl, FormArray, FormBuilder } from '@angular/forms';
import { SelectionModel } from '@angular/cdk/collections';
import { ErrorDetails } from '../add-function-dialog/function';

@Component({
  selector: 'app-process-dashboard',
  templateUrl: './process-dashboard.component.html',
  styleUrls: ['./process-dashboard.component.css'],
  animations: [
    trigger('detailExpand', [
      state('void', style({ height: '0px', minHeight: '0', visibility: 'hidden' })),
      state('*', style({ height: '*', visibility: 'visible' })),
      transition('void <=> *', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class ProcessDashboardComponent implements OnInit {
  interestFormGroup : FormGroup;
  Core_URL: any;
  public shown: string = 'Function';
  public mainbreadcrumb: string;
  public breadcrumb: string;
  gridFunctionColumns: any;
  gridProcessCol: any;
  gridTaskColumns: any;
  functionUrl;
  gridFunctionData: any;
  isDataLoadedFunction = false;
  gridProcessData: any;
  isDataLoadedProcess = false;
  processUrl;
  editButtonFlag: boolean = false;
  public isEditAllowed: boolean = false;
  processId: string;
  successAdded : boolean = false;
  successUpdated : boolean = false;
  message: any;
  //public processList: any = [];
  public processList: any[] = [];
  allFunction: any;
  id: any;
  public functionID: string;
  processID: string;
  allButtonFlag: boolean = true;
  PID:any;
  public functionId: string;
  processName :string;
  errorInProcessAdding : string = "";
  failAdded : boolean = false;
 

  //STOP PROPEGATON PROCESS
   stopPropegationProcess(event){
    event.stopPropagation();
    this.editButtonFlag = !this.editButtonFlag;
  } 
  
  //taskUrl:any;
  taskUrl;

  panelOpenState = false;
  formUrl: any;
  dataContainerUrl;
  gridFormColumns: any;
  gridDataContainerColumns: any;
  gridDataObjectColumns: any;
  gridDataElementColumns: any;
  errorDetails : ErrorDetails[];
  isLoading = false;

  public defaultColumnToSort: any;

  //displayedColumns = ['processID','processIdDis','processName','createdName','activeYN','createdDate'];
  displayedColumns = ['processID','dragHandle','processName','createdName','activeYN','createdDate'];
  dataSource: MatTableDataSource<ProcessData>;
  pageSize : number = 5;
  pageSizeOptions = "";
  resultsLength = 0;
  displayOrder: number;
  isLoadingResults = true;
  isRateLimitReached = false;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  processHttpDao: ProcessHttpDao | null;
  data: Object[] = [];
  totalRecords: number;
  httpResponse : number = 0;
  isExpansionDetailRow = (i: number, row: Object) => row.hasOwnProperty('detailRow');
  expandedElement: any;
  funcitonAcronym: string;
  result;
  fullImagePath = "";
  checked: boolean = false; 
  newaddButtonFlag: boolean = true;
  neweditButtonFlag: boolean = false;
  currentCheckboxIndex: number;
  deleteFailValidation : boolean = false;
  successDelete : boolean = false;
  loadingSpinner: boolean = false;

  selection = new SelectionModel(true, []);
  constructor(private formBuilder: FormBuilder, private http: HttpClient, private route: ActivatedRoute, private r: Router, 
    private shareServices: ServiceService, private dashboardService: DashboardServiceService, public dialog: MatDialog, private changeDetectorRefs: ChangeDetectorRef) {
        this.getHeaderMenuList();
        console.log("came after route.");
  }
  
  onChange(index, event) {
    const interests = <FormArray>this.interestFormGroup.get('interests') as FormArray;

    if(event.checked) {
      interests.push(new FormControl(event.source.value));
     
        
      
    } else {
      const i = interests.controls.findIndex(x => x.value === event.source.value);
      interests.removeAt(i);
    }
  }
  check(index, event) {
      this.currentCheckboxIndex = index;
      console.log(this.interestFormGroup.value.interests);
      this.processId = this.interestFormGroup.value.interests[0];
      
      if(this.interestFormGroup.value.interests.length == 0){
          this.newaddButtonFlag = true;
          this.neweditButtonFlag = false;
          return false;
      }
      else if(this.interestFormGroup.value.interests.length == 1){
          this.newaddButtonFlag = false;
          this.neweditButtonFlag = true;
      }
      else if(this.interestFormGroup.value.interests.length > 1){
          this.newaddButtonFlag = false;
          this.neweditButtonFlag = false;      
          return true;        
      }
   
  }


  resetEditAddButton(){
    this.processId = "";
    let interests = <FormArray>this.interestFormGroup.get('interests') as FormArray;
    while (interests.length !== 0) {
      interests.removeAt(0)
    }
    this.newaddButtonFlag = true;
    this.neweditButtonFlag = false;    
  }

  ngOnInit() {
    setTimeout(()=> {
      this.toggleScrollButtonsForCards();
  }, 0);
        this.pageSize = 5;
        // If the user changes the sort order, reset back to the first page.
        let that = this;
        this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
        this.processHttpDao = new ProcessHttpDao(this.http, this.dashboardService);
       //this.functionId = this.route.snapshot.queryParams['funtionId'];
      // console.log('this.functionId',this.functionId);
        console.log("THIS IS RELOADING THE LISST.");

        merge(this.sort.sortChange, this.paginator.page)
          .pipe(
            startWith({}),
            switchMap(() => {
              if (this.paginator.pageSize != undefined) {
                this.pageSize = this.paginator.pageSize;
                } 
              this.isLoadingResults = true;
              return this.processHttpDao!.getRepoIssues(this.sort.active, this.sort.direction, this.paginator.pageIndex, this.pageSize, this.functionID);
            }),
            map(data => {
              // Flip flag to show that loading has finished.
              this.isLoadingResults = false;
              this.isRateLimitReached = false;
              this.resultsLength = data.recordsTotal;
              console.log("data length : "+data.recordsTotal);
              return data.data;
            }),
            catchError(( response : HttpHeaderResponse) => {
              if(response.status === 404){
                this.httpResponse = response.status;
              }
              this.resetAll();
              this.isLoadingResults = false;
              this.isRateLimitReached = true;
              return observableOf([]);
            })
          ).subscribe(data => {
              this.httpResponse = 200;
              //this.resultsLength = data.recordsTotal;
              this.dataSource = new MatTableDataSource(data);
              this.resetAll();
          }); 



    this.interestFormGroup = this.formBuilder.group({
      interests: this.formBuilder.array([]),
    });
    this.pageSize = 5 ;
    //this.getAllProcess();
    this.getAllFunction();

    // If the user changes the sort order, reset back to the first page.
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    this.shareServices.refreshEditMessage();

    this.shareServices.currentMessage.subscribe(message => {
        this.message = message
        if (message != 'editFalse') {
          this.isEditAllowed = true;
        }
    });

    

 
  }

  onPaginateChange(event) {
        this.pageSizeOptions = event.pageSizeOptions;
        console.log("page Size index : "+this.pageSizeOptions);   
   }

public resetAll(){
  this.successAdded = false;
  this.successUpdated = false;
  this.editButtonFlag = false;
}

    public refreshProcessList(){        
      console.log('refreshProcessList');
      this.dashboardService.getProcessDataTableDetails(this.sort.active, this.sort.direction, this.paginator.pageIndex, this.pageSize).subscribe(response=> {
        console.log("Response on refresh : "+response);
        console.log("Total records : "+response.recordsTotal);
          this.resultsLength = response.recordsTotal;
        this.dataSource = new MatTableDataSource(response.data);
      })
      this.changeDetectorRefs.detectChanges();
  } 


  /**
   * 
   * @param jsonStr 
   * @param keyToFind 
   */
  public getParsedValue(jsonStr : string, keyToFind : string) : any{
    console.log(jsonStr);
    console.log("keyToFind : "+keyToFind);
    JSON.parse(jsonStr, (key, value) => {
      console.log("key : "+key + "  value : "+value);
      if (key.trim() === keyToFind.trim()) {
        return value;
      }
      return value;
    });
  }//end of the method


  public clearShareObj() {
    this.shareServices.setgridChangeMessage(null);
  }

  markActiveButton(that, event) {
        document.getElementsByClassName('active-button')[0].classList.remove('active-button');
        event.target.classList.add('active-button');
        this.funcitonAcronym = event.target.innerHTML;
        if(that == 'all') {
          this.allButtonFlag = true;
        }
        else{
          this.allButtonFlag = false;
        }
   }

  removeSuccess(){
    this.successAdded = false;
    this.processList.splice(0,this.processList.length);
  }

  removeUpdate(){
    console.log("removeUpdate");
    this.successUpdated = false;
    //this.refreshProcessList();       
    this.processList.splice(0,this.processList.length);

  }
 removeAllError(){
   this.removeSuccess();
   this.removeUpdate();
 }
   private getAllFunction() {
         this.dashboardService.getAllFunctionsList()
         .subscribe(data => {
         this.allFunction = data;
         setTimeout(()=> {
          this.toggleScrollButtonsForTabsSlider();
      }, 0);
       });
   }


  shownRouterLink(routerlink) {
    this.r.navigate(["/" + routerlink]);
  }

  public activeLink = 'processdashboard';
  background = '';

  toggleBackground() {
    this.background = this.background ? '' : 'primary';
  }

  menuList: any = [];
  getHeaderMenuList() {
    this.shareServices.getHeaderMenu()
      .subscribe(res => {
        this.menuList = res;

        this.menuList.forEach(element => {
          if (element.routerLink == "#2") {
            this.mainbreadcrumb = element.translation;
          }
          if (element.routerLink == "eta-web/processdashboard") {
            this.breadcrumb = element.translation;
          }
        })
      })
  }

  private publish() {
   
    if (!this.selection.selected.length) {
   
    alert("Please choose the process");
   
    } else {
   console.log('selected process id is ', this.selection.selected[0].processID);
   console.log("URL:: ", this.shareServices.Core_URL);
   
    this.http.get(this.shareServices.Core_URL + '/publishForm' + '/' + this.selection.selected[0].processID, { responseType: 'text' })
   
    .subscribe(data => {
   
    alert(data);
   this.selection.clear();
    });
   
    }
   
    }


    public clickFunctionName(functionId){
      console.log("Disha madam inside function");
      this.removeAllError();
      this.removeSuccess();
      this.removeUpdate(); 
     this.selection.clear();
      this.functionID = functionId;
      this.paginator.pageIndex = 0;
      //sidharth
     /** this.shareServices.getAllProcessByFunctionId(functionId).subscribe(data =>{
        console.log(data);
        this.resultsLength = data.recordsTotal;
        this.dataSource =new MatTableDataSource(data.data);
      });*/
      this.dashboardService.getAllProcessByFunctionId(this.sort.active, this.sort.direction, this.paginator.pageIndex, this.pageSize, functionId).subscribe(data => {
        console.log(data);
        this.resultsLength = data.recordsTotal;
        this.dataSource =new MatTableDataSource(data.data);
        this.paginator.pageIndex = 0;
        this.pageSize = 5;
        console.log("getting display order from edit : "+this.displayOrder);   

    });
     
    }  
 
 
    /** Whether the number of selected elements matches the total number of rows. */
 isAllSelected() {
   const numSelected = this.selection.selected.length;
   const numRows = this.dataSource.data.length;
   return numSelected === numRows;
}

    /** Selects all rows if they are not all selected; otherwise clear selection. */
    masterToggle() {
      this.isAllSelected() ?
          this.selection.clear() :
          this.dataSource.data.forEach(row => this.selection.select(row));
    }
    myFunction2(){
      console.log(this.dataSource);      
    }

    updateProcessId(Pid){
      this.PID =Pid;
      this.editButtonFlag = true;

    }
  
   openAddDialog(): void {
        this.resetAll();
        const dialogConfig = new MatDialogConfig();
        dialogConfig.autoFocus = true;
        dialogConfig.disableClose = true;
        dialogConfig.data = {
          functionId: this.functionID,
          displayOrder: this.displayOrder,
          currentFuncitonAcronym: this.funcitonAcronym
        }; 
        const dialogRef = this.dialog.open(ProcessComponent, dialogConfig);
        let self = this;
        dialogRef.afterClosed().subscribe(result => {
          this.selection.clear();
          this.resetEditAddButton();
        this.getAllProcessByFunctionId(this.functionID);
        this.processList.splice(0,this.processList.length);
        if(this.successAdded === true){
          this.successAdded = false;
        }else{
          this.successAdded = true;
        }
        if(this.dashboardService.cancelAdd === true){
          this.successAdded = false;
        }
        this.dashboardService.cancelAdd =false;
        this.processName = this.dashboardService.processName;
        this.dashboardService.processName = "";
      });

      } 

    deleteProcess(){
      this.loadingSpinner = true;
        for(let i = 0; i < this.selection.selected.length; i++){
          this.processList.push(this.selection.selected[i].functionID);
        }
      this.dashboardService.deleteProcess(this.processList).subscribe(data => {
        this.errorDetails = data.result;
        this.loadingSpinner = false;
        this.clickFunctionName(this.functionID);   
        if(data.message === '7001'){
          this.deleteFailValidation = true;
        }
        else{
          this.successDelete = true;
        }
       // this.refreshProcessList();  
       
        this.processList.splice(0,this.processList.length);

        this.selection.clear();  
      });
    }


   

    removeProcessDeleteValidationMsg(){
      this.deleteFailValidation = false;   
      this.resetAll(); 
      this.processList.splice(0,this.processList.length);
     // this.refreshProcessList();       
     // this.processList.splice(0,this.processList.length);
    }

    public hideProcess(){
      this.dashboardService.hideProcess(this.processList).subscribe(data => {
        this.errorDetails = data.result;
        this.clickFunctionName(this.functionID); 
        if(data.message === '7001')
          this.deleteFailValidation = true;
        else
          this.successDelete = true;
       // this.refreshProcessList();       
        this.processList.splice(0,this.processList.length);

      });
      this.selection.clear();
    }


    public unhideProcess(){
      this.dashboardService.unhideProcess(this.processList).subscribe(data => {
        this.errorDetails = data.result;
        this.clickFunctionName(this.functionID);   
        if(data.message === '7001')
          this.deleteFailValidation = true;
        else
          this.successDelete = true;
       // this.refreshProcessList();       
        this.processList.splice(0,this.processList.length);
 
      });
      
      this.selection.clear(); 
    }

    

    setProcessId(processId: string, processName: string){
      this.processId = processId;
      if(this.processList.length === 0){
          this.processList.push(processId);
      }else{
            if(this.checkProcessIDExist(this.processList, processId)){
              const index = this.processList.findIndex(f => f === processId); 
              this.processList.splice(index, 1); 
            }else{
                this.processList.push(processId);
            }
      } this.processList.forEach(function (value) {
        });
        this.dashboardService.getProcessDetailsByProcessId(this.processId).subscribe(data => {
        this.displayOrder = data.displayOrder;
      }); 

      if(this.editButtonFlag === true && this.processList.length === 0){
        this.editButtonFlag = false;
        this.processList.splice(0,this.processList.length);
      } else if( this.editButtonFlag === false || this.processList.length != 0){
        this.editButtonFlag = true;
      }

 }



openEditDialog(processID): void{  
    this.resetAll();
    this.dashboardService.getProcessDetailsByProcessId(this.processId).subscribe(data => {
        this.displayOrder = data.displayOrder;  
    });

    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.disableClose = true;
    dialogConfig.data = {
      processId: processID,
      displayOrder: this.displayOrder,
      functionId: this.functionID,
      currentFuncitonAcronym: this.funcitonAcronym,
        selectedValue: 2
    };
    
    const dialogRef = this.dialog.open(EditProcessComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      this.selection.clear();
      this.resetEditAddButton();
      //this.checked = false;
      this.getAllProcessByProcessId(this.processId);
      this.getAllProcessByFunctionId(this.functionID);
      this.resetAll(); 
      this.processList.splice(0,this.processList.length);
      if(this.successUpdated === true){
        this.successUpdated = false;
      }else{
          this.successUpdated = true;
        } 
        if(this.dashboardService.cancelAdd === true){
          this.successUpdated = false;
        }  

        this.dashboardService.cancelAdd =false;
      this.processName = this.dashboardService.processName;
      //this.refreshProcessList();
      this.processList.splice(0,this.processList.length);
      this.dashboardService.processName = "";
      console.log(this.processName);
    });
} 
    
  
    public getAllProcess() {
      this.shareServices.getAllProcessList()
        .subscribe(data => {
          this.dataSource = data;
          //this.processList = data;
          this.editButtonFlag = false;
          
        });
    }
  
    public getAllProcessByFunctionId(functionId){
      this.dashboardService.getAllProcessByFunctionId(this.sort.active, this.sort.direction, this.paginator.pageIndex, this.pageSize, functionId).subscribe(data => {
        this.resultsLength = data.recordsTotal;
        this.dataSource =new MatTableDataSource(data.data);
        this.paginator.pageIndex = 0;
        this.pageSize = 5;

    });

      /*  this.shareServices.getAllProcessByFunctionId(functionId).subscribe(data =>{
          console.log(data);
          this.resultsLength = data.recordsTotal;
          this.dataSource =new MatTableDataSource(data.data);
        }); */
    } 

    public getAllProcessByProcessId(processId){
      this.shareServices.getAllProcessByProcessId(processId)
          .subscribe(data =>{
              console.log(data);
              this.isLoadingResults = false;
              this.isRateLimitReached = false;
              this.resultsLength = data.recordsTotal;
              this.dataSource = new MatTableDataSource(data.data);
      });
    } 

  clickAllButton() {
    this.functionID = undefined;
    this.removeAllError();
    this.pageSize = 5;
    this.paginator.pageIndex = 0;
    this.dashboardService.getProcessDataTableDetails(this.sort.active, this.sort.direction, this.paginator.pageIndex, this.pageSize).subscribe(response=> {
        this.resultsLength = response.recordsTotal;
        this.paginator.pageIndex = 0;
      this.dataSource = new MatTableDataSource(response.data);
    })
  }

  public checkProcessIDExist(array : any[], testString: string) : boolean {
    return array.some(r => r === testString);
  }
/*cards slider code starts */
cards = [
  {heading: '5', description: 'Processes'},
  {heading: '8', description: 'Tasks Flows'},
  {heading: '1508', description: 'Transactions'},
  {heading: '$1M', description: 'Value of Txs.'},
  {heading: 'SAP', description: 'Most Used'},
  {heading: '2', description: 'Processes'},
  {heading: '3', description: 'Transactions'},
  {heading: '5', description: 'Processes'},
  {heading: '8', description: 'Tasks Flows'},
  {heading: '1508', description: 'Transactions'},
  {heading: '$1M', description: 'Value of Txs.'},
  {heading: 'SAP', description: 'Most Used'},
  {heading: '2', description: 'Processes'},
  {heading: '3', description: 'Transactions'},
] 
cardsScrollForwardButtonFlag: boolean;
cardsScrollBackButtonFlag: boolean;
@ViewChild('cardsContainer', { read: ElementRef, static: true }) public cardsContainer: ElementRef<any>;
@ViewChild('cardsScrollableContent', { read: ElementRef, static: true }) public cardsScrollableContent: ElementRef<any>;
onWindowResized(event) {
  this.toggleScrollButtonsForCards();
  this.toggleScrollButtonsForTabsSlider();
}
toggleScrollButtonsForCards(){
  if(this.cardsScrollableContent.nativeElement.clientWidth > this.cardsContainer.nativeElement.clientWidth){
this.cardsScrollForwardButtonFlag = true;
  }
  else{
    this.cardsScrollForwardButtonFlag = false;
    this.cardsScrollBackButtonFlag = false;
  }
}
cardsScrollBackFunction(){
  this.cardsContainer.nativeElement.scrollTo({ left: (this.cardsContainer.nativeElement.scrollLeft -300), behavior: 'smooth' });
  this.cardsScrollForwardButtonFlag = true;
  if(this.cardsContainer.nativeElement.scrollLeft < 301){
       this.cardsScrollBackButtonFlag = false;
     }
  }
  
  cardsScrollForwardButton(){
    this.cardsContainer.nativeElement.scrollWidth;
  this.cardsContainer.nativeElement.scrollTo({ left: (this.cardsContainer.nativeElement.scrollLeft + 300), behavior: 'smooth' });
  this.cardsScrollBackButtonFlag = true;
  if(this.cardsContainer.nativeElement.offsetWidth + this.cardsContainer.nativeElement.scrollLeft > (this.cardsContainer.nativeElement.scrollWidth - 301)){
              this.cardsScrollForwardButtonFlag = false;
           }
  }
  /*cards slider code ends */

//code for tabs scrollable
tabsScrollForwardButtonFlag: boolean;
tabsScrollbackButtonFlag: boolean;
@ViewChild('tabsContainer', { read: ElementRef, static: true }) public tabsContainer: ElementRef<any>;
@ViewChild('tabsScrollableContent', { read: ElementRef, static: true }) public tabsScrollableContent: ElementRef<any>;
toggleScrollButtonsForTabsSlider(){
  if(this.tabsScrollableContent.nativeElement.clientWidth > this.tabsContainer.nativeElement.clientWidth){
this.tabsScrollForwardButtonFlag = true;
  }
  else{
    this.tabsScrollForwardButtonFlag = false;
    this.tabsScrollbackButtonFlag = false;
  }
}
back(){
  this.tabsContainer.nativeElement.scrollTo({ left: (this.tabsContainer.nativeElement.scrollLeft -300), behavior: 'smooth' });
  this.tabsScrollForwardButtonFlag = true;
  if(this.tabsContainer.nativeElement.scrollLeft < 301){
       this.tabsScrollbackButtonFlag = false;
     }
  }
  
  forward(){
    this.tabsContainer.nativeElement.scrollWidth;
  this.tabsContainer.nativeElement.scrollTo({ left: (this.tabsContainer.nativeElement.scrollLeft + 300), behavior: 'smooth' });
  this.tabsScrollbackButtonFlag = true;
  if(this.tabsContainer.nativeElement.offsetWidth + this.tabsContainer.nativeElement.scrollLeft > (this.tabsContainer.nativeElement.scrollWidth - 301)){
              this.tabsScrollForwardButtonFlag = false;
           }
  }
  
}//end of the class 

    export interface ProcessData {
        organizationId : string;
        processID: string;
        processName: string;
        processDescription : string;
        createdName : string;
        activeYN : string;
        createdDate : string;
        modifiedBy : string;
        modifiedName : string;
        total_count: number;
        taskFlowID : string;
        taskFlowName : string;
        recordsTotal : number;
        processIdDis: string;
    }

    export interface TaskFlow{
        taskFlowID : string;
        taskFlowName : string;
    }

    export interface ProcessCollection { 
        data: ProcessData[];
    }

    /** An example database that the data source uses to retrieve data for the table. */
    export class ProcessHttpDao {
        constructor(private http: HttpClient, private dashboardService : DashboardServiceService) {
    }

    getRepoIssues(sort: string, order: string, page: number, pageSize: number, functionId: string): Observable<any> {
        console.log("functionId on change : "+functionId);
        if(functionId == undefined){ 
         // order = "asc"; 
          return this.dashboardService.getProcessDataTableDetails(sort, order, page, pageSize);
        } else {
         // order = "asc"; 
          return this.dashboardService.getAllProcessByFunctionId(sort, order, page, pageSize, functionId);
        }
          
    }
}