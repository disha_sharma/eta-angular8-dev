import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardCommonUiComponent } from './dashboard-common-ui.component';

describe('DashboardCommonUiComponent', () => {
  let component: DashboardCommonUiComponent;
  let fixture: ComponentFixture<DashboardCommonUiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardCommonUiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardCommonUiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
