import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-dashboard-common-ui',
  templateUrl: './dashboard-common-ui.component.html',
  styleUrls: ['./dashboard-common-ui.component.css']
})
export class DashboardCommonUiComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }
 @ViewChild('widgetsContent', { read: ElementRef, static: true }) public widgetsContent: ElementRef<any>; 
  cards = [
    {heading: 8, description: 'Tasks Flows'},
    {heading: 124, description: 'Tasks'},
    {heading: 36, description: 'Data Containers'},
    {heading: 24, description: 'Forms'},
    {heading: 8, description: 'Privileges'},
    {heading: 'Heading', description: 'Description'},
    {heading: 'Heading', description: 'Description'},
  ]

  prev(){
    this.widgetsContent.nativeElement.scrollTo({ left: (this.widgetsContent.nativeElement.scrollLeft -300), behavior: 'smooth' });  
  }
    
  next(){
    this.widgetsContent.nativeElement.scrollTo({ left: (this.widgetsContent.nativeElement.scrollLeft + 300), behavior: 'smooth' });
  }
}
