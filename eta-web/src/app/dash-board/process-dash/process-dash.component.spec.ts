import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProcessDashComponent } from './process-dash.component';

describe('ProcessDashComponent', () => {
  let component: ProcessDashComponent;
  let fixture: ComponentFixture<ProcessDashComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProcessDashComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProcessDashComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
