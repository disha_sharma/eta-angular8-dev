import { Component, OnInit,ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { ServiceService } from './../../eta-services/service.service';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { AddProcessComponent } from '../../form/core/add-process/add-process.component';
import {animate, state, style, transition, trigger} from '@angular/animations';




@Component({
  selector: 'app-process-dash',
  templateUrl: './process-dash.component.html',
  styleUrls: ['./process-dash.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0', display: 'none'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ]
})
export class ProcessDashComponent implements OnInit {

  Core_URL: any;
  public shown: string = 'Function';
  functionUrl;
  processUrl;
  message: any;
  public processList: any = [];
  allFunction: any;
  id: any;
  functionID: string;
  processID: string;
  allButtonFlag: boolean = true;
  PID:any;
  editButtonFalg: boolean = false;


  @ViewChild('widgetsContent', { read: ElementRef, static: true }) public widgetsContent: ElementRef<any>;
  cards = [

    {heading: 5, discription: 'Processes'},
    {heading: 8, discription: 'Task Flows'},
    {heading: 5208, discription: 'Transactions'},
    {heading: '$1M', discription: 'Value of Txs.'},
    {heading: 'SAP', discription: 'Most Used'},
    {heading: 12, discription: 'Processes'},
    {heading: '15.8K', discription: 'Transactions'},
    {heading: '$4M', discription: 'Value of Txs.'},
    {heading: 12, discription: 'Processes'},
    {heading: '15.8K', discription: 'Transactions'},
    {heading: '$4M', discription: 'Value of Txs.'},
    
  ]
  
  
  constructor(private http: HttpClient, private route: ActivatedRoute,
    private r: Router, private shareServices: ServiceService,private dialog: MatDialog ) {
    this.getHeaderMenuList();
    this.Core_URL = this.shareServices.Core_URL;
    this.functionUrl = this.Core_URL + '/getAllFunctionsByOrgIdGrid';
    this.processUrl = this.Core_URL + '/getAllProcessByOrgIdGrid';
  }

  ngOnInit() {
    this.shareServices.refreshEditMessage();
    this.getAllProcess();
    this.getAllFunction();
  }
  markActiveButton(that) {
   if(that == 'all') {
     this.allButtonFlag = true;
   }
   else{
     this.allButtonFlag = false;
   }
  }
  public addProcess() {
    this.r.navigateByUrl('addprocess');
  }

  public editProcess() {
    this.r.navigateByUrl('addprocess');
  }

  private getAllFunction() {
    this.http.get(this.Core_URL + '/getAllFunction')
      .subscribe(data => {
        this.allFunction = data;

      });
  }

  

  onCreate(){
    /* let dialogRef = this.dialog.open(AddProcessComponent);
    dialogRef.componentInstance.functionID = this.functionID; */
    const dialogConfig = new MatDialogConfig();
    dialogConfig.data = {
      id: this.functionID,
      PID:this.PID
     
    };

    const dialogRef = this.dialog.open(AddProcessComponent, dialogConfig);

  }

    onEdit(): void{
      const dialogConfig = new MatDialogConfig();
      dialogConfig.data = {
        id: this.functionID,
        PID:this.PID
      };
      const dialogRef = this.dialog.open(AddProcessComponent, dialogConfig);
  }
  

  public getAllProcess() {
    this.shareServices.getAllProcessList()
      .subscribe(data => {
        this.processList = data;
        this.editButtonFalg = false;
        console.log(data[0]);
        
      });
  }

  public getAllProcessByFunctionId(functionId){
    this.shareServices.getAllProcessByFunctionId(functionId)
    .subscribe(data =>{
       this.processList = data;
    });
  }


  public mainbreadcrumb: string;
  public breadcrumb: string;
  public menuList: any = [];
  getHeaderMenuList() {
    this.shareServices.getHeaderMenu()
      .subscribe(res => {
        this.menuList = res;

        this.menuList.forEach(element => {
          if (element.routerLink == "home") {
            this.mainbreadcrumb = element.translation;
          }
          if (element.routerLink == "eta-web/processdash") {
            this.breadcrumb = element.translation;
          }
        })
      })
  }

  private shownRouterLink(routerlink) {
    this.r.navigate(["/" + routerlink]);
  }

  public activeLink = 'processdash';
  background = '';

  toggleBackground() {
    this.background = this.background ? '' : 'primary';
  }

  prev(){
    this.widgetsContent.nativeElement.scrollTo({ left: (this.widgetsContent.nativeElement.scrollLeft -300), behavior: 'smooth' });
    }
    
    next(){
    this.widgetsContent.nativeElement.scrollTo({ left: (this.widgetsContent.nativeElement.scrollLeft + 300), behavior: 'smooth' });
    
    }

    clickFunctionName(functionId){
      console.log(functionId);
      this.functionID =functionId;
      this.getAllProcessByFunctionId(functionId);
      this.editButtonFalg = false;

    }

    updateProcessId(Pid){
      this.PID =Pid;
      this.editButtonFalg = true;
      alert(Pid);

    }
}