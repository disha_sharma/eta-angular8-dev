// import { Component, OnInit } from '@angular/core';

// @Component({
//   selector: 'app-post-login-dashboard',
//   templateUrl: './post-login-dashboard.component.html',
//   styleUrls: ['./post-login-dashboard.component.css']
// })
// export class PostLoginDashboardComponent implements OnInit {

//   constructor() { }

//   ngOnInit() {
//   }

// }

import { Component, OnInit, ViewChild, ElementRef, ViewChildren, QueryList, ChangeDetectorRef } from '@angular/core';
import { Location } from '@angular/common';
import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
//import { ServiceService } from './eta-services/service.service';
//import { Router, NavigationEnd } from '../../node_modules/@angular/router';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { relative } from 'path';
import {  AfterViewInit } from '@angular/core';
//import { ResizeEvent } from 'angular-resizable-element';
//import { ResizedEvent } from 'angular-resize-event';
import { MatFormField, MatDialog } from '@angular/material';
import { supportsScrollBehavior } from '@angular/cdk/platform';
import { ResponseOptions } from '@angular/http';
import { ServiceService } from '../../eta-services/service.service';
import { Router, NavigationEnd } from '@angular/router';
//import { ChangeThemeComponent } from './dummyContants/change-theme/change-theme.component';
declare var EQCSS;
   
@Component({
  selector: 'app-post-login-dashboard',
  templateUrl: './post-login-dashboard.component.html',
  styleUrls: ['./post-login-dashboard.component.css'],
  animations: [
    trigger('childAnimation', [
      // ...
      state('open', style({
        width: '256px',
        position: 'relative',
        display: 'block',
        float: 'left',

      })),
      state('closed', style({
        width: '71px',
        position: 'relative',
        display: 'block',
        float: 'left',
      })),
      transition('closed => open', [
        animate('0.5s')
      ]),

    ]),
    trigger('navBarAnimation', [
   
      state('big', style({
        width: '100%',
        display: 'flex',
      })),
      state('small', style({
        width: '256px',
        display: 'flex',
      })),
      transition('* => *', [
        animate('1s')
      ]),

    ]),
  ],
})
export class PostLoginDashboardComponent implements OnInit {


   
  title = 'app';
  Showheader: any;
  ShowheaderFlag: boolean;
  sideNavBarPosition: string = 'start';
  topMenuFlag: boolean = false;
  isExpanded: boolean = true;
  expandPannelFlag= {};

  isLoggedIn$: Observable<boolean>;
  panelOpenState: boolean = false;
  isExpandable: boolean= true;
  previousButtonFlag: boolean = false;
  nextButtonFlag: boolean = false;
  /**
   *For Default Theme to load
   *
   * @type {*}
   * @memberof AppComponent
   */
  public applyedTheme: any = "my-light-green-theme";

  /**
   * List of Themes We have 
   *
   * @type {*}
   * @memberof AppComponent
   */
  //select options for top tool bar select tag - recently used
  selectOptions = [
    {value: 'task-0', viewValue: 'Add new task'},
    {value: 'work-1', viewValue: 'Add new Work'},
    {value: 'flow-2', viewValue: 'Add new flow'}
  ];
  public themesList: any = [
    { value: 'my-light-green-theme', viewValue: 'Light Green' },
    { value: 'my-dark-green-theme', viewValue: 'Dark Green' },
    { value: 'my-red-theme', viewValue: 'Dark Red' },
    { value: 'my-orange-theme', viewValue: 'Light Orange ' },



  ];
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );

    @ViewChild('widgetsContent', { read: ElementRef, static: true }) public widgetsContent: ElementRef<any>;
    @ViewChild('scrollableContent', { read: ElementRef, static: true }) public scrollableContent: ElementRef<any>;
 
  constructor(protected cdr: ChangeDetectorRef,
     private breakpointObserver: BreakpointObserver,
      private newService: ServiceService, 
      private r: Router, public dialog: MatDialog) {


  }

onWindowResized(event) {

 this.showNextPreviousButtonOnTopNavBar();
  const innerWidth = event.target.innerWidth;

   if (innerWidth <= 1360) {
     this.isExpanded = false;
     this.isExpandable = false;
  }
  else if(innerWidth >= 1360 && !this.isExpandable){
       this.isExpanded = true;
     this.isExpandable = true;
  }
}
changeNavBarStyleOnLoad(){
  if (window.innerWidth <= 1360) {
    this.isExpanded = false;
    this.isExpandable = false;
 }
 else if(window.innerWidth >= 1360){
   this.isExpanded = true;
   this.isExpandable = true;
 }
}
  ngOnInit() {
    this.getDynamicCss();
     this.r.events.subscribe((evt) => {
       if (!(evt instanceof NavigationEnd)) {
           return;
       }
     setTimeout(() => {
      window.scrollTo(0, 0,);
     }, 1000);
       
  //  document.body.scrollTop = 0;
  });
   // this.setDynamicCss();
   this.changeNavBarStyleOnLoad();
    if (this.newService.checkedSessionToken() == true) {
      this.newService.refresh();
    }
    this.isLoggedIn$ = this.newService.isLoggedIn;

    /*  To check wheather user Login and refresh menu after login */
    this.newService.isLoggedIn.source
      .subscribe(res => {
        if (res == true) {
          console.log("This header Menu list");
 
          this.getHeaderMenuList();
          this.getDynamicCss();
        }
        if (res == false) {
          /* If Logout clear menu list that store in this veriable */
          this.menuTypeList = [];
        }
      })
  }

  logout() {

    this.newService.getLogoutInfo()
      .subscribe(res => {
        this.newService.logout();
      })
    localStorage.clear();
    sessionStorage.clear();

  }

  menuTypeList: any = [];
  menuList: any = [];
  private functionImage: any;
  private functionId: any;
  getHeaderMenuList() {

    this.newService.getHeaderMenu()
      .subscribe(res => {
        console.log('inside get header menu list');
        
        this.menuList = res;
        let menu: any = {};

        this.menuList.forEach(element => {
          this.functionImage = element.functionImage;
          this.functionId = element.fucntionId;
          let mainMenuType: any = {};
          if (element.menuType == 1) {
            if (this.functionId != null) {
              // this.getFuinctiondatabyOrgId(this.functionId,this.functionImage);
            }
            mainMenuType.translation = element.translation;
            mainMenuType.mainType = element.menuType;
            mainMenuType.mainRef = element.menuRef;
            mainMenuType.mainId = element.menuId;
            mainMenuType.routerLink = element.routerLink;
            mainMenuType.iconName = element.iconName;
             let subMenuList: any = [];
            this.menuList.forEach(subelement => {
             
              let subMenuType: any = {};
              if (subelement.menuType == 2) {

                if (mainMenuType.mainId == subelement.menuRef) {
                  subMenuType.translation = subelement.translation;
                  subMenuType.mainType = subelement.menuType;
                  subMenuType.mainRef = subelement.menuRef;
                  subMenuType.mainId = subelement.menuId;
                  mainMenuType.iconName = element.iconName;
                  subMenuList.push(subelement);
                  mainMenuType.subMenu = subMenuList;
                }
              }
            })

            this.menuTypeList.push(mainMenuType);
          }

          menu.menu = this.menuTypeList;
          console.log('this.menuTypeList;',this.menuTypeList);
          

        });
        
     
      })


  }
  showNextPreviousButtonOnTopNavBar(){
    if(this.scrollableContent.nativeElement.clientWidth > window.innerWidth){
this.nextButtonFlag = true;
    }
    else{
      this.nextButtonFlag = false;
      this.previousButtonFlag = false;
    }
    
  }
  onChange(themeName) {
    this.applyedTheme = themeName;
  }

  profileView() {
    this.newService.profileView();
  }
toggleTopMenu() {
  //this.topMenuFlag = !this.topMenuFlag;
  
  if(this.topMenuFlag){
    this.topMenuFlag = false;
    document.documentElement.style.setProperty('--bread-crumb-display', 'none');
    document.documentElement.style.setProperty('--top-nav-bar-display', 'none');
    document.documentElement.style.setProperty('--side-nav-drawer-display', 'block');
  }
  
    
  else{
    this.topMenuFlag = true;
    document.documentElement.style.setProperty('--bread-crumb-display', 'block');
    document.documentElement.style.setProperty('--top-nav-bar-display', 'block');
    document.documentElement.style.setProperty('--side-nav-drawer-display', 'none');
    this.showNextPreviousButtonOnTopNavBar();
  }
}
changeSideBarStyle () {
  //this.expandPannelFlag = {};
  this.isExpanded = !this.isExpanded;
  
}
openExpandablePanel (menuList) {
  if(menuList.subMenu == null) {
    this.expandPannelFlag = {};
    return
  }
  else{
    this.isExpandable = true;
    this.isExpanded = true;
    this.expandPannelFlag = {};
    this.expandPannelFlag[menuList.translation] = true;
  }
  
}
//function for change side bar position from right to left or left to right
changeSideBarPosition() {
if(this.sideNavBarPosition == 'start') {
  this.sideNavBarPosition = 'end';
  document.documentElement.style.setProperty('--direction', 'rtl');
}
else if(this.sideNavBarPosition == 'end') {
  this.sideNavBarPosition = 'start';
  document.documentElement.style.setProperty('--direction', 'ltr');
}

}
dashBoardId : number;
stopPropegationFunction(event){
  event.stopPropagation();
}

prev(){
  this.widgetsContent.nativeElement.scrollTo({ left: (this.widgetsContent.nativeElement.scrollLeft -300), behavior: 'smooth' });
  this.nextButtonFlag = true;
  if(this.widgetsContent.nativeElement.scrollLeft < 301){
       this.previousButtonFlag = false;
     }
  }
  
  next(){
    this.widgetsContent.nativeElement.scrollWidth;
  this.widgetsContent.nativeElement.scrollTo({ left: (this.widgetsContent.nativeElement.scrollLeft + 300), behavior: 'smooth' });
  this.previousButtonFlag = true;
  if(this.widgetsContent.nativeElement.offsetWidth + this.widgetsContent.nativeElement.scrollLeft > (this.widgetsContent.nativeElement.scrollWidth - 301)){
              this.nextButtonFlag = false;
           }
  }
  getAllDynamicCssDefault(){
    this.newService.getDefaultCss(this.dashBoardId)
     .subscribe(response => {
       console.log('default' , response);
       
      this.setAllDynamicCss(response);
     }, error => {
     })
  }
  getDynamicCss(){
    console.log('ng oninit dynamic css');
    
    this.newService.GetDynamicCss()
    .subscribe(response => {
      console.log('response is (not default) ' , response );
    this.setAllDynamicCss(response);
    console.log('dynamic css response at starting app (refresh)', response);
    
    }, error => {
      console.log('no default error is', error);
      
    })
  }
  setAllDynamicCss(dynamicCss) {
    console.log('setting dynamic css is ' , dynamicCss);
    this.dashBoardId = dynamicCss.dashBoardId
        /*app bar dynamic styles */
document.documentElement.style.setProperty('--app-bar-background', dynamicCss.appBarBgColor);
document.documentElement.style.setProperty('--app-bar-text-color', this.lightOrDark(dynamicCss.appBarBgColor));
this.orgLogoPath = dynamicCss.orgLogoImageName;
this.appBarBgImage = dynamicCss.appBarImageName;

/*colors dynamic styles */
document.documentElement.style.setProperty('--primary', dynamicCss.primaryColor);
document.documentElement.style.setProperty('--primary-rgb', this.convertHexToReb(dynamicCss.primaryColor));
document.documentElement.style.setProperty('--secondary', dynamicCss.secondaryColor);
document.documentElement.style.setProperty('--substrate', dynamicCss.substrateColor);
document.documentElement.style.setProperty('--nav-drawer-background-color', dynamicCss.navDrawerBgColor);
document.documentElement.style.setProperty('--nav-drawer-background-color-rgb', this.convertHexToReb(dynamicCss.navDrawerBgColor));
document.documentElement.style.setProperty('--cards-background-color', dynamicCss.cardsBgColor);
document.documentElement.style.setProperty('--hyperlink-color', dynamicCss.hyperlinkColor);
document.documentElement.style.setProperty('--hyperlink-hover-color', dynamicCss.hyperlinkHoverColor);
document.documentElement.style.setProperty('--hyperlink-click-color', dynamicCss.hyperlinkClickColor);
document.documentElement.style.setProperty('--button-background-color', dynamicCss.buttonBgColor);
document.documentElement.style.setProperty('--button-hover-background-color', dynamicCss.buttonHoverBgColor);
document.documentElement.style.setProperty('--button-click-background-color', dynamicCss.buttonClickBgColor);
document.documentElement.style.setProperty('--button-text-color-enabled', dynamicCss.buttonEnabledTextColor);
document.documentElement.style.setProperty('--button-text-color-disabled', dynamicCss.buttonDisabledTextColor);
document.documentElement.style.setProperty('--button-text-color-hover', dynamicCss.buttonHoverTextColor);
document.documentElement.style.setProperty('--button-text-color-click', dynamicCss.buttonHoverTextColor);

/*font dynamic styles */
document.documentElement.style.setProperty('--nav-drawer-font-family', dynamicCss.navDrawerFont);
document.documentElement.style.setProperty('--nav-drawer-font-color', dynamicCss.navDrawerTextColor);
document.documentElement.style.setProperty('--body-text-font-family', dynamicCss.bodyTextFont);
document.documentElement.style.setProperty('--body-text-font-color', dynamicCss.bodyTextColor);
document.documentElement.style.setProperty('--heading-text-color', dynamicCss.headingTextColor);
document.documentElement.style.setProperty('--success-color', dynamicCss.successMessageTextColor);
document.documentElement.style.setProperty('--errors', dynamicCss.failureMessageTextColor);
  }

 dynamicCss: any = {
   appBarBgColor: 'green',
   orgLogo: "assets/org_logo_sample.png",
   appBarBgImage: 'assets/app-bar-bg-image.jpg',
  primaryColor: 'brown',
   primaryAccent1Color: 'black',
   primaryAccent2Color: 'black',
   secondaryColor: 'black',
   secondaryAccent1Color: 'black',
   secondaryAccent2Color: 'black',
   substrateColor: 'black',
   navDrawerBgColor: 'black',
   cardsBgColor: 'black',
   hyperlinkColor: 'green',
   hyperlinkHoverColor: 'brown',
   hyperlinkClickColor: 'pink',
   buttonBgColor: 'white',
   buttonHoverBgColor: 'green',
   buttonClickBgColor: 'bronw',
   buttonDisabledTextColor: 'gray',
   buttonEnabledTextColor: 'black',
   buttonHoverTextColor: 'white',
   buttonClickTextColor: 'white',
   navDrawerFont: 'roboto',
   navDrawerTextColor: 'black',
   bodyTextFont: 'roboto',
   bodyTextColor: 'black',
   headingTextColor: 'black',
   successMessageTextColor: 'green',
   failureMessageTextColor: 'red'
 }
orgLogoPath: any = '';
appBarBgImage: any = '';

  /*convert text color based on background color */
  lightOrDark(color) {
    let r, g, b, hsp;
      // Check the format of the color, HEX or RGB?
      if (color.match(/^rgb/)) {
        // If HEX --> store the red, green, blue values in separate variables
        color = color.match(/^rgba?\((\d+),\s*(\d+),\s*(\d+)(?:,\s*(\d+(?:\.\d+)?))?\)$/);
    
      r = color[1];
       g = color[2];
       b = color[3];
      } 
      else {
    
        // If RGB --> Convert it to HEX: http://gist.github.com/983661
        color = +("0x" + color.slice(1).replace( 
          color.length < 5 && /./g, '$&$&'
        )
                 );
    
                r = color >> 16;
                 g = color >> 8 & 255;
                 b = color & 255;
      }
    
      // HSP (Highly Sensitive Poo) equation from http://alienryderflex.com/hsp.html
      hsp = Math.sqrt(
        0.299 * (r * r) +
        0.587 * (g * g) +
        0.114 * (b * b)
      );
    
      // Using the HSP value, determine whether the color is light or dark
      if (hsp>127.5) {
        return '#000000';
      } 
      else {
        return '#ffffff';
      }
    }
  convertHexToReb(hexValue){
    let hex = hexValue.replace('#','');
   let r = parseInt(hex.substring(0,2), 16);
   let  g = parseInt(hex.substring(2,4), 16);
   let b = parseInt(hex.substring(4,6), 16);
    let rgb = r+','+g+','+b;
    return rgb;
  }
  removeImages(){
    this.appBarBgImage = '';
  }

  reDirectPrefernces(){
    this.r.navigate(["/eta-web/" + 'preference']);
  }

  reDirectAccount(){
    this.r.navigate(["/eta-web/" + 'Profile']);
  }
  
  /*Function called on router-outlet get new component for scroll to top */
  @ViewChild('scrollableContainer', { read: ElementRef, static: true }) public scrollableContainer: ElementRef<any>;
  onActivate(){
    this.scrollableContainer.nativeElement.scrollTo({ top: 0, behavior: 'smooth' });
  }
  scrollDown(){
    console.log('I got here');
    
    this.scrollableContainer.nativeElement.scrollTo({ top: this.scrollableContainer.nativeElement.scrollHeight, behavior: 'smooth' });
  }
 
}
