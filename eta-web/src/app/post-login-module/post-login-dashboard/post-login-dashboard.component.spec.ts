import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostLoginDashboardComponent } from './post-login-dashboard.component';

describe('PostLoginDashboardComponent', () => {
  let component: PostLoginDashboardComponent;
  let fixture: ComponentFixture<PostLoginDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostLoginDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostLoginDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
