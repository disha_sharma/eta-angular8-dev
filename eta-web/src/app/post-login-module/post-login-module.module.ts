import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { PostLoginComponent } from './post-login-dashboard';
import { TestComponent } from '../dummyContants/test/test.component';
import { PostLoginDashboardComponent } from './post-login-dashboard/post-login-dashboard.component';
import { materialModule } from '../material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatToolbarModule, MatButtonModule, MatSidenavModule, MatIconModule, MatListModule, MatAutocompleteModule, MatInputModule } from '@angular/material';
import { CalendarsListOfOrgAcronymComponent } from '../calendar/calendars-list-of-org-acronym/calendars-list-of-org-acronym.component';
import { AddNewCalendarToOrgAcronymComponent } from '../calendar/add-new-calendar-to-org-acronym/add-new-calendar-to-org-acronym.component';
import { OrgDetailsListComponent } from '../org-details-list/org-details-list.component';
import { SampleFormComponent } from '../form/core/sample-form/sample-form.component';
import { ReviewFormComponent } from '../form/core/review-form/review-form.component';
import { MediaCardComponent } from '../videoPlayer/media-card/media-card.component';
import { EditOrgDetailsComponent } from '../edit-org-details/edit-org-details.component';
import { BrandingComponent } from '../ui-configurations/branding/branding.component';
import { VideoPlayerComponentComponent } from '../videoPlayer/video-player-component/video-player-component.component';
import { EditUserConfigComponent } from '../user-role-config/user-config/edit-user-config/edit-user-config.component';
import { AddUserConfigComponent } from '../user-role-config/user-config/add-user-config/add-user-config.component';
import { DashboardUserConfigComponent } from '../user-role-config/user-config/dashboard-user-config/dashboard-user-config.component';
import { EditRoleConfigComponent } from '../user-role-config/role-config/edit-role-config/edit-role-config.component';
import { AddRoleConfigComponent } from '../user-role-config/role-config/add-role-config/add-role-config.component';
import { DashboardRoleConfigComponent } from '../user-role-config/role-config/dashboard-role-config/dashboard-role-config.component';
import { EditMasterdataComponent } from '../master-setup/edit-masterdata/edit-masterdata.component';
import { AddMasterdataComponent } from '../master-setup/add-masterdata/add-masterdata.component';
import { MasterDashboardComponent } from '../master-setup/master-dashboard/master-dashboard.component';
import { EditDatamodelComponent } from '../data-model-setup/edit-datamodel/edit-datamodel.component';
import { AddDatamodelComponent } from '../data-model-setup/add-datamodel/add-datamodel.component';
import { DataModelDashboardComponent } from '../data-model-setup/datamodel-dashboard/datamodel-dashboard.component';
import { EditFormComponent } from '../forms/edit-form/edit-form.component';
import { AddFormComponent } from '../forms/add-form/add-form.component';
import { AddTaskDetailPopupComponent } from '../popup/add-task-popup/add-task-detail-popup.component';
import { AddTaskDetailComponent } from '../form/core/add-task-detail/add-task-detail.component';
import { AddNewMasterDataGroupComponent } from '../dummyContants/dummy-popups/add-new-master-data-group/add-new-master-data-group.component';
import { AddNewFormToProcessNameComponent } from '../dummyContants/add-new-form-to-process-name/add-new-form-to-process-name.component';
import { DataModelsDummyComponent } from '../dummyContants/data-models-dummy/data-models-dummy.component';
import { MasterDataComponent } from '../dummyContants/master-data/master-data.component';
import { FunctionDashboardDummyComponent } from '../dummyContants/function-dashboard-dummy/function-dashboard-dummy.component';
import { RadioGroupComponent } from '../dummyContants/radio-group/radio-group.component';
import { MainDashboardComponent } from '../dash-board/main-dashboard/main-dashboard.component';
import { FunctionDashboardComponent } from '../dash-board/function-dashboard/function-dashboard.component';
import { DataObjectDashBoardComponent } from '../dash-board/dataobject-dash-board/dataobject-dash-board.component';
import { OrganizationDashboardComponent } from '../form/organization/organization-dashboard/organization-dashboard.component';
import { DynamicformComponentPublish } from '../form/core/dynamicform/dynamicform.component';
import { FunctionComponent } from '../form/core/function/function.component';
import { ProcessComponent } from '../form/core/process/process.component';
import { TaskComponent } from '../form/core/task/task.component';
import { CreateDynamicFormComponent } from '../form/core/create-dynamic-form/create-dynamic-form.component';
import { CreateDataObjectComponent } from '../form/core/create-data-object/create-data-object.component';
import { CreateDataContainerComponent } from '../form/core/create-data-container/create-data-container.component';
import { CreateDataFormComponent } from '../form/core/create-data-form/create-data-form.component';
import { MyTaskComponent } from '../dash-board/user-task/my-task/my-task.component';
import { CompletedTaskComponent } from '../dash-board/user-task/completed-task/completed-task.component';
import { TaskMgtComponent } from '../dash-board/user-task/task-mgt/task-mgt.component';
import { ReportsComponent } from '../dash-board/user-task/reports/reports.component';
import { GaConfigurationComponent } from '../form/ga-configuration/ga-configuration.component';
import { EmailConfigurationComponent } from '../form/notification/email-configuration/email-configuration.component';
import { EmailTempletConfigurationComponent } from '../form/notification/email-templet-configuration/email-templet-configuration.component';
import { EmailTempletLanguageConfigurationComponent } from '../form/notification/email-templet-language-configuration/email-templet-language-configuration.component';
import { EmailLogComponent } from '../form/notification/email-log/email-log.component';
import { EmailDashboardComponent } from '../form/notification/email-dashboard/email-dashboard.component';
import { OrganizationDetailsComponent } from '../form/organization/organization-details/organization-details.component';
import { OrganizationCalendarComponent } from '../form/organization/organization-calendar/organization-calendar.component';
import { MultilanguageComponent } from '../multilanguage/multilanguage.component';
import { UserDashboardComponent } from '../form/user/user-dashboard/user-dashboard.component';
import { UserdetailsComponent } from '../form/user/user-details/user-details.component';
import { UserProfileComponent } from '../form/user/user-profile/user-profile.component';
import { UserRoleComponent } from '../form/user/user-role/user-role.component';
import { UserAssignRoleComponent } from '../form/user/user-assign-role/user-assign-role.component';
import { UserPrivilegeComponent } from '../form/user/user-privilege/user-privilege.component';
import { DynamicDatatableComponent } from '../form/core/dynamic-datatable/dynamic-datatable.component';
import { PopupComponent } from '../form/popup/popup.component';
import { PrivacyPolicyComponent } from '../form/organization/organization-policy/org-privacy-policy/privacy-policy/privacy-policy.component';
import { TermsConditionComponent } from '../form/organization/organization-policy/org-terms-conditions/terms-condition/terms-condition.component';
import { OrgPasswordPolicyComponent } from '../form/organization/organization-policy/org-password-policy/org-password-policy.component';
import { UserPasswordResetComponent } from '../form/user/user-activation/user-password-reset/user-password-reset.component';
import { UserinfoDisplayComponent } from '../userinfo-display/userinfo-display.component';
import { PopupTemplateComponent } from '../popup-template/popup-template.component';
import { AddTaskComponent } from '../popup/add-task/add-task.component';
import { EditProcessComponent } from '../form/core/edit-process/edit-process.component';
import { AddPrivacyPolicyComponent } from '../form/organization/organization-policy/org-privacy-policy/add-privacy-policy/add-privacy-policy.component';
import { AddTncComponent } from '../form/organization/organization-policy/org-terms-conditions/add-tnc/add-tnc.component';
import { PrivacyPolicyTransComponent } from '../form/organization/organization-policy/org-privacy-policy/privacy-policy-trans/privacy-policy-trans.component';
import { TncTransComponent } from '../form/organization/organization-policy/org-terms-conditions/tnc-trans/tnc-trans.component';
import { EtaPageReloadComponent } from '../eta-page-reload/eta-page-reload.component';
import { WelcomeDeshboardComponent } from '../form/welcome-deshboard/welcome-deshboard.component';
import { RoleDeshboardComponent } from '../form/user/role-deshboard/role-deshboard.component';
import { PrivilegeDeshboardComponent } from '../form/user/privilege-deshboard/privilege-deshboard.component';
import { CalendarDashboardComponent } from '../form/organization/calendar-dashboard/calendar-dashboard.component';
import { GaDashboardComponent } from '../form/organization/ga-dashboard/ga-dashboard.component';
import { OrgPasswordDashboardComponent } from '../form/organization/organization-policy/org-password-dashboard/org-password-dashboard.component';
import { OrgPolicyDashboardComponent } from '../form/organization/organization-policy/org-policy-dashboard/org-policy-dashboard.component';
import { OrgTermsDashboardComponent } from '../form/organization/organization-policy/org-terms-dashboard/org-terms-dashboard.component';
import { EmailTemplateDashboardComponent } from '../form/notification/email-template-dashboard/email-template-dashboard.component';
import { EmailLogDashboardComponent } from '../form/notification/email-log-dashboard/email-log-dashboard.component';
import { DatacontainerDashboardComponent } from '../dash-board/datacontainer-dashboard/datacontainer-dashboard.component';
import { DataelementDashboardComponent } from '../dash-board/dataelement-dashboard/dataelement-dashboard.component';
import { FormDashboardComponent } from '../dash-board/form-dashboard/form-dashboard.component';
import { ProcessDashboardComponent } from '../dash-board/process-dashboard/process-dashboard.component';
import { TaskDashboardComponent } from '../dash-board/task-dashboard/task-dashboard.component';
import { UserTranslationDtComponent } from '../form/user/user-translation-dt/user-translation-dt.component';
import { OrgTryForFreeComponent } from '../form/organization/org-try-for-free/org-try-for-free.component';
import { OrgServicesPricingComponent } from '../form/organization/org-services-pricing/org-services-pricing.component';
import { CountryDetailsComponent } from '../form/country-details/country-details.component';
import { LocationDashboardComponent } from '../form/organization/location-dashboard/location-dashboard.component';
import { LdapConfigComponent } from '../form/organization/ldap-config/ldap-config.component';
import { AddFunctionDialogComponent } from '../dash-board/add-function-dialog/add-function-dialog.component';
import { OrgCustomtableComponent } from '../form/organization/org-customtable/org-customtable.component';
import { EditFunctionComponent } from '../dash-board/edit-function/edit-function.component';
import { TaskflowDashboardComponent } from '../dash-board/taskflow-dashboard/taskflow-dashboard.component';
import { AddTaskflowComponent } from '../form/core/add-taskflow/add-taskflow.component';
import { AddTaskPopupComponent } from '../popup/add-task-popup/add-task-popup.component';
import { EditTaskflowComponent } from '../form/core/edit-taskflow/edit-taskflow.component';
import { FormListComponent } from '../dummyContants/form-list/form-list.component';
import { DummyPopupComponent } from '../popup/dummy-popup/dummy-popup.component';
import { AddDataContainerComponent } from '../dummyContants/add-data-container/add-data-container.component';
import { ScriptApprovalApplicationFormComponent } from '../popup/script-approval-application-form/script-approval-application-form.component';
import { DummyTaskFlowDashboardComponent } from '../dummyContants/dummy-task-flow-dashboard/dummy-task-flow-dashboard.component';
import { FileUploadComponent } from '../dummyContants/file-upload/file-upload.component';
import { AutoCompleteComponent } from '../dummyContants/auto-complete/auto-complete.component';
import { TestPopupComponent } from '../dummyContants/test-popup/test-popup.component';
import { CheckboxGroupComponent } from '../dummyContants/checkbox-group/checkbox-group.component';
import { HeaderComponent } from '../header/header.component';
import { MenuComponent } from '../menu/menu.component';
import { DynamicFormComponent } from '../dynamic-form-builder/components/dynamic-form/dynamic-form.component';
import { FooterComponent } from '../footer/footer.component';
import { TableComponent } from '../table/table.component';
import { DynamicDatatableTranslateComponent } from '../dynamic-datatable-translate/dynamic-datatable-translate.component';
import { PolicyDtTransComponent } from '../form/organization/organization-policy/org-privacy-policy/policy-dt-trans/policy-dt-trans.component';
import { TermsConditionDatatableTranslateComponent } from '../form/organization/organization-policy/org-terms-conditions/tnc-dt-trans/tnc-dt-trans.component';
import { DynamicDatatableEditComponent } from '../dynamic-datatable-edit/dynamic-datatable-edit.component';
import { EmailAddTempletComponent } from '../form/notification/email-add-templet/email-add-templet.component';
import { LoginHeaderComponent } from '../login-header/login-header/login-header.component';
import { AdminDynamicDatatableComponent } from '../admin-dynamic-datatable/admin-dynamic-datatable.component';
import { MatDataTableComponent } from '../mat-data-table/mat-data-table.component';
import { MinLengthDirective } from '../eta-custom-directives/min-length.directive';
import { EmailValidateDirective } from '../eta-custom-directives/email-validate.directive';
import { NumberValidateDirective } from '../eta-custom-directives/number-validate.directive';
import { NameValidateDirective } from '../eta-custom-directives/name-validate.directive';
import { CdkDetailRowDirective } from '../directives/cdk-detail-row.directive';
import { DisableControlDirective } from '../dummyContants/add-new-form-to-process-name/disable-control.directive';
import { NestedReactiveFormComponent } from '../dummyContants/nested-reactive-form/nested-reactive-form.component';
import { FormsComponent } from '../dummyContants/forms/forms.component';
import { InputComponent } from '../dynamic-form-builder/components/input/input.component';
import { TextAreaComponent } from '../dynamic-form-builder/components/textarea/textarea.component';
import { ButtonComponent } from '../dynamic-form-builder/components/button/button.component';
import { SelectComponent } from '../dynamic-form-builder/components/select/select.component';
import { MediaChannelComponent } from '../videoPlayer/media-channel/media-channel.component';
import { FontsConfigurationComponent } from '../ui-configurations/branding/fonts-configuration/fonts-configuration.component';
import { ColorsConfigurationComponent } from '../ui-configurations/branding/colors-configuration/colors-configuration.component';
import { AppBarUiConfigurationComponent } from '../ui-configurations/branding/app-bar-ui-configuration/app-bar-ui-configuration.component';
import { ProgressBarComponent } from '../dynamic-form-builder/components/progressbar/progressbar.component';
import { MultiCheckboxComponent } from '../dynamic-form-builder/components/multicheckbox/multicheckbox.component';
import { FormFactoryComponent } from '../dynamic-form-builder/form-factory/form-factory.component';
import { DashboardCommonUiComponent } from '../dash-board/dashboard-common-ui/dashboard-common-ui.component';
import { MultipleMaterialSelectComponent } from '../dynamic-form-builder/components/multiSelect/multiselect.component';
import { MasterDataListComponent } from '../dynamic-form-builder/components/masterDataList/masterdatalist.component';
import { HiddenComponent } from '../dynamic-form-builder/components/hidden/hidden.component';
import { CancelComponent } from '../dynamic-form-builder/components/cancel/cancel.component';
import { SubmitComponent } from '../dynamic-form-builder/components/submit/submit.component';
import { LabelComponent } from '../dynamic-form-builder/components/label/label.component';
import { DynamicFieldDirective } from '../dynamic-form-builder/components/dynamic-field/dynamic-field.directive';
import { CheckboxComponent } from '../dynamic-form-builder/components/checkbox/checkbox.component';
import { RadiobuttonComponent } from '../dynamic-form-builder/components/radiobutton/radiobutton.component';
import { DateComponent } from '../dynamic-form-builder/components/date/date.component';
import { DataTablesModule } from 'angular-datatables';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/multiselect.component';
import { LayoutModule } from '@angular/cdk/layout';
import { FroalaViewModule, FroalaEditorModule } from 'angular-froala-wysiwyg';
// import { NgxEditorModule } from 'ngx-editor';
import { TooltipModule } from 'ngx-bootstrap';
// import { Ng4LoadingSpinnerModule } from 'ng4-loading-spinner';
import { EmbedVideo } from 'ngx-embed-video';
import { ColorPickerModule } from 'ngx-color-picker';
import { PageNotFoundComponent } from '../page-not-found/page-not-found.component';
import { TypeaheadModule } from 'ngx-bootstrap/typeahead';
import { TreeviewModule } from'../../lib';
import { MaterialDynamicDataTableComponent } from '../form/core/material-dynamic-data-table/material-dynamic-data-table.component';
// import { FileComponent } from '../dynamic-form-builder/components/file/file.component';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { PreferenceComponent } from '../preference/preference.component';
import { FileComponent } from '../dynamic-form-builder/components/file/file.component';
import { ProfileComponent } from '../profile/profile.component';


const route: Routes = [
  {
    path: '',
    component: PostLoginDashboardComponent,
    children: [
     // { path: 'home', component: WelcomeDeshboardComponent },
      { path: '', component: WelcomeDeshboardComponent },
      //   //  User workspace
      { path: 'home', component: MainDashboardComponent },
      { path: 'functionDashBoard', component: FunctionDashboardComponent },
      { path: 'dashboardDataObject', component: DataObjectDashBoardComponent },
      { path: 'organizationdashboard', component: OrganizationDashboardComponent },

      //Core
      { path: 'taskflow/:taskFlowId/:taskId/:formId', component: DynamicformComponentPublish },
      { path: 'addfunction', component: FunctionComponent },
      { path: 'addprocess', component: ProcessComponent },
      { path: 'task', component: TaskComponent },
      { path: 'createform', component: CreateDynamicFormComponent },
      { path: 'createdataobject', component: CreateDataObjectComponent },
      { path: 'createdatacontainer', component: CreateDataContainerComponent },
      { path: 'createdataform', component: CreateDataFormComponent },
      { path: 'mytask', component: MyTaskComponent },
      { path: 'mytask/:functionId', component: MyTaskComponent },
      { path: 'completedtask', component: CompletedTaskComponent },
      { path: 'completedtask/:functionId', component: CompletedTaskComponent },
      { path: 'taskmgt', component: TaskMgtComponent },
      { path: 'taskmgt/:functionId', component: TaskMgtComponent },
      { path: 'reports', component: ReportsComponent },
      { path: 'reports/:functionId', component: ReportsComponent },

      // GA
      { path: 'addgaconfig', component: GaConfigurationComponent },

      // Notification
      { path: 'addemailconfiguration', component: EmailConfigurationComponent },
      { path: 'addemailtemplate', component: EmailTempletConfigurationComponent },
      { path: 'emailtempletlanguage', component: EmailTempletLanguageConfigurationComponent },
      { path: 'emaillog', component: EmailLogComponent },
      { path: 'emaildashboard', component: EmailDashboardComponent },

      // Org
      { path: 'addorg', component: OrganizationDetailsComponent },
      { path: 'addcalendar', component: OrganizationCalendarComponent },
      { path: 'orgdetails', component: OrganizationDetailsComponent },
      { path: 'multilanguage', component: MultilanguageComponent },

      // Org Policy

      // User
      { path: 'adduser', component: UserdetailsComponent },
      { path: 'userdashboard', component: UserDashboardComponent },
      { path: 'userprofilepage', component: UserProfileComponent },

      // Role & privilege
      { path: 'addrole', component: UserRoleComponent },
      { path: 'assignrole', component: UserAssignRoleComponent },
      { path: 'editassignrole/:userId', component: UserAssignRoleComponent },
      { path: 'addprivilege', component: UserPrivilegeComponent },


      //  Common Componentes for all developers   
      { path: 'datatable', component: DynamicDatatableComponent },
      { path: 'popup', component: PopupComponent },


      // Privacy Policy and Terms Conditions
      { path: 'privacypolicy', component: PrivacyPolicyComponent },
      { path: 'termscondition', component: TermsConditionComponent },
      { path: 'addpasswordpolicy', component: OrgPasswordPolicyComponent },

      // user password reset 
      { path: 'userpasswordreset', component: UserPasswordResetComponent },
      { path: 'userpasswordreset/:userId', component: UserPasswordResetComponent },

      // display user profile after all sucessfull login
      { path: 'userdesplay', component: UserinfoDisplayComponent },

      //popUp Template to show Info
      { path: 'userTemplate', component: PopupTemplateComponent },
      { path: 'addTaskPopup', component: AddTaskComponent },
      { path: 'editProcess', component: EditProcessComponent },

      //check box id picker for email config
      { path: 'editemailconfiguration/:configid/:orgId', component: EmailConfigurationComponent },
      { path: 'editrole/:roleId', component: UserRoleComponent },
      { path: 'editprivilege/:priviID', component: UserPrivilegeComponent },
      { path: 'editorg/:orgId', component: OrganizationDetailsComponent },
      { path: 'edituser/:userId', component: UserdetailsComponent },
      { path: 'editpasswordpolicy/:id', component: OrgPasswordPolicyComponent },

      //27 april new code
      { path: 'editprivacypolicy/:tempid/:tempvid', component: AddPrivacyPolicyComponent },
      { path: 'edittermscondition/:tempid/:tempvid', component: AddTncComponent },
      { path: 'addprivacypolicy', component: AddPrivacyPolicyComponent },
      { path: 'addtermscondition', component: AddTncComponent },

      // 28 april new add email template 
      { path: 'addemailtemplate/:tempid/:tempvid', component: EmailTempletConfigurationComponent },
      { path: 'passwordpolicy/:id', component: OrgPasswordPolicyComponent },
      { path: 'editcalendar/:id', component: OrganizationCalendarComponent },

      //11 may policy and t&C translation added
      { path: 'privacypolicytranslation', component: PrivacyPolicyTransComponent },
      { path: 'tnctranslation', component: TncTransComponent },
      { path: 'refresh', component: EtaPageReloadComponent },

      //new Component dashboard 
      { path: 'welcome', component: WelcomeDeshboardComponent },
      { path: 'roledashboard', component: RoleDeshboardComponent },
      { path: 'privilegedashboard', component: PrivilegeDeshboardComponent },
      { path: 'calendardashboard', component: CalendarDashboardComponent },
      { path: 'gadashboard', component: GaDashboardComponent },
      { path: 'orgpassworddashboard', component: OrgPasswordDashboardComponent },
      { path: 'orgpolicydashboard', component: OrgPolicyDashboardComponent },
      { path: 'orgtermsdashboard', component: OrgTermsDashboardComponent },
      { path: 'emailtemplatedashboard', component: EmailTemplateDashboardComponent },
      { path: 'emaillogdashboard', component: EmailLogDashboardComponent },

      //core componet dashboard
      { path: 'datacontainerdashboard', component: DatacontainerDashboardComponent },
      { path: 'dataelementdashboard', component: DataelementDashboardComponent },
      { path: 'formdashboard', component: FormDashboardComponent },
      { path: 'processdashboard', component: ProcessDashboardComponent },
      { path: 'taskdashboard', component: TaskDashboardComponent },


      // role and privilege Translation grid
      { path: 'roletranslationdt', component: UserTranslationDtComponent },
      { path: 'freetrial', component: OrgTryForFreeComponent },
      { path: 'services-pricing', component: OrgServicesPricingComponent },

      //country details 
      { path: 'countrydetails', component: CountryDetailsComponent },
      /** Organization location dashboard  */
      { path: 'locationdashboard', component: LocationDashboardComponent },

      { path: 'ldapconfig', component: LdapConfigComponent },

      { path: 'cuntomtable', component: OrgCustomtableComponent },

      { path: 'addfunctiondialog', component: AddFunctionDialogComponent },
      { path: 'editfunctiondialog', component: EditFunctionComponent },
      //{ path: 'processdash', component: ProcessDashComponent},
      //{ path: 'add-process', component: AddProcessComponent },

      //TaskFlow Dashboard
      { path: 'taskflowdashboard', component: TaskflowDashboardComponent },
      // Add New TaskFlow 
      { path: 'addtaskflow', component: AddTaskflowComponent },
      { path: 'addtaskpopup', component: AddTaskPopupComponent },

      /** For Edit TaskFlow via taskFlowId */
      { path: 'edittaskflow/:processId/:taskFlowId', component: EditTaskflowComponent },

      /* Dummy test Shyam joshi */
      { path: 'form-list', component: FormListComponent },
      { path: 'dummyPopup', component: DummyPopupComponent },
      { path: 'addData', component: AddDataContainerComponent },
      { path: 'script-approval-form', component: ScriptApprovalApplicationFormComponent },
      { path: 'funn', component: FunctionDashboardDummyComponent },

      { path: 'dummyPopup', component: DummyPopupComponent }, // for popup
      { path: 'addData', component: AddDataContainerComponent }, //add data dummy component
      { path: 'script-approval-form', component: ScriptApprovalApplicationFormComponent }, //for popup
      { path: 'expandable-table', component: FunctionDashboardDummyComponent },
      { path: 'custom-slider', component: DummyTaskFlowDashboardComponent },
      { path: 'file-upload', component: FileUploadComponent },
      { path: 'auto-complete', component: AutoCompleteComponent },
      { path: 'test', component: TestComponent },
      { path: 'test-popup', component: TestPopupComponent },
      { path: 'checkbox-group', component: CheckboxGroupComponent },
      { path: 'radio-group', component: RadioGroupComponent },

      { path: 'funn', component: FunctionDashboardDummyComponent },
      { path: 'master-data', component: MasterDataComponent },
      { path: 'data-models', component: DataModelsDummyComponent },
      { path: 'process-name', component: AddNewFormToProcessNameComponent },
      { path: 'add-new-master-data-group', component: AddNewMasterDataGroupComponent }, //for popup
      //  {path: 'add-task', component: AddTaskFlowDummyComponent},

      { path: 'taskdetail/:actionID/:processId/:taskFlowId', component: AddTaskDetailComponent },
      { path: 'taskdetailpopup', component: AddTaskDetailPopupComponent },
      { path: 'addForm', component: AddFormComponent },
      { path: 'editForm', component: EditFormComponent },
      { path: 'datamodel-dashboard', component: DataModelDashboardComponent },
      { path: 'addDataModel/:processId', component: AddDatamodelComponent },
      { path: 'editDataModel', component: EditDatamodelComponent },
      { path: 'masterdata', component: MasterDashboardComponent },
      //path for popups
      { path: 'add-new-master-data', component: AddMasterdataComponent },
      { path: 'edit-master-data', component: EditMasterdataComponent },
      { path: 'roleConfigDashboard', component: DashboardRoleConfigComponent },
      { path: 'add-role', component: AddRoleConfigComponent },
      { path: 'edit-role', component: EditRoleConfigComponent },
      { path: 'userConfigDashboard', component: DashboardUserConfigComponent },
      { path: 'add-user', component: AddUserConfigComponent },
      { path: 'edit-user', component: EditUserConfigComponent },
      { path: 'videoPlayer', component: VideoPlayerComponentComponent },
      { path: 'addOrgDetail', component: EditOrgDetailsComponent },
      { path: 'branding', component: BrandingComponent },
      { path: 'editOrgDetail', component: EditOrgDetailsComponent },
      { path: 'mediaPlayer/:channelName', component: MediaCardComponent },
      { path: 'reviewForm', component: ReviewFormComponent },
      { path: 'sampleForm', component: SampleFormComponent },
      { path: 'orgDetailsList', component: OrgDetailsListComponent },
      { path: 'addNewCalendarToOrgAcronym', component: AddNewCalendarToOrgAcronymComponent },
      { path: 'calendarsListOfOrgAcronym', component: CalendarsListOfOrgAcronymComponent },
      { path: 'editCalendarToOrgAcronym', component: CalendarsListOfOrgAcronymComponent },
      {path : 'preference',component: PreferenceComponent},
      {path : 'Profile',component: ProfileComponent},
      { path:'**', component: PageNotFoundComponent },
    ]
  },
  // {path:'',component:PostLoginComponent}
]

@NgModule({
  declarations: [
    FileComponent,
     MaterialDynamicDataTableComponent,
    // FileComponent,
    PostLoginComponent,
    TestComponent,
    PostLoginDashboardComponent,
    HeaderComponent,
    MenuComponent,
    TableComponent,
    FooterComponent,
    DynamicFormComponent,
    GaConfigurationComponent,
    DynamicDatatableComponent,
    EmailConfigurationComponent,
    EmailTempletConfigurationComponent,
    EmailTempletLanguageConfigurationComponent,
    FunctionComponent,
    ProcessComponent,
    FunctionDashboardComponent,
    DataObjectDashBoardComponent,
    OrganizationDetailsComponent,
    //LoginComponent,
    MainDashboardComponent,
    PopupComponent,
    TaskComponent,
    CreateDynamicFormComponent,
    CreateDataObjectComponent,
    TaskComponent,
    DynamicDatatableTranslateComponent,
    CreateDynamicFormComponent,
    CreateDataObjectComponent,
    CreateDataContainerComponent,
    CreateDataFormComponent,
    UserdetailsComponent,
    OrganizationCalendarComponent,
    UserRoleComponent,
    UserPrivilegeComponent,
    UserDashboardComponent,
    UserAssignRoleComponent,
    UserPasswordResetComponent,
    PrivacyPolicyComponent,
    PolicyDtTransComponent,
    TermsConditionComponent,
    TermsConditionDatatableTranslateComponent,
    UserinfoDisplayComponent,
    EmailLogComponent,
    DynamicDatatableEditComponent,
    OrganizationDashboardComponent,
    MultilanguageComponent,
    OrgPasswordPolicyComponent,
    EmailDashboardComponent,
    UserProfileComponent,
    AddPrivacyPolicyComponent,
    AddTncComponent,
    EmailAddTempletComponent,
    PrivacyPolicyTransComponent,
    TncTransComponent,
    EtaPageReloadComponent,
    WelcomeDeshboardComponent,
    RoleDeshboardComponent,
    PrivilegeDeshboardComponent,
    CalendarDashboardComponent,
    GaDashboardComponent,
    OrgPasswordDashboardComponent,
    OrgPolicyDashboardComponent,
    OrgTermsDashboardComponent,
    EmailTemplateDashboardComponent,
    EmailLogDashboardComponent,
    FormDashboardComponent,
    ProcessDashboardComponent,
    TaskDashboardComponent,
    DatacontainerDashboardComponent,
    DataelementDashboardComponent,
    MyTaskComponent,
    CompletedTaskComponent,
    TaskMgtComponent,
    ReportsComponent,
    UserTranslationDtComponent,
    LoginHeaderComponent,
    AdminDynamicDatatableComponent,
    MatDataTableComponent,
    CountryDetailsComponent,
    OrgTryForFreeComponent,
    OrgServicesPricingComponent,
    LdapConfigComponent,
    LocationDashboardComponent,
    MinLengthDirective,
    EmailValidateDirective,
    NumberValidateDirective,
    NameValidateDirective,
    OrgCustomtableComponent,
    AddTaskComponent,
    PopupTemplateComponent,
    TaskflowDashboardComponent,
    AddTaskflowComponent,
    AddFunctionDialogComponent,
    EditFunctionComponent,
    AddTaskPopupComponent,
    DummyPopupComponent,
    AddDataContainerComponent,
    FormListComponent,
    ScriptApprovalApplicationFormComponent,
    FunctionDashboardDummyComponent,
    CdkDetailRowDirective,
    EditProcessComponent,
    EditTaskflowComponent,
    DummyTaskFlowDashboardComponent,
    FileUploadComponent,
    AutoCompleteComponent,
    TestComponent,
    TestPopupComponent,
    AddTaskDetailComponent,
    CheckboxGroupComponent,
    RadioGroupComponent,
    MasterDataComponent,
    DataModelsDummyComponent,
    AddNewFormToProcessNameComponent,
    DisableControlDirective,
    NestedReactiveFormComponent,
    AddNewMasterDataGroupComponent,
    FormsComponent,
    AddTaskDetailPopupComponent,
    AddFormComponent,
    EditFormComponent,
    DataModelDashboardComponent,
    InputComponent,
    TextAreaComponent,
    ButtonComponent,
    SelectComponent,
    DateComponent,
    RadiobuttonComponent,
    CheckboxComponent,
    DynamicFieldDirective,
    DynamicformComponentPublish,
    LabelComponent,
    SubmitComponent,
    CancelComponent,
    HiddenComponent,
    MasterDataListComponent,
    MultipleMaterialSelectComponent,
    AddDatamodelComponent,
    EditDatamodelComponent,
    MasterDashboardComponent,
    AddMasterdataComponent,
    EditMasterdataComponent,
    DashboardCommonUiComponent,
    AddRoleConfigComponent,
    DashboardRoleConfigComponent,
    EditRoleConfigComponent,
    AddUserConfigComponent,
    EditUserConfigComponent,
    DashboardUserConfigComponent,
    FormFactoryComponent,
    MultiCheckboxComponent,
    ProgressBarComponent,
    VideoPlayerComponentComponent,
    BrandingComponent,
    AppBarUiConfigurationComponent,
    ColorsConfigurationComponent,
    FontsConfigurationComponent,
    DynamicformComponentPublish,
    EditOrgDetailsComponent,
    MediaCardComponent,
    MediaChannelComponent,
    ReviewFormComponent,
    SampleFormComponent,
    CalendarsListOfOrgAcronymComponent,
    AddNewCalendarToOrgAcronymComponent,
    OrgDetailsListComponent,
    PageNotFoundComponent,
    PreferenceComponent,
    ProfileComponent
  ],
  imports: [
    CommonModule,
    materialModule,
    FormsModule,
    ReactiveFormsModule, 
    MatToolbarModule, 
    MatButtonModule, 
    MatSidenavModule, 
    MatIconModule, 
    MatListModule,
    RouterModule.forChild(route),
    DataTablesModule,
    MatAutocompleteModule,
    NgMultiSelectDropDownModule.forRoot(),
    MatInputModule,
    AngularMultiSelectModule,
    LayoutModule,
    //MatToolbarModule,
    //MatButtonModule,
    //MatSidenavModule,
    //MatIconModule,
    //MatListModule,
    FroalaEditorModule.forRoot(),
    FroalaViewModule.forRoot(),
    // NgxEditorModule,
    TooltipModule.forRoot(),
    TypeaheadModule.forRoot(),
    TreeviewModule.forRoot(),
    // Ng4LoadingSpinnerModule.forRoot(),
    EmbedVideo.forRoot(),
    ColorPickerModule
  ],
  schemas: [NO_ERRORS_SCHEMA],
  entryComponents: [
    InputComponent,
   TextAreaComponent,
 ButtonComponent,
 SelectComponent,
 DateComponent,
 RadiobuttonComponent,
 CheckboxComponent,
 FormFactoryComponent,
 FileComponent,
 MultiCheckboxComponent,
 MultipleMaterialSelectComponent,
 LabelComponent,
 SubmitComponent,
 CancelComponent,
 HiddenComponent,
 ProgressBarComponent,
 MasterDataListComponent
  ]
  
})
export class PostLoginModuleModule { }
