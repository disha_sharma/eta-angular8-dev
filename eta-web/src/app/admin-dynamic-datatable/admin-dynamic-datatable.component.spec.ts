import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminDynamicDatatableComponent } from './admin-dynamic-datatable.component';

describe('AdminDynamicDatatableComponent', () => {
  let component: AdminDynamicDatatableComponent;
  let fixture: ComponentFixture<AdminDynamicDatatableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminDynamicDatatableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminDynamicDatatableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
