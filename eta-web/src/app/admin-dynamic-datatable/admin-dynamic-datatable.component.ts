import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { ServiceService } from './../eta-services/service.service'
import { ActivatedRoute, Router } from '@angular/router';

class DataTablesResponse {
  data: any[];
  draw: number;
  recordsFiltered: number;
  recordsTotal: number;
  coloumnName: {};
}

@Component({
  selector: 'app-admin-dynamic-datatable',
  templateUrl: './admin-dynamic-datatable.component.html',
  styleUrls: ['./admin-dynamic-datatable.component.css']
})
export class AdminDynamicDatatableComponent implements OnInit {

  /**
   * grid data URL
   */
  @Input() myurl: string;

  /**
   * static coloumns
   */
  @Input() mycoloums = [];

  /**
   * Inline edit Url
   */
  @Input() mySubmitUrl: string;

  /**
   * grid data
   */
  @Input() gefields: any[];

  @Output() translationEditObj = new EventEmitter();

  dtOptions: DataTables.Settings = {};
  language : DataTables.LanguageSettings ={};
  paginate : DataTables.LanguagePaginateSettings;
  showPopUp = false;
  isCreateCheck = false;
  Core_URL: any;
  isDataLoad = true;
  cdnList:any={}

  constructor(private http: HttpClient, private shareServices: ServiceService, private r: Router) {
    this.Core_URL = this.shareServices.Core_URL;
   
    
  }

  ngOnInit(): void {
   // this.dataTableConfigurationByLanguageId();
    let firstCol = this.mycoloums[0].data;
    if (firstCol === "check") {
      this.isCreateCheck = true;
    }
    
    this.shareServices.dataTableConfigurationByLanguageId()
    .subscribe(data=>{
      console.log(data);
      this.cdnList = data;
      console.log(data.languageId );
      console.log(data.sInfoEmpty );

    })
    const that = this;
    console.log("+++++++++++++++++++++++++++>>>>>>>>>>>>>>>");    
    console.log(this.cdnList.languageId );
    console.log(this.cdnList.sInfoEmpty + "+++++++++++++++++++++++++++>>>>>>>>>>>>>>>");
    
    
    this.language ={
     //url: "//cdn.datatables.net/plug-ins/1.10.19/i18n/Hindi.json",
     //url: "//cdn.datatables.net/plug-ins/1.10.19/i18n/English.json",
     //url: "//cdn.datatables.net/plug-ins/1.10.19/i18n/Tamil.json"
     zeroRecords: this.cdnList.sZeroRecords,
     paginate : this.cdnList.oPaginate,
     infoEmpty: this.cdnList.sInfoEmpty,
     lengthMenu : this.cdnList.sLengthMenu,
          

    }
    
   

    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 4,
      serverSide: true,
      processing: true,
      columns: this.mycoloums,
     // searching: false,
      language:this.language,
      

      
      ajax: (dataTablesParameters: any, callback) => {
        dataTablesParameters.transactionId = 7;
        that.http
          .post<DataTablesResponse>(
            this.myurl, dataTablesParameters, {}
          ).subscribe(resp => {

            that.gefields = resp.data;

            callback({
              recordsTotal: resp.recordsTotal,
              recordsFiltered: resp.recordsFiltered,
              data: []
            });
          });
      },
    };
  
  }

  /**
   * make grid cell editable  
   * @param e click event
   */
  private makeCellEdit(e) {
    e.target.parentNode.firstElementChild.setAttribute("class", "textBox");
    e.target.parentNode.firstElementChild.setAttribute("contentEditable", "true");
    //console.log("Parents parent sibling:", e.target.parentNode.firstElementChild.class = 'textBox');
  }

  /**
   * save data on focus out
   * @param event focus out
   * @param obj full row object
   * @param key edit cell value
   */
  private saveDetails(event, obj, key) {
    event.target.setAttribute("class", "");
    event.target.setAttribute("contentEditable", "false");
    let as: HTMLElement = event.target;
    console.log(obj);
    console.log(as.innerText);
    console.log(key.data);
    
    this.shareServices.dynamicInlineEditSave(obj,as.innerText,key.data)
    

   // this.editTranslationObj(obj, as.innerText, key);

  }

  private showUser(userEvent, gefield, userobj) {
    this.showPopUp = true;
    let colkey: any;
  }

  private closePopUp() {
    this.showPopUp = false;
  }

  private editCheckBox(event, object) {
    this.shareServices.setgridChangeMessage(object);
  }

  private editTranslationObj(editObj, editValue, key) {
    this.translationEditObj.emit({ "obj": editObj, "newEditValue": editValue, "languageId": key.data });
  }

  /**
   * claim task work only for Core
   * @param event
   * @param object 
   */
  private buttonClick(event, object, k) {
    if (k.data == 'reviewButton') {
      this.reviewTask(event, object, k);
    }
    else if (k.data == 'claimButton') {
      this.claimTask(event, object, k);
    }
  }

  private claimTask(event, object, k) {
    this.isDataLoad = false;
    this.http.get(this.Core_URL + '/claimTask/' + object.taskInstanceID)
      .subscribe(data => {
        this.isDataLoad = true;
        alert("Task Claimed !! please check your my task inbox");
      });
  }

  private reviewData: any;
  private reviewTask(event, object, k) {
    this.isDataLoad = false;
    this.http.get(this.Core_URL + '/getReviewTask/' + object.processId + '/' + object.taskInstanceID + '/' + object.formId)
      .subscribe(data => {
        this.reviewData = data;
        console.log(this.reviewData);
        this.shareServices.setgridChangeMessage(this.reviewData);
        this.r.navigateByUrl('process/' + object.processId + '/' + this.reviewData.nextTask + '/' + this.reviewData.nextfomId);
      });
  }

  private dataTableConfigurationByLanguageId(){
    this.shareServices.dataTableConfigurationByLanguageId()
    .subscribe(data=>{
      console.log(data);
      this.cdnList = data;
      console.log(this.cdnList.languageId );

    })
  }
}

