import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { FormServiceService } from '../../forms/form-services/form-service.service';
import { DynamicFormComponent } from '../components/dynamic-form/dynamic-form.component';
import { FieldConfig } from '../field.interface';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-form-factory',
  templateUrl: './form-factory.component.html',
  styleUrls: ['./form-factory.component.css']
})
export class FormFactoryComponent {
  jsonData : string = "";
  formName : string = "";
  regConfig : FieldConfig[];

  @ViewChild(DynamicFormComponent, {static: true}) form: DynamicFormComponent;

  constructor(private spinnerService: Ng4LoadingSpinnerService, public dialog: MatDialog, @Inject(MAT_DIALOG_DATA) data,private formService : FormServiceService){
    this.spinnerService.show(); 
    this.jsonData = data.jsonData;
      this.formName = data.formName;
      this.regConfig = data.regConfig;
      
  }

  ngAfterViewInit(){
    this.spinnerService.hide();
  }

/*   regConfig: FieldConfig[] = [
    {"type":"label","name":"Forms","label":"heading","inputType":"label","value":"Forms Help Text","validations":null,"options":null},    
    {"type": "multiSelect","name": "Salutation","label": "Salutation","inputType": "multiSelect","value": "","validations": null,"options" : ["Mr","Mrs","Dr","Miss","Er","Sir"]}
    
  ]; */

  /*  regConfig: FieldConfig[] = [
   {
     type: "input",
     label: "Username",
     inputType: "text",
     name: "name",
     validations: [
       {
         name: "required",
         validator: "Validators.required",
         message: "Name Required"
       },
       {
         name: "pattern",
         validator: Validators.pattern("^[a-zA-Z]+$"),
         message: "Accept only text"
       }
     ]
   },
   {
     type: "input",
     label: "Email Address",
     inputType: "email",
     name: "email",
     validations: [
       {
         name: "required",
         validator: Validators.required,
         message: "Email Required"
       },
       {
         name: "pattern",
         validator: Validators.pattern(
           "^[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,4}$"
         ),
         message: "Invalid email"
       }
     ]
   },
   {
     type: "input",
     label: "Password",
     inputType: "password",
     name: "password",
     validations: [
       {
         name: "required",
         validator: Validators.required,
         message: "Password Required"
       }
     ]
   },
   {
     type: "radiobutton",
     label: "Gender",
     name: "gender",
     options: ["Male", "Female"],
     value: "Male"
   },
   {
     type: "date",
     label: "DOB",
     name: "dob",
     validations: [
       {
         name: "required",
         validator: Validators.required,
         message: "Date of Birth Required"
       }
     ]
   },
   {
     type: "select",
     label: "Country",
     name: "country",
     value: "UK",
     options: ["India", "UAE", "UK", "US"]
   },
   {
     type: "checkbox",
     label: "Accept Terms",
     name: "term",
     value: true
   },
   {
     type: "button",
     label: "Save"
   }
 ];  */

  //@ViewChild(DynamicFormComponent) form: DynamicFormComponent;

  /* regConfig : FieldConfig[] = [
    {  
       "type":"select",
       "name":"state",
       "label":"State",
       "inputType":"select",
       "value":"",
       "validations":null,
       "options":[  
          "Tamilnadu",
          "Chhattisgarh",
          "Kerala",
          "MP"
       ]
    },
    {  
       "type":"date",
       "name":"dateOfBirth",
       "label":"Date Of Birth",
       "inputType":"date",
       "value":"",
       "validations":null,
       "options":null
    },
    {  
       "type":"radio",
       "name":"gender",
       "label":"Gender",
       "inputType":"radio",
       "value":"",
       "validations":null,
       "options":[  
          "Mr",
          "Mrs",
          "Dr",
          "Miss",
          "Er",
          "Sir"
       ]
    },
    {  
       "type":"select",
       "name":"department",
       "label":"Department",
       "inputType":"select",
       "value":"",
       "validations":null,
       "options":[  
          "Auckland",
          "Wellington",
          "Tokyo",
          "Hiroshima",
          "London",
          "Ireland",
          "Tamilnadu",
          "Rajasthan",
          "Kerala",
          "Andhra Pradesh",
          "Telagana",
          "Goa",
          "Gujarat",
          "Himachal Pradesh",
          "Uttar Pradesh",
          "Jammu & Kashimar"
       ]
    },
    {  
       "type":"input",
       "name":"AdLine1",
       "label":" Address Line 1",
       "inputType":"textarea",
       "value":"",
       "validations":null,
       "options":null
    },
    {  
       "type":"input",
       "name":"addressLine2",
       "label":"Address Line 2",
       "inputType":"textarea",
       "value":"",
       "validations":null,
       "options":null
    },
    {  
       "type":"input",
       "name":"contactN",
       "label":"Contact Number",
       "inputType":"text",
       "value":"",
       "validations":null,
       "options":null
    },
    {  
       "type":"select",
       "name":"salutation",
       "label":"Salutation",
       "inputType":"select",
       "value":"",
       "validations":null,
       "options":[  
          "demosheet_sh_101",
          "demosheet_sh_201",
          "demosheet_sh_301"
       ]
    },
    {  
       "type":"input",
       "name":"firstName",
       "label":"First Name",
       "inputType":"text",
       "value":"",
       "validations":null,
       "options":null
    },
    {  
       "type":"input",
       "name":"middleName",
       "label":"Middle Name",
       "inputType":"text",
       "value":"",
       "validations":null,
       "options":null
    },
    {  
       "type":"input",
       "name":"lastName",
       "label":"Last Name",
       "inputType":"text",
       "value":"",
       "validations":null,
       "options":null
    },
    {  
       "type":"select",
       "name":"age",
       "label":"Age",
       "inputType":"select",
       "value":"",
       "validations":null,
       "options":[  
          "demosheet_sh_101",
          "demosheet_sh_201",
          "demosheet_sh_301"
       ]
    },
    {  
       "type":"input",
       "name":"name",
       "label":"Name",
       "inputType":"text",
       "value":"",
       "validations":null,
       "options":null
    },
    {  
       "type":"button",
       "name":"submit",
       "label":"Submit",
       "inputType":"button",
       "value":"",
       "validations":null,
       "options":null
    },
    {  
       "type":"button",
       "name":"cancel",
       "label":"Cancel",
       "inputType":"button",
       "value":"",
       "validations":null,
       "options":null
    }
 ]; */
    

   ngOnInit() {
    console.log("this is the start");
    

    /* for (var obj in this.regConfig){
      console.log("Json : "+JSON.stringify(this.regConfig[obj]).replace('"','').replace('"',''));
      //FieldConfig fc = new FieldConfig();
      const test = JSON.stringify(this.regConfig[obj]).replace('"','').replace('"','');
      //this.regConfig2 = test;
      console.log("adfasdf  : "+JSON.stringify(this.regConfig2));
      
      //this.regConfig2.push();
    }    */
  /*    this.regConfig = [
     /*  {
        "type": "label",
        "name": "picture",
        "label": "Picture",
        "inputType" : "label",
        "value": "TESTTEST"
      }, 
      {
          "type": "radiobutton",
          "name": "Salutation",
          "label": "Salutation",
          "inputType": "radiobutton",
          "value": "",
          "validations": null,
          "options": [
              "Mr",
              "Mrs",
              "Dr",
              "Miss",
              "Er",
              "Sir"
          ]
      },
      {
        "type": "select",
        "name": "Salutation",
        "label": "Salutation",
        "inputType": "select",
        "value": "",
        "validations": null,
        "options": [
            "Mr",
            "Mrs",
            "Dr",
            "Miss",
            "Er",
            "Sir"
        ]
    },
      {
          "type": "dropdown",
          "name": "State",
          "label": "State",
          "inputType": "dropdown",
          "value": "",
          "validations": null,
          "options": null
      },
      {
        "type": "file",
        "name": "picture",
        "label": "Picture",
        "inputType" : "file"
      },
      {
        "type": "multicheckbox",
        "name": "picture",
        "label": "Picture",
        "inputType" : "multicheckbox",
        "options": [
          "Mr",
          "Mrs",
          "Dr",
          "Miss",
          "Er",
          "Sir"
      ]
      } ,
      {
          "type": "button",
          "name": "Submit",
          "label": "Submit",
          "inputType": "button",
          "value": "",
          "validations": null,
          "options": null
      },
      {
          "type": "button",
          "name": "Cancel",
          "label": "Cancel",
          "inputType": "button",
          "value": "",
          "validations": null,
          "options": null
      }
  ];  */
    /*    */
 /*  regConfig: FieldConfig[] = [
    {
        "type": "input",
        "name": "testField",
        "label": "testField",
        "inputType": "text",
        "value": "",
        "validations": null,
        "options": null
    },
    {
        "type": "input",
        "name": "testField2",
        "label": "testField2",
        "inputType": "textarea",
        "value": "",
        "validations": null,
        "options": null
    },
    {
        "type": "button",
        "name": "Submit",
        "label": "Submit",
        "inputType": "button",
        "value": "",
        "validations": null,
        "options": null
    },
    {
        "type": "button",
        "name": "Cancel",
        "label": "Cancel",
        "inputType": "button",
        "value": "",
        "validations": null,
        "options": null
    }
]; */
  

  
  }

}//end of the class
