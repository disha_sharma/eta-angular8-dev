export class Validator {
  
  name: string;
  validator: any;
  message: string;
  constructor(name : string, validator: any, message : string){
      this.name = name;
      this.validator = validator;
      this.message = message;
  }
}


export interface FieldConfig {
  label?: string;
  name?: string;
  inputType?: string;
  options?: string[];
  collections?: any;
  type: string;
  value?: any;
  hint?: any;
  validations?: Validator[];
}
