import { Component, OnInit } from "@angular/core";
import { FormGroup } from "@angular/forms";
import { FieldConfig } from "../../field.interface";
@Component({
  selector: "app-checkbox",
  template: `
<div class="demo-full-width margin-top checkbox-div" [formGroup]="group" >
<mat-checkbox class="custom-checkbox-without-margin" [formControlName]="field.name">{{field.label}}</mat-checkbox>
<mat-hint>{{field.hint}}</mat-hint>
</div>
`,
  styles: []
})
export class CheckboxComponent implements OnInit {
  field: FieldConfig;
  group: FormGroup;
  constructor() {}
  ngOnInit() {}
}
