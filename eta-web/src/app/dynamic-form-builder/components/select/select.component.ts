import { Component, OnInit } from "@angular/core";
import { FormGroup } from "@angular/forms";
import { FieldConfig } from "../../field.interface";
@Component({
  selector: "app-select",
  template: `
  <div class="custom-control-row">
  <div class="custom-select custom-field custom-field-fw">
<mat-form-field appearance="outline"   class="demo-full-width margin-top" [formGroup]="group">
<mat-label>{{field.label}}</mat-label>
<mat-select disableOptionCentering panelClass="custom-select-panel" [placeholder]="field.label" [formControlName]="field.name">
<mat-option *ngFor="let item of field.options" [value]="item">{{item}}</mat-option>
</mat-select>
<mat-hint>{{field.hint}}</mat-hint>
<ng-container *ngFor="let validation of field.validations;" ngProjectAs="mat-error">
<mat-error *ngIf="group.get(field.name).hasError(validation.name)">{{validation.message}}</mat-error>
</ng-container>
</mat-form-field>
</div>
</div>
`,
  styles: []
})
export class SelectComponent implements OnInit {
  field: FieldConfig;
  group: FormGroup;
  constructor() {}
  ngOnInit() {}
}