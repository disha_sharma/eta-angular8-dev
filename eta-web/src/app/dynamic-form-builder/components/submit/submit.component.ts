import { Component, OnInit } from "@angular/core";
import { FormGroup } from "@angular/forms";
import { FieldConfig } from "../../field.interface";
@Component({
  selector: "app-submit",
  template: `
            <div class="demo-full-width button-div margin-top" [formGroup]="group">
            <button class="button" type="submit">{{field.label}}</button>
            </div>
            `,
  styles: []
})
export class SubmitComponent implements OnInit {
  field: FieldConfig;
  group: FormGroup;
  constructor() {

  }
  ngOnInit() {
    
  }
}
