import { Component, OnInit } from "@angular/core";
import { FormGroup } from "@angular/forms";
import { FieldConfig } from "../../field.interface";
@Component({
  selector: "app-file",
  template: `

  <div class="custom-control-row">
  <div class="file-upload-container custom-field">
    
    <!-- real input -->
  <input #myInput hidden   [(ngModel)]="fileValue" [ngModelOptions]="{standalone: true}" type="file" name="file" id="file"
  class="input-file" (change)='fileInputChanged($event,field.name)'/>
  <div class="custom-input-label">
    <label class="label" (click)="myInput.click()"></label>
    <mat-form-field appearance="outline" class="demo-full-width" [formGroup]="group">
    <mat-label>Browse</mat-label>
         <input matInput readonly [formControlName] = "field.name"  multiple="" (change)="field.onUpload($event.target.files,field.name)"> 
         <mat-hint>{{field.hint}}</mat-hint>
         <mat-icon matSuffix>insert_drive_file</mat-icon>
         <ng-container *ngFor="let validation of field.validations;" ngProjectAs="mat-error">
         <mat-error *ngIf="group.get(field.name).hasError(validation.name)">{{validation.message}}</mat-error>
         </ng-container>
         </mat-form-field>
  </div>
  
  <!-- upload and toggle container starts -->
  <div class="control-button-container">
  <button type="button" class="control-button upload-button">
    <i class="material-icons">
        cloud_upload
        </i>
        <div [style.height.%]="progressBarValue"
         class="custom-progress-division"></div>
  </button>
  </div>
  <div class="control-button-container">
  <button type="button"  class="control-button" (click)="hide = !hide" >
    <i class="material-icons" >{{hide ? 'visibility_off' : 'visibility'}}
      </i>
  </button>
  
  </div>
    </div>
  </div>
`,
  styles: []
})
export class FileComponent implements OnInit {
  field: FieldConfig;
  group: FormGroup;
  fileUploaded: string = "";
  fileUploadButton: boolean = false;
  selectedFiles: FileList;
  progressBarValue = 0;
  fileValue: string = "";
  constructor() {}
  ngOnInit() {

  }

  /*
  This function is use for When File Input Change.
  */
 fileInputChanged(event,name) {
  this.fileUploaded = "";
  this.fileUploadButton = false;
  if (event && event.target && event.target.files) {
    this.selectedFiles = event.target.files;
  }
  this.progressBarValue = 0;
  this.group.controls[name].markAsTouched();
  this.group.controls[name].setValue(this.fileValue.split('\\').pop());
}

setFileValue(){
  this.group.controls['file'].setValue(this.fileValue.split('\\').pop());
}
}

// <mat-form-field class="demo-full-width" [formGroup]="group">
//  <label class="upload-button">
//               <input type="file" multiple="" (change)="field.onUpload($event.target.files)"> browse
//             </label>
// <ng-container *ngFor="let validation of field.validations;" ngProjectAs="mat-error">
// <mat-error *ngIf="group.get(field.name).hasError(validation.name)">{{validation.message}}</mat-error>
// </ng-container>
// </mat-form-field>
