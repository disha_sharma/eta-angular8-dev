import { Component, OnInit } from "@angular/core";
import { FormGroup } from "@angular/forms";
import { FieldConfig } from "../../field.interface";

@Component({
  selector: 'app-label',
  template: `
  <mat-label class="body-text1">{{field.label}}</mat-label><br>
  <mat-hint>{{field.hint}}</mat-hint>
  `,
  styleUrls: ['./label.component.css']
})
export class LabelComponent implements OnInit {
  field: FieldConfig;
  group: FormGroup;
  constructor() {}
  ngOnInit() {}

}
