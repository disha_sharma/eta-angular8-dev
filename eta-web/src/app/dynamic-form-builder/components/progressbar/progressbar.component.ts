import { Component, OnInit } from "@angular/core";
import { FormGroup } from "@angular/forms";
import { FieldConfig } from "../../field.interface";

@Component({
  selector: 'app-progressbar',
  template: `
  <div class="custom-control-row">
      <mat-progress-bar mode="determinate" value="40"></mat-progress-bar>
  </div>
  `,
  styleUrls: ['./progressbar.component.css']
})
export class ProgressBarComponent implements OnInit {
  field: FieldConfig;
  group: FormGroup;
  constructor() {}
  ngOnInit() {}

}
