import { Component, OnInit } from "@angular/core";
import { FormGroup } from "@angular/forms";
import { FieldConfig, Validator } from "../../field.interface";
@Component({
  selector: "app-hidden",
  template: `
  <div class="custom-control-row" style="display: none">
  <div class="custom-field custom-field-fw">
        <mat-form-field [formGroup]="group">
            <input matInput [formControlName]="field.name" [placeholder]="field.label" [type]="field.inputType">
        </mat-form-field>
  </div>
</div>
`,
  styles: []
})
export class HiddenComponent implements OnInit {
  field: FieldConfig;
  group: FormGroup;
  validatorArr : Validator[] = [];
  constructor() {

  }

  ngOnInit() {
    console.log("before render"+this.field.validations);
 
    for (var obj in this.field.validations){
        console.log(" validations : "+this.field.validations[obj]);
       // console.log(" validations : "+this.field.validations[obj]);
     
        console.log(" validations : "+JSON.stringify(this.field.validations[obj]));
    }
  }//end of the method


}//end of the class
