import { Component, OnInit } from "@angular/core";
import { FormGroup } from "@angular/forms";
import { FieldConfig } from "../../field.interface";
@Component({
  selector: "app-textarea",
  template: `
  <div class="custom-control-row">
      <div class="custom-text-area">
          <mat-form-field appearance="outline" class="demo-full-width" [formGroup]="group">
          <mat-label>{{field.label}}</mat-label>
          <textarea matInput [formControlName]="field.name" [placeholder]="field.label" [type]="field.inputType"></textarea>
          <mat-hint>{{field.hint}}</mat-hint>
          <ng-container *ngFor="let validation of field.validations;" ngProjectAs="mat-error">
          <mat-error *ngIf="group.get(field.name).hasError(validation.name)">{{validation.message}}</mat-error>
          </ng-container>
          </mat-form-field>
      </div>
</div>
`,
  styles: []
})
export class TextAreaComponent implements OnInit {
  field: FieldConfig;
  group: FormGroup;
  constructor() {} 
  ngOnInit() {}
}
