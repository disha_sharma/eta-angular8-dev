import { Component, OnInit } from "@angular/core";
import { FormGroup, FormControl } from "@angular/forms";
import { FieldConfig } from "../../field.interface";

@Component({
selector: 'master-data-list-material',
template: `<div class="custom-control-row">
<div class="custom-select custom-field custom-field-fw">
<mat-form-field appearance="outline">
<mat-label>{{field.label}}</mat-label>
<mat-nav-list>
   <a mat-list-item href="..." *ngFor="let item of field.options"> {{ item }} </a>
</mat-nav-list>
<mat-hint>{{field.hint}}</mat-hint>
</mat-form-field>
</div>
</div>`,



})
export class MasterDataListComponent implements OnInit {
field: FieldConfig;
group: FormGroup;
constructor() {}
  ngOnInit() {
  }
  }