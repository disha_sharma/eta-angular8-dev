import { Component, OnInit } from "@angular/core";
import { FormGroup } from "@angular/forms";
import { FieldConfig } from "../../field.interface";
@Component({
  selector: "app-button",
  template: `
            <div class="demo-full-width button-div margin-top" [formGroup]="group">
            <button class="button" type="button">{{field.label}}</button>
            </div>
            `,
  styles: []
})
export class ButtonComponent implements OnInit {
  field: FieldConfig;
  group: FormGroup;
  constructor() {

  }
  ngOnInit() {
    
  }
}
