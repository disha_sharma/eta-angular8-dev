import { Component, OnInit } from "@angular/core";
import { FormGroup } from "@angular/forms";
import { FieldConfig, Validator } from "../../field.interface";
@Component({
  selector: "app-input",
  template: `
  <div class="custom-control-row">
      <div class="custom-field custom-field-fw">
            <mat-form-field appearance = "outline" class="demo-full-width" [formGroup]="group">
            <mat-label>{{field.label}}</mat-label>
            <input matInput [formControlName]="field.name" [placeholder]="field.label" [type]="field.inputType">
            <mat-hint>{{field.hint}}</mat-hint>
            <ng-container *ngFor="let validation of field.validations;" ngProjectAs="mat-error">
                  <mat-error *ngIf="group.get(field.name).hasError(validation.name)">{{validation.message}}</mat-error>
            </ng-container>
            </mat-form-field>
      </div>
</div>
`,
  styles: []
})
export class InputComponent implements OnInit {
  field: FieldConfig;
  group: FormGroup;
  validatorArr : Validator[] = [];
  constructor() {

  }

  ngOnInit() {
    console.log("before render"+this.field.validations);
 
    for (var obj in this.field.validations){
        console.log(" validations : "+this.field.validations[obj]);
       // console.log(" validations : "+this.field.validations[obj]);
     
        console.log(" validations : "+JSON.stringify(this.field.validations[obj]));
    }
  }//end of the method


}//end of the class
