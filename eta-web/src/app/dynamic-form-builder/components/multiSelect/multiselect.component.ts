import { Component, OnInit } from "@angular/core";
import { FormGroup, FormControl } from "@angular/forms";
import { FieldConfig } from "../../field.interface";

@Component({
selector: 'multiple-material-select-example',
template: `<div class="custom-control-row">
<div class="custom-select custom-field custom-field-fw">
<mat-form-field appearance="outline">
<mat-label>{{field.label}}</mat-label>
<mat-select  placeholder="field.label" multiple  disableOptionCentering panelClass="custom-select-panel">
  <mat-option *ngFor="let item of field.options" [value]="item">{{item}}</mat-option>
</mat-select>
<mat-hint>{{field.hint}}</mat-hint>
</mat-form-field>
</div>
</div>`,



})
export class MultipleMaterialSelectComponent implements OnInit {
field: FieldConfig;
group: FormGroup;
constructor() {}
  ngOnInit() {
  }
  }