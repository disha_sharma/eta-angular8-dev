import {
  ComponentFactoryResolver,
  ComponentRef,
  Directive,
  Input,
  OnInit,
  ViewContainerRef
} from "@angular/core";
import { FormGroup } from "@angular/forms";
import { FieldConfig } from "../../field.interface";
import { InputComponent } from "../input/input.component";
import { ButtonComponent } from "../button/button.component";
import { SelectComponent } from "../select/select.component";
import { DateComponent } from "../date/date.component";
import { RadiobuttonComponent } from "../radiobutton/radiobutton.component";
import { CheckboxComponent } from "../checkbox/checkbox.component";
import { FileComponent } from "../file/file.component";
import { MultiCheckboxComponent } from "../multicheckbox/multicheckbox.component";
import { MultipleMaterialSelectComponent } from "../multiSelect/multiselect.component";
import { LabelComponent } from "../label/label.component";
import { SubmitComponent } from "../submit/submit.component";
import { CancelComponent } from "../cancel/cancel.component";
import { HiddenComponent } from "../hidden/hidden.component";
import { ProgressBarComponent } from "../progressbar/progressbar.component";
import { TextAreaComponent } from "../textarea/textarea.component";
import { MasterDataListComponent } from "../masterDataList/masterdatalist.component";

const componentMapper = {
  input: InputComponent,
  button: ButtonComponent,
  select: SelectComponent,
  date: DateComponent,
  radiobutton: RadiobuttonComponent,
 // checkbox: CheckboxComponent,
  file: FileComponent,
  checkbox: MultiCheckboxComponent,
  multiSelect : MultipleMaterialSelectComponent,
  label : LabelComponent,
  submit : SubmitComponent,
  cancel : CancelComponent,
  hidden : HiddenComponent,
  progressbar : ProgressBarComponent,
  textarea : TextAreaComponent,
  masterDataList : MasterDataListComponent,
};



@Directive({
  selector: "[dynamicField]"
})
export class DynamicFieldDirective implements OnInit {
  @Input() field: FieldConfig;
  @Input() group: FormGroup;
  componentRef: any;
  constructor(
    private resolver: ComponentFactoryResolver,
    private container: ViewContainerRef
  ) {}
  ngOnInit() {
    const factory = this.resolver.resolveComponentFactory(
      componentMapper[this.field.type]
    );
    this.componentRef = this.container.createComponent(factory);
    this.componentRef.instance.field = this.field;
    this.componentRef.instance.group = this.group;
  }
}
