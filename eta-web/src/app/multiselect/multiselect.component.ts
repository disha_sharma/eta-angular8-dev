import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Http, Headers, RequestOptions } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { ServiceService } from './../eta-services/service.service'
import { templateJitUrl } from '@angular/compiler';

@Component({
  selector: 'app-multiselect',
  templateUrl: './multiselect.component.html',
  styleUrls: ['./multiselect.component.css'],
  providers: [ServiceService]
})
export class MultiselectComponent implements OnInit {
  dropdownList = [];
  selectedItems = [];
  dropdownSettings = {};
  orgIdOnChange;
  selectedData = [];
 

  @Input() myorgId: number;
  constructor(private _formBuilder: FormBuilder, private http: HttpClient, private newService: ServiceService) {  
  }
  countrylist: any[];
  _orgId: any[];

  showOrgValueForEditOrgDetails(change) {
    this.orgIdOnChange = change;

  }
  /***************************************************************************Fetch All Country Location******************************************/
  ngOnInit() {
    this.getLocationByOrgId(this.myorgId)    
    this.newService.getAllCountry()
      .subscribe(data => {   

        data.forEach(element => {
          let id = element.countryId;
          let countryName = element.countryName;
          let countryObj: any = {};
          countryObj.id = id;
          countryObj.itemName = countryName;         
          this.dropdownList.push(countryObj);
        });

      });


    this.selectedItems = this.selectedData;

    this.dropdownSettings = {
      singleSelection: false,
      text: "Select Countries",
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      enableSearchFilter: true,
      classes: "myclass custom-class"
    };


  }

  /**************************************************************Edit Country Location Details*************************************************/
  getLocationByOrgId(myorgId) {
    this.newService.getLocationByOrgId(this.myorgId).subscribe(data => {
      data.forEach(element => {
        let id = element.location;
        let location = element.translation;
        let countryLocationObj: any = {};
        countryLocationObj.id = id;
        countryLocationObj.itemName = location;        
        this.selectedData = countryLocationObj;
        this.selectedItems.push(countryLocationObj);    

      });

    });

  }

  /***********************************************************Save Country Location*******************************************************************************/
  onSubmitCountry() {
    
    let location = [];
    this.selectedItems.forEach(element => {     
      let obj: any = {};
      obj['location'] = element.id;
      location.push(obj);
    });

    let orgCountryObj: Object = {};
    orgCountryObj["orgId"] = this.myorgId;
    orgCountryObj["location"] = location; 
    this.newService.postOrgWrapperLocation(orgCountryObj);
  }

}
