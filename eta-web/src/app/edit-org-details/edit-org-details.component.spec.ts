import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditOrgDetailsComponent } from './edit-org-details.component';

describe('EditOrgDetailsComponent', () => {
  let component: EditOrgDetailsComponent;
  let fixture: ComponentFixture<EditOrgDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditOrgDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditOrgDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
