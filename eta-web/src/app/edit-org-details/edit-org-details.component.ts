import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroupDirective, ValidatorFn, AbstractControl } from '@angular/forms';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ArrayType } from '@angular/compiler/src/output/output_ast';
import { HttpClient } from '@angular/common/http';
import { ServiceService } from '../eta-services/service.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import {startWith, map} from 'rxjs/operators';

export const _filter = (opt: string[], value: string): string[] => {
  const filterValue = value.toLowerCase();

  return opt.filter(item => item.toLowerCase().indexOf(filterValue) === 0);
};


@Component({
  selector: 'app-edit-org-details',
  templateUrl: './edit-org-details.component.html',
  styleUrls: ['./edit-org-details.component.css']
})
export class EditOrgDetailsComponent implements OnInit {

  form: FormGroup;
  disableSameAddress: boolean = false;
  showSuccess: boolean = false;
  orgDetail: any;
  notEditable:boolean=false;
  id: any = 0;
  url:any;
  allOrgName:any=[];
  allAcronym:any=[];
  allEmailaddress:any=[];
  allOrgDomain:any=[];
  orgNameOptions: Observable<any[]>;


  constructor(private fb: FormBuilder, private http: HttpClient, private taskService: ServiceService, private activatedRoute: ActivatedRoute, private router: Router) {
    this.url = this.taskService.Org_Cal_URL;
    this.id = this.activatedRoute.snapshot.queryParams['orgId'];
    this.editForm();
    this.separateUniqueFields();
  }
  ngOnInit() {

    this.form = this.newform();
    this.orgNameOptions = this.form.get('orgName')!.valueChanges
      .pipe(
        startWith(''),
        map(value => value.length >= 1 ? this._filterGroup(value):[])
      );
  }

  private _filterGroup(value: string) {
    if (value) {
      return this.allOrgName
        .map(group => ({names: _filter(group.names, value)}))
        .filter(group => group.names.length > 0);
    }

    return this.allOrgName;
  }

  newform(): FormGroup {
    return this.fb.group({
      orgId:[0],
      acronym: ['GTC', Validators.required],
      orgName: ['GreenTech Consulting LLC', Validators.required],

      orgTypeName: ['1', Validators.required],
      orgDomain: ['greentechict.com, greentechconsulting.biz, greentechconsulting.ae', Validators.required],
      addressLine1: ['412 Main Street', Validators.required],
      addressLine2: ['Suit 100', Validators.required],
      country: ['1', Validators.required],
      state: ['1', Validators.required],
      city: ['Buffalo', Validators.required],
      zipCode: ['82834', Validators.required],

      salutation: ['1', Validators.required],
      firstName: ['Deepak', Validators.required],
      middleName: [],
      lastName: ['Jain', Validators.required],
      suffix: [],

      position: ['Cheif Executive Officer', Validators.required],
      emailaddress: ['dj@greentechict.com', Validators.required],

      phoneNumber: ['+1 (150) 258-9467', Validators.required],
      workPhone: ['+1 (307) 365-6028', Validators.required],
      faxNumber: [],

      sameAsRegisteredAddress: [],
      mailAddrLine1: ['', Validators.required],
      mailAddrLine2: ['', Validators.required],

      mailingAddressCountry: ['', Validators.required],
      mailingAddressState: ['', Validators.required],
      mailingAddressCity: ['', Validators.required],
      mailingAddressZipCode: ['', Validators.required]
    })
  }

  separateUniqueFields(){
    this.http.get(this.url + "/getAllOrgDetails").subscribe(res => {
      let allOrgDetails = res
      for(let i=0;i<Object.keys(allOrgDetails).length;i++){
      this.allEmailaddress.push(allOrgDetails[i].emailaddress);
      this.allAcronym.push(allOrgDetails[i].acronym);
      this.allOrgDomain.push(allOrgDetails[i].orgDomain);
      if(allOrgDetails[i].orgName != null){
      this.allOrgName.push(allOrgDetails[i].orgName)}
    }

    let deletedListId: any = {};
       deletedListId.names = this.allOrgName
       this.allOrgName = [deletedListId]
     console.log('deletedListId',this.allOrgName);
    
    //   console.log('allEmailaddress',this.allEmailaddress);
    //   console.log('allAcronym',this.allAcronym);
    //   console.log('allOrgDomain',this.allOrgDomain);
    //   console.log('allOrgName',this.allOrgName);
    })
  }

  editForm() {
    if (this.id != undefined) {
      this.http.get(this.url + "/getOrgByOrgId/" + this.id).subscribe(res => {
        this.orgDetail = res
        console.log(this.orgDetail);


        this.form.patchValue({

          orgId: this.orgDetail.orgId,
          acronym: this.orgDetail.acronym,
          select: this.orgDetail.select,
          orgName: this.orgDetail.orgName,

          orgTypeName: this.orgDetail.orgTypeName,
          orgDomain: this.orgDetail.orgDomain,
          addressLine1: this.orgDetail.addressLine1,
          addressLine2: this.orgDetail.addressLine2,
          country: this.orgDetail.country,
          state: this.orgDetail.state,
          city: this.orgDetail.city,
          zipCode: this.orgDetail.zipCode,

          salutation: this.orgDetail.salutation,
          firstName: this.orgDetail.firstName,
          middleName: this.orgDetail.middleName,
          lastName: this.orgDetail.lastName,
          suffix: this.orgDetail.suffix,

          position: this.orgDetail.position,
          emailaddress: this.orgDetail.emailaddress,

          phoneNumber: this.orgDetail.phoneNumber,
          workPhone: this.orgDetail.workPhone,
          faxNumber: this.orgDetail.faxNumber,

          sameAsRegisteredAddress: this.checkRegisteredAddress(this.orgDetail.sameAsRegisteredAddress),
          mailAddrLine1: this.orgDetail.mailAddrLine1,
          mailAddrLine2: this.orgDetail.mailAddrLine2,

          mailingAddressCountry: this.orgDetail.mailingAddressCountry,
          mailingAddressState: this.orgDetail.mailingAddressState,
          mailingAddressCity: this.orgDetail.mailingAddressCity,
          mailingAddressZipCode: this.orgDetail.mailingAddressZipCode
        })
        this.notEditable = true;
      })
    }
    else {
      this.newform();
    }
  }

  checkRegisteredAddress(sameAsRegisteredAddress){
    if(sameAsRegisteredAddress == true){
      this.disableSameAddress = true;
    }
    return this.orgDetail.sameAsRegisteredAddress
  }

  printForm() {
    console.log(this.form.value);

  }

  registeredAddress() {
    
    if (this.form.controls['sameAsRegisteredAddress'].value === true) {
      this.disableSameAddress = true;
      // let control = <FormArray>this.form.controls['countryStateCityPostalCodeFormArray'];
      // let secondcontrol = control.controls[0];
      this.form.patchValue({
        mailAddrLine1: this.form.get('addressLine1').value,
        mailAddrLine2: this.form.get('addressLine2').value,
        // mailingAddressCountryStateCityPostalCodeFormArray: [{
        mailingAddressCountry: this.form.get('country').value,
        mailingAddressState: this.form.get('state').value,
        mailingAddressCity: this.form.get('city').value,
        mailingAddressZipCode: this.form.get('zipCode').value
        // }]
      })

    }
    else {

      this.disableSameAddress = null
      // let control = <FormArray>this.form.controls['mailingAddressCountryStateCityPostalCodeFormArray'];
      // control.reset();
      this.form.get('mailAddrLine1').reset(),
        this.form.get('mailAddrLine2').reset(),
        this.form.get('mailingAddressCountry').reset(),
        this.form.get('mailingAddressState').reset()
      this.form.get('mailingAddressCity').reset(),
        this.form.get('mailingAddressZipCode').reset()
    }

  }

  onSubmit(formDirective: FormGroupDirective) {
    let url = this.taskService.Org_URL
    let p = Object.assign({}, this.form.value)
    // console.log('form value',p);
    this.http.post(url + "/setOrgDetails", p).subscribe(data => {
      let details = data

      this.onSaveComplete(details, formDirective);
    })

  }

  onSaveComplete(details, formDirective) {
    console.log("details", details);
    formDirective.resetForm();
    this.form.reset();
    this.showSuccess = true;
    this.router.navigate(['/orgDetailsList']);
  }

  removeSuccess() {
    this.showSuccess = false
  }
  print(){
    console.log(this.form.value);
    
  }
}
