import { Component, OnInit, Input, ViewChild, Output, EventEmitter } from '@angular/core';
import { Headers, RequestOptions } from '@angular/http';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { ServiceService } from './../eta-services/service.service'
import { PopupTemplateComponent } from '../popup-template/popup-template.component';
//import { locateHostElement } from '@angular/core/src/render3/instructions';




class DataTablesResponse {
  data: any[];
  draw: number;
  recordsFiltered: number;
  recordsTotal: number;
  coloumnName: {};
}


@Component({
  selector: 'app-dynamic-datatable-edit',
  templateUrl: './dynamic-datatable-edit.component.html',
  styleUrls: ['./dynamic-datatable-edit.component.css']
})
export class DynamicDatatableEditComponent implements OnInit {

  orgId: number;
  @Input() myurl: string;
  @Input() mycoloums = [];
  @Input() mySubmitUrl: string;
  @Input() checkboxId: any;
  @Input() checkboxOrgid: any;
  @Input() orgIdSend: any;

  @Output() checkBoxOutput = new EventEmitter();


  dtOptions: DataTables.Settings = {};
  gefields: any[];
  public arrayOfKeys = [];
  public arrayOfValues = [];
  public hyperlinkArray = [];
  public editArray = [];
  public planTextArray = [];
  public checkId: string;
  public checkOrgid: string;
  userDetials: any = {};
  userIdInfo: any;
  orgDetials: any = {};
  colkey: any;
  ETA_SELECTED_ORGID: any = 0;
  constructor(private http: HttpClient, private newService: ServiceService) {
    
  }

  ngOnInit(): void {    
    if (localStorage.getItem("EtaSelectOrgId") != null) {
      this.ETA_SELECTED_ORGID = localStorage.getItem("EtaSelectOrgId");
    }


    //console.log(this.myurl + "**********ngOnInit********" + this.mycoloums);
    //console.log("*********************!!!!!!!!!!!!!!!!***********" + this.ETA_SELECTED_ORGID);
    if (this.orgIdSend == null) {
      this.orgId = 0;
      //console.log(this.orgIdSend);
    }
    else {
      this.orgId = this.orgIdSend;
    }

    //console.log(this.orgId);


    this.checkId = this.checkboxId;
    this.checkOrgid = this.checkboxOrgid;
    //console.log("______________ngOnInit________________");
    //console.log(this.myurl);
    //console.log(this.mycoloums)
    //console.log(this.mySubmitUrl);
    //console.log(this.checkboxId);
    //console.log(this.checkboxOrgid);
    //console.log(this.orgIdSend);



    //console.log("_____________ngOnInit End____________");
    for (let index = 0; index < this.mycoloums.length; index++) {
      const element = this.mycoloums[index];
      this.arrayOfKeys[index] = element.data;
      /* Added By Nitin for display Name*/
      this.arrayOfValues[index] = element.displayName;
    }
    for (let index = 0; index < this.mycoloums.length; index++) {
      const element = this.mycoloums[index];
      this.hyperlinkArray[index] = element.hyperlink;

    }
    for (let index = 0; index < this.mycoloums.length; index++) {
      const element = this.mycoloums[index];
      this.editArray[index] = element.edit;

    }
    for (let index = 0; index < this.mycoloums.length; index++) {
      const element = this.mycoloums[index];
      this.planTextArray[index] = element.plantext;

    }


    const that = this;

    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 4,
      serverSide: true,
      processing: true,
      columns: this.mycoloums,

      // columns: [{ data: 'functionName' }, { data: 'functionDescription' }, { data: 'functionID' }],
      ajax: (dataTablesParameters: any, callback) => {
        dataTablesParameters.orgId = this.ETA_SELECTED_ORGID;
        that.http.post<DataTablesResponse>(
          this.myurl, dataTablesParameters, {}
        ).subscribe(resp => {
          that.gefields = resp.data;
          this.arrayOfKeys = Object.keys(resp.coloumnName);
          //console.log("Key----------------" + this.arrayOfKeys);
          //console.log("Value----------------" + this.arrayOfValues);
          callback({
            recordsTotal: resp.recordsTotal,
            recordsFiltered: resp.recordsFiltered,
            data: []
          });
        });
      },
    };
  }
  makeCellEdit(e) {
    e.target.parentNode.firstElementChild.setAttribute("class", "textBox");
    e.target.parentNode.firstElementChild.setAttribute("contentEditable", "true");
    //console.log("Parents parent sibling:", e.target.parentNode.firstElementChild.class = 'textBox');
  }

  saveDetails(event, obj, key) {
    event.target.setAttribute("class", "");
    event.target.setAttribute("contentEditable", "false");
    //console.log(key + "++++++++++5555++++++++");

    let as: HTMLElement = event.target;
    obj[key] = as.innerText;
    obj["modifiedBy"] = '2';
    //console.log("**your updated object is**");
    //console.log(obj);

    this.http.post(this.mySubmitUrl, obj)
      .subscribe(data => {
        //console.log("details saved");
        //console.log(data);
      });
  }

  showUser(userEvent, userobj, k) {


    //console.log(userEvent + "+++++++++++++666++++++++++++++++++" + k);


    if (k == "modifiedName") {
      this.colkey = 'user';
      this.finduser(userobj.modifiedBy)
      //console.log(this.colkey);
    }
    if (k == "createdName") {
      this.colkey = 'user';
      this.finduser(userobj.createdBy)
    }
    if (k == "createdBy") {
      this.colkey = 'user';
      this.finduser(userobj.createdBy)
    }
    if (k == "orgName") {
      this.colkey = 'org';
      this.findOrg(userobj.orgId)
    }

    if (k == "fullName") {
      this.colkey = 'user';
      this.finduser(userobj.userId)
      //console.log(this.colkey);
    }
  }


  editCheckBox(event) {
    this.checkBoxOutput.emit(event.target.value);
    //console.log(event.target.value + "check box value ");

  }

  finduser(userid) {
    this.newService.getUserByUserId(userid)
      .subscribe(data => {
        //console.log(data);
        this.userDetials = data;
      })
  }
  findOrg(orgId) {

    this.newService.getOrgDetailsByOrgId(orgId)
      .subscribe(data => {
        //console.log(data);
        this.orgDetials = data;
      })
  }
}
