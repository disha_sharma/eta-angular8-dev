import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DynamicDatatableEditComponent } from './dynamic-datatable-edit.component';

describe('DynamicDatatableEditComponent', () => {
  let component: DynamicDatatableEditComponent;
  let fixture: ComponentFixture<DynamicDatatableEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DynamicDatatableEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DynamicDatatableEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
