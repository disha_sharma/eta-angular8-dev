import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserinfoDisplayComponent } from './userinfo-display.component';

describe('UserinfoDisplayComponent', () => {
  let component: UserinfoDisplayComponent;
  let fixture: ComponentFixture<UserinfoDisplayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserinfoDisplayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserinfoDisplayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
