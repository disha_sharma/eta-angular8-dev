import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddDatamodelComponent } from './add-datamodel.component';

describe('AddDatamodelComponent', () => {
  let component: AddDatamodelComponent;
  let fixture: ComponentFixture<AddDatamodelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddDatamodelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddDatamodelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
