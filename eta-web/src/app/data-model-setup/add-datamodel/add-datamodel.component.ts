import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormControl } from '@angular/forms';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ArrayType } from '@angular/compiler/src/output/output_ast';

/* scope tree code first part starts here */
import { SelectionModel } from '@angular/cdk/collections';
import { FlatTreeControl } from '@angular/cdk/tree';
import { Injectable } from '@angular/core';
import { MatTreeFlatDataSource, MatTreeFlattener } from '@angular/material/tree';
import { BehaviorSubject } from 'rxjs';
import { ServiceService } from '../../eta-services/service.service';
import { Router, ActivatedRoute } from '@angular/router';
import { DatamodelServiceService } from '../datamodel-service/datamodel-service.service';
import { Observable, of } from 'rxjs';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead';
import { mergeMap } from 'rxjs/operators';
import { TreeviewItem, TreeviewConfig, TreeItem } from '../../../lib';
import { Container } from '../../model/container';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';


@Component({
  selector: 'app-add-datamodel',
  templateUrl: './add-datamodel.component.html',
  styleUrls: ['./add-datamodel.component.css']
})
export class AddDatamodelComponent implements OnInit {

  resuableName: boolean = false;
  sharedName: boolean = false;
  //code for field name
  containerNameControl = new FormControl('', [Validators.required, Validators.maxLength(64)]);
  getErrorMessage() {
    return this.containerNameControl.hasError('required') ? 'You must enter a value' : '';
  }

  name: string = '';
  dataContainerForm: FormGroup;

  /** For BreadCrumb Menu Display */
  public mainbreadcrumb: string;
  public breadcrumb: string;
  public menuList: any = [];

  public processList: any = [];
  public processName: string = '';
  public dataElementValueTypeList: any = [];
  public process_Id: number;
  public acronym: string = '';

  selectionForDataObject = new SelectionModel(true, []);   //data object  selection for delete
  selectionForDataElement = new SelectionModel(true, []);   //for data element selection
  selected = 'option2';  //code for multi select scope starts here
  selectedInfo = [];
  newItemName = '';  /** The new item's name */

  dataSourceContainer: Observable<any>;
  typeaheadLoadingContainer: boolean;
  containerList: any[] = [];
  dataSourceObject: Observable<any>;
  typeaheadLoadingObject: boolean;
  objectList: any[] = [];
  objectName: string = '';
  dataSourceElement: Observable<any>;
  typeaheadLoadingElement: boolean;
  elementList: any[] = [];
  elementName: string = '';
  scopeData: TreeviewItem[];
  public processId: string;
  public duplicateContainerName: boolean = false;
  public duplicateObjectName: boolean = false;
  public duplicateElementName: boolean = false;
  treeView: TreeviewItem[];
  treeVal: TreeviewItem;

  /**
   * 
    These are variables for the scope tree 

   */
  rows: string[];
  dropdownEnabled = true;
  items: TreeviewItem[];
  values: number[];
  config = TreeviewConfig.create({
    hasAllCheckBox: true,
    hasFilter: true,
    hasCollapseExpand: true,
    decoupleChildFromParent: false,
    maxHeight: 400
  });

  buttonClasses = [
    'btn-outline-primary',
    'btn-outline-secondary',
    'btn-outline-success',
    'btn-outline-danger',
    'btn-outline-warning',
    'btn-outline-info',
    'btn-outline-light',
    'btn-outline-dark'
  ];
  buttonClass = this.buttonClasses[0];

  public containerDetails: Container;


  /**
   * 
   * @param fb 
   * 
   * @param database 
   * @param sharedService 
   * @param router 
   * @param dataModelService 
   * @param activeRouter 
   */
  constructor(private spinnerService: Ng4LoadingSpinnerService, private fb: FormBuilder, private sharedService: ServiceService, private router: Router,
    private dataModelService: DatamodelServiceService, private activeRouter: ActivatedRoute) {

    this.processName = this.dataModelService.processName;
    this.process_Id = this.dataModelService.processId;
    this.process_Id = this.activeRouter.snapshot.params['processId'];

    this.getHeaderMenuList();
    this.getAllProcessNamesList();
    this.getAllDataElementType();

    this.dataSourceContainer = Observable.create((observer: any) => {
      //console.log("Every search Container text: "+this.name); 
      observer.next(this.name); // Runs on every search
    })
      .pipe(
        mergeMap((token: string) => this.getDataContainer(token))
      );

    this.dataSourceObject = Observable.create((observer: any) => {
      //console.log("Every search Object text : "+this.objectName); 
      observer.next(this.objectName); // Runs on every search
    })
      .pipe(
        mergeMap((token: string) => this.getDataObject(token))
      );

    this.dataSourceElement = Observable.create((observer: any) => {
      //console.log("Every search Element text : "+this.elementName); 
      observer.next(this.elementName); // Runs on every search
    })
      .pipe(
        mergeMap((token: string) => this.getDataElement(token))
      );

    this.dataModelService.getContainerScope().subscribe(response => {
      this.treeVal = response;
      this.scopeData = [this.treeVal];
      this.scopeData = this.getData();
    });

  }

  getData(): any[] {
    const itCategory = new TreeviewItem(this.scopeData[0]);
    return [itCategory];
  }


  ngOnInit() {
    this.dataContainerForm = this.fb.group({
      'containerName': ['', Validators.required],
      'reusalbeName': '',
      'sharedName': '',
      'processId': this.process_Id,
      'dataObject': this.fb.array([
        this.initDataObject()
      ])
    });


    /** This will Fetch all the Container Names for Typeahead */
    this.dataModelService.getContainerNameList().subscribe(data => {
      for (let s of data) {
        this.containerList.push(s);
      }
      console.log(this.containerList);
    });

    /** This will Fetch all the Object Names for Typeahead */
    this.dataModelService.getObjectNameList().subscribe(data => {
      for (let s of data) {
        this.objectList.push(s);
      }
      console.log(this.objectList);
    });

    /** This will Fetch all the Element Names for Typeahead */
    this.dataModelService.getElementNameList().subscribe(data => {
      for (let s of data) {
        this.elementList.push(s);
      }
      console.log(this.elementList);
      this.dataContainerForm.controls['sharedName'].value;
    });



  }
  
  /** DataObject checked or not */
  initDataObject() {
    return this.fb.group({
      'dataObjectCheckedStatus': [''],
      'dataObjectName': ['', Validators.required],
      'dataElements': this.fb.array([
        this.initDataElement()
      ])
    });
  }

  /** DataElement checked or not */
  initDataElement() {
    return this.fb.group({
      'dataElementCheckedStatus': [''],
      'dataElementName': [''],
      'dataElementValueType': [''],
    })
  }

  public getAllProcessNamesList() {
    this.dataModelService.getAllProcessNamesList().subscribe(data => {
      this.processList = data;
      console.log(this.processList);
      this.processList.forEach(element => {
        if (this.process_Id == element.processID) {
          this.acronym = element.acronym;
        }
      });
    });
  }

  //add data object 
  addDataObject(event) {
    const control = <FormArray>this.dataContainerForm.controls['dataObject'];
    control.push(this.initDataObject());

    if(event){
    setTimeout(() => {
      this.scrollToTarget();
    }, 0);
  }
  }

  @ViewChild('dataObjectsScrollableContent', { read: ElementRef, static: true }) public dataObjectsScrollableContent: ElementRef<any>;
  @ViewChild('scrollTargetForDataObject', { read: ElementRef, static: true }) public scrollTargetForDataObject: ElementRef<any>;
  scrollToTarget() {
   this.dataObjectsScrollableContent.nativeElement.scrollTo({ top: this.dataObjectsScrollableContent.nativeElement.scrollHeight, behavior: 'smooth' });
    this.scrollTargetForDataObject.nativeElement.scrollIntoView(false);
}

  //delete data object
  deleteDataObject(index) {
    let count = 0;
    var i = this.dataContainerForm.controls['dataObject'].value.length;
    while (i--) {
      if (this.dataContainerForm.controls['dataObject'].value[i].dataObjectCheckedStatus == true) {
        const control = <FormArray>this.dataContainerForm.controls['dataObject'];
        control.removeAt(i);
        count++;
      }
    }
    this.selectionForDataObject.clear();
    let lengthAfterDelete = this.dataContainerForm.controls['dataObject'].value.length;
    if (!lengthAfterDelete) {
      this.addDataObject(undefined);
    }
  }



  // Whether the number of selected elements matches the total number of rows.
  isAllDataObjectsSelected() {
    let control = <FormArray>this.dataContainerForm.controls['dataObject'];
    const numSelected = this.selectionForDataObject.selected.length;
    const numRows = control.controls.length;
    return numSelected === numRows;
  }

  // Selects all rows if they are not all selected; otherwise clear selection. 
  masterToggleForDataObjects() {
    let formControl = <FormArray>this.dataContainerForm.controls['dataObject'];
    this.isAllDataObjectsSelected() ?
      this.unCheckAllDataObjects() : this.checkAllDataObjects();
  }

  checkAllDataObjects() {
    let formControl = <FormArray>this.dataContainerForm.controls['dataObject'];
    let length = formControl.value.length;
    formControl.controls.forEach(row => this.selectionForDataObject.select(row));

    for (let i = 0; i < length; i++) {
      formControl.controls[i].get('dataObjectCheckedStatus').setValue(true);
    }
    this.checkAllDataElementsOnAllDataObjectSelection();
  }


  unCheckAllDataObjects() {
    this.selectionForDataObject.clear();
    let formControl = <FormArray>this.dataContainerForm.controls['dataObject'];
    let length = formControl.value.length;
    for (let i = 0; i < length; i++) {
      formControl.controls[i].get('dataObjectCheckedStatus').setValue(false);
    }
    this.unCheckAllDataElementsOnAllDataObjectSelection()
  }

  selectUnselectDataElements(event, dataObjectIndex) {
    if (event.checked) {
      this.chekAllDataElement(dataObjectIndex);
    } else {
      this.unCheckAllDataElements(dataObjectIndex);
    }
  }
  //add data element 
  addDataElement(dataObjectIndex) {
    const control = (<FormArray>this.dataContainerForm.controls['dataObject']).at(dataObjectIndex).get('dataElements') as FormArray;
    control.push(this.initDataElement());
  }

  //delete data element
  deleteDataElement(dataObjectIndex, removeIndex) {
    var i = this.dataContainerForm.controls['dataObject'].value[dataObjectIndex].dataElements.length;
    while (i--) {
      if (this.dataContainerForm.controls['dataObject'].value[dataObjectIndex].dataElements[i].dataElementCheckedStatus) {
        const control = (<FormArray>this.dataContainerForm.controls['dataObject']).at(dataObjectIndex).get('dataElements') as FormArray;
        control.removeAt(i);
      }
    }
    let lenghAfterDelete = this.dataContainerForm.controls['dataObject'].value[dataObjectIndex].dataElements.length;
    if (!lenghAfterDelete) {
      this.addDataElement(dataObjectIndex);
    }
  }

  // for toggle the checked state of check all data element button
  isAllDataElementsSelectedInADataObject(dataObjectIndex) {
    let checkedFields = 0;
    let dataElementsLength = this.dataContainerForm.controls['dataObject'].value[dataObjectIndex].dataElements.length;
    if (!dataElementsLength) {
      return false;
    }
    let i = dataElementsLength
    while (i--) {
      if (this.dataContainerForm.controls['dataObject'].value[dataObjectIndex].dataElements[i].dataElementCheckedStatus) {
        checkedFields++;
      }
    }
    return checkedFields == dataElementsLength;
  }

  //for showing delete button for data element
  isAnyDataElementSeletedInADataObject(dataObjectIndex) {
    let i = this.dataContainerForm.controls['dataObject'].value[dataObjectIndex].dataElements.length;
    let count = 0;
    while (i--) {
      if (this.dataContainerForm.controls['dataObject'].value[dataObjectIndex].dataElements[i].dataElementCheckedStatus) {
        count++;
      }
    }
    if (count > 0 && this.dataContainerForm.controls['dataObject'].value[dataObjectIndex].dataElements.length > 1) {
      return true
    }

    return false;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggleForDataElement(dataObjectIndex, event) {
    const controlForDataElement = (<FormArray>this.dataContainerForm.controls['dataObject']).at(dataObjectIndex).get('dataElements');
    const secondControlForDataElement = <FormArray>controlForDataElement;
    this.checkUncheckAllDataElements(dataObjectIndex, event)
  }

  //check uncheck all data elements on conditions
  checkUncheckAllDataElements(dataObjectIndex, event) {
    if (event.checked) {
      this.chekAllDataElement(dataObjectIndex);
    }
    if (!event.checked) {
      this.unCheckAllDataElements(dataObjectIndex);
    }
  }
  chekAllDataElement(dataObjectIndex) {
    let length = this.dataContainerForm.controls['dataObject'].value[dataObjectIndex].dataElements.length;
    const control = (<FormArray>this.dataContainerForm.controls['dataObject']).at(dataObjectIndex).get('dataElements');
    const secondControl = <FormArray>control
    for (let i = 0; i < length; i++) {
      secondControl.controls[i].get('dataElementCheckedStatus').setValue(true);
    }
    const controlForDataElement = (<FormArray>this.dataContainerForm.controls['dataObject']).at(dataObjectIndex).get('dataElements');
    const secondControlForDataElement = <FormArray>controlForDataElement;
    secondControlForDataElement.controls.forEach(row => this.selectionForDataElement.select(row));
  }
  unCheckAllDataElements(dataObjectIndex) {
    let length = this.dataContainerForm.controls['dataObject'].value[dataObjectIndex].dataElements.length;
    const control = (<FormArray>this.dataContainerForm.controls['dataObject']).at(dataObjectIndex).get('dataElements');
    const secondControl = <FormArray>control
    for (let i = 0; i < length; i++) {
      secondControl.controls[i].get('dataElementCheckedStatus').setValue(false);
    }
  }
  checkAllDataElementsOnAllDataObjectSelection() {
    let formControl = <FormArray>this.dataContainerForm.controls['dataObject'];
    let dataObjectsLength = formControl.value.length;
    for (let i = 0; i < dataObjectsLength; i++) {
      let length = this.dataContainerForm.controls['dataObject'].value[i].dataElements.length;
      const control = (<FormArray>this.dataContainerForm.controls['dataObject']).at(i).get('dataElements');
      const secondControl = <FormArray>control
      for (let k = 0; k < length; k++) {
        secondControl.controls[k].get('dataElementCheckedStatus').setValue(true);
      }
      const controlForDataElement = (<FormArray>this.dataContainerForm.controls['dataObject']).at(i).get('dataElements');
      const secondControlForDataElement = <FormArray>controlForDataElement;
      secondControlForDataElement.controls.forEach(row => this.selectionForDataElement.select(row));
    }

  }
  unCheckAllDataElementsOnAllDataObjectSelection() {
    let formControl = <FormArray>this.dataContainerForm.controls['dataObject'];
    let dataObjectsLength = formControl.value.length;

    for (let i = 0; i < dataObjectsLength; i++) {
      let length = this.dataContainerForm.controls['dataObject'].value[i].dataElements.length;
      const control = (<FormArray>this.dataContainerForm.controls['dataObject']).at(i).get('dataElements');
      const secondControl = <FormArray>control
      for (let k = 0; k < length; k++) {
        secondControl.controls[k].get('dataElementCheckedStatus').setValue(false);
      }
    }
  }



  stopPropegationFunction(event) {
    event.stopPropagation();
  }

  scopeCanceled() {
    this.selectedInfo = [];
    this.selected = '';
  }

  public cancel() {
    this.router.navigate(['eta-web/datamodel-dashboard'], { queryParams: { action: 'cancel', processId: this.process_Id } })
    console.log('canceled--------------');

  }

  /******************************** Display Breadcrumb Menu ************************************** */
  getHeaderMenuList() {
    this.sharedService.getHeaderMenu()
      .subscribe(res => {
        this.menuList = res;
        this.menuList.forEach(element => {
          if (element.routerLink == "#5") {
            this.mainbreadcrumb = element.translation;
          }
          if (element.routerLink == "eta-web/datamodel-dashboard") {
            this.breadcrumb = element.translation;
          }
        })
      })
  }
  /*
    This Function For Check Container Name Unique When value Typed.
    */
  checkOnKeyUpContainer(event) {
    this.duplicateContainerName = false;
    this.dataModelService.checkDuplicateContainerName(event.target.value).subscribe(data => {
      if (data.status == true) {
        this.dataContainerForm.controls['containerName'].setErrors({ 'invalid': true });
        this.duplicateContainerName = true;
      } else {
        this.duplicateContainerName = false;
      }
    });
  }

  submitDataContainer() {
    this.dataModelService.addDataContainer(JSON.stringify(this.scopeData), this.dataContainerForm.value).subscribe(data => {
      this.router.navigate(['eta-web/datamodel-dashboard'], { queryParams: { action: 'submit', processId: this.process_Id, name: this.name } })
    });
  }

  public getAllDataElementType() {
    this.dataModelService.getAllDataElementType().subscribe(data => {
      this.dataElementValueTypeList = data;
      console.log(this.dataElementValueTypeList);
    });
  }

  /******************** Start Data Container Name Typeahead Method ***********************/
  public getDataContainer(token: string): Observable<any> {
    const query = new RegExp(token, 'i');
    this.containerList.filter((str: any) => {
      if (query.test(str.name)) {
      }
    })
    return of(
      this.containerList.filter((str: any) => {
        return query.test(str.name);
      })
    );
  }

  changeTypeaheadLoadingContainer(e: boolean): void {
    this.typeaheadLoadingContainer = e;
  }

  /*
  This Function For Check Container Name Unique When value Select.
  */
  typeaheadOnSelectContainer(e: TypeaheadMatch): void {
    this.name = e.value;
    this.duplicateContainerName = false;
    this.dataModelService.checkDuplicateContainerName(e.value).subscribe(data => {
      if (data.status == true) {
        this.dataContainerForm.controls['containerName'].setErrors({ 'invalid': true });
        this.duplicateContainerName = true;
      } else {
        this.duplicateContainerName = false;
      }
    });
  }
  /******************** End Data Container Name Typeahead Method ***********************/

  /******************** Start Data Object Name Typeahead Method ***********************/
  public getDataObject(token: string): Observable<any> {
    const query = new RegExp(token, 'i');
    this.objectList.filter((str: any) => {
      if (query.test(str.objectName)) {
      }
    })
    return of(
      this.objectList.filter((str: any) => {
        return query.test(str.objectName);
      })
    );
  }

  changeTypeaheadLoadingObject(e: boolean): void {
    this.typeaheadLoadingObject = e;
  }

  /*
  This Function For Check Data Object Name Unique When value Select.
  */
  typeaheadOnSelectObject(e: TypeaheadMatch, index): void {
    if (e && e.value) {
      for (let j = 0; j < this.objectList.length; j++) {
        var objectNameCheck = this.objectList[j].objectName.toLowerCase();
        if (objectNameCheck == e.value.toLowerCase()) {
          const control = <FormArray>this.dataContainerForm.controls['dataObject'];
          let secondControl = <FormArray>control.controls[index];
          secondControl.controls['dataObjectName'].setErrors({ 'customError': true });
          return;
        }
      }
    }
  }
  /*
  This Function For Check Data Object Name Unique When value Typed.
  */
  checkOnKeyUp(event, index) {
    console.log('object');
    const FormValues = this.dataContainerForm.controls['dataObject'].value;
    //check values type from type ahead.
    for (let j = 0; j < this.objectList.length; j++) {
      var trimvalue = this.objectList[j].objectName.trim();
      var objectNameCheck = trimvalue.toLowerCase();
      if (objectNameCheck == event.target.value.toLowerCase()) {
        const control = <FormArray>this.dataContainerForm.controls['dataObject'];
        let secondControl = <FormArray>control.controls[index];
        secondControl.controls['dataObjectName'].setErrors({ 'customError': true });
        return;
      }
    }
    //check values type if duplicate.
    let count = 0;
    if (FormValues.length > 0) {
      for (let k = 0; k < FormValues.length; k++) {
        var trimData = FormValues[k].dataObjectName.trim();
        var formValueCheck = trimData.toLowerCase();
        if (formValueCheck == event.target.value.toLocaleLowerCase()) {
          count++;
        }
      }
      if (count > 1) {
        const control = <FormArray>this.dataContainerForm.controls['dataObject'];
        let secondControl = <FormArray>control.controls[index];
        secondControl.controls['dataObjectName'].setErrors({ 'customError': true });
      }
    }
  }

  /******************** End Data Object Name Typeahead Method ***********************/

  /******************** Start Data Element Name Typeahead Method ***********************/
  public getDataElement(token: string): Observable<any> {
    const query = new RegExp(token, 'i');
    this.elementList.filter((str: any) => {
      if (query.test(str.elementName)) {
      }
    })
    return of(
      this.elementList.filter((str: any) => {
        return query.test(str.elementName);
      })
    );
  }

  changeTypeaheadLoadingElement(e: boolean): void {
    this.typeaheadLoadingElement = e;
  }

  /*
  This Function For Check Element Name Unique When value Select.
  */
  typeaheadOnSelectElement(e: TypeaheadMatch, index, dataObjectIndex): void {
    if (e && e.value) {
      for (let j = 0; j < this.elementList.length; j++) {
        var elementNameCheck = this.elementList[j].elementName.toLowerCase();
        if (elementNameCheck == e.value.toLowerCase()) {
          const controlElementObject = <FormArray>this.dataContainerForm.controls['dataObject'];
          let secondControlElement = <FormArray>controlElementObject.controls[dataObjectIndex];
          let controlElement = <FormArray>secondControlElement.controls['dataElements'].controls[index];
          controlElement.controls['dataElementName'].setErrors({ 'elementError': true });
          return;
        }
      }
    }
  }
  /*
  This Function For Check Element Name Unique When value Typed.
  */
  checkOnKeyUpElementName(event, objectIndex, dataElementIndex) {
    if (event && event.target && event.target.value) {
      //check values type from type ahead.
      const FormValuesElement = this.dataContainerForm.controls['dataObject'].value;
      for (let j = 0; j < this.elementList.length; j++) {
        var trimValue = this.elementList[j].elementName.trim();
        var objectNameCheck = trimValue.toLowerCase();
        if (objectNameCheck == event.target.value.toLowerCase()) {
          const controlElementObject = <FormArray>this.dataContainerForm.controls['dataObject'];
          let secondControlElement = <FormArray>controlElementObject.controls[objectIndex];
          let controlElement = <FormArray>secondControlElement.controls['dataElements'].controls[dataElementIndex];
          controlElement.controls['dataElementName'].setErrors({ 'elementError': true });
          return;
        }
      }
       //check values type if duplicate.
      let count = 0;
      if (FormValuesElement.length > 0) {
        for (let k = 0; k < FormValuesElement.length; k++) {
          for (let h = 0; h < FormValuesElement[k].dataElements.length; h++) {
            var trimValue = FormValuesElement[k].dataElements[h].dataElementName.trim();
            var formValueCheckElementValue = trimValue.toLowerCase();
            if (formValueCheckElementValue == event.target.value.toLocaleLowerCase()) {
              count++;
            }
          }
        }
        if (count > 1) {
          const controlElementObject = <FormArray>this.dataContainerForm.controls['dataObject'];
          let secondControlElement = <FormArray>controlElementObject.controls[objectIndex];
          let controlElement = <FormArray>secondControlElement.controls['dataElements'].controls[dataElementIndex];
          controlElement.controls['dataElementName'].setErrors({ 'elementError': true });
        }
      }


    }
  }

  /******************** End Data Element Name Typeahead Method ***********************/


  validateAllContainerFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllContainerFormFields(control);
      }
    });
  }

  /*
 This function is use for When containerName AlphaNumeric Validation.
 */
  checkCharValidationForContainer(value) {
    var str = value.target.value;
      if (str) {
      const matches = str.match(/^[A-Za-z][A-Za-z0-9/.,->_()\\*|:" ]*$/);
      return matches?null: this.dataContainerForm.controls['containerName'].setErrors({ 'patternError': true });
      }
  }

  // for Group name Validation 
  SpecialChar(event) {
    var charCode;
    charCode = event.charCode;  //         k = event.keyCode;  (Both can be used)
    return ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || (charCode >= 48 && charCode <= 57) || charCode == 8 || charCode == 32 || charCode == 40 || charCode == 41 || charCode == 58 || charCode == 92 || charCode == 124 || charCode == 95 || charCode == 62 || (charCode > 43 && charCode < 48));
  }

  /*
   This function is use for When Data Object AlphaNumeric Validation.
   */
  checkCharValidationForObject(value, index) {
    var str = value.target.value;
      if (str) {
      const matches = str.match(/^[A-Za-z][A-Za-z0-9/.,->_()\\*|:" ]*$/);
      const control = <FormArray>this.dataContainerForm.controls['dataObject'];
        let secondControl = <FormArray>control.controls[index];
      return matches?null: secondControl.controls['dataObjectName'].setErrors({ 'patternObjectError': true });
      }
  }

  /*
   This function is use for When Data Element AlphaNumeric Validation.
   */
  checkCharValidationForElement(value, index, dataObjectIndex) {
    var str = value.target.value;
    if (str) {
    const matches = str.match(/^[A-Za-z][A-Za-z0-9/.,->_()\\*|:" ]*$/);
    const controlElementObject = <FormArray>this.dataContainerForm.controls['dataObject'];
          let secondControlElement = <FormArray>controlElementObject.controls[dataObjectIndex];
          let controlElement = <FormArray>secondControlElement.controls['dataElements'].controls[index];
    return matches?null: controlElement.controls['dataElementName'].setErrors({ 'patternElementError': true });
    }
  }

}
