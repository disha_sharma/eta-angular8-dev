import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { SelectionModel } from '@angular/cdk/collections';
import { ServiceService } from '../../eta-services/service.service';
import { HttpClient, HttpHeaderResponse } from '@angular/common/http';
import { merge, Observable, of as observableOf } from 'rxjs';
import { catchError, map, startWith, switchMap } from 'rxjs/operators';
import { DatamodelServiceService } from '../datamodel-service/datamodel-service.service';
import { TaskServiceService } from '../../dash-board/task-service.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-datamodel-dashboard',
  templateUrl: './datamodel-dashboard.component.html',
  styleUrls: ['./datamodel-dashboard.component.css'],
  animations: [
    trigger('detailExpand', [
      state('void', style({ height: '0px', minHeight: '0', visibility: 'hidden' })),
      state('*', style({ height: '*', visibility: 'visible' })),
      transition('void <=> *', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class DataModelDashboardComponent implements OnInit {

  public menuList: any = [];
  public mainbreadcrumb: string;
  public breadcrumb: string;
  pageSize: number;
  resultsLength = 0;
  isLoadingResults = true;
  isRateLimitReached = false;
  httpResponse: number = 0;
  public defaultColumnToSort: any;
  dataContainerHttpDao: DataContainerHttpDao | null;
  gridDataContainerColumns: any;
  public message: string;
  public isEditAllowed: boolean = false;
  public processList: any = [];

  processName: string;
  processId: number;
  allButtonFlag: boolean = true;
  displayedColumns: string[] = ['checkbox', 'container', 'type', 'objects', 'elements', 'status'];
  dataContainerDataSource = new MatTableDataSource<DataContainer>();
  //for selection
  selection = new SelectionModel(true, []);
  pageSizeOptions = "";
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  isExpansionDetailRow = (index, row) => row.hasOwnProperty('detailRow');

  showMessageAlert: string = "";
  name: string = "";

  checkProcessChecked: boolean = false;
  


  constructor(public shareServices: ServiceService, private dataModelService: DatamodelServiceService, private httpClient: HttpClient, private taskServices: TaskServiceService, private router: Router,
    private activatedRoute: ActivatedRoute) {

    this.defaultColumnToSort = "name";
    this.getHeaderMenuList();
    this.dataModelService.getAllProcessNamesList();

  }

  ngOnInit() {
    console.log('ngonit');
    this.removeMessageAlert();
    this.pageSize = 5;
    this.processId = this.activatedRoute.snapshot.queryParams['processId'];
    let action = this.activatedRoute.snapshot.queryParams['action'];
    if (action == 'cancel' || action == 'submit' || action == 'update' || action == 'reload') {
      this.getDataContainerByProcessID(this.processId);
    }
    else {
      this.getAllDataContainerInfo();
    }

    this.getAllProcessNamesList();

    this.name = this.activatedRoute.snapshot.queryParams['name'];
   /*  if(this.activatedRoute.snapshot.queryParams['action'] != 'cancel'){
      this.showMessageAlert = this.activatedRoute.snapshot.queryParams['action'];
  } */

  if(this.activatedRoute.snapshot.queryParams['action'] == 'submit'|| this.activatedRoute.snapshot.queryParams['action'] == 'update'){
    this.showMessageAlert = this.activatedRoute.snapshot.queryParams['action']; 
    this.router.navigate(['eta-web/datamodel-dashboard'],{ queryParams: { processId: this.processId, action: 'reload' }}); /* In use actually */

  }

    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
   // this.dataContainerHttpDao = new DataContainerHttpDao(this.httpClient, this.dataModelService);
    /* this.shareServices.getCoreDataContainerColumn().subscribe(data => {
      this.gridDataContainerColumns = data.coloumnName;
    }); */
    /* this.shareServices.refreshEditMessage();
    this.shareServices.currentMessage.subscribe(message => {
      this.message = message
      if (message != 'editFalse') {
        this.isEditAllowed = true;
      }
    });*/

    // If the user changes the sort order, reset back to the first page.
 
    if (!this.activatedRoute.snapshot.queryParams['action']) {
      console.log('if ');
      this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
      this.dataContainerHttpDao = new DataContainerHttpDao(this.httpClient, this.dataModelService);
      merge(this.sort.sortChange, this.paginator.page)
        .pipe(
          startWith({}),
          switchMap(() => {
            this.isLoadingResults = true;
            if  (this.paginator.pageSize  !=  undefined) {
              this.pageSize  =  this.paginator.pageSize;
            }
            return this.dataContainerHttpDao!.getDataContainerDetails(this.sort.active, this.sort.direction, this.paginator.pageIndex, this.pageSize,this.processId);
          }),
          map(data => {
            // Flip flag to show that loading has finished.
            this.isLoadingResults = false;
            this.isRateLimitReached = false;
            if (!(action == 'cancel' || action == 'submit' || action == 'update')) {
              this.resultsLength = data.recordsTotal;
            }
            return data.data;
          }),
          catchError((response: HttpHeaderResponse) => {
            if (response.status === 404) {
              this.httpResponse = response.status;
            }
            this.isLoadingResults = false;
            // Catch if the GitHub API has reached its rate limit. Return empty data.
            this.isRateLimitReached = true;
            return observableOf([]);
          })
        ).subscribe(data => {
          this.httpResponse = 200;
          if (!(action == 'cancel' || action == 'submit' || action == 'update')) {
            this.dataContainerDataSource = new MatTableDataSource(data);
          }

        });
     //   this.router.navigate(['/datamodel-dashboard'],{ queryParams: { }});
    } else if (this.activatedRoute.snapshot.queryParams['action'] === 'submit' || 
    this.activatedRoute.snapshot.queryParams['action'] === 'update' || 
    this.activatedRoute.snapshot.queryParams['action'] === 'cancel' || 
    this.activatedRoute.snapshot.queryParams['action'] === 'reload') {
      console.log('esle if ');
      this.checkProcessChecked = true;
      this.allButtonFlag = false;
      this.name = this.activatedRoute.snapshot.queryParams['name'];
      this.processId = this.activatedRoute.snapshot.queryParams['processId'];
      this.dataContainerHttpDao.getDataContainerDetailsByProcessId(this.sort.active, this.sort.direction, this.paginator.pageIndex, this.pageSize, this.processId).subscribe(response => {
        this.resultsLength = response.recordsTotal;
        this.dataContainerDataSource = new MatTableDataSource(response.data);
      });
   //   this.router.navigate(['/datamodel-dashboard'], { queryParams: {} });
    }/* else if(this.activatedRoute.snapshot.queryParams['action'] === 'update'){
      this.checkProcessChecked = true;
      this.allButtonFlag = false;
      this.name = this.activatedRoute.snapshot.queryParams['name'];
      this.processId = this.processId;
          this.dataContainerHttpDao.getDataContainerDetailsByProcessId(this.sort.active, this.sort.direction, this.paginator.pageIndex, this.pageSize, this.processId).subscribe(response => {
            this.resultsLength = response.recordsTotal;
            this.dataContainerDataSource = new MatTableDataSource(response.data);
        });

      this.router.navigate(['/datamodel-dashboard'],{ queryParams: { }});  //this will clean url after processing.
    } else if(this.activatedRoute.snapshot.queryParams['action'] === 'cancel'){   
          this.checkProcessChecked = true;
          this.allButtonFlag = false;
          this.processId = this.processId;
             this.dataContainerHttpDao.getDataContainerDetailsByProcessId(this.sort.active, this.sort.direction, this.paginator.pageIndex, this.pageSize, this.processId).subscribe(response => {
              this.resultsLength = response.recordsTotal;
              this.dataContainerDataSource = new MatTableDataSource(response.data);
          });
          this.router.navigate(['/datamodel-dashboard'],{ queryParams: { }});  //this will clean url after processing.        
    } */
  }


  public getAllProcessNamesList() {
    this.dataModelService.getAllProcessNamesList().subscribe(data => {
      this.processList = data;
      setTimeout(()=> {
        this.toggleScrollButtonsForTabsSlider();
    }, 0);
    });
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataContainerDataSource.filter = filterValue;
  }
  stopPropagationFunction(event) {
    event.stopPropagation();
  }
  markActiveButton(event) {
    document.getElementsByClassName('active-button')[0].classList.remove('active-button');
    event.target.classList.add('active-button');
    if (event.target.innerHTML.toLowerCase() == 'all') {
      this.allButtonFlag = true;
    }
    else {
      this.allButtonFlag = false;
    }

  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    this.removeMessageAlert();
    const numSelected = this.selection.selected.length;
    const numRows = this.dataContainerDataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.removeMessageAlert();
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataContainerDataSource.data.forEach(row => this.selection.select(row));
  }

  edit() {
    console.log(this.selection.selected[0]);
  }

  /******************************** Display Breadcrumb Menu ************************************** */
  getHeaderMenuList() {
    this.shareServices.getHeaderMenu()
      .subscribe(res => {
        this.menuList = res;
        this.menuList.forEach(element => {
          if (element.routerLink == "#2") {
            this.mainbreadcrumb = element.translation;
          }
          if (element.routerLink == "eta-web/datamodel-dashboard") {
            this.breadcrumb = element.translation;
          }
        })
      })
  }

  private deleteDataContainer(): void {
    /*  console.log(this.selection);
     for(let i=0; i<this.selection.selected.length; i++){
       console.log("Selected Form IDs : "+this.selection.selected[i].formID);
       this.formIdsList.push(this.selection.selected[i].formID);      
      }
      console.log("Forms Ids list : "+this.formIdsList);
      
      this.formService.deleteFormById(this.formIdsList).subscribe(response =>{
       console.log("Form are deleted .");    
       if(this.processName == null || this.processName.length == 0){
           this.getFormsAllInfo();  
       }else{
           this.getFormsByProcessID(this.processId, this.processName);
       }
      }); */
  }

   
  onPaginateChange(event) {
    this.pageSizeOptions = event.pageSizeOptions;
    console.log("page Size index : "+this.pageSizeOptions);    
  }

  public openAddDataContainer() {
    this.removeMessageAlert();
    this.dataModelService.processName = this.processName;
    this.router.navigate(['eta-web/addDataModel', this.processId]);
  }

  public openEditDataContainer(containerId: string, processId) {
    this.removeMessageAlert();
    this.dataModelService.containerId = containerId;
    this.router.navigate(['eta-web/editDataModel'], { queryParams: { processId: processId} })
    // this.router.navigateByUrl('editDataModel');
    //this.router.navigate(['/editDataModel']);
  }

  public getDataContainerByProcessID(processID): void {
    this.removeMessageAlert();
    this.selection.clear();
    this.processId = processID;
    this.dataModelService.processId = processID;
    this.dataModelService.getDataContainerDataTableDetailsByProcessId("asc", 0, this.pageSize, processID).subscribe(response => {
      this.resultsLength = response.recordsTotal;
      this.dataContainerDataSource = new MatTableDataSource(response.data);
      this.paginator.pageIndex = 0;
    })

  }

  public getAllDataContainerInfo(): void {
    this.removeMessageAlert();
    this.processName = ''; // make to blank to keep the process name updated
    this.processId = null;
    this.dataModelService.processId = null;
    if (this.pageSize == null || this.pageSize === undefined)
      this.pageSize = 5;
    this.selection.clear();
    this.dataModelService.getDataContainerDataTableDetails("asc", 0, this.pageSize).subscribe(response => {
      console.log(response);
      console.log("Total records : ", response.recordsTotal);
      this.resultsLength = response.recordsTotal;
      this.dataContainerDataSource = new MatTableDataSource(response.data);
      this.paginator.pageIndex = 0;
    })
  }

  removeMessageAlert() {
    this.showMessageAlert = "";
  }
  
//code for tabs scrollable
tabsScrollForwardButtonFlag: boolean;
tabsScrollbackButtonFlag: boolean;
@ViewChild('tabsContainer', { read: ElementRef, static: true }) public tabsContainer: ElementRef<any>;
@ViewChild('tabsScrollableContent', { read: ElementRef, static: true }) public tabsScrollableContent: ElementRef<any>;
onWindowResized(event){
this.toggleScrollButtonsForTabsSlider();
}
toggleScrollButtonsForTabsSlider(){
  if(this.tabsScrollableContent.nativeElement.clientWidth > this.tabsContainer.nativeElement.clientWidth){
this.tabsScrollForwardButtonFlag = true;
  }
  else{
    this.tabsScrollForwardButtonFlag = false;
    this.tabsScrollbackButtonFlag = false;
  }
}
back(){
  this.tabsContainer.nativeElement.scrollTo({ left: (this.tabsContainer.nativeElement.scrollLeft -300), behavior: 'smooth' });
  this.tabsScrollForwardButtonFlag = true;
  if(this.tabsContainer.nativeElement.scrollLeft < 301){
       this.tabsScrollbackButtonFlag = false;
     }
  }
  
  forward(){
    console.log( this.tabsContainer.nativeElement.scrollWidth);
    this.tabsContainer.nativeElement.scrollWidth;
  this.tabsContainer.nativeElement.scrollTo({ left: (this.tabsContainer.nativeElement.scrollLeft + 300), behavior: 'smooth' });
  this.tabsScrollbackButtonFlag = true;
  if(this.tabsContainer.nativeElement.offsetWidth + this.tabsContainer.nativeElement.scrollLeft > (this.tabsContainer.nativeElement.scrollWidth - 301)){
              this.tabsScrollForwardButtonFlag = false;
           }
  }

} // End of DataModelDashboardComponent Class

export interface DataContainer {
  containerId: string;
  name: string;
  createdBy: string;
  modifiedBy: string;
  isActive: boolean;
  processId: number;
  typeId: number;
  reusable: boolean;
  shared: boolean;
  orgId: number;
  elementCount: number;
  objectCount: number;
  status: string;

  processName: string;
  recordsTotal: number;

} //End of DataContainer Interface

export class DataContainerHttpDao {
  constructor(private httpClient: HttpClient, private dataModelService: DatamodelServiceService) {
  }

  public getDataContainerDetails(sort: string, order: string, page: number, pageSize: number ,processId : number): Observable<any> {
    if (order == 'asc' && order.length == 0) {
      order = "desc";
    } else {
      if (order.length === 0)
        order = "asc";
    }
    if(processId== undefined)
    return this.dataModelService.getDataContainerDataTableDetails(order, page, pageSize);         
else
    return this.dataModelService.getDataContainerDataTableDetailsByProcessId(order, page, pageSize, processId);
  }

  public getDataContainerDetailsByProcessId(sort: string, order: string, page: number, pageSize: number, processID: number): Observable<any> {
    if (order == 'asc' && order.length == 0) {
      order = "desc";
    } else {
      if (order.length === 0)
        order = "asc";
    }
    return this.dataModelService.getDataContainerDataTableDetailsByProcessId(order, page, pageSize, processID);
  }

} //End of the DataContainerHttpDao Class
