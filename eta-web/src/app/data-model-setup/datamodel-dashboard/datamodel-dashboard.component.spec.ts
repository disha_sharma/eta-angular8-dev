import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DataModelDashboardComponent } from './datamodel-dashboard.component';

describe('DataModelDashboardComponent', () => {
  let component: DataModelDashboardComponent;
  let fixture: ComponentFixture<DataModelDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DataModelDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DataModelDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
