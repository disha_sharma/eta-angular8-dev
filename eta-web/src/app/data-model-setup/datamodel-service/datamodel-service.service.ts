import { Injectable } from '@angular/core';
import { ServiceService } from '../../eta-services/service.service';
import { CookieService } from 'ngx-cookie-service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment.test';
import { TreeviewItem, TreeviewConfig, TreeItem } from '../../../lib';
import { ContainerMapping } from '../../model/container-mapping';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};


const httpOptions2 = {
  headers: new HttpHeaders({ 'responseType': 'text' })
};

@Injectable({
  providedIn: 'root'
})
export class DatamodelServiceService {
  adminRole = "admin";
  myRole;
  processName : string = '';
  processId : number;
  containerId : string;
  name : string = '';
  treeView : TreeviewItem[];
  treeVal : TreeviewItem;
  scopeResponse : string = "";
  scopeId : number;
  scopeOldResponse : string;
  addDataModelStatus : boolean = false;
  updateDataModelStatus : boolean = false;
  dataModelName : string = "";


  constructor(private sharedService: ServiceService, private cookieService: CookieService, private httpClient: HttpClient) { 
    this.myRole = cookieService.get('my_role');
    this.checkedSessionToken();
  }

  /********* toGetToken *************/
  public checkedSessionToken(): boolean {
    let istokenExist: boolean = false;
    if (sessionStorage.getItem("AuthToken") != null) {
      istokenExist = true;
    }
    return istokenExist;
  }

  /************** This Method will call for All Data Container details for Pagination ***************/
  
  public getDataContainerDataTableDetails(order: string, page : number, pageSize : number): Observable<any> {
    console.log("order : "+order);
    console.log("page : "+page);
    console.log("size : "+pageSize);
    const body = JSON.stringify({
      "page": page,
      "size" : pageSize,
      "direction": order,
      "columnName" : "name",
      observe: 'response'
    });
    return this.httpClient.post<any>(environment.Core_URL  + '/getAllDataContainerInfo', body, httpOptions);
  }

  /************* This Method will call for All Data Container details for Pagination by ProcessId **************/    
  public getDataContainerDataTableDetailsByProcessId(order: string, page : number, pageSize : number, processId: number): Observable<any> {
    console.log('datacontainer');
    const body = JSON.stringify({
      "page": page,
      "size" : pageSize,
      "direction": order,
      "columnName" : "name",
      "processId" : processId,
      observe: 'response'
    });
    return this.httpClient.post<any>(environment.Core_URL  + '/getAllDataContainerInfoByProcessId', body, httpOptions);
  }

  public getAllProcessNamesList(): Observable<any> {
    return this.httpClient.get<any>(environment.Core_URL  + '/getAllProcessNamesList');
  }

  /**
   * This method will add the DataContainerDetails
   * @param dataContainer 
   */
  public addDataContainer(scopeUpdated : string, dataContainer: string) : Observable<any> {
    console.log(dataContainer);  
    let containerMapping: ContainerMapping = new ContainerMapping("", scopeUpdated, dataContainer, 0);    
    const body = JSON.parse(JSON.stringify(containerMapping));
    console.log("add method: "+ body);
    return this.httpClient.post<any>(environment.Core_URL  + '/saveDataContainer', body, httpOptions);
  }

  /**
   * This method will update the DataContainerDetails
   * @param dataContainer 
   */
  public updateDataContainer(dataContainer: string, scopeData: string, scopeResponse : string, scopeId : number) : Observable<any> {
      console.log(dataContainer); 
      console.log(scopeResponse);
      console.log("Scope ID : "+scopeId);
      
      
      console.log("Scope data : ");    
      console.log(scopeData);
      let containerMapping: ContainerMapping = new ContainerMapping(scopeResponse, scopeData, dataContainer, scopeId);
      const body = JSON.parse(JSON.stringify(containerMapping));
      
      console.log("update method: "+ body);
      
      return this.httpClient.post<any>(environment.Core_URL  + '/updateDataContainer', body, httpOptions);
  }

  public getAllDataElementType(): Observable<any> {
    console.log("getAllDataElementTypes . ");
    return this.httpClient.get<any>(environment.Core_URL + '/getAllElementTypeList');
  }


  /**************************************** Get All Container Name List For Type Ahead *******************************************/
  public getContainerNameList() {
    return this.httpClient.get<any>(environment.Core_URL + '/getContainerNameList');
  }

  /**************************************** Get All Container Name List For Type Ahead *******************************************/
  public getObjectNameList() {
    return this.httpClient.get<any>(environment.Core_URL + '/getObjectNameList');
  }

  /**************************************** Get All Container Name List For Type Ahead *******************************************/
  public getElementNameList() {
    return this.httpClient.get<any>(environment.Core_URL + '/getElementNameList');
  }


  //Edit by ravi

  public getContainerInfo(containerId : number): Observable<any> {
    console.log("getContainerInfo . ");
    console.log("Container ID : "+ containerId);    
    return this.httpClient.get<any>(environment.Core_URL + '/getDataContainerDetails/'+ containerId);
  }//end of the method

  public  getContainerScopeByScopeId(previledgeScope : number): Observable<TreeviewItem> {  
    console.log("Scope ID : " + previledgeScope);      
    return this.httpClient.get<TreeviewItem>(environment.Core_URL + '/getContainerScopeDetailsForUpdate/' + previledgeScope, httpOptions2);
  }


public  getContainerScope(): Observable<TreeviewItem> {  
  console.log("Scope");
  return this.httpClient.get<TreeviewItem>(environment.Core_URL + '/getContainerScopeDetails', httpOptions2);
}


public getScopeTree(): TreeviewItem[] {
  console.log("getScopeTree . ");

  this.httpClient.get<any>(environment.Core_URL + '/getContainerScopeDetails', httpOptions2).subscribe(response =>{
    //const fruitCategory = new TreeviewItem(response);
    this.treeVal = response;
    return new TreeviewItem(this.treeVal, true);
  })
  return this.treeView;
} 

public getContainerNameValidation(processId: number, containerName : string): Observable<any> {
  return this.httpClient.get<any>(environment.Core_URL + '/getContainerNameValidation/'+ processId + '/' + containerName);
}

public getSelectedScopeCount(json : string) : Observable<any> {
  return this.httpClient.post<any>(environment.Core_URL  + '/getScopeSelection', json, httpOptions);
}



public checkDuplicateContainerName(name) : Observable<any> {
  var object = {
    "name":name
  }
  return this.httpClient.post<any>(environment.Core_URL  + '/checkDuplicateContainerName', object, httpOptions);
}

public checkDuplicateObjectName(name) : Observable<any> {
  var object = {
    "objectName":name
  }
  return this.httpClient.post<any>(environment.Core_URL  + '/checkDuplicateObjectName', object, httpOptions);
}

public checkDuplicateElementName(name) : Observable<any> {
  var object = {
    "elementName":name
  }
  return this.httpClient.post<any>(environment.Core_URL  + '/checkDuplicateElementName', object, httpOptions);
}

}//end of the class
