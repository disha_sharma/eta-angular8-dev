import { TestBed } from '@angular/core/testing';

import { DatamodelServiceService } from './datamodel-service.service';

describe('DatamodelServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DatamodelServiceService = TestBed.get(DatamodelServiceService);
    expect(service).toBeTruthy();
  });
});
