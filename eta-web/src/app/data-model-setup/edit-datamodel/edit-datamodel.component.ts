import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormControl } from '@angular/forms';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ArrayType } from '@angular/compiler/src/output/output_ast';

/* scope tree code first part starts here */
import { SelectionModel } from '@angular/cdk/collections';
import { Router, ActivatedRoute } from '@angular/router';
import { DatamodelServiceService } from '../datamodel-service/datamodel-service.service';
import { ServiceService } from '../../eta-services/service.service';
import { Container } from '../../model/container';
import { DataObject } from '../../model/data-object';
import { TreeviewItem, TreeviewConfig, TreeItem } from '../../../lib';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead';
import { Observable, of } from 'rxjs';
import { mergeMap } from 'rxjs/operators';
import { isNil, remove, reverse } from 'lodash';
import {
  TreeviewI18n, TreeviewHelper, TreeviewComponent,
  TreeviewEventParser, OrderDownlineTreeviewEventParser, DownlineTreeviewItem
} from '../../../lib';


@Component({
  selector: 'app-edit-datamodel',
  templateUrl: './edit-datamodel.component.html',
  styleUrls: ['./edit-datamodel.component.css']
})
export class EditDatamodelComponent implements OnInit {
  @ViewChild(TreeviewComponent, {static: true}) treeviewComponent: TreeviewComponent;
  resuableName: boolean = false;
  sharedName: boolean = false;
  containerId: string;
  conName: string;
  processId: number;
  dataObject: DataObject[] = [];
  invalidContainerName: boolean = false;
  invalidObjectName: boolean = false;
  scopeData: TreeviewItem[];
  strScope: string;
  typeaheadLoadingContainer: boolean;
  typeaheadLoadingObject: boolean;
  submitButtonDisabled: boolean = false;
  //code for field name
  containerName = new FormControl('', [Validators.required, Validators.maxLength(5), Validators.pattern('[0-9]{10}')]);
  getErrorMessage() {
    return this.containerName.hasError('required') ? 'You must enter a value' : '';
  }

  dataContainerForm: FormGroup;

  /** For BreadCrumb Menu Display */
  public mainbreadcrumb: string;
  public breadcrumb: string;
  public menuList: any = [];

  public processList: any = [];
  public processName: string = '';
  public dataElementValueTypeList: any = [];
  public process_Id: number;
  public acronym: string = '';
  public defaultContainerNameOnEdit: string = "";
  public strTest: string = "";
  previledgeScope: string;
  rows: string[];
  dropdownEnabled = true;
  items: TreeviewItem[];
  values: number[]=[];
  config = TreeviewConfig.create({
    hasAllCheckBox: true,
    hasFilter: true,
    hasCollapseExpand: true,
    decoupleChildFromParent: false,
    maxHeight: 400
  });

  buttonClasses = [
    'btn-outline-primary',
    'btn-outline-secondary',
    'btn-outline-success',
    'btn-outline-danger',
    'btn-outline-warning',
    'btn-outline-info',
    'btn-outline-light',
    'btn-outline-dark'
  ];
  buttonClass = this.buttonClasses[0];

  public containerDetails: Container;

  selectionForDataObject = new SelectionModel(true, []);   //data object  selection for delete
  selectionForDataElement = new SelectionModel(true, []);   //for data element selection
  selected = 'option2';  //code for multi select scope starts here
  selectedInfo = [];

  name: string = '';
  dataSourceContainer: Observable<any>;
  containerList: any[] = [];

  dataSourceObject: Observable<any>;
  objectList: any[] = [];
  objectName: string = '';

  dataSourceElement: Observable<any>;
  typeaheadLoadingElement: boolean;
  elementList: any[] = [];
  elementName: string = '';
  dataObjectName: any = [];
  indexOfDataObject: any = [];
  indexOfDataElement: any = [];
  public duplicateContainerName: boolean = false;
  public duplicateObjectName: boolean = false;
  public duplicateElementName: boolean = false;
  public scopeResponse: string = "";
  treeView: TreeviewItem[];
  treeVal: TreeviewItem;
  //editDefaultDisable: boolean = true;
  disabledOnEdit: boolean = false;
  countOnEdit: number = 0;
  checkInit: boolean = false;


  constructor(private fb: FormBuilder, private sharedService: ServiceService, private router: Router, private dataModelService: DatamodelServiceService, private activeRouter: ActivatedRoute) {
    this.processName = this.dataModelService.processName;
    this.process_Id = this.dataModelService.processId;
    this.containerId = this.dataModelService.containerId;
    this.process_Id = this.activeRouter.snapshot.queryParams['processId'];
    this.getHeaderMenuList();
    this.getAllProcessNamesList();
    this.getAllDataElementType();

    this.dataSourceContainer = Observable.create((observer: any) => {
      observer.next(this.name); // Runs on every search
    })
      .pipe(
        mergeMap((token: string) => this.getDataContainer(token))
      );

    this.dataSourceObject = Observable.create((observer: any) => {
      observer.next(this.objectName); // Runs on every search
    })
      .pipe(
        mergeMap((token: string) => this.getDataObject(token))
      );

    this.dataSourceElement = Observable.create((observer: any) => {
      observer.next(this.elementName); // Runs on every search
    })
      .pipe(
        mergeMap((token: string) => this.getDataElement(token))
      );

  }//end of the constructor




  patchValue() {
    this.deleteAllDataObject();
    for (let i = 0; i < this.containerDetails.dataObject.length; i++) {
      this.addDataObject(undefined);
      for (let k = 0; k < this.containerDetails.dataObject[i].dataElements.length - 1; k++) {
        this.addDataElement(i);
      }
    }
    this.dataContainerForm.patchValue({
      dataObject: this.containerDetails.dataObject,
    })
  }

  public enableDefaultEdit() {
    this.disabledOnEdit = true;
    console.log("Disable on edit : " + this.disabledOnEdit);
    //this.editDefaultDisable = false;
  }


  public validateContainerName(value: string) {
    this.processId = this.dataModelService.processId;
    this.dataModelService.getContainerNameValidation(this.processId, value).subscribe(response => {
      if (response.status == true) {
        if (this.defaultContainerNameOnEdit.trim() !== value.trim()) {
          this.invalidContainerName = true;
          this.dataContainerForm.controls['containerName'].setErrors({ 'invalid': true });
        }
        else {
          this.invalidContainerName = false;
        }
      } else {
        this.invalidContainerName = false;
      }
    })
  }

  // for Group name Validation 
  SpecialChar(event) {
    var charCode;
    charCode = event.charCode;  //         k = event.keyCode;  (Both can be used)
    return ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || (charCode >= 48 && charCode <= 57) || charCode == 8 || charCode == 32 || charCode == 40 || charCode == 41 || charCode == 58 || charCode == 92 || charCode == 124 || charCode == 95 || charCode == 62 || (charCode > 43 && charCode < 48));
  }

  changeTypeaheadLoadingObject(e: boolean): void {
    this.typeaheadLoadingObject = e;
  }


  /*
This Function For Check Data Object Name Unique When value Select.
*/
  typeaheadOnSelectObject(e: TypeaheadMatch, index): void {
    if (e && e.value) {
      const objectDetails = this.containerDetails.dataObject;
      const FormValues = this.dataContainerForm.controls['dataObject'].value;
      const control = <FormArray>this.dataContainerForm.controls['dataObject'];
      let secondControl = <FormArray>control.controls[index];
      //check from edit patch value array.
      for (let i = 0; i < objectDetails.length; i++) {
        var checkConstant = objectDetails[i].dataObjectName.toLowerCase();
        if (checkConstant == e.value.toLowerCase()) {
          let count = 0;
          //check values when select same as patch value.
          if (FormValues.length > 0) {
            for (let k = 0; k < FormValues.length; k++) {
              var formValueCheck = FormValues[k].dataObjectName.toLowerCase();
              if (formValueCheck == e.value.toLocaleLowerCase()) {
                count++;
              }
            }
            if (count > 1) {
              secondControl.controls['dataObjectName'].setErrors({ 'customError': true });
            }
          }
          return;
        }
      }
      //check values when select from type ahead list.
      for (let j = 0; j < this.objectList.length; j++) {
        var objectNameCheck = this.objectList[j].objectName.toLowerCase();
        if (objectNameCheck == e.value.toLowerCase()) {
          secondControl.controls['dataObjectName'].setErrors({ 'customError': true });
          return;
        }
      }
    }
  }

  public deleteAllDataObject() {
    let length = this.dataContainerForm.controls['dataObject'].value.length;
    for (let i = 0; i < length; i++) {
      const control = <FormArray>this.dataContainerForm.controls['dataObject'];
      control.removeAt(i);
    }
  }

  onFilterChange(value: string) {
    console.log('filter:', value);
  }


  ngOnInit() {
    this.dataContainerForm = this.fb.group({
      'containerId': '',
      'containerName': ['', [Validators.required, Validators.minLength(1), Validators.maxLength(64), Validators.pattern('^[a-zA-Z0-9\. .,->|/\:()]*$')]],
      'reusalbeName': '',
      'sharedName': '',
      'processId': this.process_Id,
      'dataObject': this.fb.array([
        //this.initDataObject()
      ])
    });
    this.dataModelService.getContainerInfo(parseInt(this.containerId)).subscribe(response => {
      this.containerDetails = response;
      this.conName = this.containerDetails.containerName;
      this.defaultContainerNameOnEdit = this.containerDetails.containerName;
      this.resuableName = this.containerDetails.reusalbeName;
      this.sharedName = this.containerDetails.sharedName;
      console.log("Scope : " + this.containerDetails.previousScope);
      //this.scopeResponse = this.containerDetails.previousScope;
      this.dataModelService.scopeOldResponse = this.containerDetails.previousScope;
      this.previledgeScope = response.previledgeScope;
      this.dataModelService.scopeId = parseInt(this.previledgeScope);
      console.log("Scope id 123  : " + this.previledgeScope);
      console.log("Scope id : " + response.previledgeScope);
      /* this.scopeData = this.dataModelService.getContainerScopeByScopeId(parseInt(this.previledgeScope));
      
      console.log(this.scopeData); */
      this.dataModelService.getContainerScopeByScopeId(parseInt(this.previledgeScope)).subscribe(response => {
        this.treeVal = response;
        this.scopeData = [this.treeVal];
        this.scopeData = this.getData();
      });

      this.dataContainerForm.patchValue({
        containerId: this.containerDetails.containerId,
        containerName: this.containerDetails.containerName,
        reusalbeName: this.containerDetails.reusalbeName,
        sharedName: this.containerDetails.sharedName
      });

      this.patchValue();
      this.dataObject = this.containerDetails.dataObject;
      if (response.dataObject.length > 0) {
        for (var i = 0; i < response.dataObject.length; i++) {
          this.dataObjectName.push(response.dataObject[i].dataObjectName);
        }
      }
      //For Get index of data Object which get by backend.
      if (this.dataContainerForm.controls['dataObject'].value.length > 0) {
        for (var k = 0; k < this.dataContainerForm.controls['dataObject'].value.length; k++) {
          this.indexOfDataObject.push(k);
          //For Get index of data Elemets which get by backend.
          for (var g = 0; g < this.dataContainerForm.controls['dataObject'].value[k].dataElements.length; g++) {
            this.indexOfDataElement.push(g);
          }

        }
      }
    });
    /** This will Fetch all the Container Names for Typeahead */
    this.dataModelService.getContainerNameList().subscribe(data => {
      for (let s of data) {
        this.containerList.push(s);
      }
    });

    /** This will Fetch all the Object Names for Typeahead */
    this.dataModelService.getObjectNameList().subscribe(data => {
      for (let s of data) {
        this.objectList.push(s);
      }
    });

    /** This will Fetch all the Element Names for Typeahead */
    this.dataModelService.getElementNameList().subscribe(data => {
      for (let s of data) {
        this.elementList.push(s);
      }
    });
  }//end of ngOnInit

  getData(): any[] {
    const itCategory = new TreeviewItem(this.scopeData[0]);
    return [itCategory];
  }

  /** This will create the first DataObject with value initialized. */
  public createFirstDataObject(name: string) {
    return this.fb.group({
      'dataObjectCheckedStatus': [''],
      'dataObjectName': name
    });
  }

  /** This will create the data object on click event. */
  initDataObject() {
    return this.fb.group({
      'dataObjectId': [''],
      'dataObjectCheckedStatus': [''],
      'dataObjectName': ['', Validators.required],
      'dataElements': this.fb.array([
        this.initDataElement()
      ])
    });
  }

  /** DataElement checked or not */
  initDataElement() {
    return this.fb.group({
      'elementId': [''],
      'dataElementCheckedStatus': [''],
      'dataElementName': [''],
      'dataElementValueType': [''],
    })
  }

  public getAllProcessNamesList() {
    this.dataModelService.getAllProcessNamesList().subscribe(data => {
      this.processList = data;
      console.log(this.processList);
      this.processList.forEach(element => {
        if (this.process_Id == element.processID) {
          this.acronym = element.acronym;
        }
      });
    });
  }

  //add data object 
  addDataObject(event) {
    const control = <FormArray>this.dataContainerForm.controls['dataObject'];
    control.push(this.initDataObject());

    if(event && event.target){
      console.log('event is', event);
      
    setTimeout(() => {
      this.scrollToTarget();
    }, 0);
  }
  }
  @ViewChild('dataObjectsScrollableContent', { read: ElementRef, static: true }) public dataObjectsScrollableContent: ElementRef<any>;
  @ViewChild('scrollTargetForDataObject', { read: ElementRef, static: true }) public scrollTargetForDataObject: ElementRef<any>;
  scrollToTarget() {
   this.dataObjectsScrollableContent.nativeElement.scrollTo({ top: this.dataObjectsScrollableContent.nativeElement.scrollHeight, behavior: 'smooth' });
    this.scrollTargetForDataObject.nativeElement.scrollIntoView(false);
}
  //delete data object
  deleteDataObject() {
    this.enableDefaultEdit();
    let arr = [];
    let count = 0;
    var i = this.dataContainerForm.controls['dataObject'].value.length;
    while (i--) {
      if (this.dataContainerForm.controls['dataObject'].value[i].dataObjectCheckedStatus == true) {
        arr.push(this.dataContainerForm.controls['dataObject'].value[i]);
        const control = <FormArray>this.dataContainerForm.controls['dataObject'];
        control.removeAt(i);
        count++;
      }
    }
    //delete from patch value array.
    for (let z = 0; z < arr.length; z++) {
      for (let k = 0; k < this.containerDetails.dataObject.length; k++) {
        if (arr[z].dataObjectName == this.containerDetails.dataObject[k].dataObjectName) {
          this.containerDetails.dataObject.splice(k, 1);
        }
      }
    }

    this.selectionForDataObject.clear();
    let lengthAfterDelete = this.dataContainerForm.controls['dataObject'].value.length;
    if (!lengthAfterDelete) {
      this.addDataObject(undefined);
    }
  }



  // Whether the number of selected elements matches the total number of rows.
  isAllDataObjectsSelected() {
    let control = <FormArray>this.dataContainerForm.controls['dataObject'];
    const numSelected = this.selectionForDataObject.selected.length;
    const numRows = control.controls.length;
    return numSelected === numRows;
  }

  // Selects all rows if they are not all selected; otherwise clear selection. 
  masterToggleForDataObjects() {
    let formControl = <FormArray>this.dataContainerForm.controls['dataObject'];
    this.isAllDataObjectsSelected() ?
      this.unCheckAllDataObjects() : this.checkAllDataObjects();
  }

  checkAllDataObjects() {
    let formControl = <FormArray>this.dataContainerForm.controls['dataObject'];
    let length = formControl.value.length;
    formControl.controls.forEach(row => this.selectionForDataObject.select(row));

    for (let i = 0; i < length; i++) {
      formControl.controls[i].get('dataObjectCheckedStatus').setValue(true);
    }
    this.checkAllDataElementsOnAllDataObjectSelection();
  }
  unCheckAllDataObjects() {
    this.selectionForDataObject.clear();
    let formControl = <FormArray>this.dataContainerForm.controls['dataObject'];
    let length = formControl.value.length;
    for (let i = 0; i < length; i++) {
      formControl.controls[i].get('dataObjectCheckedStatus').setValue(false);
    }
    this.unCheckAllDataElementsOnAllDataObjectSelection()
  }
  selectUnselectDataElements(event, dataObjectIndex) {
    if (event.checked) {
      this.chekAllDataElement(dataObjectIndex);
    }
    else {
      this.unCheckAllDataElements(dataObjectIndex);
    }
  }
  //add data element 
  addDataElement(dataObjectIndex) {
    const control = (<FormArray>this.dataContainerForm.controls['dataObject']).at(dataObjectIndex).get('dataElements') as FormArray;
    control.push(this.initDataElement());
  }

  //delete data element
  deleteDataElement(dataObjectIndex, removeIndex) {
    let arr = [];
    var i = this.dataContainerForm.controls['dataObject'].value[dataObjectIndex].dataElements.length;
    while (i--) {
      if (this.dataContainerForm.controls['dataObject'].value[dataObjectIndex].dataElements[i].dataElementCheckedStatus) {
        arr.push(this.dataContainerForm.controls['dataObject'].value[dataObjectIndex].dataElements[i]);
        const control = (<FormArray>this.dataContainerForm.controls['dataObject']).at(dataObjectIndex).get('dataElements') as FormArray;
        control.removeAt(i);
      }
    }
    //delete from patch value array.
    for (let z = 0; z < arr.length; z++) {
      for (let k = 0; k < this.containerDetails.dataObject.length; k++) {
        for(let s=0;s<this.containerDetails.dataObject[k].dataElements.length;s++){
          if (arr[z].dataElementName == this.containerDetails.dataObject[k].dataElements[s].dataElementName) {
          this.containerDetails.dataObject[k].dataElements.splice(s, 1);
        }
        }
      }
    }
    let lenghAfterDelete = this.dataContainerForm.controls['dataObject'].value[dataObjectIndex].dataElements.length;
    if (!lenghAfterDelete) {
      this.addDataElement(dataObjectIndex);
    }
  }

  // for toggle the checked state of check all data element button
  isAllDataElementsSelectedInADataObject(dataObjectIndex) {
    let checkedFields = 0;
    let dataElementsLength = this.dataContainerForm.controls['dataObject'].value[dataObjectIndex].dataElements.length;
    if (!dataElementsLength) {
      return false;
    }
    let i = dataElementsLength
    while (i--) {
      if (this.dataContainerForm.controls['dataObject'].value[dataObjectIndex].dataElements[i].dataElementCheckedStatus) {
        checkedFields++;
      }
    }
    return checkedFields == dataElementsLength;
  }

  //for showing delete button for data element
  isAnyDataElementSeletedInADataObject(dataObjectIndex) {
    let i = this.dataContainerForm.controls['dataObject'].value[dataObjectIndex].dataElements.length;
    let count = 0;
    while (i--) {
      if (this.dataContainerForm.controls['dataObject'].value[dataObjectIndex].dataElements[i].dataElementCheckedStatus) {
        count++;
      }
    }
    if (count > 0 && this.dataContainerForm.controls['dataObject'].value[dataObjectIndex].dataElements.length > 1) {
      return true
    }

    return false;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggleForDataElement(dataObjectIndex, event) {
    const controlForDataElement = (<FormArray>this.dataContainerForm.controls['dataObject']).at(dataObjectIndex).get('dataElements');
    const secondControlForDataElement = <FormArray>controlForDataElement;
    this.checkUncheckAllDataElements(dataObjectIndex, event)
  }

  //check uncheck all data elements on conditions
  checkUncheckAllDataElements(dataObjectIndex, event) {
    if (event.checked) {
      this.chekAllDataElement(dataObjectIndex);
    }
    if (!event.checked) {
      this.unCheckAllDataElements(dataObjectIndex);
    }
  }


  chekAllDataElement(dataObjectIndex) {
    let length = this.dataContainerForm.controls['dataObject'].value[dataObjectIndex].dataElements.length;
    const control = (<FormArray>this.dataContainerForm.controls['dataObject']).at(dataObjectIndex).get('dataElements');
    const secondControl = <FormArray>control
    for (let i = 0; i < length; i++) {
      secondControl.controls[i].get('dataElementCheckedStatus').setValue(true);
    }
    const controlForDataElement = (<FormArray>this.dataContainerForm.controls['dataObject']).at(dataObjectIndex).get('dataElements');
    const secondControlForDataElement = <FormArray>controlForDataElement;
    secondControlForDataElement.controls.forEach(row => this.selectionForDataElement.select(row));
  }
  unCheckAllDataElements(dataObjectIndex) {
    let length = this.dataContainerForm.controls['dataObject'].value[dataObjectIndex].dataElements.length;
    const control = (<FormArray>this.dataContainerForm.controls['dataObject']).at(dataObjectIndex).get('dataElements');
    const secondControl = <FormArray>control
    for (let i = 0; i < length; i++) {
      secondControl.controls[i].get('dataElementCheckedStatus').setValue(false);
    }
  }


  checkAllDataElementsOnAllDataObjectSelection() {
    let formControl = <FormArray>this.dataContainerForm.controls['dataObject'];
    let dataObjectsLength = formControl.value.length;
    for (let i = 0; i < dataObjectsLength; i++) {
      let length = this.dataContainerForm.controls['dataObject'].value[i].dataElements.length;
      const control = (<FormArray>this.dataContainerForm.controls['dataObject']).at(i).get('dataElements');
      const secondControl = <FormArray>control
      for (let k = 0; k < length; k++) {
        secondControl.controls[k].get('dataElementCheckedStatus').setValue(true);
      }
      const controlForDataElement = (<FormArray>this.dataContainerForm.controls['dataObject']).at(i).get('dataElements');
      const secondControlForDataElement = <FormArray>controlForDataElement;
      secondControlForDataElement.controls.forEach(row => this.selectionForDataElement.select(row));
    }

  }


  unCheckAllDataElementsOnAllDataObjectSelection() {
    let formControl = <FormArray>this.dataContainerForm.controls['dataObject'];
    let dataObjectsLength = formControl.value.length;
    for (let i = 0; i < dataObjectsLength; i++) {
      let length = this.dataContainerForm.controls['dataObject'].value[i].dataElements.length;
      const control = (<FormArray>this.dataContainerForm.controls['dataObject']).at(i).get('dataElements');
      const secondControl = <FormArray>control
      for (let k = 0; k < length; k++) {
        secondControl.controls[k].get('dataElementCheckedStatus').setValue(false);
      }
    }
  }

  /**This Function use for Call On every Field Change Function*/
  public enableSubmitButtonForEdit() {
    console.log("enabling........");
    this.submitButtonDisabled = true;
  }


  scopeCanceled() {
    this.selectedInfo = [];
    this.selected = '';
  }


  public cancel() {
    this.router.navigate(['eta-web/datamodel-dashboard'], { queryParams: { action: 'cancel', processId: this.process_Id } })
  }

  /******************************** Display Breadcrumb Menu ************************************** */
  getHeaderMenuList() {
    this.sharedService.getHeaderMenu()
      .subscribe(res => {
        this.menuList = res;
        this.menuList.forEach(element => {
          if (element.routerLink == "#5") {
            this.mainbreadcrumb = element.translation;
          }
          if (element.routerLink == "eta-web/datamodel-dashboard") {
            this.breadcrumb = element.translation;
          }
        })
      })
  }
  /******************** Start Data Container Name Typeahead Method ***********************/
  public getDataContainer(token: string): Observable<any> {
    const query = new RegExp(token, 'i');
    this.containerList.filter((str: any) => {
      if (query.test(str.name)) {
      }
    })
    return of(
      this.containerList.filter((str: any) => {
        return query.test(str.name);
      })
    );
  }

  changeTypeaheadLoadingContainer(e: boolean): void {
    this.typeaheadLoadingContainer = e;
  }

  /*
This Function For Check Container Name Unique When value Select.
*/
  typeaheadOnSelectContainer(e: TypeaheadMatch): void {
    this.processId = this.dataModelService.processId;
    this.dataModelService.getContainerNameValidation(this.processId, e.value).subscribe(response => {
      if (response.status == true) {
        if (this.defaultContainerNameOnEdit !== e.value) {
          this.invalidContainerName = true;
          this.dataContainerForm.controls['containerName'].setErrors({ 'invalid': true });
        }
        else {
          this.invalidContainerName = false;
        }
      } else {
        this.invalidContainerName = false;
      }
    })
  }

  /****************************** For Conatiner Name Special Character Validation *********************/
  /*checkSpecialChar(event){
    var k; 
    k = event.charCode;
    return((k > 64 && k < 91) || (k > 96 && k < 123) || (k >= 48 && k <= 57) || k == 8 || k == 32 || k == 40 || k == 41 || k == 58 || k == 92 || k == 124 || k == 95 || k == 62 || (k > 43 && k < 48)); 
  }*/

  public updateDataContainer() {
    console.log(this.dataModelService.scopeId);
    this.dataModelService.updateDataContainer(this.dataContainerForm.value, JSON.stringify(this.scopeData), this.dataModelService.scopeOldResponse, this.dataModelService.scopeId).subscribe(data => {
      console.log(JSON.stringify(data));

      this.router.navigate(['eta-web/datamodel-dashboard'], { queryParams: { action: 'update', processId: this.process_Id, name: this.name } })
    });
  }

  public getAllDataElementType() {
    this.dataModelService.getAllDataElementType().subscribe(data => {
      this.dataElementValueTypeList = data;
      console.log(this.dataElementValueTypeList);
    });
  }

  /*
This Function For Check Data Object Name Unique When value Typed.
*/
  checkOnKeyUp(event, index) {
    if (event && event.target && event.target.value) {
      const objectDetails = this.containerDetails.dataObject;
      const FormValues = this.dataContainerForm.controls['dataObject'].value;
      const control = <FormArray>this.dataContainerForm.controls['dataObject'];
      let secondControl = <FormArray>control.controls[index];
      for (let i = 0; i < objectDetails.length; i++) {
         //check from edit patch value array.
         var trimData = objectDetails[i].dataObjectName.trim();
        var checkConstant = trimData.toLowerCase();
        var currentTrim = event.target.value.trim();
        if (checkConstant == currentTrim.toLowerCase()) {
          let count = 0;
           //check values when select same as patch value.
          if (FormValues.length > 0) {
            for (let k = 0; k < FormValues.length; k++) {
              var trimValues = FormValues[k].dataObjectName.trim();
              var formValueCheck = trimValues.toLowerCase();
              var currentCheck = event.target.value.trim();
              if (formValueCheck == currentCheck.toLocaleLowerCase()) {
                count++;
              }
            }
            if (count > 1) {
              secondControl.controls['dataObjectName'].setErrors({ 'customError': true });
            }
          }
          return;
        }
      }
      //check values when select from type ahead list.
      for (let j = 0; j < this.objectList.length; j++) {
        var trimcheck = event.target.value.trim();
        var objectNameCheck = this.objectList[j].objectName.toLowerCase();
        if (objectNameCheck == trimcheck.toLowerCase()) {
          secondControl.controls['dataObjectName'].setErrors({ 'customError': true });
          return;
        }
      }
       //check values when type same new values.
      let countNew = 0;
      if (FormValues.length > 0) {
        for (let k = 0; k < FormValues.length; k++) {
          var trimCheckForm = FormValues[k].dataObjectName.trim();
          var trimCurrentValue = event.target.value.trim();
          var formValueCheck = trimCheckForm.toLowerCase();
          if (formValueCheck == trimCurrentValue.toLocaleLowerCase()) {
            countNew++;
          }
        }
        if (countNew > 1) {
          secondControl.controls['dataObjectName'].setErrors({ 'customError': true });
        }
      }
    }
  }

  /******************** Start Data Object Name Typeahead Method ***********************/
  public getDataObject(token: string): Observable<any> {
    const query = new RegExp(token, 'i');
    this.objectList.filter((str: any) => {
      if (query.test(str.objectName)) {
      }
    })
    return of(
      this.objectList.filter((str: any) => {
        return query.test(str.objectName);
      })
    );
  }


  /******************** Start Data Element Name Typeahead Method ***********************/
  public getDataElement(token: string): Observable<any> {
    const query = new RegExp(token, 'i');
    this.elementList.filter((str: any) => {
      if (query.test(str.elementName)) {
      }
    })
    return of(
      this.elementList.filter((str: any) => {
        return query.test(str.elementName);
      })
    );
  }

  changeTypeaheadLoadingElement(e: boolean): void {
    this.typeaheadLoadingElement = e;
  }

  /*
 This Function For Check Element Name Unique When value Select.
 */
  typeaheadOnSelectElement(e: TypeaheadMatch, index, dataObjectIndex): void {
    const objectDetails = this.containerDetails.dataObject;
    const FormValues = this.dataContainerForm.controls['dataObject'].value;
    console.log('objectDetails', objectDetails);
    console.log('FormValues', FormValues);
    const controlElementObject = <FormArray>this.dataContainerForm.controls['dataObject'];
    let secondControlElement = <FormArray>controlElementObject.controls[dataObjectIndex];
    let controlElement = <FormArray>secondControlElement.controls['dataElements'].controls[index];

    if (objectDetails.length > 0) {
      for (let i = 0; i < objectDetails.length; i++) {
        for (let z = 0; z < objectDetails[i].dataElements.length; z++) {
          var checkSelect = objectDetails[i].dataElements[z].dataElementName.toLowerCase();
          if (checkSelect == e.value.toLowerCase()) {
            let count = 0;
            if (FormValues.length > 0) {
              for (let k = 0; k < FormValues.length; k++) {
                for (let y = 0; y < FormValues[k].dataElements.length; y++) {
                  var formValueCheck = FormValues[k].dataElements[y].dataElementName.toLowerCase();
                  if (formValueCheck == e.value.toLocaleLowerCase()) {
                    count++;
                  }
                }
              }
              if (count > 1) {
                controlElement.controls['dataElementName'].setErrors({ 'elementError': true });
              }
            }
            return;
          }
        }
      }
      for (let j = 0; j < this.elementList.length; j++) {
        var elemmentValue = this.elementList[j].elementName.toLowerCase();
         if(elemmentValue == e.value.toLowerCase()){
          controlElement.controls['dataElementName'].setErrors({ 'elementError': true });
          return;
        }
      }
    }


  }

  /*
   This Function For Check Element Name Unique When value Typed.
   */
  checkOnKeyUpElementName(event, objectIndex, dataElementIndex) {
    const FormValues = this.dataContainerForm.controls['dataObject'].value;
    const objectDetails = this.containerDetails.dataObject;
    const controlElementObject = <FormArray>this.dataContainerForm.controls['dataObject'];
    let secondControlElement = <FormArray>controlElementObject.controls[objectIndex];
    let controlElement = <FormArray>secondControlElement.controls['dataElements'].controls[dataElementIndex];
    for (let i = 0; i < objectDetails.length; i++) {
      for (let z = 0; z < objectDetails[i].dataElements.length; z++) {
        var trimCheck = objectDetails[i].dataElements[z].dataElementName.trim();
        var checkSelect = trimCheck.toLowerCase();
        var currentTrim = event.target.value.trim();
        if (checkSelect == currentTrim.toLowerCase()) {
          let count = 0;
          if (FormValues.length > 0) {
            for (let k = 0; k < FormValues.length; k++) {
              for (let y = 0; y < FormValues[k].dataElements.length; y++) {
                var trimCheckForm = FormValues[k].dataElements[y].dataElementName.trim();
                var formValueCheck = trimCheckForm.toLowerCase();
                var currentCheckForm = event.target.value.trim();
                if (formValueCheck == currentCheckForm.toLocaleLowerCase()) {
                  count++;
                }
              }
            }
            if (count > 1) {
              controlElement.controls['dataElementName'].setErrors({ 'elementError': true });
            }
          }
          return;
        }
      }
    }
    let countNew = 0;
    if (FormValues.length > 0) {
      for (let k = 0; k < FormValues.length; k++) {
        for (let y = 0; y < FormValues[k].dataElements.length; y++) {
          var formValueTrim = FormValues[k].dataElements[y].dataElementName.trim();
          var formValueCheck = formValueTrim.toLowerCase();
          var formValuesCheck = event.target.value.trim();
          if (formValueCheck == formValuesCheck.toLocaleLowerCase()) {
            countNew++;
          }
        }
      }
      if (countNew > 1) {
        controlElement.controls['dataElementName'].setErrors({ 'elementError': true });
      }
    }
    for (let j = 0; j < this.elementList.length; j++) {
      var listTrim = this.elementList[j].elementName.trim();
      var elemmentValue = listTrim.toLowerCase();
      var currentElement = event.target.value.trim();
       if(elemmentValue == currentElement.toLowerCase()){
        controlElement.controls['dataElementName'].setErrors({ 'elementError': true });
        return;
      }
    }
  }

  /*
  This function is use for When containerName AlphaNumeric Validation.
  */
  checkCharValidationForContainer(value) {
    var str = value.target.value;
      if (str) {
      const matches = str.match(/^[A-Za-z][A-Za-z0-9/.,->_()\\*|:" ]*$/);
      return matches?null: this.dataContainerForm.controls['containerName'].setErrors({ 'patternError': true });
      }
  }

  /*
   This function is use for When Data Object AlphaNumeric Validation.
   */
  checkCharValidationForObject(value, index) {
    var str = value.target.value;
      if (str) {
      const matches = str.match(/^[A-Za-z][A-Za-z0-9/.,->_()\\*|:" ]*$/);
      const control = <FormArray>this.dataContainerForm.controls['dataObject'];
        let secondControl = <FormArray>control.controls[index];
      return matches?null: secondControl.controls['dataObjectName'].setErrors({ 'patternObjectError': true });
      }
  }

  /*
   This function is use for When Data Element AlphaNumeric Validation.
   */
  checkCharValidationForElement(value, index, dataObjectIndex) {
    var str = value.target.value;
    if (str) {
    const matches = str.match(/^[A-Za-z][A-Za-z0-9/.,->_()\\*|:" ]*$/);
    const controlElementObject = <FormArray>this.dataContainerForm.controls['dataObject'];
          let secondControlElement = <FormArray>controlElementObject.controls[dataObjectIndex];
          let controlElement = <FormArray>secondControlElement.controls['dataElements'].controls[index];
    return matches?null: controlElement.controls['dataElementName'].setErrors({ 'patternElementError': true });
    }
  }

  ngAfterViewChecked(){
    console.log("Selected scopes : "+this.values);
    
    if(this.countOnEdit != 0){
      if(this.countOnEdit != this.values.length){
        this.disabledOnEdit = true;
        console.log("Disable on edit : "+this.disabledOnEdit);
      }
    }
    
    this.countOnEdit = this.values.length;
    console.log("Count scope : "+this.countOnEdit);
    
  }
  /* 
    enableEdit(){
      this.disabledOnEdit = true;
      console.log(this.disabledOnEdit);
    } */


}//end of the class
