import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditDatamodelComponent } from './edit-datamodel.component';

describe('EditDatamodelComponent', () => {
  let component: EditDatamodelComponent;
  let fixture: ComponentFixture<EditDatamodelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditDatamodelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditDatamodelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
