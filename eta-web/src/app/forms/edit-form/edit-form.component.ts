import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import {FormControl} from '@angular/forms'; 
import { ServiceService } from './../../eta-services/service.service'
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ArrayType } from '@angular/compiler/src/output/output_ast';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatDialogConfig} from '@angular/material';
import { ScriptApprovalApplicationFormComponent } from '../../popup/script-approval-application-form/script-approval-application-form.component';
import { SelectionModel } from '@angular/cdk/collections';
import { ActivatedRoute, Router } from '@angular/router';
import { FormServiceService } from '../form-services/form-service.service';
import { FormElementType } from '../../model/form-element-type';
import { Validation } from '../../model/validation';
import { DataElement } from '../../model/data-element';
import { Observable, of } from 'rxjs';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead';
import { mergeMap } from 'rxjs/operators';
import { MasterData } from '../../model/master-data';
import { FormFields } from '../../model/form-fields';
import { Reference } from '../../model/reference';
import { FormFactoryComponent } from '../../dynamic-form-builder/form-factory/form-factory.component';
import { FieldConfig } from '../../dynamic-form-builder/field.interface';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';



@Component({
  selector: 'app-edit-form',
  templateUrl: './edit-form.component.html',
  styleUrls: ['./edit-form.component.css']
})

export class EditFormComponent implements OnInit {  
  public mainbreadcrumb: string;
  public breadcrumb: string;
  public menuList: any = [];
  editForm: FormGroup;
  dataElementStore : string = '';
  processName : string = '';
  processAcronym : string = '';
  processId : string = '';
  name : string = '';
  formId : number;
  typeaheadLoadingDataElement: boolean;
  public invalidFormName : boolean = false;
  public invalidFieldName : boolean = false;
  public duplicateField : number;
  public formName : string = '';
  jsonAdd : string = "";
    //reusable 
  reusable: boolean = false;

   formElementList : FormElementType[] =[];
   masterDataGroupList : MasterData[] =[];
   refList : Reference[] =[];
   dataElementDatasource :  Observable<any>;
   dataElementList : any [] = []
   //dataElementList : DataElement [] = []
   formGroupList : any [] = [];
   validationList : Validation[] =[];
   formFieldsList : FormFields[] = [];
   defaultValue : string = '';
   mandatoryStatus : boolean = false;
   regConfig: FieldConfig[] = [];
   editChangeStatus : boolean = true;
   inputTypeCheck : number;
   disableReference : boolean = false;
   disableSubmitButtonForReference : boolean = false;
   disableStoreForForm : boolean = false;
   countIndex : number;
   isLoadingResults : boolean = false;
   dataList = [];
   @ViewChild('tableScrollableContent', { read: ElementRef, static: true }) public tableScrollableContent: ElementRef<any>;
  constructor(private spinnerService: Ng4LoadingSpinnerService, private fb: FormBuilder, public dialog: MatDialog, private route: ActivatedRoute, private router: Router, private shareServices: ServiceService, private formService: FormServiceService) {
    this.isLoadingResults = true; 
    this.formId = this.formService.formId;
      this.processName = this.formService.processName;
      this.processId = this.formService.processId;
      this.formName = this.formService.formName;
      if(this.formService.all == '0'){
        this.formService.processId = '';
        this.formService.processName = '';
      }
          

      if(this.processName == null || this.processName.length < 1 )
          this.router.navigateByUrl('eta-web/formdashboard');

      this.getHeaderMenuList();

      //NAME VALUE OBSERVABLE
      /*this.dataElementDatasource = Observable.create((observer: any) => {
        observer.next(this.name);
      })
      .pipe(
        mergeMap((token: string) => this.getDataElement(token))
      );*/


      //DEFAULT VALUE OBSERVABLE
      /*this.dataElementDatasource = Observable.create((observer: any) => {
        observer.next(this.defaultValue);
      })
      .pipe(
        mergeMap((token: string) => this.getDataElement(token))
      );*/

   }//end of the constructor

 
  
  ngOnInit() {
        this.initilizeFormGroup();
        this.getFormElementTypes();
        this.getAllValidations();
        this.getAllDataElementNames();
        this.getFormDetailsByFormId(this.formId);
        //this.getMasterDataGroupList();
        this.getReferenceList();
  }// end of the ngOnInit

  ngAfterViewInit(){
    this.isLoadingResults = false; 
  }

  getHeaderMenuList() {
    this.shareServices.getHeaderMenu()
      .subscribe(res => {
        this.menuList = res;
        this.menuList.forEach(element => {
          if (element.routerLink == "#5") {
            this.mainbreadcrumb = element.translation;
          }
          if (element.routerLink == "eta-web/formdashboard") {
            this.breadcrumb = element.translation;
          }
        })

      })
  }

  public checkFormName(formName : string) : void{
    this.formService.getFormNameValidation(this.processId, formName).subscribe(response => {
      if(response.status == true){
       if(this.formName != formName){
            this.editForm.controls['formName'].setErrors({'invalid': true});
          this.invalidFormName = true;
       }else{
        this.invalidFormName = false;
       }
    }else {
      this.invalidFormName = false;
    }
    })
}//end of the method



  public validateName(fieldName : string, index : number) : void {
     let length = this.editForm.value.form_group.length;
     if(length > 3){
         if(index > 1){
               for(let i = 0; i < length; i++){                
                 
                 if(this.editForm.value.form_group[i].name != 'Submit' && this.editForm.value.form_group[i].name != 'Cancel'){
                     if(this.editForm.value.form_group[i].name == fieldName){
                      // console.log(i+"=="+index);
                       if(i != (index - 1 )){
                         this.invalidFieldName = true;
                         this.duplicateField = index;
                         return;
                       }else{
                         console.log("i : "+i  + "        index -1 : " +(index - 1));                         
                       }                       
                     }else{
                       this.duplicateField = 8000;
                       this.invalidFieldName = false;
                     }
                 }         
             }
         }else{
           if(length > 3){
                  for(let i = 0; i < length; i++){                
                    if(this.editForm.value.form_group[i].name != 'Submit' && this.editForm.value.form_group[i].name != 'Cancel'){
                        if(this.editForm.value.form_group[i].name == fieldName){
                        // console.log(i+"=="+index);
                          if(i != (index - 1 )){
                            this.invalidFieldName = true;
                            this.duplicateField = index;
                            return;
                          }else{
                            console.log("i : "+i  + "        index -1 : " +(index - 1));                         
                          }                       
                        }else{
                          this.duplicateField = 8000;
                          this.invalidFieldName = false;
                        }
                    }//end of if          
                }//end of for
           }
         }
     }//if length      
 }//end of the method

  public enableSubmitButtonForEdit() {
    this.editChangeStatus = false;
  }

  public selectInputType(inputType : number){
    this.inputTypeCheck = inputType;
    if(this.inputTypeCheck == 4 || this.inputTypeCheck == 5 || this.inputTypeCheck == 6 || this.inputTypeCheck == 7){
        this.disableStoreForForm = false;
        this.editChangeStatus = true;
        this.disableReference = false;
        this.disableSubmitButtonForReference = true;
    }else if(this.inputTypeCheck == 12){
        this.disableStoreForForm = true;
    }else{
      this.disableReference = true;
      this.disableStoreForForm = false;
    }
  }
  
  public referenceSelectedForInput(){
    this.disableSubmitButtonForReference = false;
    this.editChangeStatus = false;
  }

  public getFormDetailsByFormId(formId : number) : void{
      this.formService.getFormDetailsByFormId(formId).subscribe(response => {
        this.formName = response.formName;
        this.name = response.name;
        this.processAcronym = response.processAcronym;
        this.formGroupList = response.form_group;
        this.editForm.patchValue({
          formName: response.formName, 
          reusable : response.reusable,
          processId : response.processId,
          formId : response.id,
          //defaultValue : response.defaultValue
        });
        this.formFieldsList = response.form_group;
        console.log('this.formFieldsList',this.formFieldsList);
        this.formFieldsList = this.formFieldsList.sort(function(a,b){return a.index - b.index});
        this.formFieldsList.forEach(fd => {
          if(fd.mandatory == "1")
            this.mandatoryStatus = true;
          else
            this.mandatoryStatus = false;
          if(fd.name === 'Submit'){
              this.addFormFieldButtonSubmit(fd.index, fd.typeId, fd.field_id, fd.name, this.mandatoryStatus , fd.dataElementStore, fd.defaultValue, parseInt(fd.reference), fd.helpText, parseInt(fd.validationId));
          }else if(fd.name === 'Cancel'){
              this.addFormFieldButtonCancel(fd.index, fd.typeId, fd.field_id, fd.name, this.mandatoryStatus, fd.dataElementStore, fd.defaultValue, parseInt(fd.reference), fd.helpText, parseInt(fd.validationId));
          }else{
              this.countIndex = fd.index;
              this.addFormField(fd.index, fd.field_id, fd.name, fd.typeId, this.mandatoryStatus, fd.dataElementStore, fd.defaultValue, parseInt(fd.reference), fd.helpText, parseInt(fd.validationId));
          }           
        });
     })
  }//end of the getFormDetailsByFormId

      /*getDataElement(token: string): Observable<any> {
        const query = new RegExp(token, 'i'); 
        this.dataElementList.filter((str: any) => {     
          if(query.test(str.name)){
            this.dataElementStore = str.display_element_id;
          }
        })
        return of(
          this.dataElementList.filter((str: any) => {     
            return query.test(str.name);
          })
        );
      }*/


      changeTypeaheadLoadingDataElement(e: boolean): void {
        this.typeaheadLoadingDataElement = e;
      }

      typeaheadOnSelectForm(e: TypeaheadMatch): void {
        this.name = e.value;
      }

      changeTypeaheadLoadingDataElementDefaultValue(e: boolean): void {
        this.typeaheadLoadingDataElement = e;
      }


      typeaheadOnSelectFormDefaultValue(e: TypeaheadMatch): void {
        this.defaultValue = e.value;
      }

      public getFormElementTypes() : void {
        this.formService.getAllElementTypes().subscribe(response =>{
          this.formElementList = response;
        })
      }


      public getAllValidations() : void {
        this.formService.getAllValidations().subscribe(response =>{
          this.validationList = response;
        })
      }


      public getAllDataElementNames() : void {
        this.formService.getDataElementNames().subscribe(response =>{
          this.dataList = response;
          /*for(let s of response) {
            this.dataElementList.push(s);
          }*/
        })
      }

      public getMasterDataGroupList() : void {
        this.formService.getMasterDataGroupList().subscribe(response => {
          this.masterDataGroupList = response;
        })
      }
  
      public changeForm() : void {
        if(this.invalidFormName)   //this will check if the form name is valid
          return;

        if(this.invalidFieldName)   // this will check if all the field names are valid
          return;  
        this.formService.editForm(this.editForm.value).subscribe(response => {
          this.formService.updateStatus = true;
          let qp = { formName : this.editForm.get('formName').value };
          this.router.navigate(['/eta-web/formdashboard'], {queryParams: qp});
        })
      }//end of the addForm method

      public openFormList() : void {
        this.router.navigateByUrl('eta-web/formdashboard');
      }

      shownRouterLink(routerlink) {
        this.router.navigate(["/" + routerlink]);
      }


    initilizeFormGroup(){
          /* Initiate the form structure - first default */
          this.editForm = this.fb.group({
            formId : [],
            formName: [],
            reusable: [],
            processId: [this.processId],
            form_group: this.fb.array([this.fb.group({
              filedCheckStatus: '',
              index: '1',
              name:'', 
            type: '',
            mandatory: true,
            dataElementStore:'',
            defaultValue: '',
            reference: '',
            helpText: '',
            validationId: ''
      
          })]),            
          }) 
          this.formGroup.removeAt(0);
        //for second default field
       
          this.formGroup.push(this.fb.group({
            filedCheckStatus: '', 
            index: '2',
            name:'Submit', 
            type: 'submit',
            mandatory: true,
            dataElementStore:'',
            defaultValue: '',
            reference: '',
            helpText: '',
            validationId: ''
        }));

        this.formGroup.removeAt(0);
          //for thire default field
          this.formGroup.push(this.fb.group({
            filedCheckStatus: '', 
            index: '3',
            name:'Cancel', 
            type: 'cancel',
            mandatory: true,
            dataElementStore:'',
            defaultValue: '',
            reference: '',
            helpText: '',
            validationId: ''
        }));

        this.formGroup.removeAt(0);
    }


      get formGroup() {
        return this.editForm.get('form_group') as FormArray;
      }


        addNewFormField() {
              this.formGroup.insert(this.formGroup.length - 2, (this.fb.group({
                filedCheckStatus: '', 
                index: [this.formGroup.length - 1],
                name:['',[Validators.required, Validators.minLength(1), Validators.maxLength(32),Validators.pattern('^[a-zA-Z0-9\. .,->|/\:()]*$')]], 
                typeId: '',
                mandatory: true,
                dataElementStore:'',
                defaultValue: '',
                reference: '',
                helpText: ['',[Validators.maxLength(32),Validators.pattern('^[a-zA-Z0-9\. \\.\\,\\-\\+]*$')]],
                validationId: ''
            })));
        
        }


    
        /**
         * Creating the form fields dynamically based on the value added in add
         * @param field_id  
         * @param name 
         * @param typeId 
         * @param mandatory 
         * @param dataElementStore 
         * @param defaultValue 
         * @param reference 
         * @param helpText 
         * @param id 
         */
        public addFormField(displayOrder : number, field_id : number, name : string, typeId : number, mandatory : boolean, dataElementStore : number, defaultValue : string, reference : number, helpText : string, id : number) {
          console.log('reference',reference);
          if(displayOrder == null){
            this.countIndex = this.countIndex + 1;  
            displayOrder = this.countIndex;
          }
              
          this.formGroup.insert(this.formGroup.length - 2, (this.fb.group({
                  filedCheckStatus: '', 
                  index: displayOrder,
                  field_id : field_id,
                  name: name, 
                  typeId: typeId,
                  mandatory: mandatory,
                  dataElementStore: dataElementStore,
                  defaultValue: defaultValue,
                  reference: reference,
                  helpText: helpText,
                  validationId: id
              })));
        }
        
        scrollToTarget(el: HTMLElement) {
          this.tableScrollableContent.nativeElement.scrollTo({ top: this.tableScrollableContent.nativeElement.scrollHeight, behavior: 'smooth' });
          el.scrollIntoView(false);
      }
      public addFormFieldButtonSubmit(displayOrder : number, typeId : number, field_id : number, name : string, mandatory : boolean, dataElementStore : number, defaultValue : string, reference : number, helpText : string, validation : number) {
                
            this.formGroup.push(this.fb.group({
                  filedCheckStatus: '', 
                  index: displayOrder,
                  field_id : field_id,
                  name:'Submit', 
                  typeId: typeId,
                  mandatory: mandatory,
                  dataElementStore:'',
                  defaultValue: '',
                  reference: '',
                  helpText: helpText,
                  validationId: ''
              }));
        
        }

        public addFormFieldButtonCancel(displayOrder : number, typeId : number,field_id : number, name : string,  mandatory : boolean, dataElementStore : number, defaultValue : string, reference : number, helpText : string, validation : number) {
              //for thire default field
              this.formGroup.push(this.fb.group({
                filedCheckStatus: '', 
                index: displayOrder,
                field_id : field_id,
                name:'Cancel', 
                typeId: typeId,
                mandatory: mandatory,
                dataElementStore:'',
                defaultValue: '',
                reference: '',
                helpText: helpText,
                validationId: ''
            }));
        }


      myFunction(){
        this.editForm.get('')
      }


      deleteFormField() {
        this.editChangeStatus = false;
            //this.formGroup.removeAt(this.checkedField);
          var i = this.editForm.value.form_group.length
            while (i--) {
              if (this.editForm.value.form_group[i].filedCheckStatus == true) { 
                  this.formGroup.removeAt(i);
                } 
            }
            this.selection.clear();
            let lengthAfterDelete = this.editForm.value.form_group.length;
            if((this.editForm.controls['reusable'].value && lengthAfterDelete < 1) || (!this.editForm.controls['reusable'].value && lengthAfterDelete < 3)){
              this.addFormField(null, null, '', null, true, null, '', null, '', null);
            }
      }  
      createDefaultFieldsOnReusableChange(){
        if(!this.reusable){
          if(!this.checkSubmitExistance()){
            this.addFormFieldButtonSubmit(null, null, null, '', true, null, '', null, '', null);
          }
          if(!this.checkCancelButtonExistance()){
            this.addFormFieldButtonCancel(null, null, null, '', true, null, '', null, '', null);
          }
        }
      }
      checkSubmitExistance(){
        let length = this.editForm.value.form_group.length;
        for(let i = 0; i < length; i++){
          console.log(this.editForm.value.form_group[i]);
          if (this.editForm.value.form_group[i].name.toLowerCase() == 'submit') { 
            return true;
            } 
        }
      }
      checkCancelButtonExistance(){
        let length = this.editForm.value.form_group.length;
        for(let i = 0; i < length; i++){
          if (this.editForm.value.form_group[i].name.toLowerCase() == 'cancel') { 
            return true;
            }
        }
      }
      checkDuplicacy(event, index){
        let length = this.editForm.value.form_group.length
        let count = 0;
        let control =  <FormArray>this.editForm.controls.form_group;
        let secondControl = control.controls;
        for(let i = 0; i < length; i++){
          if (this.editForm.value.form_group[i].name.toLowerCase() == event.target.value.toLowerCase()) { 
         count++
          }
        if(count > 1){
          secondControl[index].get('name').markAsTouched();
            secondControl[index].get('name').setErrors({'duplicateName': true})
        }
        }
      }
      //code for preview popup
      openDialog(): void {
            this.jsonAdd = JSON.stringify(this.editForm.value);
            this.isLoadingResults = true; 
            this.formService.getDynamicFormJson(this.jsonAdd ).subscribe(response => {
              //for change order of submit and cancel.
              let arrayDisplayOrder = [];
              var largest;
              for (var i = 0; i < response.length; i++) {
                arrayDisplayOrder.push(response[i].displayOrder);
              }
              largest = Math.max.apply(null, arrayDisplayOrder);
              for (var i = 0; i < response.length; i++) {
               // if (response[i].displayOrder == 0) {
                  if (response[i].type == "submit") {
                    response[i].displayOrder = ++largest;
                  } else if (response[i].type == "cancel") {
                    response[i].displayOrder = largest + 1;
                  }
               // }
              }
              function  GetSortOrder(prop)  {
                return  function (a,  b)  {
                  if  (a[prop]  >  b[prop])  {
                    return  1;
                  }  else if  (a[prop]  <  b[prop])  {
                    return  -1;
                  }
                  return  0;
                }
              };
              response.sort(GetSortOrder("displayOrder"));
              console.log('response',response);
              this.regConfig = response;
              const dialogConfig = new MatDialogConfig();
              dialogConfig.autoFocus = true;
              dialogConfig.disableClose = true;
              dialogConfig.data = {
                  jsonData : this.jsonAdd,
                  formName : this.formName,
                  regConfig : this.regConfig
              };
              this.isLoadingResults = false;
              const dialogRef = this.dialog.open(FormFactoryComponent, dialogConfig);
            
              dialogRef.afterClosed().subscribe(result => {
                console.log('The dialog was closed');
              });
          }); 
        
       }

      //for selection
      selection = new SelectionModel(true, []);

      /** Whether the number of selected elements matches the total number of rows. */
      isAllSelected() {
        const numSelected = this.selection.selected.length;
        const numRows = this.formGroup.controls.length;
        return numSelected === numRows;
      }
    
     /** Selects all rows if they are not all selected; otherwise clear selection. */
      masterToggle() {
        this.isAllSelected() ?
            this.unCheckAllFormFields() : this.checkAllFormFields()
      }
  checkAllFormFields(){
    this.formGroup.controls.forEach(row => this.selection.select(row));
    let length = this.editForm.controls['form_group'].value.length;
    let control = <FormArray>this.editForm.controls['form_group'];
    for(let i = 0; i < length; i++){
      control.controls[i].get('filedCheckStatus').setValue(true);
     }
  }
  unCheckAllFormFields(){
    this.selection.clear();
    let length = this.editForm.controls['form_group'].value.length;
    let control = <FormArray>this.editForm.controls['form_group'];
    for(let i = 0; i < length; i++){
      control.controls[i].get('filedCheckStatus').setValue(false);
     }
  }
  
      public selectedElement(elementTypeId : any){
        this.formService.getReferenceTypeByElementId(elementTypeId).subscribe(resposne =>{
            this.refList = resposne;
        });
      }//end of the method

      public getReferenceList(){
        this.formService.getReferenceList().subscribe(resposne =>{
            this.refList = resposne;
        });
      }//end of the method

}//end of the class
