import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ServiceService } from './../../eta-services/service.service'
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ArrayType } from '@angular/compiler/src/output/output_ast';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatDialogConfig } from '@angular/material';
import { ScriptApprovalApplicationFormComponent } from '../../popup/script-approval-application-form/script-approval-application-form.component';
import { SelectionModel } from '@angular/cdk/collections';
import { ActivatedRoute, Router } from '@angular/router';
import { FormServiceService } from '../form-services/form-service.service';
import { FormElementType } from '../../model/form-element-type';
import { Validation } from '../../model/validation';
import { DataElement } from '../../model/data-element';
import { Observable, of } from 'rxjs';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead';
import { mergeMap } from 'rxjs/operators';
import { MasterData } from '../../model/master-data';
import { FormFactoryComponent } from '../../dynamic-form-builder/form-factory/form-factory.component';
import { FieldConfig } from '../../dynamic-form-builder/field.interface';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
//import { HttpClient } from 'selenium-webdriver/http';
import { HttpClient, HttpHeaders, HttpRequest, HttpEvent, HttpParams} from '@angular/common/http';
//import { FormValidationService } from '../form-services/form-validation.service';


@Component({
  selector: 'app-add-form',
  templateUrl: './add-form.component.html',
  styleUrls: ['./add-form.component.css']
})
export class AddFormComponent implements OnInit {
  jsonAdd: string;
  public mainbreadcrumb: string;
  public breadcrumb: string;
  public menuList: any = [];
  addNewForm: FormGroup;
  dataElementStore: string = '';
  processName: string = '';
  processId: string = '';
  name: string = '';
  typeaheadLoadingDataElement: boolean;
  public invalidFormName: boolean = false;
  public invalidFieldName: boolean = false;
  public duplicateField: number;
  reusable: boolean = false;

  formElementList: FormElementType[] = [];
  masterDataGroupList: MasterData[] = [];
  dataElementDatasource: Observable<any>;
  dataElementList: any[] = []
  //formNamesList : any[] = [];
  validationList: Validation[] = [];
  defaultValue: string = "";
  regConfig: FieldConfig[] = [];
  inputTypeCheck: number;
  inputTypeMasterSelectStatus: boolean = true;
  disableReference: boolean = true;
  disableStoreForForm: boolean = false;
  dataList = [];
  isLoadingResults:boolean = false;

  constructor(private spinnerService: Ng4LoadingSpinnerService, private fb: FormBuilder, public dialog: MatDialog, private route: ActivatedRoute, private router: Router, 
    private shareServices: ServiceService, private formService: FormServiceService, private httpClient: HttpClient) {

    this.processName = this.formService.processName;
    this.processId = this.formService.processId;

    if (this.processName == null || this.processName.length < 1)
      this.router.navigateByUrl('eta-web/formdashboard');

    this.getHeaderMenuList();


    /*this.dataElementDatasource = Observable.create((observer: any) => {
      observer.next(this.name);
    })
      .pipe(
        mergeMap((token: string) => this.getDataElement(token))
      );*/

    /*this.dataElementDatasource = Observable.create((observer: any) => {
      observer.next(this.defaultValue);
    })
      .pipe(
        mergeMap((token: string) => this.getDataElement(token))
      );*/


  }//end of the constructor


  ngOnInit() {
    this.initilizeFormGroup();
    this.getFormElementTypes();
    this.getAllValidations();
    this.getAllDataElementNames();
    this.getMasterDataGroupList();
    // this.getFormNameByProcessId();
  }

  getHeaderMenuList() {
    this.shareServices.getHeaderMenu()
      .subscribe(res => {
        this.menuList = res;
        this.menuList.forEach(element => {
          if (element.routerLink == "#5") {
            this.mainbreadcrumb = element.translation;
          }
          if (element.routerLink == "eta-web/formdashboard") {
            this.breadcrumb = element.translation;
          }
        })

      })
  }

  /* public getFormNameByProcessId() : void {
    this.formService.getFormNameByProcessId(parseInt(this.processId)).subscribe(response => {
      this.formNamesList = response;
    })
  } */


  /*public getDataElement(token: string): Observable<any> {
    const query = new RegExp(token, 'i');

    this.dataElementList.filter((str: any) => {
      if (query.test(str.name)) {
        this.dataElementStore = str.display_element_id;
      }
    })
    return of(
      this.dataElementList.filter((str: any) => {
        return query.test(str.name);
      })
    );
  }*/


  changeTypeaheadLoadingDataElement(e: boolean): void {
    this.typeaheadLoadingDataElement = e;
  }

  changeTypeaheadLoadingDataElementDefaultValue(e: boolean): void {
    this.typeaheadLoadingDataElement = e;
  }

  typeaheadOnSelectForm(e: TypeaheadMatch): void {
    this.name = e.value;
  }


  typeaheadOnSelectFormDefaultValue(e: TypeaheadMatch): void {
    this.defaultValue = e.value;
  }

  public getFormElementTypes(): void {
    this.formService.getAllElementTypes().subscribe(response => {
      this.formElementList = response;
    })
  }


  public getAllValidations(): void {
    this.formService.getAllValidations().subscribe(response => {
      this.validationList = response;
    })
  }


  public getAllDataElementNames(): void {
    this.formService.getDataElementNames().subscribe(response => {
     // for (let s of response) {
       // this.dataElementList.push(s);
       this.dataList = response;
        console.log('this.dataElementList',this.dataList);
      //}
    })
  }

  public getMasterDataGroupList(): void {
    this.formService.getMasterDataGroupList().subscribe(response => {
      this.masterDataGroupList = response;
    })
  }

  /**
   * This function will add the new form and for this it will send request to backend from service
   */


  public addForm(): void {

    console.log('this.addNewForm.value',this.addNewForm.value);
    if (this.invalidFormName)   //this will check if the form name is valid
      return;
    if (this.invalidFieldName)   // this will check if all the field names are valid
      return;
      for(var i=0;i<this.addNewForm.value.form_group.length;i++){
        if(this.addNewForm.value.form_group[i].name == "Submit"){
          this.addNewForm.value.form_group[i]["dataElementStore"] = "Submit"
        }else if(this.addNewForm.value.form_group[i].name == "Cancel"){
          this.addNewForm.value.form_group[i]["dataElementStore"] = "Cancel"
        }
      }
    this.formService.addForm(this.addNewForm.value).subscribe(response => {
      let qp = { formName: this.addNewForm.get('formName').value };
      this.formService.addStatus = true;
      this.router.navigate(['eta-web/formdashboard'], { queryParams: qp });
    })
  }

  public checkFormName(event: string): void {
    this.formService.getFormNameValidation(this.processId, event).subscribe(response => {
      if (response.status == true) {
        this.addNewForm.controls['formName'].setErrors({ 'invalid': true });
        this.invalidFormName = true;
      } else {
        this.invalidFormName = false;
      }
    })
  }

  public validateName(fieldName: string, index: number): void {
    let  length  =  this.addNewForm.value.form_group.length;
    if (length > 3) {
      if (index > 1) {
        for (let  i  =  0;  i  <  length;  i++) {

          if (this.addNewForm.value.form_group[i].name != 'Submit' && this.addNewForm.value.form_group[i].name != 'Cancel') {
            if (this.addNewForm.value.form_group[i].name == fieldName) {
              console.log(i + "==" + index);
              if (i != (index - 1)) {
                this.invalidFieldName = true;
                this.duplicateField = index;
                return;
              }
            } else {
              this.duplicateField = 8000;
              this.invalidFieldName = false;
            }
          }//end of if          
        }//end of for
      }//if index
    }//if length      
  }//end of the method

  public openFormList(): void {
    this.router.navigateByUrl('eta-web/formdashboard');
  }

  shownRouterLink(routerlink) {
    this.router.navigate(["/" + routerlink]);
  }


  initilizeFormGroup() {
    /* Initiate the form structure - first default */
    this.addNewForm = this.fb.group({
      formName: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(64), Validators.pattern('^[a-zA-Z{1}][a-zA-Z0-9/. .,-/(/):/\\|_>]*$')]],
      reusable: [],
      processId: [this.processId],
      form_group: this.fb.array([this.fb.group({
        filedCheckStatus: '',
        index: '1',
        name: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(32), Validators.pattern('^[a-zA-Z{1}][a-zA-Z0-9/. .,-/(/):/\\|_>]*$')]],
        typeId: '',
        mandatory: true,
        dataElementStore: ['', [Validators.required, Validators.minLength(1)]],
        defaultValue: ['', [ Validators.maxLength(32), Validators.pattern('^[a-zA-Z{1}][a-zA-Z0-9/. .,-/(/):/\\|_>]*$')]],
        reference: '',
        helpText: ['', [ Validators.maxLength(32), Validators.pattern('^[a-zA-Z{1}][a-zA-Z0-9\. \\.\\,\\-\\+]*$')]],
        validationId: ''
      })]),

    })
    this.addSubmitButtonField();
    this.addCancelButtonField();
  }
printForm(){
  console.log(this.formElementList);
  
}
  public addSubmitButtonField() {
    this.formGroup.push(this.fb.group({
      filedCheckStatus: '',
      index: '',
      name: 'Submit',
      typeId: 13,
      mandatory: true,
      dataElementStore: 'Submit',
      defaultValue: '',
      reference: '',
      helpText: '',
      validationId: ''
    }));
  }
  public addCancelButtonField() {
    this.formGroup.push(this.fb.group({
      filedCheckStatus: '',
      index: '',
      name: 'Cancel',
      typeId: 14,
      mandatory: true,
      dataElementStore: 'Cancel',
      defaultValue: '',
      reference: '',
      helpText: '',
      validationId: ''
    }));
  }
  get formGroup() {
    return this.addNewForm.get('form_group') as FormArray;
  }

  // checkName(event, index){
  //   console.log(event.target.value.toLowerCase());

  // if((event.target.value.toLowerCase() == 'submit' && this.checkSubmitExistance()) || 
  // (event.target.value.toLowerCase() == 'cancel' && this.checkCancelButtonExistance())){
  //   console.log('I got error');

  //   let control =  <FormArray>this.addNewForm.controls.form_group;
  //   let secondControl = control.controls;

  //   secondControl[index].get('name').markAsTouched();
  //   if(event.target.value.toLowerCase() == 'submit'){
  //     secondControl[index].get('name').setErrors({'duplicateSubmitButton': true});
  //   }
  //   else if(event.target.value.toLowerCase() == 'cancel'){
  //     secondControl[index].get('name').setErrors({'duplicateCancelButton': true});
  //   }
  // }
  // }
  createDefaultFieldsOnReusableChange() {
    if (!this.reusable) {
      if (!this.checkSubmitExistance()) {
        this.addSubmitButtonField();
      }
      if (!this.checkCancelButtonExistance()) {
        this.addCancelButtonField();
      }
    }
  }


  checkSubmitExistance() {
    let length = this.addNewForm.value.form_group.length;
    for (let i = 0; i < length; i++) {
      if (this.addNewForm.value.form_group[i].name.toLowerCase() == 'submit') {
        return true;
      }
    }
  }
  checkCancelButtonExistance() {
    let length = this.addNewForm.value.form_group.length;
    for (let i = 0; i < length; i++) {
      if (this.addNewForm.value.form_group[i].name.toLowerCase() == 'cancel') {
        return true;
      }
    }
  }
  checkDuplicacy(event, index) {
    let length = this.addNewForm.value.form_group.length
    let count = 0;
    let control = <FormArray>this.addNewForm.controls.form_group;
    let secondControl = control.controls;
    for (let i = 0; i < length; i++) {
      if (this.addNewForm.value.form_group[i].name.toLowerCase() == event.target.value.toLowerCase()) {
        count++
      }
      if (count > 1) {
        secondControl[index].get('name').markAsTouched();
        secondControl[index].get('name').setErrors({ 'duplicateName': true })
      }
    }
  }
  /////// This is new /////////////////
 
  
  addFormField() {
    this.formGroup.insert(this.formGroup.length - 2, (this.fb.group({
      filedCheckStatus: '',
      index: [this.formGroup.length - 1],
      name: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(32), Validators.pattern('^[a-zA-Z{1}][a-zA-Z0-9/. .,-/(/):/\\|_>]*$')]],
      typeId: '',
      mandatory: true,
      dataElementStore: ['', [Validators.required, Validators.minLength(1)]],
      defaultValue: '',
      reference: '',
      helpText: ['', [Validators.maxLength(32), Validators.pattern('^[a-zA-Z{1}][a-zA-Z0-9\. \\.\\,\\-\\+]*$')]],
      validationId: ''
    })));
   
  }
  @ViewChild('tableScrollableContent', { read: ElementRef, static: true }) public tableScrollableContent: ElementRef<any>;
  scrollToTarget(el: HTMLElement) {
    this.tableScrollableContent.nativeElement.scrollTo({ top: this.tableScrollableContent.nativeElement.scrollHeight, behavior: 'smooth' });
    el.scrollIntoView(false);
}
  public selectInputType(inputType: number) {
    this.inputTypeCheck = inputType;
    if (this.inputTypeCheck == 4 || this.inputTypeCheck == 5 || this.inputTypeCheck == 6 || this.inputTypeCheck == 7 || this.inputTypeCheck == 12 || this.inputTypeCheck == 16) {
      this.inputTypeMasterSelectStatus = true;
      this.disableReference = false;
    } else if (this.inputTypeCheck == 12) {
      this.disableStoreForForm = true;
    } else {
      this.disableReference = true;
    }
  }

  public selectReference() {
    this.inputTypeMasterSelectStatus = false;
  }

  


  deleteFormField() {
    var i = this.addNewForm.value.form_group.length
    while (i--) {
      if (this.addNewForm.value.form_group[i].filedCheckStatus == true) {
        this.formGroup.removeAt(i);
      }
    }
    this.selection.clear();
    let lengthAfterDelete = this.addNewForm.value.form_group.length;
    if ((this.addNewForm.controls['reusable'].value && lengthAfterDelete < 1) || (!this.addNewForm.controls['reusable'].value && lengthAfterDelete < 3)) {
      this.addFormField();
    }
  }

  //code for popup
  openDialog(): void {
    this.jsonAdd = JSON.stringify(this.addNewForm.value);
    this.isLoadingResults = true;
    this.formService.getDynamicFormJson(this.jsonAdd).subscribe(response => {
      //for change order of submit and cancel.
      let arrayDisplayOrder = [];
      var largest;
      for (var i = 0; i < response.length; i++) {
        arrayDisplayOrder.push(response[i].displayOrder);
      }
      largest = Math.max.apply(null, arrayDisplayOrder);
      for (var i = 0; i < response.length; i++) {
        if (response[i].displayOrder == 0) {
          if (response[i].type == "submit") {
            response[i].displayOrder = ++largest;
          } else if (response[i].type == "cancel") {
            response[i].displayOrder = largest + 1;
          }
        }
      }
      function  GetSortOrder(prop)  {
        return  function (a,  b)  {
          if  (a[prop]  >  b[prop])  {
            return  1;
          }  else if  (a[prop]  <  b[prop])  {
            return  -1;
          }
          return  0;
        }
      };
      response.sort(GetSortOrder("displayOrder"));
      //pass on config for form.
      this.regConfig = response;
      const dialogConfig = new MatDialogConfig();
      dialogConfig.autoFocus = true;
      dialogConfig.disableClose = true;
      dialogConfig.data = {
        jsonData: this.jsonAdd,
        formName: this.addNewForm.get('formName').value,
        regConfig: this.regConfig
      };
      this.isLoadingResults = false;
      const dialogRef = this.dialog.open(FormFactoryComponent, dialogConfig);
      dialogRef.afterClosed().subscribe(result => {
        console.log('The dialog was closed');
      });
    });

//this.publishFormFuntion(this.processId);
  }
      // funtion for publish
      /* publishFormFuntion(processId){
        this.httpClient.get('http://localhost:8080' + '/publishForm' + '/' + processId, { responseType: 'text' })
          .subscribe(data => {
          alert(data);
        
          });
      } */

  //for selection
  selection = new SelectionModel(true, []);

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.formGroup.controls.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.unCheckAllFormFields() : this.checkAllFormFields()
  }
  checkAllFormFields() {
    this.formGroup.controls.forEach(row => this.selection.select(row));
    let length = this.addNewForm.controls['form_group'].value.length;
    let control = <FormArray>this.addNewForm.controls['form_group'];
    for (let i = 0; i < length; i++) {
      control.controls[i].get('filedCheckStatus').setValue(true);
    }
  }
  unCheckAllFormFields() {
    this.selection.clear();
    let length = this.addNewForm.controls['form_group'].value.length;
    let control = <FormArray>this.addNewForm.controls['form_group'];
    for (let i = 0; i < length; i++) {
      control.controls[i].get('filedCheckStatus').setValue(false);
    }
  }


}//end of the class
