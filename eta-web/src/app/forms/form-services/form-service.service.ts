import { Injectable, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { HttpClient, HttpHeaders, HttpRequest, HttpEvent, HttpParams} from '@angular/common/http';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import 'rxjs/add/operator/map';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment.test';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};


@Injectable({
  providedIn: 'root'
})
export class FormServiceService {
  adminRole = "admin";
  myRole;
  processName : string = '';
  processId : string = '';
  formId : number;
  formName : string;
  all : string ='';
  addStatus : boolean = false;
  updateStatus : boolean = false;

  
  constructor(private location: Location, private cookieService: CookieService, private r: Router, private httpClient: HttpClient) {
    this.myRole = cookieService.get('my_role');
    this.checkedSessionToken();

   }//end of the constructor


   /* toGetToken */
  public checkedSessionToken(): boolean {
    let istokenExist: boolean = false;
    if (sessionStorage.getItem("AuthToken") != null) {
      istokenExist = true;
    }
    return istokenExist;
  }

  public getProcessAcronymWithId(): Observable<any>  {
    return this.httpClient.post(environment.Core_URL + '/getAllProcessAcronymWithId', httpOptions);
  }//end of the method

  /**This is call for new Function all details */
  
  public getFormDataTableDetails(order: string, page : number, pageSize : number): Observable<any> {
    console.log("order : "+order);
    console.log("page : "+page);
    console.log("size : "+pageSize);
    const body = JSON.stringify({
      "page": page,
      "size" : pageSize,
      "direction": order,
      "columnName" : "formName",
      observe: 'response'
    });
    return this.httpClient.post<any>(environment.Core_URL  + '/getAllFormsInfo', body, httpOptions);
  }

  /**This is call for new Function all details */
  
  public getFormDataTableDetailsByProcessId(order: string, page : number, pageSize : number, processId: number): Observable<any> {
    console.log("order : "+order);
    console.log("page : "+page);
    console.log("size : "+pageSize);
    console.log("Process Id :"+ processId);
    
    //pageSize = 10;
    const body = JSON.stringify({
      "page": page,
      "size" : pageSize,
      "direction": order,
      "columnName" : "formName",
      "searchId" : processId,
      observe: 'response'
    });
    return this.httpClient.post<any>(environment.Core_URL  + '/getAllFormsInfoByProcessId', body, httpOptions);
  }


  public addForm(formDetails: any): Observable<any> {
    console.log("add form");

    const body = JSON.stringify(formDetails);

    return this.httpClient.post<any>(environment.Core_URL  + '/addForm', body, httpOptions);
  }

  public editForm(formDetails: any): Observable<any> {
    console.log("add form");
    const body = JSON.stringify(formDetails);
    console.log(body);
    
    return this.httpClient.post<any>(environment.Core_URL  + '/editForm', body, httpOptions);
  }

  public deleteFormById(formIdsList : any[]): Observable<any> {
    console.log("formIds : "+formIdsList);
    
    return this.httpClient.get<any>(environment.Core_URL  + '/deleteFormById/'+formIdsList);
  }

  public getFormDetailsByFormId(formId : number): Observable<any> {
    console.log("formId : " + formId);
    
    return this.httpClient.get<any>(environment.Core_URL  + '/getFormDetailsByFormId/' + formId);
  }
  

  public getAllElementTypes(): Observable<any> {
    console.log("getAllElementTypes . ");
    return this.httpClient.get<any>(environment.Core_URL  + '/getAllElementType');
  }


  public getAllValidations(): Observable<any> {
    console.log("getAllValidations. ");
    return this.httpClient.get<any>(environment.Core_URL  + '/getAllValidations');
  }


  public getDataElementNames(): Observable<any> {
    console.log("getDataElementNames . ");
    return this.httpClient.get<any>(environment.Core_URL  + '/getDataElementNames');
  }

  public getMasterDataGroupList(): Observable<any> {
    console.log("getMasterDataGroupList . ");
    return this.httpClient.get<any>(environment.Core_URL  + '/getMasterDataGroup');
  }

  public getReferenceList(): Observable<any> {
    console.log("getReferenceList . ");
    return this.httpClient.get<any>(environment.Core_URL  + '/getReferenceList');
  }


  public getReferenceTypeByElementId(typeId : number): Observable<any> {
    console.log("getReferenceTypeByElementId . ");
    return this.httpClient.get<any>(environment.Core_URL  + '/getReferenceTypeByElementId/'+typeId);
  }


  public getFormNameByProcessId(processId : number) : Observable<any> {
    console.log("calling getFormNameByProcessId process Id : " + processId);
    return this.httpClient.get<any>(environment.Core_URL + '/getFormNameByProcessId/' + processId);    
  }

  public getFormNameValidation(processId : string, formName : string) : Observable<any> {
    console.log("Process ID : " + processId);    
    console.log("Form name : " + formName);
    return this.httpClient.get<any>(environment.Core_URL + '/searchFormInfo/' + processId + '/' + formName);    
  }

  public getDynamicFormJson(formJson: string): Observable<any[]> {
    console.log("formJson : "+formJson);

    const body = JSON.stringify(formJson);
    console.log("json body : "+body);
    
    return this.httpClient.post<any[]>(environment.Core_URL  + '/getDynamicFormJson', formJson, httpOptions);
  }
}//end of the class
