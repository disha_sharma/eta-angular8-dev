import { Injectable, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders, HttpRequest, HttpEvent, HttpParams} from '@angular/common/http';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import 'rxjs/add/operator/map';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { ServiceService } from './../eta-services/service.service';
import { Body } from '@angular/http/src/body';
import { ProcessDetails } from './../form/core/process/Process';
//import { formDirectiveProvider } from '@angular/forms/src/directives/reactive_directives/form_group_directive';

@Injectable({
  providedIn: 'root'
})
export class PreferenceServiceService {
  adminRole = "admin";
  myRole;
  constructor(private cookieService: CookieService, private r: Router, private httpClient: HttpClient, private service : ServiceService) { 
    this.myRole = cookieService.get('my_role');
    this.checkedSessionToken();
  }

   /* toGetToken */
   public checkedSessionToken(): boolean {
    let istokenExist: boolean = false;
    if (sessionStorage.getItem("AuthToken") != null) {
      istokenExist = true;
    }
    return istokenExist;
  }
    
  
/**This is call for Get Language Data */
  public getLanguageList(){
    return this.httpClient.get<any>(this.service.Master_URL  + '/getAllLanguage');
  }
/**This is call for Get Currency Data */
  public getCurrencyList(){
    return this.httpClient.get<any>(this.service.Org_Cal_URL  + '/getAllCurrency');
  }
  /**This is call for Get  Data */
  public getPreferenceList(){
    return this.httpClient.get<any>(this.service.User_URL  + '/getUserPreferencesByUserId');
  }

  /**This is call for Get  Data */
  public getWorkshiftList(){
    return this.httpClient.get<any>(this.service.Org_Cal_URL  + '/getWorkshiftByOrgId');
  }

  /**This is call for Get Channel Data */
  public getChannelList(){
    return this.httpClient.get<any>(this.service.User_URL  + '/getAllChannel');
  }

  /**This is call for Get Week Data */
  public getWorkWeekList(){
    return this.httpClient.get<any>(this.service.Org_URL  + '/getWorkWeekByOrgId');
  }

  /**This is call for Submit  Data */
  public UpdateData(data,userId){
   data.userId = userId;
    return this.httpClient.post<any>(this.service.User_URL  + '/updateUserPreferencesBasedOnUserId ',data);
  }
}
