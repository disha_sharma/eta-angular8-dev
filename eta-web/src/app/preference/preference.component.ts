import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PreferenceServiceService } from './preference-service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-preference',
  templateUrl: './preference.component.html',
  styleUrls: ['./preference.component.css']
})
export class PreferenceComponent implements OnInit {
  form: FormGroup;
  languageList: any ;
  CurrencyList : any;
  prefernceDetails  : any;
  userId : any;
  WorkshiftList :  any;
  channelList : any;
  WorkWeekList : any;
  constructor(private fb: FormBuilder,private service : PreferenceServiceService, private router : Router) { }

  ngOnInit() {
    this.form = this.fb.group({
      'language': ['', Validators.required],
      'communicationChannel': ['', Validators.required],
      'currency': ['', Validators.required],
      'workWeek': ['', Validators.required],
      'workShift': ['', Validators.required],
      'currentPassword': ['', Validators.required]
    });
   
    this.getLanguageList();
    this.getCurrencyList();
    this.getWorkshiftList();
    this.getChannelList();
    this.getWorkWeekList();
    this.service.getPreferenceList().subscribe(response => {
      this.prefernceDetails = response;
      this.userId = this.prefernceDetails.userId;
      this.form.patchValue({
        language: this.prefernceDetails.language,
        communicationChannel: this.prefernceDetails.communicationChannel,
        currency: this.prefernceDetails.currency,
        workWeek: this.prefernceDetails.workWeek,
        workShift: this.prefernceDetails.workShift,
      })
    });
    
  }

/**This is call on Cancel Button*/
  cancel(){
    this.router.navigate(['/eta-web/welcome']);
  }
  

  /**This method is use for update the recod */
  saveRecord(value){
    this.service.UpdateData(value,this.userId).subscribe(response => {
      if(response){
        this.router.navigate(['/eta-web/welcome']);
      }
    });
  }

  getWorkshiftList(){
    this.service.getWorkshiftList().subscribe(data => {
      this.WorkshiftList = data;
      console.log('WorkshiftList',this.WorkshiftList);
    });
  }

  getChannelList(){
    this.service.getChannelList().subscribe(data => {
      this.channelList = data;
    });
  }

  getWorkWeekList(){
    this.service.getWorkWeekList().subscribe(data => {
      this.WorkWeekList = data;
    });
  }

  /**This method is use for get language data */
  getLanguageList(){
    this.service.getLanguageList().subscribe(data => {
      this.languageList = data;
    });
  }
 /**This method is use for get Currency data */
  getCurrencyList(){
    this.service.getCurrencyList().subscribe(data => {
      this.CurrencyList = data;
    });
  }

}





