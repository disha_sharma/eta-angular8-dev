import { TestBed } from '@angular/core/testing';

import { PreferenceServiceService } from './preference-service.service';

describe('PreferenceServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PreferenceServiceService = TestBed.get(PreferenceServiceService);
    expect(service).toBeTruthy();
  });
});
