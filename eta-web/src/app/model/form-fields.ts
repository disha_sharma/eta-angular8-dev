
export class FormFields {
    field_id : number;
    typeId : number;
    formId : number;
    name : string;
    mandatory : string;
    type : string;
    dataElementStore : number;
    defaultValue : string;
    reference : string;
    helpText : string;
    validationId : string;
    index : number;


    constructor(field_id : number, typeId : number, formId : number, name : string, mandatory : string, type : string, dataElementStore : number, defaultValue : string, reference: string, helpText: string, validationId: string, index:number){
        this.field_id = field_id;
        this.typeId = typeId;
        this.formId = formId;
        this.name = name;
        this.mandatory  = mandatory;
        this.type = type;
        this.dataElementStore = dataElementStore;
        this.defaultValue = defaultValue;
        this.reference = reference;
        this.helpText = helpText;
        this.validationId = validationId;
        this.index = index;
    }// end of the constructor
    
}//end of the class

