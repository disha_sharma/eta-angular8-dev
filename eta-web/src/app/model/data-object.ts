import { ElementDetails } from "./element";

export class DataObject {
     dataObjectId : number;
     dataObjectName : string;
     dataElements : ElementDetails[] = [];
    constructor(dataObjectId : number,  dataObjectName : string){
        this.dataObjectId = dataObjectId;
        this.dataObjectName = dataObjectName;
    }// end of the constructor
    
}//end of the class