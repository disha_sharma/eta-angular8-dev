export class DataElement {
    id : number;
    name : string;
    dataTypeId : string;
    elementSourceId : string;
    behaviourId : string;
    mandatory : string;


    constructor(id : number, name : string, dataTypeId : string, elementSourceId : string, behaviourId : string, mandatory : string){
        this.id = id;
        this.name = name;
        this.dataTypeId = this.dataTypeId;
        this.elementSourceId  = elementSourceId;
        this.behaviourId = behaviourId;
        this.mandatory = mandatory;
    }// end of the constructor
    
}//end of the class
