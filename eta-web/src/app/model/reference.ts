/**
 * This class will contain the Labels information which will be retrieved from the database
 */

export class Reference {
    refId : number;
    refName : string;
    formElementTypeId : number;

    constructor(refId : number, refName : string, formElementTypeId : number){
        this.refId = refId;
        this.refName = refName;
        this.formElementTypeId = formElementTypeId;
    }// end of the constructor
    
}//end of the class