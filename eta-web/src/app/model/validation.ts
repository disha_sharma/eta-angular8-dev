export class Validation {
    id : number;
    validationType : string;
    regex : string;
    message : string;
    desc : string;

    constructor(id : number, validationType : string, regex : string, message : string, desc : string){
        this.id = id;
        this.validationType = validationType;
        this.regex = regex;
        this.message  = message;
        this.desc = desc;
    }// end of the constructor
    
}//end of the class
