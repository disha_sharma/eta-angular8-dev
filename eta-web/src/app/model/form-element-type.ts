export class FormElementType {
    public typeId : number;
    public name : string;
    public multiselect : string;
    public htmlName : string;
    public htmlType : string;
    public helpText : string;


    constructor(typeId : number, name : string, multiselect : string, htmlName : string, htmlType : string, helpText : string){
        this.typeId = typeId;
        this.name = name;
        this.multiselect = this.multiselect;
        this.htmlName  = htmlName;
        this.htmlType = htmlType;
        this.helpText = helpText;
    }// end of the constructor
    
}//end of the class
