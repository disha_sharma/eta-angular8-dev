import { DataObject } from "./data-object";

export class Container {
    containerId : number;
    containerName : string;
    reusalbeName : boolean
    sharedName : boolean;
    previousScope : string;
    updatedScope : string;
    processId : number;
    previledgeScope : number;
    dataObject : DataObject[] = [];
    constructor(containerId : number, containerName : string, reusalbeName : boolean, sharedName : boolean, processId : number, previousScope : string, updatedScope: string, previledgeScope : number){
        this.containerId = containerId;
        this.containerName = containerName;
        this.reusalbeName = reusalbeName;
        this.sharedName  = sharedName;
        this.processId = processId;
        this.previousScope = previousScope;
        this.updatedScope = updatedScope;
        this.previledgeScope = previledgeScope;
    }// end of the constructor
    
}//end of the class