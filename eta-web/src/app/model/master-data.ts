export class MasterData {
    groupID : number;
    groupName : string;
    isActive : string;
    itemcount : string;
    scope : string;
    status : string;
    createdBy : string;
    createdDate : string;
    modifiedBy : string;
    modifiedDate : string;
    typeId : string;

    constructor(groupID : number,  groupName : string, isActive : string, itemcount : string, scope : string, status : string, createdBy : string,  createdDate : string,  modifiedBy : string, modifiedDate : string, typeId : string ){
        this.groupID = groupID;
        this.groupName = groupName;
        this.isActive = isActive;
        this.itemcount = itemcount;
        this.scope = scope;
        this.status = status;
        this.createdBy = createdBy;
        this.createdDate = createdDate;
        this.modifiedBy = modifiedBy;
        this.modifiedDate = modifiedDate;
        this.typeId = typeId;
    }// end of the constructor
    
}//end of the class