export class ElementDetails {
    elementId : number;
    dataElementName : string;

    constructor(elementId : number,  dataElementName : string){
        this.elementId = elementId;
        this.dataElementName = dataElementName;
    }// end of the constructor
    
}//end of the class