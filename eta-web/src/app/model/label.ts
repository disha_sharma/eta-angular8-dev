/**
 * This class will contain the Labels information which will be retrieved from the database
 */

export class LabelInfo {
    id : number;
    labelName : string;
    labelValue : string;
    orgId : number;

    constructor(id : number, labelName : string, labelValue : string, orgId : number){
        this.id = id;
        this.labelName = labelName;
        this.labelValue = labelValue;
        this.orgId  = orgId;
    }// end of the constructor
    
}//end of the class
