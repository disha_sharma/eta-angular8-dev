export class UserFormData {
        formID : number;
        formName: string;
        formType: string;
        formDescription: string;
        formStatusID : number;
        createdBy : number;
        createdDate : string;
        modifiedBy : number;
        modifiedDate : string;
        isdeleted: number;
        active : number;
        createdName: string;
        modifiedName: string;
        recordsTotal : number;
        formFieldCount : number;
        constructor(){
        }//end of the constructor
    }