import { Container } from "./container";

export class ContainerMapping {
    scopeOld : string;
    scopeUpdated : string;
    dataContainerMapping : string;
    scopeId : number;
    constructor(scopeOld : string, scopeUpdated: string, dataContainerMapping : string, scopeId : number){
        this.scopeOld = scopeOld;
        this.scopeUpdated = scopeUpdated;
        this.dataContainerMapping = dataContainerMapping;
        this.scopeId = scopeId;
    }// end of the constructor
    
}//end of the class