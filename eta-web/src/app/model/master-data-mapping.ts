export class MasterDataMapping {
    masterData : string;
    scopeData : string;
    scopeId : number;

    constructor( masterData : string, scopeData : string, scopeId : number){
        this.masterData = masterData;
        this.scopeData = scopeData;
        this.scopeId = scopeId;
    }//end of the constructor
}//end of the class