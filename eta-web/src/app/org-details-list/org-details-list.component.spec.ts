import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrgDetailsListComponent } from './org-details-list.component';

describe('OrgDetailsListComponent', () => {
  let component: OrgDetailsListComponent;
  let fixture: ComponentFixture<OrgDetailsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrgDetailsListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrgDetailsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
