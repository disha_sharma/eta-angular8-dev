import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ServiceService } from '../eta-services/service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-org-details-list',
  templateUrl: './org-details-list.component.html',
  styleUrls: ['./org-details-list.component.css']
})
export class OrgDetailsListComponent implements OnInit {

  url:any;
  allOrgDetails:any;
  newOrgDetails:any=[];

  constructor( private http: HttpClient, private taskService: ServiceService, private router: Router) {
    this.url = this.taskService.Org_Cal_URL
    this.getAllOrgDetails(); 
  }

  ngOnInit() {
    
  }

  getAllOrgDetails(){
    console.log(this.url);
    
    this.http.get(this.url + "/getAllOrgDetails").subscribe(res => {
      this.allOrgDetails = res
      console.log(this.allOrgDetails);
      this.removeBlankOrgDetails();
    })
  }

  removeBlankOrgDetails(){

    this.allOrgDetails.forEach((element,i) => {
      console.log(element.orgId,element.orgName,element.acronym, element.orgDomain, element.emailaddress );
      
      if(element.orgName != null && element.acronym != null && element.orgDomain != null && element.emailaddress != null ){
        this.newOrgDetails.push(element)
      }
      
    });
    console.log('this.newOrgDetails',this.newOrgDetails);
    
  }
  editOrgDetail(orgId){
     this.router.navigate(['/editOrgDetail'],{queryParams:{"orgId":orgId}})
  }
  addNewCalendar(){
    this.router.navigate(['/addOrgDetail'])
  }
}
