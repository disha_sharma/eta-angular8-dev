import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

import { ActivatedRoute, Router } from '@angular/router';
import { ServiceService } from '../eta-services/service.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

export interface DialogData {
  taskId: "",
  taskName: ''
}

@Component({
  selector: 'app-popup-template',
  templateUrl: './popup-template.component.html',
  styleUrls: ['./popup-template.component.css'],
  providers: [ServiceService]
})
export class PopupTemplateComponent implements OnInit {


  Core_URL: any;
  taskfrm: FormGroup;
  taskObj: Object = {};
  privilegeObj: Object = {};
  allFrom: any;
  allProcess: any;
  allTask: any;
  allTaskType: any;
  subTaskType: any;
  taskEditObj: any;
  isEdit = false;

  gridTranslationColumns: any;
  isDataLoaded = false;
  taskTransUrl: any;
  get_Data: any;
  taskId: any;
  privilegeId: any;
  formDropDownDisabled: boolean = false;
  gridTaskColumns: any;


  taskUrl: any;
  public defaultColumnToSort: any;
  public recordsTotal: number;

  /* Testing Only */
  dataElementList: any = [];
  relationalOpraters: any = [];


  /**
   *For Two Way router Conditions
   *
   * @type {*}
   * @memberof TaskComponent
   */
  public twoWayTaskCondition: number;

  /** EmailTemplate Name List */
  templateNameList: any = [];

  CreatedTaskObj: any = {};


  constructor(private _formBuilder: FormBuilder,
  private http: HttpClient,
  private shareServices: ServiceService,
  private r: Router,
  @Inject(MAT_DIALOG_DATA) public popupData: DialogData) {
    
  this.defaultColumnToSort = "taskName"
  this.Core_URL = this.shareServices.Core_URL;
  this.taskUrl = this.Core_URL + '/getAllInnerTaskByOrgIdGrid';
  this.getHeaderMenuList();
  this.getAllDataElement();

  }

  ngOnInit() {

  

  
  this.getTaskColoumnList();
  this.getAllProcess();
  this.getAllTaskType();
  this.getEmailTemplateNameInfo();
  this.shareServices.currentMessage.subscribe(message => this.taskEditObj = message);
  //this.popupData = "Hakesh You done";

  this.taskfrm = this._formBuilder.group({
    tasktitle: [],
    taskdes: [],
    previlege: [],
    previlegename: [],
    previlegedes: [],
    process: [],
    previoustask: [],
    type: [],
    subtype: [],
    form: [],
    lasttask: [],
    firstDataElement: [],
    relationalOpraters: [],
    secondDataElement: [],
    /* Task IF/ELSE conditions */
    taskTrue: [],
    taskFalse: [],
    templateName: [],

    /* For Repeating Block */
    itemRows: this._formBuilder.array([this.initItemRows()])

  });
  this.taskfrm.setControl('itemRows', this._formBuilder.array([]));

  /** only for edit */
  if (this.taskEditObj != null && this.taskEditObj != 'editFalse') {
    this.isEdit = true;
    this.taskfrm.patchValue({ tasktitle: this.taskEditObj.taskName });
    this.taskfrm.patchValue({ taskdes: this.taskEditObj.taskDescription });
    this.taskfrm.patchValue({ previlege: this.taskEditObj.previlege });
    this.taskfrm.patchValue({ previlegename: this.taskEditObj.privilegeName });
    this.taskfrm.patchValue({ previlegedes: this.taskEditObj.privilegeDescription });
    this.taskfrm.patchValue({ process: this.taskEditObj.processID });
    this.taskfrm.patchValue({ type: this.taskEditObj.taskTypeID });
    this.taskfrm.patchValue({ subtype: this.taskEditObj.subTaskTypeID });
    this.taskfrm.patchValue({ form: this.taskEditObj.formId });
    this.taskfrm.patchValue({ lasttask: this.taskEditObj.isStartOrEntTask });
    this.getAllTaskByProcessId(this.taskEditObj.processID);
    this.taskfrm.patchValue({ previoustask: this.taskEditObj.previousTaskID });
    this.taskId = this.taskEditObj.taskID;
    this.privilegeId = this.taskEditObj.privilegeId;


  }
  else {
    this.isEdit = false;
  }
  }

  initItemRows() {
  return this._formBuilder.group({
    // list all your form controls here, which belongs to your form array
    relationalConditionRow: [],
    firstDataElementRow: [],
    relationalOpratersRow: [],
    secondDataElementRow: []
  });
  }

  addNewRow() {
  // control refers to your formarray
  const control = <FormArray>this.taskfrm.controls['itemRows'];
  // add new formgroup
  control.push(this.initItemRows());
  }


  public getAllProcess() {
  this.http.get(this.Core_URL + '/getAllProcess')
    .subscribe(data => {
    this.allProcess = data;
    });
  }
  public getAllForm() {
  this.http.get(this.Core_URL + '/getAllForm')
    .subscribe(data => {
    this.allFrom = data;
    let formList: any = [];
    formList = data;
    if (formList.length == 0) {
      alert("No form avilable!!")
    }
    });
  }
  public getAllTaskType() {
  this.http.get(this.Core_URL + '/getAllTaskType')
    .subscribe(data => {
    this.allTaskType = data;
    if (this.isEdit) {
      this.taskChange(this.taskEditObj.taskTypeID);
      this.taskfrm.patchValue({ subtype: this.taskEditObj.subTaskTypeID });
    }
    });
  }

  public taskChange(changevalue: any) {
  if (changevalue == 1) {
    this.formDropDownDisabled = false;
    this.getAllForm();
  } else {
    this.formDropDownDisabled = true;
    this.allFrom = null;
  }
  this.allTaskType.forEach(element => {
    if (element.taskTypeID == changevalue) {
    this.subTaskType = element.subTaskTypeDAO;
    }
  });
  }

  public cancel() {
  this.clearShareObj();
  this.r.navigateByUrl('taskdashboard');
  }


  processId: number;
  public getAllTaskByProcessId(processId: number) {

  this.processId = processId;
  this.http.get(this.Core_URL + '/getAllTaskByProcessId/' + processId)
    .subscribe(data => {
    let taskList: any = [];
    taskList = data;

    if (taskList.length == 0) {
      alert('No task available');
    }
    this.allTask = data;
    console.log(data);

    });
  }

  onSubmit = function (taskValue) {

  /**
   * To Store 2way task Array list
   *
   * @type {*}
   * @memberof TaskComponent
   */
  let task2WayArray = [];


  if (this.isEdit) {
    this.taskEditObj.taskName = this.taskfrm.value.tasktitle;
    this.taskEditObj.taskDescription = this.taskfrm.value.taskdes;
    this.taskEditObj.previlege = this.taskfrm.value.previlege;
    this.taskEditObj.privilegeName = this.taskfrm.value.previlegename;
    this.taskEditObj.privilegeDescription = this.taskfrm.value.previlegedes;
    this.taskEditObj.processID = this.taskfrm.value.process;
    this.taskEditObj.previousTaskID = this.taskfrm.value.previoustask;
    this.taskEditObj.taskTypeID = this.taskfrm.value.type;
    this.taskEditObj.subTaskTypeID = this.taskfrm.value.subtype;
    this.taskEditObj.formId = this.taskfrm.value.form;
    this.taskEditObj.isStartOrEntTask = this.taskfrm.value.lasttask;

    console.log(this.taskEditObj);

    /*  this.http.post(this.Core_URL + '/saveTask/', this.taskEditObj)
     .subscribe(data => {         
       this.get_Data = data;
       this.taskId = this.get_Data.taskID;
       this.privilegeId = this.get_Data.privilegeId;
       if (this.taskId != null) {           
       alert("Updated successfully !! Please add translations");          
       this.gridTaskColumns = null;
       this.getTaskColoumnList();
       } else {
       alert("data not saved")
       }
     }); */
  }
  else {
    console.log(taskValue);

    let twoWayBindingObj: any = {};

    /* Reading First Two block for 2 way tasks */
    twoWayBindingObj.logicalOperators = "";
    twoWayBindingObj.expression1 = taskValue.firstDataElement;
    twoWayBindingObj.relationalOperators = taskValue.relationalOpraters;
    twoWayBindingObj.expression2 = taskValue.secondDataElement;

    task2WayArray.push(twoWayBindingObj)

    /* Reading values from repeating block */
    taskValue.itemRows.forEach(element => {
    let twoWayBindingObjInner: any = {};
    twoWayBindingObjInner.logicalOperators = element.relationalConditionRow;
    twoWayBindingObjInner.expression1 = element.firstDataElementRow;
    twoWayBindingObjInner.relationalOperators = element.relationalOpratersRow;
    twoWayBindingObjInner.expression2 = element.secondDataElementRow;

    task2WayArray.push(twoWayBindingObjInner)

    });


    console.log(task2WayArray);


    this.taskObj["taskName"] = this.taskfrm.value.tasktitle;
    this.taskObj["taskDescription"] = this.taskfrm.value.taskdes;
    this.taskObj["previlege"] = this.taskfrm.value.previlege;
    this.taskObj["privilegeName"] = this.taskfrm.value.previlegename;
    this.taskObj["privilegeDescription"] = this.taskfrm.value.previlegedes;
    this.taskObj["processID"] = this.taskfrm.value.process;
    this.taskObj["previousTaskID"] = this.taskfrm.value.previoustask;
    this.taskObj["taskTypeID"] = this.taskfrm.value.type;
    this.taskObj["subTaskTypeID"] = this.taskfrm.value.subtype;
    this.taskObj["formId"] = this.taskfrm.value.form;
    this.taskObj["isStartOrEntTask"] = this.taskfrm.value.lasttask;
    this.taskObj["twoWayRepeating"] = task2WayArray;
    this.taskObj["taskTrue"] = taskValue.taskTrue;
    this.taskObj["taskFalse"] = taskValue.taskFalse;
    this.taskObj["templateName"] = taskValue.templateName;
    console.log(this.taskObj);

    this.http.post(this.Core_URL + '/saveTask/', this.taskObj)
    .subscribe(data => {
      this.get_Data = data;
      // this.newService.createdSplitterTask(data);
      
      this.taskId = this.get_Data.taskID;
      this.privilegeId = this.get_Data.privilegeId;
      if (this.taskId != null) {
      this.clearShareObj();
      alert("Updated successfully !! Please add translations");
      this.getAllTaskByProcessId(this.processId);
      this.gridTaskColumns = null;
      this.getTaskColoumnList();
      } else {
      alert("data not saved");
      }
    });
  }
  }
  private clearShareObj() {
  this.shareServices.setgridChangeMessage(null);
  }

  public translationColumns() {
  if (this.taskId != null) {
    let coloumNames: any;
    let myColoumsObj: any;
    this.http.get(this.Core_URL + '/getCoreGridHeaderLanguageByOrgId')
    .subscribe(data => {
      myColoumsObj = data;
      coloumNames = myColoumsObj.coloumnName;
      if (Object.keys(coloumNames).length > 0) {
      this.gridTranslationColumns = coloumNames;
      }
      this.isDataLoaded = true;
      this.taskTransUrl = this.Core_URL + '/getTaskLanguageInfo/' + this.taskId + '/' + this.privilegeId;
    });
  } else {
    alert("please add task");
  }
  }

  public getTranslationEditObject(event: any) {
  let translationEditObj = {};
  translationEditObj["id"] = event.obj.id;
  translationEditObj["versionId"] = event.obj.versionId;
  translationEditObj["languageId"] = event.languageId;
  translationEditObj["orgId"] = event.obj.id;
  translationEditObj["columnName"] = event.obj.field;
  translationEditObj["columnValue"] = event.newEditValue;
  if (event.obj.field === 'Privilege' || event.obj.field === 'Privilege Description') {
    this.http.post(this.Core_URL + '/updatePrivilegeLanguage', translationEditObj)
    .subscribe(data => {
    });
  } else {

    this.http.post(this.Core_URL + '/updateTaskLanguage', translationEditObj)
    .subscribe(data => {
    });
  }
  }

  public closePopUp() {
  this.isDataLoaded = false;
  }

  public getTaskColoumnList() {
  this.shareServices.getCoreInnerTaskColumn()
    .subscribe(data => {
    this.gridTaskColumns = data.coloumnName;
    this.recordsTotal = data.recordsTotal;
    
    });
  }

  public mainbreadcrumb: string;
  public breadcrumb: string;
  public menuList: any = [];
  getHeaderMenuList() {
  this.shareServices.getHeaderMenu()
    .subscribe(res => {
    //console.log(res);
    this.menuList = res;

    this.menuList.forEach(element => {
      if (element.routerLink == "eta-web/home") {
      this.mainbreadcrumb = element.translation;
      }
      if (element.routerLink == "eta-web/taskdashboard") {
      this.breadcrumb = element.translation;
      }
    })

    })
  }

  public getAllDataElement() {
  this.http.get(this.Core_URL + '/getAllDataElementByOrgId')
    .subscribe(data => {
    this.dataElementList = data;
    });
  this.http.get(this.Core_URL + '/getAllSpdOperators')
    .subscribe(data => {
    this.relationalOpraters = data;
    });
  }

  selectedPreviousTask(taskObj) {
  console.log(taskObj);

  this.twoWayTaskCondition = taskObj.subTaskTypeID;
  }

  getEmailTemplateNameInfo() {
  this.shareServices.getAllTemplateByOrgId()
    .subscribe(res => {
    console.log(res);
    this.templateNameList = res;
    console.log(this.templateNameList);
     

    })
  }


}
