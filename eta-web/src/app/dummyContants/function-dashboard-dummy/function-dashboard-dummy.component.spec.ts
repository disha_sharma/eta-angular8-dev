import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FunctionDashboardDummyComponent } from './function-dashboard-dummy.component';

describe('FunctionDashboardDummyComponent', () => {
  let component: FunctionDashboardDummyComponent;
  let fixture: ComponentFixture<FunctionDashboardDummyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FunctionDashboardDummyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FunctionDashboardDummyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
