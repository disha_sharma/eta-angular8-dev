import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormArray, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-checkbox-group',
  templateUrl: './checkbox-group.component.html',
  styleUrls: ['./checkbox-group.component.css']
})
export class CheckboxGroupComponent implements OnInit {

  interestFormGroup : FormGroup;
  interests:any = ["option1", "option2", "option3", "option4", "option5"];
  selected: any;
  checkboxErrorApplied: boolean;

  constructor(private formBuilder: FormBuilder,
  ) { }

  ngOnInit() {

    this.interestFormGroup = this.formBuilder.group({
      interests: this.formBuilder.array([]),
    });


  

  }

  onChange(event) {
    const interests = <FormArray>this.interestFormGroup.get('interests') as FormArray;

    if(event.checked) {
      interests.push(new FormControl(event.source.value))
    } else {
      const i = interests.controls.findIndex(x => x.value === event.source.value);
      interests.removeAt(i);
    }
  }

  check() {
    if(this.interestFormGroup.value.interests.length == 0){
this.checkboxErrorApplied = true;
return false;
    }
    else {
      this.checkboxErrorApplied = true;
       console.log(this.interestFormGroup.value.interests);
     console.log(this.selected);
      return true;
      
    }
   
  }

}
