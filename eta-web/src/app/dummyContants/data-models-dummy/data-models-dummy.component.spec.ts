import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DataModelsDummyComponent } from './data-models-dummy.component';

describe('DataModelsDummyComponent', () => {
  let component: DataModelsDummyComponent;
  let fixture: ComponentFixture<DataModelsDummyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DataModelsDummyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DataModelsDummyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
