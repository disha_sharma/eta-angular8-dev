import { Component, OnInit, ViewChild } from '@angular/core';
import {MatPaginator, MatTableDataSource, MatSort} from '@angular/material';
import { animate, state, style, transition, trigger } from '@angular/animations';


@Component({
  selector: 'app-data-models-dummy',
  templateUrl: './data-models-dummy.component.html',
  styleUrls: ['./data-models-dummy.component.css'],
   animations: [
     trigger('detailExpand', [
       state('void', style({ height: '0px', minHeight: '0', visibility: 'hidden' })),
     state('*', style({ height: '*', visibility: 'visible' })),
     transition('void <=> *', animate('0ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
   ]
})
export class DataModelsDummyComponent implements OnInit {
  //obj =    {details: [{heading: 'Sap_Script_Details', items: [{itemHeading: 'ItemHeading', status: 'hidden'}, {itemHeading: 'ItemHeading', status: 'hidden'}]}]}
  displayedColumns: string[] = ['checkbox', 'container', 'type', 'objects', 'elements', 'status'];
 // displayedColumns: string[] = ['checkbox', 'container', 'type', 'objects', 'elements', 'Status'];
  tableData =  [
    {checkbox: 1, container: 'GFP Application Data', type: 'Configurable', objects: '1', elements: '14', status: 'green',  details: [{heading: 'Sap_Script_Details', items: [{itemHeading: 'ItemHeading', status: 'show'}, {itemHeading: 'ItemHeading', status: 'hidden'}]}, {heading: 'Sap_Script_Details', items: [{itemHeading: 'ItemHeading', status: 'show'}, {itemHeading: 'ItemHeading', status: 'hidden'}]}]},
    {checkbox: 2, container: 'GFP Admin Review Data', type: 'System', objects: '2', elements: '5', status: 'red',  details: [{heading: 'Sap_Script_Details', items: [{itemHeading: 'ItemHeading', status: 'show'}, {itemHeading: 'ItemHeading', status: 'hidden'}]}, {heading: 'Sap_Script_Details', items: [{itemHeading: 'ItemHeading', status: 'show'}, {itemHeading: 'ItemHeading', status: 'hidden'}]}]},
    {checkbox: 3, container: 'GFP Managor Review Data', type: 'Reused', objects: '1', elements: '1', status: 'green',  details: [{heading: 'Sap_Script_Details', items: [{itemHeading: 'ItemHeading', status: 'show'}, {itemHeading: 'ItemHeading', status: 'hidden'}]}]},
    {checkbox: 4, container: 'GFP Location Data', type: 'Resuable', objects: '2', elements: '2', status: 'amber',  details: [{heading: 'Sap_Script_Details', items: [{itemHeading: 'ItemHeading', status: 'hidden'}, {itemHeading: 'ItemHeading', status: 'hidden'}]}]},
    {checkbox: 5, container: 'GFP Resubmission Data', type: 'Shared', objects: '1', elements: '1', status: 'green',  details: [{heading: 'Sap_Script_Details', items: [{itemHeading: 'ItemHeading', status: 'hidden'}, {itemHeading: 'ItemHeading', status: 'hidden'}]}]},
    {checkbox: 6, container: 'GFP Admin Data', type: 'Shared', objects: '1', elements: '12', status: 'green', details: [{heading: 'Sap_Script_Details', items: [{itemHeading: 'ItemHeading', status: 'hidden'}, {itemHeading: 'ItemHeading', status: 'hidden'}]}]   },
    {checkbox: 7, container: 'GFP Location Data', type: 'Shared', objects: '2', elements: '1', status: 'green',  details: [{heading: 'Sap_Script_Details', items: [{itemHeading: 'ItemHeading', status: 'hidden'}, {itemHeading: 'ItemHeading', status: 'hidden'}]}]  },
    {checkbox: 8, container: 'GFP Admin Data', type: 'Shared', objects: '1', elements: '45', status: 'green',   details: [{heading: 'Sap_Script_Details', items: [{itemHeading: 'ItemHeading', status: 'hidden'}, {itemHeading: 'ItemHeading', status: 'hidden'}]}] },
    {checkbox: 9, container: 'GFP Application Data', type: 'Shared', objects: '1', elements: '14', status: 'green',   details: [{heading: 'Sap_Script_Details', items: [{itemHeading: 'ItemHeading', status: 'hidden'}, {itemHeading: 'ItemHeading', status: 'hidden'}]}] },
    {checkbox: 10, container: 'GFP Location Data', type: 'Shared', objects: '1', elements: '4', status: 'green',   details: [{heading: 'Sap_Script_Details', items: [{itemHeading: 'ItemHeading', status: 'hidden'}, {itemHeading: 'ItemHeading', status: 'hidden'}]}] },
    {checkbox: 11, container: 'GFP Application Data', type: 'Shared', objects: '1', elements: '4', status: 'green',   details: [{heading: 'Sap_Script_Details', items: [{itemHeading: 'ItemHeading', status: 'hidden'}, {itemHeading: 'ItemHeading', status: 'hidden'}]}] },
    {checkbox: 12, container: 'GFP Application Data', type: 'Shared', objects: '2', elements: '1', status: 'green',   details: [{heading: 'Sap_Script_Details', items: [{itemHeading: 'ItemHeading', status: 'hidden'}, {itemHeading: 'ItemHeading', status: 'hidden'}]}] },
    {checkbox: 13, container: 'GFP Admin Data', type: 'Shared', objects: '1', elements: '2', status: 'green',   details: [{heading: 'Sap_Script_Details', items: [{itemHeading: 'ItemHeading', status: 'hidden'}, {itemHeading: 'ItemHeading', status: 'hidden'}]}] },
    {checkbox: 14, container: 'GFP Location Data', type: 'Shared', objects: '2', elements: '2', status: 'green',   details: [{heading: 'Sap_Script_Details', items: [{itemHeading: 'ItemHeading', status: 'hidden'}, {itemHeading: 'ItemHeading', status: 'hidden'}]}] },
    {checkbox: 15, container: 'GFP Application Data', type: 'Shared', objects: '1', elements: '4', status: 'green',   details: [{heading: 'Sap_Script_Details', items: [{itemHeading: 'ItemHeading', status: 'hidden'}, {itemHeading: 'ItemHeading', status: 'hidden'}]}] },
    {checkbox: 16, container: 'GFP Application Data', type: 'Shared', objects: '2', elements: '1', status: 'green',   details: [{heading: 'Sap_Script_Details', items: [{itemHeading: 'ItemHeading', status: 'hidden'}, {itemHeading: 'ItemHeading', status: 'hidden'}]}] },
    {checkbox: 17, container: 'GFP Application Data', type: 'Shared', objects: '1', elements: '4', status: 'green',   details: [{heading: 'Sap_Script_Details', items: [{itemHeading: 'ItemHeading', status: 'hidden'}, {itemHeading: 'ItemHeading', status: 'hidden'}]}] },
    {checkbox: 18, container: 'GFP Application Data', type: 'Shared', objects: 'A2r', elements: '7', status: 'green',  details: [{heading: 'Sap_Script_Details', items: [{itemHeading: 'ItemHeading', status: 'hidden'}, {itemHeading: 'ItemHeading', status: 'hidden'}]}]  },
    {checkbox: 19, container: 'GFP Application Data', type: 'Shared', objects: '1', elements: '7', status: 'green',  details: [{heading: 'Sap_Script_Details', items: [{itemHeading: 'ItemHeading', status: 'hidden'}, {itemHeading: 'ItemHeading', status: 'hidden'}]}]  },
    {checkbox: 20, container: 'GFP Application Data', type: 'Shared', objects: '2', elements: '7', status: 'green',   details: [{heading: 'Sap_Script_Details', items: [{itemHeading: 'ItemHeading', status: 'hidden'}, {itemHeading: 'ItemHeading', status: 'hidden'}]}]  },
  ];

  dataSource = new MatTableDataSource(this.tableData);

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  isExpansionDetailRow = (index, row) => row.hasOwnProperty('detailRow');

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }
  stopPropagationFunction(event){
    event.stopPropagation();
    console.log('stop propagatin');
  }
  markActiveButton(event){
document.getElementsByClassName('active-button')[0].classList.remove('active-button');
event.target.classList.add('active-button');
  }
}



