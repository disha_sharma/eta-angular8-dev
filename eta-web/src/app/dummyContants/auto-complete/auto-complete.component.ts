import { Component, OnInit } from '@angular/core';
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs/Observable';
import {startWith} from 'rxjs/operators/startWith';
import {map} from 'rxjs/operators/map';
import {filter} from 'rxjs/operators/filter';
import {Validators} from '@angular/forms';
@Component({
  selector: 'app-auto-complete',
  templateUrl: './auto-complete.component.html',
  styleUrls: ['./auto-complete.component.css']
})
export class AutoCompleteComponent implements OnInit {
  
  constructor() { }
  myControl: FormControl = new FormControl(); //for auto complete
  inputName = new FormControl('', [Validators.required]); //for validations
   
  getErrorMessage() {
     return this.inputName.hasError('required') ? 'You must enter a value' :
            '';
   }

   options = [];
   optionInKeyValue = [
     {displayOrder: 'Shyam'},
     {displayOrder: 'Shyam joshi'},
     {displayOrder: 'Abhishek Gupta'},
     {displayOrder: 'Ravi Thapa'}
   ]
   //pushing value to options array from key value pair objects 
   pushValueToOptionArray () {
     for(var i = 0; i < this.optionInKeyValue.length; i++) {
       this.options.push(this.optionInKeyValue[i].displayOrder);
     }
   }

  

  filteredOptions: Observable<string[]>;

  ngOnInit() {
    this.filteredOptions = this.myControl.valueChanges
      .pipe(
        startWith(''),
        map(val => val.length >= 1 ? this.filter(val): [])
      );
      this.pushValueToOptionArray ();
  }

  filter(val: string): string[] {
    return this.options.filter(option =>
      option.toLowerCase().indexOf(val.toLowerCase()) === 0);
  }

}
