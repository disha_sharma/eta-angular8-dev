import { Component, OnInit } from '@angular/core';
//for auto complete
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs/Observable';
import {startWith} from 'rxjs/operators/startWith';
import {map} from 'rxjs/operators/map';
import {filter} from 'rxjs/operators/filter';
import {Validators} from '@angular/forms';
@Component({
  selector: 'app-form-list',
  templateUrl: './form-list.component.html',
  styleUrls: ['./form-list.component.css']
})
export class FormListComponent implements OnInit {
  inputNgModel;
  custom: boolean = true;
  inputName = new FormControl('', [Validators.required, Validators.email]);
  
   getErrorMessage() {
     return this.inputName.hasError('required') ? 'You must enter a value' :
        this.inputName.hasError('email') ? 'Not a valid email' :
            '';
   }

  myControl: FormControl = new FormControl();
  options = [];
  optionInKeyValue = [
    {displayOrder: 'Shyam'},
    {displayOrder: 'Shyam joshi'},
    {displayOrder: 'Abhishek Gupta'},
    {displayOrder: 'Ravi Thapa'}
  ]
  pushValueToOptionArray () {
    for(var i = 0; i < this.optionInKeyValue.length; i++) {
      this.options.push(this.optionInKeyValue[i].displayOrder);
    }
  }

  filteredOptions: Observable<string[]>;

  ngOnInit() {
    this.filteredOptions = this.myControl.valueChanges
      .pipe(
        startWith(''),
        map(val => val.length >= 1 ? this.filter(val): [])
      );
      this.pushValueToOptionArray ();
  }

  filter(val: string): string[] {
    return this.options.filter(option =>
      option.toLowerCase().indexOf(val.toLowerCase()) === 0);
  }


  
   animalControl = new FormControl('', [Validators.required]);
   selectFormControl = new FormControl('', Validators.required);
  animals = [
    {name: 'Dog', sound: 'Woof!'},
    {name: 'Cat', sound: 'Meow!'},
    {name: 'Cow', sound: 'Moo!'},
    {name: 'Fox', sound: 'Wa-pa-pa-pa-pa-pa-pow!'},
  ];

  cars : any = [
    {value: 'volvo', viewValue: 'Volvo'},
    {value: 'saab', viewValue: 'Saab'},
    {value: 'mercedes', viewValue: 'Mercedes'}
  ];
}



