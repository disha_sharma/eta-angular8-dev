import { Component, OnInit, ViewChild } from '@angular/core';
import {MatPaginator, MatTableDataSource, MatSort} from '@angular/material';
import { animate, state, style, transition, trigger } from '@angular/animations';


@Component({
  selector: 'app-forms',
  templateUrl: './forms.component.html',
  styleUrls: ['./forms.component.css']
})
export class FormsComponent implements OnInit {

 
  displayedColumns: string[] = ['checkbox', 'form', 'type', 'field', 'status', 'since'];

  tableData =  [
    {checkbox: 1, form: 'GFP Application Data', type: 'Configurable', field: '1', status: 'green', since: 'May 1, 2018'  },
    {checkbox: 1, form: 'GFP Application Data', type: 'Configurable', field: '1', status: 'green', since: 'May 1, 2018'  },
    {checkbox: 1, form: 'GFP Application Data', type: 'Configurable', field: '1', status: 'green', since: 'May 1, 2018'  },
    {checkbox: 1, form: 'GFP Application Data', type: 'Configurable', field: '1', status: 'green', since: 'May 1, 2018'  },
    {checkbox: 1, form: 'GFP Application Data', type: 'Configurable', field: '1', status: 'green', since: 'May 1, 2018'  },
    {checkbox: 1, form: 'GFP Application Data', type: 'Configurable', field: '1', status: 'green', since: 'May 1, 2018'  },
    {checkbox: 1, form: 'GFP Application Data', type: 'Configurable', field: '1', status: 'green', since: 'May 1, 2018'  },
    {checkbox: 1, form: 'GFP Application Data', type: 'Configurable', field: '1', status: 'green', since: 'May 1, 2018'  },
    {checkbox: 1, form: 'GFP Application Data', type: 'Configurable', field: '1', status: 'green', since: 'May 1, 2018'  },
    {checkbox: 1, form: 'GFP Application Data', type: 'Configurable', field: '1', status: 'green', since: 'May 1, 2018'  },
    {checkbox: 1, form: 'GFP Application Data', type: 'Configurable', field: '1', status: 'green', since: 'May 1, 2018'  },
    {checkbox: 1, form: 'GFP Application Data', type: 'Configurable', field: '1', status: 'green', since: 'May 1, 2018'  },
    {checkbox: 1, form: 'GFP Application Data', type: 'Configurable', field: '1', status: 'green', since: 'May 1, 2018'  },
    {checkbox: 1, form: 'GFP Application Data', type: 'Configurable', field: '1', status: 'green', since: 'May 1, 2018'  },
    {checkbox: 1, form: 'GFP Application Data', type: 'Configurable', field: '1', status: 'green', since: 'May 1, 2018'  },
    {checkbox: 1, form: 'GFP Application Data', type: 'Configurable', field: '1', status: 'green', since: 'May 1, 2018'  },
   ];

  dataSource = new MatTableDataSource(this.tableData);

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  isExpansionDetailRow = (index, row) => row.hasOwnProperty('detailRow');

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }
  stopPropagationFunction(event){
    event.stopPropagation();
    console.log('stop propagatin');
  }
  markActiveButton(event){
document.getElementsByClassName('active-button')[0].classList.remove('active-button');
event.target.classList.add('active-button');
  }
}