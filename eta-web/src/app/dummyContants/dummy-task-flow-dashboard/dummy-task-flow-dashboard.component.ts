import { Component, OnInit } from '@angular/core';
import {   ViewChild, ElementRef  } from '@angular/core';
import {MatPaginator, MatTableDataSource, MatSort} from '@angular/material';
import { animate, state, style, transition, trigger } from '@angular/animations';
import {HttpClient} from '@angular/common/http';

import {merge, Observable, of as observableOf} from 'rxjs';
import {catchError, map, startWith, switchMap} from 'rxjs/operators';
@Component({
  selector: 'app-dummy-task-flow-dashboard',
  templateUrl: './dummy-task-flow-dashboard.component.html',
  styleUrls: ['./dummy-task-flow-dashboard.component.css'],
  
})
export class DummyTaskFlowDashboardComponent implements OnInit {

ngOnInit(){}


  @ViewChild('widgetsContent', { read: ElementRef, static: true }) public widgetsContent: ElementRef<any>; //for custom slider

cards = [
  {heading: 'Heading', discription: 'discription'},
  {heading: 'Heading', discription: 'discription'},
  {heading: 'Heading', discription: 'discription'},
  {heading: 'Heading', discription: 'discription'},
  {heading: 'Heading', discription: 'discription'},
  {heading: 'Heading', discription: 'discription'},
  {heading: 'Heading', discription: 'discription'},
]

prev(){

  this.widgetsContent.nativeElement.scrollTo({ left: (this.widgetsContent.nativeElement.scrollLeft -300), behavior: 'smooth' });
  
  }
  
  next(){
  
  this.widgetsContent.nativeElement.scrollTo({ left: (this.widgetsContent.nativeElement.scrollLeft + 300), behavior: 'smooth' });
  
  }


   
  }
  