import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DummyTaskFlowDashboardComponent } from './dummy-task-flow-dashboard.component';

describe('DummyTaskFlowDashboardComponent', () => {
  let component: DummyTaskFlowDashboardComponent;
  let fixture: ComponentFixture<DummyTaskFlowDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DummyTaskFlowDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DummyTaskFlowDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
