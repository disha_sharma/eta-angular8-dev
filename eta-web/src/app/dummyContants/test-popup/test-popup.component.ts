import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { DummyPopupComponent } from '../../popup/dummy-popup/dummy-popup.component';
import { ScriptApprovalApplicationFormComponent } from '../../popup/script-approval-application-form/script-approval-application-form.component';
import { AddNewMasterDataGroupComponent } from '../dummy-popups/add-new-master-data-group/add-new-master-data-group.component';

@Component({
  selector: 'app-test-popup',
  templateUrl: './test-popup.component.html',
  styleUrls: ['./test-popup.component.css']
})
export class TestPopupComponent implements OnInit {

  ngOnInit() {

  }
   animal: string ;
   name: string ;

   constructor(public dialog: MatDialog) {}

  openDialog(): void {
     const dialogRef = this.dialog.open(ScriptApprovalApplicationFormComponent, {
       width: '1000px',
       data: {name: this.name, animal: this.animal}
     });

     dialogRef.afterClosed().subscribe(result => {
       console.log('The dialog was closed');
       this.animal = result;
     });
   }

}
