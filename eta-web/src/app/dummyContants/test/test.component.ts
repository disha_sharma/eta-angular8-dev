import { Component, OnInit, ViewChild } from '@angular/core';
import {FormControl, Validators} from '@angular/forms'; 
import {MatDatepickerInputEvent} from '@angular/material/datepicker';
import { MatIconRegistry } from "@angular/material/icon";
import { DomSanitizer } from "@angular/platform-browser";
@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css']
})
export class TestComponent implements OnInit {
  dummyInput = new FormControl('', [Validators.required]); // for custom input field error
   getErrorMessage() {
    return this.dummyInput.hasError('required') ? 'You must enter a value' :
       '';
  }


  constructor(
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer
  ){
    this.matIconRegistry.addSvgIcon(
      "Add_Square",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../assets/svg-icons/Add_Square.svg")
    );
  }

  ngOnInit() {
  }
 
  //animalControl = new FormControl('', [Validators.required]);
  selectFormControl = new FormControl('', [Validators.required]);
  animals = [
    {name: 'Dog', sound: 'Woof!'},
    {name: 'Cat', sound: 'Meow!'},
    {name: 'Cow', sound: 'Moo!'},
  
  ];
 
dateError:boolean = false;
addEvent(type: string, event: MatDatepickerInputEvent<Date>) {
    let datePicker = event.value;
   let today = new Date();

 if(datePicker.getTime() < today.getTime()){
   this.dateError = false;
 }
 else{
   this.dateError = true;
 }  
  }
}
