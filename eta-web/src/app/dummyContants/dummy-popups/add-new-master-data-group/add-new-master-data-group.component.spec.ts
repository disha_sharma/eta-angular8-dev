import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddNewMasterDataGroupComponent } from './add-new-master-data-group.component';

describe('AddNewMasterDataGroupComponent', () => {
  let component: AddNewMasterDataGroupComponent;
  let fixture: ComponentFixture<AddNewMasterDataGroupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddNewMasterDataGroupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddNewMasterDataGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
