import { Component, OnInit } from '@angular/core';

import {FormControl} from '@angular/forms';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ArrayType } from '@angular/compiler/src/output/output_ast';
import {  Inject } from '@angular/core';//for popup
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';//for popup
/* scope mutli select code first part starts here */
import {SelectionModel} from '@angular/cdk/collections';
import {FlatTreeControl} from '@angular/cdk/tree';
import { Injectable} from '@angular/core';
import {MatTreeFlatDataSource, MatTreeFlattener} from '@angular/material/tree';
import {BehaviorSubject} from 'rxjs';

/**
 * Node for to-do item
 */
 export class TodoItemNode {
  children: TodoItemNode[];
   item: string;
 }

/** Flat to-do item node with expandable and level information */
export class TodoItemFlatNode {
  item: string;
  level: number;
  expandable: boolean;
}

/**
 * The Json object for to-do list data.
 */
const TREE_DATA = {
  Groceries: {
    'Almond Meal flour': null,
    'Organic eggs': null,
    'Protein Powder': null,
    Fruits: {
      Apple: null,
      Berries: ['Blueberry', 'Raspberry'],
      Orange: null
    }
  },
  Reminders: [
    'Cook dinner',
    'Read the Material Design spec',
    'Upgrade Application to Angular'
  ]
};

/**
 * Checklist database, it can build a tree structured Json object.
 * Each node in Json object represents a to-do item or a category.
 * If a node is a category, it has children items and new items can be added under the category.
 */
@Injectable()
export class ChecklistDatabase {
  dataChange = new BehaviorSubject<TodoItemNode[]>([]);

  get data(): TodoItemNode[] { return this.dataChange.value; }

  constructor() {
    this.initialize();
  }

  initialize() {
    // Build the tree nodes from Json object. The result is a list of `TodoItemNode` with nested
    //     file node as children.
    const data = this.buildFileTree(TREE_DATA, 0);

    // Notify the change.
    this.dataChange.next(data);
  }

  /**
   * Build the file structure tree. The `value` is the Json object, or a sub-tree of a Json object.
   * The return value is the list of `TodoItemNode`.
   */
  buildFileTree(obj: object, level: number): TodoItemNode[] {
    return Object.keys(obj).reduce<TodoItemNode[]>((accumulator, key) => {
      const value = obj[key];
      const node = new TodoItemNode();
      node.item = key;

      if (value != null) {
        if (typeof value === 'object') {
          node.children = this.buildFileTree(value, level + 1);
        } else {
          node.item = value;
        }
      }

      return accumulator.concat(node);
    }, []);
  }

  /** Add an item to to-do list */
  insertItem(parent: TodoItemNode, name: string) {
    if (parent.children) {
      parent.children.push({item: name} as TodoItemNode);
      this.dataChange.next(this.data);
    }
  }

  updateItem(node: TodoItemNode, name: string) {
    node.item = name;
    this.dataChange.next(this.data);
  }
}

/**
 * @title Tree with checkboxes
 */
/* scope mutli select code first part ends here */
@Component({
  selector: 'app-add-new-master-data-group',
  templateUrl: './add-new-master-data-group.component.html',
  styleUrls: ['./add-new-master-data-group.component.css'],
  providers: [ChecklistDatabase]
})
export class AddNewMasterDataGroupComponent implements OnInit {
  deleteButtonFlag: boolean = false;
  checkAllItem: boolean = false;
  nameField = new FormControl('', [Validators.required]); // for custom input field error
  getErrorMessage() {
   return this.nameField.hasError('required') ? 'You must enter a value' :
      '';
 }

 constructor(public dialogRef: MatDialogRef<AddNewMasterDataGroupComponent>,
  @Inject(MAT_DIALOG_DATA) public data: any, private fb: FormBuilder, private database: ChecklistDatabase) {
  this.treeFlattener = new MatTreeFlattener(this.transformer, this.getLevel,
    this.isExpandable, this.getChildren);
  this.treeControl = new FlatTreeControl<TodoItemFlatNode>(this.getLevel, this.isExpandable);
  this.dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);

  database.dataChange.subscribe(data => {
    this.dataSource.data = data;
  });
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
form: FormGroup;
ngOnInit() {

    this.expandNodesInitially(); //for scope tree

  this.form = this.fb.group({
    'fileInput': [''],
    'fileInputLabel': [''],
    'item': this.fb.array([
      this.inititem()
    ])
  });


}

inititem() {
  return this.fb.group({
   'itemCheckedStatus': [''],
   'itemName': ['', [Validators.required]],

  });
}




//add data object
additem() {
  const control = <FormArray>this.form.controls['item'];
  control.push(this.inititem());
  this.toggleCheckAllButton()
}

//delete data object
deleteitem(){
  /* const control = <FormArray>this.form.controls['item'];
  control.removeAt(index);*/
  let count = 0;
 var i = this.form.controls['item'].value.length;
  while (i--) {
     if (this.form.controls['item'].value[i].itemCheckedStatus == true) {
      const control = <FormArray>this.form.controls['item'];
      control.removeAt(i);
      count++;
      }
  }
 this.deleteButtonFlag = false;
  this.changeStatusOfCheckAllCheckbox('uncheck');
  if(count == 0){
    alert('not checked');
  }
}

/* uncheck checkAll check box */
changeStatusOfCheckAllCheckbox(action){
  if(action == 'check'){
    this.checkAllItem = true;
  }
  else if(action == 'uncheck')
  this.checkAllItem = false;
}

/* show delete button when any field is checked */
toggleDeleteButton(){
  let length = this.form.controls['item'].value.length;
  let control = <FormArray>this.form.controls['item'];
   for(let i = 0; i < length; i++){
     console.log(control.controls[i].get('itemCheckedStatus').value)
    if( control.controls[i].get('itemCheckedStatus').value){
     this.deleteButtonFlag = true;
     return;
   }
   else{
     this.deleteButtonFlag = false;
   }
   }
}

/* toggle checkAllButton */
toggleCheckAllButton(){
  let length = this.form.controls['item'].value.length;
  let control = <FormArray>this.form.controls['item'];
   for(let i = 0; i < length; i++){
   
    if( !control.controls[i].get('itemCheckedStatus').value){
this.changeStatusOfCheckAllCheckbox('uncheck');
return
    }
    else{
      this.changeStatusOfCheckAllCheckbox('check');
     }
  }
}
/*check all item nodes by checking top check box */
checkAllItems(event) {
  if(event.checked){
 let length = this.form.controls['item'].value.length;
 let control = <FormArray>this.form.controls['item'];
  for(let i = 0; i < length; i++){

   control.controls[i].get('itemCheckedStatus').setValue(true);
  }
 }
 else if(!event.checked){
   let length = this.form.controls['item'].value.length;
   let control = <FormArray>this.form.controls['item'];
   for(let i = 0; i < length; i++){

     control.controls[i].get('itemCheckedStatus').setValue(false);
    }
 }
 }

/*file upload code starts here */
fileInput: string = "";
  hide: boolean = false;
  fileNotSupported: boolean;
  newInput: string;
  validatedStatus: boolean;
  uploadButtonEnabled: boolean = false;
  progressBarValue = 0;

  validateAndNotSupportedControls: boolean; // for showing validation and not supported buttons on screen.


  //function for upload file
  uploadFile() {
    if(this.validatedStatus){
      return true;
    }
    else{
    this.validateAndNotSupportedControls = true;
    this.progressBarValue = 50;
    }
  }
//function for successfully Validate
  validate() {
    this.validatedStatus = true;
    this.progressBarValue = 100;
    this.uploadButtonEnabled = true;
    this.validateAndNotSupportedControls = false;
  }
  // function for make the file not supported
  notSupported(){
    this.progressBarValue = 0;
    this.fileNotSupported = true;
    this.validatedStatus = false;
    this.uploadButtonEnabled = false;
    this.validateAndNotSupportedControls = false;

  }
  inputChangeFunction(){

    this.fileNotSupported = false;
    this.validatedStatus = false;
    this.validateAndNotSupportedControls = false;
    this.progressBarValue = 0;
    if(this.fileInput) {
      this.uploadButtonEnabled = true;
    }
    else{
      this.uploadButtonEnabled = false;
    }


  }

/*file upload code ends here */
/* scope multi select second part starts here */
selected = 'option2';
selectedInfo;
selectedValue = 'Shyam';


/** Map from flat node to nested node. This helps us finding the nested node to be modified */
flatNodeMap = new Map<TodoItemFlatNode, TodoItemNode>();

/** Map from nested node to flattened node. This helps us to keep the same object for selection */
nestedNodeMap = new Map<TodoItemNode, TodoItemFlatNode>();

/** A selected parent node to be inserted */
selectedParent: TodoItemFlatNode | null = null;

/** The new item's name */
newItemName = '';

treeControl: FlatTreeControl<TodoItemFlatNode>;

treeFlattener: MatTreeFlattener<TodoItemNode, TodoItemFlatNode>;

dataSource: MatTreeFlatDataSource<TodoItemNode, TodoItemFlatNode>;

/** The selection for checklist */
checklistSelection = new SelectionModel<TodoItemFlatNode>(true /* multiple */);



getLevel = (node: TodoItemFlatNode) => node.level;

isExpandable = (node: TodoItemFlatNode) => node.expandable;

getChildren = (node: TodoItemNode): TodoItemNode[] => node.children;

hasChild = (_: number, _nodeData: TodoItemFlatNode) => _nodeData.expandable;

hasNoContent = (_: number, _nodeData: TodoItemFlatNode) => _nodeData.item === '';

/**
* Transformer to convert nested node to flat node. Record the nodes in maps for later use.
*/
transformer = (node: TodoItemNode, level: number) => {
const existingNode = this.nestedNodeMap.get(node);
const flatNode = existingNode && existingNode.item === node.item
    ? existingNode
    : new TodoItemFlatNode();
flatNode.item = node.item;
flatNode.level = level;
flatNode.expandable = !!node.children;
this.flatNodeMap.set(flatNode, node);
this.nestedNodeMap.set(node, flatNode);
return flatNode;
}
/* expand all node automatically on initialization. */
expandNodesInitially(){
for (let i = 0; i < this.treeControl.dataNodes.length; i++) {

    this.treeControl.expand(this.treeControl.dataNodes[i])
}
}

/** Whether all the descendants of the node are selected */
descendantsAllSelected(node: TodoItemFlatNode): boolean {
const descendants = this.treeControl.getDescendants(node);
return descendants.every(child => this.checklistSelection.isSelected(child));
}

/** Whether part of the descendants are selected */
descendantsPartiallySelected(node: TodoItemFlatNode): boolean {
const descendants = this.treeControl.getDescendants(node);
const result = descendants.some(child => this.checklistSelection.isSelected(child));
return result && !this.descendantsAllSelected(node);
}

/** Toggle the to-do item selection. Select/deselect all the descendants node */
todoItemSelectionToggle(node: TodoItemFlatNode): void {
 this.checklistSelection.toggle(node);
 const descendants = this.treeControl.getDescendants(node);
 this.checklistSelection.isSelected(node)
  ? this.checklistSelection.select(...descendants)
  : this.checklistSelection.deselect(...descendants);
}
stopPropegationFunction(event) {
event.stopPropagation();
}
/*custom function for get values.*/
checkNodes() {

this.selectedInfo = [];

const completeNodes = this.treeControl.dataNodes;

const descAllSelected = completeNodes.forEach(child => {

  if(this.checklistSelection.isSelected(child)) {

   // this.selectedInfo.push({name: child.item, status: true})
   this.selectedInfo.push(child.item);

  }

});

if(this.selectedInfo.length == 1){
this.selected = 'oneChecked';
}
else if(this.selectedInfo.length > 1){
this.selected = 'multipleChecked';
}
else if(this.selectedInfo.length == 0){
this.selected = '';
}
 console.log(this.selectedInfo);

}
}
