import { Component, OnInit } from '@angular/core';
import {FormControl} from '@angular/forms';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ArrayType } from '@angular/compiler/src/output/output_ast';

/* scope tree code first part starts here */
import {SelectionModel} from '@angular/cdk/collections';
import {FlatTreeControl} from '@angular/cdk/tree';
import { Injectable} from '@angular/core';
import {MatTreeFlatDataSource, MatTreeFlattener} from '@angular/material/tree';
import {BehaviorSubject} from 'rxjs';

/**
 * Node for to-do item
 */
 export class TodoItemNode {
  children: TodoItemNode[];
   item: string;
 }

/** Flat to-do item node with expandable and level information */
export class TodoItemFlatNode {
  item: string;
  level: number;
  expandable: boolean;
}

/**
 * The Json object for to-do list data.
 */
const TREE_DATA = {
  Main: {
    Permits: {
      Ground_Filming: null,
      Script_Approval: null,
      Script_Approval2: null,
      Aerial_Filming_permits: ['Aerial filming permit1', 'Aerial filming permit2'],
      
    },
    'Ground Filming': null,
    'Organic eggs': null,
    'Protein Powder': null
    
  },
  Reminders: [
    'Cook dinner',
    'Read the Material Design spec',
    'Upgrade Application to Angular'
  ]
};

/**
 * Checklist database, it can build a tree structured Json object.
 * Each node in Json object represents a to-do item or a category.
 * If a node is a category, it has children items and new items can be added under the category.
 */
@Injectable()
export class ChecklistDatabase {
  dataChange = new BehaviorSubject<TodoItemNode[]>([]);

  get data(): TodoItemNode[] { return this.dataChange.value; }

  constructor() {
    this.initialize();
  }

  initialize() {
    // Build the tree nodes from Json object. The result is a list of `TodoItemNode` with nested
    //     file node as children.
    const data = this.buildFileTree(TREE_DATA, 0);

    // Notify the change.
    this.dataChange.next(data);
  }

  /**
   * Build the file structure tree. The `value` is the Json object, or a sub-tree of a Json object.
   * The return value is the list of `TodoItemNode`.
   */
  buildFileTree(obj: object, level: number): TodoItemNode[] {
    return Object.keys(obj).reduce<TodoItemNode[]>((accumulator, key) => {
      const value = obj[key];
      const node = new TodoItemNode();
      node.item = key;

      if (value != null) {
        if (typeof value === 'object') {
          node.children = this.buildFileTree(value, level + 1);
        } else {
          node.item = value;
        }
      }

      return accumulator.concat(node);
    }, []);
  }

  /** Add an item to to-do list */
  insertItem(parent: TodoItemNode, name: string) {
    if (parent.children) {
      parent.children.push({item: name} as TodoItemNode);
      this.dataChange.next(this.data);
    }
  }

  updateItem(node: TodoItemNode, name: string) {
    node.item = name;
    this.dataChange.next(this.data);
  }
}

/**
 * @title Tree with checkboxes
 */


 /* scope tree code first part ends here */
@Component({
  selector: 'app-add-data-container',
  templateUrl: './add-data-container.component.html',
  styleUrls: ['./add-data-container.component.css'],
  providers: [ChecklistDatabase]
 
})
export class AddDataContainerComponent implements OnInit {

 resuable: boolean = false;
 shared: boolean = false;
 
  //code for field name
  name = new FormControl('', [Validators.required, Validators.maxLength(64)]); 
  getErrorMessage() {
    return this.name.hasError('required') ? 'You must enter a value' :
       '';
  }
 
/*data element value type field */
dataElementValueType: any =['type1','type2','type3','type4',];

form: FormGroup;
dataObjectdeleteButtonFlag: boolean = false;
dataObjectCheckAllItem : boolean = false;

dataElementdeleteButtonFlag: boolean = false;
dataElementCheckAllItem : boolean = false;
  ngOnInit() {
    //for scope tree
    this.expandNodesInitially();
    this.form = this.fb.group({
      'dataObject': this.fb.array([
        this.initDataObject()
      ])
    });
    //this.form.controls['dataObject'][0].dataObjectCheckedStatus.setValue(true);
    // this.form.controls['dataObject'][0].patchValue({
    //   dataObjectCheckedStatus: true
    // })
    
  }

  initDataObject() {
    return this.fb.group({
     'dataObjectCheckedStatus': [''],
     'dataObjectName': ['', Validators.required],
     'dataElements': this.fb.array([
        this.initDataElement()
      ])
    });
  }

  initDataElement() {
    return this.fb.group({
     
      'dataElementCheckedStatus': [''],
      'dataElementName': [''],
      'dataElementValueType': [''],
      
    })
  }

  
//add data object 
  addDataObject() {
    const control = <FormArray>this.form.controls['dataObject'];
    control.push(this.initDataObject());
    this.toggleCheckAllDataObjectButton()
  }
 
  //delete data object
  deleteDataObject(index){
    /* const control = <FormArray>this.form.controls['dataObject'];
    control.removeAt(index);*/
    let count = 0;
   var i = this.form.controls['dataObject'].value.length;
    while (i--) {
       if (this.form.controls['dataObject'].value[i].dataObjectCheckedStatus == true) { 
        const control = <FormArray>this.form.controls['dataObject'];
        control.removeAt(i);
        count++;
        } 
    }
    this.changeStatusOfCheckAllDataObjectButton('uncheck');
this.dataObjectdeleteButtonFlag = false;
    if(count == 0){
      alert('not checked')
    }
  
  }
/* uncheck checkAll check box */
changeStatusOfCheckAllDataObjectButton(action){
  if(action == 'check'){
    this.dataObjectCheckAllItem = true;
  }
  else if(action == 'uncheck')
  this.dataObjectCheckAllItem = false;
}

/* show delete button when any field is checked */
toggleDeleteDataObjectButton(){
  let length = this.form.controls['dataObject'].value.length;
  let control = <FormArray>this.form.controls['dataObject'];
   for(let i = 0; i < length; i++){
    if( control.controls[i].get('dataObjectCheckedStatus').value){
      this.dataObjectdeleteButtonFlag = true;
      return;
    }
    else{
      this.dataObjectdeleteButtonFlag = false;
    }
   }
}

/* toggle checkAllButton */
toggleCheckAllDataObjectButton(){
  let length = this.form.controls['dataObject'].value.length;
  let control = <FormArray>this.form.controls['dataObject'];
   for(let i = 0; i < length; i++){
   
    if( !control.controls[i].get('dataObjectCheckedStatus').value){
this.changeStatusOfCheckAllDataObjectButton('uncheck');
return
    }
    else{
      this.changeStatusOfCheckAllDataObjectButton('check');
     }
  }
}



//add data element 
  addDataElement(ix) {
    const control = (<FormArray>this.form.controls['dataObject']).at(ix).get('dataElements') as FormArray;
    control.push(this.initDataElement());
    this.toggleCheckAllDataElementButton(ix);
  }

  //delete data element
  deleteDataElement(ix,removeIndex) {
    let count = 0;
    var i = this.form.controls['dataObject'].value[ix].dataElements.length;
  while (i--) {
       if (this.form.controls['dataObject'].value[ix].dataElements[i].dataElementCheckedStatus) { 
        const control = (<FormArray>this.form.controls['dataObject']).at(ix).get('dataElements') as FormArray;
    control.removeAt(i);
    count++
        } 
    }
    this.changeStatusOfCheckAllDataElementButton('uncheck');
    this.dataElementdeleteButtonFlag = false;
    if(count == 0){
      alert('not checked')
    }
 
  }

  /* uncheck checkAll check box */
changeStatusOfCheckAllDataElementButton(action){
  if(action == 'check'){
    this.dataElementCheckAllItem = true;
  }
  else if(action == 'uncheck')
  this.dataElementCheckAllItem = false;
}

/* show delete button when any field is checked */
toggleDeleteDataElementButton(dataObjectIndex){
  let length = this.form.controls['dataObject'].value[dataObjectIndex].dataElements.length;
  let control = (<FormArray>this.form.controls['dataObject']).at(dataObjectIndex).get('dataElements') as FormArray;
   for(let i = 0; i < length; i++){
    
     if( control.controls[i].get('dataElementCheckedStatus').value){
      this.dataElementdeleteButtonFlag = true;
      return;
    }
    else{
      this.dataElementdeleteButtonFlag = false;
    }
   }
}

/* toggle checkAllButton */
toggleCheckAllDataElementButton(dataObjectIndex){
  let length = this.form.controls['dataObject'].value[dataObjectIndex].dataElements.length;
  let control = (<FormArray>this.form.controls['dataObject']).at(dataObjectIndex).get('dataElements') as FormArray;
   for(let i = 0; i < length; i++){
   
    if( !control.controls[i].get('dataElementCheckedStatus').value){
this.changeStatusOfCheckAllDataElementButton('uncheck');
return
    }
    else{
      this.changeStatusOfCheckAllDataElementButton('check');
     }
  }
}



  
 checkAllDataObject(event) {
 if(event.checked){
   
  let length = this.form.controls['dataObject'].value.length;
  let control = <FormArray>this.form.controls['dataObject'];
   for(let i = 0; i < length; i++){
    
    control.controls[i].get('dataObjectCheckedStatus').setValue(true);
   }
  }
  else if(!event.checked){
    let length = this.form.controls['dataObject'].value.length;
    let control = <FormArray>this.form.controls['dataObject'];
    for(let i = 0; i < length; i++){
    
      control.controls[i].get('dataObjectCheckedStatus').setValue(false);
     }
  }
  }
  checkAllDataElement(dataObjectIndex, event){
if(event.checked){
    let length = this.form.controls['dataObject'].value[dataObjectIndex].dataElements.length;
    const control = (<FormArray>this.form.controls['dataObject']).at(dataObjectIndex).get('dataElements');
    const secondControl = <FormArray>control
 
   for(let i = 0; i < length; i++){
    secondControl.controls[i].get('dataElementCheckedStatus').setValue(true);
   }
   
  }
  if(!event.checked){
    let length = this.form.controls['dataObject'].value[dataObjectIndex].dataElements.length;
    const control = (<FormArray>this.form.controls['dataObject']).at(dataObjectIndex).get('dataElements');
    const secondControl = <FormArray>control
 
   for(let i = 0; i < length; i++){
    secondControl.controls[i].get('dataElementCheckedStatus').setValue(false);
   }
   
  }
// for(let i = 0; i < length; i++){
  
// }
  /*  var i = this.form.controls['dataObject'].value[dataObjectIndex].dataElements.length;
  while (i--) {
       if (this.form.controls['dataObject'].value[ix].dataElements[i].dataElementCheckedStatus) { 
        const control = (<FormArray>this.form.controls['dataObject']).at(ix).get('dataElements') as FormArray;
    control.removeAt(i);
    count++
        } 
    }
    if(count == 0){
      alert('not checked')
    }*/

  }
  // const control = <FormArray>this.form.controls['dataObject'];
  // control.controls[0].get('dataObjectCheckedStatus').setValue(true);
    //const control = <FormArray>this.form.controls['item'];
 // console.log(control.controls[0].get('itemName'));
 //onsole.log(this.form.controls['item'].valid[0]);
 




  // constructor(private fb: FormBuilder) {

  // }
  
  selected = 'option2';
  selectedInfo;
  selectedValue = 'Shyam';

/** Map from flat node to nested node. This helps us finding the nested node to be modified */
flatNodeMap = new Map<TodoItemFlatNode, TodoItemNode>();

/** Map from nested node to flattened node. This helps us to keep the same object for selection */
nestedNodeMap = new Map<TodoItemNode, TodoItemFlatNode>();

/** A selected parent node to be inserted */
selectedParent: TodoItemFlatNode | null = null;

/** The new item's name */
newItemName = '';

treeControl: FlatTreeControl<TodoItemFlatNode>;

treeFlattener: MatTreeFlattener<TodoItemNode, TodoItemFlatNode>;

dataSource: MatTreeFlatDataSource<TodoItemNode, TodoItemFlatNode>;

/** The selection for checklist */
checklistSelection = new SelectionModel<TodoItemFlatNode>(true /* multiple */);

constructor(private fb: FormBuilder, private database: ChecklistDatabase) {
  this.treeFlattener = new MatTreeFlattener(this.transformer, this.getLevel,
    this.isExpandable, this.getChildren);
  this.treeControl = new FlatTreeControl<TodoItemFlatNode>(this.getLevel, this.isExpandable);
  this.dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);

  database.dataChange.subscribe(data => {
    this.dataSource.data = data;
  });
}

getLevel = (node: TodoItemFlatNode) => node.level;

isExpandable = (node: TodoItemFlatNode) => node.expandable;

getChildren = (node: TodoItemNode): TodoItemNode[] => node.children;

hasChild = (_: number, _nodeData: TodoItemFlatNode) => _nodeData.expandable;

hasNoContent = (_: number, _nodeData: TodoItemFlatNode) => _nodeData.item === '';

/**
 * Transformer to convert nested node to flat node. Record the nodes in maps for later use.
 */
transformer = (node: TodoItemNode, level: number) => {
  const existingNode = this.nestedNodeMap.get(node);
  const flatNode = existingNode && existingNode.item === node.item
      ? existingNode
      : new TodoItemFlatNode();
  flatNode.item = node.item;
  flatNode.level = level;
  flatNode.expandable = !!node.children;
  this.flatNodeMap.set(flatNode, node);
  this.nestedNodeMap.set(node, flatNode);
  return flatNode;
}
/* expand all node automatically on initialization. */
expandNodesInitially(){
  for (let i = 0; i < this.treeControl.dataNodes.length; i++) {
  
      this.treeControl.expand(this.treeControl.dataNodes[i])
  }
}

/** Whether all the descendants of the node are selected */
descendantsAllSelected(node: TodoItemFlatNode): boolean {
  const descendants = this.treeControl.getDescendants(node);
  return descendants.every(child => this.checklistSelection.isSelected(child));
}

/** Whether part of the descendants are selected */
descendantsPartiallySelected(node: TodoItemFlatNode): boolean {
  const descendants = this.treeControl.getDescendants(node);
  const result = descendants.some(child => this.checklistSelection.isSelected(child));
  return result && !this.descendantsAllSelected(node);
}

/** Toggle the to-do item selection. Select/deselect all the descendants node */
  todoItemSelectionToggle(node: TodoItemFlatNode): void {
   this.checklistSelection.toggle(node);
   const descendants = this.treeControl.getDescendants(node);
   this.checklistSelection.isSelected(node)
    ? this.checklistSelection.select(...descendants)
    : this.checklistSelection.deselect(...descendants);
}
stopPropegationFunction(event) {
  event.stopPropagation();
}
/*custom function for get values.*/
checkNodes() {

  this.selectedInfo = [];

  const completeNodes = this.treeControl.dataNodes;

  const descAllSelected = completeNodes.forEach(child => {

    if(this.checklistSelection.isSelected(child)) {

     // this.selectedInfo.push({name: child.item, status: true})
     this.selectedInfo.push(child.item);

    }

  });

 if(this.selectedInfo.length == 1){
  this.selected = 'oneChecked';
}
else if(this.selectedInfo.length > 1){
  this.selected = 'multipleChecked';
}
else if(this.selectedInfo.length == 0){
  this.selected = '';
}
   console.log(this.selectedInfo);

}


}


