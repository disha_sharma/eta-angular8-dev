import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { FormGroup, FormBuilder, FormArray } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { ServiceService } from '../../eta-services/service.service';
import { TaskServiceService } from '../../dash-board/task-service.service';
import { DashboardServiceService } from '../../dash-board/dashboard-service.service';
import { MatDialog, MatDialogConfig } from '@angular/material'; // for splitter task, added by Shyam joshi
import { ActivatedRoute, Router } from '@angular/router'; // for getting value from url Shyam joshi
import { PopupTemplateComponent } from '../../popup-template/popup-template.component';
import { AddTaskComponent } from '../../popup/add-task/add-task.component';
export interface DialogData {
  id: "",
  action: ''
}

@Component({
  selector: 'app-add-task-flow-dummy',
  templateUrl: './add-task-flow-dummy.component.html',
  styleUrls: ['./add-task-flow-dummy.component.css']
})
export class AddTaskFlowDummyComponent implements OnInit {
  taskTypeId: any;
  taskfrm: FormGroup;
  taskFlowName: any = "ENK";
  Core_URL: any;
  category: any;
  allTaskType: any;
  subTaskType: any;
  dataContainer: any;
  allForm: any;
  taskObj: any = {};
  taskflowId: any ;
  responseMsg: any = {};
  public isSpinnerShow: boolean = false;
  private taskEditObj: any;
  private buttonAction: boolean;
  private actionValue: String;
  private editTrueFlag: boolean = false;
  public allTask: any;
  public relationalOpraters:any;
  public categoryValue:any;
  splitter_task: any = []; // for splitter task, added by Shyam joshi
  joinner_task: any = [];// for splitter task, added by Shyam joshi
  joinner_task_id: any = [];// for splitter task, added by Shyam joshi
 /** EmailTemplate Name List */
 templateNameList: any = [];
 dataElementList: any = [];


  constructor(private _router: Router, private _formBuilder: FormBuilder, private http: HttpClient, public shareServices: ServiceService, private dashboardService : DashboardServiceService,
    private _activatedRouter: ActivatedRoute, public dialog: MatDialog) {
    this.Core_URL = this.shareServices.Core_URL;
    this.taskflowId = this._activatedRouter.snapshot.params['taskFlowId'];
    this.getAllDataElement(); // shyam joshi
     
    console.log('task flow id is');
    console.log(this.taskflowId);
    console.log("this.taskflowId:::: "+this.taskflowId);
    

  }
myFunction() {
  this._router.navigate(['test']);
}
  ngOnInit() {
    this.getEmailTemplateNameInfo();
    this.taskfrm = this._formBuilder.group({

      previousTask: [],
      taskName: [],
      category: [],
      taskType: [],
      container: [],
      taskDes: [],
      privilege: [],
      form: [],

//shyam joshi
      tasktitle: [],
      taskdes: [],
      previlege: [],
      previlegename: [],
      previlegedes: [],
      process: [],
      previoustask: [],
      type: [],
      subtype: [],
      lasttask: [],
      firstDataElement: [],
      relationalOpraters: [],
      secondDataElement: [],

 /* Task IF/ELSE conditions */ // shyam joshi
      taskTrue: [],
      taskFalse: [],
      templateName: [],
      OtherConstant1: [],
      OtherConstant2: [],
      businessRuleTask :[],
      bRuleSecondDataElement :[],
      bRuleRelationalOpraters : [],
      bRuleFirstDataElement: [],
      bRuleOtherConstant2:[],
      businessRuleElseTask:[],

      /*for repeating blocks*/ 
      itemRows: this._formBuilder.array([this.initItemRows()]), //shyam joshi
      businessRuleRow: this._formBuilder.array([this.initBusinessRuleRows()]), // shyam joshi

      darivationTaskList: this._formBuilder.array([this.intiDerivationTaskArray()])

    });

    this.taskfrm.setControl('itemRows', this._formBuilder.array([])); //shyam joshi
   // this.taskfrm.setControl('darivationTaskList', this._formBuilder.array([]));
    this.actionValue = this._activatedRouter.snapshot.params['action'];
    console.log('action is');
    console.log(this.actionValue);
    /* Functions Start Here */
    this.getAllTaskType();
    this.getAllDataContainer();
    this.getAllForm();
    this.getAllTaskByTaskFlowId(this.taskflowId)
    this.getAllOperators();
    this.shareServices.currentMessage.subscribe(message => this.taskEditObj = message);

  }

  intiDerivationTaskArray() {
    return this._formBuilder.group({
      // list all your form controls here, which belongs to your form Business rule array
      dataElementOneID: [],
      derivationOperatorID: [],
      dataElementTwoID: [],
      derivationDataElementID:[],
    });
  }

  



  taskEditFx() {
    if (this.actionValue == "Edit") {
      //  alert(this.taskEditObj.subTaskTypeID)
      console.log("i am in edit" + this.taskEditObj.subTaskTypeID);

      this.taskTypeId = this.taskEditObj.subTaskTypeID;
      this.taskfrm.patchValue({ previousTask: this.taskEditObj.previousTaskID });
      this.taskfrm.patchValue({ taskName: this.taskEditObj.taskName });
      this.taskfrm.patchValue({ taskDes: this.taskEditObj.taskDescription });
      this.taskfrm.patchValue({ privilege: this.taskEditObj.previlege });
      this.taskfrm.patchValue({ container: this.taskEditObj.taskFlowID });
      this.taskfrm.patchValue({ category: this.taskEditObj.taskTypeID });
      this.taskfrm.patchValue({ taskType: this.taskTypeId });
      this.taskfrm.patchValue({ form: this.taskEditObj.formId });
      this.taskfrm.patchValue({ darivationTaskList :this.taskEditObj.derivationList})
      this.categoryChange(this.taskEditObj.taskTypeID, this.taskTypeId)


    }
  }

  foods: any = [
    { value: 1, viewValue: 'START TASK' },
    { value: 2, viewValue: 'MIDDLE' },
    { value: 3, viewValue: 'END TASK' }
  ];

  taskTypeFx(taskType) {
    this.taskTypeId = taskType;
    console.log("The Task Type is :====" + this.taskTypeId);

  }

  public getAllTaskType() {
    this.shareServices.getAllTaskType()
      .subscribe(data => {
        this.category = data;
        console.log("i am cat");
        this.taskEditFx();
      });
  }

  public categoryChange(changevalue: any, value: any) {

    console.log(changevalue);
    this.categoryValue = changevalue;

    this.category.forEach(element => {
      if (element.taskTypeID == changevalue) {
        this.subTaskType = element.subTaskTypeDAO;
        console.log(this.subTaskType);
        this.taskTypeFx(value);
      }
    });
  }

  onSubmit = function (taskValue) {
    console.log(taskValue);
    console.log(taskValue.darivationTaskList);
    
    this.taskObj["taskID"] = this.taskEditObj.taskID;
    this.taskObj["taskName"] = this.taskfrm.value.taskName;
    this.taskObj["taskDescription"] = this.taskfrm.value.taskDes;
    this.taskObj["previlege"] = this.taskfrm.value.privilege;
    this.taskObj["privilegeName"] = this.taskfrm.value.privilege;
    this.taskObj["privilegeDescription"] = this.taskfrm.value.privilege;

    this.taskObj["taskFlowID"] = this.taskflowId;

    this.taskObj["previousTaskID"] = this.taskfrm.value.previousTask;
    this.taskObj["taskTypeID"] = this.taskfrm.value.category;
    this.taskObj["subTaskTypeID"] = this.taskfrm.value.taskType;
    this.taskObj["formId"] = this.taskfrm.value.form;
    this.taskObj["isStartOrEntTask"] = 0;
    this.taskObj["derivationList"] = taskValue.darivationTaskList;


    /* edit Value */
    this.taskObj["privilegeId"] = this.taskEditObj.privilegeId
    console.log("add-task compo ......."+this.taskflowId);
    
    //this.taskService.saveTask(this.taskFlowId);
    ///this.shareServices.setTaskFlowId(this.taskflowId);
    console.log(this.taskObj);
   // this.isSpinnerShow = true;
    //this.shareServices.saveTask(this.taskObj).subscribe();
     this.http.post(this.Core_URL + '/saveTask/', this.taskObj)
      .subscribe(data => {
        console.log(data);

        if (data != null) {
          //alert("i am in success");
          this.responseMsg.displayMsg = "Successfully added Leasing and placed it at the end of the display order. Drag it up to reorder.";
          this.responseMsg.cssClass = "Success";
          this.isSpinnerShow = false;
         
        } else {
          //  alert("i am in Error");
          this.responseMsg.displayMsg = "Error added Leasing and placed it at the end of the display order. Drag it up to reorder.";
          this.responseMsg.cssClass = "Error";
          this.isSpinnerShow = false;
        }
      }, error => {
        this.responseMsg.displayMsg = "Error added Leasing and placed it at the end of the display order. Drag it up to reorder.";
        this.responseMsg.cssClass = "Error";
        this.isSpinnerShow = false;
      });

  }

  getAllDataContainer() {
    this.shareServices.getAllDataContainer()
      .subscribe(res => {
        this.dataContainer = res;
        console.log(res);

      })
  }

  public getAllForm() {
    this.http.get(this.Core_URL + '/getAllForm')
      .subscribe(data => {
        this.allForm = data;
      });
  }


  public getAllTaskByTaskFlowId(taskFlowId: number) {


    this.http.get(this.Core_URL + '/getAllTaskByTaskFlowId/' + taskFlowId)
      .subscribe(data => {
        console.log(data);

        let taskList: any = [];
        taskList = data;

        if (taskList.length == 0) {
          alert('No task available');
        }
        this.allTask = data;
        console.log(data);

      });
  }

  addNewDerivation() {
    // control refers to your formarray
    const control = <FormArray>this.taskfrm.controls['darivationTaskList'];
    // add new formgroup
    control.push(this.intiDerivationTaskArray());
  }


  /* Get All Operator  */

  getAllOperators(){
    this.http.get(this.Core_URL + '/getAllSpdOperators')
    .subscribe(data => {
      this.relationalOpraters = data;
      console.log(data);
      
    });
  }
  //code for 2w route task starts, added by shyam joshi
  initItemRows() {
    return this._formBuilder.group({
      // list all your form controls here, which belongs to your form array
      relationalConditionRow: [],
      firstDataElementRow: [],
      relationalOpratersRow: [],
      secondDataElementRow: [],
      OtherConstant1: [],
      OtherConstant2: []
    });
  }
  initBusinessRuleRows() {
    return this._formBuilder.group({
      // list all your form controls here, which belongs to your form Business rule array
      bRulefirstDataElementRow: [],
      bRuleRelationalOpratersRow: [],
      bRuleSecondDataElementRow: [],
      businessRuleTaskRow: [],
      bRuleOtherConstantRow: [],
      OtherConstant2: [],
    });
  }
  addNewRow() {
    // control refers to your formarray
    const control = <FormArray>this.taskfrm.controls['itemRows'];
    // add new formgroup
    control.push(this.initItemRows());
  }
  deleteRow(index: number) {
    // control refers to your formarray
    const control = <FormArray>this.taskfrm.controls['itemRows'];
    // remove the chosen row
    control.removeAt(index);
  }
//code for 2w route task ends


//code for splitter task, added by Shyam joshi, starts here
openDialog(): void {

  /*  const dialogRef = this.dialog.open(PopupTemplateComponent, {
     width: '80% auto',
     data: { taskId: this.taskId, taskName: this.taskName }
   }); */

  const dialogConfig = new MatDialogConfig();
  dialogConfig.disableClose = true;
  dialogConfig.autoFocus = true;

  dialogConfig.data = {
    id: 1,
    title: 'Angular For Beginners'
  };

  const dialogRef = this.dialog.open(AddTaskComponent, dialogConfig);

  dialogRef.afterClosed()
    .subscribe(result => {
      console.log('The dialog was closed' + result);
      console.log(result);
      // Add our fruit
      this.shareServices.getTaskByTaskId(result)
        .subscribe(res => {
          const value = res.taskName;
          if ((value || '').trim()) {
            this.splitter_task.push({ name: value.trim(), id: res.taskID });
            // this.splitter_task_id.push({id:res.id});
          }
        }, error => {
          alert("No Splitter Task Added !!!")
        })
    });
}
openFutureTaskDialog(): void {

  /*  const dialogRef = this.dialog.open(PopupTemplateComponent, {
     width: '80% auto',
     data: { taskId: this.taskId, taskName: this.taskName }
   }); */

  const dialogConfig = new MatDialogConfig();
  dialogConfig.disableClose = true;
  dialogConfig.autoFocus = true;

  dialogConfig.data = {
    id: 1,
    title: 'Angular For Beginners'
  };

  const dialogRef = this.dialog.open(AddTaskComponent, dialogConfig);

  dialogRef.afterClosed()
    .subscribe(result => {
      console.log('The dialog was closed' + result);
      console.log(result);
      // Add our fruit
      this.shareServices.getTaskByTaskId(result)
        .subscribe(res => {
          const value = res.taskName;
          if ((value || '').trim()) {
            this.joinner_task.push({ name: value.trim(), id: res.taskID });
            // this.splitter_task_id.push({id:res.id});
          }
        }, error => {
          alert("No Joinner Task Added !!!")
        })
    });
}
//code for splitter task, added by Shyam joshi, ends here

//done by shyam joshi
addNewBusinessRule() {
  // control refers to your formarray
  const control = <FormArray>this.taskfrm.controls['businessRuleRow'];
  // add new formgroup
  control.push(this.initBusinessRuleRows());
}
deleteBusinessRule(index: number) {
  // control refers to your formarray
  const control = <FormArray>this.taskfrm.controls['businessRuleRow'];
  // remove the chosen row
  control.removeAt(index);
}

/*for notification, done by Shyam, starts */
getEmailTemplateNameInfo() {
  this.shareServices.getAllTemplateByOrgId()
    .subscribe(res => {
      console.log(res);
      this.templateNameList = res;
      if (this.templateNameList == null || this.templateNameList.length == 0) {
        alert("No Template Found!! please add notification template");
      }

    })
}
/*for notification, done by Shyam, ends */

//done by Shyam joshi
public getAllDataElement() {
  this.http.get(this.Core_URL + '/getAllDataElementByOrgId')
    .subscribe(data => {
      this.dataElementList = data;
    });
  this.http.get(this.Core_URL + '/getAllSpdOperators')
    .subscribe(data => {
      this.relationalOpraters = data;
    });
}

}
