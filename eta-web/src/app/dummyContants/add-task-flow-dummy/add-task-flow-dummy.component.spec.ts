import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddTaskFlowDummyComponent } from './add-task-flow-dummy.component';

describe('AddTaskFlowDummyComponent', () => {
  let component: AddTaskFlowDummyComponent;
  let fixture: ComponentFixture<AddTaskFlowDummyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddTaskFlowDummyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddTaskFlowDummyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
