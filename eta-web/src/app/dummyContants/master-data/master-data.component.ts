import { Component, OnInit, ViewChild } from '@angular/core';
import {MatPaginator, MatTableDataSource, MatSort} from '@angular/material';
import { animate, state, style, transition, trigger } from '@angular/animations';


@Component({
  selector: 'app-master-data',
  templateUrl: './master-data.component.html',
  styleUrls: ['./master-data.component.css'],
  animations: [
    trigger('detailExpand', [
      state('void', style({ height: '0px', minHeight: '0', visibility: 'hidden' })),
      state('*', style({ height: '*', visibility: 'visible' })),
      transition('void <=> *', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ]
})
export class MasterDataComponent implements OnInit {
  readMoreCheckbox: boolean;
  displayedColumns: string[] = ['checkbox', 'group', 'items', 'type', 'scope', 'status'];
 // displayedColumns: string[] = ['checkbox', 'Group', 'Items', 'Type', 'Scope', 'Status'];
  tableData =  [
    {checkbox: 1, group: 'Salutation', items: 9, type: 'System', scope: 'Global', status: 'green', description: [{name: 'Mr. Deepak'}, {name: 'Mr. Deepak'},{name: 'Mr. Deepak'},{name: 'Dr. Ravi'},{name: 'Mr. Deepak'},{name: 'Dr. Ravi'},{name: 'Dr. Himanshu'},{name: 'Mr. Kanhiya'},{name: 'Dr. Abhishek',},{name: 'Dr. Shyam',}]},
    {checkbox: 2, group: 'Country', items: 5, type: 'System', scope: 'Global', status: 'red', description: [{name: 'Dr. Ravi'},{name: 'Mr. Deepak'},{name: 'Dr. Ravi'},{name: 'Dr. Himanshu'},{name: 'Mr. Kanhiya'},{name: 'Dr. Abhishek',},{name: 'Dr. Shyam',}]},
    {checkbox: 3, group: 'Location', items: 4, type: 'Reused', scope: 'Global', status: 'green', description: [{name: 'Mr. Deepak'},{name: 'Dr. Ravi'},{name: 'Mr. Deepak'},{name: 'Dr. Ravi'},{name: 'Dr. Himanshu'},{name: 'Mr. Kanhiya'},{name: 'Dr. Abhishek',},{name: 'Dr. Shyam',}]},
    {checkbox: 4, group: 'Production Type', items: 4, type: 'Configurable', scope: 'Global', status: 'amber', description: [{name: 'Mr. Deepak'},{name: 'Dr. Ravi'},{name: 'Mr. Deepak'},{name: 'Dr. Ravi'},{name: 'Dr. Himanshu'},{name: 'Mr. Kanhiya'},{name: 'Dr. Abhishek',},{name: 'Dr. Shyam',}]},
    {checkbox: 5, group: 'Venue', items: 4, type: 'Configurable', scope: 'Global', status: 'green', description: [{name: 'Mr. Deepak'},{name: 'Dr. Ravi'},{name: 'Mr. Deepak'},{name: 'Dr. Ravi'},{name: 'Dr. Himanshu'},{name: 'Mr. Kanhiya'},{name: 'Dr. Abhishek',},{name: 'Dr. Shyam',}]},
    {checkbox: 6, group: 'Production Type', items: 5, type: 'Configurable', scope: 'Global', status: 'green', description: [{name: 'Mr. Deepak'},{name: 'Dr. Ravi'},{name: 'Mr. Deepak'},{name: 'Dr. Ravi'},{name: 'Dr. Himanshu'},{name: 'Mr. Kanhiya'},{name: 'Dr. Abhishek',},{name: 'Dr. Shyam',}]},
    {checkbox: 7, group: 'Genre', items: 1, type: 'Configurable', scope: 'Global', status: 'green', description: [{name: 'Mr. Deepak'}, {name: 'Mr. Deepak'},{name: 'Dr. Ravi'},{name: 'Mr. Deepak'},{name: 'Dr. Ravi'},{name: 'Dr. Himanshu'},{name: 'Mr. Kanhiya'},{name: 'Dr. Abhishek',},{name: 'Dr. Shyam',}]},
    {checkbox: 1, group: 'Salutation', items: 2, type: 'Configurable', scope: 'Global', status: 'green', description: [{name: 'Mr. Deepak'}, {name: 'Mr. Deepak'},{name: 'Mr. Deepak'},{name: 'Dr. Ravi'},{name: 'Mr. Deepak'},{name: 'Dr. Ravi'},{name: 'Dr. Himanshu'},{name: 'Mr. Kanhiya'},{name: 'Dr. Abhishek',},{name: 'Dr. Shyam',}]},
    {checkbox: 8, group: 'Salutation', items: 5, type: 'Configurable', scope: 'Global', status: 'green', description: [{name: 'Mr. Deepak'},{name: 'Dr. Ravi'},{name: 'Mr. Deepak'},{name: 'Dr. Ravi'},{name: 'Dr. Himanshu'},{name: 'Mr. Kanhiya'},{name: 'Dr. Abhishek',},{name: 'Dr. Shyam',}]},
    {checkbox: 9, group: 'Country', items: 7, type: 'Configurable', scope: 'Global', status: 'green', description: [{name: 'Mr. Deepak'},{name: 'Dr. Ravi'},{name: 'Mr. Deepak'},{name: 'Dr. Ravi'},{name: 'Dr. Himanshu'},{name: 'Mr. Kanhiya'},{name: 'Dr. Abhishek',},{name: 'Dr. Shyam',}]},
    {checkbox: 10, group: 'Venue', items: 7, type: 'Configurable', scope: 'Global', status: 'green', description: [{name: 'Mr. Deepak'},{name: 'Dr. Ravi'},{name: 'Mr. Deepak'},{name: 'Dr. Ravi'},{name: 'Dr. Himanshu'},{name: 'Mr. Kanhiya'},{name: 'Dr. Abhishek',},{name: 'Dr. Shyam',}]},
    {checkbox: 11, group: 'Venue', items: 7, type: 'Configurable', scope: 'Global', status: 'green', description: [{name: 'Mr. Deepak'},{name: 'Dr. Ravi'},{name: 'Mr. Deepak'},{name: 'Dr. Ravi'},{name: 'Dr. Himanshu'},{name: 'Mr. Kanhiya'},{name: 'Dr. Abhishek',},{name: 'Dr. Shyam',}]},
    {checkbox: 12, group: 'Venue', items: 7, type: 'Configurable', scope: 'Global', status: 'green', description: [{name: 'Mr. Deepak'},{name: 'Dr. Ravi'},{name: 'Mr. Deepak'},{name: 'Dr. Ravi'},{name: 'Dr. Himanshu'},{name: 'Mr. Kanhiya'},{name: 'Dr. Abhishek',},{name: 'Dr. Shyam',}]},
    {checkbox: 13, group: 'Venue', items: 7, type: 'Configurable', scope: 'Global', status: 'green', description: [{name: 'Mr. Deepak'},{name: 'Dr. Ravi'},{name: 'Mr. Deepak'},{name: 'Dr. Ravi'},{name: 'Dr. Himanshu'},{name: 'Mr. Kanhiya'},{name: 'Dr. Abhishek',},{name: 'Dr. Shyam',}]},
    {checkbox: 14, group: 'Venue', items: 8, type: 'Configurable', scope: 'Global', status: 'green', description: [{name: 'Mr. Deepak'},{name: 'Dr. Ravi'},{name: 'Mr. Deepak'},{name: 'Dr. Ravi'},{name: 'Dr. Himanshu'},{name: 'Mr. Kanhiya'},{name: 'Dr. Abhishek',},{name: 'Dr. Shyam',}]},
    {checkbox: 15, group: 'Venue', items: 5, type: 'Configurable', scope: 'Global', status: 'green', description: [{name: 'Mr. Deepak'},{name: 'Dr. Ravi'},{name: 'Mr. Deepak'},{name: 'Dr. Ravi'},{name: 'Dr. Himanshu'},{name: 'Mr. Kanhiya'},{name: 'Dr. Abhishek',},{name: 'Dr. Shyam',}]},
    {checkbox: 16, group: 'Venue', items: 5, type: 'Configurable', scope: 'Global', status: 'green', description: [{name: 'Mr. Deepak'},{name: 'Dr. Ravi'},{name: 'Mr. Deepak'},{name: 'Dr. Ravi'},{name: 'Dr. Himanshu'},{name: 'Mr. Kanhiya'},{name: 'Dr. Abhishek',},{name: 'Dr. Shyam',}]},
    {checkbox: 17, group: 'Venue', items: 5, type: 'Configurable', scope: 'Global', status: 'green', description: [{name: 'Mr. Deepak'},{name: 'Dr. Ravi'},{name: 'Mr. Deepak'},{name: 'Dr. Ravi'},{name: 'Dr. Himanshu'},{name: 'Mr. Kanhiya'},{name: 'Dr. Abhishek',},{name: 'Dr. Shyam',}]},
    {checkbox: 18, group: 'Venue', items: 2, type: 'Configurable', scope: 'Global', status: 'green', description: [{name: 'Mr. Deepak'},{name: 'Dr. Ravi'},{name: 'Mr. Deepak'},{name: 'Dr. Ravi'},{name: 'Dr. Himanshu'},{name: 'Mr. Kanhiya'},{name: 'Dr. Abhishek',},{name: 'Dr. Shyam',}]},
    {checkbox: 19, group: 'Venue', items: 2, type: 'Configurable', scope: 'Global', status: 'green', description: [{name: 'Mr. Deepak'},{name: 'Dr. Ravi'},{name: 'Mr. Deepak'},{name: 'Dr. Ravi'},{name: 'Dr. Himanshu'},{name: 'Mr. Kanhiya'},{name: 'Dr. Abhishek',},{name: 'Dr. Shyam',}]},
    {checkbox: 20, group: 'Venue', items: 8, type: 'Configurable', scope: 'Global', status: 'green', description: [{name: 'Mr. Deepak'},{name: 'Dr. Ravi'},{name: 'Mr. Deepak'},{name: 'Dr. Ravi'},{name: 'Dr. Himanshu'},{name: 'Mr. Kanhiya'},{name: 'Dr. Abhishek',},{name: 'Dr. Shyam',}]},
  ];

  dataSource = new MatTableDataSource(this.tableData);

  @ViewChild(MatPaginator,  {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  isExpansionDetailRow = (index, row) => row.hasOwnProperty('detailRow');

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }
  stopPropagationFunction(event){
    event.stopPropagation();
  }
}



