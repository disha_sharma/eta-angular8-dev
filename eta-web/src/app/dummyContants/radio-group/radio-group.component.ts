import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-radio-group',
  templateUrl: './radio-group.component.html',
  styleUrls: ['./radio-group.component.css']
})
export class RadioGroupComponent implements OnInit {
  selectedRadio: string;
  options: string[] = ['option1', 'option2', 'option3', 'option4', 'option5'];
  radioError: boolean;

  constructor() { }

  ngOnInit() {
  }
  

  check() {
    if(this.selectedRadio) {
      this.radioError = false;
      console.log(this.selectedRadio);
    }
    else {
     this.radioError = true;
    }
    
 
  }

}
