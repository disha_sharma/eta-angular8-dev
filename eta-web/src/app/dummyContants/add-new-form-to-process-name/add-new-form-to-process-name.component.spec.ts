import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddNewFormToProcessNameComponent } from './add-new-form-to-process-name.component';

describe('AddNewFormToProcessNameComponent', () => {
  let component: AddNewFormToProcessNameComponent;
  let fixture: ComponentFixture<AddNewFormToProcessNameComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddNewFormToProcessNameComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddNewFormToProcessNameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
