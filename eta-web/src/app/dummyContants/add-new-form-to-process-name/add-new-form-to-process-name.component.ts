import { Component, OnInit, Input } from '@angular/core';
import {FormControl} from '@angular/forms'; 

import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ArrayType } from '@angular/compiler/src/output/output_ast';
@Component({
  selector: 'app-add-new-form-to-process-name',
  templateUrl: './add-new-form-to-process-name.component.html',
  styleUrls: ['./add-new-form-to-process-name.component.css']
})
export class AddNewFormToProcessNameComponent implements OnInit {
  //code for field name
  name = new FormControl('', [Validators.required, Validators.maxLength(64)]); 
  getErrorMessage() {
    return this.name.hasError('required') ? 'You must enter a value' :
       '';
  }
  //reusable 
  reusable: boolean = false;
  //code for type field
  types: any = ['type1', 'submit', 'cancel'];
 // typeFormControl = new FormControl('', [Validators.required]);

   //code for reference field
   references: any = ['reference1', 'reference2', 'reference3'];

   //data for validation field
   validations: any = ['validation1', 'validation2', 'validation3'];
   checkedField: number = -1;
  constructor(private fb: FormBuilder) { }

  myForm: FormGroup;
  deleteButtonFlag: boolean = false;
  checkAllItem: boolean = false;
  ngOnInit() {
     /* Initiate the form structure  */
    this.initilizeFormGroup();
  }
initilizeFormGroup(){
      /* Initiate the form structure - first default */
      this.myForm = this.fb.group({
        title: [],
        form_group: this.fb.array([this.fb.group({
          filedCheckStatus: '',
          index: '',
          name:'', 
        type: '',
        mandatory: '',
        fieldStore:'',
        default: '',
        reference: '',
        helpText: '',
        validation: ''
  
      })]),
        
      })
    //for second default field
      this.formGroup.push(this.fb.group({
        filedCheckStatus: '', 
        index: '',
        name:'Submit', 
        type: 'submit',
        mandatory: '',
        fieldStore:'',
        default: '',
        reference: '',
        helpText: '',
        validation: ''
    }));
      //for thire default field
      this.formGroup.push(this.fb.group({
        filedCheckStatus: '', 
        index: '',
        name:'Cancel', 
        type: 'cancel',
        mandatory: '',
        fieldStore:'',
        default: '',
        reference: '',
        helpText: '',
        validation: ''
    }));
}
  get formGroup() {
    return this.myForm.get('form_group') as FormArray;
  }

  /////// This is new /////////////////

  addFormField() {
    this.formGroup.push(this.fb.group({
      filedCheckStatus: '', 
      index: '',
      name:'', 
      type: '',
      mandatory: '',
      fieldStore:'',
      default: '',
      reference: '',
      helpText: '',
      validation: ''
  }));
  this.toggleCheckAllButton();
  }
  
markActiveFieldByCheckBox(checkedIndex){
  this.checkedField = checkedIndex;
}

  deleteFormField() {
    //this.formGroup.removeAt(this.checkedField);
   var i = this.myForm.value.form_group.length
while (i--) {
   if (this.myForm.value.form_group[i].filedCheckStatus == true) { 
      this.formGroup.removeAt(i);
    } 
}
this.changeStatusOfCheckAllCheckbox('uncheck');
this.deleteButtonFlag = false;
}


/* show delete button when any field is checked */
toggleDeleteButton(){
  let length = this.myForm.controls['form_group'].value.length;
  let control = <FormArray>this.myForm.controls['form_group'];
   for(let i = 0; i < length; i++){
    if( control.controls[i].get('filedCheckStatus').value){
     this.deleteButtonFlag = true;
     return;
   }
   else{
     this.deleteButtonFlag = false;
   }
   }
}

/* toggle checkAllButton */
toggleCheckAllButton(){
  let length = this.myForm.controls['form_group'].value.length;
  let control = <FormArray>this.myForm.controls['form_group'];
   for(let i = 0; i < length; i++){
   
    if( !control.controls[i].get('filedCheckStatus').value){
this.changeStatusOfCheckAllCheckbox('uncheck');
return
    }
    else{
      this.changeStatusOfCheckAllCheckbox('check');
     }
  }
}
/* uncheck checkAll check box */
changeStatusOfCheckAllCheckbox(action){
  if(action == 'check'){
    this.checkAllItem = true;
  }
  else if(action == 'uncheck')
  this.checkAllItem = false;
}

//check and uncheck all 
checkAllItems(event) {
  console.log(event.checked);
  
  if(event.checked){
  
 let length = this.myForm.controls['form_group'].value.length;
 let control = <FormArray>this.myForm.controls['form_group'];
  for(let i = 0; i < length; i++){
   
   control.controls[i].get('filedCheckStatus').setValue(true);
  }
 }
  else if(!event.checked){
    let length = this.myForm.controls['form_group'].value.length;
    let control = <FormArray>this.myForm.controls['form_group'];
    for(let i = 0; i < length; i++){
   
     control.controls[i].get('filedCheckStatus').setValue(false);
     }
  }
 }
  
}