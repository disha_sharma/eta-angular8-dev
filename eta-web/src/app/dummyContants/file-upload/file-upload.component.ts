import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.css']
})
export class FileUploadComponent implements OnInit {
  fileInput: string = "";
  hide: boolean = false;
  fileNotSupported: boolean;
  newInput: string;
  validatedStatus: boolean;
  uploadButtonEnabled: boolean = false;
  progressBarValue = 0;
  
  validateAndNotSupportedControls: boolean; // for showing validation and not supported buttons on screen.
  constructor() { }

  ngOnInit() {
  }

  //function for upload file
  uploadFile() {
    if(this.validatedStatus){
      return true;
    }
    else{
    this.validateAndNotSupportedControls = true;
    this.progressBarValue = 50;
    }
  }
//function for successfully Validate 
  validate() {
    this.validatedStatus = true;
    this.progressBarValue = 100;
    this.uploadButtonEnabled = true;
    this.validateAndNotSupportedControls = false;
  }
  // function for make the file not supported
  notSupported(){
    this.progressBarValue = 100;
    this.fileNotSupported = true;
    this.validatedStatus = false;
    this.uploadButtonEnabled = false;
    this.validateAndNotSupportedControls = false;
    
  }
  inputChangeFunction(){
    
    this.fileNotSupported = false;
    this.validatedStatus = false;
    this.validateAndNotSupportedControls = false;
    this.progressBarValue = 0;
    if(this.fileInput) {
      this.uploadButtonEnabled = true;
    }
    else{
      this.uploadButtonEnabled = false;
    }
  }
  
 
 }
