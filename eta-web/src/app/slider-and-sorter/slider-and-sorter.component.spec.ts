import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SliderAndSorterComponent } from './slider-and-sorter.component';

describe('SliderAndSorterComponent', () => {
  let component: SliderAndSorterComponent;
  let fixture: ComponentFixture<SliderAndSorterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SliderAndSorterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SliderAndSorterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
