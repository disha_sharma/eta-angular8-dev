import { Component, OnInit,  ViewChild, ElementRef  } from '@angular/core';
import {animate, state, style, transition, trigger} from '@angular/animations';

@Component({
  selector: 'app-slider-and-sorter',
  templateUrl: './slider-and-sorter.component.html',
  styleUrls: ['./slider-and-sorter.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0', display: 'none'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ]
})
export class SliderAndSorterComponent implements OnInit {

  @ViewChild('widgetsContent', { read: ElementRef, static: true }) public widgetsContent: ElementRef<any>;
  cards = [

    {heading: 3, discription: 'Functions'},
    {heading: 12, discription: 'Processes'},
    {heading: '15.8K', discription: 'Transactions'},
    {heading: '$4M', discription: 'Value of Txs.'},
    {heading: 3, discription: 'Functions'},
    {heading: 12, discription: 'Processes'},
    {heading: '15.8K', discription: 'Transactions'},
    {heading: '$4M', discription: 'Value of Txs.'},
    {heading: 3, discription: 'Functions'},
    {heading: 12, discription: 'Processes'},
    {heading: '15.8K', discription: 'Transactions'},
    {heading: '$4M', discription: 'Value of Txs.'},
    
  ]

  dataSource = [
    {
      position: 1,
      name: 'Hydrogen',
      weight: 1.0079,
      symbol: 'H',
      description: `Hydrogen is a chemical element with symbol H and atomic number 1. With a standard
          atomic weight of 1.008, hydrogen is the lightest element on the periodic table.`
    }, {
      position: 2,
      name: 'Helium',
      weight: 4.0026,
      symbol: 'He',
      description: `Helium is a chemical element with symbol He and atomic number 2. It is a
          colorless, odorless, tasteless, non-toxic, inert, monatomic gas, the first in the noble gas
          group in the periodic table. Its boiling point is the lowest among all the elements.`
    }, {
      position: 3,
      name: 'Lithium',
      weight: 6.941,
      symbol: 'Li',
      description: `Lithium is a chemical element with symbol Li and atomic number 3. It is a soft,
          silvery-white alkali metal. Under standard conditions, it is the lightest metal and the
          lightest solid element.`
    }, {
      position: 4,
      name: 'Beryllium',
      weight: 9.0122,
      symbol: 'Be',
      description: `Beryllium is a chemical element with symbol Be and atomic number 4. It is a
          relatively rare element in the universe, usually occurring as a product of the spallation of
          larger atomic nuclei that have collided with cosmic rays.`
    }, {
      position: 5,
      name: 'Boron',
      weight: 10.811,
      symbol: 'B',
      description: `Boron is a chemical element with symbol B and atomic number 5. Produced entirely
          by cosmic ray spallation and supernovae and not by stellar nucleosynthesis, it is a
          low-abundance element in the Solar system and in the Earth's crust.`
    }, 
  ];;
    columnsToDisplay = ['name', 'weight', 'symbol', 'position'];
  
  
  constructor() { }

  ngOnInit() {
  }

  




prev(){

this.widgetsContent.nativeElement.scrollTo({ left: (this.widgetsContent.nativeElement.scrollLeft -300), behavior: 'smooth' });

}

next(){

this.widgetsContent.nativeElement.scrollTo({ left: (this.widgetsContent.nativeElement.scrollLeft + 300), behavior: 'smooth' });

}

}
