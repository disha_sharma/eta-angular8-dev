import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EtaPageReloadComponent } from './eta-page-reload.component';

describe('EtaPageReloadComponent', () => {
  let component: EtaPageReloadComponent;
  let fixture: ComponentFixture<EtaPageReloadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EtaPageReloadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EtaPageReloadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
