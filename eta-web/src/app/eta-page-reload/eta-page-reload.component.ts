import { Component, OnInit } from '@angular/core';
import { ServiceService } from '../eta-services/service.service';

@Component({
  selector: 'app-eta-page-reload',
  template: '',
})
export class EtaPageReloadComponent implements OnInit {

  constructor(private newService :ServiceService) { 
    this.newService.gridRefresh();
  }

  ngOnInit() {

  }

}
