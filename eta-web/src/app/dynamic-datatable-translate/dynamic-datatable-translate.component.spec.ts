import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DynamicDatatableTranslateComponent } from './dynamic-datatable-translate.component';

describe('DynamicDatatableTranslateComponent', () => {
  let component: DynamicDatatableTranslateComponent;
  let fixture: ComponentFixture<DynamicDatatableTranslateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DynamicDatatableTranslateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DynamicDatatableTranslateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
