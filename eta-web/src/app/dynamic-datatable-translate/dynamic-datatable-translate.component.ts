import { Component, OnInit, Input } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { ServiceService } from './../eta-services/service.service'
import { FormGroup, FormControl } from '@angular/forms';

class DataTablesResponse {
  data: any[];
  draw: number;
  recordsFiltered: number;
  recordsTotal: number;
  coloumnName: {};
}

@Component({
  selector: 'app-dynamic-datatable-translate',
  templateUrl: './dynamic-datatable-translate.component.html',
  styleUrls: ['./dynamic-datatable-translate.component.css'],
  providers: [ServiceService],
})
export class DynamicDatatableTranslateComponent implements OnInit {

  @Input() childMessage: string;
  @Input() myurl: string;
  @Input() mycoloums = [];
  @Input() orgId :number;


  dtOptions: DataTables.Settings = {};
  gefields: any[];
  public arrayOfKeys = [];
  public arrayOfValues = [];

  form;
  dataEdit: any = {};
  allTemplate: any = [];
  editTemp: any = {};
  emailLang: any = {};
  tdheader: any = {};
  emailLangTrans = [];
  editEmailTemplateshow: any = {};
  languageData:any;


  tempid = "";
  tempvid = "";
  langid = "";


  editEmailTemplate = "";
  editorgId = "";
  editMailEvent = "";
  editLangId = "";
  editSubjectValue = "";
  editMessageValue = "";

  
  min: number;
  max: number;
  ETA_SELECTED_ORGID:any =0;

  constructor(private http: HttpClient, private newService: ServiceService, ) {

    //console.log(this.myurl + "**********constructor********" + this.mycoloums);
    this.newService.getHeaderLanguageByOrgId(1)
      .subscribe(data => {
        this.tdheader = data;
        this.tdheader.forEach(element => {
          this.languageData=element.spdlanguage.language;
        });
      });
    
  }

  ngOnInit(): void {
    if(localStorage.getItem("EtaSelectOrgId")!=null){
      this.ETA_SELECTED_ORGID = localStorage.getItem("EtaSelectOrgId");
    }
    //console.log(this.myurl + "**********ngOnInit********" + this.mycoloums);
    //console.log("*********************!!!!!!!!!!!!!!!!***********"+this.ETA_SELECTED_ORGID);
    
    const that = this;

    this.form = new FormGroup({
      edittemplateId: new FormControl(""),
      editlanguageId: new FormControl(""),
      editSubject: new FormControl(""),
      editMessage: new FormControl(""),
    });


    

      $.fn['dataTable'].ext.search.push((settings, data, dataIndex) => {
        const id = parseFloat(data[0]) || 0; // use data for the id column
        if ((isNaN(this.min) && isNaN(this.max)) ||
          (isNaN(this.min) && id <= this.max) ||
          (this.min <= id && isNaN(this.max)) ||
          (this.min <= id && id <= this.max)) {
          return true;
        }
        return false;
      });


    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 4,
      serverSide: true,
      processing: true,
      columns: this.mycoloums,
      // columns: [{ data: 'functionName' }, { data: 'functionDescription' }, { data: 'functionID' }],
      ajax: (dataTablesParameters: any, callback) => {
        dataTablesParameters.orgId=this.ETA_SELECTED_ORGID;
        that.http
          .post<DataTablesResponse>(

            this.myurl, dataTablesParameters, {}


          ).subscribe(resp => {

            //console.log("-------1111---------");
            //console.log("-------222---------" + resp.data);
            //console.log("------333----------" + resp.coloumnName);

            that.gefields = resp.data;
            // this.arrayOfKeys =Object.keys(resp.data[0]);
            this.arrayOfKeys = Object.keys(resp.coloumnName);
            //console.log("Key----------------" + this.arrayOfKeys);

            this.arrayOfValues = Object.values(resp.coloumnName);
            //console.log("Value----------------" + this.arrayOfValues);

            // document.getElementById("datatabledcsdf").firstElementChild.firstElementChild.nextElementSibling.setAttribute("style","display:none");

            // document.getElementById("datatabledcsdf").lastElementChild.nextElementSibling.setAttribute("style","display:none");

            this.arrayOfKeys.forEach(element => {
              //console.log(element);

              //this.dtOptions.columns[{data: element}];
            });

            callback({
              recordsTotal: resp.recordsTotal,
              recordsFiltered: resp.recordsFiltered,
              data: []
            });
          });
      },

    };
  }

  
  htmlToPlaintext = function (Message) {
    return Message ? String(Message).replace(/<[^>]+>/gm, '') : '';
  }

  data: object;
  onSubmit = function (tempobj) {
    let userobj: Object = {};
    let tempdataobj: Object = {};
    tempdataobj = this.editEmailTemplate;

    this.form = new FormGroup({
      edittemplateId: new FormControl(""),
      editlanguageId: new FormControl(""),
      editSubject: new FormControl(""),
      editMessage: new FormControl(""),

    });

    userobj["emailTemplateId"] = this.tempid;
    userobj["emailTemplateVersionId"] = this.tempvid;
    userobj["languageId"] = this.langid;
    userobj["translationMailSubject"] = tempobj.editSubject;
    userobj["translationMailMessage"] = tempobj.editMessage;
    this.newService.postLanguage(userobj);
    //console.log(userobj)

    this.refresh();
  }

  refresh(): void {
    //console.log("*********refresh**********")
    window.location.reload();
  }

  editMessage = function (temp, lid) {
    this.editEmailTemplate = temp;
    this.tempid = temp.emailTemplateId;
    this.tempvid = temp.emailTemplateVersionId;
    this.langid = lid.languageId;

    this.newService.getLanguageByTempIdTempVidLangId(this.tempid, this.tempvid, this.langid)
      .subscribe(data => {
        this.editSubjectValue = data.translationMailSubject;
        this.editMessageValue = data.translationMailMessage;
        //console.log(data.translationMailSubject + "++++++9999++++++" + data.translationMailMessage);
        this.form = new FormGroup({
          editSubject: new FormControl(this.editSubjectValue),
          editMessage: new FormControl(data.translationMailMessage),
        });
        
      });
    this.editLangId = lid.spdlanguage.language;
    this.editEmailTemplate = temp.mailSendEvent;

  }

}
