import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SliderAndSorterTableComponent } from './slider-and-sorter-table.component';

describe('SliderAndSorterTableComponent', () => {
  let component: SliderAndSorterTableComponent;
  let fixture: ComponentFixture<SliderAndSorterTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SliderAndSorterTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SliderAndSorterTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
