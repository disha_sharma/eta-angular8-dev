import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddTaskDetailPopupComponent } from './add-task-detail-popup.component';

describe('AddTaskDetailPopupComponent', () => {
  let component: AddTaskDetailPopupComponent;
  let fixture: ComponentFixture<AddTaskDetailPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddTaskDetailPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddTaskDetailPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
