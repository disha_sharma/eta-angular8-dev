import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { FormGroup, FormBuilder, FormArray } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { ServiceService } from '../../eta-services/service.service';
import { TaskServiceService } from '../../dash-board/task-service.service';
import { DashboardServiceService } from '../../dash-board/dashboard-service.service';
//import { forEach } from '@angular/router/src/utils/collection';
import { element } from 'protractor';
import { ActivatedRoute, Router } from '@angular/router';

export interface DialogData {
  id: "",
  action: ''
}

@Component({
  selector: 'app-add-task',
  templateUrl: './add-task.component.html',
  styleUrls: ['./add-task.component.css']
})
export class AddTaskComponent implements OnInit {
  taskTypeId: any;
  taskfrm: FormGroup;
  taskFlowName: string = '';
  Core_URL: any;
  category: any;
  allTaskType: any;
  subTaskType: any;
  dataContainer: any;
  allForm: any;
  taskObj: any = {};
  taskflowId: any ;
  responseMsg: any = {};
  public isSpinnerShow: boolean = false;
  private taskEditObj: any;
  private buttonAction: boolean;
  private actionValue: String;
  private editTrueFlag: boolean = false;
  public allTask: any;
  public relationalOpraters:any;
  public categoryValue:any;

  public taskFlowList: any=[];

  currentTaskFlowId: any;

  constructor(private _formBuilder: FormBuilder, private http: HttpClient, public shareServices: ServiceService, public dialogRef: MatDialogRef<AddTaskComponent>,
              @Inject(MAT_DIALOG_DATA) data,  @Inject(MAT_DIALOG_DATA) public popupData: DialogData, private dashboardService : DashboardServiceService, public taskService: TaskServiceService,
              private activeRouter: ActivatedRoute) {

                this.getAllTaskFlowList();
    this.Core_URL = this.shareServices.Core_URL;
    this.taskflowId = data.taskFlowId;
    console.log("this.taskflowId:::: "+this.taskflowId);
    

  }

  ngOnInit() {
    this.currentTaskFlowId = this.activeRouter.snapshot.params['taskFlowId'];
    console.log("this.taskflowId:::: "+this.taskflowId);
    this.taskfrm = this._formBuilder.group({

      previousTask: [],
      taskName: [],
      category: [],
      taskType: [],
      container: [],
      taskDes: [],
      privilege: [],
      form: [],

      darivationTaskList: this._formBuilder.array([this.intiDerivationTaskArray()])

    });

   // this.taskfrm.setControl('darivationTaskList', this._formBuilder.array([]));
    this.actionValue = this.popupData.action;

    /* Functions Start Here */
    this.getAllTaskType();
    this.getAllDataContainer();
    this.getAllForm();
    this.getAllTaskByTaskFlowId(this.taskflowId)
    this.getAllOperators();
    this.shareServices.currentMessage.subscribe(message => this.taskEditObj = message);
    
  }

  intiDerivationTaskArray() {
    return this._formBuilder.group({
      // list all your form controls here, which belongs to your form Business rule array
      dataElementOneID: [],
      derivationOperatorID: [],
      dataElementTwoID: [],
      derivationDataElementID:[],
    });
  }

  



  taskEditFx() {
    if (this.actionValue == "Edit") {
      //  alert(this.taskEditObj.subTaskTypeID)
      console.log("i am in edit" + this.taskEditObj.subTaskTypeID);

      this.taskTypeId = this.taskEditObj.subTaskTypeID;
      this.taskfrm.patchValue({ previousTask: this.taskEditObj.previousTaskID });
      this.taskfrm.patchValue({ taskName: this.taskEditObj.taskName });
      this.taskfrm.patchValue({ taskDes: this.taskEditObj.taskDescription });
      this.taskfrm.patchValue({ privilege: this.taskEditObj.previlege });
      this.taskfrm.patchValue({ container: this.taskEditObj.taskFlowID });
      this.taskfrm.patchValue({ category: this.taskEditObj.taskTypeID });
      this.taskfrm.patchValue({ taskType: this.taskTypeId });
      this.taskfrm.patchValue({ form: this.taskEditObj.formId });
      this.taskfrm.patchValue({ darivationTaskList :this.taskEditObj.derivationList})
      this.categoryChange(this.taskEditObj.taskTypeID, this.taskTypeId)


    }
  }

  foods: any = [
    { value: 1, viewValue: 'START TASK' },
    { value: 2, viewValue: 'MIDDLE' },
    { value: 3, viewValue: 'END TASK' }
  ];

  taskTypeFx(taskType) {
    this.taskTypeId = taskType;
    console.log("The Task Type is :====" + this.taskTypeId);

  }

  public getAllTaskType() {
    this.shareServices.getAllTaskType()
      .subscribe(data => {
        this.category = data;
        console.log("i am cat");
        this.taskEditFx();
      });
  }

  public categoryChange(changevalue: any, value: any) {

    console.log(changevalue);
    this.categoryValue = changevalue;

    this.category.forEach(element => {
      if (element.taskTypeID == changevalue) {
        this.subTaskType = element.subTaskTypeDAO;
        console.log(this.subTaskType);
        this.taskTypeFx(value);
      }
    });
  }

  onSubmit = function (taskValue) {
    console.log(taskValue);
    console.log(taskValue.darivationTaskList);
    
    this.taskObj["taskID"] = this.taskEditObj.taskID;
    this.taskObj["taskName"] = this.taskfrm.value.taskName;
    this.taskObj["taskDescription"] = this.taskfrm.value.taskDes;
    this.taskObj["previlege"] = this.taskfrm.value.privilege;
    this.taskObj["privilegeName"] = this.taskfrm.value.privilege;
    this.taskObj["privilegeDescription"] = this.taskfrm.value.privilege;

    this.taskObj["taskFlowID"] = this.taskflowId;

    this.taskObj["previousTaskID"] = this.taskfrm.value.previousTask;
    this.taskObj["taskTypeID"] = this.taskfrm.value.category;
    this.taskObj["subTaskTypeID"] = this.taskfrm.value.taskType;
    this.taskObj["formId"] = this.taskfrm.value.form;
    this.taskObj["isStartOrEntTask"] = 0;
    this.taskObj["derivationList"] = taskValue.darivationTaskList;


    /* edit Value */
    this.taskObj["privilegeId"] = this.taskEditObj.privilegeId
    console.log("add-task compo ......."+this.taskflowId);
    
    //this.taskService.saveTask(this.taskFlowId);
    ///this.shareServices.setTaskFlowId(this.taskflowId);
    console.log(this.taskObj);
   // this.isSpinnerShow = true;
    //this.shareServices.saveTask(this.taskObj).subscribe();
     this.http.post(this.Core_URL + '/saveTask/', this.taskObj)
      .subscribe(data => {
        console.log(data);

        if (data != null) {
          //alert("i am in success");
          this.responseMsg.displayMsg = "Successfully added Leasing and placed it at the end of the display order. Drag it up to reorder.";
          this.responseMsg.cssClass = "Success";
          this.isSpinnerShow = false;
        } else {
          //  alert("i am in Error");
          this.responseMsg.displayMsg = "Error added Leasing and placed it at the end of the display order. Drag it up to reorder.";
          this.responseMsg.cssClass = "Error";
          this.isSpinnerShow = false;
        }
      }, error => {
        this.responseMsg.displayMsg = "Error added Leasing and placed it at the end of the display order. Drag it up to reorder.";
        this.responseMsg.cssClass = "Error";
        this.isSpinnerShow = false;
      });

  }

  getAllDataContainer() {
    this.shareServices.getAllDataContainer()
      .subscribe(res => {
        this.dataContainer = res;
        console.log(res);

      })
  }

  public getAllForm() {
    this.http.get(this.Core_URL + '/getAllForm')
      .subscribe(data => {
        this.allForm = data;
      });
  }


  public getAllTaskByTaskFlowId(taskFlowId: number) {


    this.http.get(this.Core_URL + '/getAllTaskByTaskFlowId/' + taskFlowId)
      .subscribe(data => {
        console.log(data);

        let taskList: any = [];
        taskList = data;

        if (taskList.length == 0) {
          alert('No task available');
        }
        this.allTask = data;
        console.log(data);

      });
  }

  addNewDerivation() {
    // control refers to your formarray
    const control = <FormArray>this.taskfrm.controls['darivationTaskList'];
    // add new formgroup
    control.push(this.intiDerivationTaskArray());
  }


  /* Get All Operator  */

  getAllOperators(){
    this.http.get(this.Core_URL + '/getAllSpdOperators')
    .subscribe(data => {
      this.relationalOpraters = data;
      console.log(data);
      
    });
  }

  /** This Method is used for Display Task FLow Name in Dialog Box */
  getAllTaskFlowList(){
      this.taskService.getTaskFlowNameList().subscribe(data => {
          this.taskFlowList = data;
          this.taskFlowList.forEach(element =>{
            if (this.taskflowId == element.taskFlowID){
              this.taskFlowName = element.taskFlowName;
            }
          });
      });
  }

}
