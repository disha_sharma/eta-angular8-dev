import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScriptApprovalApplicationFormComponent } from './script-approval-application-form.component';

describe('ScriptApprovalApplicationFormComponent', () => {
  let component: ScriptApprovalApplicationFormComponent;
  let fixture: ComponentFixture<ScriptApprovalApplicationFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScriptApprovalApplicationFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScriptApprovalApplicationFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
