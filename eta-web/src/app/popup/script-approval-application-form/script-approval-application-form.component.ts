
import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {FormControl, Validators} from '@angular/forms'; 
@Component({
  selector: 'app-script-approval-application-form',
  templateUrl: './script-approval-application-form.component.html',
  styleUrls: ['./script-approval-application-form.component.css']
})
export class ScriptApprovalApplicationFormComponent implements OnInit {
  ngOnInit(){}

  constructor(
    public dialogRef: MatDialogRef<ScriptApprovalApplicationFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {}

  onNoClick(): void {
    this.dialogRef.close();
  }
 
   //salutation code
   salutations: any = ['option1', 'option2', 'option3'];

//authore first Name field code
  firstNameField = new FormControl('', [Validators.required]); // for custom input field error
  

  //suffixes code
  suffixes: any = ['option1', 'option2', 'option3'];
  //nationality field code
  nationalityFormField = new FormControl('', [Validators.required]);
  nationality: any = ['option1', 'option2', 'option3'];

  //genre field code
  gnereValues: any = ['option1', 'option2', 'option3'];

   //type field code
   typeFormField = new FormControl('', [Validators.required]);
   types: any = ['option1', 'option2', 'option3'];

   //file upload code strts here
   fileInput: string = "";
   hide: boolean = false;
   fileNotSupported: boolean;
   newInput: string;
   validatedStatus: boolean;
   uploadButtonEnabled: boolean = false;
   progressBarValue = 0;
   validateAndNotSupportedControls: boolean; // for showing validation and not supported buttons on screen.

   
  //function for upload file
  uploadFile() {
    if(this.validatedStatus){
      return true;
    }
    else{
    this.validateAndNotSupportedControls = true;
    this.progressBarValue = 50;
    }
  }
//function for successfully Validate 
  validate() {
    this.validatedStatus = true;
    this.progressBarValue = 100;
    this.uploadButtonEnabled = true;
    this.validateAndNotSupportedControls = false;
  }
  // function for make the file not supported
  notSupported(){
    this.progressBarValue = 100;
    this.fileNotSupported = true;
    this.validatedStatus = false;
    this.uploadButtonEnabled = false;
    this.validateAndNotSupportedControls = false;
    
  }
  inputChangeFunction(){
    
    this.fileNotSupported = false;
    this.validatedStatus = false;
    this.validateAndNotSupportedControls = false;
    this.progressBarValue = 0;
    if(this.fileInput) {
      this.uploadButtonEnabled = true;
    }
    else{
      this.uploadButtonEnabled = false;
    }
  }
  //file upload code ends here



}
