import { Component, OnInit, Inject } from '@angular/core';
import { ServiceService } from './../../eta-services/service.service';
import { Router } from '@angular/router';
import { DOCUMENT } from '@angular/common';
//import {DOCUMENT} from '@angular/platform-browser';
//import { port } from '_debugger';

@Component({
  selector: 'app-login-header',
  templateUrl: './login-header.component.html',
  styleUrls: ['./login-header.component.css']
})
export class LoginHeaderComponent implements OnInit {
  firstName: any;
  lastName: any;
  sendOrgId: any;
  getlogo: any;
  hostUrl:any;


  orgUrl: any;
  LastloginTime: any;
  CurrentUserName: any;
  currentUserLogin:any;
  loggedIn  :any;
  Hostport :any ;
  IsloginOrNot:any;

  constructor(private newService: ServiceService, private r: Router,@Inject(DOCUMENT) private document) {
    this.firstName = null;
    this.lastName = null;
    //this.getLoginUserInfo()
    //this.getUserDetailByUserId();
    this.Hostport = document.location.port;
    this.hostUrl = document.location.hostname ;
    this.getOrgdatabyOrgId(this.hostUrl);
    
  }
  userDetails: any;


  ngOnInit() {

    this.IsloginOrNot ="show";

  }

  getOrgdatabyOrgId(hostUrl) {
    this.newService.getOrgLogoInfoByDomain(this.hostUrl)
      .subscribe(data => {
        this.sendOrgId = data.orgId;        
        this.getlogo = data.orgLogo;        
        this.newService.getOrgProfileImage(this.sendOrgId, this.getlogo)
        .subscribe(res => {         
        this.createImageFromBlob(res)
        })
      });
  }

  logout() {

    this.newService.getLogoutInfo()
      .subscribe(res => {
        //this.newService.loggedIn=0;
      })
      localStorage.clear();
      sessionStorage.clear();
      this.ngOnInit();
      this.r.navigate(["/login"]);
      this.IsloginOrNot=null;
      this.getLoginUserInfo();
    //this.newService.pageRefresh();
  }
  
  imageToShow: any;

  createImageFromBlob(image: Blob) {
    let reader = new FileReader();
    reader.addEventListener("load", () => {
      this.imageToShow = reader.result;
    }, false);

    if (image) {
      reader.readAsDataURL(image);
    }
  }
  profileToShow: any;
  createImageFromBlobForUser(image: Blob) {
    let reader = new FileReader();
    reader.addEventListener("load", () => {
      this.profileToShow = reader.result;
    }, false);

    if (image) {
      reader.readAsDataURL(image);
    }
  }


  public getLoginUserInfo() {  
    this.newService.getLoginInfo()
      .subscribe(res => {    
        this.IsloginOrNot="show"; 
        this.LastloginTime = res.loginTime;
        this.CurrentUserName = res.userName;
      })
  }
  getUserDetailByUserId() {
    this.newService.getLoggedInUserByUserId()
      .subscribe(data => {
        data.userId;
        data.profileImage;
        this.newService.getUserProfileImage(data.userId, data.profileImage)
          .subscribe(res => {
            this.createImageFromBlobForUser(res)
          })

      })
  }
}
