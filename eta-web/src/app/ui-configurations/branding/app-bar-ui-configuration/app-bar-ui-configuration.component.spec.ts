import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppBarUiConfigurationComponent } from './app-bar-ui-configuration.component';

describe('AppBarUiConfigurationComponent', () => {
  let component: AppBarUiConfigurationComponent;
  let fixture: ComponentFixture<AppBarUiConfigurationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppBarUiConfigurationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppBarUiConfigurationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
