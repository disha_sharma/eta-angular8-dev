import { Component, ViewContainerRef, OnInit } from '@angular/core';

import { ColorPickerService, Cmyk } from 'ngx-color-picker';
import { AppComponent } from '../../../app.component';
import { ServiceService } from '../../../eta-services/service.service';
import { FormControl, Validators } from '@angular/forms';
import { PostLoginDashboardComponent } from '../../../post-login-module/post-login-dashboard/post-login-dashboard.component';
import { MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';


@Component({
  selector: 'app-app-bar-ui-configuration',
  templateUrl: './app-bar-ui-configuration.component.html',
  styleUrls: ['./app-bar-ui-configuration.component.css']
})
export class AppBarUiConfigurationComponent implements OnInit {
  public dashboardId: number;
  public toggle: boolean = false;
  public appBarBgImageName: string = '';
  public orgLogo: string = '';
  public rgbaText: string = 'rgba(165, 26, 214, 0.2)';
  public showChooseAppBarBgOptionCard: boolean;

public colorList = [
    { key: "flame", value: "", friendlyName: "Flame" },
    {key: "orange", value: "#fa761e", friendlyName: "Orange" },
    {key: "infrared",     value: "#ef486e", friendlyName: "Infrared" },
    {key: "male",       value: "#4488ff", friendlyName: "Male Color" },
    {key: "female",     value: "#ff44aa", friendlyName: "Female Color" },
    {key: "paleyellow",    value: "#ffd165", friendlyName: "Pale Yellow" },
    {key: "gargoylegas",  value: "#fde84e", friendlyName: "Gargoyle Gas" },
    {key: "androidgreen",   value: "#9ac53e", friendlyName: "Android Green" },
    {key: "carribeangreen",    value: "#05d59e", friendlyName: "Carribean Green" },
    {key: "bluejeans",    value: "#5bbfea", friendlyName: "Blue Jeans" },
		{key: "cyancornflower",    value: "#1089b1", friendlyName: "Cyan Cornflower" },
		{key: "warmblack",    value: "#06394a", friendlyName: "Warm Black" },
];


  public presetValues : string[] = [];

  public selectedColor: string = 'color1';

  public cmykColor: Cmyk = new Cmyk(0, 0, 0, 0);

  constructor(public vcRef: ViewContainerRef,
     private cpService: ColorPickerService, 
     private appComponent: PostLoginDashboardComponent, 
    private service : ServiceService,
    private _snackBar: MatSnackBar,
    private route: Router) {
    this.presetValues = this.getColorValues();
  }
ngOnInit(){
  this.editFormWithCurrentValues();
}
openSnackBar(message: string) {
  this._snackBar.open(message, null , {
    duration: 2000,
  });
}
  getColorValues(){
  return this.colorList.map(c => c.value);
  }


  public onEventLog(event: string, data: any): void {
  }

  public onChangeColorCmyk(color: string): Cmyk {
    const hsva = this.cpService.stringToHsva(color);

    if (hsva) {
      const rgba = this.cpService.hsvaToRgba(hsva);

      return this.cpService.rgbaToCmyk(rgba);
    }

    return new Cmyk(0, 0, 0, 0);
  }

  public onChangeColorHex8(color: string): string {
    const hsva = this.cpService.stringToHsva(color, true);

    if (hsva) {
      return this.cpService.outputFormat(hsva, 'rgba', null);
    }

    return '';
  }

  submit(){
   let payload : any = {
    dashBoardId: this.dashboardId,
     updateFor : 'appBar',
    appBarBgColor: this.colorList[0].value,
    appBarImageName: this.appBarBgImageName,
    orgLogoImageName: this.orgLogo
   };

      this.service.updateDynamicCss(payload)
      .subscribe(response => {
        this.dashboardId = response.dashBoardId;
        this.setAppBarDynamicCss(response);
       }, error => {
  })
}
setAppBarDynamicCss(dynamicCss){
document.documentElement.style.setProperty('--app-bar-background', dynamicCss.appBarBgColor);
document.documentElement.style.setProperty('--app-bar-text-color', this.appComponent.lightOrDark(dynamicCss.appBarBgColor));
this.appComponent.orgLogoPath = dynamicCss.orgLogoImageName;
this.appComponent.appBarBgImage = dynamicCss.appBarImageName;
this.appBarBgImageName = dynamicCss.appBarImageName;
      this.appBarBgImageFormControl.setValue(dynamicCss.appBarImageName);
      this.orgLogo = dynamicCss.orgLogoImageName;
      this.orgLogoFormControl.setValue(dynamicCss.orgLogoImageName);
      this.openSnackBar('Updated Successfully!')
      
}

editFormWithCurrentValues(){
    this.service.GetDynamicCss()
    .subscribe(response => {
      this.dashboardId = response.dashBoardId;
      this.colorList[0].value = response.appBarBgColor;
      this.appBarBgImageName = response.appBarImageName;
      this.appBarBgImageFormControl.setValue(response.appBarImageName);
      this.orgLogo = response.orgLogoImageName;
      this.orgLogoFormControl.setValue(response.orgLogoImageName);
      if(!response.orgLogoImageName){
        this.submitDisabled = true;
      }
    
    

    }, error => {
  

    })
}

myFunction(event){
  
}
removeAppBarBgImage(){
  let payload : any = {
   dashBoardId: this.dashboardId,
    updateFor : 'appBar',
   appBarBgColor: this.colorList[0].value,
   appBarImageName: null,
   orgLogoImageName: this.orgLogo
  };
     this.service.updateDynamicCss(payload)
     .subscribe(response => {
       this.setAppBarDynamicCss(response);
       this.appBarBgImageChanged(null);
 }, error => {
 })
}

/*Image upload for app bar background starts here */
progressBarValue: number = 0;
uploadAppBarBgButtonEnabled: boolean = false;
appBarBgImageFormControl = new FormControl('');
selectedFilesAppBarBgImg: FileList;
appBarBgImageNgModel: any;
submitDisabled: boolean = false;
uploadAppBarBgImage() {
  if (this.selectedFilesAppBarBgImg && this.selectedFilesAppBarBgImg.item(0)) {
    const file = this.selectedFilesAppBarBgImg.item(0)
    const reader = new FileReader();
    reader.readAsDataURL(this.selectedFilesAppBarBgImg[0]);
    var ext = file.name.substr(file.name.lastIndexOf('.') + 1).toLowerCase();
    if (ext === 'png' || ext === 'jpeg') {
      reader.onload = event => {
        const img = new Image();
        img.src = (event.target as any).result;
        img.onload = () => {
          const elem = document.createElement('canvas');
          elem.width = img.width;
          elem.height = img.height;
          if (elem.width <= 3840 && elem.height <= 184 && (ext === 'png' || ext === 'jpeg' || ext === 'jpg') ) {
            
            let imageForUpload;
            imageForUpload = this.selectedFilesAppBarBgImg.item(0);
            this.service.uploadImageForBranding(imageForUpload, 'appBarBgImg', 'branding').subscribe(response => {
            
              this.appBarBgImageValidate();
              this.appBarBgImageName = response.fileName;
              this.appBarBgImageFormControl.setValue(response.fileName);
          })
          } else {
            this.appBarBgImgeNotSupported();
          }
        },
          reader.onerror = error => console.log(error);
      };
    } else {
      this.appBarBgImgeNotSupported();
    }
  }
}
appBarBgImageValidate() {
 this.progressBarValue = 100;
}
appBarBgImgeNotSupported() {
 this.progressBarValue = 100;
 this.appBarBgImageFormControl.setErrors({ 'invalidFile': true });
  this.appBarBgImageFormControl.markAsTouched();
}
appBarBgImageChanged(event) {
    this.progressBarValue = 0;
    this.uploadAppBarBgButtonEnabled = true;
  if (event && event.target && event.target.files) {
    this.selectedFilesAppBarBgImg = event.target.files;
  }
  this.appBarBgImageFormControl.markAsTouched();
  this.appBarBgImageFormControl.patchValue(this.appBarBgImageNgModel.split('\\').pop());
}
/*Image upload for app bar background ends here */

/*Image upload for org logo starts here */
progressBarValueOrgLogo: number = 0;
uploadOrgLogoButtonEnabled: boolean = false;
orgLogoFormControl = new FormControl('', [Validators.required]);
selectedFilesOrgLogo: FileList;
orgLogoNgModel: any;
uploadOrgLogo() {
  if (this.selectedFilesOrgLogo && this.selectedFilesOrgLogo.item(0)) {
    const file = this.selectedFilesOrgLogo.item(0)
    const reader = new FileReader();
    reader.readAsDataURL(this.selectedFilesOrgLogo[0]);
    var ext = file.name.substr(file.name.lastIndexOf('.') + 1);
    if (ext.toLowerCase() === 'png' || ext.toLowerCase() === 'jpeg') {
      reader.onload = event => {
        const img = new Image();
        img.src = (event.target as any).result;
        img.onload = () => {
          const elem = document.createElement('canvas');
          elem.width = img.width;
          elem.height = img.height;
          if (elem.width <= 512 && elem.height <= 128 && (ext.toLowerCase() === 'png' || ext.toLowerCase() === 'jpeg')) {
           
            let orgLogo ;
            orgLogo = this.selectedFilesOrgLogo.item(0);
            this.service.uploadImageForBranding(orgLogo, 'orgoLogo', 'branding').subscribe(response => {
             this.orgLogoImageValidate();
                this.orgLogo = response.fileName;
              this.orgLogoFormControl.setValue(response.fileName);
            })
          } else {
            this.orgLogoNotSupported();
          }
        },
          reader.onerror = error => console.log(error);
      };
    } else {
      this.orgLogoNotSupported();
    }
  }
}
/*function for validate file */
orgLogoImageValidate() {
 this.progressBarValueOrgLogo = 100;
 this.submitDisabled = false;
}
/*function for file not supported */
orgLogoNotSupported() {
 this.progressBarValueOrgLogo = 100;
 this.orgLogoFormControl.setErrors({ 'invalidFile': true });
  this.orgLogoFormControl.markAsTouched();
}
/*app bar background image changed */
orgLogoChanged(event) {
    this.progressBarValueOrgLogo = 0;
    this.uploadOrgLogoButtonEnabled = true;
    this.submitDisabled = true;
  if (event && event.target && event.target.files) {
    this.selectedFilesOrgLogo = event.target.files;
  }
  this.orgLogoFormControl.markAsTouched();
  this.orgLogoFormControl.patchValue(this.orgLogoNgModel.split('\\').pop());
}
/*Image upload for org logo ends here */

chooseAppBarBgOption(){
  this.showChooseAppBarBgOptionCard = true;
}
cancel(){
  this.route.navigate(['eta-web/welcome'])
}
}