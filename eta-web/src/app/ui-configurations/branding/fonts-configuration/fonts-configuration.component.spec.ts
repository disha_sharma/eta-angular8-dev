import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FontsConfigurationComponent } from './fonts-configuration.component';

describe('FontsConfigurationComponent', () => {
  let component: FontsConfigurationComponent;
  let fixture: ComponentFixture<FontsConfigurationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FontsConfigurationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FontsConfigurationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
