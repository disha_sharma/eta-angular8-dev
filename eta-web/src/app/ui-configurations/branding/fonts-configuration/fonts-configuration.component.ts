import { Component, OnInit } from '@angular/core';
import { ServiceService } from '../../../eta-services/service.service';
//import { NodeStringDecoder } from 'string_decoder';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';

@Component({
  selector: 'app-fonts-configuration',
  templateUrl: './fonts-configuration.component.html',
  styleUrls: ['./fonts-configuration.component.css']
})
export class FontsConfigurationComponent implements OnInit { 
  public dashboardId: number;
  navDrawerFont;
  navDrawerFontColor;
  bodyTextFont;
  bodyTextFontColor;
  headingColor;
  messageFontColor;
  failureMessageFontColor;

  primaryColor: string;
  primaryAccent1Color: string;
  primaryAccent2Color: string;
  secondaryColor: string;
  secondaryAccent1Color: string;
  secondaryAccent2Color: string;
  substrateColor;
  navDrawerColor;
  cardsColor;
  public form: FormGroup;
  webSafeFonts: any = ['roboto', 'Arial Black','Impact', 'Arial',' Helvetica','Times New Roman','Times','Courier New','Courier',
  'Verdana','Georgia','Palatino','Garamond','Bookman','Comic Sans MS','Trebuchet MS']
  constructor(private service: ServiceService, private fb: FormBuilder, private _snackBar: MatSnackBar,
     private route: Router) { }

 

  openSnackBar(message: string) {
    this._snackBar.open(message, null , {
      duration: 2000,
    });
  }
  ngOnInit() {
    
    this.form = this.fb.group({
      'dashBoardId': [],
      'navDrawerFont': [],
      'navDrawerTextColor': [],
      'bodyTextFont': [],
      'bodyTextColor': [],
      'headingTextColor': [],
      'successMessageTextColor': [],
      'failureMessageTextColor': []
    })
    this.editFormWithCurrentValues();
  }
  submit(){
   /* let payload : any = {
      dashBoardId: this.dashboardId,
      updateFor: 'fonts',
      navDrawerFont: this.navDrawerFont,
      navDrawerTextColor: this.navDrawerFontColor,
      bodyTextFont: this.bodyTextFont,
      bodyTextColor: this.bodyTextFontColor,
      headingTextColor: this.headingColor,
      successMessageTextColor: this.messageFontColor,
      failureMessageTextColor: this.failureMessageFontColor
    };*/
    let payload = this.form.value;
    payload.updateFor = 'fonts'
    this.service.updateDynamicCss(payload)
    .subscribe(response => {
      this.dashboardId = response.dashBoardId;
      this.setDynamicFonts(response);
      this.openSnackBar('Updated successfully!');
}, error => {
})
  }
  setDynamicFonts(dynamicCss){
    /*font dynamic styles */
document.documentElement.style.setProperty('--nav-drawer-font-family', dynamicCss.navDrawerFont);
document.documentElement.style.setProperty('--nav-drawer-font-color', dynamicCss.navDrawerTextColor);
document.documentElement.style.setProperty('--body-text-font-family', dynamicCss.bodyTextFont);
document.documentElement.style.setProperty('--body-text-font-color', dynamicCss.bodyTextColor);
document.documentElement.style.setProperty('--heading-text-color', dynamicCss.headingTextColor);
document.documentElement.style.setProperty('--success-color', dynamicCss.successMessageTextColor);
document.documentElement.style.setProperty('--errors', dynamicCss.failureMessageTextColor);
  }

  editFormWithCurrentValues(){
    this.service.GetDynamicCss()
    .subscribe(response => {
      this.form.patchValue(response);
      this.substrateColor = response.substrateColor;
      this.navDrawerColor = response.navDrawerBgColor;
      this.cardsColor = response.cardsBgColor;
      this.primaryColor = response.primaryColor;
      this.primaryAccent1Color = response.primaryAccent1Color;
      this.primaryAccent2Color = response.primaryAccent2Color;
      this.secondaryColor = response.secondaryColor;
      this.secondaryAccent1Color = response.secondaryAccent1Color;
      this.secondaryAccent2Color = response.secondaryAccent2Color;
    }, error => {})

}
cancel(){
  this.route.navigate(['eta-web/welcome'])
}
}
