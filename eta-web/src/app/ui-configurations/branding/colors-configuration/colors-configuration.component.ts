import { Component, ViewContainerRef, OnInit, ViewChild, ChangeDetectorRef, ApplicationRef } from '@angular/core';

import { ColorPickerService, Cmyk } from 'ngx-color-picker';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { ServiceService } from '../../../eta-services/service.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatSelect, MatSnackBar } from '@angular/material';
import { subscribeOn } from 'rxjs/operators';
import { Router } from '@angular/router';

interface ControlValueAccessor {  
  writeValue(obj: any): void
  registerOnChange(fn: any): void
  registerOnTouched(fn: any): void
  setDisabledState(isDisabled: boolean): void
}

@Component({
  selector: 'app-colors-configuration',
  templateUrl: './colors-configuration.component.html',
  styleUrls: ['./colors-configuration.component.css']
})
export class ColorsConfigurationComponent implements OnInit {
  public dashboardId: number;
  public toggle: boolean = false;

  public rgbaText: string = 'rgba(165, 26, 214, 0.2)';

  public arrayColors: any = {
    color1: '#2883e9',
    color2: '#e920e9',
    color3: 'rgb(255,245,0)',
    color4: 'rgb(236,64,64)',
    color5: 'rgba(45,208,45,1)'
  };
 
  public selectedColor: string = 'color1';

  public color1: string;
  public color2: string;
  public color3: string;
  public color4: string;
  public color5: string;
  public color6: string;
  public color7: string;
  public color8: string;
  public color9: string;
  public color10: string;
  public color11: string;
  public color12: string;
  public color13: string;
  public color14: string;
  public color15: string;
  public color16: string;
  public color17: string;
  public color18: string;

  public cmykColor: Cmyk = new Cmyk(0, 0, 0, 0);

  public colorOptions = [
    {name: 'Primary', value: this.color1},
    {name: 'Primary Accent 1', value: this.color2 },
    {name: 'Primary Accent 2', value: this.color3},
    {name: 'Secondary', value: this.color4},
    {name: 'Secondary Accent 1', value: this.color5},
    {name: 'Secondary Accent 2', value: this.color6},
    {name: 'Black', value: '#000000'},
    {name: 'White', value: '#ffffff'},
    {name: 'Gray', value: '#EEEEEE'}
  ]
 public substrateColor = {name: '', value: ''};
  public navDrawerColor = {name: '', value: ''};
  public cardsColor = {name: '', value: ''};
  public hyperLinkOnDisplayColor = {name: '', value: ''};
  public hyperLinkHoverColor = {name: '', value: ''};
  public hyperLinkOnClickColor = {name: '', value: ''};
  public buttonColor = {name: '', value: ''};
  public buttonHoverColor = {name: '', value: ''};
  public buttonClickColor = {name: '', value: ''};
  public buttonTextColorDisabled = {name: '', value: ''};
  public buttonTextColorEnabled = {name: '', value: ''};
  public buttonTextHoverColor = {name: '', value: ''};
  public buttonTextClickColor = {name: '', value: ''};


  form: FormGroup;

 

  constructor(private cdRef:ChangeDetectorRef, private fb: FormBuilder, public vcRef: ViewContainerRef,
     private cpService: ColorPickerService,  private spinnerService: Ng4LoadingSpinnerService, private service : ServiceService, 
     private _snackBar: MatSnackBar,
     private route: Router) {}
 

  openSnackBar(message: string) {
    this._snackBar.open(message, null , {
      duration: 2000,
    });
  }
changed(){
  this.cdRef.detectChanges();
}
  ngAfterViewChecked()
  {
    
    this.cdRef.detectChanges();
    
  }
  ngOnInit(){
    this.form = this.fb.group({
      'dashBoardId': [],
      'primaryColor': [],
   'primaryAccent1Color': [],
   'primaryAccent2Color': [],
   'secondaryColor': [],
   'secondaryAccent1Color': [],
   'secondaryAccent2Color': [],
   'substrateColor': [],
   'navDrawerBgColor': [],
   'cardsBgColor': [],
   'hyperlinkColor': [],
   'hyperlinkHoverColor': [],
   'hyperlinkClickColor': [],
   'buttonBgColor': [],
   'buttonHoverBgColor': [],
   'buttonClickBgColor': [],
   'buttonDisabledTextColor': [],
   'buttonEnabledTextColor': [],
   'buttonHoverTextColor': [],
   'buttonClickTextColor': [],
    })

    this.editFormWithCurrentValues();
  }
  public onEventLog(event: string, data: any): void {
  }


  doCompare(value: string) {
  //   if(this.substrateColor){
  //   this.substrateColor.value === value ? this.substrateColor = this.colorOptions.find(x => x.value === value) : null;
  //   this.navDrawerColor.value === value ? this.navDrawerColor = this.colorOptions.find(x => x.value === value) : null
  // }
  }

  public onChangeColorCmyk(color: string): Cmyk {
    const hsva = this.cpService.stringToHsva(color);

    if (hsva) {
      const rgba = this.cpService.hsvaToRgba(hsva);

      return this.cpService.rgbaToCmyk(rgba);
    }

    return new Cmyk(0, 0, 0, 0);
  }

  public onChangeColorHex8(color: string): string {
    const hsva = this.cpService.stringToHsva(color, true);

    if (hsva) {
      return this.cpService.outputFormat(hsva, 'rgba', null);
    }

    return '';
  }
  
  submit(){
   let payload : any = {
   dashBoardId: this.dashboardId,
   updateFor : 'color',
   primaryColor: this.colorOptions[0].value,
   primaryAccent1Color: this.colorOptions[1].value,
   primaryAccent2Color: this.colorOptions[2].value,
   secondaryColor: this.colorOptions[3].value,
   secondaryAccent1Color: this.colorOptions[4].value,
   secondaryAccent2Color: this.colorOptions[5].value,
   substrateColor: this.substrateColor.value,
   navDrawerBgColor: this.navDrawerColor.value,
   cardsBgColor: this.cardsColor.value,
   hyperlinkColor: this.hyperLinkOnDisplayColor.value,
   hyperlinkHoverColor: this.hyperLinkHoverColor.value,
   hyperlinkClickColor: this.hyperLinkOnClickColor.value,
   buttonBgColor: this.buttonColor.value,
   buttonHoverBgColor: this.buttonHoverColor.value,
   buttonClickBgColor: this.buttonClickColor.value,
   buttonDisabledTextColor: this.buttonTextColorDisabled.value,
   buttonEnabledTextColor: this.buttonTextColorEnabled.value,
   buttonHoverTextColor: this.buttonTextHoverColor.value,
   buttonClickTextColor: this.buttonTextClickColor.value,
    }; 
  //  let payload =this.form.value;
  //  payload.updateFor = 'color';
   
      this.service.updateDynamicCss(payload)
      .subscribe(response => {
        this.dashboardId = response.dashBoardId;
        this.setDynamicColors(response);
        this.openSnackBar('Updated successfully!')
  }, error => {
  })
    }


    setDynamicColors(dynamicCss){
      /*colors dynamic styles */
document.documentElement.style.setProperty('--primary', dynamicCss.primaryColor);
document.documentElement.style.setProperty('--primary-rgb', this.convertHexToReb(dynamicCss.primaryColor));
document.documentElement.style.setProperty('--secondary', dynamicCss.secondaryColor);
document.documentElement.style.setProperty('--substrate', dynamicCss.substrateColor);
document.documentElement.style.setProperty('--nav-drawer-background-color', dynamicCss.navDrawerBgColor);
document.documentElement.style.setProperty('--nav-drawer-background-color-rgb', this.convertHexToReb(dynamicCss.navDrawerBgColor));
document.documentElement.style.setProperty('--cards-background-color', dynamicCss.cardsBgColor);
document.documentElement.style.setProperty('--hyperlink-color', dynamicCss.hyperlinkColor);
document.documentElement.style.setProperty('--hyperlink-hover-color', dynamicCss.hyperlinkHoverColor);
document.documentElement.style.setProperty('--hyperlink-click-color', dynamicCss.hyperlinkClickColor);
document.documentElement.style.setProperty('--button-background-color', dynamicCss.buttonBgColor);
document.documentElement.style.setProperty('--button-hover-background-color', dynamicCss.buttonHoverBgColor);
document.documentElement.style.setProperty('--button-click-background-color', dynamicCss.buttonClickBgColor);
document.documentElement.style.setProperty('--button-text-color-enabled', dynamicCss.buttonEnabledTextColor);
document.documentElement.style.setProperty('--button-text-color-disabled', dynamicCss.buttonDisabledTextColor);
document.documentElement.style.setProperty('--button-text-color-hover', dynamicCss.buttonHoverTextColor);
document.documentElement.style.setProperty('--button-text-color-click', dynamicCss.buttonHoverTextColor);
    }
  convertHexToReb(hexValue){
    let hex = hexValue.replace('#','');
   let r = parseInt(hex.substring(0,2), 16);
   let  g = parseInt(hex.substring(2,4), 16);
   let b = parseInt(hex.substring(4,6), 16);
    let rgb = r+','+g+','+b;
    return rgb;
  }
  editFormWithCurrentValues(){
    this.service.GetDynamicCss()
    .subscribe(response => {
      //this.form.patchValue(response);
      this.dashboardId = response.dashBoardId;
     this.color1 = response.primaryColor;
     this.color2 = response.primaryAccent1Color;
     this.color3 = response.primaryAccent2Color;
     this.color4 = response.secondaryColor;
     this.color5 = response.secondaryAccent1Color;
     this.color6 = response.secondaryAccent2Color;
     this.colorOptions[0].value = response.primaryColor;
     this.colorOptions[1].value = response.primaryAccent1Color;
     this.colorOptions[2].value = response.primaryAccent2Color;
     this.colorOptions[3].value = response.secondaryColor;
     this.colorOptions[4].value = response.secondaryAccent1Color;
     this.colorOptions[5].value = response.secondaryAccent2Color;
      this.substrateColor = this.colorOptions.find(x => x.value ==  response.substrateColor) ? this.colorOptions.find(x => x.value ==  response.substrateColor) : {name: 'defualut', value : response.substrateColor};
      this.navDrawerColor =  this.colorOptions.find(x => x.value ==  response.navDrawerBgColor) ? this.colorOptions.find(x => x.value ==  response.navDrawerBgColor) : {name: 'default', value : response.navDrawerBgColor};
      this.cardsColor = this.colorOptions.find(x => x.value ==  response.cardsBgColor) ? this.colorOptions.find(x => x.value ==  response.cardsBgColor) : {name: 'defualut', value : response.cardsBgColor};
      this.hyperLinkOnDisplayColor = this.colorOptions.find(x => x.value ==  response.hyperlinkColor) ? this.colorOptions.find(x => x.value ==  response.hyperlinkColor) : {name: 'defualut', value : response.hyperlinkColor};
      this.hyperLinkHoverColor = this.colorOptions.find(x => x.value ==  response.hyperlinkHoverColor) ? this.colorOptions.find(x => x.value ==  response.hyperlinkHoverColor) : {name: 'defualut', value : response.hyperlinkHoverColor};
      this.hyperLinkOnClickColor = this.colorOptions.find(x => x.value ==  response.hyperlinkClickColor) ? this.colorOptions.find(x => x.value ==  response.hyperlinkClickColor) : {name: 'defualut', value : response.hyperlinkClickColor};
      this.buttonColor = this.colorOptions.find(x => x.value ==  response.buttonBgColor) ? this.colorOptions.find(x => x.value ==  response.buttonBgColor) : {name: 'defualut', value : response.buttonBgColor};
      this.buttonHoverColor =this.colorOptions.find(x => x.value ==  response.buttonHoverBgColor) ? this.colorOptions.find(x => x.value ==  response.buttonHoverBgColor) : {name: 'defualut', value : response.buttonHoverBgColor};
      this.buttonClickColor = this.colorOptions.find(x => x.value ==  response.buttonClickBgColor) ? this.colorOptions.find(x => x.value ==  response.buttonClickBgColor) : {name: 'defualut', value : response.buttonClickBgColor};
      this.buttonTextColorEnabled = this.colorOptions.find(x => x.value ==  response.buttonEnabledTextColor) ? this.colorOptions.find(x => x.value ==  response.buttonEnabledTextColor) : {name: 'defualut', value : response.buttonEnabledTextColor};
      this.buttonTextColorDisabled = this.colorOptions.find(x => x.value ==  response.buttonDisabledTextColor) ? this.colorOptions.find(x => x.value ==  response.buttonDisabledTextColor) : {name: 'defualut', value : response.buttonDisabledTextColor};
      this.buttonTextHoverColor = this.colorOptions.find(x => x.value ==  response.buttonHoverTextColor) ? this.colorOptions.find(x => x.value ==  response.buttonHoverTextColor) : {name: 'defualut', value : response.buttonHoverTextColor};
      this.buttonTextClickColor = this.colorOptions.find(x => x.value ==  response.buttonClickTextColor) ? this.colorOptions.find(x => x.value ==  response.buttonClickTextColor) : {name: 'defualut', value : response.buttonClickTextColor};
     
    }, error => {
  })
  }
  cancel(){
    this.route.navigate(['eta-web/welcome'])
  }
}
