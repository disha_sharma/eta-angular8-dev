import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ColorsConfigurationComponent } from './colors-configuration.component';

describe('ColorsConfigurationComponent', () => {
  let component: ColorsConfigurationComponent;
  let fixture: ComponentFixture<ColorsConfigurationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ColorsConfigurationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ColorsConfigurationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
