import { Component, OnInit } from '@angular/core';
import {ViewChild} from '@angular/core';
import { MatTabChangeEvent } from '@angular/material';
import { AppBarUiConfigurationComponent } from './app-bar-ui-configuration/app-bar-ui-configuration.component';
import { ColorsConfigurationComponent } from './colors-configuration/colors-configuration.component';
import { FontsConfigurationComponent } from './fonts-configuration/fonts-configuration.component';
import { AppComponent } from '../../app.component';
import { PostLoginDashboardComponent } from '../../post-login-module/post-login-dashboard/post-login-dashboard.component';
//...

@Component({
  selector: 'app-branding',
  templateUrl: './branding.component.html',
  styleUrls: ['./branding.component.css']
})
export class BrandingComponent implements OnInit {
  @ViewChild(AppBarUiConfigurationComponent, {static: true}) private AppBarUiConfigurationComponent: AppBarUiConfigurationComponent;
  @ViewChild(ColorsConfigurationComponent, {static: true}) private ColorsConfigurationComponent: ColorsConfigurationComponent;
  @ViewChild(FontsConfigurationComponent, {static: true}) private FontsConfigurationComponent : FontsConfigurationComponent;
  @ViewChild(AppComponent, {static: true}) private AppComponent : AppComponent;
  constructor(private appComponent: PostLoginDashboardComponent) { }

  ngOnInit() {
  }
  onTabChanged(event: MatTabChangeEvent) 
  {
    if(event.index == 0)
    {
         this.AppBarUiConfigurationComponent.editFormWithCurrentValues();//Or whatever name the method is called
    }
    else if(event.index == 1)
    {
        this.ColorsConfigurationComponent.editFormWithCurrentValues(); 
    }
    else{
        this.FontsConfigurationComponent.editFormWithCurrentValues();
    }
  }
  getHeaderMenuList (){
    this.appComponent.getHeaderMenuList();
  }
}
